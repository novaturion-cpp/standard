configuration = "%{cfg.system}-%{cfg.buildcfg}-%{cfg.platform}"

workspaceDir = os.getcwd() .. "/"
projectDir = "%{prj.group}/%{prj.name}/"

binaryName = "%{prj.name}"
binaryDir = "binary/" .. configuration .. "/"
intermediateDir = "binary/intermediate/" .. configuration .. "/"

includeDir = "include/"
sourceDir = "source/"

projectFiles = "**premake5.lua"
thirdPartyProjectFiles = "**%{prj.name}.lua"

asmFiles = "**.asm"
inlineFiles = includeDir .. "**.inl"
includeFiles = includeDir .. "**.hpp"
sourceFiles = sourceDir .. "**.cpp"

precompiledName = "precompiled-%{prj.name}"
precompiledHeader = precompiledName .. ".hpp"
precompiledSource = "source/" .. precompiledName .. ".cpp"

workspace ( path.getbasename(os.getcwd()) )
	language "c++"
	cppdialect "c++20"
	startproject "benchmark"

	platforms {
		"x86",
		"x64"
	}
	configurations {
		"debug",
		"fast-debug",
		"release"
	}

	filter { "platforms:x64" }
	   architecture "x86_64"

	filter { "platforms:x86" }
	   architecture "x86"

	filter { "configurations:debug" }
	   symbols "on"

	filter { "configurations:fast-debug" }
	   symbols "on"
	   optimize "on"

	filter { "configurations:release" }
	   symbols "off"
	   optimize "full"

    filter { "files:*.lua" }
        buildaction ( "none" )

	filter { }

	group ( "third-party" )
		include ( "third-party" )
	group ( "" )

	include ( "standard" )

	group ( "test" )
		include ( "test/benchmark" )
		include ( "test/unit-test" )
	group ( "" )


-- todo: gmake support

require('vstudio')

	function setObjFileName(cfg)
		local data = ""
		local extension = ".o"

		if string.startswith(_ACTION, "vs") and cfg.system == "windows" then
			extension = extension .. "bj"
			data = "<ObjectFileName>$(IntDir)%%(RelativeDir)%%(Filename)" .. extension .. "</ObjectFileName>"
		end

		if not (data == "") then
			premake.w(data)
		end
	end

	premake.override(
		premake.vstudio.vc2010.elements,
		"clCompile",
		function(base, cfg)
			local calls = base(cfg)
			table.insert(calls, setObjFileName)
			return calls
		end
	)
