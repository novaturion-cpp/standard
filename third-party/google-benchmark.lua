googleBenchmarkIncludeDir = os.getcwd() .. "/google-benchmark/include/"

project "google-benchmark"
	kind "staticlib"
	links { "shlwapi" }

	targetname ( binaryName )

	targetdir ( thirdPartyBinaryDir )
	objdir ( thirdPartyIntermediateDir )

	includedirs {
		googleBenchmarkIncludeDir,
		"%{prj.name}/src/"
	}
	files {
		thirdPartyProjectFiles,
		googleBenchmarkIncludeDir .. "**/*.h",
		"%{prj.name}/src/*.cc"
	}

	defines {
		"WIN32",
		"_WINDOWS",
		"BENCHMARK_STATIC_DEFINE",
		"_CRT_SECURE_NO_WARNINGS",
		"HAVE_STD_REGEX",
		"HAVE_STEADY_CLOCK"
	}

	filter { "configurations:debug" }
		defines { 'CMAKE_INTDIR="Debug"' }

	filter { "configurations:release" }
		defines { "NDEBUG", 'CMAKE_INTDIR="Release"' }

	filter { }
