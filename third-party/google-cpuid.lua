googleCpuidIncludeDir = os.getcwd() .. "/google-cpuid/include/"

project "google-cpuid"
	kind "staticlib"

	targetname ( binaryName )

	targetdir ( thirdPartyBinaryDir )
	objdir ( thirdPartyIntermediateDir )

	includedirs {
		googleCpuidIncludeDir
	}
	files {
		thirdPartyProjectFiles,
		googleCpuidIncludeDir .. "cpu*.h",
		"%{prj.name}/src/impl_*.inl",
		"%{prj.name}/src/impl_*.c"
	}
