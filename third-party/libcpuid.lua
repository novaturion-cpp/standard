﻿libcpuidIncludeDir = os.getcwd() .. "/libcpuid/"

project "libcpuid"
	kind "staticlib"

	targetname ( binaryName )

	targetdir ( thirdPartyBinaryDir )
	objdir ( thirdPartyIntermediateDir )
	
	includedirs {
		libcpuidIncludeDir
	}
	files {
		thirdPartyProjectFiles,
		libcpuidIncludeDir .. "**/libcpuid.h",
		libcpuidIncludeDir .. "libcpuid/*.c"
	}

	defines { "VERSION=\"0.6.5\"" }

	filter { "platforms:x64" }
		files { libcpuidIncludeDir .. "libcpuid/*.asm" }

	filter { }
