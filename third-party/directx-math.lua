directxMathIncludeDir = os.getcwd() .. "/directx-math/"

project "directx-math"
	kind "staticlib"

	targetname ( binaryName )

	targetdir ( thirdPartyBinaryDir )
	objdir ( thirdPartyIntermediateDir )

	includedirs {
		directxMathIncludeDir
	}
	files {
		thirdPartyProjectFiles,
		directxMathIncludeDir .. "**/*.h"
	}
