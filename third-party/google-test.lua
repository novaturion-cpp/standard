googleTestIncludeDir = os.getcwd() .. "/google-test/googletest/include/"

project "google-test"
	kind "staticlib"

	targetname ( binaryName )

	targetdir ( thirdPartyBinaryDir )
	objdir ( thirdPartyIntermediateDir )

	includedirs {
		googleTestIncludeDir,
		"%{prj.name}/googletest/"
	}
	files {
		thirdPartyProjectFiles,
		googleTestIncludeDir .. "**/*.h",
		"%{prj.name}/**/gtest-all.cc",
		"%{prj.name}/**/gtest_main.cc"
	}
