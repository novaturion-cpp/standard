vectorClassIncludeDir = os.getcwd() .. "/vector-class/"

project "vector-class"
	kind "staticlib"

	targetname ( binaryName )

	targetdir ( thirdPartyBinaryDir )
	objdir ( thirdPartyIntermediateDir )

	includedirs {
		vectorClassIncludeDir
	}
	files {
		thirdPartyProjectFiles,
		vectorClassIncludeDir .. "**/*.h"
	}
