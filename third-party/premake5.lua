thirdPartyDir = os.getcwd() .. "/"

thirdPartyBinaryDir = "binary/%{prj.name}/" .. configuration .. "/"
thirdPartyIntermediateDir = "binary/%{prj.name}/intermediate/" .. configuration .. "/"

include ( "google-benchmark" )
include ( "google-cpuid" )
include ( "google-test" )

include ( "libcpuid" )

include ( "directx-math" )
include ( "vector-class" )
