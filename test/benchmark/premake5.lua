benchmarkIncludeDir = os.getcwd() .. "/include/"

project ( path.getbasename(os.getcwd()) )
	kind "consoleapp"
	links {
		"google-benchmark",
		"google-cpuid",
		"libcpuid",
		"standard"
	}

	pchheader ( precompiledHeader )
	pchsource ( precompiledSource )

	targetname ( binaryName )

	targetdir ( binaryDir )
	objdir ( intermediateDir )

	includedirs {
		includeDir,
		googleBenchmarkIncludeDir,
		directxMathIncludeDir,
		vectorClassIncludeDir,
		googleCpuidIncludeDir,
		libcpuidIncludeDir,
		standardIncludeDir
	}
	files {
		projectFiles,
		inlineFiles,
		includeFiles,
		sourceFiles
	}

	vectorextensions ( "avx2" )

	defines { "BENCHMARK_STATIC_DEFINE" }

	filter { "action:vs*" }
		buildoptions { "/FAs", "/Fa\"$(IntDir)%(RelativeDir)%(Filename).asm\"" }

	filter { }
