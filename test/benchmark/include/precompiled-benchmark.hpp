#pragma once
#include <cmath>
#include <cpuinfo_x86.h>

#include <directx-math/Inc/DirectXMath.h>

#include <libcpuid/libcpuid.h>

#include <standard/math/vector/bitwise.hpp>
#include <standard/math/vector/common.hpp>
#include <standard/math/vector/comparison.hpp>
#include <standard/math/vector/constant.hpp>
#include <standard/math/vector/exponential.hpp>

#include <standard/platform/cpu/x86.hpp>

#include <vector-class/vectormath_common.h>
#include <vector-class/vectormath_exp.h>
#include <vector-class/vectormath_hyp.h>
#include <vector-class/vectormath_trig.h>

#include "benchmark/benchmark.h"
