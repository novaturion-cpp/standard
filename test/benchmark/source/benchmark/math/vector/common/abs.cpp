﻿#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark::Math::Common
{
	template<class T>
	void AbsStd(
		benchmark::State& state
	)
	{
		const auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(std::abs(value));
		}
	}

	template<class T>
	void AbsVc(
		benchmark::State& state
	)
	{
		const auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(abs(value));
		}
	}

	template<class T>
	void AbsDx(
		benchmark::State& state
	)
	{
		const auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(DirectX::XMVectorAbs(value.Simd));
		}
	}

	template<class T>
	void Abs(
		benchmark::State& state
	)
	{
		const auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(Std::Math::Abs<typename T::SimdType, typename T::ValueType>(value.Simd));
		}
	}

	#if ALL || MATH || MATH_VECTOR || MATH_VECTOR_COMMON || MATH_VECTOR_COMMON_ABS

	BENCHMARK(AbsStd<float32>);
	BENCHMARK(AbsStd<float64>);

	BENCHMARK(AbsVc<Vec4f>);
	BENCHMARK(AbsVc<Vec2d>);
	BENCHMARK(AbsVc<Vec4d>);

	BENCHMARK(AbsDx<Std::Math::Vector4>);

	BENCHMARK(Abs<Std::Math::Vector4>);
	BENCHMARK(Abs<Std::Math::Vector2Float64>);
	BENCHMARK(Abs<Std::Math::Vector4Float64>);

	#endif
}
