﻿#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark::Math::Common
{
	template<class T>
	T AddWrapper(
		const T argA,
		const T argB
	)
	{
		return argA + argB;
	}

	template<class T>
	void AddStd(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(AddWrapper(a, b));
		}
	}

	template<class T>
	void AddVc(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(AddWrapper(a, b));
		}
	}

	template<class T>
	void AddDx(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(DirectX::XMVectorAdd(a.Simd, b.Simd));
		}
	}

	template<class T>
	void Add(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(Std::Math::Add(a, b));
		}
	}

	#if ALL || MATH || MATH_VECTOR || MATH_VECTOR_COMMON || MATH_VECTOR_COMMON_ADD

	BENCHMARK(AddStd<float32>);
	BENCHMARK(AddStd<float64>);

	BENCHMARK(AddVc<Vec4f>);
	BENCHMARK(AddVc<Vec2d>);
	BENCHMARK(AddVc<Vec4d>);

	BENCHMARK(AddDx<Std::Math::Vector4>);

	BENCHMARK(Add<Std::Math::Vector4>);
	BENCHMARK(Add<Std::Math::Vector2Float64>);
	BENCHMARK(Add<Std::Math::Vector4Float64>);

	#endif
}
