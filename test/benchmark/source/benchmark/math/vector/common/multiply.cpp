#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark::Math::Common
{
	template<class T>
	T MultiplyWrapper(
		const T argA,
		const T argB
	)
	{
		return argA * argB;
	}

	template<class T>
	void MultiplyStd(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(MultiplyWrapper(a, b));
		}
	}

	template<class T>
	void MultiplyVc(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		const auto divide = [](
			const T argA,
			const T argB
		)
		{
			return argA / argB;
		};

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(MultiplyWrapper(a, b));
		}
	}

	template<class T>
	void MultiplyDx(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(DirectX::XMVectorMultiply(a.Simd, b.Simd));
		}
	}

	template<class T>
	void Multiply(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(MultiplyWrapper(a, b));
		}
	}

	#if ALL || MATH || MATH_VECTOR || MATH_VECTOR_COMMON || MATH_VECTOR_COMMON_MULTIPLY

	BENCHMARK(MultiplyStd<float32>);
	BENCHMARK(MultiplyStd<float64>);

	BENCHMARK(MultiplyVc<Vec4f>);
	BENCHMARK(MultiplyVc<Vec2d>);
	BENCHMARK(MultiplyVc<Vec4d>);

	BENCHMARK(MultiplyDx<Std::Math::Vector4>);

	BENCHMARK(Multiply<Std::Math::Vector4>);
	BENCHMARK(Multiply<Std::Math::Vector2Float64>);
	BENCHMARK(Multiply<Std::Math::Vector4Float64>);

	#endif
}
