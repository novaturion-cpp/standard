#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark::Math::Common
{
	template<class T>
	T SubtractWrapper(
		const T argA,
		const T argB
	)
	{
		return argA - argB;
	}

	template<class T>
	void SubtractStd(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(SubtractWrapper(a, b));
		}
	}

	template<class T>
	void SubtractVc(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		const auto divide = [](
			const T argA,
			const T argB
		)
		{
			return argA / argB;
		};

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(SubtractWrapper(a, b));
		}
	}

	template<class T>
	void SubtractDx(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(DirectX::XMVectorSubtract(a.Simd, b.Simd));
		}
	}

	template<class T>
	void Subtract(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(SubtractWrapper(a, b));
		}
	}

	#if ALL || MATH || MATH_VECTOR || MATH_VECTOR_COMMON || MATH_VECTOR_COMMON_SUBTRACT

	BENCHMARK(SubtractStd<float32>);
	BENCHMARK(SubtractStd<float64>);

	BENCHMARK(SubtractVc<Vec4f>);
	BENCHMARK(SubtractVc<Vec2d>);
	BENCHMARK(SubtractVc<Vec4d>);

	BENCHMARK(SubtractDx<Std::Math::Vector4>);

	BENCHMARK(Subtract<Std::Math::Vector4>);
	BENCHMARK(Subtract<Std::Math::Vector2Float64>);
	BENCHMARK(Subtract<Std::Math::Vector4Float64>);

	#endif
}
