#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark::Math::Common
{
	template<class T>
	T DivideWrapper(
		const T argA,
		const T argB
	)
	{
		return argA / argB;
	}

	template<class T>
	void DivideStd(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(DivideWrapper(a, b));
		}
	}

	template<class T>
	void DivideVc(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		const auto divide = [](
			const T argA,
			const T argB
		)
		{
			return argA / argB;
		};

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(DivideWrapper(a, b));
		}
	}

	template<class T>
	void DivideDx(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(DirectX::XMVectorDivide(a.Simd, b.Simd));
		}
	}

	template<class T>
	void Divide(
		benchmark::State& state
	)
	{
		const auto a = T();
		const auto b = T(30);

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(DivideWrapper(a, b));
		}
	}

	#if ALL || MATH || MATH_VECTOR || MATH_VECTOR_COMMON || MATH_VECTOR_COMMON_DIVIDE

	BENCHMARK(DivideStd<float32>);
	BENCHMARK(DivideStd<float64>);

	BENCHMARK(DivideVc<Vec4f>);
	BENCHMARK(DivideVc<Vec2d>);
	BENCHMARK(DivideVc<Vec4d>);

	BENCHMARK(DivideDx<Std::Math::Vector4>);

	BENCHMARK(Divide<Std::Math::Vector4>);
	BENCHMARK(Divide<Std::Math::Vector2Float64>);
	BENCHMARK(Divide<Std::Math::Vector4Float64>);

	#endif
}
