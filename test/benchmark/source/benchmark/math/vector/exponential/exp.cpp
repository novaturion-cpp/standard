﻿#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark
{
	using namespace Std;

	template<class T>
	void ExpStd(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(std::exp(value));
		}
	}

	template<class T>
	void ExpVc(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(exp(value));
		}
	}

	template<class T>
	void Exp(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(Math::Exp(value));
		}
	}

	template<class T>
	void ExpFast(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(Math::ExpFast(value));
		}
	}

	#if ALL || MATH || MATH_VECTOR || MATH_VECTOR_EXPONENTIAL_EXP

	BENCHMARK(ExpStd<float32>);
	BENCHMARK(ExpStd<float64>);

	BENCHMARK(ExpVc<Vec4f>);
	BENCHMARK(ExpVc<Vec2d>);
	BENCHMARK(ExpVc<Vec4d>);

	BENCHMARK(Exp<Math::Vector4>);
	BENCHMARK(Exp<Math::Vector2Float64>);
	BENCHMARK(Exp<Math::Vector4Float64>);

	BENCHMARK(ExpFast<Math::Vector4>);
	BENCHMARK(ExpFast<Math::Vector2Float64>);
	BENCHMARK(ExpFast<Math::Vector4Float64>);

	#endif
}
