﻿#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark
{
	using namespace Std;

	template<class T>
	void Pow2Std(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(std::exp2(value));
		}
	}

	template<class T>
	void Pow2Vc(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(exp2(value));
		}
	}

	template<class T>
	void Pow2(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(Math::Pow2(value));
		}
	}

	#if ALL || MATH || MATH_VECTOR || MATH_VECTOR_EXPONENTIAL_POW_2

	BENCHMARK(Pow2Std<float32>);
	BENCHMARK(Pow2Std<float64>);

	BENCHMARK(Pow2Vc<Vec4f>);
	BENCHMARK(Pow2Vc<Vec2d>);
	BENCHMARK(Pow2Vc<Vec4d>);

	BENCHMARK(Pow2<Math::Vector4>);
	BENCHMARK(Pow2<Math::Vector2Float64>);
	BENCHMARK(Pow2<Math::Vector4Float64>);

	#endif
}
