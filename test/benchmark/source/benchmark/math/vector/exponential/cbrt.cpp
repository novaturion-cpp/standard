﻿#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark
{
	using namespace Std;

	template<class T>
	void CbrtStd(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(std::cbrt(value));
		}
	}

	template<class T>
	void CbrtVc(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(cbrt(value));
		}
	}

	template<class T>
	void Cbrt(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(Math::Cbrt(value));
		}
	}

	template<class T>
	void CbrtN(
		benchmark::State& state
	)
	{
		constexpr auto value = T();

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(
				Math::CbrtN<typename T::SimdType, typename T::ValueType>(value.Simd)
			);
		}
	}

	#if ALL || MATH || MATH_VECTOR || MATH_VECTOR_EXPONENTIAL_CBRT

	BENCHMARK(CbrtStd<float32>);
	BENCHMARK(CbrtStd<float64>);

	BENCHMARK(CbrtVc<Vec4f>);
	BENCHMARK(CbrtVc<Vec2d>);
	BENCHMARK(CbrtVc<Vec4d>);

	BENCHMARK(Cbrt<Math::Vector4>);
	// BENCHMARK(Cbrt<Math::Vector2Float64>);
	// BENCHMARK(Cbrt<Math::Vector4Float64>);

	BENCHMARK(CbrtN<Math::Vector4>);
	// BENCHMARK(CbrtN<Math::Vector2Float64>);
	// BENCHMARK(CbrtN<Math::Vector4Float64>);

	#endif
}
