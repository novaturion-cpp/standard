﻿#include "precompiled-benchmark.hpp"
#include "benchmark/set.hpp"


namespace Benchmark::Platform::Cpu
{
	void MyX86(
		benchmark::State& state
	)
	{
		for (auto _ : state)
		{
			benchmark::DoNotOptimize(Std::Cpu::X86());
		}
	}

	void GoogleX86(
		benchmark::State& state
	)
	{
		for (auto _ : state)
		{
			benchmark::DoNotOptimize(cpu_features::GetX86Info());
			benchmark::DoNotOptimize(cpu_features::GetX86CacheInfo());
		}
	}

	void LibcpuX86(
		benchmark::State& state
	)
	{
		cpu_id_t libcpuX86;

		for (auto _ : state)
		{
			benchmark::DoNotOptimize(cpu_identify(null, &libcpuX86));
		}
	}

	#if ALL || PLATFORM || PLATFORM_CPU || PLATFORM_CPU_X86

	BENCHMARK(MyX86);
	BENCHMARK(GoogleX86);
	BENCHMARK(LibcpuX86);

	#endif
}
