﻿#pragma once
#include <standard/math/type.hpp>

#include "gtest/gtest.h"


namespace Test::Math::Type
{
	using Types = testing::Types<
		Std::Matrix2x2T<int8>,
		Std::Matrix2x2T<int16>,
		Std::Matrix2x2T<int32>,
		Std::Matrix2x2T<int64>,
		Std::Matrix2x2T<uint8>,
		Std::Matrix2x2T<uint16>,
		Std::Matrix2x2T<uint32>,
		Std::Matrix2x2T<uint64>,
		Std::Matrix2x2T<float32>,
		Std::Matrix2x2T<float64>,

		Std::Matrix3x3T<int8>,
		Std::Matrix3x3T<int16>,
		Std::Matrix3x3T<int32>,
		Std::Matrix3x3T<int64>,
		Std::Matrix3x3T<uint8>,
		Std::Matrix3x3T<uint16>,
		Std::Matrix3x3T<uint32>,
		Std::Matrix3x3T<uint64>,
		Std::Matrix3x3T<float32>,
		Std::Matrix3x3T<float64>,

		Std::Matrix4x4T<int8>,
		Std::Matrix4x4T<int16>,
		Std::Matrix4x4T<int32>,
		Std::Matrix4x4T<int64>,
		Std::Matrix4x4T<uint8>,
		Std::Matrix4x4T<uint16>,
		Std::Matrix4x4T<uint32>,
		Std::Matrix4x4T<uint64>,
		Std::Matrix4x4T<float32>,
		Std::Matrix4x4T<float64>
	>;
}
