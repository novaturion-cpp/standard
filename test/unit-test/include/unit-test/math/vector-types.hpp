﻿#pragma once
#include <standard/math/type.hpp>

#include "gtest/gtest.h"


namespace Test::Math::Type
{
	using Types = testing::Types<
		Std::Vector2T<int8>,
		Std::Vector2T<int16>,
		Std::Vector2T<int32>,
		Std::Vector2T<int64>,
		Std::Vector2T<uint8>,
		Std::Vector2T<uint16>,
		Std::Vector2T<uint32>,
		Std::Vector2T<uint64>,
		Std::Vector2T<float32>,
		Std::Vector2T<float64>,

		Std::Vector3T<int8>,
		Std::Vector3T<int16>,
		Std::Vector3T<int32>,
		Std::Vector3T<int64>,
		Std::Vector3T<uint8>,
		Std::Vector3T<uint16>,
		Std::Vector3T<uint32>,
		Std::Vector3T<uint64>,
		Std::Vector3T<float32>,
		Std::Vector3T<float64>,

		Std::Vector4T<int8>,
		Std::Vector4T<int16>,
		Std::Vector4T<int32>,
		Std::Vector4T<int64>,
		Std::Vector4T<uint8>,
		Std::Vector4T<uint16>,
		Std::Vector4T<uint32>,
		Std::Vector4T<uint64>,
		Std::Vector4T<float32>,
		Std::Vector4T<float64>
	>;
}
