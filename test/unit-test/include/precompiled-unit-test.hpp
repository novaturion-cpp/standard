#pragma once
#include <cmath>
#include <cpuinfo_x86.h>
#include <intrin.h>
#include <limits>
#include <utility>

#include <libcpuid/libcpuid.h>

#include <standard/math/type.hpp>

#include <standard/platform/cpu/x86.hpp>
#include <standard/platform/intrinsic/x86.hpp>

#include <standard/utility/common.hpp>
#include <standard/utility/numeric-limits.hpp>
#include <standard/utility/type-traits.hpp>

#include "gtest/gtest.h"
