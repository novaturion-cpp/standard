unitTestIncludeDir = os.getcwd() .. "/include/"

project ( path.getbasename(os.getcwd()) )
	kind "consoleapp"
	links {
		"google-test",
		"google-cpuid",
		"libcpuid",
		"standard"
	}

	pchheader ( precompiledHeader )
	pchsource ( precompiledSource )

	targetname ( binaryName )

	targetdir ( binaryDir )
	objdir ( intermediateDir )

	includedirs {
		includeDir,
		vectorClassIncludeDir,
		googleTestIncludeDir,
		googleCpuidIncludeDir,
		libcpuidIncludeDir,
		standardIncludeDir
	}
	files {
		projectFiles,
		inlineFiles,
		includeFiles,
		sourceFiles
	}

	vectorextensions ( "avx2" )
	
	filter { "action:vs*" }
		buildoptions { "/bigobj" }

	filter { }
