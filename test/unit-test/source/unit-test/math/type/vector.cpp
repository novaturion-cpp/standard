﻿#include "precompiled-unit-test.hpp"
#include <unit-test/math/vector-types.hpp>


namespace Test::Math::Type
{
	template<class T>
	class Vector : public testing::Test
	{
	public:
		using Type = T;
	};


	TYPED_TEST_SUITE(Vector, Types);

	TYPED_TEST(Vector, SimdType)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;
		using SimdType = typename VectorType::SimdType;

		constexpr auto valueSize = sizeof(ValueType);
		constexpr auto vectorSize = VectorType::Size * valueSize;

		if constexpr (VectorType::Size <= 2 || valueSize <= sizeof(int32))
		{
			EXPECT_TRUE(
				(Std::IsSameValue<SimdType, Std::Vector128>)
			);
		}
		else
		{
			EXPECT_TRUE(
				(Std::IsSameValue<SimdType, Std::Vector256>)
			);
		}
	}

	TYPED_TEST(Vector, OperatorAdd)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		auto value = static_cast<ValueType>(3);
		auto expected = VectorType(value);

		EXPECT_EQ(
			expected,
			VectorType() += value
		);
		EXPECT_EQ(
			expected,
			VectorType() += VectorType(value)
		);
		EXPECT_EQ(
			expected,
			VectorType() + value
		);
		EXPECT_EQ(
			expected,
			VectorType() + VectorType(value)
		);
	}

	TYPED_TEST(Vector, OperatorAnd)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_FALSE(
				VectorType() && VectorType(1)
			);
			EXPECT_TRUE(
				VectorType(1) && VectorType(2)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Vector, OperatorAssign)
	{
		using VectorType = typename TestFixture::Type;

		auto a = VectorType(1);
		auto b = VectorType(2);

		auto temp = a;
		a = b;
		b = temp;

		EXPECT_EQ(
			VectorType(1),
			b
		);
		EXPECT_EQ(
			VectorType(2),
			a
		);
	}

	TYPED_TEST(Vector, OperatorBitAnd)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				VectorType(1),
				VectorType(1) &= 3
			);
			EXPECT_EQ(
				VectorType(1),
				VectorType(1) &= VectorType(3)
			);
			EXPECT_EQ(
				VectorType(1),
				VectorType(1) & 3
			);
			EXPECT_EQ(
				VectorType(1),
				VectorType(1) & VectorType(3)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Vector, OperatorBitNot)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;
		using SimdType = typename VectorType::SimdType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				VectorType(SimdType(static_cast<uint32>(-1))),
				~VectorType()
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Vector, OperatorBitOr)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				VectorType(3),
				VectorType(1) |= 2
			);
			EXPECT_EQ(
				VectorType(3),
				VectorType(1) |= VectorType(2)
			);
			EXPECT_EQ(
				VectorType(3),
				VectorType(1) | 2
			);
			EXPECT_EQ(
				VectorType(3),
				VectorType(1) | VectorType(2)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Vector, OperatorBitXor)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				VectorType(2),
				VectorType(1) ^= 3
			);
			EXPECT_EQ(
				VectorType(2),
				VectorType(1) ^= VectorType(3)
			);
			EXPECT_EQ(
				VectorType(2),
				VectorType(1) ^ 3
			);
			EXPECT_EQ(
				VectorType(2),
				VectorType(1) ^ VectorType(3)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Vector, OperatorDecrement)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		auto a = VectorType(3);

		EXPECT_EQ(
			VectorType(2),
			--a
		);
		EXPECT_EQ(
			VectorType(2),
			a--
		);
		EXPECT_EQ(
			VectorType(1),
			a
		);
	}

	TYPED_TEST(Vector, OperatorDivide)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		auto operand = static_cast<ValueType>(2);
		auto result = VectorType(3);

		if constexpr (Std::IsSameValue<ValueType, float32> && VectorType::Size < 4)
		{
			for (auto i = 0; i < VectorType::Size; ++i)
			{
				EXPECT_FLOAT_EQ(
					result[i],
					(VectorType(6) / operand)[i]
				);
				EXPECT_FLOAT_EQ(
					result[i],
					(VectorType(6) / VectorType(operand))[i]
				);
			}
		}
		else if constexpr (Std::IsSameValue<ValueType, float64> && VectorType::Size < 4)
		{
			for (auto i = 0; i < VectorType::Size; ++i)
			{
				EXPECT_DOUBLE_EQ(
					result[i],
					(VectorType(6) / operand)[i]
				);
				EXPECT_DOUBLE_EQ(
					result[i],
					(VectorType(6) / VectorType(operand))[i]
				);
			}
		}
		else
		{
			EXPECT_EQ(
				result,
				VectorType(6) /= operand
			);
			EXPECT_EQ(
				result,
				VectorType(6) /= VectorType(operand)
			);
			EXPECT_EQ(
				result,
				VectorType(6) / operand
			);
			EXPECT_EQ(
				result,
				VectorType(6) / VectorType(operand)
			);
		}
	}

	TYPED_TEST(Vector, OperatorEqual)
	{
		using VectorType = typename TestFixture::Type;

		EXPECT_TRUE(
			VectorType() == VectorType()
		);
		EXPECT_FALSE(
			VectorType() == VectorType(1)
		);
	}

	TYPED_TEST(Vector, OperatorIncrement)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		auto a = VectorType(1);

		EXPECT_EQ(
			VectorType(2),
			++a
		);
		EXPECT_EQ(
			VectorType(2),
			a++
		);
		EXPECT_EQ(
			VectorType(3),
			a
		);
	}

	TYPED_TEST(Vector, OperatorMinus)
	{
		using VectorType = typename TestFixture::Type;

		EXPECT_EQ(
			VectorType(-1),
			-VectorType(1)
		);
	}

	TYPED_TEST(Vector, OperatorModulo)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		auto operand = static_cast<ValueType>(4);
		auto result = VectorType(2);

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				result,
				VectorType(6) %= operand
			);
			EXPECT_EQ(
				result,
				VectorType(6) %= VectorType(operand)
			);
			EXPECT_EQ(
				result,
				VectorType(6) % operand
			);
			EXPECT_EQ(
				result,
				VectorType(6) % VectorType(operand)
			);
		}
	}

	TYPED_TEST(Vector, OperatorMultiply)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		auto operand = static_cast<ValueType>(2);
		auto result = VectorType(6);

		EXPECT_EQ(
			result,
			VectorType(3) *= operand
		);
		EXPECT_EQ(
			result,
			VectorType(3) *= VectorType(operand)
		);
		EXPECT_EQ(
			result,
			VectorType(3) * operand
		);
		EXPECT_EQ(
			result,
			VectorType(3) * VectorType(operand)
		);
	}

	TYPED_TEST(Vector, OperatorNotEqual)
	{
		using VectorType = typename TestFixture::Type;

		EXPECT_TRUE(
			VectorType() != VectorType(1)
		);
		EXPECT_FALSE(
			VectorType() != VectorType()
		);
	}

	TYPED_TEST(Vector, OperatorOr)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_FALSE(
				VectorType() || VectorType()
			);
			EXPECT_TRUE(
				VectorType() || VectorType(1)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Vector, OperatorPlus)
	{
		using VectorType = typename TestFixture::Type;

		EXPECT_EQ(
			VectorType(1),
			+VectorType(1)
		);
		EXPECT_EQ(
			VectorType(-1),
			+VectorType(-1)
		);
	}

	TYPED_TEST(Vector, OperatorShiftLeft)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				VectorType(2),
				VectorType(1) << 1
			);
			EXPECT_EQ(
				VectorType(-1 << 1),
				VectorType(-1) << 1
			);
			EXPECT_EQ(
				VectorType(2),
				VectorType(1) << VectorType(1)
			);
			EXPECT_EQ(
				VectorType(-1 << 1),
				VectorType(-1) << VectorType(1)
			);
		}
	}

	TYPED_TEST(Vector, OperatorShiftRight)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				VectorType(1),
				VectorType(2) >> 1
			);
			EXPECT_EQ(
				VectorType(static_cast<ValueType>(-2) >> 1),
				VectorType(-2) >> 1
			);
			EXPECT_EQ(
				VectorType(1),
				VectorType(2) >> VectorType(1)
			);
			EXPECT_EQ(
				VectorType(static_cast<ValueType>(-2) >> 1),
				VectorType(-2) >> VectorType(1)
			);
		}
	}

	TYPED_TEST(Vector, OperatorSubscript)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		auto vector = VectorType();

		for (auto i = 0; i < VectorType::Size; ++i)
		{
			vector[i] = static_cast<ValueType>(i);
		}

		for (auto i = 0; i < VectorType::Size; ++i)
		{
			if constexpr (Std::IsFloatValue<ValueType>)
			{
				if constexpr (sizeof(ValueType) == sizeof(float32))
				{
					EXPECT_FLOAT_EQ(
						static_cast<ValueType>(i),
						vector[i]
					);
				}
				else
				{
					EXPECT_DOUBLE_EQ(
						static_cast<ValueType>(i),
						vector[i]
					);
				}
			}
			else
			{
				EXPECT_EQ(
					i,
					vector[i]
				);
			}
		}
	}

	TYPED_TEST(Vector, OperatorSubtract)
	{
		using VectorType = typename TestFixture::Type;
		using ValueType = typename VectorType::ValueType;

		auto operand = static_cast<ValueType>(3);
		auto result = VectorType(3);

		EXPECT_EQ(
			result,
			VectorType(6) -= operand
		);
		EXPECT_EQ(
			result,
			VectorType(6) -= VectorType(operand)
		);
		EXPECT_EQ(
			result,
			VectorType(6) - operand
		);
		EXPECT_EQ(
			result,
			VectorType(6) - VectorType(operand)
		);
	}
}
