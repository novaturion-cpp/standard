﻿#include "precompiled-unit-test.hpp"
#include <unit-test/math/matrix-types.hpp>


namespace Test::Math::Type
{
	template<class T>
	class Matrix : public testing::Test
	{
	public:
		using Type = T;
	};


	TYPED_TEST_SUITE(Matrix, Types);

	TYPED_TEST(Matrix, OperatorAdd)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto operand = static_cast<ValueType>(3);
		auto result = MatrixType(operand);

		EXPECT_EQ(
			result,
			MatrixType() += operand
		);
		EXPECT_EQ(
			result,
			MatrixType() += MatrixType(operand)
		);
		EXPECT_EQ(
			result,
			MatrixType() + operand
		);
		EXPECT_EQ(
			result,
			MatrixType() + MatrixType(operand)
		);
	}

	TYPED_TEST(Matrix, OperatorAnd)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_FALSE(
				MatrixType() && MatrixType(1)
			);
			EXPECT_TRUE(
				MatrixType(1) && MatrixType(2)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Matrix, OperatorAssign)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto a = MatrixType(1);
		auto b = MatrixType(2);

		auto temp = a;
		a = b;
		b = temp;

		EXPECT_EQ(
			MatrixType(1),
			b
		);
		EXPECT_EQ(
			MatrixType(2),
			a
		);
	}

	TYPED_TEST(Matrix, OperatorBitAnd)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				MatrixType(1),
				MatrixType(1) &= 3
			);
			EXPECT_EQ(
				MatrixType(1),
				MatrixType(1) &= MatrixType(3)
			);
			EXPECT_EQ(
				MatrixType(1),
				MatrixType(1) & 3
			);
			EXPECT_EQ(
				MatrixType(1),
				MatrixType(1) & MatrixType(3)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Matrix, OperatorBitNot)
	{
		using MatrixType = typename TestFixture::Type;
		using RowType = typename MatrixType::RowType;
		using ValueType = typename MatrixType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				MatrixType(RowType(RowType::SimdType(static_cast<uint32>(-1)))),
				~MatrixType()
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Matrix, OperatorBitOr)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				MatrixType(3),
				MatrixType(1) |= 2
			);
			EXPECT_EQ(
				MatrixType(3),
				MatrixType(1) |= MatrixType(2)
			);
			EXPECT_EQ(
				MatrixType(3),
				MatrixType(1) | 2
			);
			EXPECT_EQ(
				MatrixType(3),
				MatrixType(1) | MatrixType(2)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Matrix, OperatorBitXor)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				MatrixType(2),
				MatrixType(1) ^= 3
			);
			EXPECT_EQ(
				MatrixType(2),
				MatrixType(1) ^= MatrixType(3)
			);
			EXPECT_EQ(
				MatrixType(2),
				MatrixType(1) ^ 3
			);
			EXPECT_EQ(
				MatrixType(2),
				MatrixType(1) ^ MatrixType(3)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Matrix, OperatorDecrement)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto a = MatrixType(3);

		EXPECT_EQ(
			MatrixType(2),
			--a
		);
		EXPECT_EQ(
			MatrixType(2),
			a--
		);
		EXPECT_EQ(
			MatrixType(1),
			a
		);
	}

	TYPED_TEST(Matrix, OperatorDivide)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto operand = static_cast<ValueType>(2);
		auto result = MatrixType(3);

		if constexpr (Std::IsSameValue<ValueType, float32> && MatrixType::RowsCount < 4)
		{
			for (auto i = 0; i < MatrixType::RowsCount; ++i)
			{
				for (auto j = 0; j < MatrixType::ColumnsCount; ++j)
				{
					EXPECT_FLOAT_EQ(
						result[i][j],
						(MatrixType(6) / operand)[i][j]
					);
					EXPECT_FLOAT_EQ(
						result[i][j],
						(MatrixType(6) / MatrixType(operand))[i][j]
					);
				}
			}
		}
		else if constexpr (Std::IsSameValue<ValueType, float64> && MatrixType::RowsCount < 4)
		{
			for (auto i = 0; i < MatrixType::RowsCount; ++i)
			{
				for (auto j = 0; j < MatrixType::ColumnsCount; ++j)
				{
					EXPECT_DOUBLE_EQ(
						result[i][j],
						(MatrixType(6) / operand)[i][j]
					);
					EXPECT_DOUBLE_EQ(
						result[i][j],
						(MatrixType(6) / MatrixType(operand))[i][j]
					);
				}
			}
		}
		else
		{
			EXPECT_EQ(
				result,
				MatrixType(6) /= operand
			);
			EXPECT_EQ(
				result,
				MatrixType(6) /= MatrixType(operand)
			);
			EXPECT_EQ(
				result,
				MatrixType(6) / operand
			);
			EXPECT_EQ(
				result,
				MatrixType(6) / MatrixType(operand)
			);
		}
	}

	TYPED_TEST(Matrix, OperatorEqual)
	{
		using MatrixType = typename TestFixture::Type;

		EXPECT_TRUE(
			MatrixType() == MatrixType()
		);
		EXPECT_FALSE(
			MatrixType() == MatrixType(1)
		);
	}

	TYPED_TEST(Matrix, OperatorIdentity)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		constexpr auto elementsCount = MatrixType::ElementsCount;

		ValueType array[elementsCount] = {};

		for (auto i = 0; i < elementsCount; i += MatrixType::ColumnsCount + 1)
		{
			array[i] = static_cast<ValueType>(1);
		}

		auto result = MatrixType(array);
		auto identity = MatrixType::GetIdentity();

		for (auto i = 0; i < MatrixType::RowsCount; ++i)
		{
			EXPECT_EQ(
				identity[i],
				result[i]
			);
		}
	}

	TYPED_TEST(Matrix, OperatorIncrement)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto a = MatrixType(1);

		EXPECT_EQ(
			MatrixType(2),
			++a
		);
		EXPECT_EQ(
			MatrixType(2),
			a++
		);
		EXPECT_EQ(
			MatrixType(3),
			a
		);
	}

	TYPED_TEST(Matrix, OperatorMinus)
	{
		using MatrixType = typename TestFixture::Type;

		EXPECT_EQ(
			MatrixType(-1),
			-MatrixType(1)
		);
	}

	TYPED_TEST(Matrix, OperatorModulo)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto operand = static_cast<ValueType>(4);
		auto result = MatrixType(2);

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				result,
				MatrixType(6) %= operand
			);
			EXPECT_EQ(
				result,
				MatrixType(6) %= MatrixType(operand)
			);
			EXPECT_EQ(
				result,
				MatrixType(6) % operand
			);
			EXPECT_EQ(
				result,
				MatrixType(6) % MatrixType(operand)
			);
		}
	}

	TYPED_TEST(Matrix, OperatorMultiply)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto operand = static_cast<ValueType>(2);
		auto result = MatrixType(6);

		EXPECT_EQ(
			result,
			MatrixType(3) *= operand
		);
		EXPECT_EQ(
			result,
			MatrixType(3) *= MatrixType(operand)
		);
		EXPECT_EQ(
			result,
			MatrixType(3) * operand
		);
		EXPECT_EQ(
			result,
			MatrixType(3) * MatrixType(operand)
		);
	}

	TYPED_TEST(Matrix, OperatorNotEqual)
	{
		using MatrixType = typename TestFixture::Type;

		EXPECT_TRUE(
			MatrixType() != MatrixType(1)
		);
		EXPECT_FALSE(
			MatrixType() != MatrixType()
		);
	}

	TYPED_TEST(Matrix, OperatorOr)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_FALSE(
				MatrixType() || MatrixType()
			);
			EXPECT_TRUE(
				MatrixType() || MatrixType(1)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(Matrix, OperatorPlus)
	{
		using MatrixType = typename TestFixture::Type;

		EXPECT_EQ(
			MatrixType(1),
			+MatrixType(1)
		);
		EXPECT_EQ(
			MatrixType(-1),
			+MatrixType(-1)
		);
	}

	TYPED_TEST(Matrix, OperatorShiftLeft)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				MatrixType(2),
				MatrixType(1) << 1
			);
			EXPECT_EQ(
				MatrixType(-1 << 1),
				MatrixType(-1) << 1
			);
			EXPECT_EQ(
				MatrixType(2),
				MatrixType(1) << MatrixType(1)
			);
			EXPECT_EQ(
				MatrixType(-1 << 1),
				MatrixType(-1) << MatrixType(1)
			);
		}
	}

	TYPED_TEST(Matrix, OperatorShiftRight)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		if constexpr (!Std::IsFloatValue<ValueType>)
		{
			EXPECT_EQ(
				MatrixType(1),
				MatrixType(2) >> 1
			);
			EXPECT_EQ(
				MatrixType(static_cast<ValueType>(-2) >> 1),
				MatrixType(-2) >> 1
			);
			EXPECT_EQ(
				MatrixType(1),
				MatrixType(2) >> MatrixType(1)
			);
			EXPECT_EQ(
				MatrixType(static_cast<ValueType>(-2) >> 1),
				MatrixType(-2) >> MatrixType(1)
			);
		}
	}

	TYPED_TEST(Matrix, OperatorSubscript)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto result = MatrixType();

		for (auto i = 0; i < MatrixType::ElementsCount; ++i)
		{
			result[i / MatrixType::RowsCount][i % MatrixType::ColumnsCount] = static_cast<ValueType>(i);
		}

		for (auto i = 0; i < MatrixType::ElementsCount; ++i)
		{
			if constexpr (Std::IsFloatValue<ValueType>)
			{
				if constexpr (sizeof(ValueType) == sizeof(float32))
				{
					EXPECT_FLOAT_EQ(
						static_cast<ValueType>(i),
						result[i / MatrixType::RowsCount][i % MatrixType::ColumnsCount]
					);
				}
				else
				{
					EXPECT_DOUBLE_EQ(
						static_cast<ValueType>(i),
						result[i / MatrixType::RowsCount][i % MatrixType::ColumnsCount]
					);
				}
			}
			else
			{
				EXPECT_EQ(
					i,
					result[i / MatrixType::RowsCount][i % MatrixType::ColumnsCount]
				);
			}
		}
	}

	TYPED_TEST(Matrix, OperatorSubtract)
	{
		using MatrixType = typename TestFixture::Type;
		using ValueType = typename MatrixType::ValueType;

		auto operand = static_cast<ValueType>(3);
		auto result = MatrixType(3);

		EXPECT_EQ(
			result,
			MatrixType(6) -= operand
		);
		EXPECT_EQ(
			result,
			MatrixType(6) -= MatrixType(operand)
		);
		EXPECT_EQ(
			result,
			MatrixType(6) - operand
		);
		EXPECT_EQ(
			result,
			MatrixType(6) - MatrixType(operand)
		);
	}
}
