﻿#include "precompiled-unit-test.hpp"


namespace Test::Utility
{
	using namespace std;

	template<class T>
	using MyNumericLimits = Std::NumericLimits<T>;

	using Types = testing::Types<
		int8,
		int16,
		int32,
		int64,
		uint8,
		uint16,
		uint32,
		uint64,
		float32,
		float64,
		float128
	>;


	template<class T>
	class NumericLimits : public testing::Test
	{
	public:
		using Type = T;
	};


	TYPED_TEST_SUITE(NumericLimits, Types);


	TYPED_TEST(NumericLimits, Digits)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::digits10,
			MyNumericLimits<T>::Digits
		);
	}

	TYPED_TEST(NumericLimits, FloatDenormStyle)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			constexpr auto style = MyNumericLimits<T>::DenormStyle;

			EXPECT_EQ(
				numeric_limits<T>::has_denorm,
				static_cast<int32>(style)
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatDigitsMax)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				numeric_limits<T>::max_digits10,
				MyNumericLimits<T>::DigitsMax
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatEpsilon)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				numeric_limits<T>::epsilon(),
				MyNumericLimits<T>::Epsilon
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatExponentMax)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				numeric_limits<T>::max_exponent,
				MyNumericLimits<T>::ExponentMax
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatExponentMin)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				1 - (1 << (MyNumericLimits<T>::ExponentSize - 1)),
				MyNumericLimits<T>::ExponentMin
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatExponentRealMax)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				(1 << (MyNumericLimits<T>::ExponentSize - 1)) - 1,
				MyNumericLimits<T>::ExponentRealMax
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatExponentRealMin)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				2 - (1 << (MyNumericLimits<T>::ExponentSize - 1)),
				MyNumericLimits<T>::ExponentRealMin
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatExponentSize)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			if constexpr (is_same_v<T, float32>)
			{
				EXPECT_EQ(
					sizeof(T) * 8 - FLT_MANT_DIG,
					MyNumericLimits<T>::ExponentSize
				);
			}
			else
			{
				EXPECT_EQ(
					sizeof(T) * 8 - DBL_MANT_DIG,
					MyNumericLimits<T>::ExponentSize
				);
			}
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatHasDenormLoss)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				numeric_limits<T>::has_denorm_loss,
				MyNumericLimits<T>::HasDenormLoss
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatHasInfinity)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				numeric_limits<T>::has_infinity,
				MyNumericLimits<T>::HasInfinity
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatHasNegativeInfinity)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				numeric_limits<T>::has_infinity,
				MyNumericLimits<T>::HasNegativeInfinity
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatIsIee754)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				numeric_limits<T>::is_iec559,
				MyNumericLimits<T>::IsIee754
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatMantissaRealSize)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			if constexpr (is_same_v<T, float32>)
			{
				EXPECT_EQ(
					FLT_MANT_DIG - 1,
					MyNumericLimits<T>::MantissaRealSize
				);
			}
			else
			{
				EXPECT_EQ(
					DBL_MANT_DIG - 1,
					MyNumericLimits<T>::MantissaRealSize
				);
			}
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatMantissaSize)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			if constexpr (is_same_v<T, float32>)
			{
				EXPECT_EQ(
					FLT_MANT_DIG,
					MyNumericLimits<T>::MantissaSize
				);
			}
			else
			{
				EXPECT_EQ(
					DBL_MANT_DIG,
					MyNumericLimits<T>::MantissaSize
				);
			}
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, FloatRealMin)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			EXPECT_EQ(
				numeric_limits<T>::denorm_min(),
				MyNumericLimits<T>::RealMin
			);
		}
		else
		{
			GTEST_SKIP();
		}
	}

	TYPED_TEST(NumericLimits, IsBounded)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::is_bounded,
			MyNumericLimits<T>::IsBounded
		);
	}

	TYPED_TEST(NumericLimits, IsExactRepresentation)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::is_exact,
			MyNumericLimits<T>::IsExactRepresentation
		);
	}

	TYPED_TEST(NumericLimits, IsFloat)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			!numeric_limits<T>::is_integer,
			MyNumericLimits<T>::IsFloat
		);
	}

	TYPED_TEST(NumericLimits, IsHandleOverflow)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::is_modulo,
			MyNumericLimits<T>::IsHandleOverflow
		);
	}

	TYPED_TEST(NumericLimits, IsInteger)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::is_integer,
			MyNumericLimits<T>::IsInteger
		);
	}

	TYPED_TEST(NumericLimits, IsSigned)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::is_signed,
			MyNumericLimits<T>::IsSigned
		);
	}

	TYPED_TEST(NumericLimits, IsSpecialized)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::is_specialized,
			MyNumericLimits<T>::IsSpecialized
		);
	}

	TYPED_TEST(NumericLimits, IsTraps)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::traps,
			MyNumericLimits<T>::IsTraps
		);
	}

	TYPED_TEST(NumericLimits, Max)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			if constexpr (is_same_v<T, float32>)
			{
				EXPECT_FLOAT_EQ(
					numeric_limits<T>::max(),
					MyNumericLimits<T>::Max
				);
			}
			else
			{
				EXPECT_DOUBLE_EQ(
					numeric_limits<T>::max(),
					MyNumericLimits<T>::Max
				);
			}
		}
		else
		{
			EXPECT_EQ(
				numeric_limits<T>::max(),
				MyNumericLimits<T>::Max
			);
		}
	}

	TYPED_TEST(NumericLimits, Min)
	{
		using T = typename TestFixture::Type;

		if constexpr (is_floating_point_v<T>)
		{
			if constexpr (is_same_v<T, float32>)
			{
				EXPECT_FLOAT_EQ(
					numeric_limits<T>::min(),
					MyNumericLimits<T>::Min
				);
			}
			else
			{
				EXPECT_DOUBLE_EQ(
					numeric_limits<T>::min(),
					MyNumericLimits<T>::Min
				);
			}
		}
		else
		{
			EXPECT_EQ(
				numeric_limits<T>::min(),
				MyNumericLimits<T>::Min
			);
		}
	}

	TYPED_TEST(NumericLimits, Radix)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			numeric_limits<T>::radix,
			MyNumericLimits<T>::Radix
		);
	}

	TYPED_TEST(NumericLimits, RealSize)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			sizeof(T) * 8 - is_signed_v<T>,
			MyNumericLimits<T>::RealSize
		);
	}

	TYPED_TEST(NumericLimits, Size)
	{
		using T = typename TestFixture::Type;

		EXPECT_EQ(
			sizeof(T) * 8,
			MyNumericLimits<T>::Size
		);
	}
}
