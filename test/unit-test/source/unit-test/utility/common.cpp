﻿#include "precompiled-unit-test.hpp"


namespace Test::Utility
{
	using namespace std;
	using namespace Std::Utility;
	using namespace Std::TypeTrait;


	struct Forwarding
	{
		int32 F = 0;

		Forwarding() = default;

		Forwarding(
			int32& value
		):
			F(value) {}


		Forwarding(
			int32&& value
		):
			F(value) {}
	};


	struct Forwarder
	{
		Forwarding ForwardedA = Forwarding();
		Forwarding ForwardedB = Forwarding();

		template<class TA, class TB>
		Forwarder(
			bool isStlForward,
			TA&& value1,
			TB&& value2
		):
			ForwardedA(
				isStlForward
				? Forward<TA>(value1)
				: forward<TA>(value1)
			),
			ForwardedB(
				isStlForward
				? Forward<TB>(value2)
				: forward<TB>(value2)
			) {}
	};


	TEST(Common, AsConst)
	{
		auto value = 1;
		const auto& lvalue = value;

		EXPECT_TRUE(
			(IsSameValue<
				decltype(as_const(value)),
				decltype(AsConst(value))
				>)
		);
		EXPECT_TRUE(
			(IsSameValue<
				decltype(as_const(value)),
				decltype(AsConst(lvalue))
				>)
		);
	}

	TEST(Common, DeclareLValue)
	{
		EXPECT_TRUE(
			(IsSameValue<
				int32&,
				decltype(DeclareLValue<int32>())
				>)
		);
	}

	TEST(Common, DeclareRValue)
	{
		EXPECT_TRUE(
			(IsSameValue<
				int32&&,
				decltype(DeclareRValue<int32>())
				>)
		);
	}

	TEST(Common, DeclareValue)
	{
		EXPECT_TRUE(
			(IsSameValue<
				int32,
				decltype(DeclareValue<int32>())
				>)
		);
	}

	TEST(Common, EnumAsUnderlyingType)
	{
		enum EA : uint16
		{
			Fa
		};

		enum class EB : uint32
		{
			Fa = 2,
			Fb
		};

		EXPECT_EQ(
			static_cast<EnumUnderlyingType<EA>>(EA::Fa),
			EnumAsUnderlyingType(EA::Fa)
		);
		EXPECT_EQ(
			static_cast<EnumUnderlyingType<EB>>(EB::Fb),
			EnumAsUnderlyingType(EB::Fb)
		);
	}

	TEST(Common, Exchange)
	{
		struct A
		{
			int32 F = -1;
		};

		struct B
		{
			int32 F = -1;

			B() = default;

			B(
				const B& other
			) = delete;

			B(
				B&& other
			) = default;

			B& operator=(
				const B& other
			) = delete;

			B& operator=(
				B&& other
			) = default;
		};

		auto a = A();
		auto b = B();

		constexpr auto fieldValue = 1;

		a.F = fieldValue;
		b.F = fieldValue;

		const auto exchangedA = Exchange(a, A());
		const auto exchangedB = Exchange(b, B());

		EXPECT_EQ(
			A().F,
			a.F
		);
		EXPECT_EQ(
			fieldValue,
			exchangedA.F
		);
		EXPECT_EQ(
			B().F,
			b.F
		);
		EXPECT_EQ(
			fieldValue,
			exchangedB.F
		);
	}

	TEST(Common, Forward)
	{
		auto lvalue = 1;

		const auto forwarder = Forwarder(
			false,
			lvalue,
			2
		);

		const auto sForwarder = Forwarder(
			true,
			lvalue,
			2
		);

		EXPECT_EQ(
			forwarder.ForwardedA.F,
			sForwarder.ForwardedA.F
		);
		EXPECT_EQ(
			forwarder.ForwardedB.F,
			sForwarder.ForwardedB.F
		);
	}

	TEST(Common, IsConstantEvaluated)
	{
		#pragma warning(push)
		#pragma warning(disable: 5063)

		constexpr auto constantEvaluated = is_constant_evaluated();
		constexpr auto myConstantEvaluated = IsConstantEvaluated();

		#pragma warning(pop)

		EXPECT_EQ(
			constantEvaluated,
			myConstantEvaluated
		);
		EXPECT_EQ(
			is_constant_evaluated(),
			IsConstantEvaluated()
		);
	}

	TEST(Common, Move)
	{
		struct A
		{
			string F;
		};

		struct B
		{
			string F;

			B() = default;

			B(
				const B& other
			) = delete;

			B(
				B&& other
			) = default;

			B& operator=(
				const B& other
			) = delete;

			B& operator=(
				B&& other
			) = default;
		};

		struct C
		{
			string F;

			C() = default;

			C(
				const C& other
			) = default;

			C(
				C&& other
			) = delete;

			C& operator=(
				const C& other
			) = default;

			C& operator=(
				C&& other
			) = delete;
		};

		auto lvalueA = A();
		auto lvalueB = B();
		auto lvalueC = C();

		EXPECT_TRUE(
			(IsSameValue<
				decltype(move(lvalueA)),
				decltype(Move(lvalueA))
				>)
		);
		EXPECT_TRUE(
			(IsSameValue<
				decltype(move(lvalueB)),
				decltype(Move(lvalueB))
				>)
		);
		EXPECT_TRUE(
			(IsSameValue<
				decltype(move(lvalueC)),
				decltype(Move(lvalueC))
				>)
		);
	}

	TEST(Common, MoveIfNoexcept)
	{
		struct A
		{
			string F;
		};

		struct B
		{
			string F;

			B() = default;

			B(
				const B& other
			) = delete;

			B(
				B&& other
			) noexcept = default;

			B& operator=(
				const B& other
			) = delete;

			B& operator=(
				B&& other
			) noexcept = default;
		};

		struct C
		{
			string F;

			C() = default;

			C(
				const C& other
			) noexcept = default;

			C(
				C&& other
			) = delete;

			C& operator=(
				const C& other
			) noexcept = default;

			C& operator=(
				C&& other
			) = delete;
		};

		auto lvalueA = A();
		auto lvalueB = B();
		auto lvalueC = C();

		EXPECT_TRUE(
			(IsSameValue<
				decltype(move_if_noexcept(lvalueA)),
				decltype(MoveIfNoexcept(lvalueA))
				>)
		);
		EXPECT_TRUE(
			(IsSameValue<
				decltype(move_if_noexcept(lvalueB)),
				decltype(MoveIfNoexcept(lvalueB))
				>)
		);
		EXPECT_TRUE(
			(IsSameValue<
				decltype(move_if_noexcept(lvalueC)),
				decltype(MoveIfNoexcept(lvalueC))
				>)
		);
	}

	TEST(Common, Swap)
	{
		struct A
		{
			int32 F = 0;
		};

		struct B
		{
			int32 F = 0;

			B() = default;

			B(
				const B& other
			) = delete;

			B(
				B&& other
			) = default;

			B& operator=(
				const B& other
			) = delete;

			B& operator=(
				B&& other
			) = default;
		};

		struct C
		{
			int32 F = 0;

			C() = default;

			C(
				const C& other
			) = default;

			C(
				C&& other
			) = delete;

			C& operator=(
				const C& other
			) = default;

			C& operator=(
				C&& other
			) = delete;
		};

		constexpr auto valueLeft = 0;
		constexpr auto valueRight = 1;

		int32 leftArray[2] = {valueLeft, valueLeft};
		int32 rightArray[2] = {valueRight, valueRight};

		auto leftA = A();
		auto rightA = A();

		auto leftB = B();
		auto rightB = B();

		leftA.F = valueLeft;
		rightA.F = valueRight;

		leftB.F = valueLeft;
		rightB.F = valueRight;

		Swap(leftArray, rightArray);
		Swap(leftA, rightA);
		Swap(leftB, rightB);

		EXPECT_TRUE(
			leftArray[0] == valueRight && leftArray[1] == valueRight &&
			rightArray[0] == valueLeft && rightArray[1] == valueLeft
		);
		EXPECT_TRUE(
			leftA.F == valueRight && rightA.F == valueLeft
		);
		EXPECT_TRUE(
			leftB.F == valueRight && rightB.F == valueLeft
		);
	}

	TEST(Common, SwapIterators)
	{
		constexpr int32 array[2] = {5, 10};
		int32 swappedArray[2] = {5, 10};

		const auto left = swappedArray;
		const auto right = swappedArray + 1;

		SwapPointers(left, right);

		EXPECT_TRUE(
			*left == array[1] && *right == array[0]
		);
	}
}
