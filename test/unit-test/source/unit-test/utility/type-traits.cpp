﻿#include "precompiled-unit-test.hpp"


namespace Test::Utility
{
	using namespace std;
	using namespace Std::Utility::Type;
	using namespace Std::Utility::TypeTrait;

	TEST(TypeTraits, AddConst)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<int32>,
				AddConst<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<const int32>,
				AddConst<const int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_const_t<int32&>,
				AddConst<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<const int32&>,
				AddConst<const int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_const_t<int32*>,
				AddConst<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<const int32*>,
				AddConst<const int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<int32* const>,
				AddConst<int32* const>
				>)
		);
	}

	TEST(TypeTraits, AddConstPointer)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<add_pointer_t<int32>>,
				AddConstPointer<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<add_pointer_t<const int32>>,
				AddConstPointer<const int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_const_t<add_pointer_t<int32&>>,
				AddConstPointer<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<add_pointer_t<const int32&>>,
				AddConstPointer<const int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_const_t<add_pointer_t<const int32*>>,
				AddConstPointer<const int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_const_t<add_pointer_t<const int32* const>>,
				AddConstPointer<const int32* const>
				>)
		);
	}

	TEST(TypeTraits, AddConstVolatile)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<int32>,
				AddConstVolatile<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<const int32>,
				AddConstVolatile<const int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<volatile int32>,
				AddConstVolatile<volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<const volatile int32>,
				AddConstVolatile<const volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<int32&>,
				AddConstVolatile<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<const int32&>,
				AddConstVolatile<const int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<volatile int32&>,
				AddConstVolatile<volatile int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<const volatile int32&>,
				AddConstVolatile<const volatile int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<int32*>,
				AddConstVolatile<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<const int32*>,
				AddConstVolatile<const int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<volatile int32*>,
				AddConstVolatile<volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<const volatile int32*>,
				AddConstVolatile<const volatile int32*>
				>)
		);
	}

	TEST(TypeTraits, AddConstVolatilePointer)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<int32>>,
				AddConstVolatilePointer<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<const int32>>,
				AddConstVolatilePointer<const int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<volatile int32>>,
				AddConstVolatilePointer<volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<const volatile int32>>,
				AddConstVolatilePointer<const volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<int32&>>,
				AddConstVolatilePointer<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<const int32&>>,
				AddConstVolatilePointer<const int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<volatile int32&>>,
				AddConstVolatilePointer<volatile int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<const volatile int32&>>,
				AddConstVolatilePointer<const volatile int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<int32*>>,
				AddConstVolatilePointer<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<const int32*>>,
				AddConstVolatilePointer<const int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<volatile int32*>>,
				AddConstVolatilePointer<volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_cv_t<add_pointer_t<const volatile int32*>>,
				AddConstVolatilePointer<const volatile int32*>
				>)
		);
	}

	TEST(TypeTraits, AddLValueReference)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_lvalue_reference_t<int32>,
				AddLValueReference<int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_lvalue_reference_t<int32&>,
				AddLValueReference<int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_lvalue_reference_t<int32&&>,
				AddLValueReference<int32&&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_lvalue_reference_t<int32*>,
				AddLValueReference<int32*>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_lvalue_reference_t<int32*&>,
				AddLValueReference<int32*&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_lvalue_reference_t<int32*&&>,
				AddLValueReference<int32*&&>
				>)
		);
	}

	TEST(TypeTraits, AddPointer)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<int32>,
				AddPointer<int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<int32&>,
				AddPointer<int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<int32&&>,
				AddPointer<int32&&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<int32*>,
				AddPointer<int32*>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<int32*&>,
				AddPointer<int32*&>
				>)
		);
	}

	TEST(TypeTraits, AddPointerToConst)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<int32>>,
				AddPointerToConst<int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<remove_reference_t<int32&>>>,
				AddPointerToConst<int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<remove_reference_t<int32&&>>>,
				AddPointerToConst<int32&&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<remove_pointer_t<int32*>>>,
				AddPointerToConst<int32*>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<remove_pointer_t<remove_reference_t<int32*&>>>>,
				AddPointerToConst<int32*&>
				>)
		);
	}

	TEST(TypeTraits, AddPointerToVolatile)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_volatile_t<int32>>,
				AddPointerToVolatile<int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_volatile_t<remove_reference_t<int32&>>>,
				AddPointerToVolatile<int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_volatile_t<remove_reference_t<int32&&>>>,
				AddPointerToVolatile<int32&&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_volatile_t<remove_pointer_t<int32*>>>,
				AddPointerToVolatile<int32*>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_volatile_t<remove_pointer_t<remove_reference_t<int32*&>>>>,
				AddPointerToVolatile<int32*&>
				>)
		);
	}

	TEST(TypeTraits, AddPointerToConstVolatile)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<add_volatile_t<int32>>>,
				AddPointerToConstVolatile<int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<add_volatile_t<remove_reference_t<int32&>>>>,
				AddPointerToConstVolatile<int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<add_volatile_t<remove_reference_t<int32&&>>>>,
				AddPointerToConstVolatile<int32&&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<add_volatile_t<remove_pointer_t<int32*>>>>,
				AddPointerToConstVolatile<int32*>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_pointer_t<add_const_t<add_volatile_t<remove_pointer_t<remove_reference_t<int32*&>>>>>,
				AddPointerToConstVolatile<int32*&>
				>)
		);
	}

	TEST(TypeTraits, AddRValueReference)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_rvalue_reference_t<int32>,
				AddRValueReference<int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_rvalue_reference_t<int32&>,
				AddRValueReference<int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_rvalue_reference_t<int32&&>,
				AddRValueReference<int32&&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_rvalue_reference_t<int32*>,
				AddRValueReference<int32*>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_rvalue_reference_t<int32*&>,
				AddRValueReference<int32*&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_rvalue_reference_t<int32*&&>,
				AddRValueReference<int32*&&>
				>)
		);
	}

	TEST(TypeTraits, AddVolatile)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<int32>,
				AddVolatile<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<volatile int32>,
				AddVolatile<volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<int32&>,
				AddVolatile<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<volatile int32&>,
				AddVolatile<volatile int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<int32*>,
				AddVolatile<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<volatile int32*>,
				AddVolatile<volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<int32* volatile>,
				AddVolatile<int32* volatile>
				>)
		);
	}

	TEST(TypeTraits, AddVolatilePointer)
	{
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<add_pointer_t<int32>>,
				AddVolatilePointer<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<add_pointer_t<volatile int32>>,
				AddVolatilePointer<volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<add_pointer_t<int32&>>,
				AddVolatilePointer<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<add_pointer_t<volatile int32&>>,
				AddVolatilePointer<volatile int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<add_pointer_t<volatile int32*>>,
				AddVolatilePointer<volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				add_volatile_t<add_pointer_t<volatile int32* volatile>>,
				AddVolatilePointer<volatile int32* volatile>
				>)
		);
	}

	TEST(TypeTraits, AlignmentOf)
	{
		struct A
		{
			int32 M1;
			int8 M2;
		};

		struct B
		{
			int64 M1;
			int8 M2;
		};

		EXPECT_TRUE(
			(alignment_of_v<A> == AlignmentOf<A>)
		);
		EXPECT_TRUE(
			(alignment_of_v<B> == AlignmentOf<B>)
		);

		EXPECT_TRUE(
			(AlignmentOf<A> < AlignmentOf<B>)
		);
	}

	TEST(TypeTraits, And)
	{
		EXPECT_EQ(
			conjunction_v<true_type>,
			And<TrueConstant>::Value
		);
		EXPECT_EQ(
			(conjunction_v<true_type, true_type>),
			(And<TrueConstant, TrueConstant>::Value)
		);
		EXPECT_EQ(
			(conjunction_v<true_type, false_type>),
			(And<TrueConstant, FalseConstant>::Value)
		);
		EXPECT_EQ(
			(conjunction_v<false_type, false_type>),
			(And<FalseConstant, FalseConstant>::Value)
		);

		EXPECT_EQ(
			And<TrueConstant>::Value,
			AndValue<TrueConstant>
		);
	}

	TEST(TypeTraits, ArrayRank)
	{
		EXPECT_EQ(
			rank<int32>::value,
			ArrayRank<int32>::Value
		);
		EXPECT_EQ(
			rank<int32[5]>::value,
			ArrayRank<int32[5]>::Value
		);
		EXPECT_EQ(
			rank<int32[5][5]>::value,
			ArrayRank<int32[5][5]>::Value
		);
		EXPECT_EQ(
			rank<int32[][5][5]>::value,
			ArrayRank<int32[][5][5]>::Value
		);

		EXPECT_EQ(
			ArrayRank<int32>::Value,
			ArrayRankValue<int32>
		);
		EXPECT_EQ(
			ArrayRank<int32[5]>::Value,
			ArrayRankValue<int32[5]>
		);
		EXPECT_EQ(
			ArrayRank<int32[5][5]>::Value,
			ArrayRankValue<int32[5][5]>
		);
		EXPECT_EQ(
			ArrayRank<int32[][5][5]>::Value,
			ArrayRankValue<int32[][5][5]>
		);

		int32 array[][3] = {{1, 2, 3}};
		// array[0] is type of reference of 'int32[3]', so,
		// rank cannot calculate correctly and return 0
		EXPECT_EQ(
			rank<decltype(array[0])>::value,
			ArrayRank<decltype(array[0])>::Value
		);
		// removing reference will give correct rank value
		EXPECT_EQ(
			rank<remove_cvref_t<decltype(array[0])>>::value,
			ArrayRank<RemoveConstVolatileReference<decltype(array[0])>>::Value
		);

		EXPECT_EQ(
			ArrayRank<RemoveConstVolatileReference<decltype(array[0])>>::Value,
			ArrayRankValue<RemoveConstVolatileReference<decltype(array[0])>>
		);
	}

	TEST(TypeTraits, ArraySize)
	{
		EXPECT_EQ(
			extent_v<int32[]>,
			ArrayLength<int32[]>::Value
		);
		EXPECT_EQ(
			extent_v<int32[3]>,
			ArrayLength<int32[3]>::Value
		);
		EXPECT_EQ(
			(extent_v<int32[3][4], 0>),
			(ArrayLength<int32[3][4], 0>::Value)
		);
		EXPECT_EQ(
			(extent_v<int32[3][4], 1>),
			(ArrayLength<int32[3][4], 1>::Value)
		);
		EXPECT_EQ(
			(extent_v<int32[3][4], 2>),
			(ArrayLength<int32[3][4], 2>::Value)
		);

		EXPECT_EQ(
			ArrayLength<int32[]>::Value,
			ArrayLengthValue<int32[]>
		);
		EXPECT_EQ(
			ArrayLength<int32[3]>::Value,
			ArrayLengthValue<int32[3]>
		);
		EXPECT_EQ(
			(ArrayLength<int32[3][4], 0>::Value),
			(ArrayLengthValue<int32[3][4], 0>)
		);
		EXPECT_EQ(
			(ArrayLength<int32[3][4], 1>::Value),
			(ArrayLengthValue<int32[3][4], 1>)
		);
		EXPECT_EQ(
			(ArrayLength<int32[3][4], 2>::Value),
			(ArrayLengthValue<int32[3][4], 2>)
		);

		int32 array[][3] = {{1, 2, 3}};

		// array[0] is type of reference of 'int32[3]', so,
		// extent cannot calculate correctly and return 0
		EXPECT_EQ(
			extent<decltype(array[0])>::value,
			ArrayLength<decltype(array[0])>::Value
		);
		// removing reference will give correct extent value
		EXPECT_EQ(
			extent<remove_cvref_t<decltype(array[0])>>::value,
			ArrayLength<RemoveConstVolatileReference<decltype(array[0])>>::Value
		);

		EXPECT_EQ(
			ArrayLength<RemoveConstVolatileReference<decltype(array[0])>>::Value,
			ArrayLengthValue<RemoveConstVolatileReference<decltype(array[0])>>
		);
	}

	TEST(TypeTraits, CommonType)
	{
		struct A {};

		struct B : A {};

		EXPECT_TRUE(
			(is_same_v<
				common_type_t<int32, float32>,
				CommonType<int32, float32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				common_type_t<A, B>,
				CommonType<A, B>
				>)
		);
	}

	TEST(TypeTraits, CopyConstVolatile)
	{
		EXPECT_TRUE(
			(is_same_v<
				int32,
				CopyConstVolatile<int32, int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const int32,
				CopyConstVolatile<const int32, int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				volatile int32,
				CopyConstVolatile<volatile int32, int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32,
				CopyConstVolatile<const volatile int32, int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				const volatile int32,
				CopyConstVolatile<int32, const volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32,
				CopyConstVolatile<const int32, const volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32,
				CopyConstVolatile<volatile int32, const volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32,
				CopyConstVolatile<const volatile int32, const volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				int32*,
				CopyConstVolatile<int32, int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				int32* const,
				CopyConstVolatile<const int32, int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				int32* volatile,
				CopyConstVolatile<volatile int32, int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				int32* const volatile,
				CopyConstVolatile<const volatile int32, int32*>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				const volatile int32*,
				CopyConstVolatile<int32, const volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32* const,
				CopyConstVolatile<const int32, const volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32* volatile,
				CopyConstVolatile<volatile int32, const volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32* const volatile,
				CopyConstVolatile<const volatile int32, const volatile int32*>
				>)
		);
	}

	TEST(TypeTraits, Decay)
	{
		EXPECT_TRUE(
			(is_same_v<
				decay_t<int32>,
				Decay<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				decay_t<int32>,
				Decay<int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				decay_t<int32&>,
				Decay<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				decay_t<int32&&>,
				Decay<int32&&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				decay_t<const int32&>,
				Decay<const int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				decay_t<int32[2]>,
				Decay<int32[2]>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				decay_t<int32[4][2]>,
				Decay<int32[4][2]>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				decay_t<int32(int32)>,
				Decay<int32(int32)>
				>)
		);
	}

	TEST(TypeTraits, EnableIf)
	{
		EXPECT_TRUE(
			(is_same_v<
				enable_if_t<true, int32>(),
				EnableIf<true, int32>()
				>)
		);
	}

	TEST(TypeTraits, EnumUnderlyingType)
	{
		enum EA : uint8 {};

		enum class EB : uint16 {};

		EXPECT_TRUE(
			(is_same_v<
				underlying_type_t<EA>,
				EnumUnderlyingType<EA>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				underlying_type_t<EB>,
				EnumUnderlyingType<EB>
				>)
		);
	}

	TEST(TypeTraits, HasUniqueObjectRepresentation)
	{
		struct A
		{
			int8 F1;
			float F2;
			short F3;
			int32 F4;
		};

		struct B
		{
			int32 F1;
			int32 F2;
		};

		EXPECT_EQ(
			has_unique_object_representations_v<A>,
			HasUniqueObjectRepresentation<A>::Value
		);
		EXPECT_EQ(
			has_unique_object_representations_v<B>,
			HasUniqueObjectRepresentation<B>::Value
		);

		EXPECT_EQ(
			HasUniqueObjectRepresentation<A>::Value,
			HasUniqueObjectRepresentationValue<A>
		);
		EXPECT_EQ(
			HasUniqueObjectRepresentation<B>::Value,
			HasUniqueObjectRepresentationValue<B>
		);
	}

	TEST(TypeTraits, HasVirtualDestructor)
	{
		struct A
		{
			~A() = default;
		};

		struct B
		{
			virtual ~B() = default;
		};

		EXPECT_EQ(
			has_virtual_destructor_v<A>,
			HasVirtualDestructor<A>::Value
		);
		EXPECT_EQ(
			has_virtual_destructor_v<B>,
			HasVirtualDestructor<B>::Value
		);

		EXPECT_EQ(
			HasVirtualDestructor<A>::Value,
			HasVirtualDestructorValue<A>
		);
		EXPECT_EQ(
			HasVirtualDestructor<B>::Value,
			HasVirtualDestructorValue<B>
		);
	}

	TEST(TypeTraits, Identity)
	{
		EXPECT_TRUE(
			(is_same_v<
				type_identity_t<int32>,
				Identity<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				type_identity_t<void*>,
				Identity<void*>
				>)
		);
	}

	TEST(TypeTraits, If)
	{
		EXPECT_TRUE(
			(If<true, TrueConstant, FalseConstant>::Value)
		);
		EXPECT_FALSE(
			(If<false, TrueConstant, FalseConstant>::Value)
		);
	}

	TEST(TypeTraits, InvokeResult)
	{
		struct R
		{
			int8 operator()();

			int16 operator()(
				int8
			);
		};


		struct S
		{
			R Callable;

			function<int32()> Functor1;
			function<int64(
				int8
			)> Functor2;

			float32 operator()(
				int8
			);

			float64 Test(
				int8
			);
		};


		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<S, int32>,
				InvokeResult<S, int32>
				>)
		);
		// EXPECT_TRUE(
		// 	(is_same_v<
		// 		invoke_result_t<decltype(S::Callable)>,
		// 		InvokeResult<decltype(S::Callable)>
		// 		>)
		// );
		// EXPECT_TRUE(
		// 	(is_same_v<
		// 		invoke_result_t<decltype(S::Callable), int8>,
		// 		InvokeResult<decltype(S::Callable), int8>
		// 		>)
		// );
		// EXPECT_TRUE(
		// 	(is_same_v<
		// 		invoke_result_t<decltype(S::Functor1)>,
		// 		InvokeResult<decltype(S::Functor1)>
		// 		>)
		// );
		// EXPECT_TRUE(
		// 	(is_same_v<
		// 		invoke_result_t<decltype(S::Functor2), int8>,
		// 		InvokeResult<decltype(S::Functor2), int8>
		// 		>)
		// );
		// EXPECT_TRUE(
		// 	(is_same_v<
		// 		invoke_result_t<decltype(S::Test), int8>,
		// 		InvokeResult<decltype(S::Test), int8>
		// 		>)
		// );

		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<decltype(&S::Callable), S>,
				InvokeResult<decltype(&S::Callable), S>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<decltype(&S::Functor1), S>,
				InvokeResult<decltype(&S::Functor1), S>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<decltype(&S::Functor2), S>,
				InvokeResult<decltype(&S::Functor2), S>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<decltype(&S::Test), S, int8>,
				InvokeResult<decltype(&S::Test), S, int8>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<decltype(&S::Callable), S*>,
				InvokeResult<decltype(&S::Callable), S*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<decltype(&S::Functor1), S*>,
				InvokeResult<decltype(&S::Functor1), S*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<decltype(&S::Functor2), S*>,
				InvokeResult<decltype(&S::Functor2), S*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				invoke_result_t<decltype(&S::Test), S*, int8>,
				InvokeResult<decltype(&S::Test), S*, int8>
				>)
		);
	}

	TEST(TypeTraits, IsAbstract)
	{
		struct A
		{
			int32 M;
		};

		struct B
		{
			virtual void foo() {}
		};

		struct C
		{
			virtual void foo() = 0;
		};

		struct D : C {};

		EXPECT_EQ(
			is_abstract<A>::value,
			IsAbstract<A>::Value
		);
		EXPECT_EQ(
			is_abstract<B>::value,
			IsAbstract<B>::Value
		);
		EXPECT_EQ(
			is_abstract<C>::value,
			IsAbstract<C>::Value
		);
		EXPECT_EQ(
			is_abstract<D>::value,
			IsAbstract<D>::Value
		);

		EXPECT_EQ(
			IsAbstract<A>::Value,
			IsAbstractValue<A>
		);
		EXPECT_EQ(
			IsAbstract<B>::Value,
			IsAbstractValue<B>
		);
		EXPECT_EQ(
			IsAbstract<C>::Value,
			IsAbstractValue<C>
		);
		EXPECT_EQ(
			IsAbstract<D>::Value,
			IsAbstractValue<D>
		);
	}

	TEST(TypeTraits, IsAllBase)
	{
		EXPECT_TRUE(
			(IsAllBase<int32, int32, const int32, int32&, int32*, int32*&>::Value)
		);
		EXPECT_TRUE(
			(IsAllBase<const int32, int32, const int32, int32&, int32*, int32*&>::Value)
		);
		EXPECT_TRUE(
			(IsAllBase<int32*, int32, const int32, int32&, int32*, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsAllBase<int32, int32, const int32, int32&, int32*, int32*&>::Value),
			(IsAllBaseValue<int32, int32, const int32, int32&, int32*, int32*&>)
		);
		EXPECT_EQ(
			(IsAllBase<const int32, int32, const int32, int32&, int32*, int32*&>::Value),
			(IsAllBaseValue<const int32, int32, const int32, int32&, int32*, int32*&>)
		);
		EXPECT_EQ(
			(IsAllBase<int32*, int32, const int32, int32&, int32*, int32*&>::Value),
			(IsAllBaseValue<int32*, int32, const int32, int32&, int32*, int32*&>)
		);
	}

	TEST(TypeTraits, IsAllStrong)
	{
		EXPECT_TRUE(
			(IsAllStrong<int32, int32>::Value)
		);
		EXPECT_FALSE(
			(IsAllStrong<const int32, int32, const int32, int32&, int32*, int32*&>::Value)
		);
		EXPECT_FALSE(
			(IsAllStrong<int32*, int32, const int32, int32&, int32*, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsAllStrong<int32, int32>::Value),
			(IsAllStrongValue<int32, int32>)
		);
		EXPECT_EQ(
			(IsAllStrong<const int32, int32, const int32, int32&, int32*, int32*&>::Value),
			(IsAllStrongValue<const int32, int32, const int32, int32&, int32*, int32*&>)
		);
		EXPECT_EQ(
			(IsAllStrong<int32*, int32, const int32, int32&, int32*, int32*&>::Value),
			(IsAllStrongValue<int32*, int32, const int32, int32&, int32*, int32*&>)
		);
	}

	TEST(TypeTraits, IsAllWeak)
	{
		EXPECT_TRUE(
			(IsAllWeak<int32, int32, const int32>::Value)
		);
		EXPECT_TRUE(
			(IsAllWeak<const int32, int32, const int32>::Value)
		);
		EXPECT_FALSE(
			(IsAllWeak<int32*, int32, const int32, int32&, int32*, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsAllWeak<int32, int32, const int32>::Value),
			(IsAllWeakValue<int32, int32, const int32>)
		);
		EXPECT_EQ(
			(IsAllWeak<const int32, int32, const int32>::Value),
			(IsAllWeakValue<const int32, int32, const int32>)
		);
		EXPECT_EQ(
			(IsAllWeak<int32*, int32, const int32, int32&, int32*, int32*&>::Value),
			(IsAllWeakValue<int32*, int32, const int32, int32&, int32*, int32*&>)
		);
	}

	TEST(TypeTraits, IsAnyBase)
	{
		EXPECT_TRUE(
			(IsAnyBase<int32, int32, const int64, float32&, char16*, int32*&>::Value)
		);
		EXPECT_TRUE(
			(IsAnyBase<const int32, int32, const int64, float32&, char16*, int32*&>::Value)
		);
		EXPECT_TRUE(
			(IsAnyBase<int32*, int32, const int64, float32&, char16*, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsAnyBase<int32, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyBaseValue<int32, int32, const int64, float32&, char16*, int32*&>)
		);
		EXPECT_EQ(
			(IsAnyBase<const int32, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyBaseValue<const int32, int32, const int64, float32&, char16*, int32*&>)
		);
		EXPECT_EQ(
			(IsAnyBase<int32*, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyBaseValue<int32*, int32, const int64, float32&, char16*, int32*&>)
		);
	}

	TEST(TypeTraits, IsAnyStrong)
	{
		EXPECT_TRUE(
			(IsAnyStrong<int32, int32, const int64, float32&, char16*, int32*&>::Value)
		);
		EXPECT_FALSE(
			(IsAnyStrong<const int32, int32, const int64, float32&, char16*, int32*&>::Value)
		);
		EXPECT_FALSE(
			(IsAnyStrong<int32*, int32, const int64, float32&, char16*, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsAnyStrong<int32, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyStrongValue<int32, int32, const int64, float32&, char16*, int32*&>)
		);
		EXPECT_EQ(
			(IsAnyStrong<const int32, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyStrongValue<const int32, int32, const int64, float32&, char16*, int32*&>)
		);
		EXPECT_EQ(
			(IsAnyStrong<int32*, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyStrongValue<int32*, int32, const int64, float32&, char16*, int32*&>)
		);
	}

	TEST(TypeTraits, IsAnyWeak)
	{
		EXPECT_TRUE(
			(IsAnyWeak<int32, int32, const int64, float32&, char16*, int32*&>::Value)
		);
		EXPECT_TRUE(
			(IsAnyWeak<const int32, int32, const int64, float32&, char16*, int32*&>::Value)
		);
		EXPECT_FALSE(
			(IsAnyWeak<int32*, int32, const int64, float32&, char16*, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsAnyWeak<int32, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyWeakValue<int32, int32, const int64, float32&, char16*, int32*&>)
		);
		EXPECT_EQ(
			(IsAnyWeak<const int32, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyWeakValue<const int32, int32, const int64, float32&, char16*, int32*&>)
		);
		EXPECT_EQ(
			(IsAnyWeak<int32*, int32, const int64, float32&, char16*, int32*&>::Value),
			(IsAnyWeakValue<int32*, int32, const int64, float32&, char16*, int32*&>)
		);
	}

	TEST(TypeTraits, IsArray)
	{
		EXPECT_EQ(
			is_array_v<int32>,
			IsArray<int32>::Value
		);
		EXPECT_EQ(
			is_array_v<int32[]>,
			IsArray<int32[]>::Value
		);
		EXPECT_EQ(
			is_array_v<int32[5]>,
			IsArray<int32[5]>::Value
		);
		EXPECT_EQ(
			is_array_v<int32*>,
			IsArray<int32*>::Value
		);

		EXPECT_EQ(
			IsArray<int32>::Value,
			IsArrayValue<int32>
		);
		EXPECT_EQ(
			IsArray<int32[]>::Value,
			IsArrayValue<int32[]>
		);
		EXPECT_EQ(
			IsArray<int32[5]>::Value,
			IsArrayValue<int32[5]>
		);
		EXPECT_EQ(
			IsArray<int32*>::Value,
			IsArrayValue<int32*>
		);
	}

	TEST(TypeTraits, IsAssignable)
	{
		// 1 = 1; wouldn't compile
		EXPECT_EQ(
			(is_assignable_v<int32, int32>),
			(IsAssignable<int32, int32>::Value)
		);
		// int32 a; a = 1; works
		EXPECT_EQ(
			(is_assignable_v<int32&, int32>),
			(IsAssignable<int32&, int32>::Value)
		);
		EXPECT_EQ(
			(is_assignable_v<int32&, int64>),
			(IsAssignable<int32&, int64>::Value)
		);
		EXPECT_EQ(
			(is_assignable_v<int32, float64>),
			(IsAssignable<int32, float64>::Value)
		);
		EXPECT_EQ(
			(is_assignable_v<char8, float64>),
			(IsAssignable<char8, float64>::Value)
		);

		// 1 = 1; wouldn't compile
		EXPECT_EQ(
			(IsAssignable<int32, int32>::Value),
			(IsAssignableValue<int32, int32>)
		);
		// int32 a = 1; works
		EXPECT_EQ(
			(IsAssignable<int32&, int32>::Value),
			(IsAssignableValue<int32&, int32>)
		);
		EXPECT_EQ(
			(IsAssignable<int32&, int64>::Value),
			(IsAssignableValue<int32&, int64>)
		);
		EXPECT_EQ(
			(IsAssignable<int32, float64>::Value),
			(IsAssignableValue<int32, float64>)
		);
		EXPECT_EQ(
			(IsAssignable<char8, float64>::Value),
			(IsAssignableValue<char8, float64>)
		);
	}

	TEST(TypeTraits, IsAssignableNoThrow)
	{
		EXPECT_EQ(
			(is_nothrow_assignable_v<int32&, float64>),
			(IsAssignableNoThrow<int32&, float64>::Value)
		);

		EXPECT_EQ(
			(IsAssignableNoThrow<int32&, float64>::Value),
			(IsAssignableNoThrowValue<int32&, float64>)
		);
	}

	TEST(TypeTraits, IsAssignableTrivially)
	{
		struct Ex1
		{
			int32 M;
		};

		EXPECT_EQ(
			(is_trivially_assignable_v<Ex1&, const Ex1&>),
			(IsAssignableTrivially<Ex1&, const Ex1&>::Value)
		);

		EXPECT_EQ(
			(IsAssignableTrivially<Ex1&, const Ex1&>::Value),
			(IsAssignableTriviallyValue<Ex1&, const Ex1&>)
		);
	}

	TEST(TypeTraits, IsBool)
	{
		EXPECT_TRUE(
			IsBool<bool>::Value
		);
		EXPECT_FALSE(
			IsBool<bool&>::Value
		);
		EXPECT_FALSE(
			IsBool<bool*>::Value
		);
		EXPECT_FALSE(
			IsBool<bool()>::Value
		);

		EXPECT_TRUE(
			IsBoolValue<bool>
		);
		EXPECT_FALSE(
			IsBoolValue<bool&>
		);
		EXPECT_FALSE(
			IsBoolValue<bool*>
		);
		EXPECT_FALSE(
			IsBoolValue<bool()>
		);
	}

	TEST(TypeTraits, IsBoundedArray)
	{
		EXPECT_EQ(
			is_bounded_array_v<int32>,
			IsBoundedArray<int32>::Value
		);
		EXPECT_EQ(
			is_bounded_array_v<int32[]>,
			IsBoundedArray<int32[]>::Value
		);
		EXPECT_EQ(
			is_bounded_array_v<int32[5]>,
			IsBoundedArray<int32[5]>::Value
		);
		EXPECT_EQ(
			is_bounded_array_v<int32*>,
			IsBoundedArray<int32*>::Value
		);

		EXPECT_EQ(
			IsBoundedArray<int32>::Value,
			IsBoundedArrayValue<int32>
		);
		EXPECT_EQ(
			IsBoundedArray<int32[]>::Value,
			IsBoundedArrayValue<int32[]>
		);
		EXPECT_EQ(
			IsBoundedArray<int32[5]>::Value,
			IsBoundedArrayValue<int32[5]>
		);
		EXPECT_EQ(
			IsBoundedArray<int32*>::Value,
			IsBoundedArrayValue<int32*>
		);
	}

	TEST(TypeTraits, IsCastable)
	{
		struct A {};

		struct B : A {};

		struct C {};

		struct D
		{
			C M;

			operator C() const
			{
				return M;
			}
		};

		// struct E
		// {
		// 	template<class T>
		// 	E(
		// 		T&&
		//
		// 	) { }
		// };

		EXPECT_EQ(
			(is_convertible_v<B*, A*>),
			(IsCastable<B*, A*>::Value)
		);
		EXPECT_EQ(
			(is_convertible_v<A*, B*>),
			(IsCastable<A*, B*>::Value)
		);
		EXPECT_EQ(
			(is_convertible_v<D, C>),
			(IsCastable<D, C>::Value)
		);
		EXPECT_EQ(
			(is_convertible_v<B*, C*>),
			(IsCastable<B*, C*>::Value)
		);
		// Perfect Forwarding constructor makes the class E be
		// "convertible" from everything. So, A is replaceable by B, C, D..:
		// is_convertible_v<A, E> << ' ';

		EXPECT_EQ(
			(IsCastable<B*, A*>::Value),
			(IsCastableValue<B*, A*>)
		);
		EXPECT_EQ(
			(IsCastable<A*, B*>::Value),
			(IsCastableValue<A*, B*>)
		);
		EXPECT_EQ(
			(IsCastable<D, C>::Value),
			(IsCastableValue<D, C>)
		);
		EXPECT_EQ(
			(IsCastable<B*, C*>::Value),
			(IsCastableValue<B*, C*>)
		);
	}

	TEST(TypeTraits, IsCastableNoThrow)
	{
		EXPECT_EQ(
			(is_nothrow_convertible_v<int32, float32>),
			(IsCastableNoThrow<int32, float32>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_convertible_v<float32, char8*>),
			(IsCastableNoThrow<float32, char8*>::Value)
		);

		EXPECT_EQ(
			(IsCastableNoThrow<int32, float32>::Value),
			(IsCastableNoThrowValue<int32, float32>)
		);
		EXPECT_EQ(
			(IsCastableNoThrow<float32, char8*>::Value),
			(IsCastableNoThrowValue<float32, char8*>)
		);
	}

	TEST(TypeTraits, IsChar)
	{
		EXPECT_FALSE(
			IsChar<char>::Value
		);
		EXPECT_FALSE(
			IsChar<unsigned char>::Value
		);
		EXPECT_TRUE(
			IsChar<char8>::Value
		);
		EXPECT_TRUE(
			IsChar<char16>::Value
		);
		EXPECT_TRUE(
			IsChar<char32>::Value
		);

		EXPECT_EQ(
			IsChar<char>::Value,
			IsCharValue<char>
		);
		EXPECT_EQ(
			IsChar<unsigned char>::Value,
			IsCharValue<unsigned char>
		);
		EXPECT_EQ(
			IsChar<char8>::Value,
			IsCharValue<char8>
		);
		EXPECT_EQ(
			IsChar<char16>::Value,
			IsCharValue<char16>
		);
		EXPECT_EQ(
			IsChar<char32>::Value,
			IsCharValue<char32>
		);
	}

	TEST(TypeTraits, IsClass)
	{
		enum EA {};

		enum class EB {};

		union UA {};

		struct A {};

		class B {};

		EXPECT_EQ(
			is_class_v<EA>,
			IsClass<EA>::Value
		);
		EXPECT_EQ(
			is_class_v<EB>,
			IsClass<EB>::Value
		);
		EXPECT_EQ(
			is_class_v<UA>,
			IsClass<UA>::Value
		);
		EXPECT_EQ(
			is_class_v<A>,
			IsClass<A>::Value
		);
		EXPECT_EQ(
			is_class_v<B>,
			IsClass<B>::Value
		);

		EXPECT_EQ(
			IsClass<EA>::Value,
			IsClassValue<EA>
		);
		EXPECT_EQ(
			IsClass<EB>::Value,
			IsClassValue<EB>
		);
		EXPECT_EQ(
			IsClass<UA>::Value,
			IsClassValue<UA>
		);
		EXPECT_EQ(
			IsClass<A>::Value,
			IsClassValue<A>
		);
		EXPECT_EQ(
			IsClass<B>::Value,
			IsClassValue<B>
		);
	}

	TEST(TypeTraits, IsCompound)
	{
		enum EA {};

		enum class EB {};

		union UA {};

		struct A
		{
			int32 F;

			int32 M();
		};

		class B {};

		EXPECT_EQ(
			is_compound_v<int32>,
			IsCompound<int32>::Value
		);
		EXPECT_EQ(
			is_compound_v<int32[5]>,
			IsCompound<int32[5]>::Value
		);
		EXPECT_EQ(
			is_compound_v<int32()>,
			IsCompound<int32()>::Value
		);
		EXPECT_EQ(
			is_compound_v<int32&>,
			IsCompound<int32&>::Value
		);
		EXPECT_EQ(
			is_compound_v<int32*>,
			IsCompound<int32*>::Value
		);
		EXPECT_EQ(
			is_compound_v<int32(*)()>,
			IsCompound<int32(*)()>::Value
		);
		EXPECT_EQ(
			is_compound_v<decltype(&A::F)>,
			IsCompound<decltype(&A::F)>::Value
		);
		EXPECT_EQ(
			is_compound_v<decltype(&A::M)>,
			IsCompound<decltype(&A::M)>::Value
		);
		EXPECT_EQ(
			is_compound_v<EA>,
			IsCompound<EA>::Value
		);
		EXPECT_EQ(
			is_compound_v<EB>,
			IsCompound<EB>::Value
		);
		EXPECT_EQ(
			is_compound_v<UA>,
			IsCompound<UA>::Value
		);
		EXPECT_EQ(
			is_compound_v<A>,
			IsCompound<A>::Value
		);
		EXPECT_EQ(
			is_compound_v<B>,
			IsCompound<B>::Value
		);

		EXPECT_EQ(
			IsCompound<int32>::Value,
			IsCompoundValue<int32>
		);
		EXPECT_EQ(
			IsCompound<int32[5]>::Value,
			IsCompoundValue<int32[5]>
		);
		EXPECT_EQ(
			IsCompound<int32()>::Value,
			IsCompoundValue<int32()>
		);
		EXPECT_EQ(
			IsCompound<int32&>::Value,
			IsCompoundValue<int32&>
		);
		EXPECT_EQ(
			IsCompound<int32*>::Value,
			IsCompoundValue<int32*>
		);
		EXPECT_EQ(
			IsCompound<int32(*)()>::Value,
			IsCompoundValue<int32(*)()>
		);
		EXPECT_EQ(
			IsCompound<decltype(&A::F)>::Value,
			IsCompoundValue<decltype(&A::F)>
		);
		EXPECT_EQ(
			IsCompound<decltype(&A::M)>::Value,
			IsCompoundValue<decltype(&A::M)>
		);
		EXPECT_EQ(
			IsCompound<EA>::Value,
			IsCompoundValue<EA>
		);
		EXPECT_EQ(
			IsCompound<EB>::Value,
			IsCompoundValue<EB>
		);
		EXPECT_EQ(
			IsCompound<UA>::Value,
			IsCompoundValue<UA>
		);
		EXPECT_EQ(
			IsCompound<A>::Value,
			IsCompoundValue<A>
		);
		EXPECT_EQ(
			IsCompound<B>::Value,
			IsCompoundValue<B>
		);
	}

	TEST(TypeTraits, IsConst)
	{
		EXPECT_EQ(
			is_const_v<int32>,
			IsConst<int32>::Value
		);
		EXPECT_EQ(
			is_const_v<const int32>,
			IsConst<const int32>::Value
		);
		EXPECT_EQ(
			is_const_v<const int32&>,
			IsConst<const int32&>::Value
		);
		EXPECT_EQ(
			is_const_v<const int32*>,
			IsConst<const int32*>::Value
		);
		EXPECT_EQ(
			is_const_v<int32* const>,
			IsConst<int32* const>::Value
		);
		EXPECT_EQ(
			is_const_v<const int32()>,
			IsConst<const int32()>::Value
		);
		EXPECT_EQ(
			is_const_v<int32() const>,
			IsConst<int32() const>::Value
		);

		EXPECT_EQ(
			IsConst<int32>::Value,
			IsConstValue<int32>
		);
		EXPECT_EQ(
			IsConst<const int32>::Value,
			IsConstValue<const int32>
		);
		EXPECT_EQ(
			IsConst<const int32&>::Value,
			IsConstValue<const int32&>
		);
		EXPECT_EQ(
			IsConst<const int32*>::Value,
			IsConstValue<const int32*>
		);
		EXPECT_EQ(
			IsConst<int32* const>::Value,
			IsConstValue<int32* const>
		);
		EXPECT_EQ(
			IsConst<const int32()>::Value,
			IsConstValue<const int32()>
		);
		EXPECT_EQ(
			IsConst<int32() const>::Value,
			IsConstValue<int32() const>
		);
	}

	TEST(TypeTraits, IsConstructible)
	{
		class A
		{
			int32 F1;
			float64 F2;

		public:
			A() = default;

			A(
				const int32 f1
			):
				F1(f1),
				F2() {}

			A(
				const int32 f1,
				const float64 f2
			) noexcept :
				F1(f1),
				F2(f2) {}
		};

		EXPECT_EQ(
			(is_constructible_v<int32>),
			(IsConstructible<int32>::Value)
		);
		EXPECT_EQ(
			(is_constructible_v<int32*>),
			(IsConstructible<int32*>::Value)
		);
		EXPECT_EQ(
			(is_constructible_v<A>),
			(IsConstructible<A>::Value)
		);
		EXPECT_EQ(
			(is_constructible_v<A, int32>),
			(IsConstructible<A, int32>::Value)
		);
		EXPECT_EQ(
			(is_constructible_v<A, int32*>),
			(IsConstructible<A, int32*>::Value)
		);
		EXPECT_EQ(
			(is_constructible_v<A, int32, float64>),
			(IsConstructible<A, int32, float64>::Value)
		);
		EXPECT_EQ(
			(is_constructible_v<A, const A&>),
			(IsConstructible<A, const A&>::Value)
		);

		EXPECT_EQ(
			(IsConstructible<int32>::Value),
			(IsConstructibleValue<int32>)
		);
		EXPECT_EQ(
			(IsConstructible<int32*>::Value),
			(IsConstructibleValue<int32*>)
		);
		EXPECT_EQ(
			(IsConstructible<A>::Value),
			(IsConstructibleValue<A>)
		);
		EXPECT_EQ(
			(IsConstructible<A, int32>::Value),
			(IsConstructibleValue<A, int32>)
		);
		EXPECT_EQ(
			(IsConstructible<A, int32*>::Value),
			(IsConstructibleValue<A, int32*>)
		);
		EXPECT_EQ(
			(IsConstructible<A, int32, float64>::Value),
			(IsConstructibleValue<A, int32, float64>)
		);
		EXPECT_EQ(
			(IsConstructible<A, const A&>::Value),
			(IsConstructibleValue<A, const A&>)
		);
	}

	TEST(TypeTraits, IsConstructibleNoThrow)
	{
		class A
		{
			int32 F1;
			float64 F2;

		public:
			A() = default;

			A(
				const int32 f1
			):
				F1(f1),
				F2() {}

			A(
				const int32 f1,
				const float64 f2
			) noexcept :
				F1(f1),
				F2(f2) {}
		};

		EXPECT_EQ(
			(is_nothrow_constructible_v<int32>),
			(IsConstructibleNoThrow<int32>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_constructible_v<int32*>),
			(IsConstructibleNoThrow<int32*>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_constructible_v<A>),
			(IsConstructibleNoThrow<A>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_constructible_v<A, int32>),
			(IsConstructibleNoThrow<A, int32>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_constructible_v<A, int32*>),
			(IsConstructibleNoThrow<A, int32*>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_constructible_v<A, int32, float64>),
			(IsConstructibleNoThrow<A, int32, float64>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_constructible_v<A, const A&>),
			(IsConstructibleNoThrow<A, const A&>::Value)
		);

		EXPECT_EQ(
			(IsConstructibleNoThrow<int32>::Value),
			(IsConstructibleNoThrowValue<int32>)
		);
		EXPECT_EQ(
			(IsConstructibleNoThrow<int32*>::Value),
			(IsConstructibleNoThrowValue<int32*>)
		);
		EXPECT_EQ(
			(IsConstructibleNoThrow<A>::Value),
			(IsConstructibleNoThrowValue<A>)
		);
		EXPECT_EQ(
			(IsConstructibleNoThrow<A, int32>::Value),
			(IsConstructibleNoThrowValue<A, int32>)
		);
		EXPECT_EQ(
			(IsConstructibleNoThrow<A, int32*>::Value),
			(IsConstructibleNoThrowValue<A, int32*>)
		);
		EXPECT_EQ(
			(IsConstructibleNoThrow<A, int32, float64>::Value),
			(IsConstructibleNoThrowValue<A, int32, float64>)
		);
		EXPECT_EQ(
			(IsConstructibleNoThrow<A, const A&>::Value),
			(IsConstructibleNoThrowValue<A, const A&>)
		);
	}

	TEST(TypeTraits, IsConstructibleTrivially)
	{
		class A
		{
			int32 F1;
			float64 F2;

		public:
			A() = default;

			A(
				const int32 f1
			):
				F1(f1),
				F2() {}

			A(
				const int32 f1,
				const float64 f2
			) noexcept :
				F1(f1),
				F2(f2) {}
		};

		EXPECT_EQ(
			(is_trivially_constructible_v<int32>),
			(IsConstructibleTrivially<int32>::Value)
		);
		EXPECT_EQ(
			(is_trivially_constructible_v<int32*>),
			(IsConstructibleTrivially<int32*>::Value)
		);
		EXPECT_EQ(
			(is_trivially_constructible_v<A>),
			(IsConstructibleTrivially<A>::Value)
		);
		EXPECT_EQ(
			(is_trivially_constructible_v<A, int32>),
			(IsConstructibleTrivially<A, int32>::Value)
		);
		EXPECT_EQ(
			(is_trivially_constructible_v<A, int32*>),
			(IsConstructibleTrivially<A, int32*>::Value)
		);
		EXPECT_EQ(
			(is_trivially_constructible_v<A, int32, float64>),
			(IsConstructibleTrivially<A, int32, float64>::Value)
		);
		EXPECT_EQ(
			(is_trivially_constructible_v<A, const A&>),
			(IsConstructibleTrivially<A, const A&>::Value)
		);

		EXPECT_EQ(
			(IsConstructibleTrivially<int32>::Value),
			(IsConstructibleTriviallyValue<int32>)
		);
		EXPECT_EQ(
			(IsConstructibleTrivially<int32*>::Value),
			(IsConstructibleTriviallyValue<int32*>)
		);
		EXPECT_EQ(
			(IsConstructibleTrivially<A>::Value),
			(IsConstructibleTriviallyValue<A>)
		);
		EXPECT_EQ(
			(IsConstructibleTrivially<A, int32>::Value),
			(IsConstructibleTriviallyValue<A, int32>)
		);
		EXPECT_EQ(
			(IsConstructibleTrivially<A, int32*>::Value),
			(IsConstructibleTriviallyValue<A, int32*>)
		);
		EXPECT_EQ(
			(IsConstructibleTrivially<A, int32, float64>::Value),
			(IsConstructibleTriviallyValue<A, int32, float64>)
		);
		EXPECT_EQ(
			(IsConstructibleTrivially<A, const A&>::Value),
			(IsConstructibleTriviallyValue<A, const A&>)
		);
	}

	TEST(TypeTraits, IsConstVolatile)
	{
		EXPECT_EQ(
			(is_const_v<int32> && is_volatile_v<int32>),
			IsConstVolatile<int32>::Value
		);
		EXPECT_EQ(
			(is_const_v<const int32> && is_volatile_v<const int32>),
			IsConstVolatile<const int32>::Value
		);
		EXPECT_EQ(
			(is_const_v<volatile int32> && is_volatile_v<volatile int32>),
			IsConstVolatile<volatile int32>::Value
		);
		EXPECT_EQ(
			(is_const_v<const volatile int32> && is_volatile_v<const volatile int32>),
			IsConstVolatile<const volatile int32>::Value
		);
		EXPECT_EQ(
			(is_const_v<const volatile int32*> && is_volatile_v<const volatile int32*>),
			IsConstVolatile<const volatile int32*>::Value
		);
		EXPECT_EQ(
			(is_const_v<int32* const volatile> && is_volatile_v<int32* const volatile>),
			IsConstVolatile<int32* const volatile>::Value
		);

		EXPECT_EQ(
			IsConstVolatile<int32>::Value,
			IsConstVolatileValue<int32>
		);
		EXPECT_EQ(
			IsConstVolatile<const int32>::Value,
			IsConstVolatileValue<const int32>
		);
		EXPECT_EQ(
			IsConstVolatile<volatile int32>::Value,
			IsConstVolatileValue<volatile int32>
		);
		EXPECT_EQ(
			IsConstVolatile<const volatile int32>::Value,
			IsConstVolatileValue<const volatile int32>
		);
		EXPECT_EQ(
			IsConstVolatile<const volatile int32*>::Value,
			IsConstVolatileValue<const volatile int32*>
		);
		EXPECT_EQ(
			IsConstVolatile<int32* const volatile>::Value,
			IsConstVolatileValue<int32* const volatile>
		);
	}

	TEST(TypeTraits, IsCopyAssignable)
	{
		struct A
		{
			int32 F;
		};

		EXPECT_EQ(
			is_copy_assignable_v<int32>,
			IsCopyAssignable<int32>::Value
		);
		EXPECT_EQ(
			is_copy_assignable_v<int32*>,
			IsCopyAssignable<int32*>::Value
		);
		EXPECT_EQ(
			is_copy_assignable_v<int32* const>,
			IsCopyAssignable<int32* const>::Value
		);
		EXPECT_EQ(
			is_copy_assignable_v<int32[2]>,
			IsCopyAssignable<int32[2]>::Value
		);
		EXPECT_EQ(
			is_copy_assignable_v<A>,
			IsCopyAssignable<A>::Value
		);

		EXPECT_EQ(
			IsCopyAssignable<int32>::Value,
			IsCopyAssignableValue<int32>
		);
		EXPECT_EQ(
			IsCopyAssignable<int32*>::Value,
			IsCopyAssignableValue<int32*>
		);
		EXPECT_EQ(
			IsCopyAssignable<int32* const>::Value,
			IsCopyAssignableValue<int32* const>
		);
		EXPECT_EQ(
			IsCopyAssignable<int32[2]>::Value,
			IsCopyAssignableValue<int32[2]>
		);
		EXPECT_EQ(
			IsCopyAssignable<A>::Value,
			IsCopyAssignableValue<A>
		);
	}

	TEST(TypeTraits, IsCopyAssignableNoThrow)
	{
		struct A
		{
			int32 F;
		};

		EXPECT_EQ(
			is_nothrow_copy_assignable_v<int32>,
			IsCopyAssignableNoThrow<int32>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_assignable_v<int32*>,
			IsCopyAssignableNoThrow<int32*>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_assignable_v<int32* const>,
			IsCopyAssignableNoThrow<int32* const>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_assignable_v<int32[2]>,
			IsCopyAssignableNoThrow<int32[2]>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_assignable_v<A>,
			IsCopyAssignableNoThrow<A>::Value
		);

		EXPECT_EQ(
			IsCopyAssignableNoThrow<int32>::Value,
			IsCopyAssignableNoThrowValue<int32>
		);
		EXPECT_EQ(
			IsCopyAssignableNoThrow<int32*>::Value,
			IsCopyAssignableNoThrowValue<int32*>
		);
		EXPECT_EQ(
			IsCopyAssignableNoThrow<int32* const>::Value,
			IsCopyAssignableNoThrowValue<int32* const>
		);
		EXPECT_EQ(
			IsCopyAssignableNoThrow<int32[2]>::Value,
			IsCopyAssignableNoThrowValue<int32[2]>
		);
		EXPECT_EQ(
			IsCopyAssignableNoThrow<A>::Value,
			IsCopyAssignableNoThrowValue<A>
		);
	}

	TEST(TypeTraits, IsCopyAssignableTrivially)
	{
		struct A
		{
			int32 F;
		};

		EXPECT_EQ(
			is_trivially_copy_assignable_v<int32>,
			IsCopyAssignableTrivially<int32>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_assignable_v<int32*>,
			IsCopyAssignableTrivially<int32*>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_assignable_v<int32* const>,
			IsCopyAssignableTrivially<int32* const>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_assignable_v<int32[2]>,
			IsCopyAssignableTrivially<int32[2]>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_assignable_v<A>,
			IsCopyAssignableTrivially<A>::Value
		);

		EXPECT_EQ(
			IsCopyAssignableTrivially<int32>::Value,
			IsCopyAssignableTriviallyValue<int32>
		);
		EXPECT_EQ(
			IsCopyAssignableTrivially<int32*>::Value,
			IsCopyAssignableTriviallyValue<int32*>
		);
		EXPECT_EQ(
			IsCopyAssignableTrivially<int32* const>::Value,
			IsCopyAssignableTriviallyValue<int32* const>
		);
		EXPECT_EQ(
			IsCopyAssignableTrivially<int32[2]>::Value,
			IsCopyAssignableTriviallyValue<int32[2]>
		);
		EXPECT_EQ(
			IsCopyAssignableTrivially<A>::Value,
			IsCopyAssignableTriviallyValue<A>
		);
	}

	TEST(TypeTraits, IsCopyConstructible)
	{
		struct A
		{
			int32 F;
		};

		struct B
		{
			string F;
		};

		EXPECT_EQ(
			is_copy_constructible_v<int32>,
			IsCopyConstructible<int32>::Value
		);
		EXPECT_EQ(
			is_copy_constructible_v<int32*>,
			IsCopyConstructible<int32*>::Value
		);
		EXPECT_EQ(
			is_copy_constructible_v<int32* const>,
			IsCopyConstructible<int32* const>::Value
		);
		EXPECT_EQ(
			is_copy_constructible_v<int32[2]>,
			IsCopyConstructible<int32[2]>::Value
		);
		EXPECT_EQ(
			is_copy_constructible_v<A>,
			IsCopyConstructible<A>::Value
		);
		EXPECT_EQ(
			is_copy_constructible_v<B>,
			IsCopyConstructible<B>::Value
		);

		EXPECT_EQ(
			IsCopyConstructible<int32>::Value,
			IsCopyConstructibleValue<int32>
		);
		EXPECT_EQ(
			IsCopyConstructible<int32*>::Value,
			IsCopyConstructibleValue<int32*>
		);
		EXPECT_EQ(
			IsCopyConstructible<int32* const>::Value,
			IsCopyConstructibleValue<int32* const>
		);
		EXPECT_EQ(
			IsCopyConstructible<int32[2]>::Value,
			IsCopyConstructibleValue<int32[2]>
		);
		EXPECT_EQ(
			IsCopyConstructible<A>::Value,
			IsCopyConstructibleValue<A>
		);
		EXPECT_EQ(
			IsCopyConstructible<B>::Value,
			IsCopyConstructibleValue<B>
		);
	}

	TEST(TypeTraits, IsCopyConstructibleNoThrow)
	{
		struct A
		{
			int32 F;
		};

		struct B
		{
			string F;
		};

		EXPECT_EQ(
			is_nothrow_copy_constructible_v<int32>,
			IsCopyConstructibleNoThrow<int32>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_constructible_v<int32*>,
			IsCopyConstructibleNoThrow<int32*>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_constructible_v<int32* const>,
			IsCopyConstructibleNoThrow<int32* const>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_constructible_v<int32[2]>,
			IsCopyConstructibleNoThrow<int32[2]>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_constructible_v<A>,
			IsCopyConstructibleNoThrow<A>::Value
		);
		EXPECT_EQ(
			is_nothrow_copy_constructible_v<B>,
			IsCopyConstructibleNoThrow<B>::Value
		);

		EXPECT_EQ(
			IsCopyConstructibleNoThrow<int32>::Value,
			IsCopyConstructibleNoThrowValue<int32>
		);
		EXPECT_EQ(
			IsCopyConstructibleNoThrow<int32*>::Value,
			IsCopyConstructibleNoThrowValue<int32*>
		);
		EXPECT_EQ(
			IsCopyConstructibleNoThrow<int32* const>::Value,
			IsCopyConstructibleNoThrowValue<int32* const>
		);
		EXPECT_EQ(
			IsCopyConstructibleNoThrow<int32[2]>::Value,
			IsCopyConstructibleNoThrowValue<int32[2]>
		);
		EXPECT_EQ(
			IsCopyConstructibleNoThrow<A>::Value,
			IsCopyConstructibleNoThrowValue<A>
		);
		EXPECT_EQ(
			IsCopyConstructibleNoThrow<B>::Value,
			IsCopyConstructibleNoThrowValue<B>
		);
	}

	TEST(TypeTraits, IsCopyConstructibleTrivially)
	{
		struct A
		{
			int32 F;
		};

		struct B
		{
			string F;
		};

		EXPECT_EQ(
			is_trivially_copy_constructible_v<int32>,
			IsCopyConstructibleTrivially<int32>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_constructible_v<int32*>,
			IsCopyConstructibleTrivially<int32*>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_constructible_v<int32* const>,
			IsCopyConstructibleTrivially<int32* const>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_constructible_v<int32[2]>,
			IsCopyConstructibleTrivially<int32[2]>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_constructible_v<A>,
			IsCopyConstructibleTrivially<A>::Value
		);
		EXPECT_EQ(
			is_trivially_copy_constructible_v<B>,
			IsCopyConstructibleTrivially<B>::Value
		);

		EXPECT_EQ(
			IsCopyConstructibleTrivially<int32>::Value,
			IsCopyConstructibleTriviallyValue<int32>
		);
		EXPECT_EQ(
			IsCopyConstructibleTrivially<int32*>::Value,
			IsCopyConstructibleTriviallyValue<int32*>
		);
		EXPECT_EQ(
			IsCopyConstructibleTrivially<int32* const>::Value,
			IsCopyConstructibleTriviallyValue<int32* const>
		);
		EXPECT_EQ(
			IsCopyConstructibleTrivially<int32[2]>::Value,
			IsCopyConstructibleTriviallyValue<int32[2]>
		);
		EXPECT_EQ(
			IsCopyConstructibleTrivially<A>::Value,
			IsCopyConstructibleTriviallyValue<A>
		);
		EXPECT_EQ(
			IsCopyConstructibleTrivially<B>::Value,
			IsCopyConstructibleTriviallyValue<B>
		);
	}

	// Todo: IsCorrespondingMember
	TEST(TypeTraits, IsCorrespondingMember)
	{
		GTEST_SKIP();
		// 	struct A
		// 	{
		// 		int32 Fa;
		// 	};
		//
		// 	struct B
		// 	{
		// 		int32 Fb1;
		// 		float64 Fb2;
		// 	};
		//
		// 	// not standard-layout
		// 	struct C : A, B { };
		//
		// 	EXPECT_EQ(
		// 		(is_corresponding_member(&A::Fa, &B::Fb1)),
		// 		(IsCorrespondingMember(&A::Fa, &B::Fb1))
		// 	);
		// 	EXPECT_EQ(
		// 		(is_corresponding_member(&C::Fa, &C::Fb1)),
		// 		(IsCorrespondingMember(&C::Fa, &C::Fb1))
		// 	);
		// 	EXPECT_EQ(
		// 		(is_corresponding_member<C, C, int32, int32>(&C::Fa, &C::Fb1)),
		// 		(IsCorrespondingMember<C, C, int32, int32>(&C::Fa, &C::Fb1))
		// 	);
	}

	TEST(TypeTraits, IsDestructible)
	{
		struct A
		{
			std::string F;

			~A() noexcept = default;
		};
		struct B
		{
			~B() = default;
		};

		EXPECT_EQ(
			is_destructible_v<void>,
			IsDestructible<void>::Value
		);
		EXPECT_EQ(
			is_destructible_v<int32>,
			IsDestructible<int32>::Value
		);
		EXPECT_EQ(
			is_destructible_v<int32&>,
			IsDestructible<int32&>::Value
		);
		EXPECT_EQ(
			is_destructible_v<int32*>,
			IsDestructible<int32*>::Value
		);
		EXPECT_EQ(
			is_destructible_v<int32[]>,
			IsDestructible<int32[]>::Value
		);
		EXPECT_EQ(
			is_destructible_v<int32()>,
			IsDestructible<int32()>::Value
		);
		EXPECT_EQ(
			is_destructible_v<A>,
			IsDestructible<A>::Value
		);
		EXPECT_EQ(
			is_destructible_v<B>,
			IsDestructible<B>::Value
		);

		EXPECT_EQ(
			IsDestructible<void>::Value,
			IsDestructibleValue<void>
		);
		EXPECT_EQ(
			IsDestructible<int32>::Value,
			IsDestructibleValue<int32>
		);
		EXPECT_EQ(
			IsDestructible<int32&>::Value,
			IsDestructibleValue<int32&>
		);
		EXPECT_EQ(
			IsDestructible<int32*>::Value,
			IsDestructibleValue<int32*>
		);
		EXPECT_EQ(
			IsDestructible<int32[]>::Value,
			IsDestructibleValue<int32[]>
		);
		EXPECT_EQ(
			IsDestructible<int32()>::Value,
			IsDestructibleValue<int32()>
		);
		EXPECT_EQ(
			IsDestructible<A>::Value,
			IsDestructibleValue<A>
		);
		EXPECT_EQ(
			IsDestructible<B>::Value,
			IsDestructibleValue<B>
		);
	}

	TEST(TypeTraits, IsDestructibleNoThrow)
	{
		struct A
		{
			std::string F;

			~A() noexcept = default;
		};
		struct B
		{
			~B() = default;
		};

		EXPECT_EQ(
			is_nothrow_destructible_v<void>,
			IsDestructibleNoThrow<void>::Value
		);
		EXPECT_EQ(
			is_nothrow_destructible_v<int32>,
			IsDestructibleNoThrow<int32>::Value
		);
		EXPECT_EQ(
			is_nothrow_destructible_v<int32&>,
			IsDestructibleNoThrow<int32&>::Value
		);
		EXPECT_EQ(
			is_nothrow_destructible_v<int32*>,
			IsDestructibleNoThrow<int32*>::Value
		);
		EXPECT_EQ(
			is_nothrow_destructible_v<int32[]>,
			IsDestructibleNoThrow<int32[]>::Value
		);
		EXPECT_EQ(
			is_nothrow_destructible_v<int32()>,
			IsDestructibleNoThrow<int32()>::Value
		);
		EXPECT_EQ(
			is_nothrow_destructible_v<A>,
			IsDestructibleNoThrow<A>::Value
		);
		EXPECT_EQ(
			is_nothrow_destructible_v<B>,
			IsDestructibleNoThrow<B>::Value
		);

		EXPECT_EQ(
			IsDestructibleNoThrow<void>::Value,
			IsDestructibleNoThrowValue<void>
		);
		EXPECT_EQ(
			IsDestructibleNoThrow<int32>::Value,
			IsDestructibleNoThrowValue<int32>
		);
		EXPECT_EQ(
			IsDestructibleNoThrow<int32&>::Value,
			IsDestructibleNoThrowValue<int32&>
		);
		EXPECT_EQ(
			IsDestructibleNoThrow<int32*>::Value,
			IsDestructibleNoThrowValue<int32*>
		);
		EXPECT_EQ(
			IsDestructibleNoThrow<int32[]>::Value,
			IsDestructibleNoThrowValue<int32[]>
		);
		EXPECT_EQ(
			IsDestructibleNoThrow<int32()>::Value,
			IsDestructibleNoThrowValue<int32()>
		);
		EXPECT_EQ(
			IsDestructibleNoThrow<A>::Value,
			IsDestructibleNoThrowValue<A>
		);
		EXPECT_EQ(
			IsDestructibleNoThrow<B>::Value,
			IsDestructibleNoThrowValue<B>
		);
	}

	TEST(TypeTraits, IsDestructibleTrivially)
	{
		struct A
		{
			std::string F;

			~A() noexcept = default;
		};
		struct B
		{
			~B() = default;
		};

		EXPECT_EQ(
			is_trivially_destructible_v<void>,
			IsDestructibleTrivially<void>::Value
		);
		EXPECT_EQ(
			is_trivially_destructible_v<int32>,
			IsDestructibleTrivially<int32>::Value
		);
		EXPECT_EQ(
			is_trivially_destructible_v<int32&>,
			IsDestructibleTrivially<int32&>::Value
		);
		EXPECT_EQ(
			is_trivially_destructible_v<int32*>,
			IsDestructibleTrivially<int32*>::Value
		);
		EXPECT_EQ(
			is_trivially_destructible_v<int32[]>,
			IsDestructibleTrivially<int32[]>::Value
		);
		EXPECT_EQ(
			is_trivially_destructible_v<int32()>,
			IsDestructibleTrivially<int32()>::Value
		);
		EXPECT_EQ(
			is_trivially_destructible_v<A>,
			IsDestructibleTrivially<A>::Value
		);
		EXPECT_EQ(
			is_trivially_destructible_v<B>,
			IsDestructibleTrivially<B>::Value
		);

		EXPECT_EQ(
			IsDestructibleTrivially<void>::Value,
			IsDestructibleTriviallyValue<void>
		);
		EXPECT_EQ(
			IsDestructibleTrivially<int32>::Value,
			IsDestructibleTriviallyValue<int32>
		);
		EXPECT_EQ(
			IsDestructibleTrivially<int32&>::Value,
			IsDestructibleTriviallyValue<int32&>
		);
		EXPECT_EQ(
			IsDestructibleTrivially<int32*>::Value,
			IsDestructibleTriviallyValue<int32*>
		);
		EXPECT_EQ(
			IsDestructibleTrivially<int32[]>::Value,
			IsDestructibleTriviallyValue<int32[]>
		);
		EXPECT_EQ(
			IsDestructibleTrivially<int32()>::Value,
			IsDestructibleTriviallyValue<int32()>
		);
		EXPECT_EQ(
			IsDestructibleTrivially<A>::Value,
			IsDestructibleTriviallyValue<A>
		);
		EXPECT_EQ(
			IsDestructibleTrivially<B>::Value,
			IsDestructibleTriviallyValue<B>
		);
	}

	TEST(TypeTraits, IsEmpty)
	{
		union UA {};

		union UB
		{
			int32 F1;
			float32 F2;
		};

		struct A {};

		struct B
		{
			int32 F;
		};

		EXPECT_EQ(
			is_empty_v<TrueConstant>,
			IsEmpty<TrueConstant>::Value
		);
		EXPECT_EQ(
			is_empty_v<UA>,
			IsEmpty<UA>::Value
		);
		EXPECT_EQ(
			is_empty_v<UB>,
			IsEmpty<UB>::Value
		);
		EXPECT_EQ(
			is_empty_v<A>,
			IsEmpty<A>::Value
		);
		EXPECT_EQ(
			is_empty_v<B>,
			IsEmpty<B>::Value
		);

		EXPECT_EQ(
			IsEmpty<TrueConstant>::Value,
			IsEmptyValue<TrueConstant>
		);
		EXPECT_EQ(
			IsEmpty<UA>::Value,
			IsEmptyValue<UA>
		);
		EXPECT_EQ(
			IsEmpty<UB>::Value,
			IsEmptyValue<UB>
		);
		EXPECT_EQ(
			IsEmpty<A>::Value,
			IsEmptyValue<A>
		);
		EXPECT_EQ(
			IsEmpty<B>::Value,
			IsEmptyValue<B>
		);
	}

	TEST(TypeTraits, IsEnum)
	{
		enum EA {};

		enum class EB {};

		struct A {};

		EXPECT_EQ(
			is_enum_v<int32>,
			IsEnum<int32>::Value
		);
		EXPECT_EQ(
			is_enum_v<EA>,
			IsEnum<EA>::Value
		);
		EXPECT_EQ(
			is_enum_v<EB>,
			IsEnum<EB>::Value
		);
		EXPECT_EQ(
			is_enum_v<A>,
			IsEnum<A>::Value
		);

		EXPECT_EQ(
			IsEnum<int32>::Value,
			IsEnumValue<int32>
		);
		EXPECT_EQ(
			IsEnum<EA>::Value,
			IsEnumValue<EA>
		);
		EXPECT_EQ(
			IsEnum<EB>::Value,
			IsEnumValue<EB>
		);
		EXPECT_EQ(
			IsEnum<A>::Value,
			IsEnumValue<A>
		);
	}

	TEST(TypeTraits, IsEnumClass)
	{
		enum EA {};

		enum class EB {};

		struct A {};

		EXPECT_EQ(
			(is_enum_v<EA> && is_convertible_v<EA, underlying_type_t<EA>>),
			IsEnumClass<EA>::Value
		);
		EXPECT_EQ(
			(is_enum_v<EB> && is_convertible_v<EB, underlying_type_t<EB>>),
			IsEnumClass<EB>::Value
		);

		EXPECT_EQ(
			IsEnumClass<int32>::Value,
			IsEnumClassValue<int32>
		);
		EXPECT_EQ(
			IsEnumClass<EA>::Value,
			IsEnumClassValue<EA>
		);
		EXPECT_EQ(
			IsEnumClass<EB>::Value,
			IsEnumClassValue<EB>
		);
		EXPECT_EQ(
			IsEnumClass<A>::Value,
			IsEnumClassValue<A>
		);
	}

	TEST(TypeTraits, IsFieldPointer)
	{
		struct A {};

		EXPECT_EQ(
			is_member_object_pointer_v<int32(A::*)>,
			IsFieldPointer<int32(A::*)>::Value
		);
		EXPECT_EQ(
			is_member_object_pointer_v<int32(A::*)()>,
			IsFieldPointer<int32(A::*)()>::Value
		);

		EXPECT_EQ(
			IsFieldPointer<int32(A::*)>::Value,
			IsFieldPointerValue<int32(A::*)>
		);
		EXPECT_EQ(
			IsFieldPointer<int32(A::*)()>::Value,
			IsFieldPointerValue<int32(A::*)()>
		);
	}

	TEST(TypeTraits, IsFinal)
	{
		struct A {};

		struct B final {};

		EXPECT_EQ(
			is_final_v<A>,
			IsFinal<A>::Value
		);
		EXPECT_EQ(
			is_final_v<B>,
			IsFinal<B>::Value
		);

		EXPECT_EQ(
			IsFinal<A>::Value,
			IsFinalValue<A>
		);
		EXPECT_EQ(
			IsFinal<B>::Value,
			IsFinalValue<B>
		);
	}

	TEST(TypeTraits, IsFloat)
	{
		EXPECT_FALSE(
			IsFloat<int32>::Value
		);
		EXPECT_TRUE(
			IsFloat<float32>::Value
		);
		EXPECT_TRUE(
			IsFloat<float64>::Value
		);
		EXPECT_TRUE(
			IsFloat<float128>::Value
		);
		EXPECT_FALSE(
			IsFloat<float32&>::Value
		);
		EXPECT_FALSE(
			IsFloat<float32*>::Value
		);
		EXPECT_FALSE(
			IsFloat<float32()>::Value
		);

		EXPECT_FALSE(
			IsFloatValue<int32>
		);
		EXPECT_TRUE(
			IsFloatValue<float32>
		);
		EXPECT_TRUE(
			IsFloatValue<float64>
		);
		EXPECT_TRUE(
			IsFloatValue<float128>
		);
		EXPECT_FALSE(
			IsFloatValue<float32&>
		);
		EXPECT_FALSE(
			IsFloatValue<float32*>
		);
		EXPECT_FALSE(
			IsFloatValue<float32()>
		);
	}

	TEST(TypeTraits, IsFunction)
	{
		void a();

		struct A
		{
			void M() const &;
		};

		EXPECT_EQ(
			is_function_v<int32>,
			IsFunction<int32>::Value
		);
		EXPECT_EQ(
			is_function_v<A>,
			IsFunction<A>::Value
		);
		EXPECT_EQ(
			is_function_v<int32(int32)>,
			IsFunction<int32(int32)>::Value
		);
		EXPECT_EQ(
			is_function_v<decltype(a)>,
			IsFunction<decltype(a)>::Value
		);

		EXPECT_EQ(
			IsFunction<int32>::Value,
			IsFunctionValue<int32>
		);
		EXPECT_EQ(
			IsFunction<A>::Value,
			IsFunctionValue<A>
		);
		EXPECT_EQ(
			IsFunction<int32(int32)>::Value,
			IsFunctionValue<int32(int32)>
		);
		EXPECT_EQ(
			IsFunction<decltype(a)>::Value,
			IsFunctionValue<decltype(a)>
		);
	}

	TEST(TypeTraits, IsInt)
	{
		EXPECT_TRUE(
			IsInt<int8>::Value
		);
		EXPECT_TRUE(
			IsInt<int16>::Value
		);
		EXPECT_TRUE(
			IsInt<int32>::Value
		);
		EXPECT_TRUE(
			IsInt<int64>::Value
		);
		EXPECT_TRUE(
			IsInt<intSize>::Value
		);
		EXPECT_TRUE(
			IsInt<intPointer>::Value
		);

		EXPECT_TRUE(
			IsInt<uint8>::Value
		);
		EXPECT_TRUE(
			IsInt<uint16>::Value
		);
		EXPECT_TRUE(
			IsInt<uint32>::Value
		);
		EXPECT_TRUE(
			IsInt<uint64>::Value
		);
		EXPECT_TRUE(
			IsInt<uintSize>::Value
		);
		EXPECT_TRUE(
			IsInt<uintPointer>::Value
		);

		EXPECT_FALSE(
			IsInt<float32>::Value
		);
		EXPECT_FALSE(
			IsInt<uint8&>::Value
		);
		EXPECT_FALSE(
			IsInt<uint16*>::Value
		);
		EXPECT_FALSE(
			IsInt<uint32()>::Value
		);
	}

	TEST(TypeTraits, IsInvocable)
	{
		int32 (*f(
			int8
		))();

		EXPECT_EQ(
			is_invocable_v<int32()>,
			IsInvocable<int32()>::Value
		);
		EXPECT_EQ(
			(is_invocable_v<int32(), int32>),
			(IsInvocable<int32(), int32>::Value)
		);
		EXPECT_EQ(
			(is_invocable_r_v<int32, int32()>),
			(IsInvocableReturn<int32, int32()>::Value)
		);
		EXPECT_EQ(
			(is_invocable_r_v<int32*, int32()>),
			(IsInvocableReturn<int32*, int32()>::Value)
		);
		EXPECT_EQ(
			(is_invocable_r_v<void, void(int32), int32>),
			(IsInvocableReturn<void, void(int32), int32>::Value)
		);
		EXPECT_EQ(
			(is_invocable_r_v<void, void(int32), void>),
			(IsInvocableReturn<void, void(int32), void>::Value)
		);
		EXPECT_EQ(
			(is_invocable_r_v<int32(*)(), decltype(f), int8>),
			(IsInvocableReturn<int32(*)(), decltype(f), int8>::Value)
		);
		EXPECT_EQ(
			(is_invocable_r_v<int32(*)(), decltype(f), void>),
			(IsInvocableReturn<int32(*)(), decltype(f), void>::Value)
		);

		EXPECT_EQ(
			IsInvocable<int32()>::Value,
			IsInvocableValue<int32()>
		);
		EXPECT_EQ(
			(IsInvocable<int32(), int32>::Value),
			(IsInvocableValue<int32(), int32>)
		);
		EXPECT_EQ(
			(IsInvocableReturn<int32, int32()>::Value),
			(IsInvocableReturnValue<int32, int32()>)
		);
		EXPECT_EQ(
			(IsInvocableReturn<int32*, int32()>::Value),
			(IsInvocableReturnValue<int32*, int32()>)
		);
		EXPECT_EQ(
			(IsInvocableReturn<void, void(int32), int32>::Value),
			(IsInvocableReturnValue<void, void(int32), int32>)
		);
		EXPECT_EQ(
			(IsInvocableReturn<void, void(int32), void>::Value),
			(IsInvocableReturnValue<void, void(int32), void>)
		);
		EXPECT_EQ(
			(IsInvocableReturn<int32(*)(), decltype(f), int8>::Value),
			(IsInvocableReturnValue<int32(*)(), decltype(f), int8>)
		);
		EXPECT_EQ(
			(IsInvocableReturn<int32(*)(), decltype(f), void>::Value),
			(IsInvocableReturnValue<int32(*)(), decltype(f), void>)
		);
	}

	TEST(TypeTraits, IsInvocableNoThrow)
	{
		int32 (*f(
			int8
		) noexcept)();

		EXPECT_EQ(
			is_nothrow_invocable_v<int32()>,
			IsInvocableNoThrow<int32()>::Value
		);
		EXPECT_EQ(
			(is_nothrow_invocable_v<int32(), int32>),
			(IsInvocableNoThrow<int32(), int32>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_invocable_r_v<int32, int32()>),
			(IsInvocableReturnNoThrow<int32, int32()>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_invocable_r_v<int32*, int32()>),
			(IsInvocableReturnNoThrow<int32*, int32()>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_invocable_r_v<void, void(int32), int32>),
			(IsInvocableReturnNoThrow<void, void(int32), int32>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_invocable_r_v<void, void(int32), void>),
			(IsInvocableReturnNoThrow<void, void(int32), void>::Value)
		);
		EXPECT_EQ(
			is_nothrow_invocable_v<int32() noexcept>,
			IsInvocableNoThrow<int32() noexcept>::Value
		);
		EXPECT_EQ(
			(is_nothrow_invocable_v<int32() noexcept, int32>),
			(IsInvocableNoThrow<int32() noexcept, int32>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_invocable_r_v<int32, int32() noexcept>),
			(IsInvocableReturnNoThrow<int32, int32() noexcept>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_invocable_r_v<int32*, int32() noexcept>),
			(IsInvocableReturnNoThrow<int32*, int32() noexcept>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_invocable_r_v<void, void(int32) noexcept, int32>),
			(IsInvocableReturnNoThrow<void, void(int32) noexcept, int32>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_invocable_r_v<void, void(int32) noexcept, void>),
			(IsInvocableReturnNoThrow<void, void(int32) noexcept, void>::Value)
		);
		EXPECT_EQ(
			(is_invocable_r_v<int32(*)(), decltype(f), int8>),
			(IsInvocableReturn<int32(*)(), decltype(f), int8>::Value)
		);
		EXPECT_EQ(
			(is_invocable_r_v<int32(*)(), decltype(f), void>),
			(IsInvocableReturn<int32(*)(), decltype(f), void>::Value)
		);

		EXPECT_EQ(
			IsInvocableNoThrow<int32()>::Value,
			IsInvocableNoThrowValue<int32()>
		);
		EXPECT_EQ(
			(IsInvocableNoThrow<int32(), int32>::Value),
			(IsInvocableNoThrowValue<int32(), int32>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<int32, int32()>::Value),
			(IsInvocableReturnNoThrowValue<int32, int32()>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<int32*, int32()>::Value),
			(IsInvocableReturnNoThrowValue<int32*, int32()>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<void, void(int32), int32>::Value),
			(IsInvocableReturnNoThrowValue<void, void(int32), int32>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<void, void(int32), void>::Value),
			(IsInvocableReturnNoThrowValue<void, void(int32), void>)
		);
		EXPECT_EQ(
			IsInvocableNoThrow<int32() noexcept>::Value,
			IsInvocableNoThrowValue<int32() noexcept>
		);
		EXPECT_EQ(
			(IsInvocableNoThrow<int32() noexcept, int32>::Value),
			(IsInvocableNoThrowValue<int32() noexcept, int32>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<int32, int32() noexcept>::Value),
			(IsInvocableReturnNoThrowValue<int32, int32() noexcept>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<int32*, int32() noexcept>::Value),
			(IsInvocableReturnNoThrowValue<int32*, int32() noexcept>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<void, void(int32) noexcept, int32>::Value),
			(IsInvocableReturnNoThrowValue<void, void(int32) noexcept, int32>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<void, void(int32) noexcept, void>::Value),
			(IsInvocableReturnNoThrowValue<void, void(int32) noexcept, void>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<int32(*)(), decltype(f), int8>::Value),
			(IsInvocableReturnNoThrowValue<int32(*)(), decltype(f), int8>)
		);
		EXPECT_EQ(
			(IsInvocableReturnNoThrow<int32(*)(), decltype(f), void>::Value),
			(IsInvocableReturnNoThrowValue<int32(*)(), decltype(f), void>)
		);
	}

	TEST(TypeTraits, IsLayoutCompatible)
	{
		struct A
		{
			int32 Fa1;
			int8 Fa2;
		};

		class B
		{
			const int32 Fb1 = 42;
			volatile int8 Fb2 = '*';
		};

		enum EA : int32 {};
		enum class EB : int32 {};

		EXPECT_EQ(
			(is_layout_compatible_v<const void, volatile void>),
			(IsLayoutCompatible<const void, volatile void>::Value)
		);
		EXPECT_EQ(
			(is_layout_compatible_v<A, B>),
			(IsLayoutCompatible<A, B>::Value)
		);
		EXPECT_EQ(
			(is_layout_compatible_v<A[2], B[2]>),
			(IsLayoutCompatible<A[2], B[2]>::Value)
		);
		EXPECT_EQ(
			(is_layout_compatible_v<int32, EA>),
			(IsLayoutCompatible<int32, EA>::Value)
		);
		EXPECT_EQ(
			(is_layout_compatible_v<EA, EB>),
			(IsLayoutCompatible<EA, EB>::Value)
		);
		EXPECT_EQ(
			(is_layout_compatible_v<long, unsigned long>),
			(IsLayoutCompatible<long, unsigned long>::Value)
		);
		EXPECT_EQ(
			(is_layout_compatible_v<int8*, const int8*>),
			(IsLayoutCompatible<int8*, const int8*>::Value)
		);
		EXPECT_EQ(
			(is_layout_compatible_v<int8*, int8* const>),
			(IsLayoutCompatible<int8*, int8* const>::Value)
		);
	}

	TEST(TypeTraits, IsLValueReference)
	{
		EXPECT_FALSE(
			IsLValueReference<int32>::Value
		);
		EXPECT_TRUE(
			IsLValueReference<int32&>::Value
		);
		EXPECT_FALSE(
			IsLValueReference<int32&&>::Value
		);
		EXPECT_FALSE(
			IsLValueReference<int32*>::Value
		);
		EXPECT_TRUE(
			IsLValueReference<int32*&>::Value
		);
		EXPECT_FALSE(
			IsLValueReference<int32*&&>::Value
		);

		EXPECT_EQ(
			IsLValueReference<int32>::Value,
			IsLValueReferenceValue<int32>
		);
		EXPECT_EQ(
			IsLValueReference<int32&>::Value,
			IsLValueReferenceValue<int32&>
		);
		EXPECT_EQ(
			IsLValueReference<int32&&>::Value,
			IsLValueReferenceValue<int32&&>
		);
		EXPECT_EQ(
			IsLValueReference<int32*>::Value,
			IsLValueReferenceValue<int32*>
		);
		EXPECT_EQ(
			IsLValueReference<int32*&>::Value,
			IsLValueReferenceValue<int32*&>
		);
		EXPECT_EQ(
			IsLValueReference<int32*&&>::Value,
			IsLValueReferenceValue<int32*&&>
		);
	}

	TEST(TypeTraits, IsMemberPointer)
	{
		struct A {};

		EXPECT_EQ(
			is_member_object_pointer_v<int32(A::*)> || is_member_function_pointer_v<int32(A::*)>,
			IsMemberPointer<int32(A::*)>::Value
		);
		EXPECT_EQ(
			is_member_object_pointer_v<int32(A::*)()> || is_member_function_pointer_v<int32(A::*)()>,
			IsMemberPointer<int32(A::*)()>::Value
		);

		EXPECT_EQ(
			IsMemberPointer<int32(A::*)>::Value,
			IsMemberPointerValue<int32(A::*)>
		);
		EXPECT_EQ(
			IsMemberPointer<int32(A::*)()>::Value,
			IsMemberPointerValue<int32(A::*)()>
		);
	}

	TEST(TypeTraits, IsMethodPointer)
	{
		struct A {};

		EXPECT_EQ(
			is_member_function_pointer_v<int32(A::*)>,
			IsMethodPointer<int32(A::*)>::Value
		);
		EXPECT_EQ(
			is_member_function_pointer_v<int32(A::*)()>,
			IsMethodPointer<int32(A::*)()>::Value
		);

		EXPECT_EQ(
			IsMethodPointer<int32(A::*)>::Value,
			IsMethodPointerValue<int32(A::*)>
		);
		EXPECT_EQ(
			IsMethodPointer<int32(A::*)()>::Value,
			IsMethodPointerValue<int32(A::*)()>
		);
	}

	TEST(TypeTraits, IsMoveAssignable)
	{
		struct A
		{
			A& operator=(
				A& other
			)
			{
				return *this;
			}
		};

		struct B
		{
			B& operator=(
				const B& other
			)
			{
				return *this;
			}
		};

		EXPECT_EQ(
			is_move_assignable_v<int32>,
			IsMoveAssignable<int32>::Value
		);
		EXPECT_EQ(
			is_move_assignable_v<int32*>,
			IsMoveAssignable<int32*>::Value
		);
		EXPECT_EQ(
			is_move_assignable_v<int32[2]>,
			IsMoveAssignable<int32[2]>::Value
		);
		EXPECT_EQ(
			is_move_assignable_v<A>,
			IsMoveAssignable<A>::Value
		);
		EXPECT_EQ(
			is_move_assignable_v<B>,
			IsMoveAssignable<B>::Value
		);

		EXPECT_EQ(
			IsMoveAssignable<int32>::Value,
			IsMoveAssignableValue<int32>
		);
		EXPECT_EQ(
			IsMoveAssignable<int32*>::Value,
			IsMoveAssignableValue<int32*>
		);
		EXPECT_EQ(
			IsMoveAssignable<int32[2]>::Value,
			IsMoveAssignableValue<int32[2]>
		);
		EXPECT_EQ(
			IsMoveAssignable<A>::Value,
			IsMoveAssignableValue<A>
		);
		EXPECT_EQ(
			IsMoveAssignable<B>::Value,
			IsMoveAssignableValue<B>
		);
	}

	TEST(TypeTraits, IsMoveAssignableNoThrow)
	{
		struct A
		{
			A& operator=(
				A& other
			)
			{
				return *this;
			}
		};

		struct B
		{
			B& operator=(
				const B& other
			) noexcept
			{
				return *this;
			}
		};

		EXPECT_EQ(
			is_nothrow_move_assignable_v<int32>,
			IsMoveAssignableNoThrow<int32>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_assignable_v<int32*>,
			IsMoveAssignableNoThrow<int32*>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_assignable_v<int32[2]>,
			IsMoveAssignableNoThrow<int32[2]>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_assignable_v<A>,
			IsMoveAssignableNoThrow<A>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_assignable_v<B>,
			IsMoveAssignableNoThrow<B>::Value
		);

		EXPECT_EQ(
			IsMoveAssignableNoThrow<int32>::Value,
			IsMoveAssignableNoThrowValue<int32>
		);
		EXPECT_EQ(
			IsMoveAssignableNoThrow<int32*>::Value,
			IsMoveAssignableNoThrowValue<int32*>
		);
		EXPECT_EQ(
			IsMoveAssignableNoThrow<int32[2]>::Value,
			IsMoveAssignableNoThrowValue<int32[2]>
		);
		EXPECT_EQ(
			IsMoveAssignableNoThrow<A>::Value,
			IsMoveAssignableNoThrowValue<A>
		);
		EXPECT_EQ(
			IsMoveAssignableNoThrow<B>::Value,
			IsMoveAssignableNoThrowValue<B>
		);
	}

	TEST(TypeTraits, IsMoveAssignableTrivially)
	{
		struct A
		{
			A& operator=(
				A& other
			)
			{
				return *this;
			}
		};

		struct B
		{
			B& operator=(
				const B& other
			)
			{
				return *this;
			}
		};

		struct C
		{
			int32 F;
		};

		EXPECT_EQ(
			is_trivially_move_assignable_v<int32>,
			IsMoveAssignableTrivially<int32>::Value
		);
		EXPECT_EQ(
			is_trivially_move_assignable_v<int32*>,
			IsMoveAssignableTrivially<int32*>::Value
		);
		EXPECT_EQ(
			is_trivially_move_assignable_v<int32[2]>,
			IsMoveAssignableTrivially<int32[2]>::Value
		);
		EXPECT_EQ(
			is_trivially_move_assignable_v<A>,
			IsMoveAssignableTrivially<A>::Value
		);
		EXPECT_EQ(
			is_trivially_move_assignable_v<B>,
			IsMoveAssignableTrivially<B>::Value
		);
		EXPECT_EQ(
			is_trivially_move_assignable_v<C>,
			IsMoveAssignableTrivially<C>::Value
		);

		EXPECT_EQ(
			IsMoveAssignableTrivially<int32>::Value,
			IsMoveAssignableTriviallyValue<int32>
		);
		EXPECT_EQ(
			IsMoveAssignableTrivially<int32*>::Value,
			IsMoveAssignableTriviallyValue<int32*>
		);
		EXPECT_EQ(
			IsMoveAssignableTrivially<int32[2]>::Value,
			IsMoveAssignableTriviallyValue<int32[2]>
		);
		EXPECT_EQ(
			IsMoveAssignableTrivially<A>::Value,
			IsMoveAssignableTriviallyValue<A>
		);
		EXPECT_EQ(
			IsMoveAssignableTrivially<B>::Value,
			IsMoveAssignableTriviallyValue<B>
		);
		EXPECT_EQ(
			IsMoveAssignableTrivially<C>::Value,
			IsMoveAssignableTriviallyValue<C>
		);
	}

	TEST(TypeTraits, IsMoveConstructible)
	{
		struct A
		{
			A(
				A& other
			) {}
		};

		struct B
		{
			B(
				const B& other
			) {}
		};

		struct C
		{
			C(
				C&& other
			) = default;
		};

		EXPECT_EQ(
			is_move_constructible_v<int32>,
			IsMoveConstructible<int32>::Value
		);
		EXPECT_EQ(
			is_move_constructible_v<int32*>,
			IsMoveConstructible<int32*>::Value
		);
		EXPECT_EQ(
			is_move_constructible_v<int32[2]>,
			IsMoveConstructible<int32[2]>::Value
		);
		EXPECT_EQ(
			is_move_constructible_v<A>,
			IsMoveConstructible<A>::Value
		);
		EXPECT_EQ(
			is_move_constructible_v<B>,
			IsMoveConstructible<B>::Value
		);
		EXPECT_EQ(
			is_move_constructible_v<C>,
			IsMoveConstructible<C>::Value
		);

		EXPECT_EQ(
			IsMoveConstructible<int32>::Value,
			IsMoveConstructibleValue<int32>
		);
		EXPECT_EQ(
			IsMoveConstructible<int32*>::Value,
			IsMoveConstructibleValue<int32*>
		);
		EXPECT_EQ(
			IsMoveConstructible<int32[2]>::Value,
			IsMoveConstructibleValue<int32[2]>
		);
		EXPECT_EQ(
			IsMoveConstructible<A>::Value,
			IsMoveConstructibleValue<A>
		);
		EXPECT_EQ(
			IsMoveConstructible<B>::Value,
			IsMoveConstructibleValue<B>
		);
		EXPECT_EQ(
			IsMoveConstructible<C>::Value,
			IsMoveConstructibleValue<C>
		);
	}

	TEST(TypeTraits, IsMoveConstructibleNoThrow)
	{
		struct A
		{
			A(
				A& other
			) {}
		};

		struct B
		{
			B(
				const B& other
			) {}
		};

		struct C
		{
			C(
				C&& other
			) = default;
		};

		EXPECT_EQ(
			is_nothrow_move_constructible_v<int32>,
			IsMoveConstructibleNoThrow<int32>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_constructible_v<int32*>,
			IsMoveConstructibleNoThrow<int32*>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_constructible_v<int32[2]>,
			IsMoveConstructibleNoThrow<int32[2]>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_constructible_v<A>,
			IsMoveConstructibleNoThrow<A>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_constructible_v<B>,
			IsMoveConstructibleNoThrow<B>::Value
		);
		EXPECT_EQ(
			is_nothrow_move_constructible_v<C>,
			IsMoveConstructibleNoThrow<C>::Value
		);

		EXPECT_EQ(
			IsMoveConstructibleNoThrow<int32>::Value,
			IsMoveConstructibleNoThrowValue<int32>
		);
		EXPECT_EQ(
			IsMoveConstructibleNoThrow<int32*>::Value,
			IsMoveConstructibleNoThrowValue<int32*>
		);
		EXPECT_EQ(
			IsMoveConstructibleNoThrow<int32[2]>::Value,
			IsMoveConstructibleNoThrowValue<int32[2]>
		);
		EXPECT_EQ(
			IsMoveConstructibleNoThrow<A>::Value,
			IsMoveConstructibleNoThrowValue<A>
		);
		EXPECT_EQ(
			IsMoveConstructibleNoThrow<B>::Value,
			IsMoveConstructibleNoThrowValue<B>
		);
		EXPECT_EQ(
			IsMoveConstructibleNoThrow<C>::Value,
			IsMoveConstructibleNoThrowValue<C>
		);
	}

	TEST(TypeTraits, IsMoveConstructibleTrivially)
	{
		struct A
		{
			A(
				A& other
			) {}
		};

		struct B
		{
			B(
				const B& other
			) {}
		};

		struct C
		{
			C(
				C&& other
			) = default;
		};

		struct D
		{
			string F;
		};

		EXPECT_EQ(
			is_trivially_move_constructible_v<int32>,
			IsMoveConstructibleTrivially<int32>::Value
		);
		EXPECT_EQ(
			is_trivially_move_constructible_v<int32*>,
			IsMoveConstructibleTrivially<int32*>::Value
		);
		EXPECT_EQ(
			is_trivially_move_constructible_v<int32[2]>,
			IsMoveConstructibleTrivially<int32[2]>::Value
		);
		EXPECT_EQ(
			is_trivially_move_constructible_v<A>,
			IsMoveConstructibleTrivially<A>::Value
		);
		EXPECT_EQ(
			is_trivially_move_constructible_v<B>,
			IsMoveConstructibleTrivially<B>::Value
		);
		EXPECT_EQ(
			is_trivially_move_constructible_v<C>,
			IsMoveConstructibleTrivially<C>::Value
		);
		EXPECT_EQ(
			is_trivially_move_constructible_v<D>,
			IsMoveConstructibleTrivially<D>::Value
		);

		EXPECT_EQ(
			IsMoveConstructibleTrivially<int32>::Value,
			IsMoveConstructibleTriviallyValue<int32>
		);
		EXPECT_EQ(
			IsMoveConstructibleTrivially<int32*>::Value,
			IsMoveConstructibleTriviallyValue<int32*>
		);
		EXPECT_EQ(
			IsMoveConstructibleTrivially<int32[2]>::Value,
			IsMoveConstructibleTriviallyValue<int32[2]>
		);
		EXPECT_EQ(
			IsMoveConstructibleTrivially<A>::Value,
			IsMoveConstructibleTriviallyValue<A>
		);
		EXPECT_EQ(
			IsMoveConstructibleTrivially<B>::Value,
			IsMoveConstructibleTriviallyValue<B>
		);
		EXPECT_EQ(
			IsMoveConstructibleTrivially<C>::Value,
			IsMoveConstructibleTriviallyValue<C>
		);
		EXPECT_EQ(
			IsMoveConstructibleTrivially<D>::Value,
			IsMoveConstructibleTriviallyValue<D>
		);
	}

	TEST(TypeTraits, IsNull)
	{
		EXPECT_TRUE(
			IsNull<NullType>::Value
		);
		EXPECT_FALSE(
			IsNull<NullType*>::Value
		);

		EXPECT_EQ(
			IsNull<NullType>::Value,
			IsNullValue<NullType>
		);
		EXPECT_EQ(
			IsNull<NullType*>::Value,
			IsNullValue<NullType*>
		);
	}

	TEST(TypeTraits, IsNumber)
	{
		EXPECT_TRUE(
			IsNumeric<int32>::Value
		);
		EXPECT_TRUE(
			IsNumeric<float32>::Value
		);
		EXPECT_FALSE(
			IsNumeric<char8>::Value
		);

		EXPECT_EQ(
			IsNumeric<int32>::Value,
			IsNumericValue<int32>
		);
		EXPECT_EQ(
			IsNumeric<float32>::Value,
			IsNumericValue<float32>
		);
		EXPECT_EQ(
			IsNumeric<char8>::Value,
			IsNumericValue<char8>
		);
	}

	TEST(TypeTraits, IsObject)
	{
		EXPECT_EQ(
			is_object_v<void>,
			IsObject<void>::Value
		);
		EXPECT_EQ(
			is_object_v<int32>,
			IsObject<int32>::Value
		);
		EXPECT_EQ(
			is_object_v<int32&>,
			IsObject<int32&>::Value
		);
		EXPECT_EQ(
			is_object_v<int32*>,
			IsObject<int32*>::Value
		);
		EXPECT_EQ(
			is_object_v<int32[2]>,
			IsObject<int32[2]>::Value
		);
		EXPECT_EQ(
			is_object_v<int32()>,
			IsObject<int32()>::Value
		);

		EXPECT_EQ(
			IsObject<void>::Value,
			IsObjectValue<void>
		);
		EXPECT_EQ(
			IsObject<int32>::Value,
			IsObjectValue<int32>
		);
		EXPECT_EQ(
			IsObject<int32&>::Value,
			IsObjectValue<int32&>
		);
		EXPECT_EQ(
			IsObject<int32*>::Value,
			IsObjectValue<int32*>
		);
		EXPECT_EQ(
			IsObject<int32[2]>::Value,
			IsObjectValue<int32[2]>
		);
		EXPECT_EQ(
			IsObject<int32()>::Value,
			IsObjectValue<int32()>
		);
	}

	TEST(TypeTraits, IsParent)
	{
		struct A {};

		struct B {};

		struct C : A {};

		struct D : B, C {};

		EXPECT_EQ(
			(is_base_of_v<A, B>),
			(IsParent<A, B>::Value)
		);
		EXPECT_EQ(
			(is_base_of_v<A, C>),
			(IsParent<A, C>::Value)
		);
		EXPECT_EQ(
			(is_base_of_v<D, A>),
			(IsParent<D, A>::Value)
		);
		EXPECT_EQ(
			(is_base_of_v<D, B>),
			(IsParent<D, B>::Value)
		);
		EXPECT_EQ(
			(is_base_of_v<D, C>),
			(IsParent<D, C>::Value)
		);

		EXPECT_EQ(
			(IsParent<A, B>::Value),
			(IsParentValue<A, B>)
		);
		EXPECT_EQ(
			(IsParent<A, C>::Value),
			(IsParentValue<A, C>)
		);
		EXPECT_EQ(
			(IsParent<D, A>::Value),
			(IsParentValue<D, A>)
		);
		EXPECT_EQ(
			(IsParent<D, B>::Value),
			(IsParentValue<D, B>)
		);
		EXPECT_EQ(
			(IsParent<D, C>::Value),
			(IsParentValue<D, C>)
		);
	}

	TEST(TypeTraits, IsPointer)
	{
		EXPECT_EQ(
			is_pointer_v<int32>,
			IsPointer<int32>::Value
		);
		EXPECT_EQ(
			is_pointer_v<int32&>,
			IsPointer<int32&>::Value
		);
		EXPECT_EQ(
			is_pointer_v<int32*>,
			IsPointer<int32*>::Value
		);
		EXPECT_EQ(
			is_pointer_v<int32[2]>,
			IsPointer<int32[2]>::Value
		);
		EXPECT_EQ(
			is_pointer_v<int32(*)()>,
			IsPointer<int32(*)()>::Value
		);

		EXPECT_EQ(
			IsPointer<int32>::Value,
			IsPointerValue<int32>
		);
		EXPECT_EQ(
			IsPointer<int32&>::Value,
			IsPointerValue<int32&>
		);
		EXPECT_EQ(
			IsPointer<int32*>::Value,
			IsPointerValue<int32*>
		);
		EXPECT_EQ(
			IsPointer<int32[2]>::Value,
			IsPointerValue<int32[2]>
		);
		EXPECT_EQ(
			IsPointer<int32(*)()>::Value,
			IsPointerValue<int32(*)()>
		);
	}

	// Todo: IsPointerInterconvertibleClass
	TEST(TypeTraits, IsPointerInterconvertibleClass)
	{
		GTEST_SKIP();
		// 	struct A
		// 	{
		// 		int32 Fa;
		// 	};
		// 	struct B
		// 	{
		// 		int32 Fb;
		// 	};
		//
		// 	struct C : A, B { };
		//
		// 	EXPECT_EQ(
		// 		(is_pointer_interconvertible_with_class(&C::Fa)),
		// 		(IsPointerInterconvertibleClass(&C::Fa))
		// 	);
		// 	EXPECT_EQ(
		// 		(is_pointer_interconvertible_with_class<C, int32>(&C::Fa)),
		// 		(IsPointerInterconvertibleClass<C, int32>(&C::Fa))
		// 	);
	}

	TEST(TypeTraits, IsPointerInterconvertibleParent)
	{
		struct A {};

		struct B {};

		struct C : A, B
		{
			int32 Fc;
		};

		struct D : C
		{
			int32 Fd;
		};

		EXPECT_EQ(
			(is_pointer_interconvertible_base_of_v<B, C>),
			(IsPointerInterconvertibleParent<B, C>::Value)
		);
		EXPECT_EQ(
			(is_pointer_interconvertible_base_of_v<A, C>),
			(IsPointerInterconvertibleParent<A, C>::Value)
		);
		EXPECT_EQ(
			(is_pointer_interconvertible_base_of_v<C, D>),
			(IsPointerInterconvertibleParent<C, D>::Value)
		);
		EXPECT_EQ(
			(is_pointer_interconvertible_base_of_v<D, D>),
			(IsPointerInterconvertibleParent<D, D>::Value)
		);
	}

	TEST(TypeTraits, IsPolymorphic)
	{
		struct A
		{
			int32 Fa;
		};

		struct B
		{
			void Mb();
		};

		struct C : B {};

		struct D
		{
			virtual void Md() {}
		};

		struct E : D
		{
			virtual ~E() = default;
		};

		EXPECT_EQ(
			is_polymorphic_v<A>,
			IsPolymorphic<A>::Value
		);
		EXPECT_EQ(
			is_polymorphic_v<B>,
			IsPolymorphic<B>::Value
		);
		EXPECT_EQ(
			is_polymorphic_v<C>,
			IsPolymorphic<C>::Value
		);
		EXPECT_EQ(
			is_polymorphic_v<D>,
			IsPolymorphic<D>::Value
		);
		EXPECT_EQ(
			is_polymorphic_v<E>,
			IsPolymorphic<E>::Value
		);

		EXPECT_EQ(
			IsPolymorphic<A>::Value,
			IsPolymorphicValue<A>
		);
		EXPECT_EQ(
			IsPolymorphic<B>::Value,
			IsPolymorphicValue<B>
		);
		EXPECT_EQ(
			IsPolymorphic<C>::Value,
			IsPolymorphicValue<C>
		);
		EXPECT_EQ(
			IsPolymorphic<D>::Value,
			IsPolymorphicValue<D>
		);
		EXPECT_EQ(
			IsPolymorphic<E>::Value,
			IsPolymorphicValue<E>
		);
	}

	TEST(TypeTraits, IsPrimitive)
	{
		enum EA {};

		enum class EB {};

		union UA {};

		struct A {};

		class B {};

		EXPECT_TRUE(
			IsPrimitive<void>::Value
		);
		EXPECT_TRUE(
			IsPrimitive<int32>::Value
		);
		EXPECT_TRUE(
			IsPrimitive<float32>::Value
		);
		EXPECT_TRUE(
			IsPrimitive<char8>::Value
		);
		EXPECT_TRUE(
			IsPrimitive<NullType>::Value
		);
		EXPECT_FALSE(
			IsPrimitive<int32&>::Value
		);
		EXPECT_FALSE(
			IsPrimitive<int32*>::Value
		);
		EXPECT_FALSE(
			IsPrimitive<EA>::Value
		);
		EXPECT_FALSE(
			IsPrimitive<EB>::Value
		);
		EXPECT_FALSE(
			IsPrimitive<UA>::Value
		);
		EXPECT_FALSE(
			IsPrimitive<A>::Value
		);
		EXPECT_FALSE(
			IsPrimitive<B>::Value
		);

		EXPECT_EQ(
			IsPrimitive<void>::Value,
			IsPrimitiveValue<void>
		);
		EXPECT_EQ(
			IsPrimitive<int32>::Value,
			IsPrimitiveValue<int32>
		);
		EXPECT_EQ(
			IsPrimitive<float32>::Value,
			IsPrimitiveValue<float32>
		);
		EXPECT_EQ(
			IsPrimitive<char8>::Value,
			IsPrimitiveValue<char8>
		);
		EXPECT_EQ(
			IsPrimitive<NullType>::Value,
			IsPrimitiveValue<NullType>
		);
		EXPECT_EQ(
			IsPrimitive<int32&>::Value,
			IsPrimitiveValue<int32&>
		);
		EXPECT_EQ(
			IsPrimitive<int32*>::Value,
			IsPrimitiveValue<int32*>
		);
		EXPECT_EQ(
			IsPrimitive<EA>::Value,
			IsPrimitiveValue<EA>
		);
		EXPECT_EQ(
			IsPrimitive<EB>::Value,
			IsPrimitiveValue<EB>
		);
		EXPECT_EQ(
			IsPrimitive<UA>::Value,
			IsPrimitiveValue<UA>
		);
		EXPECT_EQ(
			IsPrimitive<A>::Value,
			IsPrimitiveValue<A>
		);
		EXPECT_EQ(
			IsPrimitive<B>::Value,
			IsPrimitiveValue<B>
		);
	}

	TEST(TypeTraits, IsReference)
	{
		EXPECT_EQ(
			is_reference_v<int32>,
			IsReference<int32>::Value
		);
		EXPECT_EQ(
			is_reference_v<int32&>,
			IsReference<int32&>::Value
		);
		EXPECT_EQ(
			is_reference_v<int32&&>,
			IsReference<int32&&>::Value
		);
		EXPECT_EQ(
			is_reference_v<int32*>,
			IsReference<int32*>::Value
		);
		EXPECT_EQ(
			is_reference_v<int32()>,
			IsReference<int32()>::Value
		);
		EXPECT_EQ(
			is_reference_v<int32(&)()>,
			IsReference<int32(&)()>::Value
		);

		EXPECT_EQ(
			IsReference<int32>::Value,
			IsReferenceValue<int32>
		);
		EXPECT_EQ(
			IsReference<int32&>::Value,
			IsReferenceValue<int32&>
		);
		EXPECT_EQ(
			IsReference<int32&&>::Value,
			IsReferenceValue<int32&&>
		);
		EXPECT_EQ(
			IsReference<int32*>::Value,
			IsReferenceValue<int32*>
		);
		EXPECT_EQ(
			IsReference<int32()>::Value,
			IsReferenceValue<int32()>
		);
		EXPECT_EQ(
			IsReference<int32(&)()>::Value,
			IsReferenceValue<int32(&)()>
		);
	}

	TEST(TypeTraits, IsRValueReference)
	{
		EXPECT_FALSE(
			IsRValueReference<int32>::Value
		);
		EXPECT_FALSE(
			IsRValueReference<int32&>::Value
		);
		EXPECT_TRUE(
			IsRValueReference<int32&&>::Value
		);
		EXPECT_FALSE(
			IsRValueReference<int32*>::Value
		);
		EXPECT_FALSE(
			IsRValueReference<int32*&>::Value
		);
		EXPECT_TRUE(
			IsRValueReference<int32*&&>::Value
		);

		EXPECT_EQ(
			IsRValueReference<int32>::Value,
			IsRValueReferenceValue<int32>
		);
		EXPECT_EQ(
			IsRValueReference<int32&>::Value,
			IsRValueReferenceValue<int32&>
		);
		EXPECT_EQ(
			IsRValueReference<int32&&>::Value,
			IsRValueReferenceValue<int32&&>
		);
		EXPECT_EQ(
			IsRValueReference<int32*>::Value,
			IsRValueReferenceValue<int32*>
		);
		EXPECT_EQ(
			IsRValueReference<int32*&>::Value,
			IsRValueReferenceValue<int32*&>
		);
		EXPECT_EQ(
			IsRValueReference<int32*&&>::Value,
			IsRValueReferenceValue<int32*&&>
		);
	}

	TEST(TypeTraits, IsSameBase)
	{
		#define remove_all(T) remove_cv_t<remove_pointer_t<remove_reference_t<T>>>

		EXPECT_EQ(
			(is_same_v<int32, remove_all(int32)>),
			(IsSameBase<int32, int32>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, remove_all(const int64)>),
			(IsSameBase<int32, const int64>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, remove_all(float32&)>),
			(IsSameBase<int32, float32&>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, remove_all(char16*)>),
			(IsSameBase<int32, char16*>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, remove_all(int32*&)>),
			(IsSameBase<int32, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsSameBase<int32, int32>::Value),
			(IsSameBaseValue<int32, int32>)
		);
		EXPECT_EQ(
			(IsSameBase<int32, const int64>::Value),
			(IsSameBaseValue<int32, const int64>)
		);
		EXPECT_EQ(
			(IsSameBase<int32, float32&>::Value),
			(IsSameBaseValue<int32, float32&>)
		);
		EXPECT_EQ(
			(IsSameBase<int32, char16*>::Value),
			(IsSameBaseValue<int32, char16*>)
		);
		EXPECT_EQ(
			(IsSameBase<int32, int32*&>::Value),
			(IsSameBaseValue<int32, int32*&>)
		);

		#undef remove_all
	}

	TEST(TypeTraits, IsSameStrong)
	{
		EXPECT_EQ(
			(is_same_v<int32, int32>),
			(IsSameBase<int32, int32>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, const int64>),
			(IsSameStrong<int32, const int64>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, float32&>),
			(IsSameStrong<int32, float32&>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, char16*>),
			(IsSameStrong<int32, char16*>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, int32*&>),
			(IsSameStrong<int32, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsSameStrong<int32, int32>::Value),
			(IsSameStrongValue<int32, int32>)
		);
		EXPECT_EQ(
			(IsSameStrong<int32, const int64>::Value),
			(IsSameStrongValue<int32, const int64>)
		);
		EXPECT_EQ(
			(IsSameStrong<int32, float32&>::Value),
			(IsSameStrongValue<int32, float32&>)
		);
		EXPECT_EQ(
			(IsSameStrong<int32, char16*>::Value),
			(IsSameStrongValue<int32, char16*>)
		);
		EXPECT_EQ(
			(IsSameStrong<int32, int32*&>::Value),
			(IsSameStrongValue<int32, int32*&>)
		);
	}

	TEST(TypeTraits, IsSameWeak)
	{
		EXPECT_EQ(
			(is_same_v<int32, remove_cv_t<int32>>),
			(IsSameWeak<int32, int32>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, remove_cv_t<const int64>>),
			(IsSameWeak<int32, const int64>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, remove_cv_t<float32&>>),
			(IsSameWeak<int32, float32&>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, remove_cv_t<char16*>>),
			(IsSameWeak<int32, char16*>::Value)
		);
		EXPECT_EQ(
			(is_same_v<int32, remove_cv_t<int32*&>>),
			(IsSameWeak<int32, int32*&>::Value)
		);

		EXPECT_EQ(
			(IsSameWeak<int32, int32>::Value),
			(IsSameWeakValue<int32, int32>)
		);
		EXPECT_EQ(
			(IsSameWeak<int32, const int64>::Value),
			(IsSameWeakValue<int32, const int64>)
		);
		EXPECT_EQ(
			(IsSameWeak<int32, float32&>::Value),
			(IsSameWeakValue<int32, float32&>)
		);
		EXPECT_EQ(
			(IsSameWeak<int32, char16*>::Value),
			(IsSameWeakValue<int32, char16*>)
		);
		EXPECT_EQ(
			(IsSameWeak<int32, int32*&>::Value),
			(IsSameWeakValue<int32, int32*&>)
		);
	}

	TEST(TypeTraits, IsScalar)
	{
		enum EA {};

		enum class EB {};

		struct A {};

		EXPECT_EQ(
			!is_scalar_v<char8>,
			IsScalar<char8>::Value
		);
		EXPECT_EQ(
			is_scalar_v<int32>,
			IsScalar<int32>::Value
		);
		EXPECT_EQ(
			is_scalar_v<int32&>,
			IsScalar<int32&>::Value
		);
		EXPECT_EQ(
			is_scalar_v<int32*>,
			IsScalar<int32*>::Value
		);
		EXPECT_EQ(
			is_scalar_v<int32 A::*>,
			IsScalar<int32 A::*>::Value
		);
		EXPECT_EQ(
			is_scalar_v<EA>,
			IsScalar<EA>::Value
		);
		EXPECT_EQ(
			is_scalar_v<EB>,
			IsScalar<EB>::Value
		);
		EXPECT_EQ(
			is_scalar_v<A>,
			IsScalar<A>::Value
		);

		EXPECT_EQ(
			IsScalar<char8>::Value,
			IsScalarValue<char8>
		);
		EXPECT_EQ(
			IsScalar<int32>::Value,
			IsScalarValue<int32>
		);
		EXPECT_EQ(
			IsScalar<int32&>::Value,
			IsScalarValue<int32&>
		);
		EXPECT_EQ(
			IsScalar<int32*>::Value,
			IsScalarValue<int32*>
		);
		EXPECT_EQ(
			IsScalar<int32 A::*>::Value,
			IsScalarValue<int32 A::*>
		);
		EXPECT_EQ(
			IsScalar<EA>::Value,
			IsScalarValue<EA>
		);
		EXPECT_EQ(
			IsScalar<EB>::Value,
			IsScalarValue<EB>
		);
		EXPECT_EQ(
			IsScalar<A>::Value,
			IsScalarValue<A>
		);
	}

	TEST(TypeTraits, IsSigned)
	{
		EXPECT_TRUE(
			IsSigned<int32>::Value
		);
		EXPECT_FALSE(
			IsSigned<uint32>::Value
		);
		EXPECT_FALSE(
			IsSigned<float32>::Value
		);
		EXPECT_FALSE(
			IsSigned<char8>::Value
		);

		EXPECT_EQ(
			IsSigned<int32>::Value,
			IsSignedValue<int32>
		);
		EXPECT_EQ(
			IsSigned<uint32>::Value,
			IsSignedValue<uint32>
		);
		EXPECT_EQ(
			IsSigned<float32>::Value,
			IsSignedValue<float32>
		);
		EXPECT_EQ(
			IsSigned<char8>::Value,
			IsSignedValue<char8>
		);
	}

	TEST(TypeTraits, IsStandardLayout)
	{
		struct A
		{
			int32 Fa;
		};

		struct B
		{
			int32 Fb;

		private:
			int32 _fb;
		};

		struct C
		{
			virtual void M() {}
		};

		EXPECT_EQ(
			is_standard_layout_v<A>,
			IsStandardLayout<A>::Value
		);
		EXPECT_EQ(
			is_standard_layout_v<B>,
			IsStandardLayout<B>::Value
		);
		EXPECT_EQ(
			is_standard_layout_v<C>,
			IsStandardLayout<C>::Value
		);
	}

	TEST(TypeTraits, IsSwappable)
	{
		struct A {};

		struct B
		{
			B(
				const B& other
			) = delete;
		};

		struct C
		{
			C(
				const C& other
			) noexcept = delete;

			C& operator=(
				const C& other
			) noexcept = delete;
		};

		EXPECT_EQ(
			is_swappable_v<int32>,
			IsSwappable<int32>::Value
		);
		EXPECT_EQ(
			is_swappable_v<int32[2]>,
			IsSwappable<int32[2]>::Value
		);
		EXPECT_EQ(
			is_swappable_v<A>,
			IsSwappable<A>::Value
		);
		EXPECT_EQ(
			is_swappable_v<B>,
			IsSwappable<B>::Value
		);
		EXPECT_EQ(
			is_swappable_v<C>,
			IsSwappable<C>::Value
		);

		EXPECT_EQ(
			IsSwappable<int32>::Value,
			IsSwappableValue<int32>
		);
		EXPECT_EQ(
			IsSwappable<int32[]>::Value,
			IsSwappableValue<int32[]>
		);
		EXPECT_EQ(
			IsSwappable<A>::Value,
			IsSwappableValue<A>
		);
		EXPECT_EQ(
			IsSwappable<B>::Value,
			IsSwappableValue<B>
		);
		EXPECT_EQ(
			IsSwappable<C>::Value,
			IsSwappableValue<C>
		);
	}

	TEST(TypeTraits, IsSwappableNoThrow)
	{
		struct A {};


		struct B
		{
			B(
				const B& other
			);
		};


		struct C
		{
			C(
				const C& other
			) noexcept;

			C& operator=(
				const C& other
			) noexcept;
		};

		EXPECT_EQ(
			is_nothrow_swappable_v<int32>,
			IsSwappableNoThrow<int32>::Value
		);
		EXPECT_EQ(
			is_nothrow_swappable_v<int32[2]>,
			IsSwappableNoThrow<int32[2]>::Value
		);
		EXPECT_EQ(
			is_nothrow_swappable_v<A>,
			IsSwappableNoThrow<A>::Value
		);
		EXPECT_EQ(
			is_nothrow_swappable_v<B>,
			IsSwappableNoThrow<B>::Value
		);
		EXPECT_EQ(
			is_nothrow_swappable_v<C>,
			IsSwappableNoThrow<C>::Value
		);

		EXPECT_EQ(
			IsSwappableNoThrow<int32>::Value,
			IsSwappableNoThrowValue<int32>
		);
		EXPECT_EQ(
			IsSwappableNoThrow<int32[2]>::Value,
			IsSwappableNoThrowValue<int32[2]>
		);
		EXPECT_EQ(
			IsSwappableNoThrow<A>::Value,
			IsSwappableNoThrowValue<A>
		);
		EXPECT_EQ(
			IsSwappableNoThrow<B>::Value,
			IsSwappableNoThrowValue<B>
		);
		EXPECT_EQ(
			IsSwappableNoThrow<C>::Value,
			IsSwappableNoThrowValue<C>
		);
	}

	TEST(TypeTraits, IsSwappableWith)
	{
		struct A {};


		struct B
		{
			B(
				const B& other
			);
		};


		struct C
		{
			C(
				const C& other
			) noexcept;

			C& operator=(
				const C& other
			) noexcept;
		};


		#pragma warning(push)
		#pragma warning(disable: 5046)

		constexpr void swap(
			A& a,
			B& b
		);

		constexpr void swap(
			B& a,
			A& b
		);

		constexpr void swap(
			B& a,
			C& b
		) noexcept;

		constexpr void swap(
			C& a,
			B& b
		) noexcept;

		constexpr void Swap(
			A& a,
			B& b
		);

		constexpr void Swap(
			B& a,
			A& b
		);

		constexpr void Swap(
			B& a,
			C& b
		) noexcept;

		constexpr void Swap(
			C& a,
			B& b
		) noexcept;

		#pragma warning(pop)

		EXPECT_EQ(
			(is_swappable_with_v<int32, int32>),
			(IsSwappableWith<int32, int32>::Value)
		);
		EXPECT_EQ(
			(is_swappable_with_v<int32&, int32&>),
			(IsSwappableWith<int32&, int32&>::Value)
		);
		EXPECT_EQ(
			(is_swappable_with_v<int32&, float32&>),
			(IsSwappableWith<int32&, float32&>::Value)
		);
		EXPECT_EQ(
			(is_swappable_with_v<A&, B&>),
			(IsSwappableWith<A&, B&>::Value)
		);
		EXPECT_EQ(
			(is_swappable_with_v<B&, A&>),
			(IsSwappableWith<B&, A&>::Value)
		);
		EXPECT_EQ(
			(is_swappable_with_v<A&, C&>),
			(IsSwappableWith<A&, C&>::Value)
		);
		EXPECT_EQ(
			(is_swappable_with_v<C&, A&>),
			(IsSwappableWith<C&, A&>::Value)
		);
		EXPECT_EQ(
			(is_swappable_with_v<B&, C&>),
			(IsSwappableWith<B&, C&>::Value)
		);
		EXPECT_EQ(
			(is_swappable_with_v<C&, B&>),
			(IsSwappableWith<C&, B&>::Value)
		);

		EXPECT_EQ(
			(IsSwappableWith<int32, int32>::Value),
			(IsSwappableWithValue<int32, int32>)
		);
		EXPECT_EQ(
			(IsSwappableWith<int32&, int32&>::Value),
			(IsSwappableWithValue<int32&, int32&>)
		);
		EXPECT_EQ(
			(IsSwappableWith<int32&, float32&>::Value),
			(IsSwappableWithValue<int32&, float32&>)
		);
		EXPECT_EQ(
			(IsSwappableWith<A&, B&>::Value),
			(IsSwappableWithValue<A&, B&>)
		);
		EXPECT_EQ(
			(IsSwappableWith<B&, A&>::Value),
			(IsSwappableWithValue<B&, A&>)
		);
		EXPECT_EQ(
			(IsSwappableWith<A&, C&>::Value),
			(IsSwappableWithValue<A&, C&>)
		);
		EXPECT_EQ(
			(IsSwappableWith<C&, A&>::Value),
			(IsSwappableWithValue<C&, A&>)
		);
		EXPECT_EQ(
			(IsSwappableWith<B&, C&>::Value),
			(IsSwappableWithValue<B&, C&>)
		);
		EXPECT_EQ(
			(IsSwappableWith<C&, B&>::Value),
			(IsSwappableWithValue<C&, B&>)
		);
	}

	TEST(TypeTraits, IsSwappableWithNoThrow)
	{
		struct A {};


		struct B
		{
			B(
				const B& other
			);
		};


		struct C
		{
			C(
				const C& other
			) noexcept;

			C& operator=(
				const C& other
			) noexcept;
		};


		#pragma warning(push)
		#pragma warning(disable: 5046)

		constexpr void swap(
			A& a,
			B& b
		);

		constexpr void swap(
			B& a,
			A& b
		);

		constexpr void swap(
			B& a,
			C& b
		) noexcept;

		constexpr void swap(
			C& a,
			B& b
		) noexcept;

		constexpr void Swap(
			A& a,
			B& b
		);

		constexpr void Swap(
			B& a,
			A& b
		);

		constexpr void Swap(
			B& a,
			C& b
		) noexcept;

		constexpr void Swap(
			C& a,
			B& b
		) noexcept;

		#pragma warning(pop)

		EXPECT_EQ(
			(is_nothrow_swappable_with_v<int32, int32>),
			(IsSwappableWithNoThrow<int32, int32>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_swappable_with_v<int32&, int32&>),
			(IsSwappableWithNoThrow<int32&, int32&>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_swappable_with_v<int32&, float32&>),
			(IsSwappableWithNoThrow<int32&, float32&>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_swappable_with_v<A&, B&>),
			(IsSwappableWithNoThrow<A&, B&>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_swappable_with_v<A&, C&>),
			(IsSwappableWithNoThrow<A&, C&>::Value)
		);
		EXPECT_EQ(
			(is_nothrow_swappable_with_v<B&, C&>),
			(IsSwappableWithNoThrow<B&, C&>::Value)
		);

		EXPECT_EQ(
			(IsSwappableWithNoThrow<int32, int32>::Value),
			(IsSwappableWithNoThrowValue<int32, int32>)
		);
		EXPECT_EQ(
			(IsSwappableWithNoThrow<int32&, int32&>::Value),
			(IsSwappableWithNoThrowValue<int32&, int32&>)
		);
		EXPECT_EQ(
			(IsSwappableWithNoThrow<int32&, float32&>::Value),
			(IsSwappableWithNoThrowValue<int32&, float32&>)
		);
		EXPECT_EQ(
			(IsSwappableWithNoThrow<A&, B&>::Value),
			(IsSwappableWithNoThrowValue<A&, B&>)
		);
		EXPECT_EQ(
			(IsSwappableWithNoThrow<A&, C&>::Value),
			(IsSwappableWithNoThrowValue<A&, C&>)
		);
		EXPECT_EQ(
			(IsSwappableWithNoThrow<B&, C&>::Value),
			(IsSwappableWithNoThrowValue<B&, C&>)
		);
	}

	TEST(TypeTraits, IsTrivial)
	{
		struct A
		{
			int32 F;
		};

		struct B
		{
			B() {}
		};

		EXPECT_EQ(
			is_trivial_v<A>,
			IsTrivial<A>::Value
		);
		EXPECT_EQ(
			is_trivial_v<B>,
			IsTrivial<B>::Value
		);

		EXPECT_EQ(
			IsTrivial<A>::Value,
			IsTrivialValue<A>
		);
		EXPECT_EQ(
			IsTrivial<B>::Value,
			IsTrivialValue<B>
		);
	}

	TEST(TypeTraits, IsUnboundedArray)
	{
		EXPECT_EQ(
			is_unbounded_array_v<int32>,
			IsUnboundedArray<int32>::Value
		);
		EXPECT_EQ(
			is_unbounded_array_v<int32[]>,
			IsUnboundedArray<int32[]>::Value
		);
		EXPECT_EQ(
			is_unbounded_array_v<int32[3]>,
			IsUnboundedArray<int32[3]>::Value
		);

		EXPECT_EQ(
			IsUnboundedArray<int32>::Value,
			IsUnboundedArrayValue<int32>
		);
		EXPECT_EQ(
			IsUnboundedArray<int32[]>::Value,
			IsUnboundedArrayValue<int32[]>
		);
		EXPECT_EQ(
			IsUnboundedArray<int32[3]>::Value,
			IsUnboundedArrayValue<int32[3]>
		);
	}

	TEST(TypeTraits, IsUnion)
	{
		union UA {};

		enum EA {};

		EXPECT_EQ(
			is_union_v<int32>,
			IsUnion<int32>::Value
		);
		EXPECT_EQ(
			is_union_v<EA>,
			IsUnion<EA>::Value
		);
		EXPECT_EQ(
			is_union_v<UA>,
			IsUnion<UA>::Value
		);

		EXPECT_EQ(
			IsUnion<int32>::Value,
			IsUnionValue<int32>
		);
		EXPECT_EQ(
			IsUnion<EA>::Value,
			IsUnionValue<EA>
		);
		EXPECT_EQ(
			IsUnion<UA>::Value,
			IsUnionValue<UA>
		);
	}

	TEST(TypeTraits, IsUnsigned)
	{
		EXPECT_TRUE(
			IsUnsigned<uint32>::Value
		);
		EXPECT_FALSE(
			IsUnsigned<int32>::Value
		);
		EXPECT_FALSE(
			IsUnsigned<float32>::Value
		);
		EXPECT_FALSE(
			IsUnsigned<char8>::Value
		);

		EXPECT_EQ(
			IsUnsigned<uint32>::Value,
			IsUnsignedValue<uint32>
		);
		EXPECT_EQ(
			IsUnsigned<int32>::Value,
			IsUnsignedValue<int32>
		);
		EXPECT_EQ(
			IsUnsigned<float32>::Value,
			IsUnsignedValue<float32>
		);
		EXPECT_EQ(
			IsUnsigned<char8>::Value,
			IsUnsignedValue<char8>
		);
	}

	TEST(TypeTraits, IsVoid)
	{
		EXPECT_EQ(
			is_void_v<void>,
			IsVoid<void>::Value
		);
		EXPECT_EQ(
			is_void_v<void*>,
			IsVoid<void*>::Value
		);
		EXPECT_EQ(
			is_void_v<void()>,
			IsVoid<void()>::Value
		);

		EXPECT_EQ(
			IsVoid<void>::Value,
			IsVoidValue<void>
		);
		EXPECT_EQ(
			IsVoid<void*>::Value,
			IsVoidValue<void*>
		);
		EXPECT_EQ(
			IsVoid<void()>::Value,
			IsVoidValue<void()>
		);
	}

	TEST(TypeTraits, IsVolatile)
	{
		EXPECT_EQ(
			is_volatile_v<int32>,
			IsVolatile<int32>::Value
		);
		EXPECT_EQ(
			is_volatile_v<const volatile int32>,
			IsVolatile<const volatile int32>::Value
		);
		EXPECT_EQ(
			is_volatile_v<volatile int32*>,
			IsVolatile<volatile int32*>::Value
		);
		EXPECT_EQ(
			is_volatile_v<int32* volatile>,
			IsVolatile<int32* volatile>::Value
		);

		EXPECT_EQ(
			IsVolatile<int32>::Value,
			IsVolatileValue<int32>
		);
		EXPECT_EQ(
			IsVolatile<const volatile int32>::Value,
			IsVolatileValue<const volatile int32>
		);
		EXPECT_EQ(
			IsVolatile<volatile int32*>::Value,
			IsVolatileValue<volatile int32*>
		);
		EXPECT_EQ(
			IsVolatile<int32* volatile>::Value,
			IsVolatileValue<int32* volatile>
		);
	}

	TEST(TypeTraits, MakeSigned)
	{
		enum EA {};

		enum class EB {};

		EXPECT_TRUE(
			(is_same_v<
				make_signed_t<uint32>,
				MakeSigned<uint32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				make_signed_t<uint32>,
				MakeSigned<uint32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				make_signed_t<EA>,
				MakeSigned<EA>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				make_signed_t<EB>,
				MakeSigned<EB>
				>)
		);
	}

	TEST(TypeTraits, MakeUnsigned)
	{
		enum EA {};

		enum class EB {};

		EXPECT_TRUE(
			(is_same_v<
				make_unsigned_t<uint32>,
				MakeUnsigned<uint32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				make_unsigned_t<uint32>,
				MakeUnsigned<uint32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				make_unsigned_t<EA>,
				MakeUnsigned<EA>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				make_unsigned_t<EB>,
				MakeUnsigned<EB>
				>)
		);
	}

	TEST(TypeTraits, Not)
	{
		struct A : true_type {};

		struct B : TrueConstant {};

		EXPECT_EQ(
			negation_v<true_type>,
			Not<TrueConstant>::Value
		);
		EXPECT_EQ(
			negation_v<false_type>,
			Not<FalseConstant>::Value
		);
		EXPECT_EQ(
			negation_v<A>,
			Not<B>::Value
		);

		EXPECT_EQ(
			Not<TrueConstant>::Value,
			NotValue<TrueConstant>
		);
		EXPECT_EQ(
			Not<FalseConstant>::Value,
			NotValue<FalseConstant>
		);
		EXPECT_EQ(
			Not<B>::Value,
			NotValue<B>
		);
	}

	TEST(TypeTraits, Or)
	{
		EXPECT_EQ(
			disjunction_v<true_type>,
			Or<TrueConstant>::Value
		);
		EXPECT_EQ(
			(disjunction_v<true_type, true_type>),
			(Or<TrueConstant, TrueConstant>::Value)
		);
		EXPECT_EQ(
			(disjunction_v<true_type, false_type>),
			(Or<TrueConstant, FalseConstant>::Value)
		);
		EXPECT_EQ(
			(disjunction_v<false_type, false_type>),
			(Or<FalseConstant, FalseConstant>::Value)
		);

		EXPECT_EQ(
			Or<TrueConstant>::Value,
			OrValue<TrueConstant>
		);
		EXPECT_EQ(
			(Or<TrueConstant, TrueConstant>::Value),
			(OrValue<TrueConstant, TrueConstant>)
		);
		EXPECT_EQ(
			(Or<TrueConstant, FalseConstant>::Value),
			(OrValue<TrueConstant, FalseConstant>)
		);
		EXPECT_EQ(
			(Or<FalseConstant, FalseConstant>::Value),
			(OrValue<FalseConstant, FalseConstant>)
		);
	}

	TEST(TypeTraits, RemoveAll)
	{
		#define remove_all(T) remove_cv_t<remove_pointer_t<remove_reference_t<T>>>

		EXPECT_TRUE(
			(is_same_v<
				remove_all(int32),
				RemoveAll<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_all(const int64),
				RemoveAll<const int64>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_all(float32&),
				RemoveAll<float32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_all(char16*),
				RemoveAll<char16*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_all(int32*&),
				RemoveAll<int32*&>
				>)
		);

		#undef remove_all
	}

	TEST(TypeTraits, RemoveArrayDimension)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_extent_t<int>,
				RemoveArrayDimension<int>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_extent_t<int[5]>,
				RemoveArrayDimension<int[5]>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_extent_t<int[5][10]>,
				RemoveArrayDimension<int[5][10]>
				>)
		);
	}

	TEST(TypeTraits, RemoveArrayDimensions)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_all_extents_t<int>,
				RemoveArrayDimensions<int>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_all_extents_t<int[5]>,
				RemoveArrayDimensions<int[5]>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_all_extents_t<int[5][10]>,
				RemoveArrayDimensions<int[5][10]>
				>)
		);
	}

	TEST(TypeTraits, RemoveConst)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<int32>,
				RemoveConst<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<const int32>,
				RemoveConst<const int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<int32&>,
				RemoveConst<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<const int32&>,
				RemoveConst<const int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<int32*>,
				RemoveConst<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<const int32*>,
				RemoveConst<const int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<int32* const>,
				RemoveConst<int32* const>
				>)
		);
	}

	TEST(TypeTraits, RemoveConstPointer)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<remove_pointer_t<int32>>,
				RemoveConstPointer<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const int32,
				RemoveConstPointer<const int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<remove_pointer_t<int32&>>,
				RemoveConstPointer<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<remove_pointer_t<const int32&>>,
				RemoveConstPointer<const int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const int32*,
				RemoveConstPointer<const int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<const int32* const>,
				RemoveConstPointer<const int32* const>
				>)
		);
	}

	TEST(TypeTraits, RemoveConstReference)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<int32>,
				RemoveConstReference<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<const int32>,
				RemoveConstReference<const int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<remove_reference_t<int32&>>,
				RemoveConstReference<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<remove_reference_t<const int32&>>,
				RemoveConstReference<const int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_const_t<remove_reference_t<const int32*&>>,
				RemoveConstReference<const int32*&>
				>)
		);
	}

	TEST(TypeTraits, RemoveConstVolatile)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<int32>,
				RemoveConstVolatile<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<const int32>,
				RemoveConstVolatile<const int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<volatile int32>,
				RemoveConstVolatile<volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<const volatile int32>,
				RemoveConstVolatile<const volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<int32&>,
				RemoveConstVolatile<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<const int32&>,
				RemoveConstVolatile<const int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<volatile int32&>,
				RemoveConstVolatile<volatile int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<const volatile int32&>,
				RemoveConstVolatile<const volatile int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<int32*>,
				RemoveConstVolatile<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<const int32*>,
				RemoveConstVolatile<const int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<volatile int32*>,
				RemoveConstVolatile<volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<const volatile int32*>,
				RemoveConstVolatile<const volatile int32*>
				>)
		);
	}

	TEST(TypeTraits, RemoveConstVolatilePointer)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<int32>>,
				RemoveConstVolatilePointer<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<const int32>>,
				RemoveConstVolatilePointer<const int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<volatile int32>>,
				RemoveConstVolatilePointer<volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<const volatile int32>>,
				RemoveConstVolatilePointer<const volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<int32&>>,
				RemoveConstVolatilePointer<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<const int32&>>,
				RemoveConstVolatilePointer<const int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<volatile int32&>>,
				RemoveConstVolatilePointer<volatile int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<const volatile int32&>>,
				RemoveConstVolatilePointer<const volatile int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<int32*>>,
				RemoveConstVolatilePointer<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<const int32*>>,
				RemoveConstVolatilePointer<const int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<volatile int32*>>,
				RemoveConstVolatilePointer<volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_pointer_t<const volatile int32*>>,
				RemoveConstVolatilePointer<const volatile int32*>
				>)
		);
	}

	TEST(TypeTraits, RemoveConstVolatileReference)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<int32>>,
				RemoveConstVolatileReference<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<const int32>>,
				RemoveConstVolatileReference<const int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<volatile int32>>,
				RemoveConstVolatileReference<volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<const volatile int32>>,
				RemoveConstVolatileReference<const volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<int32&>>,
				RemoveConstVolatileReference<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<const int32&>>,
				RemoveConstVolatileReference<const int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<volatile int32&>>,
				RemoveConstVolatileReference<volatile int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<const volatile int32&>>,
				RemoveConstVolatileReference<const volatile int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<const int32*&>>,
				RemoveConstVolatileReference<const int32*&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<volatile int32*&>>,
				RemoveConstVolatileReference<volatile int32*&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_cv_t<remove_reference_t<const volatile int32*&>>,
				RemoveConstVolatileReference<const volatile int32*&>
				>)
		);
	}

	TEST(TypeTraits, RemovePointer)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<int32>,
				RemovePointer<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<int32&>,
				RemovePointer<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<int32&&>,
				RemovePointer<int32&&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<int32*>,
				RemovePointer<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<int32*&>,
				RemovePointer<int32*&>
				>)
		);
	}

	TEST(TypeTraits, RemovePointerReference)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<remove_reference_t<int32>>,
				RemovePointerReference<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<remove_reference_t<int32&>>,
				RemovePointerReference<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<remove_reference_t<int32&&>>,
				RemovePointerReference<int32&&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<remove_reference_t<int32*>>,
				RemovePointerReference<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<remove_reference_t<int32*&>>,
				RemovePointerReference<int32*&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_pointer_t<remove_reference_t<int32*&&>>,
				RemovePointerReference<int32*&&>
				>)
		);
	}

	TEST(TypeTraits, RemoveReference)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_reference_t<int32>,
				RemoveReference<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_reference_t<int32&>,
				RemoveReference<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_reference_t<int32&&>,
				RemoveReference<int32&&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_reference_t<int32*>,
				RemoveReference<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_reference_t<int32*&>,
				RemoveReference<int32*&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_reference_t<int32*&&>,
				RemoveReference<int32*&&>
				>)
		);
	}

	TEST(TypeTraits, RemoveVolatile)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<int32>,
				RemoveVolatile<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<volatile int32>,
				RemoveVolatile<volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<int32&>,
				RemoveVolatile<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<volatile int32&>,
				RemoveVolatile<volatile int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<int32*>,
				RemoveVolatile<int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<volatile int32*>,
				RemoveVolatile<volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<int32* volatile>,
				RemoveVolatile<int32* volatile>
				>)
		);
	}

	TEST(TypeTraits, RemoveVolatilePointer)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_pointer_t<int32>>,
				RemoveVolatilePointer<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_pointer_t<volatile int32>>,
				RemoveVolatilePointer<volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_pointer_t<int32&>>,
				RemoveVolatilePointer<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_pointer_t<volatile int32&>>,
				RemoveVolatilePointer<volatile int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_pointer_t<volatile int32*>>,
				RemoveVolatilePointer<volatile int32*>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_pointer_t<volatile int32* volatile>>,
				RemoveVolatilePointer<volatile int32* volatile>
				>)
		);
	}

	TEST(TypeTraits, RemoveVolatileReference)
	{
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_reference_t<int32>>,
				RemoveVolatileReference<int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_reference_t<volatile int32>>,
				RemoveVolatileReference<volatile int32>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_reference_t<int32&>>,
				RemoveVolatileReference<int32&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_reference_t<volatile int32&>>,
				RemoveVolatileReference<volatile int32&>
				>)
		);

		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_reference_t<volatile int32*&>>,
				RemoveVolatileReference<volatile int32*&>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				remove_volatile_t<remove_reference_t<volatile int32* volatile&>>,
				RemoveVolatileReference<volatile int32* volatile&>
				>)
		);
	}

	TEST(TypeTraits, ReplaceConstVolatile)
	{
		EXPECT_TRUE(
			(is_same_v<
				int32,
				ReplaceConstVolatile<int32, int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const int32,
				ReplaceConstVolatile<const int32, int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				volatile int32,
				ReplaceConstVolatile<volatile int32, int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32,
				ReplaceConstVolatile<const volatile int32, int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				int32,
				ReplaceConstVolatile<int32, const volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const int32,
				ReplaceConstVolatile<const int32, const volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				volatile int32,
				ReplaceConstVolatile<volatile int32, const volatile int32>
				>)
		);
		EXPECT_TRUE(
			(is_same_v<
				const volatile int32,
				ReplaceConstVolatile<const volatile int32, const volatile int32>
				>)
		);
	}
}
