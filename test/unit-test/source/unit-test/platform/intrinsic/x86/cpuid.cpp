﻿#include "precompiled-unit-test.hpp"


namespace Test::Platform::Intrinsic
{
	using namespace Std::Cpuid;

	TEST(Cpuid, Basic)
	{
		int32 data[4];
		__cpuid(data, 0);

		for (auto i = 0; i <= data[0]; ++i)
		{
			__cpuid(data, i);
			const auto leaf = Cpuid(i);

			EXPECT_EQ(
				data[0],
				leaf.Eax
			) << "\n\tFunction id is: " << i;
			EXPECT_EQ(
				data[1],
				leaf.Ebx
			) << "\n\tFunction id is: " << i;
			EXPECT_EQ(
				data[2],
				leaf.Ecx
			) << "\n\tFunction id is: " << i;
			EXPECT_EQ(
				data[3],
				leaf.Edx
			) << "\n\tFunction id is: " << i;
		}
	}

	TEST(Cpuid, Function7)
	{
		int32 data[4];
		__cpuid(data, 7);

		for (auto i = 0; i <= data[0]; ++i)
		{
			__cpuidex(data, 7, i);
			const auto leaf = Cpuid(7, i);

			EXPECT_EQ(
				data[0],
				leaf.Eax
			) << "\n\tEcx value is: " << i;
			EXPECT_EQ(
				data[1],
				leaf.Ebx
			) << "\n\tEcx value is: " << i;
			EXPECT_EQ(
				data[2],
				leaf.Ecx
			) << "\n\tEcx value is: " << i;
			EXPECT_EQ(
				data[3],
				leaf.Edx
			) << "\n\tEcx value is: " << i;
		}
	}

	TEST(Cpuid, Extended)
	{
		int32 data[4];
		__cpuid(data, static_cast<int32>(0x80000000));

		for (auto i = 0; i <= static_cast<int32>(data[0] - 0x80000000); ++i)
		{
			__cpuid(data, i);
			const auto leaf = Cpuid(i);

			EXPECT_EQ(
				data[0],
				leaf.Eax
			) << "\n\tFunction id is: 0x80000000 + " << i;
			EXPECT_EQ(
				data[1],
				leaf.Ebx
			) << "\n\tFunction id is: 0x80000000 + " << i;
			EXPECT_EQ(
				data[2],
				leaf.Ecx
			) << "\n\tFunction id is: 0x80000000 + " << i;
			EXPECT_EQ(
				data[3],
				leaf.Edx
			) << "\n\tFunction id is: 0x80000000 + " << i;
		}
	}
}
