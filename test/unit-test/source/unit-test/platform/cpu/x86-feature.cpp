﻿#include "precompiled-unit-test.hpp"


namespace Test::Platform::Cpu
{
	using namespace Std::Cpu;
	using namespace Std::Cpuid;


	namespace
	{
		const X86& GetMyCpu()
		{
			static auto instance = X86();
			return instance;
		}

		const cpu_features::X86Info& GetGoogleCpu()
		{
			static auto instance = cpu_features::GetX86Info();
			return instance;
		}

		const cpu_id_t& GetLibCpu()
		{
			static auto isInitialized = false;
			static cpu_id_t instance;

			if (isInitialized)
			{
				return instance;
			}

			isInitialized = true;
			cpu_identify(null, &instance);
			return instance;
		}
	}


	TEST(X86Feature, Abm)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_ABM],
			GetMyCpu().Features.Abm
		);
	}

	TEST(X86Feature, AddressMasking)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.uai) || static_cast<bool>(GetGoogleCpu().features.lam),
			GetMyCpu().Features.AddressMasking
		);
	}

	TEST(X86Feature, Adx)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_ADX],
			GetMyCpu().Features.Adx
		);
	}

	TEST(X86Feature, Aes)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AES],
			GetMyCpu().Features.Aes
		);
	}

	TEST(X86Feature, AmxBrainFloat16)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.amx_bf16),
			GetMyCpu().Features.AmxBrainFloat16
		);
	}

	TEST(X86Feature, AmxComplex)
	{
		EXPECT_EQ(
			Cpuid(7, 1).Edx & (1 << 7),
			GetMyCpu().Features.AmxComplex
		);
	}

	TEST(X86Feature, AmxFloat16)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.amx_fp16),
			GetMyCpu().Features.AmxFloat16
		);
	}

	TEST(X86Feature, AmxInt8)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.amx_int8),
			GetMyCpu().Features.AmxInt8
		);
	}

	TEST(X86Feature, AmxTile)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.amx_tile),
			GetMyCpu().Features.AmxTile
		);
	}

	TEST(X86Feature, Avx)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX],
			GetMyCpu().Features.Avx
		);
	}

	TEST(X86Feature, Avx2)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX2],
			GetMyCpu().Features.Avx2
		);
	}

	TEST(X86Feature, Avx512BitAlgorithms)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx512bitalg),
			GetMyCpu().Features.Avx512BitAlgorithms
		);
	}

	TEST(X86Feature, Avx512Bmi)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512VBMI],
			GetMyCpu().Features.Avx512Bmi
		);
	}

	TEST(X86Feature, Avx512Bmi2)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512VBMI2],
			GetMyCpu().Features.Avx512Bmi2
		);
	}

	TEST(X86Feature, Avx512BrainFloat16)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx512_bf16),
			GetMyCpu().Features.Avx512BrainFloat16
		);
	}

	TEST(X86Feature, Avx512ByteWord)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512BW],
			GetMyCpu().Features.Avx512ByteWord
		);
	}

	TEST(X86Feature, Avx512ConflictDetection)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512CD],
			GetMyCpu().Features.Avx512ConflictDetection
		);
	}

	TEST(X86Feature, Avx512DoubleQuad)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512DQ],
			GetMyCpu().Features.Avx512DoubleQuad
		);
	}

	TEST(X86Feature, Avx512ExponentialReciprocal)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512ER],
			GetMyCpu().Features.Avx512ExponentialReciprocal
		);
	}

	TEST(X86Feature, Avx512)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512F],
			GetMyCpu().Features.Avx512
		);
	}

	TEST(X86Feature, Avx512Float16)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx512_fp16),
			GetMyCpu().Features.Avx512Float16
		);
	}

	TEST(X86Feature, Avx512FmaPs4)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx512_4fmaps),
			GetMyCpu().Features.Avx512FmaPs4
		);
	}

	TEST(X86Feature, Avx512FmaInt)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx512ifma),
			GetMyCpu().Features.Avx512FmaInt
		);
	}

	TEST(X86Feature, Avx512IntersectInt)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx512_vp2intersect),
			GetMyCpu().Features.Avx512IntersectInt
		);
	}

	TEST(X86Feature, Avx512NeuralNetwork4)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx512_4vnniw),
			GetMyCpu().Features.Avx512NeuralNetwork4
		);
	}

	TEST(X86Feature, Avx512NeuralNetwork)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512VNNI],
			GetMyCpu().Features.Avx512NeuralNetwork
		);
	}

	TEST(X86Feature, Avx512PopulationCount)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx512vpopcntdq),
			GetMyCpu().Features.Avx512PopulationCount
		);
	}

	TEST(X86Feature, Avx512Prefetch)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512PF],
			GetMyCpu().Features.Avx512Prefetch
		);
	}

	TEST(X86Feature, Avx512VectorLength)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_AVX512VL],
			GetMyCpu().Features.Avx512VectorLength
		);
	}

	TEST(X86Feature, AvxNeuralNetwork)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.avx_vnni),
			GetMyCpu().Features.AvxNeuralNetwork
		);
	}

	TEST(X86Feature, AvxNeuralNetworkInt16)
	{
		EXPECT_EQ(
			Cpuid(7, 1).Edx & (1 << 9),
			GetMyCpu().Features.AvxNeuralNetworkInt16
		);
	}

	TEST(X86Feature, AvxNeuralNetworkInt8)
	{
		EXPECT_EQ(
			Cpuid(7, 1).Edx & (1 << 3),
			GetMyCpu().Features.AvxNeuralNetworkInt8
		);
	}

	TEST(X86Feature, AvxNoExceptionFloatConvert)
	{
		EXPECT_EQ(
			Cpuid(7, 1).Edx & (1 << 4),
			GetMyCpu().Features.AvxNoExceptionFloatConvert
		);
	}

	TEST(X86Feature, Bmi)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_BMI1],
			GetMyCpu().Features.Bmi
		);
	}

	TEST(X86Feature, Bmi2)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_BMI2],
			GetMyCpu().Features.Bmi2
		);
	}

	TEST(X86Feature, CacheLineFlush)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_CLFLUSH],
			GetMyCpu().Features.CacheLineFlush
		);
	}

	TEST(X86Feature, CacheLineFlushOptimized)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.clflushopt),
			GetMyCpu().Features.CacheLineFlushOptimized
		);
	}

	TEST(X86Feature, CacheLineWriteBack)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.clwb),
			GetMyCpu().Features.CacheLineWriteBack
		);
	}

	TEST(X86Feature, CarryLessMultiplication256)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.vpclmulqdq),
			GetMyCpu().Features.CarryLessMultiplication256
		);
	}

	TEST(X86Feature, CarryLessMultiplication64)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_PCLMUL],
			GetMyCpu().Features.CarryLessMultiplication64
		);
	}

	TEST(X86Feature, CompareExchange16)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_CX16],
			GetMyCpu().Features.CompareExchange16
		);
	}

	TEST(X86Feature, CompareExchange8)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_CX8],
			GetMyCpu().Features.CompareExchange8
		);
	}

	TEST(X86Feature, ConditionalMove)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_CMOV],
			GetMyCpu().Features.ConditionalMove
		);
	}

	TEST(X86Feature, DirectCacheAccess)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_DCA],
			GetMyCpu().Features.DirectCacheAccess
		);
	}

	TEST(X86Feature, EnhancedRepMove)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.erms),
			GetMyCpu().Features.EnhancedRepMove
		);
	}

	TEST(X86Feature, F16c)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_F16C],
			GetMyCpu().Features.F16c
		);
	}

	TEST(X86Feature, FXSave)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_FXSR],
			GetMyCpu().Features.FXSave
		);
	}

	TEST(X86Feature, FastShortRepCompare)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.fs_rep_cmpsb_scasb),
			GetMyCpu().Features.FastShortRepCompare
		);
	}

	TEST(X86Feature, FastShortRepMove)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.fs_rep_mov),
			GetMyCpu().Features.FastShortRepMove
		);
	}

	TEST(X86Feature, FastShortRepStore)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.fs_rep_stosb),
			GetMyCpu().Features.FastShortRepStore
		);
	}

	TEST(X86Feature, FastZeroLengthRepMove)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.fz_rep_movsb),
			GetMyCpu().Features.FastZeroLengthRepMove
		);
	}

	TEST(X86Feature, Fma3)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_FMA3],
			GetMyCpu().Features.Fma3
		);
	}

	TEST(X86Feature, Fma4)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_FMA4],
			GetMyCpu().Features.Fma4
		);
	}

	TEST(X86Feature, Fpu)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_FPU],
			GetMyCpu().Features.Fpu
		);
	}

	TEST(X86Feature, Gfni)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.gfni),
			GetMyCpu().Features.Gfni
		);
	}

	TEST(X86Feature, Hle)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_HLE],
			GetMyCpu().Features.Hle
		);
	}

	TEST(X86Feature, InvPcid)
	{
		EXPECT_EQ(
			(Cpuid(7).Ebx >> 10) & 1,
			GetMyCpu().Features.InvPcid
		);
	}

	TEST(X86Feature, Mmx)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_MMX],
			GetMyCpu().Features.Mmx
		);
	}

	TEST(X86Feature, Monitor)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_MONITOR],
			GetMyCpu().Features.Monitor
		);
	}

	TEST(X86Feature, MovBigEndian)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_MOVBE],
			GetMyCpu().Features.MovBigEndian
		);
	}

	TEST(X86Feature, MoveDirect)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.movdiri),
			GetMyCpu().Features.MoveDirect
		);
	}

	TEST(X86Feature, MoveDirect64b)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.movdir64b),
			GetMyCpu().Features.MoveDirect64b
		);
	}

	TEST(X86Feature, OsXSave)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_OSXSAVE],
			GetMyCpu().Features.OsXSave
		);
	}

	TEST(X86Feature, PopulationCount)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_POPCNT],
			GetMyCpu().Features.PopulationCount
		);
	}

	TEST(X86Feature, RdRand)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_RDRAND],
			GetMyCpu().Features.RdRand
		);
	}

	TEST(X86Feature, RdSeed)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_RDSEED],
			GetMyCpu().Features.RdSeed
		);
	}

	TEST(X86Feature, Rtm)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_RTM],
			GetMyCpu().Features.Rtm
		);
	}

	TEST(X86Feature, SelfSnoop)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SS],
			GetMyCpu().Features.SelfSnoop
		);
	}

	TEST(X86Feature, Sgx)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SGX],
			GetMyCpu().Features.Sgx
		);
	}

	TEST(X86Feature, Sha)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SHA_NI],
			GetMyCpu().Features.Sha
		);
	}

	TEST(X86Feature, Smx)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SMX],
			GetMyCpu().Features.Smx
		);
	}

	TEST(X86Feature, Sse)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SSE],
			GetMyCpu().Features.Sse
		);
	}

	TEST(X86Feature, Sse2)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SSE2],
			GetMyCpu().Features.Sse2
		);
	}

	TEST(X86Feature, Sse3)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.sse3),
			GetMyCpu().Features.Sse3
		);
	}

	TEST(X86Feature, Sse41)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SSE4_1],
			GetMyCpu().Features.Sse41
		);
	}

	TEST(X86Feature, Sse42)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SSE4_2],
			GetMyCpu().Features.Sse42
		);
	}

	TEST(X86Feature, Sse4a)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SSE4A],
			GetMyCpu().Features.Sse4a
		);
	}

	TEST(X86Feature, Ssse3)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_SSSE3],
			GetMyCpu().Features.Ssse3
		);
	}

	TEST(X86Feature, Tbm)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_TBM],
			GetMyCpu().Features.Tbm
		);
	}

	TEST(X86Feature, ThreeDNowPrefetch)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_3DNOWPREFETCH],
			GetMyCpu().Features.ThreeDNowPrefetch
		);
	}

	TEST(X86Feature, RdTsc)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_TSC],
			GetMyCpu().Features.RdTsc
		);
	}

	TEST(X86Feature, VectorAes)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.vaes),
			GetMyCpu().Features.VectorAes
		);
	}

	TEST(X86Feature, XSave)
	{
		EXPECT_EQ(
			GetLibCpu().flags[CPU_FEATURE_XSAVE],
			GetMyCpu().Features.XSave
		);
	}

	TEST(X86Feature, XTest)
	{
		EXPECT_EQ(
			static_cast<bool>(GetGoogleCpu().features.hle) || static_cast<bool>(GetGoogleCpu().features.rtm),
			GetMyCpu().Features.XTest
		);
	}
}
