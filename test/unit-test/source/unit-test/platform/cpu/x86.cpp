﻿#include "precompiled-unit-test.hpp"


namespace Test::Platform::Cpu
{
	using namespace Std::Cpu;
	using namespace Std::Cpuid;


	namespace
	{
		const X86& GetMyCpu()
		{
			static auto instance = X86();
			return instance;
		}

		const cpu_id_t& GetLibCpu()
		{
			static auto isInitialized = false;
			static cpu_id_t instance;

			if (isInitialized)
			{
				return instance;
			}

			isInitialized = true;
			cpu_identify(null, &instance);
			return instance;
		}
	}


	TEST(X86, ActualFamily)
	{
		EXPECT_EQ(
			GetLibCpu().ext_family,
			GetMyCpu().ActualFamily
		);
	}

	TEST(X86, ActualModel)
	{
		EXPECT_EQ(
			GetLibCpu().ext_model,
			GetMyCpu().ActualModel
		);
	}

	TEST(X86, Brand)
	{
		EXPECT_EQ(
			std::string(GetLibCpu().brand_str),
			std::string(reinterpret_cast<const char(&)[49]>(GetMyCpu().BrandString))
		);
	}

	TEST(X86, BrandIndex)
	{
		EXPECT_EQ(
			Cpuid(1).Ebx & 0xff,
			GetMyCpu().BrandIndex
		);
	}

	TEST(X86, ExtendedFamily)
	{
		const auto eax = Cpuid(1).Eax;
		EXPECT_EQ(
			eax >> 20 & 0xff,
			GetMyCpu().ExtendedFamily
		);
	}

	TEST(X86, ExtendedModel)
	{
		const auto eax = Cpuid(1).Eax;
		EXPECT_EQ(
			eax >> 16 & 0xf,
			GetMyCpu().ExtendedModel
		);
	}

	TEST(X86, Family)
	{
		EXPECT_EQ(
			GetLibCpu().family,
			GetMyCpu().Family
		);
	}

	TEST(X86, L1DataCache)
	{
		EXPECT_EQ(
			GetLibCpu().l1_data_cache > 0,
			GetMyCpu().L1DataCache.IsPresent
		);
		EXPECT_EQ(
			GetLibCpu().l1_data_cache,
			GetMyCpu().L1DataCache.Size
		);
		EXPECT_EQ(
			GetLibCpu().l1_data_cacheline,
			GetMyCpu().L1DataCache.LineSize
		);
		EXPECT_EQ(
			GetLibCpu().l1_data_assoc,
			GetMyCpu().L1DataCache.Associativity
		);
	}

	TEST(X86, L1InstrucionsCache)
	{
		EXPECT_EQ(
			GetLibCpu().l1_instruction_cache > 0,
			GetMyCpu().L1InstructionsCache.IsPresent
		);
		EXPECT_EQ(
			GetLibCpu().l1_instruction_cache,
			GetMyCpu().L1InstructionsCache.Size
		);
		EXPECT_EQ(
			GetLibCpu().l1_instruction_cacheline,
			GetMyCpu().L1InstructionsCache.LineSize
		);
		EXPECT_EQ(
			GetLibCpu().l1_instruction_assoc,
			GetMyCpu().L1InstructionsCache.Associativity
		);
	}

	TEST(X86, L2Cache)
	{
		EXPECT_EQ(
			GetLibCpu().l2_cache > 0,
			GetMyCpu().L2UnifiedCache.IsPresent
		);
		EXPECT_EQ(
			GetLibCpu().l2_cache,
			GetMyCpu().L2UnifiedCache.Size
		);
		EXPECT_EQ(
			GetLibCpu().l2_cacheline,
			GetMyCpu().L2UnifiedCache.LineSize
		);
		EXPECT_EQ(
			GetLibCpu().l2_assoc,
			GetMyCpu().L2UnifiedCache.Associativity
		);
	}

	TEST(X86, L3Cache)
	{
		EXPECT_EQ(
			GetLibCpu().l3_cache > 0,
			GetMyCpu().L3UnifiedCache.IsPresent
		);
		EXPECT_EQ(
			GetLibCpu().l3_cache,
			GetMyCpu().L3UnifiedCache.Size
		);
		EXPECT_EQ(
			GetLibCpu().l3_cacheline,
			GetMyCpu().L3UnifiedCache.LineSize
		);
		EXPECT_EQ(
			GetLibCpu().l3_assoc,
			GetMyCpu().L3UnifiedCache.Associativity
		);
	}

	TEST(X86, Leaves)
	{
		static cpu_raw_data_t libData;
		cpuid_get_raw_data(&libData);

		EXPECT_EQ(
			Cpuid(0),
			GetMyCpu().Leaves[X86::ELeafId::Function0]
		);

		auto cpuidLeaf1 = Cpuid(1);
		auto myLeaf1 = GetMyCpu().Leaves[X86::ELeafId::Function1];

		// Clear APIC id
		cpuidLeaf1.Ebx &= 0x00ffffff;
		myLeaf1.Ebx &= 0x00ffffff;

		EXPECT_EQ(
			cpuidLeaf1,
			myLeaf1
		);

		EXPECT_EQ(
			Cpuid(2),
			GetMyCpu().Leaves[X86::ELeafId::Function2]
		);
		EXPECT_EQ(
			Cpuid(7),
			GetMyCpu().Leaves[X86::ELeafId::Function7]
		);
		EXPECT_EQ(
			Cpuid(7, 1),
			GetMyCpu().Leaves[X86::ELeafId::Function7Ecx1]
		);
		EXPECT_EQ(
			Cpuid(0x80000000),
			GetMyCpu().Leaves[X86::ELeafId::Function80000000]
		);
		EXPECT_EQ(
			Cpuid(0x80000001),
			GetMyCpu().Leaves[X86::ELeafId::Function80000001]
		);
		EXPECT_EQ(
			Cpuid(0x80000002),
			GetMyCpu().Leaves[X86::ELeafId::Function80000002]
		);
		EXPECT_EQ(
			Cpuid(0x80000003),
			GetMyCpu().Leaves[X86::ELeafId::Function80000003]
		);
		EXPECT_EQ(
			Cpuid(0x80000004),
			GetMyCpu().Leaves[X86::ELeafId::Function80000004]
		);

		EXPECT_EQ(
			Cpuid(0x80000021),
			GetMyCpu().Leaves[X86::ELeafId::Function80000021]
		);
	}

	TEST(X86, LogicalCores)
	{
		EXPECT_EQ(
			GetLibCpu().num_logical_cpus,
			GetMyCpu().LogicalCores
		);
	}

	TEST(X86, Model)
	{
		EXPECT_EQ(
			GetLibCpu().model,
			GetMyCpu().Model
		);
	}

	TEST(X86, PhysicalCores)
	{
		EXPECT_EQ(
			GetLibCpu().num_cores,
			GetMyCpu().PhysicalCores
		);
	}

	TEST(X86, Stepping)
	{
		EXPECT_EQ(
			GetLibCpu().stepping,
			GetMyCpu().Stepping
		);
	}

	TEST(X86, Vendor)
	{
		const std::string vendors[] = {
			"AuthenticAMD",
			"GenuineIntel",
			"HygonGenuine",
		};

		EXPECT_EQ(
			std::string(GetLibCpu().vendor_str),
			std::string(reinterpret_cast<const char(&)[13]>(GetMyCpu().VendorString))
		);

		EXPECT_EQ(
			GetLibCpu().vendor,
			static_cast<int32>(GetMyCpu().Vendor) - 1
		);
	}
}
