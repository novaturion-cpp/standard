﻿// ReSharper disable CppClangTidyBugproneReservedIdentifier \
// CppInconsistentNaming \
// CppClangTidyClangDiagnosticReservedIdentifier
#pragma once
#include "standard/platform/type/uint.hpp"


namespace Std::_Internal
{
	/// @brief	Extract bit from value
	template<class T>
	static inline constexpr T ExtractBit(
		const uint32 value,
		const uint8 position
	)
	{
		return static_cast<T>((value >> position) & 0x1);
	}

	/// @brief	Extract bits from value in range [lowest, highest]
	template<class T>
	static inline constexpr T ExtractBits(
		const uint32 value,
		const uint8 lowest,
		const uint8 highest
	)
	{
		const auto mask = (1 << (highest - lowest + 1)) - 1;
		return static_cast<T>((value >> lowest) & mask);
	}
}


// ReSharper restore CppClangTidyBugproneReservedIdentifier \
// CppInconsistentNaming \
// CppClangTidyClangDiagnosticReservedIdentifier
