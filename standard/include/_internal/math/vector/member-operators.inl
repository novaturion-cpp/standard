﻿template<class TOther>
	requires (
		requires
		{
			TOther::Size;
			TOther::ValueType;
		} &&
		TOther::Size == Size &&
		IsCastableValue<typename TOther::ValueType, TNumeric>
	)
constexpr VectorT& operator=(
	const TOther& other
)
{
	for (auto i = 0; i < Size; ++i)
	{
		(*this)[i] = static_cast<TNumeric>(other[i]);
	}
	return *this;
}

constexpr TNumeric& operator[](
	const uintSize index
)
{
	return Array[index];
}

constexpr const TNumeric& operator[](
	const uintSize index
) const
{
	return Array[index];
}


constexpr VectorT& operator++()
{
	*this += VectorT(1);
	return *this;
}

constexpr VectorT& operator--()
{
	*this -= VectorT(1);
	return *this;
}

constexpr VectorT operator++(
	int
)
{
	auto result = VectorT(*this);
	++*this;
	return result;
}

constexpr VectorT operator--(
	int
)
{
	auto result = VectorT(*this);
	--*this;
	return result;
}


template<class TValue = TNumeric>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator+=(
	TValue value
)
{
	*this += VectorT(static_cast<TNumeric>(value));
	return *this;
}

template<class TValue = TNumeric>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator-=(
	TValue value
)
{
	*this -= VectorT(static_cast<TNumeric>(value));
	return *this;
}

template<class TValue = TNumeric>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator*=(
	TValue value
)
{
	*this *= VectorT(static_cast<TNumeric>(value));
	return *this;
}

template<class TValue = TNumeric>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator/=(
	TValue value
)
{
	*this /= VectorT(static_cast<TNumeric>(value));
	return *this;
}

template<class TValue = TNumeric>
	requires (IsIntValue<TNumeric> && IsIntValue<TValue>)
constexpr VectorT& operator%=(
	TValue value
)
{
	*this %= VectorT(static_cast<TNumeric>(value));
	return *this;
}


constexpr VectorT& operator+=(
	const VectorT other
)
{
	*this = VectorT(Add<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}

constexpr VectorT& operator-=(
	const VectorT other
)
{
	*this = VectorT(Subtract<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}

constexpr VectorT& operator*=(
	const VectorT other
)
{
	*this = VectorT(Multiply<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}

constexpr VectorT& operator/=(
	const VectorT other
)
{
	*this = VectorT(Divide<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}

template<class = void>
	requires (IsIntValue<TNumeric>)
constexpr VectorT& operator%=(
	const VectorT other
)
{
	this->Simd = Mod<SimdType, ValueType>(this->Simd, other.Simd);
	return *this;
}


template<class TValue>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator&=(
	TValue value
)
{
	*this &= VectorT(static_cast<TNumeric>(value));
	return *this;
}

template<class TValue>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator|=(
	TValue value
)
{
	*this |= VectorT(static_cast<TNumeric>(value));
	return *this;
}

template<class TValue>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator^=(
	TValue value
)
{
	*this ^= VectorT(static_cast<TNumeric>(value));
	return *this;
}

template<class TValue>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator<<=(
	TValue value
)
{
	*this <<= VectorT(static_cast<TNumeric>(value));
	return *this;
}

template<class TValue>
	requires (IsCastableValue<TValue, TNumeric>)
constexpr VectorT& operator>>=(
	TValue value
)
{
	*this >>= VectorT(static_cast<TNumeric>(value));
	return *this;
}

constexpr VectorT& operator&=(
	const VectorT other
)
{
	*this = VectorT(BitAnd<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}

constexpr VectorT& operator|=(
	const VectorT other
)
{
	*this = VectorT(BitOr<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}

constexpr VectorT& operator^=(
	const VectorT other
)
{
	*this = VectorT(BitXor<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}

constexpr VectorT& operator<<=(
	const VectorT other
)
{
	*this = VectorT(ShiftLeft<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}

constexpr VectorT& operator>>=(
	const VectorT other
)
{
	*this = VectorT(ShiftRight<SimdType, ValueType>(this->Simd, other.Simd));
	return *this;
}
