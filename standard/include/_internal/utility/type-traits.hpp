﻿// ReSharper disable CppFunctionIsNotImplemented \
// CppClangTidyClangDiagnosticReservedIdentifier \
// CppClangTidyBugproneReservedIdentifier \
// CppInconsistentNaming
#pragma once
// todo: should internal/ include standard/?
#include "standard/platform/type/char.hpp"
#include "standard/platform/type/float.hpp"
#include "standard/platform/type/int.hpp"
#include "standard/platform/type/null.hpp"
#include "standard/platform/type/uint.hpp"
#include "standard/utility/type/bool-constant.hpp"
#include "standard/utility/type/void.hpp"


namespace Std::_Internal
{
	template<class T>
	constexpr T DeclareValue() noexcept;

	template<class T>
	constexpr T& DeclareLValue() noexcept;

	template<class T>
	constexpr T&& DeclareRValue() noexcept;

	template<class TTo>
	constexpr void ImplicitCast(
		TTo
	) noexcept;


	template<class T>
	inline constexpr auto IsConst = false;

	template<class T>
	inline constexpr auto IsConst<const T> = true;

	template<class T>
	inline constexpr auto IsVoid = false;

	template<>
	inline constexpr auto IsVoid<void> = true;

	template<class T>
	inline constexpr auto IsLValueReference = false;

	template<class T>
	inline constexpr auto IsLValueReference<T&> = true;

	template<class T>
	inline constexpr auto IsRValueReference = false;

	template<class T>
	inline constexpr auto IsRValueReference<T&&> = true;

	template<class T>
	inline constexpr bool IsReference = IsLValueReference<T> || IsRValueReference<T>;

	// [C4180] qualifier applied to function type has no meaning; ignored
	// Only function types and reference types can't be const-qualified
	#pragma warning(push)
	#pragma warning(disable: 4180)

	template<class T>
	inline constexpr bool IsFunction = !IsConst<const T> && !IsReference<T>;

	#pragma warning(pop)


	template<class T, uintSize VDimension = 0>
	inline constexpr uintSize ArraySize = 0;

	template<class T, uintSize VDimension>
	inline constexpr uintSize ArraySize<T[], VDimension> = ArraySize<T, VDimension - 1>;

	template<class T, uintSize VSize>
	inline constexpr auto ArraySize<T[VSize], 0> = VSize;

	template<class T, uintSize VSize, uintSize VDimension>
	inline constexpr uintSize ArraySize<T[VSize], VDimension> = ArraySize<T, VDimension - 1>;

	template<class T>
	inline constexpr uintSize ArrayRank = 0;

	template<class T>
	inline constexpr uintSize ArrayRank<T[]> = ArrayRank<T> + 1;

	template<class T, uintSize VSize>
	inline constexpr uintSize ArrayRank<T[VSize]> = ArrayRank<T> + 1;

	template<class T>
	inline constexpr auto IsArray = false;

	template<class T>
	inline constexpr auto IsArray<T[]> = true;

	template<class T, uintSize VSize>
	inline constexpr auto IsArray<T[VSize]> = true;

	template<class T>
	inline constexpr auto IsBool = false;

	template<>
	inline constexpr auto IsBool<bool> = true;

	template<class>
	inline constexpr auto IsBoundedArray = false;

	template<class T, uintSize VSize>
	inline constexpr auto IsBoundedArray<T[VSize]> = true;

	template<class>
	inline constexpr auto IsUnboundedArray = false;

	template<class T>
	inline constexpr auto IsUnboundedArray<T[]> = true;

	// [C4244] conversion from 'TFrom' to 'TTo', possible loss of data
	#pragma warning(push)
	#pragma warning(disable: 4244)

	template<class TFrom, class TTo, bool = IsVoid<TFrom> || IsFunction<TTo> || IsArray<TTo>>
	inline constexpr bool IsCastable = IsVoid<TTo>;

	template<class TFrom, class TTo>
		requires requires { ImplicitCast<TTo>(DeclareRValue<TFrom>()); }
	inline constexpr auto IsCastable<TFrom, TTo, false> = true;

	template<class TFrom, class TTo, bool = IsCastable<TFrom, TTo>, bool = IsVoid<TTo>>
	inline constexpr auto IsCastableNoThrow = noexcept(ImplicitCast<TTo>(DeclareRValue<TFrom>()));

	template<class TFrom, class TTo, bool VIsVoid>
	inline constexpr auto IsCastableNoThrow<TFrom, TTo, false, VIsVoid> = false;

	template<class TFrom, class TTo>
	inline constexpr auto IsCastableNoThrow<TFrom, TTo, true, true> = true;

	#pragma warning(pop)

	template<class T>
	inline constexpr auto IsChar = false;

	template<>
	inline constexpr auto IsChar<char16> = true;

	template<>
	inline constexpr auto IsChar<char32> = true;

	template<>
	inline constexpr auto IsChar<char8> = true;

	template<class T>
	inline constexpr auto IsEnum = __is_enum(T);

	template<class T>
	inline constexpr auto IsFieldPointer = false;

	template<class T, class TPointer>
	inline constexpr bool IsFieldPointer<T TPointer::*> = !IsFunction<T>;

	template<class T>
	inline constexpr auto IsFloat = false;

	template<>
	inline constexpr auto IsFloat<float128> = true;

	template<>
	inline constexpr auto IsFloat<float32> = true;

	template<>
	inline constexpr auto IsFloat<float64> = true;

	template<class T>
	inline constexpr auto IsInteger = false;

	template<>
	inline constexpr auto IsInteger<int8> = true;

	template<>
	inline constexpr auto IsInteger<int16> = true;

	template<>
	inline constexpr auto IsInteger<int32> = true;

	template<>
	inline constexpr auto IsInteger<int64> = true;

	template<>
	inline constexpr auto IsInteger<uint8> = true;

	template<>
	inline constexpr auto IsInteger<uint16> = true;

	template<>
	inline constexpr auto IsInteger<uint32> = true;

	template<>
	inline constexpr auto IsInteger<uint64> = true;

	template<class T>
	inline constexpr auto IsMethodPointer = false;

	template<class T, class TPointer>
	inline constexpr bool IsMethodPointer<T TPointer::*> = IsFunction<T>;

	template<class T>
	inline constexpr auto IsNull = false;

	template<>
	inline constexpr auto IsNull<NullType> = true;

	template<class T>
	inline constexpr bool IsNumeric = IsInteger<T> || IsFloat<T>;

	template<class T>
	inline constexpr auto IsPointer = false;

	template<class T>
	inline constexpr auto IsPointer<T*> = true;

	template<class T>
	inline constexpr auto IsScalar = IsNumeric<T> || IsEnum<T> || IsPointer<T> || IsFieldPointer<T> || IsNull<T>;

	template<class TA, class TB>
	inline constexpr auto IsSame = false;

	template<class T>
	inline constexpr auto IsSame<T, T> = true;

	template<class T>
	inline constexpr auto IsSigned = false;

	template<class T>
		requires (IsInteger<T>)
	inline constexpr bool IsSigned<T> = static_cast<T>(-1) < static_cast<T>(0);

	template<class TSpecialization, template <class...> class T>
	inline constexpr auto IsSpecialization = false;

	template<class... TArgs, template <class...> class T>
	inline constexpr auto IsSpecialization<T<TArgs...>, T> = true;

	template<class T>
	inline constexpr auto IsUnsigned = false;

	template<class T>
		requires (IsInteger<T>)
	inline constexpr bool IsUnsigned<T> = static_cast<T>(-1) > static_cast<T>(0);

	template<class T>
	inline constexpr auto IsVolatile = false;

	template<class T>
	inline constexpr auto IsVolatile<volatile T> = true;


	template<class T>
		requires requires
		{
			T::Value;
			ImplicitCast<bool>(T::Value);
		}
	using Not = BoolConstant<!T::Value>;


	template<bool VCondition, class TTrue, class TFalse>
	struct If
	{
		using Type = TTrue;
	};


	template<class TTrue, class TFalse>
	struct If<false, TTrue, TFalse>
	{
		using Type = TFalse;
	};


	template<class...>
	struct And : TrueConstant {};


	template<class T>
		requires requires
		{
			T::Value;
			ImplicitCast<bool>(T::Value);
		}
	struct And<T> : T {};


	template<class T, class... TArgs>
		requires requires
		{
			T::Value;
			ImplicitCast<bool>(T::Value);
		}
	struct And<T, TArgs...> : If<T::Value, And<TArgs...>, T>::Type {};


	template<class...>
	struct Or : FalseConstant {};


	template<class T>
		requires requires
		{
			T::Value;
			ImplicitCast<bool>(T::Value);
		}
	struct Or<T> : T {};


	template<class T, class... TArgs>
		requires requires
		{
			T::Value;
			ImplicitCast<bool>(T::Value);
		}
	struct Or<T, TArgs...> : If<T::Value, T, Or<TArgs...>>::Type {};


	template<class T>
	struct RemoveReference
	{
		using Type = T;
	};


	template<class T>
	struct RemoveReference<T&&>
	{
		using Type = T;
	};


	template<class T>
	struct RemoveReference<T&>
	{
		using Type = T;
	};


	template<class T>
	struct RemoveArrayDimension
	{
		using Type = T;
	};


	template<class T>
	struct RemoveArrayDimension<T[]>
	{
		using Type = T;
	};


	template<class T, uintSize VLength>
	struct RemoveArrayDimension<T[VLength]>
	{
		using Type = T;
	};


	template<class T>
	struct RemoveConst
	{
		using Type = T;
	};


	template<class T>
	struct RemoveConst<const T>
	{
		using Type = T;
	};


	template<class T>
	struct RemoveVolatile
	{
		using Type = T;
	};


	template<class T>
	struct RemoveVolatile<volatile T>
	{
		using Type = T;
	};


	template<class T>
	using RemoveConstVolatile = typename RemoveVolatile<typename RemoveConst<T>::Type>::Type;


	template<class T>
	using RemoveConstVolatileReference = RemoveConstVolatile<typename RemoveReference<T>::Type>;


	template<class T>
	struct AddPointer
	{
		using Type = T;
	};


	template<class T>
		requires requires { DeclareValue<typename RemoveReference<T>::Type*>(); }
	struct AddPointer<T>
	{
		using Type = typename RemoveReference<T>::Type*;
	};


	template<class T>
	struct Decay
	{
	private:
		using U = typename RemoveReference<T>::Type;

	public:
		using Type = typename If<
			IsArray<U>,
			typename RemoveArrayDimension<U>::Type*,
			typename If<
				IsFunction<U>,
				typename AddPointer<U>::Type,
				RemoveConstVolatile<U>
			>::Type
		>::Type;
	};


	template<bool VCondition, class T = void>
	struct EnableIf {};


	template<class T>
	struct EnableIf<true, T>
	{
		using Type = T;
	};


	template<class T>
	struct AddConst
	{
		using Type = const T;
	};


	template<class T>
	struct AddLValueReference
	{
		using Type = T;
	};


	template<class T>
		requires requires { DeclareLValue<T>(); }
	struct AddLValueReference<T>
	{
		using Type = T&;
	};


	template<class T>
	struct AddRValueReference
	{
		using Type = T;
	};


	template<class T>
		requires requires { DeclareRValue<T>(); }
	struct AddRValueReference<T>
	{
		using Type = T&&;
	};


	template<class T>
	struct AddVolatile
	{
		using Type = volatile T;
	};


	template<uintSize VSize>
	struct ChangeSign {};


	template<>
	struct ChangeSign<1>
	{
		using Signed = int8;
		using Unsigned = uint8;
	};


	template<>
	struct ChangeSign<2>
	{
		using Signed = int16;
		using Unsigned = uint16;
	};


	template<>
	struct ChangeSign<4>
	{
		using Signed = int32;
		using Unsigned = uint32;
	};


	template<>
	struct ChangeSign<8>
	{
		using Signed = int64;
		using Unsigned = uint64;
	};


	template<class TA, class TB>
	using Conditional = decltype(false
		? DeclareRValue<TA>()
		: DeclareRValue<TB>());


	template<class TA, class TB>
	struct ConditionalConstReference {};


	template<class TA, class TB>
		requires requires { Conditional<const TA&, const TB&>(); }
	struct ConditionalConstReference<TA, TB>
	{
		using Type = RemoveConstVolatileReference<Conditional<const TA&, const TB&>>;
	};


	template<class TA, class TB>
	struct ConditionalDecayed : ConditionalConstReference<TA, TB> {};


	template<class TA, class TB>
		requires requires { Conditional<TA, TB>(); }
	struct ConditionalDecayed<TA, TB>
	{
		using Type = typename Decay<Conditional<TA, TB>>::Type;
	};


	template<class... TArgs>
	struct CommonType {};


	template<class... TArgs>
	using CommonTypeInternal = typename CommonType<TArgs...>::Type;


	template<>
	struct CommonType<> {};


	template<class T>
	struct CommonType<T> : CommonType<T, T> {};


	template<
		class TA,
		class TB,
		class TDecayedA = typename Decay<TA>::Type,
		class TDecayedB = typename Decay<TB>::Type>
	struct CommonType2 : CommonType<TDecayedA, TDecayedB> {};


	template<class TA, class TB>
	struct CommonType2<TA, TB, TA, TB> : ConditionalDecayed<TA, TB> {};


	template<class TVoid, class TA, class TB, class... TRest>
	struct CommonType3 {};


	template<class TA, class TB, class... TArgs>
	struct CommonType3<Void<CommonTypeInternal<TA, TB>>, TA, TB, TArgs...>
		: CommonType<CommonTypeInternal<TA, TB>, TArgs...> {};


	template<class TA, class TB>
	struct CommonType<TA, TB> : CommonType2<TA, TB> {};


	template<class TA, class TB, class... TArgs>
	struct CommonType<TA, TB, TArgs...> : CommonType3<void, TA, TB, TArgs...> {};


	template<class TFrom, class TTo>
	struct CopyReplaceConstVolatile
	{
		using Copy = TTo;
		using Replace = typename RemoveVolatile<typename RemoveConst<TTo>::Type>::Type;
	};


	template<class TFrom, class TTo>
	struct CopyReplaceConstVolatile<const TFrom, TTo>
	{
		using Copy = const TTo;
		using Replace = const typename RemoveVolatile<TTo>::Type;
	};


	template<class TFrom, class TTo>
	struct CopyReplaceConstVolatile<const volatile TFrom, TTo>
	{
		using Copy = const volatile TTo;
		using Replace = Copy;
	};


	template<class TFrom, class TTo>
	struct CopyReplaceConstVolatile<volatile TFrom, TTo>
	{
		using Copy = volatile TTo;
		using Replace = volatile typename RemoveConst<TTo>::Type;
	};


	template<uintSize VSize>
	struct CorrespondingFloat {};


	template<>
	struct CorrespondingFloat<1>
	{
		using Type = float32;
	};


	template<>
	struct CorrespondingFloat<2>
	{
		using Type = float32;
	};


	template<>
	struct CorrespondingFloat<4>
	{
		using Type = float32;
	};


	template<>
	struct CorrespondingFloat<8>
	{
		using Type = float64;
	};


	template<uintSize VSize>
	struct CorrespondingInt {};


	template<>
	struct CorrespondingInt<1>
	{
		using Type = int8;
	};


	template<>
	struct CorrespondingInt<2>
	{
		using Type = int16;
	};


	template<>
	struct CorrespondingInt<4>
	{
		using Type = int32;
	};


	template<>
	struct CorrespondingInt<8>
	{
		using Type = int64;
	};


	template<uintSize VSize>
	struct CorrespondingUint {};


	template<>
	struct CorrespondingUint<1>
	{
		using Type = uint8;
	};


	template<>
	struct CorrespondingUint<2>
	{
		using Type = uint16;
	};


	template<>
	struct CorrespondingUint<4>
	{
		using Type = uint32;
	};


	template<>
	struct CorrespondingUint<8>
	{
		using Type = uint64;
	};


	template<class T>
	struct EnumUnderlyingType {};


	template<class T>
		requires (__is_enum(T))
	struct EnumUnderlyingType<T>
	{
		using Type = __underlying_type(T);
	};


	template<class T>
	struct Identity
	{
		using Type = T;
	};


	template<class T>
	struct IsEnumClass : FalseConstant {};


	template<class T>
		requires (__is_enum(T))
	struct IsEnumClass<T> : BoolConstant<IsCastable<T, typename EnumUnderlyingType<T>::Type>> {};


	template<class T>
		requires (!IsBool<T> && (IsInteger<T> || IsChar<T> || IsEnum<T>))
	struct MakeSigned
	{
		using Type = typename CopyReplaceConstVolatile<T, typename ChangeSign<sizeof(T)>::Signed>::Copy;
	};


	template<class T>
		requires (!IsBool<T> && (IsInteger<T> || IsChar<T> || IsEnum<T>))
	struct MakeUnsigned
	{
		using Type = typename CopyReplaceConstVolatile<T, typename ChangeSign<sizeof(T)>::Unsigned>::Copy;
	};


	template<class T>
	struct RemoveArrayDimensions
	{
		using Type = T;
	};


	template<class T>
	struct RemoveArrayDimensions<T[]>
	{
		using Type = typename RemoveArrayDimensions<T>::Type;
	};


	template<class T, uintSize VLength>
	struct RemoveArrayDimensions<T[VLength]>
	{
		using Type = typename RemoveArrayDimensions<T>::Type;
	};


	template<class T>
	struct RemoveConstPointer
	{
		using Type = T;
	};


	template<class T>
	struct RemoveConstPointer<T* const>
	{
		using Type = T;
	};


	template<class T>
	struct RemovePointer
	{
		using Type = T;
	};


	template<class T>
	struct RemovePointer<T*>
	{
		using Type = T;
	};


	template<class T>
	struct RemoveVolatilePointer
	{
		using Type = T;
	};


	template<class T>
	struct RemoveVolatilePointer<T* volatile>
	{
		using Type = T;
	};


	template<class T>
	using RemoveAll = RemoveConstVolatile<typename RemovePointer<typename RemoveReference<T>::Type>::Type>;


	namespace InvokeStrategy
	{
		struct InvokeField {};


		struct InvokeFieldPointer {};


		struct InvokeMethod {};


		struct InvokeMethodPointer {};


		struct InvokeFunction {};
	}


	template<class TPointer, class TOwner>
	constexpr bool TestIsInvocableNoThrow(
		InvokeStrategy::InvokeFieldPointer
	)
	{
		return noexcept((*DeclareRValue<TOwner>()).*DeclareRValue<TPointer>());
	}


	// template<class>
	// constexpr FalseConstant TestDestructor();
	//
	// template<class T>
	// 	requires requires { DeclareRValue<T&>().~T(); }
	// constexpr TrueConstant TestDestructor();
	//
	// template<class>
	// constexpr FalseConstant TestDestructorNoThrow() noexcept;
	//
	// template<class T>
	// 	requires (noexcept(DeclareRValue<T&>().~T()) && requires { DeclareRValue<T&>().~T(); })
	// constexpr TrueConstant TestDestructorNoThrow() noexcept;

	template<class TTo, class TFrom>
	inline constexpr auto IsAssignable = __is_assignable(TTo, TFrom);

	template<class TTo, class TFrom>
	inline constexpr auto IsAssignableNoThrow = __is_nothrow_assignable(TTo, TFrom);

	template<class T, class... TArgs>
	inline constexpr auto IsConstructible = __is_constructible(T, TArgs...);

	template<class T, class... TArgs>
	inline constexpr auto IsConstructibleNoThrow = __is_nothrow_constructible(T, TArgs...);

	template<class TParent, class TDerived>
	inline constexpr auto IsParent = __is_base_of(TParent, TDerived);

	template<
		class T,
		bool = IsVoid<T> || IsFunction<T> || IsUnboundedArray<T>,
		bool = IsReference<T> || IsScalar<T>
	>
	inline constexpr auto IsDestructible = false;

	template<class T>
		requires requires { DeclareRValue<T&>().~T(); }
	inline constexpr auto IsDestructible<T, false, false> = true;
	// decltype(TestDestructor<RemoveAll<T>>())::Value;

	template<class T>
	inline constexpr auto IsDestructible<T, false, true> = true;

	template<class T>
	inline constexpr auto IsDestructible<T, true, false> = false;

	template<
		class T,
		bool = IsVoid<T> || IsFunction<T> || IsUnboundedArray<T>,
		bool = IsReference<T> || IsScalar<T>
	>
	inline constexpr auto IsDestructibleNoThrow = false;

	template<class T>
		requires (noexcept(DeclareRValue<T&>().~T()) && requires { DeclareRValue<T&>().~T(); })
	inline constexpr auto IsDestructibleNoThrow<T, false, false> = true;
	// decltype(TestDestructorNoThrow<RemoveAll<T>>())::Value;

	template<class T>
	inline constexpr auto IsDestructibleNoThrow<T, false, true> = true;

	template<class T>
	inline constexpr auto IsDestructibleNoThrow<T, true, false> = false;

	template<class T>
	inline constexpr auto IsMoveAssignable = IsAssignable<
		typename AddLValueReference<T>::Type,
		typename AddRValueReference<T>::Type
	>;

	template<class T>
	inline constexpr auto IsMoveAssignableNoThrow = IsAssignableNoThrow<
		typename AddLValueReference<T>::Type,
		typename AddRValueReference<T>::Type
	>;

	template<class T>
	inline constexpr auto IsMoveConstructible = IsConstructible<T, typename AddRValueReference<T>::Type>;

	template<class T>
	inline constexpr auto IsMoveConstructibleNoThrow = IsConstructibleNoThrow<T, typename AddRValueReference<T>::Type>;


	template<class T>
	struct IsSwappable;


	template<class T>
	struct IsSwappableNoThrow;


	template<class T>
		requires (IsMoveConstructible<T> && IsMoveAssignable<T>)
	constexpr void Swap(
		T& a,
		T& b
	) noexcept(IsMoveConstructibleNoThrow<T> && IsMoveAssignableNoThrow<T>);


	template<typename T, uintSize VSize>
		requires (IsSwappable<T>::Value)
	constexpr void Swap(
		T (&a)[VSize],
		T (&b)[VSize]
	) noexcept (IsSwappableNoThrow<T>::Value);


	template<class TA, class TB>
	struct IsSwapDoNotThrow : BoolConstant<
			noexcept(Swap(DeclareRValue<TA>(), DeclareRValue<TB>())) &&
			noexcept(Swap(DeclareRValue<TB>(), DeclareRValue<TA>()))
		> {};


	template<class T>
	struct IsSwappable : FalseConstant {};


	template<class T>
		requires requires { Swap(DeclareRValue<T&>(), DeclareRValue<T&>()); }
	struct IsSwappable<T> : TrueConstant {};


	template<class T>
	struct IsSwappableNoThrow : And<IsSwappable<T>, IsSwapDoNotThrow<T&, T&>> {};


	template<class TA, class TB>
	struct SwappableWithHelper : FalseConstant {};


	template<class TA, class TB>
		requires requires { Swap(DeclareRValue<TA>(), DeclareRValue<TB>()); }
	struct SwappableWithHelper<TA, TB> : TrueConstant {};


	template<class TA, class TB>
	using IsSwappableWith = And<SwappableWithHelper<TA, TB>, SwappableWithHelper<TB, TA>>;


	template<class TPointer, class TOwner>
	constexpr decltype(DeclareRValue<TOwner>().*DeclareRValue<TPointer>()) InvokeField();


	template<class TPointer, class TOwner>
	constexpr decltype((*DeclareRValue<TOwner>()).*DeclareRValue<TPointer>()) InvokeFieldPointer();


	template<class TPointer, class TOwner, class... TArgs>
	constexpr decltype((DeclareRValue<TOwner>().*DeclareRValue<TPointer>())(
		DeclareRValue<TArgs>()...
	)) InvokeMethod();


	template<class TPointer, class TOwner, class... TArgs>
	constexpr decltype(((*DeclareRValue<TOwner>()).*DeclareRValue<TPointer>())(
		DeclareRValue<TArgs>()...
	)) InvokeMethodPointer();


	template<class TFunction, class... TArgs>
	constexpr decltype(DeclareRValue<TFunction>()(DeclareRValue<TArgs>()...)) InvokeFunction();


	template<class TPointer, class TOwner>
	struct InvokeResultField {};


	template<class TReturn, class TClass, class TOwner>
		requires(IsSame<TClass, RemoveConstVolatileReference<TOwner>>
			|| IsParent<TClass, RemoveConstVolatileReference<TOwner>>
			&& requires { InvokeField<TReturn TClass::*, TOwner>(); })
	struct InvokeResultField<TReturn TClass::*, TOwner>
	{
		using Strategy = InvokeStrategy::InvokeField;
		using Pointer = TReturn TClass::*;
		using Type = decltype(InvokeField<TReturn TClass::*, TOwner>());
	};


	template<class TReturn, class TClass, class TOwner>
		requires(!(IsSame<TClass, RemoveConstVolatileReference<TOwner>>
				|| IsParent<TClass, RemoveConstVolatileReference<TOwner>>)
			&& requires { InvokeFieldPointer<TReturn TClass::*, TOwner>(); })
	struct InvokeResultField<TReturn TClass::*, TOwner>
	{
		using Strategy = InvokeStrategy::InvokeFieldPointer;
		using Pointer = TReturn TClass::*;
		using Type = decltype(InvokeFieldPointer<TReturn TClass::*, TOwner>());
	};


	template<class TPointer, class TOwner, class... TArgs>
	struct InvokeResultMethod {};


	template<class TReturn, class TClass, class TOwner, class... TArgs>
		requires (IsParent<TClass, typename RemoveReference<TOwner>::Type> && requires
		{
			InvokeMethod<TReturn TClass::*, TOwner, TArgs...>();
		})
	struct InvokeResultMethod<TReturn TClass::*, TOwner, TArgs...>
	{
		using Strategy = InvokeStrategy::InvokeMethod;
		using Pointer = TReturn TClass::*;
		using Type = decltype(InvokeMethod<TReturn TClass::*, TOwner, TArgs...>());
	};


	template<class TReturn, class TClass, class TOwner, class... TArgs>
		requires (!IsParent<TClass, typename RemoveReference<TOwner>::Type> && requires
		{
			InvokeMethodPointer<TReturn TClass::*, TOwner, TArgs...>();
		})
	struct InvokeResultMethod<TReturn TClass::*, TOwner, TArgs...>
	{
		using Strategy = InvokeStrategy::InvokeMethodPointer;
		using Pointer = TReturn TClass::*;
		using Type = decltype(InvokeMethodPointer<TReturn TClass::*, TOwner, TArgs...>());
	};


	template<class T>
	class ReferenceWrapper;


	template<class T, class = RemoveConstVolatileReference<T>>
	struct UnwrapReference
	{
		using Type = T;
	};


	template<class T, class TInner>
	struct UnwrapReference<T, ReferenceWrapper<TInner>>
	{
		using Type = TInner&;
	};


	template<bool VField, bool VMethod, class TPointer, class... TArgs>
	struct InvokeResult {};


	template<class TPointer, class... TArgs>
		requires requires { InvokeFunction<TPointer, TArgs...>(); }
	struct InvokeResult<false, false, TPointer, TArgs...>
	{
		using Strategy = InvokeStrategy::InvokeFunction;
		using Type = decltype(InvokeFunction<TPointer, TArgs...>());
	};


	template<class TPointer, class TArg, class... TArgs>
	struct InvokeResult<false, true, TPointer, TArg, TArgs...> : InvokeResultMethod<
			typename Decay<TPointer>::Type,
			typename UnwrapReference<TArg>::Type, TArgs...
		> {};


	template<class TPointer, class TArg>
	struct InvokeResult<true, false, TPointer, TArg> : InvokeResultField<
			typename Decay<TPointer>::Type,
			typename UnwrapReference<TArg>::Type
		> {};


	template<class T, class... TArgs>
	using InvokeResultInternal = InvokeResult<
		IsFieldPointer<typename RemoveReference<T>::Type>,
		IsMethodPointer<typename RemoveReference<T>::Type>,
		T,
		TArgs...
	>;


	template<class TPointer, class TOwner>
	constexpr bool TestIsInvocableNoThrow(
		InvokeStrategy::InvokeField
	)
	{
		return noexcept(DeclareRValue<UnwrapReference<TOwner>>().*DeclareRValue<TPointer>());
	}


	template<class TPointer, class TOwner, class... TArgs>
	constexpr bool TestIsInvocableNoThrow(
		InvokeStrategy::InvokeMethod
	)
	{
		return noexcept((DeclareRValue<UnwrapReference<TOwner>>().*DeclareRValue<TPointer>()));
	}


	template<class TPointer, class TOwner, class... TArgs>
	constexpr bool TestIsInvocableNoThrow(
		InvokeStrategy::InvokeMethodPointer
	)
	{
		return noexcept(((*DeclareRValue<TOwner>()).*DeclareRValue<TPointer>())(DeclareRValue<TArgs>()...));
	}


	template<class TPointer, class... TArgs>
	constexpr bool TestIsInvocableNoThrow(
		InvokeStrategy::InvokeFunction
	)
	{
		return noexcept(DeclareRValue<TPointer>()(DeclareRValue<TArgs>()...));
	}


	template<class T, class... TArgs>
	inline constexpr auto IsInvocable = false;


	template<class T, class... TArgs>
		requires requires { InvokeResultInternal<T, TArgs...>::Type; }
	inline constexpr auto IsInvocable<T, TArgs...> = true;


	template<class T, class... TArgs>
	inline constexpr auto IsInvocableNoThrow = false;


	template<class T, class... TArgs>
		requires requires { InvokeResultInternal<T, TArgs...>::Type; }
	inline constexpr bool IsInvocableNoThrow<T, TArgs...> =
		TestIsInvocableNoThrow<T, TArgs...>(
			typename InvokeResultInternal<T, TArgs...>::Strategy {}
		);


	template<class TReturn, class T, class... TArgs>
	inline constexpr auto IsInvocableReturn = false;


	template<class TReturn, class T, class... TArgs>
		requires requires { InvokeResultInternal<T, TArgs...>::Type; }
	inline constexpr bool IsInvocableReturn<TReturn, T, TArgs...> =
		IsVoid<TReturn> || IsCastable<typename InvokeResultInternal<T, TArgs...>::Type, TReturn>;


	template<class TReturn, class T, class... TArgs>
	inline constexpr auto IsInvocableReturnNoThrow = false;


	template<class TReturn, class T, class... TArgs>
		requires requires { InvokeResultInternal<T, TArgs...>::Type; }
	inline constexpr bool IsInvocableReturnNoThrow<TReturn, T, TArgs...> =
		TestIsInvocableNoThrow<T, TArgs...>(
			typename InvokeResultInternal<T, TArgs...>::Strategy {}
		) &&
		(IsVoid<TReturn> || IsCastableNoThrow<typename InvokeResultInternal<T, TArgs...>::Type, TReturn>);
}


// ReSharper restore CppFunctionIsNotImplemented \
// CppClangTidyClangDiagnosticReservedIdentifier \
// CppClangTidyBugproneReservedIdentifier \
// CppInconsistentNaming
