﻿// ReSharper disable CppClangTidyBugproneReservedIdentifier \
// CppInconsistentNaming
#pragma once
// todo: should internal/ include standard/?
#include "standard/platform/type/uint.hpp"


namespace Std::_Internal
{
	template<class T>
	struct _NumericLimitsBase
	{
		static inline constexpr auto IsSpecialized = false;

		static inline constexpr auto IsInteger = false;
		static inline constexpr auto IsFloat = false;

		static inline constexpr auto IsSigned = static_cast<T>(-1) < static_cast<T>(0);
		static inline constexpr auto IsBounded = false;

		static inline constexpr auto IsTraps = false;
		static inline constexpr auto IsHandleOverflow = false;
		static inline constexpr auto IsExactRepresentation = false;

		static inline constexpr auto Size = sizeof(T) * 8;
		static inline constexpr auto RealSize = Size - IsSigned;

		static inline constexpr auto Digits = 0;

		static inline constexpr auto Radix = 2;

		static inline constexpr T Max = 0;
		static inline constexpr T Min = 0;
	};


	template<class T>
	struct _NumericLimitsInt : _NumericLimitsBase<T>
	{
		static inline constexpr auto IsSpecialized = true;

		static inline constexpr auto IsInteger = true;

		static inline constexpr auto IsBounded = true;

		static inline constexpr auto IsExactRepresentation = true;

		static inline constexpr T Max = static_cast<T>(
			static_cast<T>(-1) < static_cast<T>(0)
			? ~(1_ui64 << (sizeof(T) * 8 - 1))
			: -1
		);
		static inline constexpr T Min = static_cast<T>(
			static_cast<T>(-1) < static_cast<T>(0)
			? 1_ui64 << (sizeof(T) * 8 - 1)
			: 0
		);
	};


	template<class T, uint8 VExponentSize>
	struct _NumericLimitsFloat : _NumericLimitsBase<T>
	{
		enum class EDenormStyle : int8
		{
			Indeterminate = -1,
			Absent = 0,
			Present = 1
		};


		enum class ERoundStyle : int8
		{
			Indeterminate = -1,
			ToZero = 0,
			ToNearest = 1,
			ToInfinity = 2,
			ToNegativeInfinity = 3
		};


		static inline constexpr auto IsSpecialized = true;

		static inline constexpr auto IsFloat = true;

		static inline constexpr auto IsSigned = true;
		static inline constexpr auto IsBounded = true;

		static inline constexpr auto IsHandleOverflow = false;

		/// @brief	IEC 559/IEEE 754 floating-point types
		static inline constexpr auto IsIee754 = true;

		/// @brief	Detect loss of precision as
		/// denormalization loss rather than inexact result
		static inline constexpr auto HasDenormLoss = false;

		static inline constexpr auto HasInfinity = true;
		static inline constexpr auto HasNegativeInfinity = true;

		static inline constexpr auto HasQuietNan = true;
		static inline constexpr auto HasSignalingNan = true;

		static inline constexpr auto Size = sizeof(T) * 8;
		static inline constexpr auto RealSize = Size - 1;

		static inline constexpr auto DigitsMax = 0;

		static inline constexpr auto ExponentSize = VExponentSize;
		static inline constexpr auto ExponentMax = (1 << (ExponentSize - 1));
		static inline constexpr auto ExponentRealMax = ExponentMax - 1;
		static inline constexpr auto ExponentMin = 1 - ExponentMax;
		static inline constexpr auto ExponentRealMin = 1 - ExponentRealMax;
		static inline constexpr auto ExponentBias = 1 - ExponentMax;

		static inline constexpr auto MantissaSize = Size - ExponentSize;
		static inline constexpr auto MantissaRealSize = RealSize - ExponentSize;

		static inline constexpr auto MantissaMask = (1_uis << MantissaRealSize) - 1;
		static inline constexpr auto ExponentMask = ((1_uis << RealSize) - 1) & ~MantissaMask;

		static inline constexpr T Epsilon = 0;
		static inline constexpr T Infinity = 0;
		static inline constexpr T QuietNan = 0;
		static inline constexpr T SignalingNan = 0;

		static inline constexpr T Max = 0;
		static inline constexpr T Min = 0;
		static inline constexpr T RealMin = 0;

		static inline constexpr T RoundError = 0.5;

		static inline constexpr ERoundStyle RoundStyle = ERoundStyle::ToNearest;
		static inline constexpr EDenormStyle DenormStyle = EDenormStyle::Present;
	};
}


// ReSharper restore CppClangTidyBugproneReservedIdentifier \
// CppInconsistentNaming
