﻿#pragma once
#include "architecture.hpp"

#if ARCH_IS_ARM || ARCH_IS_ARM64
#include "cpu/arm.hpp"
#elif ARCH_IS_X86 || ARCH_IS_X64
#include "cpu/x86.hpp"
#endif
