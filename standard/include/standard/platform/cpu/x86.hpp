﻿#pragma once
#include "standard/platform/type.hpp"
#include "standard/platform/intrinsic/x86/cpuid.hpp"
#include "_internal/bits.hpp"

#if !ARCH_IS_X86 && !ARCH_IS_X64
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::Cpu
{
	class X86
	{
		//	AMD
		//		https://www.amd.com/en/support/tech-docs?keyword=Processor+Programming+Reference
		//		https://www.amd.com/en/support/tech-docs?keyword=Architecture+Programmer%27s+Manual
		//	Intel
		//		https://www.intel.com/content/www/us/en/developer/articles/technical/intel-sdm.html
		//	Other
		//		https://en.wikipedia.org/wiki/CPUID
	public:
		struct ERegisterId
		{
			enum : uint8
			{
				Eax,
				Ebx,
				Ecx,
				Edx,
			};
		};


		struct ELeafId
		{
			enum : uint8
			{
				Function0,
				Function1,
				Function2,
				Function7,
				Function7Ecx1,
				Function80000000,
				Function80000001,
				Function80000002,
				Function80000003,
				Function80000004,
				Function80000021,
			};
		};

	public:
		enum class ECacheLevel : uint8
		{
			Unknown,
			L1Data,
			L1Instructions,
			L2Unified,
			L3Unified,
		};


		enum class EVendor : uint8
		{
			Unknown,
			Intel,
			Amd,
		};

	public:
		struct Cache
		{
			uint32 Size = 0;
			uint16 LineSize = 0;
			uint8 Associativity = 0;

			ECacheLevel Level = {};

			bool IsPresent = false;
		};


		struct Features
		{
			/// @brief	Advanced BitManipulation (LZCNT instruction) support
			/// @see	{@link Std::Platform::Intrinsic::Abm}
			bool Abm : 1;
			/// @brief	Linear Address Masking (LAM) support for Intel,\n
			/// Upper Address Ignore support (UAI) for AMD,\n
			/// Top Byte Ignore support (TBI) for ARM
			bool AddressMasking : 1;
			/// @brief	Multi-precision Add-Carry instructions support
			/// @see	{@link Std::Platform::Intrinsic::Adx}
			bool Adx : 1;
			/// @brief	Advanced Encryption Standard instructions support
			/// @see	{@link Std::Platform::Intrinsic::Aes}
			bool Aes : 1;
			/// @brief	Advanced Matrix Extensions
			/// 16-bit Brain float instructions support
			/// todo: add link
			bool AmxBrainFloat16 : 1;
			/// @brief	Advanced Matrix Extensions
			/// Complex tiles instructions support
			/// todo: add link
			bool AmxComplex : 1;
			/// @brief	Advanced Matrix Extensions
			/// 16-bit IEEE float instructions support
			/// todo: add link
			bool AmxFloat16 : 1;
			/// @brief	Advanced Matrix Extensions
			/// 8-bit integer instructions support
			/// todo: add link
			bool AmxInt8 : 1;
			/// @brief	Advanced Matrix Extensions
			/// load/store instructions support
			/// todo: add link
			bool AmxTile : 1;
			/// @brief	Advanced Vector Extensions support
			/// @see	{@link Std::Platform::Intrinsic::Avx}
			bool Avx : 1;
			/// @brief	Advanced Vector Extensions 2 support
			/// @see	{@link Std::Platform::Intrinsic::Avx2}
			bool Avx2 : 1;
			/// @brief	Advanced Vector Extensions 512
			/// bit algorithms support
			/// todo: add link
			bool Avx512BitAlgorithms : 1;
			/// @brief	Advanced Vector Extensions 512
			/// Byte Manipulation Instruction Set support
			/// todo: add link
			bool Avx512Bmi : 1;
			/// @brief	Advanced Vector Extensions 512
			/// Byte Manipulation Instruction Set 2 support
			/// todo: add link
			bool Avx512Bmi2 : 1;
			/// @brief	/// todo: add link
			bool Avx512BrainFloat16 : 1;
			/// @brief	Advanced Vector Extensions 512
			/// 8 and 16-bit integer instructions support
			/// todo: add link
			bool Avx512ByteWord : 1;
			/// @brief	Advanced Vector Extensions 512
			/// Conflict Detection instructions support
			/// todo: add link
			bool Avx512ConflictDetection : 1;
			/// @brief	Advanced Vector Extensions 512
			/// 32 and 64-bit integer instructions support
			/// todo: add link
			bool Avx512DoubleQuad : 1;
			/// @brief	Advanced Vector Extensions 512
			/// exponential and reciprocal instructions support
			/// todo: add link
			bool Avx512ExponentialReciprocal : 1;
			/// @brief	Advanced Vector Extensions 512 Foundation support
			/// todo: add link
			bool Avx512 : 1;
			/// @brief	Advanced Vector Extensions 512
			/// 16-bit float instructions support
			/// todo: add link
			bool Avx512Float16 : 1;
			/// @brief	Advanced Vector Extensions 512 4-operand packed
			/// 32-bit float Fused Multiply Accumulation instructions support
			/// todo: add link
			bool Avx512FmaPs4 : 1;
			/// @brief	Advanced Vector Extensions 512
			/// Integer Fused Multiply-Add instructions support
			/// todo: add link
			bool Avx512FmaInt : 1;
			/// @brief	Advanced Vector Extensions 512
			/// intersection instructions support
			/// todo: add link
			bool Avx512IntersectInt : 1;
			/// @brief	Advanced Vector Extensions 512
			/// 4-operand Neural Network instructions support
			/// todo: add link
			bool Avx512NeuralNetwork4 : 1;
			/// @brief	Advanced Vector Extensions 512
			/// Neural Network instructions support
			/// todo: add link
			bool Avx512NeuralNetwork : 1;
			/// @brief	Advanced Vector Extensions 512
			/// population count instructions support
			/// todo: add link
			bool Avx512PopulationCount : 1;
			/// @brief	Advanced Vector Extensions 512
			/// prefetch instructions support
			/// todo: add link
			bool Avx512Prefetch : 1;
			/// @brief	Advanced Vector Extensions 512
			/// 128 and 256-bit instructions support
			/// todo: add link
			bool Avx512VectorLength : 1;
			/// @brief	Advanced Vector Extensions
			/// Neural Network instructions support
			/// todo: add link
			bool AvxNeuralNetwork : 1;
			/// @brief	Advanced Vector Extensions 16-bit integer
			/// Neural Network instructions support
			/// todo: add link
			bool AvxNeuralNetworkInt16 : 1;
			/// @brief	Advanced Vector Extensions 8-bit integer
			/// Neural Network instructions support
			/// todo: add link
			bool AvxNeuralNetworkInt8 : 1;
			/// @brief	Advanced Vector Extensions No Exception
			/// Float Conversion instructions support
			bool AvxNoExceptionFloatConvert : 1;
			/// @brief	Bit Manipulation Instruction Set 1 support
			/// @see	{@link Std::Platform::Intrinsic::Bmi}
			bool Bmi : 1;
			/// @brief	Bit Manipulation Instruction Set 2 support
			/// @see	{@link Std::Platform::Intrinsic::Bmi2}
			bool Bmi2 : 1;
			/// @brief	CLFLUSH instruction support
			/// @see	{@link Std::Platform::Intrinsic::Sse2}
			bool CacheLineFlush : 1;
			/// @brief	CLFLUSHOPT instruction support
			/// todo: add link
			bool CacheLineFlushOptimized : 1;
			/// @brief	CLWB instruction support
			/// todo: add link
			bool CacheLineWriteBack : 1;
			/// @brief	VPCLMULQDQ instruction support
			/// @see	{@link Std::Platform::Intrinsic::ClMul}
			bool CarryLessMultiplication256 : 1;
			/// @brief	PCLMULQDQ instruction support
			/// @see	{@link Std::Platform::Intrinsic::ClMul}
			bool CarryLessMultiplication64 : 1;
			/// @brief	CMPXCHG16B instruction support
			/// todo: add link
			bool CompareExchange16 : 1;
			/// @brief	CMPXCHG8B instruction support
			/// todo: add link
			bool CompareExchange8 : 1;
			/// @brief	CMOV/FCMOV instructions support
			/// todo: add link
			bool ConditionalMove : 1;
			/// @brief	Direct Cache Access for DMA writes support
			bool DirectCacheAccess : 1;
			/// @brief	Enhanced REP MOVSB/STOSB instructions support
			/// todo: add link
			bool EnhancedRepMove : 1;
			/// @brief	Float16 conversion instructions support
			/// @see	{@link Std::Platform::Intrinsic::F16c}
			bool F16c : 1;
			/// @brief	FXSAVE/FXRSTOR instructions support
			/// @see	{@link Std::Platform::Intrinsic::XSave}
			bool FXSave : 1;
			/// @brief	Fast short REP CMPSB/SCASB instructions support
			/// todo: add link
			bool FastShortRepCompare : 1;
			/// @brief	Fast short REP MOV instruction support
			/// todo: add link
			bool FastShortRepMove : 1;
			/// @brief	Fast short REP STOSB instruction support
			/// todo: add link
			bool FastShortRepStore : 1;
			/// @brief	Fast zero-length REP MOVSB instruction support
			/// todo: add link
			bool FastZeroLengthRepMove : 1;
			/// @brief	Fused Multiply-Add instructions support
			/// @see	{@link Std::Platform::Intrinsic::Fma3}
			bool Fma3 : 1;
			/// @brief	4-operands Fused Multiply-Add instructions support
			/// todo: add link
			bool Fma4 : 1;
			/// @brief	x87 FPU on Chip support
			bool Fpu : 1;
			/// @brief	Galois Field New Instructions support
			/// todo: add link
			bool Gfni : 1;
			/// @brief	TSX Hardware Lock Elision instructions support
			/// @see	{@link Std::Platform::Intrinsic::Hle}
			bool Hle : 1;
			/// @brief	INVPCID instruction support
			/// @see	{@link Std::Platform::Intrinsic::InvPcid}
			bool InvPcid : 1;
			/// @brief	MMX instructions support
			/// @see	{@link Std::Platform::Intrinsic::Mmx}
			bool Mmx : 1;
			/// @brief	MONITOR/MWAIT instructions support
			/// @see	{@link Std::Platform::Intrinsic::Monitor}
			bool Monitor : 1;
			/// @brief	MOVBE instruction support
			/// @see	{@link Std::Platform::Intrinsic::MovBe}
			bool MovBigEndian : 1;
			/// @brief	MOVDIRI instruction support
			/// todo: add link
			bool MoveDirect : 1;
			/// @brief	MOVDIR64B instruction support
			/// todo: add link
			bool MoveDirect64b : 1;
			/// @brief	Are XSave (and related) instructions enabled			
			/// @see	{@link Std::Platform::Intrinsic::XSave}
			bool OsXSave : 1;
			/// @brief	POPCNT instruction support
			/// @see	{@link Std::Platform::Intrinsic::Abm}
			bool PopulationCount : 1;
			/// @brief	RDRAND instruction support
			/// @see	{@link Std::Platform::Intrinsic::RdRand}
			bool RdRand : 1;
			/// @brief	RDSEED instruction support
			/// @see	{@link Std::Platform::Intrinsic::RdSeed}
			bool RdSeed : 1;
			/// @brief	Time Stamp Counter and RDTSC instruction support
			/// @see	{@link Std::Platform::Intrinsic::RdTsc}
			bool RdTsc : 1;
			/// @brief	TSX Restricted Transactional Memory instructions support
			/// @see	{@link Std::Platform::Intrinsic::Rtm}
			bool Rtm : 1;
			/// @brief	Supervisor Mode Access Prevention support
			/// @see	{@link Std::Platform::Intrinsic::Smap}
			bool Smap : 1;
			/// @brief	Cache self-snooping support
			bool SelfSnoop : 1;
			/// @brief	Software Guard Extensions support
			/// todo: add link
			bool Sgx : 1;
			/// @brief	SHA-1/SHA-256 instructions support
			/// todo: add link
			bool Sha : 1;
			/// @brief	GETSEC instruction support
			/// todo: add link
			bool Smx : 1;
			/// @brief	Streaming SIMD Extensions support
			/// @see	{@link Std::Platform::Intrinsic::Sse}
			bool Sse : 1;
			/// @brief	Streaming SIMD Extensions 2 support
			/// @see	{@link Std::Platform::Intrinsic::Sse2}
			bool Sse2 : 1;
			/// @brief	Streaming SIMD Extensions 3 support
			/// @see	{@link Std::Platform::Intrinsic::Sse3}
			bool Sse3 : 1;
			/// @brief	Streaming SIMD Extensions 4.1 support
			/// @see	{@link Std::Platform::Intrinsic::Sse41}
			bool Sse41 : 1;
			/// @brief	Streaming SIMD Extensions 4.2 support
			/// @see	{@link Std::Platform::Intrinsic::Sse42}
			bool Sse42 : 1;
			/// @brief	Streaming SIMD Extensions 4a support
			/// todo: add link
			bool Sse4a : 1;
			/// @brief	Supplemental Streaming SIMD Extensions 3 support
			/// @see	{@link Std::Platform::Intrinsic::Ssse3}
			bool Ssse3 : 1;
			/// @brief	Trailing Bit Manipulation instructions support
			/// @see	{@link Std::Platform::Intrinsic::Tbm}
			bool Tbm : 1;
			/// @brief	PREFETCH/PREFETCHW instructions support
			/// @see	{@link Std::Platform::Intrinsic::ThreeDNowPrefetch}
			bool ThreeDNowPrefetch : 1;
			/// @brief	Vector AES instructions support
			/// todo: add link
			bool VectorAes : 1;
			/// @brief	XSave (and related) instructions support
			/// @see	{@link Std::Platform::Intrinsic::XSave}
			bool XSave : 1;
			/// @brief	XTEST instruction support
			/// @see	{@link Std::Platform::Intrinsic::XTest}
			bool XTest : 1;
		};

	public:
		static inline constexpr uint8 RegistersCount = 4;
		static inline constexpr uint8 LeavesCount = 11;

		static inline constexpr uint8 BrandStringLength = 49;
		static inline constexpr uint8 VendorStringLength = 13;

	public:
		X86()
		{
			FetchData();

			LoadBrand();
			LoadVendor();
			LoadCpuInfo();
			LoadFeatures();

			switch (Vendor)
			{
				case EVendor::Amd:
				{
					HighestExtendedFunction > 0x8000001c
						? LoadCache(0x8000001d)
						: LoadAmdCache();
					LoadAmdCores();
					break;
				}
				case EVendor::Intel:
				{
					LoadCache(4);
					LoadIntelCores();
					break;
				}
				default: {}
			}
		}

	public:
		Cpuid::Leaf Leaves[LeavesCount] = {};

		char8 BrandString[BrandStringLength] = {};
		char8 VendorString[VendorStringLength] = {};

		Features Features = {};

		Cache L1InstructionsCache = {};
		Cache L1DataCache = {};
		Cache L2UnifiedCache = {};
		Cache L3UnifiedCache = {};

		uint32 HighestExtendedFunction = 0;

		EVendor Vendor = {};

		uint8 HighestBasicFunction = 0;

		uint8 Stepping = 0;

		uint8 Model = 0;
		uint8 Family = 0;

		uint8 ExtendedModel = 0;
		uint8 ExtendedFamily = 0;

		uint8 ActualModel = 0;
		uint8 ActualFamily = 0;

		uint8 BrandIndex = 0;

		uint8 LogicalCores = 0;
		uint8 PhysicalCores = 0;

	private:
		inline void FetchData()
		{
			using Cpuid::Cpuid;

			Leaves[0] = Cpuid(0);
			Leaves[1] = Cpuid(1);
			Leaves[2] = Cpuid(2);
			Leaves[3] = Cpuid(7);
			Leaves[4] = Cpuid(7, 1);
			Leaves[5] = Cpuid(0x80000000);
			Leaves[6] = Cpuid(0x80000001);
			Leaves[7] = Cpuid(0x80000002);
			Leaves[8] = Cpuid(0x80000003);
			Leaves[9] = Cpuid(0x80000004);
			Leaves[10] = Cpuid(0x80000021);

			HighestBasicFunction = static_cast<uint8>(Leaves[ELeafId::Function0].Eax);
			HighestExtendedFunction = Leaves[ELeafId::Function80000000].Eax;
		}

	private:
		inline void LoadVendor()
		{
			*reinterpret_cast<uint32*>(VendorString) = Leaves[ELeafId::Function0].Ebx;
			*reinterpret_cast<uint32*>(VendorString + 4) = Leaves[ELeafId::Function0].Edx;
			*reinterpret_cast<uint32*>(VendorString + 8) = Leaves[ELeafId::Function0].Ecx;

			// todo: String::Compare()?
			char8 vendors[] =
				u8"GenuineIntel"
				"AuthenticAMD";

			for (auto i = 0u; i < sizeof(vendors); i += 12)
			{
				if (*reinterpret_cast<uint64*>(VendorString) == *reinterpret_cast<uint64*>(vendors + i)
					&& *reinterpret_cast<uint32*>(VendorString + 8) == *reinterpret_cast<uint32*>(vendors + i + 8)
				)
				{
					Vendor = static_cast<EVendor>(i / 12 + 1);
				}
			}
		}

		inline void LoadBrand()
		{
			auto offset = 0u;
			for (uint32 i = ELeafId::Function80000002; i <= ELeafId::Function80000004; ++i, offset += 16)
			{
				// todo: make sure casts are portable

				*reinterpret_cast<uint64*>(BrandString + offset) =
					*reinterpret_cast<uint64*>(&Leaves[i]);

				*reinterpret_cast<uint64*>(BrandString + offset + 8) =
					*(reinterpret_cast<uint64*>(&Leaves[i]) + 1);
			}
		}

		inline void LoadCpuInfo()
		{
			Stepping = _Internal::ExtractBits<uint8>(Leaves[ELeafId::Function1].Eax, 0, 3);

			Model = _Internal::ExtractBits<uint8>(Leaves[ELeafId::Function1].Eax, 4, 7);
			Family = _Internal::ExtractBits<uint8>(Leaves[ELeafId::Function1].Eax, 8, 11);

			ExtendedModel = _Internal::ExtractBits<uint8>(Leaves[ELeafId::Function1].Eax, 16, 19);
			ExtendedFamily = _Internal::ExtractBits<uint8>(Leaves[ELeafId::Function1].Eax, 20, 27);

			BrandIndex = _Internal::ExtractBits<uint8>(Leaves[ELeafId::Function1].Ebx, 0, 7);

			ActualModel = Family == 6 || Family == 15
				? Model + static_cast<uint8>(ExtendedModel << 4)
				: Model;
			ActualFamily = Family == 15
				? Family + ExtendedFamily
				: Family;
		}

		void LoadFeatures()
		{
			Features.Abm = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function80000001].Ecx, 5);
			Features.AddressMasking = Vendor == EVendor::Amd
				? _Internal::ExtractBit<bool>(Leaves[ELeafId::Function80000021].Eax, 7)
				: _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Eax, 26);
			Features.Adx = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 19);
			Features.Aes = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 25);
			Features.AmxBrainFloat16 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Edx, 22);
			Features.AmxComplex = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Edx, 8);
			Features.AmxFloat16 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Eax, 21);
			Features.AmxInt8 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Edx, 25);
			Features.AmxTile = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Edx, 24);
			Features.Avx = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 28);
			Features.Avx2 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 5);
			Features.Avx512BitAlgorithms = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 12);
			Features.Avx512Bmi = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 1);
			Features.Avx512Bmi2 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 6);
			Features.Avx512BrainFloat16 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Eax, 5);
			Features.Avx512ByteWord = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 30);
			Features.Avx512ConflictDetection = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 28);
			Features.Avx512DoubleQuad = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 17);
			Features.Avx512ExponentialReciprocal = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 27);
			Features.Avx512 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 16);
			Features.Avx512Float16 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Edx, 23);
			Features.Avx512FmaPs4 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Edx, 3);
			Features.Avx512FmaInt = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 21);
			Features.Avx512IntersectInt = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Edx, 8);
			Features.Avx512NeuralNetwork4 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Edx, 2);
			Features.Avx512NeuralNetwork = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 11);
			Features.Avx512PopulationCount = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 14);
			Features.Avx512Prefetch = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 26);
			Features.Avx512VectorLength = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 31);
			Features.AvxNeuralNetwork = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Eax, 4);
			Features.AvxNeuralNetworkInt16 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Edx, 10);
			Features.AvxNeuralNetworkInt8 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Edx, 4);
			Features.AvxNoExceptionFloatConvert = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Edx, 5);
			Features.Bmi = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 3);
			Features.Bmi2 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 8);
			Features.CacheLineFlush = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 19);
			Features.CacheLineFlushOptimized = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 23);
			Features.CacheLineWriteBack = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 24);
			Features.CarryLessMultiplication256 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 10);
			Features.CarryLessMultiplication64 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 1);
			Features.CompareExchange16 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 13);
			Features.CompareExchange8 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 8);
			Features.ConditionalMove = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 15);
			Features.DirectCacheAccess = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 18);
			Features.EnhancedRepMove = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 9);
			Features.F16c = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 29);
			Features.FXSave = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function80000001].Edx, 24);
			Features.FastShortRepCompare = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Eax, 12);
			Features.FastShortRepMove = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Edx, 4);
			Features.FastShortRepStore = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Eax, 11);
			Features.FastZeroLengthRepMove = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7Ecx1].Eax, 10);
			Features.Fma3 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 12);
			Features.Fma4 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function80000001].Ecx, 16);
			Features.Fpu = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 0);
			Features.Gfni = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 8);
			Features.Hle = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 4);
			Features.InvPcid = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 10);
			Features.Mmx = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 23);
			Features.Monitor = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 3);
			Features.MovBigEndian = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 22);
			Features.MoveDirect = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 27);
			Features.MoveDirect64b = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 28);
			Features.OsXSave = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 27);
			Features.PopulationCount = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 23);
			Features.RdRand = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 30);
			Features.RdSeed = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 18);
			Features.RdTsc = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 4);
			Features.Rtm = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 11);
			Features.Smap = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 20);
			Features.SelfSnoop = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 27);
			Features.Sgx = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 2);
			Features.Sha = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ebx, 29);
			Features.Smx = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 6);
			Features.Sse = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 25);
			Features.Sse2 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Edx, 26);
			Features.Sse3 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 0);
			Features.Sse41 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 19);
			Features.Sse42 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 20);
			Features.Sse4a = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function80000001].Ecx, 6);
			Features.Ssse3 = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 9);
			Features.Tbm = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function80000001].Ecx, 21);
			Features.ThreeDNowPrefetch = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function80000001].Ecx, 8);
			Features.VectorAes = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function7].Ecx, 9);
			Features.XSave = _Internal::ExtractBit<bool>(Leaves[ELeafId::Function1].Ecx, 26);
			Features.XTest = Features.Hle || Features.Rtm;
		}

		inline void LoadCache(
			const uint32 functionId
		)
		{
			for (auto i = 0; i < 4; ++i)
			{
				const auto leaf = Cpuid::Cpuid(functionId, i);

				const auto cacheLevel = _Internal::ExtractBits<uint8>(leaf.Eax, 5, 7);
				const auto cacheType = _Internal::ExtractBits<uint8>(leaf.Eax, 0, 4);
				const auto cacheId = cacheLevel * 10 + cacheType;

				uint32* size;
				uint16* lineSize;
				uint8* associativity;
				bool* isPresent;

				switch (cacheId)
				{
					case 11:
					{
						size = &L1DataCache.Size;
						lineSize = &L1DataCache.LineSize;
						associativity = &L1DataCache.Associativity;
						isPresent = &L1DataCache.IsPresent;

						L1DataCache.Level = ECacheLevel::L1Data;
						break;
					}
					case 12:
					{
						size = &L1InstructionsCache.Size;
						lineSize = &L1InstructionsCache.LineSize;
						associativity = &L1InstructionsCache.Associativity;
						isPresent = &L1InstructionsCache.IsPresent;

						L1InstructionsCache.Level = ECacheLevel::L1Instructions;
						break;
					}
					case 23:
					{
						size = &L2UnifiedCache.Size;
						lineSize = &L2UnifiedCache.LineSize;
						associativity = &L2UnifiedCache.Associativity;
						isPresent = &L2UnifiedCache.IsPresent;

						L2UnifiedCache.Level = ECacheLevel::L2Unified;
						break;
					}
					case 33:
					{
						size = &L3UnifiedCache.Size;
						lineSize = &L3UnifiedCache.LineSize;
						associativity = &L3UnifiedCache.Associativity;
						isPresent = &L3UnifiedCache.IsPresent;

						L3UnifiedCache.Level = ECacheLevel::L3Unified;
						break;
					}
					default:
					{
						continue;
					}
				}

				const auto setsCount = leaf.Ecx;
				const auto linePartitions = _Internal::ExtractBits<uint16>(leaf.Ebx, 12, 21);

				*associativity = _Internal::ExtractBits<uint8>(leaf.Ebx, 22, 31) + 1;
				*lineSize = _Internal::ExtractBits<uint16>(leaf.Ebx, 0, 11) + 1;
				*size = *associativity * *lineSize * (linePartitions + 1) * (setsCount + 1) / 1024;
				*isPresent = *size != 0;
			}
		}

	private:
		/// @brief	Legacy way to determine AMD cpu cache
		inline void LoadAmdCache()
		{
			if (HighestExtendedFunction < 0x80000005)
			{
				return;
			}

			auto leaf = Cpuid::Cpuid(0x80000005);

			L1DataCache.Size = _Internal::ExtractBits<uint32>(leaf.Ecx, 24, 31);
			L1DataCache.LineSize = _Internal::ExtractBits<uint16>(leaf.Ecx, 0, 7);
			L1DataCache.Associativity = _Internal::ExtractBits<uint8>(leaf.Ecx, 16, 23);
			L1DataCache.IsPresent = L1DataCache.Size != 0;

			L1InstructionsCache.Size = _Internal::ExtractBits<uint32>(leaf.Edx, 24, 31);
			L1InstructionsCache.LineSize = _Internal::ExtractBits<uint16>(leaf.Edx, 0, 7);
			L1InstructionsCache.Associativity = _Internal::ExtractBits<uint8>(leaf.Edx, 16, 23);
			L1InstructionsCache.IsPresent = L1InstructionsCache.Size != 0;

			if (HighestExtendedFunction < 0x80000006)
			{
				return;
			}

			constexpr uint8 associativityTable[16] = {0, 1, 2, 0, 4, 0, 8, 0, 16, 16, 32, 48, 64, 96, 128, 255};
			leaf = Cpuid::Cpuid(0x80000006);

			L2UnifiedCache.Size = _Internal::ExtractBits<uint32>(leaf.Ecx, 16, 31);
			L2UnifiedCache.LineSize = _Internal::ExtractBits<uint16>(leaf.Ecx, 0, 7);
			L2UnifiedCache.Associativity = associativityTable[_Internal::ExtractBits<uint8>(leaf.Ecx, 12, 15)];
			L2UnifiedCache.IsPresent = L2UnifiedCache.Size != 0;

			// cpuid returns number of 512 Kb units for L3 cache
			L3UnifiedCache.Size = _Internal::ExtractBits<uint32>(leaf.Edx, 18, 31) * 512;
			L3UnifiedCache.LineSize = _Internal::ExtractBits<uint16>(leaf.Edx, 0, 7);
			L3UnifiedCache.Associativity = associativityTable[_Internal::ExtractBits<uint8>(leaf.Edx, 12, 15)];
			L3UnifiedCache.IsPresent = L3UnifiedCache.Size != 0;
		}

	private:
		inline void LoadAmdCores()
		{
			LogicalCores = HighestBasicFunction > 0
				? _Internal::ExtractBits<uint8>(Leaves[ELeafId::Function1].Ebx, 16, 23)
				: 1;

			PhysicalCores = HighestExtendedFunction > 0x80000007
				? _Internal::ExtractBits<uint8>(Cpuid::Cpuid(0x80000008).Ecx, 0, 7) + 1
				: 1;

			constexpr auto hyperThreadingBit = 1 << 28;
			if (!(Leaves[ELeafId::Function1].Edx & hyperThreadingBit))
			{
				return;
			}

			LogicalCores = LogicalCores > 1
				? LogicalCores
				: 2;

			// Ryzen 3 has SMT flag, but in fact cores count is equal to threads count.
			// Ryzen 5/7 reports twice as many "real" cores (e.g. 16 cores instead of 8) because of SMT.
			//
			// CPUID_Fn8000001E_EBX [Core Identifiers][15:8] is ThreadsPerCore
			// ThreadsPerCore: [...] The number of threads per core is ThreadsPerCore+1
			if (PhysicalCores > 1 && ActualFamily > 22 && HighestExtendedFunction > 0x8000001d)
			{
				PhysicalCores /= _Internal::ExtractBits<uint8>(Cpuid::Cpuid(0x8000001e).Ebx, 8, 15) + 1;
			}
		}

		inline void LoadIntelCores()
		{
			LogicalCores = HighestBasicFunction > 0
				? _Internal::ExtractBits<uint8>(Leaves[ELeafId::Function1].Ebx, 16, 23)
				: 1;

			PhysicalCores = HighestBasicFunction > 3
				? _Internal::ExtractBits<uint8>(Cpuid::Cpuid(4).Eax, 26, 31) + 1
				: 1;

			if (HighestBasicFunction < 11)
			{
				return;
			}

			uint8 threadsCount = 1;
			for (auto i = 0; i < 4; ++i)
			{
				const auto leaf = Cpuid::Cpuid(11, i);
				switch (_Internal::ExtractBits<uint8>(leaf.Ecx, 8, 15))
				{
					case 1:
					{
						threadsCount = _Internal::ExtractBits<uint8>(leaf.Ebx, 0, 15);
						break;
					}
					case 2:
					{
						LogicalCores = _Internal::ExtractBits<uint8>(leaf.Ebx, 0, 15);
						break;
					}
					default: {}
				}
			}

			threadsCount = threadsCount
				? threadsCount
				: 1;
			PhysicalCores = LogicalCores / threadsCount;
		}
	};
}
