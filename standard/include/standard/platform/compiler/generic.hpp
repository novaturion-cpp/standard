﻿#pragma once


namespace Std::inline Platform::inline Compiler
{
	// todo: u8 string literals?

	/// @brief	Defined as string literal that
	/// encodes short name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------|---------
	/// Unknown	| 
	/// ARM	| Arm
	/// Clang	| Clang
	/// GNU CC	| Gcc
	/// Microsoft	| Msvc
	/// </pre>
	#define COMPILER_NAME	""

	/// @brief	Defined as string literal that
	/// encodes full name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------|----------------------------
	/// Unknown	| 
	/// ARM	| Arm C/C++
	/// Clang	| Clang C Language Family
	/// GNU CC	| Gnu Compiler Collection
	/// Microsoft	| Microsoft Visual C++
	/// </pre>
	#define COMPILER_FULL_NAME	""

	/// @brief	Defined as integer literal
	/// Encodes the version of C++ standard that is being used
	/// <pre>
	/// Standard	| Value
	/// ---------|--------
	/// C++11	| 201103L
	/// C++14	| 201402L
	/// C++17	| 201703L
	/// C++20	| 202002L
	/// </pre>
	#define CPP_VERSION	__cplusplus

	/// @brief	Defined as integer literal whose value is the alignment
	/// guaranteed by a call to alignment-unaware operator new
	/// @note	Larger alignments will be passed to alignment-aware
	/// overload, such as operator new(uintSize, AlignValue)
	#define DEFAULT_NEW_ALIGNMENT	__STDCPP_DEFAULT_NEW_ALIGNMENT__

	/// @brief	Defined as string literal
	/// Encodes the name of the current file
	#define CURRENT_FILE	__FILE__

	/// @brief	Defined as integer literal
	/// Encodes the source file line number
	/// @note	Can be changed by the #line directive
	#define CURRENT_LINE	__LINE__

	/// @brief	Defined as string literal of the form "Mmm dd yyyy"
	/// Encodes the compilation date of the current source file\n
	/// @note	First character of date dd is a space if the value is less than 10
	#define CURRENT_DATE	__DATE__

	/// @brief	Defined as string literal of the form "hh:mm:ss"
	/// Encodes the time of translation
	#define CURRENT_TIME	__TIME__

	/// @brief	Expands to an integer literal that starts at 0
	/// The value is incremented by 1 every time it's used in a source file,
	/// or in included headers of the source file
	#define COUNTER	__COUNTER__

	/// @brief	Defined as string literal that contains the date and time of the
	/// last modification of the current source file
	#define TIMESTAMP	__TIMESTAMP__


	#if defined(__AVX__)
	/// @brief	Defined as boolean literal 'true' when the AVX, AVX2, or
	/// AVX512 compiler options are set and the compiler target is x86 or x64
	#define IS_AVX_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the AVX, AVX2, or
	/// AVX512 compiler options are set and the compiler target is x86 or x64
	#define IS_AVX_ENABLED	false
	#endif

	#if defined(__AVX2__)
	/// @brief	Defined as boolean literal 'true' when the AVX2 or AVX512
	/// compiler options are set and the compiler target is x86 or x64
	#define IS_AVX2_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the AVX2 or AVX512
	/// compiler options are set and the compiler target is x86 or x64
	#define IS_AVX2_ENABLED	false
	#endif

	#if defined(__AVX512BW__)
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_BW_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_BW_ENABLED	false
	#endif

	#if defined(__AVX512CD__)
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_CD_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_CD_ENABLED	false
	#endif

	#if defined(__AVX512DQ__)
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_DQ_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_DQ_ENABLED	false
	#endif

	#if defined(__AVX512F__)
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_F_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_F_ENABLED	false
	#endif

	#if defined(__AVX512VL__)
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_VL_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the AVX512
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_AVX512_VL_ENABLED	false
	#endif

	#if IS_AVX512_VL_ENABLED && IS_AVX512_F_ENABLED && IS_AVX512_DQ_ENABLED \
		&& IS_AVX512_CD_ENABLED && IS_AVX512_BW_ENABLED
	/// @brief	Defined as boolean literal 'true' when the IS_AVX512_VL, IS_AVX512_F, IS_AVX512_DQ,
	/// IS_AVX512_CD and IS_AVX512_BW are 'true' and the compiler target is x86 or x64
	#define IS_AVX512_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the AVX-512VL, AVX-512F, AVX-512DQ,
	/// AVX-512CD and AVX-512BW are set and the compiler target is x86 or x64
	#define IS_AVX512_ENABLED	false
	#endif

	#if defined(__SSE__)
	/// @brief	Defined as boolean literal 'true' when the SSE
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the SSE
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE_ENABLED	false
	#endif

	#if defined(__SSE2__)
	/// @brief	Defined as boolean literal 'true' when the SSE2
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE2_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the SSE2
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE2_ENABLED	false
	#endif

	#if defined(__SSE3__)
	/// @brief	Defined as boolean literal 'true' when the SSE3
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE3_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the SSE3
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE3_ENABLED	false
	#endif

	#if defined(__SSE4_1__)
	/// @brief	Defined as boolean literal 'true' when the SSE4.1
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE4_1_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the SSE4.1
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE4_1_ENABLED	false
	#endif

	#if defined(__SSE4_2__)
	/// @brief	Defined as boolean literal 'true' when the SSE4.2
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE4_2_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the SSE4.2
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSE4_2_ENABLED	false
	#endif

	#if defined(__SSSE3__)
	/// @brief	Defined as boolean literal 'true' when the SSSE3
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSSE3_ENABLED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the SSSE3
	/// compiler option is set and the compiler target is x86 or x64
	#define IS_SSSE3_ENABLED	false
	#endif
}
