﻿#pragma once


namespace Std::inline Platform::inline Compiler::Msvc
{
	// todo: u8 string literals?

	#if defined(_MSC_VER)
	/// @brief	Defined as boolean literal 'true' if current compiler is MSVC
	#define COMPILER_IS_MSVC	true
	#else
	/// @brief	Defined as boolean literal 'true' if current compiler is MSVC
	#define COMPILER_IS_MSVC	false
	#endif

	#if COMPILER_IS_MSVC
	#undef COMPILER_NAME
	#undef COMPILER_FULL_NAME
	#undef COUNTER
	#undef TIMESTAMP

	/// @brief	Defined as string literal that
	/// encodes short name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------|---------
	/// Unknown	| 
	/// ARM	| Arm
	/// Clang	| Clang
	/// GNU CC	| Gcc
	/// Microsoft	| Msvc
	/// </pre>
	#define COMPILER_NAME	"Msvc"

	/// @brief	Defined as string literal that
	/// encodes full name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------|----------------------------
	/// Unknown	| 
	/// ARM	| Arm C/C++
	/// Clang	| Clang C Language Family
	/// GNU CC	| Gnu Compiler Collection
	/// Microsoft	| Microsoft Visual C++
	/// </pre>
	#define COMPILER_FULL_NAME	"Microsoft Visual C++"

	/// @brief	Expands to an integer literal that starts at 0
	/// The value is incremented by 1 every time it's used in a source file,
	/// or in included headers of the source file
	/// MSVC COUNTER remembers its state when you use precompiled headers
	#define COUNTER	__COUNTER__

	/// @brief	Defined as string literal that contains the date and time of the
	/// last modification of the current source file, in the abbreviated,
	/// constant length form returned by the CRT asctime function\n
	/// For example, 'Fri 19 Aug 13:32:58 2016'
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/asctime-wasctime">asctime</a>
	#define TIMESTAMP	__TIMESTAMP__
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as integer literal value 64,
	/// the maximum size (in bits) for a non-vector integral type
	#define MSVC_INTEGRAL_MAX_BITS	_INTEGRAL_MAX_BITS
	#else
	/// @brief	Defined as integer literal value 64,
	/// the maximum size (in bits) for a non-vector integral type
	#define MSVC_INTEGRAL_MAX_BITS	0
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as boolean literal 'true' during an IntelliSense compiler pass in the Visual Studio IDE\n
	/// You can use this macro to guard code the IntelliSense compiler doesn't understand,
	/// or use it to toggle between the build and IntelliSense compiler
	/// @see	<a href="https://devblogs.microsoft.com/cppblog/troubleshooting-tips-for-intellisense-slowness">Troubleshooting Tips for IntelliSense Slowness</a>
	#define MSVC_INTELLISENSE	__INTELLISENSE__
	#else
	/// @brief	Defined as boolean literal 'true' during an IntelliSense compiler pass in the Visual Studio IDE\n
	/// You can use this macro to guard code the IntelliSense compiler doesn't understand,
	/// or use it to toggle between the build and IntelliSense compiler
	/// @see	<a href="https://devblogs.microsoft.com/cppblog/troubleshooting-tips-for-intellisense-slowness">Troubleshooting Tips for IntelliSense Slowness</a>
	#define MSVC_INTELLISENSE	false
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as string literal that contains the undecorated name of the enclosing function
	/// The macro is defined only within a function
	/// The macro isn't expanded if you use the /EP or /P compiler option
	/// @code
	/// void exampleFunction()
	/// {
	/// 	printf("Function name: %s\n", MSVC_FUNCTION_NAME);
	/// 	// Function name: exampleFunction
	/// }
	/// @endcode
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/ep-preprocess-to-stdout-without-hash-line-directives">/EP</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/p-preprocess-to-a-file">/P</a>
	#define MSVC_FUNCTION_NAME	__FUNCTION__
	#else
	/// @brief	Defined as string literal that contains the undecorated name of the enclosing function
	/// The macro is defined only within a function
	/// The macro isn't expanded if you use the /EP or /P compiler option
	/// @code
	/// void exampleFunction()
	/// {
	/// 	printf("Function name: %s\n", MSVC_FUNCTION_NAME);
	/// 	// Function name: exampleFunction
	/// }
	/// @endcode
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/ep-preprocess-to-stdout-without-hash-line-directives">/EP</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/p-preprocess-to-a-file">/P</a>
	#define MSVC_FUNCTION_NAME	""
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as string literal that contains the decorated name of the enclosing function
	/// The macro is defined only within a function
	/// The macro isn't expanded if you use the /EP or /P compiler option
	/// @code
	/// void exampleFunction()
	/// {
	/// 	printf("Decorated function name: %s\n", MSVC_FUNCTION_MANGLED_NAME);
	/// 	// Decorated function name: ?exampleFunction@@YAXXZ
	/// }
	/// @endcode
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/decorated-names">Decorated name</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/ep-preprocess-to-stdout-without-hash-line-directives">/EP</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/p-preprocess-to-a-file">/P</a>
	#define MSVC_FUNCTION_MANGLED_NAME	__FUNCDNAME__
	#else
	/// @brief	Defined as string literal that contains the decorated name of the enclosing function
	/// The macro is defined only within a function
	/// The macro isn't expanded if you use the /EP or /P compiler option
	/// @code
	/// void exampleFunction()
	/// {
	/// 	printf("Decorated function name: %s\n", MSVC_FUNCTION_MANGLED_NAME);
	/// 	// Decorated function name: ?exampleFunction@@YAXXZ
	/// }
	/// @endcode
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/decorated-names">Decorated name</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/ep-preprocess-to-stdout-without-hash-line-directives">/EP</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/p-preprocess-to-a-file">/P</a>
	#define MSVC_FUNCTION_MANGLED_NAME	""
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as string literal that contains the signature of the enclosing function
	/// The macro is defined only within a function
	/// The macro isn't expanded if you use the /EP or /P compiler option
	/// When compiled for a 64-bit target, the calling convention is __cdecl by default
	/// @code
	/// void exampleFunction()
	/// {
	/// 	printf("Function signature: %s\n", MSVC_FUNCTION_SIGNATURE);
	/// 	// Function signature: void __cdecl exampleFunction(void)
	/// }
	/// @endcode
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/ep-preprocess-to-stdout-without-hash-line-directives">/EP</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/p-preprocess-to-a-file">/P</a>
	#define MSVC_FUNCTION_SIGNATURE	__FUNCSIG__
	#else
	/// @brief	Defined as string literal that contains the signature of the enclosing function
	/// The macro is defined only within a function
	/// The macro isn't expanded if you use the /EP or /P compiler option
	/// When compiled for a 64-bit target, the calling convention is __cdecl by default
	/// @code
	/// void exampleFunction()
	/// {
	/// 	printf("Function signature: %s\n", MSVC_FUNCTION_SIGNATURE);
	/// 	// Function signature: void __cdecl exampleFunction(void)
	/// }
	/// @endcode
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/ep-preprocess-to-stdout-without-hash-line-directives">/EP</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/p-preprocess-to-a-file">/P</a>
	#define MSVC_FUNCTION_SIGNATURE	""
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as integer literal that encodes the
	/// major and minor number elements of the compiler's version number
	/// The major number is the first element of the period-delimited
	/// version number, the minor number is the second element
	/// For example, if the version number of the Microsoft C/C++ compiler
	/// is 15.00.20706.01, the macro evaluates to 1500
	/// <pre>
	/// Visual Studio			| Version
	/// ----------------------------------------|---------
	/// Visual Studio 6.0			| 1200
	/// Visual Studio .NET 2002 (7.0)		| 1300
	/// Visual Studio .NET 2003 (7.1)		| 1310
	/// Visual Studio 2005 (8.0)		| 1400
	/// Visual Studio 2008 (9.0)		| 1500
	/// Visual Studio 2010 (10.0)		| 1600
	/// Visual Studio 2012 (11.0)		| 1700
	/// Visual Studio 2013 (12.0)		| 1800
	/// Visual Studio 2015 (14.0)		| 1900
	/// Visual Studio 2017 RTW (15.0)		| 1910
	/// Visual Studio 2017 version 15.3	| 1911
	/// Visual Studio 2017 version 15.5	| 1912
	/// Visual Studio 2017 version 15.6	| 1913
	/// Visual Studio 2017 version 15.7	| 1914
	/// Visual Studio 2017 version 15.8	| 1915
	/// Visual Studio 2017 version 15.9	| 1916
	/// Visual Studio 2019 RTW (16.0)		| 1920
	/// Visual Studio 2019 version 16.1	| 1921
	/// Visual Studio 2019 version 16.2	| 1922
	/// Visual Studio 2019 version 16.3	| 1923
	/// Visual Studio 2019 version 16.4	| 1924
	/// Visual Studio 2019 version 16.5	| 1925
	/// Visual Studio 2019 version 16.6	| 1926
	/// Visual Studio 2019 version 16.7	| 1927
	/// Visual Studio 2019 version 16.8	| 1928
	/// Visual Studio 2019 version 16.9	| 1928
	/// Visual Studio 2019 version 16.10	| 1929
	/// Visual Studio 2019 version 16.11	| 1929
	/// Visual Studio 2022 RTW (17.0)		| 1930
	/// </pre>
	#define MSVC_VERSION	_MSC_VER
	#else
	/// @brief	Defined as integer literal that encodes the
	/// major and minor number elements of the compiler's version number
	/// The major number is the first element of the period-delimited
	/// version number, the minor number is the second element
	/// For example, if the version number of the Microsoft C/C++ compiler
	/// is 15.00.20706.01, the macro evaluates to 1500
	/// <pre>
	/// Visual Studio			| Version
	/// ----------------------------------------|---------
	/// Visual Studio 6.0			| 1200
	/// Visual Studio .NET 2002 (7.0)		| 1300
	/// Visual Studio .NET 2003 (7.1)		| 1310
	/// Visual Studio 2005 (8.0)		| 1400
	/// Visual Studio 2008 (9.0)		| 1500
	/// Visual Studio 2010 (10.0)		| 1600
	/// Visual Studio 2012 (11.0)		| 1700
	/// Visual Studio 2013 (12.0)		| 1800
	/// Visual Studio 2015 (14.0)		| 1900
	/// Visual Studio 2017 RTW (15.0)		| 1910
	/// Visual Studio 2017 version 15.3	| 1911
	/// Visual Studio 2017 version 15.5	| 1912
	/// Visual Studio 2017 version 15.6	| 1913
	/// Visual Studio 2017 version 15.7	| 1914
	/// Visual Studio 2017 version 15.8	| 1915
	/// Visual Studio 2017 version 15.9	| 1916
	/// Visual Studio 2019 RTW (16.0)		| 1920
	/// Visual Studio 2019 version 16.1	| 1921
	/// Visual Studio 2019 version 16.2	| 1922
	/// Visual Studio 2019 version 16.3	| 1923
	/// Visual Studio 2019 version 16.4	| 1924
	/// Visual Studio 2019 version 16.5	| 1925
	/// Visual Studio 2019 version 16.6	| 1926
	/// Visual Studio 2019 version 16.7	| 1927
	/// Visual Studio 2019 version 16.8	| 1928
	/// Visual Studio 2019 version 16.9	| 1928
	/// Visual Studio 2019 version 16.10	| 1929
	/// Visual Studio 2019 version 16.11	| 1929
	/// Visual Studio 2022 RTW (17.0)		| 1930
	/// </pre>
	#define MSVC_VERSION	0
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as integer literal that encodes the major,
	/// minor, and build number elements of the compiler's version number
	/// The major number is the first element of the period-delimited version number,
	/// the minor number is the second element, and the build number is the third element
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 150020706
	#define MSVC_FULL_VERSION	_MSC_FULL_VER
	#else
	/// @brief	Defined as integer literal that encodes the major,
	/// minor, and build number elements of the compiler's version number
	/// The major number is the first element of the period-delimited version number,
	/// the minor number is the second element, and the build number is the third element
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 150020706
	#define MSVC_FULL_VERSION	0
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as integer literal that encodes
	/// the major number element of the compiler's version number
	/// The major number is the first element of the period-delimited version number
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 15
	#define MSVC_MAJOR_VERSION	(MSVC_VERSION / 100)
	#else
	/// @brief	Defined as integer literal that encodes
	/// the major number element of the compiler's version number
	/// The major number is the first element of the period-delimited version number
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 15
	#define MSVC_MAJOR_VERSION	0
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as integer literal that encodes
	/// the minor number element of the compiler's version number
	/// The minor number is the second element of the period-delimited version number
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 00
	#define MSVC_MINOR_VERSION	(MSVC_VERSION % 100)
	#else
	/// @brief	Defined as integer literal that encodes
	/// the minor number element of the compiler's version number
	/// The minor number is the second element of the period-delimited version number
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 00
	#define MSVC_MINOR_VERSION	0
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as integer literal that encodes
	/// the build number element of the compiler's version number
	/// The build number is the third element of the period-delimited version number
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 20706
	#define MSVC_BUILD_NUMBER	(MSVC_FULL_VERSION % 100000)
	#else
	/// @brief	Defined as integer literal that encodes
	/// the build number element of the compiler's version number
	/// The build number is the third element of the period-delimited version number
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 20706
	#define MSVC_BUILD_NUMBER	0
	#endif

	#if COMPILER_IS_MSVC
	/// @brief	Defined as integer literal that contains the
	/// revision number element of the compiler's version number
	/// The revision number is the fourth element of the period-delimited version number
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 1
	#define MSVC_REVISION	_MSC_BUILD
	#else
	/// @brief	Defined as integer literal that contains the
	/// revision number element of the compiler's version number
	/// The revision number is the fourth element of the period-delimited version number
	/// For example, if the version number of the Microsoft C/C++ compiler is 15.00.20706.01,
	/// the macro evaluates to 1
	#define MSVC_REVISION	0
	#endif

	#if COMPILER_IS_MSVC && defined(__CLR_VER__)
	/// @brief	Defined as boolean literal 'true' when the /clr compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CLR	true
	/// @brief	Defined as integer literal that encodes the version
	/// of the Common Language Runtime (CLR) used to compile the app
	/// The value is encoded in the form Mmmbbbbb,
	/// where M is the major version of the runtime,
	/// mm is the minor version of the runtime,
	/// and bbbbb is the build number
	#define MSVC_CLR_VERSION	__CLR_VER__
	/// @brief	Defined as integer literal that encodes the major version
	/// of the Common Language Runtime (CLR) used to compile the app
	#define MSVC_CLR_MAJOR_VERSION	(__CLR_VER__ / 10000000)
	/// @brief	Defined as integer literal that encodes the minor version
	/// of the Common Language Runtime (CLR) used to compile the app
	#define MSVC_CLR_MINOR_VERSION	(__CLR_VER__ / 100000 % 100)
	/// @brief	Defined as integer literal that encodes the build number
	/// of the Common Language Runtime (CLR) used to compile the app
	#define MSVC_CLR_BUILD_NUMBER	(__CLR_VER__ % 100000)
	#else
	/// @brief	Defined as boolean literal 'true' when the /clr compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CLR	false
	/// @brief	Defined as integer literal that encodes the version
	/// of the Common Language Runtime (CLR) used to compile the app
	/// The value is encoded in the form Mmmbbbbb,
	/// where M is the major version of the runtime,
	/// mm is the minor version of the runtime,
	/// and bbbbb is the build number
	#define MSVC_CLR_VERSION	0
	/// @brief	Defined as integer literal that encodes the major version
	/// of the Common Language Runtime (CLR) used to compile the app
	#define MSVC_CLR_MAJOR_VERSION	0
	/// @brief	Defined as integer literal that encodes the minor version
	/// of the Common Language Runtime (CLR) used to compile the app
	#define MSVC_CLR_MINOR_VERSION	0
	/// @brief	Defined as integer literal that encodes the build number
	/// of the Common Language Runtime (CLR) used to compile the app
	#define MSVC_CLR_BUILD_NUMBER	0
	#endif

	#if COMPILER_IS_MSVC && defined(__ATOM__)
	/// @brief	Defined as boolean literal 'true' when the /favor:ATOM compiler option is set
	/// and the compiler target is x86 or x64
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/favor-optimize-for-architecture-specifics">/favor:ATOM</a>
	#define MSVC_IS_FAVOR_ATOM	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /favor:ATOM compiler option is set
	/// and the compiler target is x86 or x64
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/favor-optimize-for-architecture-specifics">/favor:ATOM</a>
	#define MSVC_IS_FAVOR_ATOM	false
	#endif

	#if COMPILER_IS_MSVC && defined(_CHAR_UNSIGNED)
	/// @brief	Defined as boolean literal 'true' when the /J (Default char type is unsigned)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/j-default-char-type-is-unsigned">/J</a>
	#define MSVC_IS_CHAR_UNSIGNED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /J (Default char type is unsigned)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/j-default-char-type-is-unsigned">/J</a>
	#define MSVC_IS_CHAR_UNSIGNED	false
	#endif

	#if COMPILER_IS_MSVC && defined(_CONTROL_FLOW_GUARD)
	/// @brief	Defined as boolean literal 'true' when the /guard:cf (Enable Control Flow Guard)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/guard-enable-control-flow-guard">/guard:cf</a>
	#define MSVC_IS_CONTROL_FLOW_GUARD	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /guard:cf (Enable Control Flow Guard)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/guard-enable-control-flow-guard">/guard:cf</a>
	#define MSVC_IS_CONTROL_FLOW_GUARD	false
	#endif

	#if COMPILER_IS_MSVC && defined(__cplusplus_cli)
	/// @brief	Defined as boolean literal 'true' when compiled as C++ and the /clr compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CPP_CLI	true
	/// @brief	Defined as integer literal '200406' when compiled as C++ and the /clr compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_CPP_CLI	__cplusplus_cli
	#else
	/// @brief	Defined as boolean literal 'true' when compiled as C++ and the /clr compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CPP_CLI	false
	/// @brief	Defined as integer literal '200406' when compiled as C++ and the /clr compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_CPP_CLI	0
	#endif

	#if COMPILER_IS_MSVC && defined(__cplusplus_winrt)
	/// @brief	Defined as boolean literal 'true' when compiled as C++ and the /ZW (Windows Runtime Compilation)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zw-windows-runtime-compilation">/ZW</a>
	#define MSVC_IS_CPP_WIN_RT	true
	/// @brief	Defined as integer literal '201009' when compiled as C++ and the /ZW (Windows Runtime Compilation)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zw-windows-runtime-compilation">/ZW</a>
	#define MSVC_CPP_WIN_RT	__cplusplus_winrt
	#else
	/// @brief	Defined as boolean literal 'true' when compiled as C++ and the /ZW (Windows Runtime Compilation)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zw-windows-runtime-compilation">/ZW</a>
	#define MSVC_IS_CPP_WIN_RT	false
	/// @brief	Defined as integer literal '201009' when compiled as C++ and the /ZW (Windows Runtime Compilation)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zw-windows-runtime-compilation">/ZW</a>
	#define MSVC_CPP_WIN_RT	0
	#endif

	#if COMPILER_IS_MSVC && defined(_CPPRTTI)
	/// @brief	Defined as boolean literal 'true' when the /GR (Enable Run-Time Type Information)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/gr-enable-run-time-type-information">/GR</a>
	#define MSVC_IS_RTTI	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /GR (Enable Run-Time Type Information)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/gr-enable-run-time-type-information">/GR</a>
	#define MSVC_IS_RTTI	false
	#endif

	#if COMPILER_IS_MSVC && defined(_CPPUNWIND)
	/// @brief	Defined as boolean literal 'true' when one or more of the /GX (Enable Exception Handling),
	/// /clr (Common Language Runtime Compilation), or /EH (Exception Handling Model)
	/// compiler options are set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/gx-enable-exception-handling">/GX</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/eh-exception-handling-model">/EH</a>
	#define MSVC_IS_CPP_UNWIND	true
	#else
	/// @brief	Defined as boolean literal 'true' when one or more of the /GX (Enable Exception Handling),
	/// /clr (Common Language Runtime Compilation), or /EH (Exception Handling Model)
	/// compiler options are set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/gx-enable-exception-handling">/GX</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/eh-exception-handling-model">/EH</a>
	#define MSVC_IS_CPP_UNWIND	false
	#endif

	#if COMPILER_IS_MSVC && defined(_DEBUG)
	/// @brief	Defined as boolean literal 'true' when the /LDd, /MDd, or /MTd
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library">/LDd, /MDd, /MTd</a>
	#define MSVC_IS_DEBUG	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /LDd, /MDd, or /MTd
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library">/LDd, /MDd, /MTd</a>
	#define MSVC_IS_DEBUG	false
	#endif

	#if COMPILER_IS_MSVC && defined(_DLL)
	/// @brief	Defined as boolean literal 'true' when the /MD or /MDd (Multithreaded DLL)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library">/MD, /MDd</a>
	#define MSVC_IS_DLL	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /MD or /MDd (Multithreaded DLL)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library">/MD, /MDd</a>
	#define MSVC_IS_DLL	false
	#endif

	#if COMPILER_IS_MSVC && defined(_ISO_VOLATILE)
	/// @brief	Defined as boolean literal 'true' when the /volatile:iso compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/volatile-volatile-keyword-interpretation">/volatile:iso</a>
	#define MSVC_IS_ISO_VOLATILE	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /volatile:iso compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/volatile-volatile-keyword-interpretation">/volatile:iso</a>
	#define MSVC_IS_ISO_VOLATILE	false
	#endif

	#if COMPILER_IS_MSVC && defined(_KERNEL_MODE)
	/// @brief	Defined as boolean literal 'true' when the /kernel (Create Kernel Mode Binary)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/kernel-create-kernel-mode-binary">/kernel</a>
	#define MSVC_IS_KERNEL_MODE	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /kernel (Create Kernel Mode Binary)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/kernel-create-kernel-mode-binary">/kernel</a>
	#define MSVC_IS_KERNEL_MODE	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_ARM)
	/// @brief	Defined as boolean literal 'true' for compilations that target ARM processors
	#define MSVC_ARM	true
	#else
	/// @brief	Defined as boolean literal 'true' for compilations that target ARM processors
	#define MSVC_ARM	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_ARM_ARMV7VE)
	/// @brief	Defined as boolean literal 'true' when the /arch:ARMv7VE compiler option is set
	/// for compilations that target ARM processors
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/arch-arm">/arch:ARMv7VE</a>
	#define MSVC_IS_ARM7_VE	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /arch:ARMv7VE compiler option is set
	/// for compilations that target ARM processors
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/arch-arm">/arch:ARMv7VE</a>
	#define MSVC_IS_ARM7_VE	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_ARM_FP)
	/// @brief	Defined as integer literal value that indicates which /arch compiler option was set
	/// for ARM processor targets
	/// - A value in the range 30-39 if no /arch ARM option was specified,
	/// indicating the default architecture for ARM was set (VFPv3)
	/// - A value in the range 40-49 if /arch:VFPv4 was set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/arch-arm">/arch (ARM)</a>
	#define MSVC_ARM_FP	_M_ARM_FP
	#else
	/// @brief	Defined as integer literal value that indicates which /arch compiler option was set
	/// for ARM processor targets
	/// - A value in the range 30-39 if no /arch ARM option was specified,
	/// indicating the default architecture for ARM was set (VFPv3)
	/// - A value in the range 40-49 if /arch:VFPv4 was set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/arch-arm">/arch (ARM)</a>
	#define MSVC_ARM_FP	0
	#endif

	#if COMPILER_IS_MSVC && defined(_M_ARM64)
	/// @brief	Defined as boolean literal 'true' for compilations that target 64-bit ARM processors
	#define MSVC_IS_ARM64	true
	#else
	/// @brief	Defined as boolean literal 'true' for compilations that target 64-bit ARM processors
	#define MSVC_IS_ARM64	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_CEE)
	/// @brief	Defined as boolean literal 'true' when any /clr (Common Language Runtime Compilation)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CEE	true
	#else
	/// @brief	Defined as boolean literal 'true' when any /clr (Common Language Runtime Compilation)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CEE	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_CEE_PURE)
	/// @brief	Defined as boolean literal 'true' when the /clr:pure compiler option is set
	/// Deprecated beginning in Visual Studio 2015
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CEE_PURE	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /clr:pure compiler option is set
	/// Deprecated beginning in Visual Studio 2015
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CEE_PURE	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_CEE_SAFE)
	/// @brief	Defined as boolean literal 'true' when the /clr:safe compiler option is set
	/// Deprecated beginning in Visual Studio 2015
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CEE_SAFE	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /clr:safe compiler option is set
	/// Deprecated beginning in Visual Studio 2015
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_CEE_SAFE	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_FP_CONTRACT)
	/// @brief	Defined as boolean literal 'true' when the /fp:contract or /fp:fast compiler option is set
	/// Available beginning in Visual Studio 2022
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_CONTRACT	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /fp:contract or /fp:fast compiler option is set
	/// Available beginning in Visual Studio 2022
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_CONTRACT	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_FP_EXCEPT)
	/// @brief	Defined as boolean literal 'true' when the /fp:except or /fp:strict compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_EXCEPT	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /fp:except or /fp:strict compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_EXCEPT	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_FP_FAST)
	/// @brief	Defined as boolean literal 'true' when the /fp:fast compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_FAST	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /fp:fast compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_FAST	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_FP_PRECISE)
	/// @brief	Defined as boolean literal 'true' when the /fp:precise compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_PRECISE	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /fp:precise compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_PRECISE	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_FP_STRICT)
	/// @brief	Defined as boolean literal 'true' when the /fp:strict compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_STRICT	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /fp:strict compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fp-specify-floating-point-behavior">/fp</a>
	#define MSVC_IS_FP_STRICT	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_IX86)
	/// @brief	Defined as boolean literal 'true' for compilations that target x86 processors
	/// 'false' for x64 or ARM compilation targets
	#define MSVC_IS_X86	true
	#else
	/// @brief	Defined as boolean literal 'true' for compilations that target x86 processors
	/// 'false' for x64 or ARM compilation targets
	#define MSVC_IS_X86	false
	#endif

	#if COMPILER_IS_MSVC && defined(_M_IX86_FP)
	/// @brief	Defined as integer literal value that indicates the /arch compiler option that was set, or the default
	/// Always set when the compilation target is an x86 processor. Otherwise, '-1'
	/// - 0 when the /arch:IA32 compiler option was set
	/// - 1 when the /arch:SSE compiler option was set
	/// - 2 when the /arch:SSE2, /arch:AVX, /arch:AVX2, or /arch:AVX512 compiler option was set
	///	This value is the default if an /arch compiler option wasn't specified
	/// 	- When /arch:AVX is specified, the macro MSVC_IS_AVX is also set to 'true'
	/// 	- When /arch:AVX2 is specified, both MSVC_IS_AVX and MSVC_IS_AVX2 are also set to 'true'
	/// 	- When /arch:AVX512 is specified, MSVC_IS_AVX, MSVC_IS_AVX2, MSVC_IS_AVX512_BW,
	/// 	MSVC_IS_AVX512_CD, MSVC_IS_AVX512_DQ, MSVC_IS_AVX512_F, and MSVC_IS_AVX512_VL are also set to 'true'
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/arch-x86">/arch (x86)</a>
	#define MSVC_X86_FP	_M_IX86_FP
	#else
	/// @brief	Defined as integer literal value that indicates the /arch compiler option that was set, or the default
	/// Always set when the compilation target is an x86 processor. Otherwise, '-1'
	/// - 0 when the /arch:IA32 compiler option was set
	/// - 1 when the /arch:SSE compiler option was set
	/// - 2 when the /arch:SSE2, /arch:AVX, /arch:AVX2, or /arch:AVX512 compiler option was set
	///	This value is the default if an /arch compiler option wasn't specified
	/// 	- When /arch:AVX is specified, the macro MSVC_IS_AVX is also set to 'true'
	/// 	- When /arch:AVX2 is specified, both MSVC_IS_AVX and MSVC_IS_AVX2 are also set to 'true'
	/// 	- When /arch:AVX512 is specified, MSVC_IS_AVX, MSVC_IS_AVX2, MSVC_IS_AVX512_BW,
	/// 	MSVC_IS_AVX512_CD, MSVC_IS_AVX512_DQ, MSVC_IS_AVX512_F, and MSVC_IS_AVX512_VL are also set to 'true'
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/arch-x86">/arch (x86)</a>
	#define MSVC_X86_FP	(-1)
	#endif

	#if COMPILER_IS_MSVC && (defined(_M_X64) || defined(_M_AMD64))
	/// @brief	Defined as boolean literal 'true' for compilations that target x64 processors
	#define MSVC_IS_X64	true
	#else
	/// @brief	Defined as boolean literal 'true' for compilations that target x64 processors
	#define MSVC_IS_X64	false
	#endif

	#if COMPILER_IS_MSVC && defined(_MANAGED)
	/// @brief	Defined as boolean literal 'true' when the /clr compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_MANAGED	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /clr compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/clr-common-language-runtime-compilation">/clr</a>
	#define MSVC_IS_MANAGED	false
	#endif

	#if COMPILER_IS_MSVC && defined(_MSC_EXTENSIONS)
	/// @brief	Defined as boolean literal 'true' when the on-by-default /Ze (Enable Language Extensions)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/za-ze-disable-language-extensions">/Ze</a>
	#define MSVC_IS_EXTENSIONS	true
	#else
	/// @brief	Defined as boolean literal 'true' when the on-by-default /Ze (Enable Language Extensions)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/za-ze-disable-language-extensions">/Ze</a>
	#define MSVC_IS_EXTENSIONS	false
	#endif

	#if COMPILER_IS_MSVC && defined(_MSVC_LANG)
	/// @brief	Defined as integer literal that specifies the C++ language standard targeted by the compiler
	/// It's set only in code compiled as C++
	/// - '201402L' when the /std:c++14 compiler option is specified (default)
	/// - '201703L' when the /std:c++17 compiler option is specified
	/// - '202002L' when the /std:c++20 compiler option is specified
	/// - Higher, unspecified value when the /std:c++latest option is specified
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/std-specify-language-standard-version">/std</a>
	#define MSVC_CPP_STD	_MSVC_LANG
	#else
	/// @brief	Defined as integer literal that specifies the C++ language standard targeted by the compiler
	/// It's set only in code compiled as C++
	/// - '201402L' when the /std:c++14 compiler option is specified (default)
	/// - '201703L' when the /std:c++17 compiler option is specified
	/// - '202002L' when the /std:c++20 compiler option is specified
	/// - Higher, unspecified value when the /std:c++latest option is specified
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/std-specify-language-standard-version">/std</a>
	#define MSVC_CPP_STD	0
	#endif

	#if COMPILER_IS_MSVC && defined(__MSVC_RUNTIME_CHECKS)
	/// @brief	Defined as boolean literal 'true' when one of the /RTC compiler options is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/rtc-run-time-error-checks">/RTC</a>
	#define MSVC_IS_RUNTIME_CHECKS	true
	#else
	/// @brief	Defined as boolean literal 'true' when one of the /RTC compiler options is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/rtc-run-time-error-checks">/RTC</a>
	#define MSVC_IS_RUNTIME_CHECKS	false
	#endif

	#if COMPILER_IS_MSVC && defined(_MSVC_TRADITIONAL)
	/// @brief	Indicates which preprocessor is in use
	/// - Available beginning with Visual Studio 2017 version 15.8:
	/// 	- 'false' when the preprocessor conformance mode /experimental:preprocessor compiler option is set
	/// 	- 'true' by default, or when the /experimental:preprocessor- compiler option is set,
	/// 	to indicate the traditional preprocessor is in use
	/// - Available beginning with Visual Studio 2019 version 16.5:
	/// 	- 'false' when the preprocessor conformance mode /Zc:preprocessor compiler option is set
	/// 	- 'true' by default, or when the /Zc:preprocessor- compiler option is set,
	/// 	to indicate the traditional preprocessor is in use
	/// 	(essentially, /Zc:preprocessor replaces the deprecated /experimental:preprocessor)
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/experimental-preprocessor">/experimental:preprocessor</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zc-preprocessor">/Zc:preprocessor</a>
	#define MSVC_IS_TRADITIONAL_PREPROCESSOR	true
	#else
	/// @brief	Indicates which preprocessor is in use
	/// - Available beginning with Visual Studio 2017 version 15.8:
	/// 	- 'false' when the preprocessor conformance mode /experimental:preprocessor compiler option is set
	/// 	- 'true' by default, or when the /experimental:preprocessor- compiler option is set,
	/// 	to indicate the traditional preprocessor is in use
	/// - Available beginning with Visual Studio 2019 version 16.5:
	/// 	- 'false' when the preprocessor conformance mode /Zc:preprocessor compiler option is set
	/// 	- 'true' by default, or when the /Zc:preprocessor- compiler option is set,
	/// 	to indicate the traditional preprocessor is in use
	/// 	(essentially, /Zc:preprocessor replaces the deprecated /experimental:preprocessor)
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/experimental-preprocessor">/experimental:preprocessor</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zc-preprocessor">/Zc:preprocessor</a>
	#define MSVC_IS_TRADITIONAL_PREPROCESSOR	false
	#endif

	#if COMPILER_IS_MSVC && defined(_MT)
	/// @brief	Defined as boolean literal 'true' when /MD or /MDd (Multithreaded DLL)
	/// or /MT or /MTd (Multithreaded) is specified
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library">/MD, /MDd</a>
	#define MSVC_IS_MULTITHREADED	true
	#else
	/// @brief	Defined as boolean literal 'true' when /MD or /MDd (Multithreaded DLL)
	/// or /MT or /MTd (Multithreaded) is specified
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library">/MD, /MDd</a>
	#define MSVC_IS_MULTITHREADED	false
	#endif

	#if COMPILER_IS_MSVC && defined(_NATIVE_WCHAR_T_DEFINED)
	/// @brief	Defined as boolean literal 'true' when the /Zc:wchar_t compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zc-wchar-t-wchar-t-is-native-type">/Zc:wchar_t</a>
	#define MSVC_IS_NATIVE_WCHAR	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /Zc:wchar_t compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zc-wchar-t-wchar-t-is-native-type">/Zc:wchar_t</a>
	#define MSVC_IS_NATIVE_WCHAR	false
	#endif

	#if COMPILER_IS_MSVC && defined(_OPENMP)
	/// @brief	Defined as boolean literal 'true', when the /openmp (Enable OpenMP 2.0 Support) compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/openmp-enable-openmp-2-0-support">/openmp</a>
	#define MSVC_IS_OPENMP	true
	/// @brief	'200203', when the /openmp (Enable OpenMP 2.0 Support) compiler option is set
	/// This value encodes the date of the OpenMP specification implemented by MSVC
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/openmp-enable-openmp-2-0-support">/openmp</a>
	#define MSVC_OPENMP	_OPENMP
	#else
	/// @brief	Defined as boolean literal 'true', when the /openmp (Enable OpenMP 2.0 Support) compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/openmp-enable-openmp-2-0-support">/openmp</a>
	#define MSVC_IS_OPENMP	false
	/// @brief	'200203', when the /openmp (Enable OpenMP 2.0 Support) compiler option is set
	/// This value encodes the date of the OpenMP specification implemented by MSVC
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/openmp-enable-openmp-2-0-support">/openmp</a>
	#define MSVC_OPENMP	0
	#endif

	#if COMPILER_IS_MSVC && defined(_PREFAST_)
	/// @brief	Defined as boolean literal 'true' when the /analyze compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/analyze-code-analysis">/analyze</a>
	#define MSVC_IS_ANALYZE	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /analyze compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/analyze-code-analysis">/analyze</a>
	#define MSVC_IS_ANALYZE	false
	#endif

	#if COMPILER_IS_MSVC && defined(__SANITIZE_ADDRESS__)
	/// @brief	Defined as boolean literal 'true' when the /fsanitize=address compiler option is set
	/// Available beginning with Visual Studio 2019 version 16.9
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fsanitize">/fsanitize</a>
	#define MSVC_IS_SANITIZE_ADDRESS	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /fsanitize=address compiler option is set
	/// Available beginning with Visual Studio 2019 version 16.9
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/fsanitize">/fsanitize</a>
	#define MSVC_IS_SANITIZE_ADDRESS	false
	#endif

	#if COMPILER_IS_MSVC && defined(_VC_NODEFAULTLIB)
	/// @brief	Defined as boolean literal 'true' when the /Zl (Omit Default Library Name)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zl-omit-default-library-name">/Zl</a>
	#define MSVC_IS_NO_DEFAULT_LIB	true
	#else
	/// @brief	Defined as boolean literal 'true' when the /Zl (Omit Default Library Name)
	/// compiler option is set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zl-omit-default-library-name">/Zl</a>
	#define MSVC_IS_NO_DEFAULT_LIB	false
	#endif

	#if COMPILER_IS_MSVC && defined(_WIN32)
	/// @brief	Defined as boolean literal 'true' when the compilation target is
	/// 32-bit ARM, 64-bit ARM, x86, or x64
	#define MSVC_IS_WIN32	true
	#else
	/// @brief	Defined as boolean literal 'true' when the compilation target is
	/// 32-bit ARM, 64-bit ARM, x86, or x64
	#define MSVC_IS_WIN32	false
	#endif

	#if COMPILER_IS_MSVC && defined(_WIN64)
	/// @brief	Defined as boolean literal 'true' when the compilation target is
	/// 64-bit ARM or x64
	#define MSVC_IS_WIN64	true
	#else
	/// @brief	Defined as boolean literal 'true' when the compilation target is
	/// 64-bit ARM or x64
	#define MSVC_IS_WIN64	false
	#endif

	#if COMPILER_IS_MSVC && defined(_WINRT_DLL)
	/// @brief	Defined as boolean literal 'true' when compiled as C++ and both
	/// /ZW (Windows Runtime Compilation) and /LD or /LDd
	/// compiler options are set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library">/LD, /LDd</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zw-windows-runtime-compilation">/ZW</a>
	#define MSVC_IS_WIN_RT_DLL	true
	#else
	/// @brief	Defined as boolean literal 'true' when compiled as C++ and both
	/// /ZW (Windows Runtime Compilation) and /LD or /LDd
	/// compiler options are set
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library">/LD, /LDd</a>
	/// @see	<a href="https://docs.microsoft.com/en-us/cpp/build/reference/zw-windows-runtime-compilation">/ZW</a>
	#define MSVC_IS_WIN_RT_DLL	false
	#endif
}
