#pragma once


namespace Std::inline Platform::inline Compiler::Gcc
{
	// todo: u8 string literals?

	#if defined(__GNUC__)
	/// @brief	Defined as boolean literal 'true' if current compiler is GCC
	#define COMPILER_IS_GCC	true
	#else
	/// @brief	Defined as boolean literal 'true' if current compiler is GCC
	#define COMPILER_IS_GCC	false
	#endif

	#if COMPILER_IS_GCC
	#undef COMPILER_NAME
	#undef COMPILER_FULL_NAME
	#undef COUNTER
	#undef TIMESTAMP

	/// @brief	Defined as string literal that
	/// encodes short name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------|---------
	/// Unknown	| 
	/// ARM	| Arm
	/// Clang	| Clang
	/// GNU CC	| Gcc
	/// Microsoft	| Msvc
	/// </pre>
	#define COMPILER_NAME	"Gcc"

	/// @brief	Defined as string literal that
	/// encodes full name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------|----------------------------
	/// Unknown	| 
	/// ARM	| Arm C/C++
	/// Clang	| Clang C Language Family
	/// GNU CC	| Gnu Compiler Collection
	/// Microsoft	| Microsoft Visual C++
	/// </pre>
	#define COMPILER_FULL_NAME	"Gnu Compiler Collection"

	/// @brief	Expands to an integer literal that starts at 0
	/// The value is incremented by 1 every time it's used in a source file,
	/// or in included headers of the source file\n
	/// Care must be taken to ensure that GCC COUNTER is not expanded prior to inclusion
	/// of precompiled headers which use it. Otherwise, the precompiled headers will not be used
	#define COUNTER	__COUNTER__

	/// @brief	Expands to a string literal that describes the date
	/// and time of the last modification of the current source file
	/// The string constant contains abbreviated day of the week, month,
	/// day of the month, time in hh:mm:ss form, year and looks like "Sun Sep 16 01:03:52 1973"
	/// If the day of the month is less than 10, it is padded with a space on the left\n
	/// If GCC cannot determine the current date, it will emit a warning message (once per compilation)
	/// and COMPILER_TIMESTAMP will expand to "??? ??? ?? ??:??:?? ????"
	#define TIMESTAMP	__TIMESTAMP__
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined as integer literal that represents
	/// the depth of nesting in include files starting from 0
	/// The value of this macro is incremented on every ‘#include’
	/// directive and decremented at the end of every included file
	#define GCC_INCLUDE_LEVEL	__INCLUDE_LEVEL__
	#else
	/// @brief	Defined as integer literal that represents
	/// the depth of nesting in include files starting from 0
	/// The value of this macro is incremented on every ‘#include’
	/// directive and decremented at the end of every included file
	#define GCC_INCLUDE_LEVEL	-1
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined as string literal that
	/// encodes the name of the main file
	#define GCC_MAIN_FILE	__BASE_FILE__
	#else
	/// @brief	Defined as string literal that
	/// encodes the name of the main file
	#define GCC_MAIN_FILE	""
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined as string literal that
	/// encodes the basename of the current file
	/// For example, processing 'project/include/header.h'
	/// would set this macro to 'header.h'
	#define GCC_FILE_NAME	__FILE_NAME__
	#else
	/// @brief	Defined as string literal that
	/// encodes the basename of the current file
	/// For example, processing 'project/include/header.h'
	/// would set this macro to 'header.h'
	#define GCC_FILE_NAME	""
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined as integer literal that encodes
	/// the major number element of the compiler's version number
	/// The major number is the first element of the period-delimited version number\n
	/// For example, if the version number of the GCC compiler is 3.2.0,
	/// the macro evaluates to 3
	#define GCC_MAJOR_VERSION	__GNUC__
	#else
	/// @brief	Defined as integer literal that encodes
	/// the major number element of the compiler's version number
	/// The major number is the first element of the period-delimited version number\n
	/// For example, if the version number of the GCC compiler is 3.2.0,
	/// the macro evaluates to 3
	#define GCC_MAJOR_VERSION	0
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined as integer literal that encodes
	/// the minor number element of the compiler's version number
	/// The minor number is the second element of the period-delimited version number\n
	/// For example, if the version number of the GCC compiler is 3.2.0,
	/// the macro evaluates to 2
	#define GCC_MINOR_VERSION	__GNUC_MINOR__
	#else
	/// @brief	Defined as integer literal that encodes
	/// the minor number element of the compiler's version number
	/// The minor number is the second element of the period-delimited version number\n
	/// For example, if the version number of the GCC compiler is 3.2.0,
	/// the macro evaluates to 2
	#define GCC_MINOR_VERSION	0
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined as integer literal that encodes
	/// the patch number element of the compiler's version number
	/// The patch number is the third element of the period-delimited version number\n
	/// For example, if the version number of the GCC compiler is 3.2.0,
	/// the macro evaluates to 0
	#define GCC_PATCH_VERSION	__GNUC_PATCHLEVEL__
	#else
	/// @brief	Defined as integer literal that encodes
	/// the patch number element of the compiler's version number
	/// The patch number is the third element of the period-delimited version number\n
	/// For example, if the version number of the GCC compiler is 3.2.0,
	/// the macro evaluates to 0
	#define GCC_PATCH_VERSION	0
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined as integer literal that encodes the
	/// major, minor and patch number elements of the compiler's version number
	/// The major number is the first element of the period-delimited
	/// version number, the minor number is the second element, the patch is the third element\n
	/// For example, if the version number of the Microsoft C/C++ compiler
	/// is 3.2.0, the macro evaluates to 30200
	#define GCC_VERSION	(GCC_MAJOR_VERSION * 10000 + GCC_MINOR_VERSION * 100 + GCC_PATCH_NUMBER)
	#else
	/// @brief	Defined as integer literal that encodes the
	/// major, minor and patch number elements of the compiler's version number
	/// The major number is the first element of the period-delimited
	/// version number, the minor number is the second element, the patch is the third element\n
	/// For example, if the version number of the Microsoft C/C++ compiler
	/// is 3.2.0, the macro evaluates to 30200
	#define GCC_VERSION	0
	#endif

	#if COMPILER_IS_GCC
	#define GCC_ORDER_LITTLE_ENDIAN	__ORDER_LITTLE_ENDIAN__
	#else
	#define GCC_ORDER_LITTLE_ENDIAN	0
	#endif

	#if COMPILER_IS_GCC
	#define GCC_ORDER_BIG_ENDIAN	__ORDER_BIG_ENDIAN__
	#else
	#define GCC_ORDER_BIG_ENDIAN	0
	#endif

	#if COMPILER_IS_GCC
	#define GCC_ORDER_PDP_ENDIAN	__ORDER_PDP_ENDIAN__
	#else
	#define GCC_ORDER_PDP_ENDIAN	0
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined to one of the values GCC_ORDER_LITTLE_ENDIAN,
	/// GCC_ORDER_BIG_ENDIAN, or GCC_ORDER_PDP_ENDIAN to reflect the
	/// layout of multi-byte and multi-word quantities in memory\n
	/// If GCC_BYTE_ORDER is equal to GCC_ORDER_LITTLE_ENDIAN or GCC_ORDER_BIG_ENDIAN,
	/// then multi-byte and multi-word quantities are laid out identically:
	/// the byte (word) at the lowest address is the least significant or
	/// most significant byte (word) of the quantity, respectively\n
	/// If GCC_BYTE_ORDER is equal to GCC_ORDER_PDP_ENDIAN,
	/// then bytes in 16-bit words are laid out in an little-endian fashion,
	/// whereas the 16-bit subwords of a 32-bit quantity are laid out in big-endian fashion
	#define GCC_BYTE_ORDER	__BYTE_ORDER__
	#else
	/// @brief	Defined to one of the values GCC_ORDER_LITTLE_ENDIAN,
	/// GCC_ORDER_BIG_ENDIAN, or GCC_ORDER_PDP_ENDIAN to reflect the
	/// layout of multi-byte and multi-word quantities in memory\n
	/// If GCC_BYTE_ORDER is equal to GCC_ORDER_LITTLE_ENDIAN or GCC_ORDER_BIG_ENDIAN,
	/// then multi-byte and multi-word quantities are laid out identically:
	/// the byte (word) at the lowest address is the least significant or
	/// most significant byte (word) of the quantity, respectively\n
	/// If GCC_BYTE_ORDER is equal to GCC_ORDER_PDP_ENDIAN,
	/// then bytes in 16-bit words are laid out in an little-endian fashion,
	/// whereas the 16-bit subwords of a 32-bit quantity are laid out in big-endian fashion
	#define GCC_BYTE_ORDER	0
	#endif

	#if COMPILER_IS_GCC
	/// @brief	Defined to one of the values GCC_ORDER_LITTLE_ENDIAN or GCC_ORDER_BIG_ENDIAN
	/// to reflect the layout of the words of multi-word floating-point quantities
	#define GCC_FLOAT_BYTE_ORDER	__FLOAT_WORD_ORDER__
	#else
	/// @brief	Defined to one of the values GCC_ORDER_LITTLE_ENDIAN or GCC_ORDER_BIG_ENDIAN
	/// to reflect the layout of the words of multi-word floating-point quantities
	#define GCC_FLOAT_BYTE_ORDER	0
	#endif

	#if defined(__GNUG__) || defined(__MINGW32__)
	/// @brief	Defined as boolean literal 'true'
	/// when GNU C++ compiler is in use
	#define GCC_IS_CPP	true
	#else
	/// @brief	Defined as boolean literal 'true'
	/// when GNU C++ compiler is in use
	#define GCC_IS_CPP	false
	#endif

	#if defined(__STRICT_ANSI__)
	/// @brief	Defined as boolean literal 'true'
	/// when the -ansi switch, or a -std switch specifying strict
	/// conformance to some version of ISO C or ISO C++, is set
	#define GCC_IS_STRICT_ANSI	true
	#else
	/// @brief	Defined as boolean literal 'true'
	/// when the -ansi switch, or a -std switch specifying strict
	/// conformance to some version of ISO C or ISO C++, is set
	#define GCC_IS_STRICT_ANSI	false
	#endif

	#if defined(__ELF__)
	/// @brief	Defined as boolean literal 'true'
	/// when the target uses the ELF object format
	#define GCC_IS_ELF	true
	#else
	/// @brief	Defined as boolean literal 'true'
	/// when the target uses the ELF object format
	#define GCC_IS_ELF	false
	#endif

	#if defined(__OPTIMIZE__)
	/// @brief	Defined as boolean literal 'true'
	/// in all optimizing compilations
	#define GCC_IS_OPTIMIZE	true
	#else
	/// @brief	Defined as boolean literal 'true'
	/// in all optimizing compilations
	#define GCC_IS_OPTIMIZE	false
	#endif

	#if defined(__OPTIMIZE_SIZE__)
	/// @brief	Defined as boolean literal 'true' when
	/// the compiler is optimizing for size, not speed
	#define GCC_IS_OPTIMIZE_SIZE	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the compiler is optimizing for size, not speed
	#define GCC_IS_OPTIMIZE_SIZE	false
	#endif

	#if defined(__NO_INLINE__)
	/// @brief	Defined as boolean literal 'true' when
	/// no functions will be inlined into their callers
	/// (when not optimizing, or when inlining has been
	/// specifically disabled by -fno-inline)
	#define GCC_IS_NO_INLINE	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// no functions will be inlined into their callers
	/// (when not optimizing, or when inlining has been
	/// specifically disabled by -fno-inline)
	#define GCC_IS_NO_INLINE	false
	#endif

	#if defined(__GNUC_GNU_INLINE__)
	/// @brief	Defined as boolean literal 'true' when functions
	/// declared inline will be handled in GCC’s traditional gnu90 mode
	/// Object files will contain externally visible definitions of
	/// all functions declared 'inline' without 'extern' or 'static'
	/// They will not contain any definitions of any functions declared 'extern inline'
	#define GCC_IS_TRADITIONAL_INLINE	true
	#else
	/// @brief	Defined as boolean literal 'true' when functions
	/// declared inline will be handled in GCC’s traditional gnu90 mode
	/// Object files will contain externally visible definitions of
	/// all functions declared 'inline' without 'extern' or 'static'
	/// They will not contain any definitions of any functions declared 'extern inline'
	#define GCC_IS_TRADITIONAL_INLINE	false
	#endif

	#if defined(__GNUC_STDC_INLINE__)
	/// @brief	Defined as boolean literal 'true' when functions
	/// declared inline will be handled according to the ISO C99 or later standards
	/// Object files will contain externally visible definitions of
	/// all functions declared 'extern inline'
	/// They will not contain definitions of any functions declared 'inline' without 'extern'
	#define GCC_IS_C99_INLINE	true
	#else
	/// @brief	Defined as boolean literal 'true' when functions
	/// declared inline will be handled according to the ISO C99 or later standards
	/// Object files will contain externally visible definitions of
	/// all functions declared 'extern inline'
	/// They will not contain definitions of any functions declared 'inline' without 'extern'
	#define GCC_IS_C99_INLINE	false
	#endif

	#if defined(__CHAR_UNSIGNED__)
	/// @brief	Defined as boolean literal 'true' when
	/// the data type 'char' is unsigned on the target machine
	#define GCC_IS_CHAR_UNSIGNED	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the data type 'char' is unsigned on the target machine
	#define GCC_IS_CHAR_UNSIGNED	false
	#endif

	#if defined(__WCHAR_UNSIGNED__)
	/// @brief	Defined as boolean literal 'true' when
	/// the data type wchar_t is unsigned and the front-end is in C++ mode
	#define GCC_IS_WCHAR_UNSIGNED	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the data type wchar_t is unsigned and the front-end is in C++ mode
	#define GCC_IS_WCHAR_UNSIGNED	false
	#endif

	#if defined(__REGISTER_PREFIX__)
	/// @brief	Expands to a single token (not a string constant) which is
	/// the prefix applied to CPU register names in assembly language for this target
	/// You can use it to write assembly that is usable in multiple environments
	/// For example, in the m68k-aout environment it expands to nothing,
	/// but in the m68k-coff environment it expands to a single ‘%’
	#define GCC_ASM_REGISTER_PREFIX	__REGISTER_PREFIX__
	#else
	/// @brief	Expands to a single token (not a string constant) which is
	/// the prefix applied to CPU register names in assembly language for this target
	/// You can use it to write assembly that is usable in multiple environments
	/// For example, in the m68k-aout environment it expands to nothing,
	/// but in the m68k-coff environment it expands to a single ‘%’
	#define GCC_ASM_REGISTER_PREFIX
	#endif

	#if defined(__USER_LABEL_PREFIX__)
	/// @brief	Expands to a single token which is the prefix
	/// applied to user labels (symbols visible to C code / mangled names) in assembly
	/// For example, in the m68k-aout environment it expands to an ‘_’,
	/// but in the m68k-coff environment it expands to nothing
	///
	/// This macro will have the correct definition even if -f(no-)underscores is in use,
	/// but it will not be correct if target-specific options that adjust this prefix
	/// are used (e.g. the OSF/rose -mno-underscores option)
	#define GCC_USER_LABEL_PREFIX	__USER_LABEL_PREFIX__
	#else
	/// @brief	Expands to a single token which is the prefix
	/// applied to user labels (symbols visible to C code / mangled names) in assembly
	/// For example, in the m68k-aout environment it expands to an ‘_’,
	/// but in the m68k-coff environment it expands to nothing\n
	/// This macro will have the correct definition even if -f(no-)underscores is in use,
	/// but it will not be correct if target-specific options that adjust this prefix
	/// are used (e.g. the OSF/rose -mno-underscores option)
	#define GCC_USER_LABEL_PREFIX
	#endif

	#if defined(__DEPRECATED)
	/// @brief	Defined as boolean literal 'true' when compiling
	/// a C++ source file with warnings about deprecated constructs enabled
	/// These warnings are enabled by default, but can be disabled with -Wno-deprecated
	#define GCC_IS_DEPRECATED	true
	#else
	/// @brief	Defined as boolean literal 'true' when compiling
	/// a C++ source file with warnings about deprecated constructs enabled
	/// These warnings are enabled by default, but can be disabled with -Wno-deprecated
	#define GCC_IS_DEPRECATED	false
	#endif

	#if defined(__EXCEPTIONS)
	/// @brief	Defined as boolean literal 'true' when
	/// compiling a C++ source file with exceptions enabled
	/// Exceptions can be disabled with -fno-exceptions
	#define GCC_IS_EXCEPTIONS	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// compiling a C++ source file with exceptions enabled
	/// Exceptions can be disabled with -fno-exceptions
	#define GCC_IS_EXCEPTIONS	false
	#endif

	#if defined(__GXX_RTTI)
	/// @brief	Defined as boolean literal 'true' when compiling
	/// a C++ source file with runtime type identification enabled
	/// RTTI can be disabled with -fno-rtti
	#define GCC_IS_RTTI	true
	#else
	/// @brief	Defined as boolean literal 'true' when compiling
	/// a C++ source file with runtime type identification enabled
	/// RTTI can be disabled with -fno-rtti
	#define GCC_IS_RTTI	false
	#endif

	#if defined(__USING_SJLJ_EXCEPTIONS__)
	/// @brief	Defined as boolean literal 'true' when the compiler uses
	/// the old mechanism based on 'setjmp' and 'longjmp' for exception handling
	#define GCC_IS_OLD_EXCEPTIONS	true
	#else
	/// @brief	Defined as boolean literal 'true' when the compiler uses
	/// the old mechanism based on 'setjmp' and 'longjmp' for exception handling
	#define GCC_IS_OLD_EXCEPTIONS	false
	#endif

	#if defined(__GXX_WEAK__)
	/// @brief	Defined as boolean literal 'true' when the compiler will
	/// use weak symbols, COMDAT sections, or other similar techniques to collapse
	/// symbols with “vague linkage” that are defined in multiple translation units
	/// In general, user code should not need to make use of this macro
	#define GCC_IS_WEAK	true
	#else
	/// @brief	Defined as boolean literal 'true' when the compiler will
	/// use weak symbols, COMDAT sections, or other similar techniques to collapse
	/// symbols with “vague linkage” that are defined in multiple translation units
	/// In general, user code should not need to make use of this macro
	#define GCC_IS_WEAK	false
	#endif

	#if defined(__LP64__) || defined(_LP64)
	/// @brief	Defined as boolean literal 'true' when the compilation
	/// is for a target where 'long int' and pointer both use 64-bits
	/// and 'int' uses 32-bit
	#define GCC_IS_LP64	true
	#else
	/// @brief	Defined as boolean literal 'true' when the compilation
	/// is for a target where 'long int' and pointer both use 64-bits
	/// and 'int' uses 32-bit
	#define GCC_IS_LP64	false
	#endif

	#if defined(__SSP__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fstack-protector is in use
	#define GCC_IS_SSP	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fstack-protector is in use
	#define GCC_IS_SSP	false
	#endif

	#if defined(__SSP_ALL__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fstack-protector-all is in use
	#define GCC_IS_SSP_ALL	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fstack-protector-all is in use
	#define GCC_IS_SSP_ALL	false
	#endif

	#if defined(__SSP_STRONG__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fstack-protector-strong is in use
	#define GCC_IS_SSP_STRONG	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fstack-protector-strong is in use
	#define GCC_IS_SSP_STRONG	false
	#endif

	#if defined(__SSP_EXPLICIT__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fstack-protector-explicit is in use
	#define GCC_IS_SSP_EXPLICIT	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fstack-protector-explicit is in use
	#define GCC_IS_SSP_EXPLICIT	false
	#endif

	#if defined(__SANITIZE_ADDRESS__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fsanitize=address or -fsanitize=kernel-address are in use
	#define GCC_IS_SANITIZE_ADDRESS	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fsanitize=address or -fsanitize=kernel-address are in use
	#define GCC_IS_SANITIZE_ADDRESS	false
	#endif

	#if defined(__SANITIZE_THREAD__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fsanitize=thread is in use
	#define GCC_IS_SANITIZE_THREAD	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fsanitize=thread is in use
	#define GCC_IS_SANITIZE_THREAD	false
	#endif

	#if defined(__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1)
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 1 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_1	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 1 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_1	false
	#endif

	#if defined(__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2)
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 2 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_2	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 2 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_2	false
	#endif

	#if defined(__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4)
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 4 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_4	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 4 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_4	false
	#endif

	#if defined(__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8)
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 8 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_8	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 8 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_8	false
	#endif

	#if defined(__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16)
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 16 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_16	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the target processor supports atomic compare and
	/// swap operations on operands 16 byte in length
	#define GCC_IS_ATOMIC_COMPARE_SWAP_16	false
	#endif

	// todo: check __builtin_speculation_safe_value function
	#if defined(__HAVE_SPECULATION_SAFE_VALUE)
	/// @brief	Defined as boolean literal 'true' when
	/// this version of GCC supports __builtin_speculation_safe_value
	#define GCC_IS_SPECULATION_SAFE_VALUE	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// this version of GCC supports __builtin_speculation_safe_value
	#define GCC_IS_SPECULATION_SAFE_VALUE	false
	#endif

	#if defined(__GCC_HAVE_DWARF2_CFI_ASM)
	/// @brief	Defined as boolean literal 'true' when
	/// compiler is emitting DWARF CFI directives to the assembler
	#define GCC_IS_DWARF_CFI_ASM	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// compiler is emitting DWARF CFI directives to the assembler
	#define GCC_IS_DWARF_CFI_ASM	false
	#endif

	// todo: check FMA functions
	#if defined(__FP_FAST_FMA)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma builtin functions
	#define GCC_IS_FP_FAST_FMA	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma builtin functions
	#define GCC_IS_FP_FAST_FMA	false
	#endif

	// todo: check FMAF functions
	#if defined(__FP_FAST_FMAF)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fmaf builtin functions
	#define GCC_IS_FP_FAST_FMAF	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fmaf builtin functions
	#define GCC_IS_FP_FAST_FMAF	false
	#endif

	// todo: check FMAL functions
	#if defined(__FP_FAST_FMAL)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fmal builtin functions
	#define GCC_IS_FP_FAST_FMAL	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fmal builtin functions
	#define GCC_IS_FP_FAST_FMAL	false
	#endif

	// todo: check FMAF16 functions and _Floatn type
	#if defined(__FP_FAST_FMAF16)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatn type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF16	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatn type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF16	false
	#endif

	// todo: check FMAF32 functions and _Floatn type
	#if defined(__FP_FAST_FMAF32)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatn type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF32	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatn type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF32	false
	#endif

	// todo: check FMAF64 functions and _Floatn type
	#if defined(__FP_FAST_FMAF64)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatn type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF64	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatn type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF64	false
	#endif

	// todo: check FMAF128 functions and _Floatn type
	#if defined(__FP_FAST_FMAF128)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatn type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF128	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatn type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF128	false
	#endif

	// todo: check FMAF32X functions and _Floatnx type
	#if defined(__FP_FAST_FMAF32X)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatnx type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF32X	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatnx type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF32X	false
	#endif

	// todo: check FMAF64X functions and _Floatnx type
	#if defined(__FP_FAST_FMAF64X)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatnx type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF64X	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatnx type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF64X	false
	#endif

	// todo: check FMAF128X functions and _Floatnx type
	#if defined(__FP_FAST_FMAF128X)
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatnx type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF128X	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// the backend supports the fma functions using the
	/// additional _Floatnx type that defined
	/// in ISO/IEC TS 18661-3:2015
	#define GCC_IS_FP_FAST_FMAF128X	false
	#endif

	#if defined(__GCC_IEC_559)
	/// @brief	Defined as integer literal to indicate the intended level
	/// of support for IEEE 754 (IEC 60559) floating-point arithmetic:
	///		- If 0, it indicates that the combination of the compiler configuration
	///		and the command-line options is not intended to support IEEE 754 arithmetic
	///		for float and double as defined in C99 and C11 Annex F (for example, that the
	///		standard rounding modes and exceptions are not supported, or that optimizations
	///		are enabled that conflict with IEEE 754 semantics)
	///		- If 1, it indicates that IEEE 754 arithmetic is intended to be supported
	///		This does not mean that all relevant language features are supported by GCC
	///		- If 2 or more, it additionally indicates support for IEEE 754-2008 (in particular,
	///		that the binary encodings for quiet and signaling NaNs are as specified in IEEE 754-2008)\n
	///	This macro does not indicate the default state of command-line options that control optimizations
	///	that C99 and C11 permit to be controlled by standard pragmas, where those standards do not require
	///	a particular default state. It does not indicate whether optimizations respect signaling NaN semantics
	///	(the macro for that is GCC_IS_SIGNALING_NAN)\n
	///	It does not indicate support for decimal floating point or the IEEE 754 binary16 and binary128 types
	#define GCC_IS_IEC_559	__GCC_IEC_559
	#else
	/// @brief	Defined as integer literal to indicate the intended level
	/// of support for IEEE 754 (IEC 60559) floating-point arithmetic:
	///		- If 0, it indicates that the combination of the compiler configuration
	///		and the command-line options is not intended to support IEEE 754 arithmetic
	///		for float and double as defined in C99 and C11 Annex F (for example, that the
	///		standard rounding modes and exceptions are not supported, or that optimizations
	///		are enabled that conflict with IEEE 754 semantics)
	///		- If 1, it indicates that IEEE 754 arithmetic is intended to be supported
	///		This does not mean that all relevant language features are supported by GCC
	///		- If 2 or more, it additionally indicates support for IEEE 754-2008 (in particular,
	///		that the binary encodings for quiet and signaling NaNs are as specified in IEEE 754-2008)\n
	///	This macro does not indicate the default state of command-line options that control optimizations
	///	that C99 and C11 permit to be controlled by standard pragmas, where those standards do not require
	///	a particular default state. It does not indicate whether optimizations respect signaling NaN semantics
	///	(the macro for that is GCC_IS_SIGNALING_NAN)\n
	///	It does not indicate support for decimal floating point or the IEEE 754 binary16 and binary128 types
	#define GCC_IS_IEC_559	0
	#endif

	#if defined(__GCC_IEC_559_COMPLEX)
	/// @brief	Defined as integer literal to indicate the intended level
	/// of support for IEEE 754 (IEC 60559) floating-point arithmetic for complex numbers,
	/// as defined in C99 and C11 Annex G:
	///		- If 0, it indicates that the combination of the compiler configuration
	///		and the command-line options is not intended to support Annex G requirements
	///		(for example, because -fcx-limited-range was used)
	///		- If 1 or more, it indicates that it is intended to support those requirements
	///		This does not mean that all relevant language features are supported by GCC
	#define GCC_IS_IEC_559_COMPLEX	__GCC_IEC_559_COMPLEX
	#else
	/// @brief	Defined as integer literal to indicate the intended level
	/// of support for IEEE 754 (IEC 60559) floating-point arithmetic for complex numbers,
	/// as defined in C99 and C11 Annex G:
	///		- If 0, it indicates that the combination of the compiler configuration
	///		and the command-line options is not intended to support Annex G requirements
	///		(for example, because -fcx-limited-range was used)
	///		- If 1 or more, it indicates that it is intended to support those requirements
	///		This does not mean that all relevant language features are supported by GCC
	#define GCC_IS_IEC_559_COMPLEX	0
	#endif

	#if defined(__NO_SIGNED_ZEROS__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fno-signed-zeros is used, or enabled by another
	/// option such as -ffast-math or by default
	#define GCC_IS_NO_SIGNED_ZEROS	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fno-signed-zeros is used, or enabled by another
	/// option such as -ffast-math or by default
	#define GCC_IS_NO_SIGNED_ZEROS	false
	#endif

	#if defined(__NO_MATH_ERRNO__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fno-math-errno is used, or enabled by another
	/// option such as -ffast-math or by default
	#define GCC_IS_NO_MATH_ERRNO	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fno-math-errno is used, or enabled by another
	/// option such as -ffast-math or by default
	#define GCC_IS_NO_MATH_ERRNO	false
	#endif

	#if defined(__NO_TRAPPING_MATH__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fno-trapping-math is used
	#define GCC_IS_NO_TRAPPING_MATH	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fno-trapping-math is used
	#define GCC_IS_NO_TRAPPING_MATH	false
	#endif

	#if defined(__RECIPROCAL_MATH__)
	/// @brief	Defined as boolean literal 'true' when
	/// -freciprocal-math is used, or enabled by another
	/// option such as -ffast-math or by default
	#define GCC_IS_RECIPROCAL_MATH	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -freciprocal-math is used, or enabled by another
	/// option such as -ffast-math or by default
	#define GCC_IS_RECIPROCAL_MATH	false
	#endif

	#if defined(__ASSOCIATIVE_MATH__)
	/// @brief	Defined as boolean literal 'true' when
	/// -fassociative-math is used, or enabled by another
	/// option such as -ffast-math or by default
	#define GCC_IS_ASSOCIATIVE_MATH	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -fassociative-math is used, or enabled by another
	/// option such as -ffast-math or by default
	#define GCC_IS_ASSOCIATIVE_MATH	false
	#endif

	#if defined(__ROUNDING_MATH__)
	/// @brief	Defined as boolean literal 'true' when
	/// -frounding-math is used
	#define GCC_IS_ROUNDING_MATH	true
	#else
	/// @brief	Defined as boolean literal 'true' when
	/// -frounding-math is used
	#define GCC_IS_ROUNDING_MATH	false
	#endif

	#if defined(__GNUC_EXECUTION_CHARSET_NAME)
	/// @brief	Expands to a narrow string literal of the name of
	/// the narrow and wide compile-time execution character set used
	/// It directly reflects the name passed to the option -fexec-charset,
	/// or the defaults documented for those options
	/// (that is, it can expand to something like "UTF-8")
	#define GCC_CHARSET	__GNUC_EXECUTION_CHARSET_NAME
	#else
	/// @brief	Expands to a narrow string literal of the name of
	/// the narrow and wide compile-time execution character set used
	/// It directly reflects the name passed to the option -fexec-charset,
	/// or the defaults documented for those options
	/// (that is, it can expand to something like "UTF-8")
	#define GCC_CHARSET	""
	#endif

	#if defined(__GNUC_WIDE_EXECUTION_CHARSET_NAME)
	/// @brief	Expands to a narrow string literal of the name of
	/// the narrow and wide compile-time execution character set used
	/// It directly reflects the name passed to the option -fwide-exec-charset,
	/// or the defaults documented for those options
	/// (that is, it can expand to something like "UTF-8")
	#define GCC_WIDE_CHARSET	__GNUC_WIDE_EXECUTION_CHARSET_NAME
	#else
	/// @brief	Expands to a narrow string literal of the name of
	/// the narrow and wide compile-time execution character set used
	/// It directly reflects the name passed to the option -fwide-exec-charset,
	/// or the defaults documented for those options
	/// (that is, it can expand to something like "UTF-8")
	#define GCC_WIDE_CHARSET	""
	#endif
}
