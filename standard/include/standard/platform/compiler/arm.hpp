﻿#pragma once


namespace Std::inline Platform::inline Compiler
{
	// todo: u8 string literals?

	#if defined(__CC_ARM)
	/// @brief	Defined as boolean literal 'true' if current compiler is Arm
	#define COMPILER_IS_ARM	true
	#else
	/// @brief	Defined as boolean literal 'true' if current compiler is Arm
	#define COMPILER_IS_ARM	false
	#endif

	#if COMPILER_IS_ARM
	#undef COMPILER_NAME
	#undef COMPILER_FULL_NAME
	#undef COMPILER_COUNTER
	#undef COMPILER_TIMESTAMP

	/// @brief	Defined as string literal that
	/// encodes short name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------------|---------
	/// Unknown	| 
	/// ARM		| Arm
	/// Clang		| Clang
	/// GNU CC		| Gcc
	/// LLVM		| Llvm
	/// Microsoft	| Msvc
	/// </pre>
	#define COMPILER_NAME	"Arm"
	/// @brief	Defined as string literal that
	/// encodes full name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------------|----------------------------
	/// Unknown	| 
	/// ARM		| Arm C/C++
	/// Clang		| Clang C Language Family
	/// GNU CC		| Gnu Compiler Collection
	/// LLVM		| Llvm
	/// Microsoft	| Microsoft Visual C++
	/// </pre>
	#define COMPILER_FULL_NAME	"Arm C/C++"
	#endif

	// todo: add Arm-specific macros
}
