﻿#pragma once


namespace Std::inline Platform::inline Compiler
{
	// todo: u8 string literals?

	#if defined(__clang__)
	/// @brief	Defined as boolean literal 'true' if current compiler is Clang
	#define COMPILER_IS_CLANG	true
	#else
	/// @brief	Defined as boolean literal 'true' if current compiler is Clang
	#define COMPILER_IS_CLANG	false
	#endif

	#if COMPILER_IS_CLANG
	#undef COMPILER_NAME
	#undef COMPILER_FULL_NAME
	#undef COUNTER
	#undef TIMESTAMP

	/// @brief	Defined as string literal that
	/// encodes short name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------|---------
	/// Unknown	| 
	/// ARM	| Arm
	/// Clang	| Clang
	/// GNU CC	| Gcc
	/// Microsoft	| Msvc
	/// </pre>
	#define COMPILER_NAME	"Clang"

	/// @brief	Defined as string literal that
	/// encodes full name of the compiler
	/// <pre>
	/// Compiler	| Value
	/// ---------|----------------------------
	/// Unknown	| 
	/// ARM	| Arm C/C++
	/// Clang	| Clang C Language Family
	/// GNU CC	| Gnu Compiler Collection
	/// Microsoft	| Microsoft Visual C++
	/// </pre>
	#define COMPILER_FULL_NAME	"Clang C Language Family"

	/// @brief	Defined as an integer literal that starts at 0
	/// Incremented each time the macro is expanded
	#define COUNTER	__COUNTER__

	/// @brief	Expands to a string literal that describes the date
	/// and time of the last modification of the current source file
	/// The string constant contains abbreviated day of the week, month,
	/// day of the month, time in hh:mm:ss form, year and looks like "Sun Sep 16 01:03:52 1973"
	/// If the day of the month is less than 10, it is padded with a space on the left
	#define TIMESTAMP	__TIMESTAMP__
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as integer literal that represents
	/// the depth of nesting in include files starting from 0
	/// The value of this macro is incremented on every ‘#include’
	/// directive and decremented at the end of every included file
	#define CLANG_INCLUDE_LEVEL	__INCLUDE_LEVEL__
	#else
	/// @brief	Defined as integer literal that represents
	/// the depth of nesting in include files starting from 0
	/// The value of this macro is incremented on every ‘#include’
	/// directive and decremented at the end of every included file
	#define CLANG_INCLUDE_LEVEL	-1
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as string literal that
	/// encodes the name of the main file
	#define CLANG_MAIN_FILE	__BASE_FILE__
	#else
	/// @brief	Defined as string literal that
	/// encodes the name of the main file
	#define CLANG_MAIN_FILE	""
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as string literal that
	/// encodes the basename of the current file
	/// For example, processing 'project/include/header.h'
	/// would set this macro to 'header.h'
	#define CLANG_FILE_NAME	__FILE_NAME__
	#else
	/// @brief	Defined as string literal that
	/// encodes the basename of the current file
	/// For example, processing 'project/include/header.h'
	/// would set this macro to 'header.h'
	#define CLANG_FILE_NAME	""
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as integer literal that encodes the 
	/// major marketing version number of Clang (e.g., the 2 in 2.0.1)
	/// @note	Marketing version numbers should not be used to check for language features,
	/// as different vendors use different numbering schemes. Instead, use the Feature Checking Macros
	#define CLANG_MAJOR_VERSION	__clang_major__
	#else
	/// @brief	Defined as integer literal that encodes the 
	/// major marketing version number of Clang (e.g., the 2 in 2.0.1)
	/// @note	Marketing version numbers should not be used to check for language features,
	/// as different vendors use different numbering schemes. Instead, use the Feature Checking Macros
	#define CLANG_MAJOR_VERSION	0
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as integer literal that encodes the 
	/// minor version number of Clang (e.g., the 0 in 2.0.1)
	/// @note	Marketing version numbers should not be used to check for language features,
	/// as different vendors use different numbering schemes. Instead, use the Feature Checking Macros
	#define CLANG_MINOR_VERSION	__clang_minor__
	#else
	/// @brief	Defined as integer literal that encodes the 
	/// minor version number of Clang (e.g., the 0 in 2.0.1)
	/// @note	Marketing version numbers should not be used to check for language features,
	/// as different vendors use different numbering schemes. Instead, use the Feature Checking Macros
	#define CLANG_MINOR_VERSION	0
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as integer literal that encodes the 
	/// marketing patch level of Clang (e.g., the 1 in 2.0.1)
	/// @note	Marketing version numbers should not be used to check for language features,
	/// as different vendors use different numbering schemes. Instead, use the Feature Checking Macros
	#define CLANG_PATCH_VERSION	__clang_patchlevel__
	#else
	/// @brief	Defined as integer literal that encodes the 
	/// marketing patch level of Clang (e.g., the 1 in 2.0.1)
	/// @note	Marketing version numbers should not be used to check for language features,
	/// as different vendors use different numbering schemes. Instead, use the Feature Checking Macros
	#define CLANG_PATCH_VERSION	0
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as string literal that captures the Clang marketing version,
	/// including the Subversion tag or revision number, e.g., “1.5 (trunk 102332)”
	#define CLANG_VERSION	__clang_version__
	#else
	/// @brief	Defined as string literal that captures the Clang marketing version,
	/// including the Subversion tag or revision number, e.g., “1.5 (trunk 102332)”
	#define CLANG_VERSION	""
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as string literal that represents the current encoding
	/// of string literals, e.g., "hello". This macro typically expands to “UTF-8”
	/// @note	Can be changed by the -fexec-charset="Encoding-Name" option
	#define CLANG_STRING_ENCODING	__clang_literal_encoding__
	#else
	/// @brief	Defined as string literal that represents the current encoding
	/// of string literals, e.g., "hello". This macro typically expands to “UTF-8”
	/// @note	Can be changed by the -fexec-charset="Encoding-Name" option
	#define CLANG_STRING_ENCODING	""
	#endif

	#if COMPILER_IS_CLANG
	/// @brief	Defined as string literal that represents the current encoding
	/// of wide string literals, e.g., L"hello". This macro typically expands to “UTF-16” or “UTF-32”
	/// @note	Can be changed by the -fwide-exec-charset="Encoding-Name" option
	#define CLANG_WIDE_STRING_ENCODING	__clang_wide_literal_encoding__
	#else
	/// @brief	Defined as string literal that represents the current encoding
	/// of wide string literals, e.g., L"hello". This macro typically expands to “UTF-16” or “UTF-32”
	/// @note	Can be changed by the -fwide-exec-charset="Encoding-Name" option
	#define CLANG_WIDE_STRING_ENCODING	""
	#endif

	// todo: add more clang-specific macros
}
