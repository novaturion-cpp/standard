﻿#pragma once
#include "_internal/utility/type-traits.hpp"


namespace Std::inline Platform::inline Type
{
	/// @brief	Minimal addressable unit of memory
	enum class byte : unsigned char {};


	template<class T>
	[[nodiscard]]
	constexpr byte operator<<(
		const byte value,
		const T shift
	) noexcept
	{
		return static_cast<byte>(static_cast<unsigned char>(static_cast<unsigned int>(value) << shift));
	}

	template<class T>
	[[nodiscard]]
	constexpr byte operator>>(
		const byte value,
		const T shift
	) noexcept
	{
		return static_cast<byte>(static_cast<unsigned char>(static_cast<unsigned int>(value) >> shift));
	}

	[[nodiscard]]
	constexpr byte operator|(
		const byte left,
		const byte right
	) noexcept
	{
		return static_cast<byte>(
			static_cast<unsigned char>(static_cast<unsigned int>(left) | static_cast<unsigned int>(right)));
	}

	[[nodiscard]]
	constexpr byte operator&(
		const byte left,
		const byte right
	) noexcept
	{
		return static_cast<byte>(
			static_cast<unsigned char>(static_cast<unsigned int>(left) & static_cast<unsigned int>(right)));
	}

	[[nodiscard]]
	constexpr byte operator^(
		const byte left,
		const byte right
	) noexcept
	{
		return static_cast<byte>(
			static_cast<unsigned char>(static_cast<unsigned int>(left) ^ static_cast<unsigned int>(right)));
	}

	[[nodiscard]]
	constexpr byte operator~(
		const byte value
	) noexcept
	{
		return static_cast<byte>(static_cast<unsigned char>(~static_cast<unsigned int>(value)));
	}

	template<class T>
	constexpr byte& operator<<=(
		byte& value,
		const T shift
	) noexcept
	{
		return value = value << shift;
	}

	template<class T>
	constexpr byte& operator>>=(
		byte& value,
		const T shift
	) noexcept
	{
		return value = value >> shift;
	}

	constexpr byte& operator|=(
		byte& left,
		const byte right
	) noexcept
	{
		return left = left | right;
	}

	constexpr byte& operator&=(
		byte& left,
		const byte right
	) noexcept
	{
		return left = left & right;
	}

	constexpr byte& operator^=(
		byte& left,
		const byte right
	) noexcept
	{
		return left = left ^ right;
	}

	constexpr byte operator""_b(
		const unsigned long long value
	)
	{
		return byte {static_cast<unsigned char>(value)};
	}

	template<class T>
		requires _Internal::IsInteger<T>
	[[nodiscard]]
	constexpr T ToInteger(
		const byte value
	) noexcept
	{
		return static_cast<T>(value);
	}
}


using Std::byte;

using Std::operator""_b;
