#pragma once


namespace Std::inline Platform::inline Type
{
	using NullType = decltype(nullptr);

	static inline constexpr auto null = nullptr;
}


using Std::null;
using Std::NullType;
