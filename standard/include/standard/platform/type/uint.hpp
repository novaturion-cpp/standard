#pragma once
#include "standard/platform/architecture.hpp"


namespace Std::inline Platform::inline Type
{
	/// @brief	Unsigned integer type with width of exactly 8 bits
	using uint8 = unsigned char;
	/// @brief	Unsigned integer type with width of exactly 16 bits
	using uint16 = unsigned short;
	/// @brief	Unsigned integer type with width of exactly 32 bits
	using uint32 = unsigned int;
	/// @brief	Unsigned integer type with width of exactly 64 bits
	using uint64 = unsigned long long;

	/// @brief	Smallest unsigned integer type with width of at least 8 bits
	using uint8Least = unsigned char;
	/// @brief	Smallest unsigned integer type with width of at least 16 bits
	using uint16Least = unsigned short;
	/// @brief	Smallest unsigned integer type with width of at least 32 bits
	using uint32Least = unsigned int;
	/// @brief	Smallest unsigned integer type with width of at least 64 bits
	using uint64Least = unsigned long long;

	/// @brief	Fastest unsigned integer type with width of at least 8 bits
	using uint8Fast = unsigned char;
	/// @brief	Fastest unsigned integer type with width of at least 16 bits
	using uint16Fast = unsigned int;
	/// @brief	Fastest unsigned integer type with width of at least 32 bits
	using uint32Fast = unsigned int;
	/// @brief	Fastest unsigned integer type with width of at least 64 bits
	using uint64Fast = unsigned long long;


	#if ARCH_IS_X64

	/// @brief	Maximum-width unsigned integer type
	using uintSize = unsigned long long;

	/// @brief	Unsigned integer type capable of holding a pointer
	using uintPointer = uint64;

	#else

	/// @brief	Maximum-width unsigned integer type
	using uintSize = unsigned int;

	/// @brief	Unsigned integer type capable of holding a pointer
	using uintPointer = uint32;

	#endif


	constexpr uint8 operator""_ui8(
		const unsigned long long value
	)
	{
		return static_cast<uint8>(value);
	}

	constexpr uint16 operator""_ui16(
		const unsigned long long value
	)
	{
		return static_cast<uint16>(value);
	}

	constexpr uint32 operator""_ui32(
		const unsigned long long value
	)
	{
		return static_cast<uint32>(value);
	}

	constexpr uint64 operator""_ui64(
		const unsigned long long value
	)
	{
		return static_cast<uint64>(value);
	}

	constexpr uintSize operator""_uis(
		const unsigned long long value
	)
	{
		return static_cast<uintSize>(value);
	}

	constexpr uintPointer operator""_uip(
		const unsigned long long value
	)
	{
		return static_cast<uintPointer>(value);
	}
}


using Std::uint8;
using Std::uint16;
using Std::uint32;
using Std::uint64;

using Std::uint8Least;
using Std::uint16Least;
using Std::uint32Least;
using Std::uint64Least;

using Std::uint8Fast;
using Std::uint16Fast;
using Std::uint32Fast;
using Std::uint64Fast;

using Std::uintSize;

using Std::uintPointer;

using Std::operator""_ui8;
using Std::operator""_ui16;
using Std::operator""_ui32;
using Std::operator""_ui64;
using Std::operator""_uis;
using Std::operator""_uip;
