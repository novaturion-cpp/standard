#pragma once


namespace Std::inline Platform::inline Type
{
	/// @brief	Single precision floating point type. Matches IEEE-754 binary32 format
	using float32 = float;
	/// @brief	Double precision floating point type. Matches IEEE-754 binary64 format
	using float64 = double;
	/// @brief	Quadruple precision floating point type. Matches IEEE-754 binary128 format if supported,
	/// otherwise matches IEEE-754 binary64-extended format if supported,
	/// otherwise matches IEEE-754 binary64 format
	using float128 = long double;


	constexpr float32 operator ""_f32(
		const long double value
	)
	{
		return static_cast<float32>(value);
	}

	constexpr float64 operator ""_f64(
		const long double value
	)
	{
		return static_cast<float64>(value);
	}

	constexpr float128 operator ""_f128(
		const long double value
	)
	{
		return static_cast<float128>(value);
	}
}


using Std::float32;
using Std::float64;
using Std::float128;

using Std::operator ""_f32;
using Std::operator ""_f64;
using Std::operator ""_f128;
