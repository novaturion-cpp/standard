﻿#pragma once
#include "standard/platform/architecture.hpp"


namespace Std::inline Platform::inline Type
{
	/// @brief	Signed integer type with width of exactly 8 bits
	using int8 = signed char;
	/// @brief	Signed integer type with width of exactly 16 bits
	using int16 = signed short;
	/// @brief	Signed integer type with width of exactly 32 bits
	using int32 = signed int;
	/// @brief	Signed integer type with width of exactly 64 bits
	using int64 = signed long long;

	/// @brief	Smallest signed integer type with width of at least 8 bits
	using int8Least = signed char;
	/// @brief	Smallest signed integer type with width of at least 16 bits
	using int16Least = signed short;
	/// @brief	Smallest signed integer type with width of at least 32 bits
	using int32Least = signed int;
	/// @brief	Smallest signed integer type with width of at least 64 bits
	using int64Least = signed long long;

	/// @brief	Fastest signed integer type with width of at least 8 bits
	using int8Fast = signed char;
	/// @brief	Fastest signed integer type with width of at least 16 bits
	using int16Fast = signed int;
	/// @brief	Fastest signed integer type with width of at least 32 bits
	using int32Fast = signed int;
	/// @brief	Fastest signed integer type with width of at least 64 bits
	using int64Fast = signed long long;


	#if ARCH_IS_X64

	/// @brief	Maximum-width signed integer type
	using intSize = signed long long;

	/// @brief	Signed integer type capable of holding a pointer
	using intPointer = int64;

	#else

	/// @brief	Maximum-width signed integer type
	using intSize = signed int;

	/// @brief	Signed integer type capable of holding a pointer
	using intPointer = int32;

	#endif


	constexpr int8 operator""_i8(
		const unsigned long long value
	)
	{
		return static_cast<int8>(value);
	}

	constexpr int16 operator""_i16(
		const unsigned long long value
	)
	{
		return static_cast<int16>(value);
	}

	constexpr int32 operator""_i32(
		const unsigned long long value
	)
	{
		return static_cast<int32>(value);
	}

	constexpr int64 operator""_i64(
		const unsigned long long value
	)
	{
		return static_cast<int64>(value);
	}

	constexpr intSize operator""_is(
		const unsigned long long value
	)
	{
		return static_cast<intSize>(value);
	}

	constexpr intPointer operator""_ip(
		const unsigned long long value
	)
	{
		return static_cast<intPointer>(value);
	}
}


using Std::int8;
using Std::int16;
using Std::int32;
using Std::int64;

using Std::int8Least;
using Std::int16Least;
using Std::int32Least;
using Std::int64Least;

using Std::int8Fast;
using Std::int16Fast;
using Std::int32Fast;
using Std::int64Fast;

using Std::intSize;

using Std::intPointer;

using Std::operator""_i8;
using Std::operator""_i16;
using Std::operator""_i32;
using Std::operator""_i64;
using Std::operator""_is;
using Std::operator""_ip;
