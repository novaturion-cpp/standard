﻿#pragma once


namespace Std::inline Platform::inline Type
{
	/// @brief	UTF-8 character representation
	using char8 = char8_t;
	/// @brief	UTF-16 character representation
	using char16 = char16_t;
	/// @brief	UTF-32 character representation
	using char32 = char32_t;


	constexpr char8 operator ""_ch8(
		const char value
	)
	{
		return static_cast<char8>(value);
	}

	constexpr char16 operator ""_ch16(
		const char value
	)
	{
		return static_cast<char16>(value);
	}

	constexpr char32 operator ""_ch32(
		const char value
	)
	{
		return static_cast<char32>(value);
	}

	constexpr char32 operator ""_ch32(
		const char16_t value
	)
	{
		return static_cast<char32>(value);
	}
}


using Std::char8;
using Std::char16;
using Std::char32;

using Std::operator ""_ch8;
using Std::operator ""_ch16;
using Std::operator ""_ch32;
