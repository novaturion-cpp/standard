﻿#pragma once
#include "type/byte.hpp"
#include "type/char.hpp"
#include "type/float.hpp"
#include "type/int.hpp"
#include "type/null.hpp"
#include "type/uint.hpp"
