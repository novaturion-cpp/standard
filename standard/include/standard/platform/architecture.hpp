﻿#pragma once


namespace Std::inline Platform::Arch
{
	#if defined(__arm__) || defined(_M_ARM)

	#define ARCH_NAME	u8"Arm"

	#define ARCH_IS_ARM true

	#if defined(__thumb__) || defined(_M_ARMT);
	#define ARCH_IS_ARM_THUMB true
	#else
	#define ARCH_IS_ARM_THUMB false
	#endif

	#else

	#define ARCH_IS_ARM false
	#define ARCH_IS_ARM_THUMB false

	#endif

	#if defined(__aarch64__) || defined(_M_ARM64)

	#define ARCH_NAME	u8"Arm64"

	#define ARCH_IS_ARM64 true

	#else

	#define ARCH_IS_ARM64 false

	#endif


	#if defined(_M_X64) || defined(_M_AMD64) || defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)

	#define ARCH_NAME	u8"x64"

	#define ARCH_IS_X64 true

	#else

	#define ARCH_IS_X64 false

	#endif

	#if defined(i386) || defined(__i386) || defined(__i386__) || defined(_M_X86) || defined(_M_IX86) || defined(_X86_)

	#define ARCH_NAME	u8"x86"

	#define ARCH_IS_X86 true

	#else

	#define ARCH_IS_X86 false

	#endif
}
