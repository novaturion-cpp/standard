﻿#pragma once
#include "x86/3d-now-prefetch.hpp"
#include "x86/abm.hpp"
#include "x86/adx.hpp"
#include "x86/aes.hpp"
#include "x86/avx.hpp"
#include "x86/avx2.hpp"
#include "x86/bmi.hpp"
#include "x86/bmi2.hpp"
#include "x86/clflush.hpp"
#include "x86/clmul.hpp"
#include "x86/comparison-type.hpp"
#include "x86/cpuid-leaf.hpp"
#include "x86/cpuid.hpp"
#include "x86/csr.hpp"
#include "x86/extension-msvc.hpp"
#include "x86/extension.hpp"
#include "x86/f16c.hpp"
#include "x86/fma3.hpp"
#include "x86/fs-gs-base.hpp"
#include "x86/hle.hpp"
#include "x86/inv-pcid.hpp"
#include "x86/monitor.hpp"
#include "x86/movbe.hpp"
#include "x86/rdrand.hpp"
#include "x86/rdseed.hpp"
#include "x86/rdtsc.hpp"
#include "x86/rounding-type.hpp"
#include "x86/rtm.hpp"
#include "x86/shuffle.hpp"
#include "x86/smap.hpp"
#include "x86/sse.hpp"
#include "x86/sse2.hpp"
#include "x86/sse3.hpp"
#include "x86/sse41.hpp"
#include "x86/sse42.hpp"
#include "x86/ssse3.hpp"
#include "x86/tbm.hpp"
#include "x86/xsave.hpp"
#include "x86/xtest.hpp"

#if ARCH_IS_X86
#include "x86/mmx.hpp"
#endif
