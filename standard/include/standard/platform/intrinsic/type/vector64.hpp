﻿#pragma once
#include "standard/platform/type.hpp"
#include "standard/utility/type-traits.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	struct alignas(__m64) Vector64 final
	{
	public:
		constexpr Vector64() = default;

	public:
		explicit constexpr Vector64(
			const float32 value
		)
		{
			M64 = {.m64_f32 = {value, value}};
		}

		explicit constexpr Vector64(
			const float64 value
		)
		{
			F64 = value;
		}

	public:
		explicit constexpr Vector64(
			const int32 value
		)
		{
			M64 = {.m64_i32 = {value, value}};
		}

		explicit constexpr Vector64(
			const int64 value
		) noexcept
		{
			M64 = {.m64_i64 = value};
		}

	public:
		explicit constexpr Vector64(
			const uint32 value
		)
		{
			M64 = {.m64_u32 = {value, value}};
		}

		explicit constexpr Vector64(
			const uint64 value
		) noexcept
		{
			M64 = {.m64_u64 = value};
		}

	public:
		template<class TValue, uintSize VElements>
			requires IsNumericValue<TValue> && (sizeof(TValue) * VElements == sizeof(__m128))
		explicit constexpr Vector64(
			const TValue (&array)[VElements]
		)
		{
			const auto pointer = static_cast<const uintPointer*>(static_cast<const void*>(&array[0]));
			#if ARCH_IS_X64
			M64 = {.m64_u64 = pointer[0]};
			#else
			M64 = {.m64_u32 = {pointer[0], pointer[1]}};
			#endif
		}

	public:
		constexpr Vector64(
			const __m64& value
		) noexcept:
			M64(value) {}

	public:
		constexpr Vector64(
			const __m64&& value
		) noexcept:
			M64(value) {}

	public:
		template<class TNumeric>
			requires !IsAnyValue<TNumeric, int8, uint8, int64, uint64> && IsNumericValue<TNumeric>
		constexpr TNumeric (& AsT() noexcept)[sizeof(__m64) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int16>)
			{
				return AsInt16();
			}
			else if constexpr (IsSameValue<TNumeric, int32>)
			{
				return AsInt32();
			}
			else if constexpr (IsSameValue<TNumeric, int64>)
			{
				return AsInt64();
			}
			else if constexpr (IsSameValue<TNumeric, uint16>)
			{
				return AsUint16();
			}
			else if constexpr (IsSameValue<TNumeric, uint32>)
			{
				return AsUint32();
			}
			else if constexpr (IsSameValue<TNumeric, uint64>)
			{
				return AsUint64();
			}
			else
			{
				return AsFloat32();
			}
		}

		template<class TNumeric>
			requires IsAnyValue<TNumeric, int8, uint8>
		constexpr char (& AsT() noexcept)[sizeof(__m64) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int8>)
			{
				return AsInt8();
			}
			else
			{
				return AsUint8();
			}
		}

		template<class TNumeric>
			requires IsAnyValue<TNumeric, int64, uint64>
		constexpr TNumeric& AsT() noexcept
		{
			if constexpr (IsSameValue<TNumeric, int64>)
			{
				return AsInt64();
			}
			else
			{
				return AsUint64();
			}
		}

	public:
		template<class TNumeric>
			requires !IsAnyValue<TNumeric, int8, uint8, int64, uint64> && IsNumericValue<TNumeric>
		[[nodiscard]]
		constexpr const TNumeric (& AsT() const noexcept)[sizeof(__m64) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int16>)
			{
				return AsInt16();
			}
			else if constexpr (IsSameValue<TNumeric, int32>)
			{
				return AsInt32();
			}
			else if constexpr (IsSameValue<TNumeric, int64>)
			{
				return AsInt64();
			}
			else if constexpr (IsSameValue<TNumeric, uint16>)
			{
				return AsUint16();
			}
			else if constexpr (IsSameValue<TNumeric, uint32>)
			{
				return AsUint32();
			}
			else if constexpr (IsSameValue<TNumeric, uint64>)
			{
				return AsUint64();
			}
			else
			{
				return AsFloat32();
			}
		}

		template<class TNumeric>
			requires IsAnyValue<TNumeric, int8, uint8>
		[[nodiscard]]
		constexpr const char (& AsT() const noexcept)[sizeof(__m64) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int8>)
			{
				return AsInt8();
			}
			else
			{
				return AsUint8();
			}
		}

		template<class TNumeric>
			requires IsAnyValue<TNumeric, int64, uint64>
		[[nodiscard]]
		constexpr const TNumeric& AsT() const noexcept
		{
			if constexpr (IsSameValue<TNumeric, int64>)
			{
				return AsInt64();
			}
			else
			{
				return AsUint64();
			}
		}

	public:
		constexpr char (& AsInt8() noexcept)[8]
		{
			return M64.m64_i8;
		}

		constexpr int16 (& AsInt16() noexcept)[4]
		{
			return M64.m64_i16;
		}

		constexpr int32 (& AsInt32() noexcept)[2]
		{
			return M64.m64_i32;
		}

		constexpr int64& AsInt64() noexcept
		{
			return M64.m64_i64;
		}

		constexpr uint8 (& AsUint8() noexcept)[8]
		{
			return M64.m64_u8;
		}

		constexpr uint16 (& AsUint16() noexcept)[4]
		{
			return M64.m64_u16;
		}

		constexpr uint32 (& AsUint32() noexcept)[2]
		{
			return M64.m64_u32;
		}

		constexpr uint64& AsUint64() noexcept
		{
			return M64.m64_u64;
		}

		constexpr float32 (& AsFloat32() noexcept)[2]
		{
			return M64.m64_f32;
		}

		constexpr float64& AsFloat64() noexcept
		{
			return F64;
		}

	public:
		[[nodiscard]]
		constexpr const char (& AsInt8() const noexcept)[8]
		{
			return M64.m64_i8;
		}

		[[nodiscard]]
		constexpr const int16 (& AsInt16() const noexcept)[4]
		{
			return M64.m64_i16;
		}

		[[nodiscard]]
		constexpr const int32 (& AsInt32() const noexcept)[2]
		{
			return M64.m64_i32;
		}

		[[nodiscard]]
		constexpr int64 AsInt64() const noexcept
		{
			return M64.m64_i64;
		}

		[[nodiscard]]
		constexpr const uint8 (& AsUint8() const noexcept)[8]
		{
			return M64.m64_u8;
		}

		[[nodiscard]]
		constexpr const uint16 (& AsUint16() const noexcept)[4]
		{
			return M64.m64_u16;
		}

		[[nodiscard]]
		constexpr const uint32 (& AsUint32() const noexcept)[2]
		{
			return M64.m64_u32;
		}

		[[nodiscard]]
		constexpr uint64 AsUint64() const noexcept
		{
			return M64.m64_u64;
		}

		[[nodiscard]]
		constexpr const float32 (& AsFloat32() const noexcept)[2]
		{
			return M64.m64_f32;
		}

		[[nodiscard]]
		constexpr float64 AsFloat64() const noexcept
		{
			return F64;
		}

	public:
		static constexpr Vector64 GetMaskX()
		{
			return __m64 {.m64_u32 = {0xffffffff, 0x00000000}};
		}

		static constexpr Vector64 GetMaskY()
		{
			return __m64 {.m64_u32 = {0x00000000, 0xffffffff}};
		}

		static constexpr Vector64 GetMaskXY()
		{
			return __m64 {.m64_u32 = {0xffffffff, 0xffffffff}};
		}

	public:
		static constexpr Vector64 GetSignMaskX()
		{
			return __m64 {.m64_u32 = {0x80000000, 0x00000000}};
		}

		static constexpr Vector64 GetSignMaskY()
		{
			return __m64 {.m64_u32 = {0x00000000, 0x80000000}};
		}

		static constexpr Vector64 GetSignMaskXY()
		{
			return __m64 {.m64_u32 = {0x80000000, 0x80000000}};
		}

		// todo: copy and swap?
	public:
		constexpr Vector64& operator=(
			const __m64& value
		) noexcept
		{
			M64 = value;
			return *this;
		}

		//todo: actual move
	public:
		constexpr Vector64& operator=(
			__m64&& value
		) noexcept
		{
			M64 = value;
			return *this;
		}

	public:
		constexpr operator __m64&() noexcept
		{
			return M64;
		}

		constexpr operator __m64*() noexcept
		{
			return &M64;
		}

	public:
		constexpr operator const __m64&() const noexcept
		{
			return M64;
		}

		constexpr operator const __m64*() const noexcept
		{
			return &M64;
		}

	private:
		union
		{
			__m64 M64;
			float64 F64;
		};
	};
}
