#pragma once
#include "standard/platform/type.hpp"
#include "standard/utility/type-traits.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	struct alignas(__m128) Vector128 final
	{
	public:
		constexpr Vector128() = default;

	public:
		explicit constexpr Vector128(
			const float32 value
		)
		{
			M128 = {.m128_f32 = {value, value, value, value}};
		}

		explicit constexpr Vector128(
			const float64 value
		)
		{
			M128Double = {.m128d_f64 = {value, value}};
		}

	public:
		explicit constexpr Vector128(
			const int32 value
		)
		{
			M128Int = {.m128i_i32 = {value, value, value, value}};
		}

		explicit constexpr Vector128(
			const int64 value
		)
		{
			M128Int = {.m128i_i64 = {value, value}};
		}

	public:
		explicit constexpr Vector128(
			const uint32 value
		)
		{
			M128Int = {.m128i_u32 = {value, value, value, value}};
		}

		explicit constexpr Vector128(
			const uint64 value
		)
		{
			M128Int = {.m128i_u64 = {value, value}};
		}

	public:
		template<class TValue, uintSize VElements>
			requires IsNumericValue<TValue> && (sizeof(TValue) * VElements == sizeof(__m128))
		explicit constexpr Vector128(
			const TValue (&array)[VElements]
		)
		{
			const auto pointer = static_cast<const uintPointer*>(static_cast<const void*>(&array[0]));
			#if ARCH_IS_X64
			M128 = {.m128_u64 = {pointer[0], pointer[1]}};
			#else
			M128 = {.m128_u32 = {pointer[0], pointer[1], pointer[2], pointer[3]}};
			#endif
		}

	public:
		constexpr Vector128(
			const __m128& value
		) noexcept:
			M128 {value} {}

		constexpr Vector128(
			const __m128i& value
		) noexcept:
			M128Int {value} {}

		constexpr Vector128(
			const __m128d& value
		) noexcept:
			M128Double {value} {}

	public:
		constexpr Vector128(
			const __m128&& value
		) noexcept:
			M128 {value} {}

		constexpr Vector128(
			const __m128i&& value
		) noexcept:
			M128Int {value} {}

		constexpr Vector128(
			const __m128d&& value
		) noexcept:
			M128Double {value} {}

	public:
		template<class TNumeric>
			requires !IsAnyValue<TNumeric, int8, uint8> && IsNumericValue<TNumeric>
		constexpr TNumeric (& AsT() noexcept)[sizeof(__m128) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int16>)
			{
				return AsInt16();
			}
			else if constexpr (IsSameValue<TNumeric, int32>)
			{
				return AsInt32();
			}
			else if constexpr (IsSameValue<TNumeric, int64>)
			{
				return AsInt64();
			}
			else if constexpr (IsSameValue<TNumeric, uint16>)
			{
				return AsUint16();
			}
			else if constexpr (IsSameValue<TNumeric, uint32>)
			{
				return AsUint32();
			}
			else if constexpr (IsSameValue<TNumeric, uint64>)
			{
				return AsUint64();
			}
			else if constexpr (IsSameValue<TNumeric, float32>)
			{
				return AsFloat32();
			}
			else
			{
				return AsFloat64();
			}
		}

		template<class TNumeric>
			requires IsAnyValue<TNumeric, int8, uint8>
		constexpr If<IsSignedValue<TNumeric>, char, uint8>
		(& AsT() noexcept)[sizeof(__m128) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int8>)
			{
				return AsInt8();
			}
			else
			{
				return AsUint8();
			}
		}

	public:
		template<class TNumeric>
			requires !IsAnyValue<TNumeric, int8, uint8> && IsNumericValue<TNumeric>
		[[nodiscard]]
		constexpr const TNumeric (& AsT() const noexcept)[sizeof(__m128) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int16>)
			{
				return AsInt16();
			}
			else if constexpr (IsSameValue<TNumeric, int32>)
			{
				return AsInt32();
			}
			else if constexpr (IsSameValue<TNumeric, int64>)
			{
				return AsInt64();
			}
			else if constexpr (IsSameValue<TNumeric, uint16>)
			{
				return AsUint16();
			}
			else if constexpr (IsSameValue<TNumeric, uint32>)
			{
				return AsUint32();
			}
			else if constexpr (IsSameValue<TNumeric, uint64>)
			{
				return AsUint64();
			}
			else if constexpr (IsSameValue<TNumeric, float32>)
			{
				return AsFloat32();
			}
			else
			{
				return AsFloat64();
			}
		}

		template<class TNumeric>
			requires IsAnyValue<TNumeric, int8, uint8>
		[[nodiscard]]
		constexpr const If<IsSignedValue<TNumeric>, char, uint8>
		(& AsT() const noexcept)[sizeof(__m128) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int8>)
			{
				return AsInt8();
			}
			else
			{
				return AsUint8();
			}
		}

	public:
		constexpr char (& AsInt8() noexcept)[16]
		{
			return M128Int.m128i_i8;
		}

		constexpr int16 (& AsInt16() noexcept)[8]
		{
			return M128Int.m128i_i16;
		}

		constexpr int32 (& AsInt32() noexcept)[4]
		{
			return M128Int.m128i_i32;
		}

		constexpr int64 (& AsInt64() noexcept)[2]
		{
			return M128Int.m128i_i64;
		}

		constexpr uint8 (& AsUint8() noexcept)[16]
		{
			return M128Int.m128i_u8;
		}

		constexpr uint16 (& AsUint16() noexcept)[8]
		{
			return M128Int.m128i_u16;
		}

		constexpr uint32 (& AsUint32() noexcept)[4]
		{
			return M128Int.m128i_u32;
		}

		constexpr uint64 (& AsUint64() noexcept)[2]
		{
			return M128Int.m128i_u64;
		}

		constexpr float32 (& AsFloat32() noexcept)[4]
		{
			return M128.m128_f32;
		}

		constexpr float64 (& AsFloat64() noexcept)[2]
		{
			return M128Double.m128d_f64;
		}

	public:
		[[nodiscard]]
		constexpr const char (& AsInt8() const noexcept)[16]
		{
			return M128Int.m128i_i8;
		}

		[[nodiscard]]
		constexpr const int16 (& AsInt16() const noexcept)[8]
		{
			return M128Int.m128i_i16;
		}

		[[nodiscard]]
		constexpr const int32 (& AsInt32() const noexcept)[4]
		{
			return M128Int.m128i_i32;
		}

		[[nodiscard]]
		constexpr const int64 (& AsInt64() const noexcept)[2]
		{
			return M128Int.m128i_i64;
		}

		[[nodiscard]]
		constexpr const uint8 (& AsUint8() const noexcept)[16]
		{
			return M128Int.m128i_u8;
		}

		[[nodiscard]]
		constexpr const uint16 (& AsUint16() const noexcept)[8]
		{
			return M128Int.m128i_u16;
		}

		[[nodiscard]]
		constexpr const uint32 (& AsUint32() const noexcept)[4]
		{
			return M128Int.m128i_u32;
		}

		[[nodiscard]]
		constexpr const uint64 (& AsUint64() const noexcept)[2]
		{
			return M128Int.m128i_u64;
		}

		[[nodiscard]]
		constexpr const float32 (& AsFloat32() const noexcept)[4]
		{
			return M128.m128_f32;
		}

		[[nodiscard]]
		constexpr const float64 (& AsFloat64() const noexcept)[2]
		{
			return M128Double.m128d_f64;
		}

	public:
		static constexpr Vector128 GetMaskX()
		{
			return __m128i {.m128i_u32 = {0xffffffff, 0x00000000, 0x00000000, 0x00000000}};
		}

		static constexpr Vector128 GetMaskY()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0xffffffff, 0x00000000, 0x00000000}};
		}

		static constexpr Vector128 GetMaskZ()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x00000000, 0xffffffff, 0x00000000}};
		}

		static constexpr Vector128 GetMaskW()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x00000000, 0x00000000, 0xffffffff}};
		}

		static constexpr Vector128 GetMaskXY()
		{
			return __m128i {.m128i_u32 = {0xffffffff, 0xffffffff, 0x00000000, 0x00000000}};
		}

		static constexpr Vector128 GetMaskXZ()
		{
			return __m128i {.m128i_u32 = {0xffffffff, 0x00000000, 0xffffffff, 0x00000000}};
		}

		static constexpr Vector128 GetMaskXW()
		{
			return __m128i {.m128i_u32 = {0xffffffff, 0x00000000, 0x00000000, 0xffffffff}};
		}

		static constexpr Vector128 GetMaskYZ()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0xffffffff, 0xffffffff, 0x00000000}};
		}

		static constexpr Vector128 GetMaskYW()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0xffffffff, 0x00000000, 0xffffffff}};
		}

		static constexpr Vector128 GetMaskZW()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x00000000, 0xffffffff, 0xffffffff}};
		}

		static constexpr Vector128 GetMaskXYZ()
		{
			return __m128i {.m128i_u32 = {0xffffffff, 0xffffffff, 0xffffffff, 0x00000000}};
		}

		static constexpr Vector128 GetMaskXYW()
		{
			return __m128i {.m128i_u32 = {0xffffffff, 0xffffffff, 0x00000000, 0xffffffff}};
		}

		static constexpr Vector128 GetMaskXZW()
		{
			return __m128i {.m128i_u32 = {0xffffffff, 0x00000000, 0xffffffff, 0xffffffff}};
		}

		static constexpr Vector128 GetMaskYZW()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0xffffffff, 0xffffffff, 0xffffffff}};
		}

		static constexpr Vector128 GetMaskXYZW()
		{
			return __m128i {.m128i_u32 = {0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff}};
		}

	public:
		static constexpr Vector128 GetSignMaskX()
		{
			return __m128i {.m128i_u32 = {0x80000000, 0x00000000, 0x00000000, 0x00000000}};
		}

		static constexpr Vector128 GetSignMaskY()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x80000000, 0x00000000, 0x00000000}};
		}

		static constexpr Vector128 GetSignMaskZ()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x00000000, 0x80000000, 0x00000000}};
		}

		static constexpr Vector128 GetSignMaskW()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x00000000, 0x00000000, 0x80000000}};
		}

		static constexpr Vector128 GetSignMaskXY()
		{
			return __m128i {.m128i_u32 = {0x80000000, 0x80000000, 0x00000000, 0x00000000}};
		}

		static constexpr Vector128 GetSignMaskXZ()
		{
			return __m128i {.m128i_u32 = {0x80000000, 0x00000000, 0x80000000, 0x00000000}};
		}

		static constexpr Vector128 GetSignMaskXW()
		{
			return __m128i {.m128i_u32 = {0x80000000, 0x00000000, 0x00000000, 0x80000000}};
		}

		static constexpr Vector128 GetSignMaskYZ()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x80000000, 0x80000000, 0x00000000}};
		}

		static constexpr Vector128 GetSignMaskYW()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x80000000, 0x00000000, 0x80000000}};
		}

		static constexpr Vector128 GetSignMaskZW()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x00000000, 0x80000000, 0x80000000}};
		}

		static constexpr Vector128 GetSignMaskXYZ()
		{
			return __m128i {.m128i_u32 = {0x80000000, 0x80000000, 0x80000000, 0x00000000}};
		}

		static constexpr Vector128 GetSignMaskXYW()
		{
			return __m128i {.m128i_u32 = {0x80000000, 0x80000000, 0x00000000, 0x80000000}};
		}

		static constexpr Vector128 GetSignMaskXZW()
		{
			return __m128i {.m128i_u32 = {0x80000000, 0x00000000, 0x80000000, 0x80000000}};
		}

		static constexpr Vector128 GetSignMaskYZW()
		{
			return __m128i {.m128i_u32 = {0x00000000, 0x80000000, 0x80000000, 0x80000000}};
		}

		static constexpr Vector128 GetSignMaskXYZW()
		{
			return __m128i {.m128i_u32 = {0x80000000, 0x80000000, 0x80000000, 0x80000000}};
		}

		// todo: copy and swap?
	public:
		constexpr Vector128& operator=(
			const __m128& value
		) noexcept
		{
			M128 = value;
			return *this;
		}

		constexpr Vector128& operator=(
			const __m128i& value
		) noexcept
		{
			M128Int = value;
			return *this;
		}

		constexpr Vector128& operator=(
			const __m128d& value
		) noexcept
		{
			M128Double = value;
			return *this;
		}

		//todo: actual move
	public:
		constexpr Vector128& operator=(
			__m128&& value
		) noexcept
		{
			M128 = value;
			return *this;
		}

		constexpr Vector128& operator=(
			__m128i&& value
		) noexcept
		{
			M128Int = value;
			return *this;
		}

		constexpr Vector128& operator=(
			__m128d&& value
		) noexcept
		{
			M128Double = value;
			return *this;
		}

	public:
		constexpr operator __m128&() noexcept
		{
			return M128;
		}

		constexpr operator __m128i&() noexcept
		{
			return M128Int;
		}

		constexpr operator __m128d&() noexcept
		{
			return M128Double;
		}

		constexpr operator __m128*() noexcept
		{
			return &M128;
		}

		constexpr operator __m128i*() noexcept
		{
			return &M128Int;
		}

		constexpr operator __m128d*() noexcept
		{
			return &M128Double;
		}

	public:
		constexpr operator const __m128&() const noexcept
		{
			return M128;
		}

		constexpr operator const __m128i&() const noexcept
		{
			return M128Int;
		}

		constexpr operator const __m128d&() const noexcept
		{
			return M128Double;
		}

		constexpr operator const __m128*() const noexcept
		{
			return &M128;
		}

		constexpr operator const __m128i*() const noexcept
		{
			return &M128Int;
		}

		constexpr operator const __m128d*() const noexcept
		{
			return &M128Double;
		}

	private:
		union
		{
			__m128 M128;
			__m128i M128Int;
			__m128d M128Double;
		};
	};
}
