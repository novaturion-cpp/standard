#pragma once
#include "standard/platform/type.hpp"
#include "standard/utility/type-traits.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	struct alignas(__m256) Vector256 final
	{
	public:
		constexpr Vector256() = default;

	public:
		explicit constexpr Vector256(
			const float32 value
		)
		{
			M256 = {
				.m256_f32 = {value, value, value, value, value, value, value, value}
			};
		}

		explicit constexpr Vector256(
			const float64 value
		)
		{
			M256Double = {.m256d_f64 = {value, value, value, value}};
		}

	public:
		explicit constexpr Vector256(
			const int32 value
		)
		{
			M256Int = {
				.m256i_i32 = {value, value, value, value, value, value, value, value}
			};
		}

		explicit constexpr Vector256(
			const int64 value
		)
		{
			M256Int = {.m256i_i64 = {value, value, value, value}};
		}

	public:
		explicit constexpr Vector256(
			const uint32 value
		)
		{
			M256Int = {
				.m256i_u32 = {value, value, value, value, value, value, value, value}
			};
		}

		explicit constexpr Vector256(
			const uint64 value
		)
		{
			M256Int = {.m256i_u64 = {value, value, value, value}};
		}

	public:
		template<class TValue, uintSize VElements>
			requires IsNumericValue<TValue> && (sizeof(TValue) * VElements == sizeof(__m256))
		explicit constexpr Vector256(
			const TValue (&array)[VElements]
		)
		{
			const auto pointer = static_cast<const uintPointer*>(static_cast<const void*>(&array[0]));
			#if ARCH_IS_X64
			M256Int = {.m256i_u64 = {pointer[0], pointer[1], pointer[2], pointer[3]}};
			#else
			M256Int = {.m256i_u32 = {pointer[0], pointer[1], pointer[2], pointer[3], pointer[4], pointer[5], pointer[6], pointer[7]}};
			#endif
		}

	public:
		constexpr Vector256(
			const __m256& value
		) noexcept:
			M256 {value} {}

		constexpr Vector256(
			const __m256i& value
		) noexcept:
			M256Int {value} {}

		constexpr Vector256(
			const __m256d& value
		) noexcept:
			M256Double {value} {}

	public:
		constexpr Vector256(
			const __m256&& value
		) noexcept:
			M256 {value} {}

		constexpr Vector256(
			const __m256i&& value
		) noexcept:
			M256Int {value} {}

		constexpr Vector256(
			const __m256d&& value
		) noexcept:
			M256Double {value} {}

	public:
		template<class TNumeric>
			requires IsAnyValue<
				TNumeric,
				int16,
				int32,
				int64,
				uint16,
				uint32,
				uint64,
				float32,
				float64>
		constexpr TNumeric (& AsT() noexcept)[sizeof(__m256) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int16>)
			{
				return AsInt16();
			}
			if constexpr (IsSameValue<TNumeric, int32>)
			{
				return AsInt32();
			}
			if constexpr (IsSameValue<TNumeric, int64>)
			{
				return AsInt64();
			}
			if constexpr (IsSameValue<TNumeric, uint16>)
			{
				return AsUint16();
			}
			if constexpr (IsSameValue<TNumeric, uint32>)
			{
				return AsUint32();
			}
			if constexpr (IsSameValue<TNumeric, uint64>)
			{
				return AsUint64();
			}
			if constexpr (IsSameValue<TNumeric, float32>)
			{
				return AsFloat32();
			}
			if constexpr (IsSameValue<TNumeric, float64>)
			{
				return AsFloat64();
			}
		}

		template<class TNumeric>
			requires IsAnyValue<
				TNumeric,
				int8,
				uint8>
		constexpr char (& AsT() noexcept)[sizeof(__m256) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int8>)
			{
				return AsInt8();
			}
			if constexpr (IsSameValue<TNumeric, uint8>)
			{
				return AsUint8();
			}
		}

	public:
		template<class TNumeric>
			requires IsAnyValue<
				TNumeric,
				int16,
				int32,
				int64,
				uint16,
				uint32,
				uint64,
				float32,
				float64>
		[[nodiscard]]
		constexpr const TNumeric (& AsT() const noexcept)[sizeof(__m256) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int16>)
			{
				return AsInt16();
			}
			if constexpr (IsSameValue<TNumeric, int32>)
			{
				return AsInt32();
			}
			if constexpr (IsSameValue<TNumeric, int64>)
			{
				return AsInt64();
			}
			if constexpr (IsSameValue<TNumeric, uint16>)
			{
				return AsUint16();
			}
			if constexpr (IsSameValue<TNumeric, uint32>)
			{
				return AsUint32();
			}
			if constexpr (IsSameValue<TNumeric, uint64>)
			{
				return AsUint64();
			}
			if constexpr (IsSameValue<TNumeric, float32>)
			{
				return AsFloat32();
			}
			if constexpr (IsSameValue<TNumeric, float64>)
			{
				return AsFloat64();
			}
		}

		template<class TNumeric>
			requires IsAnyValue<
				TNumeric,
				int8,
				uint8>
		[[nodiscard]]
		constexpr const char (& AsT() const noexcept)[sizeof(__m256) / sizeof(TNumeric)]
		{
			if constexpr (IsSameValue<TNumeric, int8>)
			{
				return AsInt8();
			}
			if constexpr (IsSameValue<TNumeric, uint8>)
			{
				return AsUint8();
			}
		}

	public:
		constexpr char (& AsInt8() noexcept)[32]
		{
			return M256Int.m256i_i8;
		}

		constexpr int16 (& AsInt16() noexcept)[16]
		{
			return M256Int.m256i_i16;
		}

		constexpr int32 (& AsInt32() noexcept)[8]
		{
			return M256Int.m256i_i32;
		}

		constexpr int64 (& AsInt64() noexcept)[4]
		{
			return M256Int.m256i_i64;
		}

		constexpr uint8 (& AsUint8() noexcept)[32]
		{
			return M256Int.m256i_u8;
		}

		constexpr uint16 (& AsUint16() noexcept)[16]
		{
			return M256Int.m256i_u16;
		}

		constexpr uint32 (& AsUint32() noexcept)[8]
		{
			return M256Int.m256i_u32;
		}

		constexpr uint64 (& AsUint64() noexcept)[4]
		{
			return M256Int.m256i_u64;
		}

		constexpr float32 (& AsFloat32() noexcept)[8]
		{
			return M256.m256_f32;
		}

		constexpr float64 (& AsFloat64() noexcept)[4]
		{
			return M256Double.m256d_f64;
		}

	public:
		[[nodiscard]]
		constexpr const char (& AsInt8() const noexcept)[32]
		{
			return M256Int.m256i_i8;
		}

		[[nodiscard]]
		constexpr const int16 (& AsInt16() const noexcept)[16]
		{
			return M256Int.m256i_i16;
		}

		[[nodiscard]]
		constexpr const int32 (& AsInt32() const noexcept)[8]
		{
			return M256Int.m256i_i32;
		}

		[[nodiscard]]
		constexpr const int64 (& AsInt64() const noexcept)[4]
		{
			return M256Int.m256i_i64;
		}

		[[nodiscard]]
		constexpr const uint8 (& AsUint8() const noexcept)[32]
		{
			return M256Int.m256i_u8;
		}

		[[nodiscard]]
		constexpr const uint16 (& AsUint16() const noexcept)[16]
		{
			return M256Int.m256i_u16;
		}

		[[nodiscard]]
		constexpr const uint32 (& AsUint32() const noexcept)[8]
		{
			return M256Int.m256i_u32;
		}

		[[nodiscard]]
		constexpr const uint64 (& AsUint64() const noexcept)[4]
		{
			return M256Int.m256i_u64;
		}

		[[nodiscard]]
		constexpr const float32 (& AsFloat32() const noexcept)[8]
		{
			return M256.m256_f32;
		}

		[[nodiscard]]
		constexpr const float64 (& AsFloat64() const noexcept)[4]
		{
			return M256Double.m256d_f64;
		}

	public:
		static constexpr Vector256 GetMaskX()
		{
			return __m256i {
				.m256i_u64 = {0xffffffffffffffff, 0x0000000000000000, 0x0000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetMaskY()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0xffffffffffffffff, 0x0000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetMaskZ()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x0000000000000000, 0xffffffffffffffff, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetMaskW()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x0000000000000000, 0x0000000000000000, 0xffffffffffffffff}
			};
		}

		static constexpr Vector256 GetMaskXY()
		{
			return __m256i {
				.m256i_u64 = {0xffffffffffffffff, 0xffffffffffffffff, 0x0000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetMaskXZ()
		{
			return __m256i {
				.m256i_u64 = {0xffffffffffffffff, 0x0000000000000000, 0xffffffffffffffff, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetMaskXW()
		{
			return __m256i {
				.m256i_u64 = {0xffffffffffffffff, 0x0000000000000000, 0x0000000000000000, 0xffffffffffffffff}
			};
		}

		static constexpr Vector256 GetMaskYZ()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0xffffffffffffffff, 0xffffffffffffffff, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetMaskYW()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0xffffffffffffffff, 0x0000000000000000, 0xffffffffffffffff}
			};
		}

		static constexpr Vector256 GetMaskZW()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x0000000000000000, 0xffffffffffffffff, 0xffffffffffffffff}
			};
		}

		static constexpr Vector256 GetMaskXYZ()
		{
			return __m256i {
				.m256i_u64 = {0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetMaskXYW()
		{
			return __m256i {
				.m256i_u64 = {0xffffffffffffffff, 0xffffffffffffffff, 0x0000000000000000, 0xffffffffffffffff}
			};
		}

		static constexpr Vector256 GetMaskXZW()
		{
			return __m256i {
				.m256i_u64 = {0xffffffffffffffff, 0x0000000000000000, 0xffffffffffffffff, 0xffffffffffffffff}
			};
		}

		static constexpr Vector256 GetMaskYZW()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff}
			};
		}

		static constexpr Vector256 GetMaskXYZW()
		{
			return __m256i {
				.m256i_u64 = {0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff}
			};
		}

	public:
		static constexpr Vector256 GetSignMaskX()
		{
			return __m256i {
				.m256i_u64 = {0x8000000000000000, 0x0000000000000000, 0x0000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskY()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x8000000000000000, 0x0000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskZ()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x0000000000000000, 0x8000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskW()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x0000000000000000, 0x0000000000000000, 0x8000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskXY()
		{
			return __m256i {
				.m256i_u64 = {0x8000000000000000, 0x8000000000000000, 0x0000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskXZ()
		{
			return __m256i {
				.m256i_u64 = {0x8000000000000000, 0x0000000000000000, 0x8000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskXW()
		{
			return __m256i {
				.m256i_u64 = {0x8000000000000000, 0x0000000000000000, 0x0000000000000000, 0x8000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskYZ()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x8000000000000000, 0x8000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskYW()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x8000000000000000, 0x0000000000000000, 0x8000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskZW()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x0000000000000000, 0x8000000000000000, 0x8000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskXYZ()
		{
			return __m256i {
				.m256i_u64 = {0x8000000000000000, 0x8000000000000000, 0x8000000000000000, 0x0000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskXYW()
		{
			return __m256i {
				.m256i_u64 = {0x8000000000000000, 0x8000000000000000, 0x0000000000000000, 0x8000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskXZW()
		{
			return __m256i {
				.m256i_u64 = {0x8000000000000000, 0x0000000000000000, 0x8000000000000000, 0x8000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskYZW()
		{
			return __m256i {
				.m256i_u64 = {0x0000000000000000, 0x8000000000000000, 0x8000000000000000, 0x8000000000000000}
			};
		}

		static constexpr Vector256 GetSignMaskXYZW()
		{
			return __m256i {
				.m256i_u64 = {0x8000000000000000, 0x8000000000000000, 0x8000000000000000, 0x8000000000000000}
			};
		}

		// todo: copy and swap?
	public:
		constexpr Vector256& operator=(
			const __m256& value
		) noexcept
		{
			M256 = value;
			return *this;
		}

		constexpr Vector256& operator=(
			const __m256i& value
		) noexcept
		{
			M256Int = value;
			return *this;
		}

		constexpr Vector256& operator=(
			const __m256d& value
		) noexcept
		{
			M256Double = value;
			return *this;
		}

	public:
		constexpr Vector256& operator=(
			__m256&& value
		) noexcept
		{
			M256 = value;
			return *this;
		}

		constexpr Vector256& operator=(
			__m256i&& value
		) noexcept
		{
			M256Int = value;
			return *this;
		}

		constexpr Vector256& operator=(
			__m256d&& value
		) noexcept
		{
			M256Double = value;
			return *this;
		}

	public:
		constexpr operator __m256&() noexcept
		{
			return M256;
		}

		constexpr operator __m256i&() noexcept
		{
			return M256Int;
		}

		constexpr operator __m256d&() noexcept
		{
			return M256Double;
		}

		constexpr operator __m256*() noexcept
		{
			return &M256;
		}

		constexpr operator __m256i*() noexcept
		{
			return &M256Int;
		}

		constexpr operator __m256d*() noexcept
		{
			return &M256Double;
		}

	public:
		constexpr operator const __m256&() const noexcept
		{
			return M256;
		}

		constexpr operator const __m256i&() const noexcept
		{
			return M256Int;
		}

		constexpr operator const __m256d&() const noexcept
		{
			return M256Double;
		}

		constexpr operator const __m256*() const noexcept
		{
			return &M256;
		}

		constexpr operator const __m256i*() const noexcept
		{
			return &M256Int;
		}

		constexpr operator const __m256d*() const noexcept
		{
			return &M256Double;
		}

	private:
		union
		{
			__m256 M256;
			__m256i M256Int;
			__m256d M256Double;
		};
	};
}
