﻿#pragma once
#include "standard/platform/architecture.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::ThreeDNowPrefetch
{
	/// @brief	Load at least 32-byte aligned
	/// memory sequence containing the specified
	/// memory address into the L1 data cache
	inline void Prefetch(
		void* address
	)
	{
		_m_prefetch(address);
	}

	/// @brief	Load at least 32-byte aligned memory
	/// sequence containing the specified memory address
	/// into the L1 data cache in anticipation of a write
	inline void PrefetchWrite(
		const volatile void* address
	)
	{
		_m_prefetchw(address);
	}
}
