﻿#pragma once

#if !ARCH_IS_X64
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::FsGsBase
{
	/// @brief	Read the FS segment base register
	inline uint32 ReadFsBase()
	{
		return _readfsbase_u32();
	}

	/// @brief	Read the FS segment base register
	inline uint64 ReadFsBase64()
	{
		return _readfsbase_u64();
	}

	/// @brief	Read the GS segment base register
	inline uint32 ReadGsBase()
	{
		return _readgsbase_u32();
	}

	/// @brief	Read the GS segment base register
	inline uint64 ReadGsBase64()
	{
		return _readgsbase_u64();
	}

	/// @brief	Write the unsigned 32-bit integer
	/// a to the FS segment base register
	inline void WriteFsBase(
		const uint32 a
	)
	{
		_writefsbase_u32(a);
	}

	/// @brief	Write the unsigned 64-bit integer
	/// a to the FS segment base register
	inline void WriteFsBase(
		const uint64 a
	)
	{
		_writefsbase_u64(a);
	}

	/// @brief	Write the unsigned 32-bit integer
	/// a to the GS segment base register
	inline void WriteGsBase(
		const uint32 a
	)
	{
		_writegsbase_u32(a);
	}

	/// @brief	Write the unsigned 64-bit integer
	/// a to the GS segment base register
	inline void WriteGsBase(
		const uint64 a
	)
	{
		_writegsbase_u64(a);
	}
}
