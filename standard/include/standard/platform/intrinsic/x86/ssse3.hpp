#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Ssse3
	{
		/// @brief	Compute the absolute value of
		/// packed signed 16-bit integers in a
		inline Vector128 AbsEpi16(
			const Vector128 a
		)
		{
			return _mm_abs_epi16(a);
		}

		/// @brief	Compute the absolute value of
		/// packed signed 32-bit integers in a
		inline Vector128 AbsEpi32(
			const Vector128 a
		)
		{
			return _mm_abs_epi32(a);
		}

		/// @brief	Compute the absolute value of
		/// packed signed 8-bit integers in a
		inline Vector128 AbsEpi8(
			const Vector128 a
		)
		{
			return _mm_abs_epi8(a);
		}

		/// @brief	Concatenate 16-byte blocks in a and b into
		/// a 32-byte temporary result, shift the result right
		/// by imm8 bytes, and store the low 16 bytes
		template<int32 VImm8>
		inline Vector128 AlignRightEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_alignr_epi8(a, b, VImm8);
		}

		/// @brief	Cast vector of type Vector128 (double) to type Vector128
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		inline Vector128 CastPdPs(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm_castpd_ps(a);
		}

		/// @brief	Cast vector of type Vector128 (double) to type Vector128 (int)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		inline Vector128 CastPdSi128(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm_castpd_si128(a);
		}

		/// @brief	Cast vector of type Vector128 to type Vector128 (double)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		inline Vector128 CastPsPd(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm_castps_pd(a);
		}

		/// @brief	Cast vector of type Vector128 to type Vector128 (int)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		inline Vector128 CastPsSi128(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm_castps_si128(a);
		}

		/// @brief	Copy the lower double-precision (64-bit) floating-point element of a
		inline float64 ConvertSdF64(
			const Vector128 a
		)
		{
			return _mm_cvtsd_f64(a);
		}

		/// @brief	Cast vector of type Vector128 (int) to type Vector128 (double)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		inline Vector128 CastSi128Pd(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm_castsi128_pd(a);
		}

		/// @brief	Cast vector of type Vector128 (int) to type Vector128
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		inline Vector128 CastSi128Ps(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm_castsi128_ps(a);
		}

		/// @brief	Copy the lower single-precision (32-bit) floating-point element of a
		inline float32 ConvertSsF32(
			const Vector128 a
		)
		{
			return _mm_cvtss_f32(a);
		}

		/// @brief	Horizontally add adjacent pairs of 16-bit integers in a and b, and pack the signed 16-bit
		/// results in dst
		inline Vector128 HorizontallyAddEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hadd_epi16(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of 32-bit integers in a and b, and pack the signed 32-bit
		/// results in dst
		inline Vector128 HorizontallyAddEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hadd_epi32(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of signed 16-bit integers in a and b using saturation, and pack the
		/// signed 16-bit results in dst
		inline Vector128 HorizontallyAddSaturationEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hadds_epi16(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of 16-bit integers in a and b, and pack the signed 16-bit
		/// results in dst
		inline Vector128 HorizontallySubtractEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hsub_epi16(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of 32-bit integers in a and b, and pack the signed 32-bit
		/// results in dst
		inline Vector128 HorizontallySubtractEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hsub_epi32(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of signed 16-bit integers in a and b using saturation, and pack the
		/// signed 16-bit results in dst
		inline Vector128 HorizontallySubtractSaturationEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hsubs_epi16(a, b);
		}

		/// @brief	Multiply packed signed 16-bit integers in a and
		/// b, producing intermediate signed 32-bit integers. Truncate each
		/// intermediate integer to the 18 most significant bits, round by adding 1, and store bits [16:1]
		inline Vector128 MultiplyTruncateEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mulhrs_epi16(a, b);
		}

		/// @brief	Shuffle packed 8-bit integers in a according to
		/// shuffle control mask in the corresponding 8-bit element of b
		inline Vector128 ShuffleEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_shuffle_epi8(a, b);
		}

		/// @brief	Negate packed 16-bit integers in a when the
		/// corresponding signed 16-bit integer in b is negative, and store the
		/// results in dst. Element in dst are
		/// zeroed out when the corresponding element in b is zero
		inline Vector128 SignEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sign_epi16(a, b);
		}

		/// @brief	Negate packed 32-bit integers in a when the
		/// corresponding signed 32-bit integer in b is negative, and store the
		/// results in dst. Element in dst are
		/// zeroed out when the corresponding element in b is zero
		inline Vector128 SignEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sign_epi32(a, b);
		}

		/// @brief	Negate packed 8-bit integers in a when the
		/// corresponding signed 8-bit integer in b is negative, and store the
		/// results in dst. Element in dst are
		/// zeroed out when the corresponding element in b is zero
		inline Vector128 SignEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sign_epi8(a, b);
		}

		/// @brief	Vertically multiply each unsigned 8-bit integer from a with the corresponding signed 8-bit integer from b, producing intermediate signed 16-bit integers. Horizontally add
		/// adjacent pairs of intermediate signed 16-bit integers, and pack the saturated results in dst
		inline Vector128 VerticallyMultiplyAddSaturationEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_maddubs_epi16(a, b);
		}

		#if ARCH_IS_X86

		/// @brief	Compute the absolute value of
		/// packed signed 16-bit integers in a
		inline Vector64 AbsPi16(
			const Vector64 a
		)
		{
			return _mm_abs_pi16(a);
		}

		/// @brief	Compute the absolute value of
		/// packed signed 32-bit integers in a
		inline Vector64 AbsPi32(
			const Vector64 a
		)
		{
			return _mm_abs_pi32(a);
		}

		/// @brief	Compute the absolute value of
		/// packed signed 8-bit integers in a
		inline Vector64 AbsPi8(
			const Vector64 a
		)
		{
			return _mm_abs_pi8(a);
		}

		/// @brief	Concatenate 8-byte blocks in a and b into
		/// a 16-byte temporary result, shift the result right
		/// by imm8 bytes, and store the low 16 bytes
		template<int32 VImm8>
		inline Vector64 AlignRightPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_alignr_pi8(a, b, VImm8);
		}

		/// @brief	Horizontally add adjacent pairs of 16-bit
		/// integers in a and b, and pack the signed 16-bit results
		inline Vector64 HorizontallyAddPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_hadd_pi16(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of 32-bit integers in a and b, and pack the signed 32-bit
		/// results in dst
		inline Vector64 HorizontallyAddPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_hadd_pi32(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of signed 16-bit integers in a and b using saturation, and pack the
		/// signed 16-bit results in dst
		inline Vector64 HorizontallyAddSaturationPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_hadds_pi16(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of 16-bit integers in a and b, and pack the signed 16-bit
		/// results in dst
		inline Vector64 HorizontallySubtractPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_hsub_pi16(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of 32-bit integers in a and b, and pack the signed 32-bit
		/// results in dst
		inline Vector64 HorizontallySubtractPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_hsub_pi32(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of signed 16-bit integers in a and b using saturation, and pack the
		/// signed 16-bit results in dst
		inline Vector64 HorizontallySubtractSaturationPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_hsubs_pi16(a, b);
		}

		/// @brief	Multiply packed signed 16-bit integers in a and
		/// b, producing intermediate signed 32-bit integers. Truncate each
		/// intermediate integer to the 18 most significant bits, round by adding 1, and store bits [16:1]
		inline Vector64 MultiplyTruncatePi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_mulhrs_pi16(a, b);
		}

		/// @brief	Shuffle packed 8-bit integers in a according to
		/// shuffle control mask in the corresponding 8-bit element of b
		inline Vector64 ShufflePi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_shuffle_pi8(a, b);
		}

		/// @brief	Negate packed 16-bit integers in a when the
		/// corresponding signed 16-bit integer in b is negative, and store the
		/// results in dst. Element in dst are
		/// zeroed out when the corresponding element in b is zero
		inline Vector64 SignPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_sign_pi16(a, b);
		}

		/// @brief	Negate packed 32-bit integers in a when the
		/// corresponding signed 32-bit integer in b is negative, and store the
		/// results in dst. Element in dst are
		/// zeroed out when the corresponding element in b is zero
		inline Vector64 SignPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_sign_pi32(a, b);
		}

		/// @brief	Negate packed 8-bit integers in a when the
		/// corresponding signed 8-bit integer in b is negative, and store the
		/// results in dst. Element in dst are
		/// zeroed out when the corresponding element in b is zero
		inline Vector64 SignPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_sign_pi8(a, b);
		}

		/// @brief	Vertically multiply each unsigned 8-bit integer from a with the corresponding signed 8-bit integer from b, producing intermediate signed 16-bit integers. Horizontally add
		/// adjacent pairs of intermediate signed 16-bit integers, and pack the saturated results in dst
		inline Vector64 VerticallyMultiplyAddSaturationPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_maddubs_pi16(a, b);
		}

		#endif
	}
}
