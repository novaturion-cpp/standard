﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::XSave
{
	/// @brief	Copy up to 64-bits from the value of the
	/// extended control register (XCR) specified by a
	inline uint64 XGet(
		const uint32 a
	)
	{
		return _xgetbv(a);
	}

	/// @brief	Perform a full or partial restore of
	/// the enabled processor states using the state
	/// information stored in memory at address\n
	/// State is restored based on bits [62:0]
	/// in mask, XCR0, and address\n
	/// Address must be aligned on a 64-byte boundary
	inline void XRestore(
		const void* address,
		const uint64 mask
	)
	{
		_xrstor(address, mask);
	}

	/// @brief	Reload the x87 FPU, MMX technology, XMM, and
	/// MXCSR registers from the 512-byte memory image at address\n
	/// This data should have been written to memory previously using the FXSAVE
	/// instruction, and in the same format as required by the operating mode\n
	/// Address must be aligned on a 16-byte boundary
	inline void XRestoreFloat(
		const void* address
	)
	{
		_fxrstor(address);
	}

	/// @brief	Perform a full or partial save of the
	/// enabled processor states to memory at address\n
	/// State is saved based on bits [62:0] in mask and XCR0\n
	/// Address must be aligned on a 64-byte boundary
	inline void XSave(
		void* address,
		const uint64 mask
	)
	{
		_xsave(address, mask);
	}

	/// @brief	Save the current state of the x87 FPU, MMX technology,
	/// XMM, and MXCSR registers to a 512-byte memory location at address\n
	/// The layout of the 512-byte region depends on the operating mode
	/// Bytes [511:464] are available for software use
	/// and will not be overwritten by the processor
	inline void XSaveFloat(
		void* address
	)
	{
		_fxsave(address);
	}

	/// @brief	Perform a full or partial save of the
	/// enabled processor states to memory at address\n
	/// State is saved based on bits [62:0] in mask and XCR0\n
	/// Address must be aligned on a 64-byte boundary\n
	/// The hardware may optimize the manner in which data is saved
	/// The performance of this instruction will be equal to or
	/// better than using the XSave function
	inline void XSaveOptimized(
		void* address,
		const uint64 mask
	)
	{
		_xsaveopt(address, mask);
	}

	/// @brief	Copy 64-bits from a to the extended
	/// control register (XCR) specified by mask
	inline void XSet(
		const uint64 a,
		const uint32 mask
	)
	{
		_xsetbv(mask, a);
	}


	#if ARCH_IS_X64

	/// @brief	Reload the x87 FPU, MMX technology, XMM, and
	/// MXCSR registers from the 512-byte memory image at address\n
	/// This data should have been written to memory previously using the FXSAVE64
	/// instruction, and in the same format as required by the operating mode\n
	/// Address must be aligned on a 16-byte boundary
    /// @note   Available only on x64 targets
	inline void FloatXRestore64(
		const void* address
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		_fxrstor64(address);
	}

	/// @brief	Save the current state of the x87 FPU, MMX technology,
	/// XMM, and MXCSR registers to a 512-byte memory location at address\n
	/// The layout of the 512-byte region depends on the operating mode\n
	/// Bytes [511:464] are available for software use
	/// and will not be overwritten by the processor
    /// @note   Available only on x64 targets
	inline void FloatXSave64(
		void* address
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		_fxsave64(address);
	}

	/// @brief	Perform a full or partial restore of
	/// the enabled processor states using the state
	/// information stored in memory at address\n
	/// State is restored based on bits [62:0]
	/// in mask, XCR0, and address\n
	/// Address must be aligned on a 64-byte boundary
    /// @note   Available only on x64 targets
	inline void XRestore64(
		const void* address,
		const uint64 mask
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		_xrstor64(address, mask);
	}

	/// @brief	Perform a full or partial save of the
	/// enabled processor states to memory at address\n
	/// State is saved based on bits [62:0] in mask and XCR0\n
	/// Address must be aligned on a 64-byte boundary
    /// @note   Available only on x64 targets
	inline void XSave64(
		void* address,
		const uint64 mask
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		_xsave64(address, mask);
	}

	/// @brief	Perform a full or partial save of the
	/// enabled processor states to memory at address\n
	/// State is saved based on bits [62:0] in mask and XCR0\n
	/// Address must be aligned on a 64-byte boundary\n
	/// The hardware may optimize the manner in which data is saved
	/// The performance of this instruction will be equal to or
	/// better than using the XSave64 function
    /// @note   Available only on x64 targets
	inline void XSaveOptimized64(
		void* address,
		const uint64 mask
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		_xsaveopt64(address, mask);
	}

	#endif
}
