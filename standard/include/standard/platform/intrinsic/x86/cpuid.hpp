﻿#pragma once
#include "cpuid-leaf.hpp"

#include "standard/platform/type/int.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Cpuid
{
	/// @brief	Execute 'cpuid' instruction
	/// @see	<a href="https://en.wikipedia.org/wiki/CPUID">Wikipedia</a>
	inline Leaf Cpuid(
		const uint32 function
	)
	{
		Leaf data;
		__cpuid(
			static_cast<int32*>(static_cast<void*>(&data)),
			static_cast<int32>(function)
		);
		return data;
	}

	/// @brief	Execute 'cpuid' instruction
	/// @see	<a href="https://en.wikipedia.org/wiki/CPUID">Wikipedia</a>
	inline Leaf Cpuid(
		const uint32 function,
		const uint32 subFunction
	)
	{
		Leaf data;
		__cpuidex(
			static_cast<int32*>(static_cast<void*>(&data)),
			static_cast<int32>(function),
			static_cast<int32>(subFunction)
		);
		return data;
	}
}
