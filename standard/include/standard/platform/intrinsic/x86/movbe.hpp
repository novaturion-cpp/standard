﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::MovBe
{
	/// @brief	Load 16 bits from memory and
	/// perform a byte swap operation
	inline uint16 Load16(
		const void* address
	)
	{
		return _load_be_u16(address);
	}

	/// @brief	Load 32 bits from memory and
	/// perform a byte swap operation
	inline uint32 Load32(
		const void* address
	)
	{
		return _load_be_u32(address);
	}

	/// @brief	Perform a bit swap operation of the 16
	/// bits in data, and store the results to memory
	inline void Store16(
		void* address,
		const uint16 data
	)
	{
		_store_be_u16(address, data);
	}

	/// @brief	Perform a bit swap operation of the 32
	/// bits in data, and store the results to memory
	inline void Store32(
		void* address,
		const uint32 data
	)
	{
		_store_be_u32(address, data);
	}


	#if ARCH_IS_X64

	/// @brief	Load 64 bits from memory and
	/// perform a byte swap operation
    /// @note   Available only on x64 targets
	inline uint64 Load64(
		const void* address
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _load_be_u64(address);
	}

	/// @brief	Perform a bit swap operation of the 64
	/// bits in data, and store the results to memory
    /// @note   Available only on x64 targets
	inline void Store64(
		void* address,
		const uint64 data
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		_store_be_u64(address, data);
	}

	#endif
}
