﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::RdTsc
{
	/// @brief	Get the current 64-bit value
	/// of the processor's time-stamp counter,
	/// and store the IA32_TSC_AUX MSR (signature
	/// value) into memory at outMsr
	inline uint64 ReadTimeStampCounter(
		uint32* outMsr
	)
	{
		return __rdtscp(outMsr);
	}
}
