#pragma once
#include "standard/platform/type/uint.hpp"

#if !ARCH_IS_X86 && !ARCH_IS_X64
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif


namespace Std::inline Platform::inline Intrinsic
{
	consteval uint32 ShuffleMask(
		const uint8 e
	)
	{
		return (e | (e << 2) | (e << 4) | (e << 6));
	}

	consteval uint32 ShuffleMask(
		const uint8 e0,
		const uint8 e1,
		const uint8 e2,
		const uint8 e3
	)
	{
		return (e0 | (e1 << 2) | (e2 << 4) | (e3 << 6));
	}
}
