﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::RdSeed
{
	/// @brief	Read a 16-bit NIST SP800-90B and SP800-90C
	/// compliant random value and store in outValue
	/// @return	'true' if a random value was generated
	inline bool Step(
		uint16* outValue
	)
	{
		return _rdseed16_step(outValue);
	}

	/// @brief	Read a 32-bit NIST SP800-90B and SP800-90C
	/// compliant random value and store in outValue
	/// @return	'true' if a random value was generated
	inline bool Step(
		uint32* outValue
	)
	{
		return _rdseed32_step(outValue);
	}


	#if ARCH_IS_X64

	/// @brief	Read a 64-bit NIST SP800-90B and SP800-90C
	/// compliant random value and store in outValue
	/// @return	'true' if a random value was generated
    /// @note   Available only on x64 targets
	inline bool Step(
		uint64* outValue
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _rdseed64_step(outValue);
	}

	#endif
}
