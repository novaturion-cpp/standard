﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Control and Status Register (CSR) states and modes
	namespace Csr
	{
		inline constexpr uint16 ExceptionStateMask = 0x003f;
		inline constexpr uint16 ExceptionMaskMask = 0x1f80;
		inline constexpr uint16 RoundingModeMask = 0x6000;
		inline constexpr uint16 FlushZeroModeMask = 0x8000;


		namespace EExceptionMask
		{
			enum : uint16
			{
				Invalid = 0x0080,
				Denorm = 0x0100,
				DivZero = 0x0200,
				Overflow = 0x0400,
				Underflow = 0x0800,
				Inexact = 0x1000,
			};
		}


		namespace EExceptionState
		{
			enum : uint16
			{
				Invalid = 0x0001,
				Denorm = 0x0002,
				DivZero = 0x0004,
				Overflow = 0x0008,
				Underflow = 0x0010,
				Inexact = 0x0020,
			};
		}


		namespace EFlushZeroMode
		{
			enum : uint16
			{
				On = 0x8000,
				Off = 0x0000,
			};
		}


		namespace ERoundingMode
		{
			enum : uint16
			{
				Nearest = 0x0000,
				Down = 0x2000,
				Up = 0x4000,
				ToZero = 0x6000,
			};
		}
	}
}
