﻿#pragma once

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Monitor
{
	/// @brief	Arm address monitoring hardware
	/// A store to an address within the specified
	/// address range triggers the monitoring hardware
	inline void Monitor(
		const void* address,
		const uint32 extensions,
		const uint32 hints
	)
	{
		_mm_monitor(address, extensions, hints);
	}

	/// @brief	Hint to the processor that it can enter an
	/// implementation-dependent-optimized state while waiting
	/// for an event or store operation to the address range specified by MONITOR
	inline void MonitorWait(
		const uint32 extensions,
		const uint32 hints
	)
	{
		_mm_mwait(extensions, hints);
	}
}
