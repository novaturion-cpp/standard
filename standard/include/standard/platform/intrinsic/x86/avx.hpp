﻿#pragma once
#include "comparison-type.hpp"
#include "rounding-type.hpp"

#include "standard/platform/intrinsic/type/vector128.hpp"
#include "standard/platform/intrinsic/type/vector256.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Avx
	{
		/// @brief	Add packed double-precision (64-bit)
		/// floating-point elements in a and b
		inline Vector256 AddPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_add_pd(a, b);
		}

		/// @brief	Add packed single-precision (32-bit)
		/// floating-point elements in a and b
		inline Vector256 AddPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_add_ps(a, b);
		}

		/// @brief	Add odd-numbered packed double-precision
		/// (64-bit) floating-point elements and
		/// subtract even-numbered elements in a and b
		inline Vector256 AddSubtractPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_addsub_pd(a, b);
		}

		/// @brief	Add odd-numbered packed single-precision
		/// (32-bit) floating-point elements and
		/// subtract even-numbered elements in a and b
		inline Vector256 AddSubtractPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_addsub_ps(a, b);
		}

		/// @brief	Compute the bitwise NOT of packed
		/// double-precision (64-bit) floating-point
		/// elements in a and then AND with b
		inline Vector256 AndNotPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_andnot_pd(a, b);
		}

		/// @brief	Compute the bitwise NOT of packed
		/// single-precision (32-bit) floating-point
		/// elements in a and then AND with b
		inline Vector256 AndNotPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_andnot_ps(a, b);
		}

		/// @brief	Compute the bitwise AND of packed
		/// double-precision (64-bit) floating-point
		/// elements in a and b
		inline Vector256 AndPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_and_pd(a, b);
		}

		/// @brief	Compute the bitwise AND of packed
		/// single-precision (32-bit) floating-point
		/// elements in a and b
		inline Vector256 AndPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_and_ps(a, b);
		}

		/// @brief	Blend packed double-precision (64-bit)
		/// floating-point elements from a and b using mask imm8
		template<int32 VImm8>
		inline Vector256 BlendPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_blend_pd(a, b, VImm8);
		}

		/// @brief	Blend packed double-precision (64-bit)
		/// floating-point elements from a and b using mask
		inline Vector256 BlendPd(
			const Vector256 a,
			const Vector256 b,
			const Vector256 mask
		)
		{
			return _mm256_blendv_pd(a, b, mask);
		}

		/// @brief	Blend packed single-precision (32-bit)
		/// floating-point elements from a and b using mask imm8
		template<int32 VImm8>
		inline Vector256 BlendPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_blend_ps(a, b, VImm8);
		}

		/// @brief	Blend packed single-precision (32-bit)
		/// floating-point elements from a and b using mask
		inline Vector256 BlendPs(
			const Vector256 a,
			const Vector256 b,
			const Vector256 mask
		)
		{
			return _mm256_blendv_ps(a, b, mask);
		}

		/// @brief	Broadcast 128 bits from memory
		/// (composed of 2 packed double-precision (64-bit)
		/// floating-point elements) to all elements of the vector
		inline Vector256 BroadcastPd(
			const Vector128* address
		)
		{
			return _mm256_broadcast_pd(*address);
		}

		/// @brief	Broadcast 128 bits from memory
		/// (composed of 4 packed single-precision (32-bit)
		/// floating-point elements) to all elements of the vector
		inline Vector256 BroadcastPs(
			const Vector128* address
		)
		{
			return _mm256_broadcast_ps(*address);
		}

		/// @brief	Broadcast a double-precision (64-bit)
		/// floating-point element from memory to all
		/// elements of the vector
		inline Vector256 BroadcastSd(
			const float64* address
		)
		{
			return _mm256_broadcast_sd(address);
		}

		/// @brief	Broadcast a single-precision (32-bit)
		/// floating-point element from memory to all
		/// elements of the vector
		inline Vector128 BroadcastSs(
			const float32* address
		)
		{
			return _mm_broadcast_ss(address);
		}

		/// @brief	Broadcast a single-precision (32-bit)
		/// floating-point element from memory to all
		/// elements of the vector
		inline Vector256 BroadcastSs256(
			const float32* address
		)
		{
			return _mm256_broadcast_ss(address);
		}

		/// @brief	Cast vector of type Vector128 (double) to type Vector256 (double)
		/// @note	The upper 128 bits of the result are undefined
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastPd128Pd256(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castpd128_pd256(a);
		}

		/// @brief	Cast vector of type Vector256 (double) to type Vector128 (double)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector128 CastPd256Pd128(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castpd256_pd128(a);
		}

		/// @brief	Cast vector of type Vector256 (double) to type Vector256
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastPdPs(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castpd_ps(a);
		}

		/// @brief	Cast vector of type Vector256 (double) to type Vector256 (int)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastPdSi256(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castpd_si256(a);
		}

		/// @brief	Cast vector of type Vector128 to type Vector256
		/// @note	The upper 128 bits of the result are undefined
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastPs128Ps256(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castps128_ps256(a);
		}

		/// @brief	Cast vector of type Vector256 to type Vector128
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector128 CastPs256Ps128(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castps256_ps128(a);
		}

		/// @brief	Cast vector of type Vector256 to type Vector256 (double)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastPsPd(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castps_pd(a);
		}

		/// @brief	Cast vector of type Vector256 to type Vector256 (int)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastPsSi256(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castps_si256(a);
		}

		/// @brief	Cast vector of type Vector128 (int) to type Vector256 (int)
		/// @note	The upper 128 bits of the result are undefined
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastSi128Si256(
			const Vector128 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castsi128_si256(a);
		}

		/// @brief	Cast vector of type Vector256 (int) to type Vector256 (double)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastSi256Pd(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castsi256_pd(a);
		}

		/// @brief	Cast vector of type Vector256 (int) to type Vector256
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector256 CastSi256Ps(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castsi256_ps(a);
		}

		/// @brief	Cast vector of type Vector256 (int) to type Vector128 (int)
		/// @note	This intrinsic is only used for compilation and does not
		/// generate any instructions, thus it has zero latency
		constexpr Vector128 CastSi256Si128(
			const Vector256 a
		)
		{
			// todo: can be deleted (?)
			return _mm256_castsi256_si128(a);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b based on the
		/// comparison operand specified by imm8
		template<EComparisonType VComparison>
		inline Vector128 ComparePd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmp_pd(a, b, static_cast<int32>(VComparison));
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b based on the
		/// comparison operand specified by imm8
		template<EComparisonType VComparison>
		inline Vector256 ComparePd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmp_pd(a, b, static_cast<int32>(VComparison));
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b based on the
		/// comparison operand specified by imm8
		template<EComparisonType VComparison>
		inline Vector128 ComparePs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmp_ps(a, b, static_cast<int32>(VComparison));
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b based on the
		/// comparison operand specified by imm8
		template<EComparisonType VComparison>
		inline Vector256 ComparePs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmp_ps(a, b, static_cast<int32>(VComparison));
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// element in a and b based on the comparison operand specified by imm8
		/// Store the result in the lower element, and copy
		/// the upper element from a to the upper element of the vector
		template<EComparisonType VComparison>
		inline Vector128 CompareSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmp_sd(a, b, static_cast<int32>(VComparison));
		}

		/// @brief	Compare the lower single-precision (32-bit) floating-point
		/// element in a and b based on the comparison operand specified by imm8\n
		/// Store the result in the lower element, and copy
		/// the upper 3 packed elements from a to the upper elements of the vector
		template<EComparisonType VComparison>
		inline Vector128 CompareSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmp_ss(a, b, static_cast<int32>(VComparison));
		}

		/// @brief	Convert packed signed 32-bit integers in a to
		/// packed double-precision (64-bit) floating-point elements
		inline Vector256 ConvertEpi32Pd(
			const Vector128 a
		)
		{
			return _mm256_cvtepi32_pd(a);
		}

		/// @brief	Convert packed signed 32-bit integers in a to
		/// packed single-precision (32-bit) floating-point elements
		inline Vector256 ConvertEpi32Ps(
			const Vector256 a
		)
		{
			return _mm256_cvtepi32_ps(a);
		}

		/// @brief	Convert packed double-precision (64-bit)
		/// floating-point elements to packed 32-bit integers
		inline Vector128 ConvertPdEpi32(
			const Vector256 a
		)
		{
			return _mm256_cvtpd_epi32(a);
		}

		/// @brief	Convert packed double-precision (64-bit)
		/// floating-point elements to packed single-precision
		/// (32-bit) floating-point elements
		inline Vector128 ConvertPdPs(
			const Vector256 a
		)
		{
			return _mm256_cvtpd_ps(a);
		}

		/// @brief	Convert packed single-precision (32-bit)
		/// floating-point elements to packed 32-bit integers
		inline Vector256 ConvertPsEpi32(
			const Vector256 a
		)
		{
			return _mm256_cvtps_epi32(a);
		}

		/// @brief	Convert packed single-precision (32-bit)
		/// floating-point elements to packed double-precision
		/// (64-bit) floating-point elements
		inline Vector256 ConvertPsPd(
			const Vector128 a
		)
		{
			return _mm256_cvtps_pd(a);
		}

		/// @brief	Convert packed double-precision (64-bit)
		/// floating-point elements to packed 32-bit integers
		/// with truncation
		inline Vector128 ConvertTruncatePdEpi32(
			const Vector256 a
		)
		{
			return _mm256_cvttpd_epi32(a);
		}

		/// @brief	Convert packed single-precision (32-bit)
		/// floating-point elements to packed 32-bit integers
		/// with truncation
		inline Vector256 ConvertTruncatePsEpi32(
			const Vector256 a
		)
		{
			return _mm256_cvttps_epi32(a);
		}

		/// @brief	Divide packed double-precision (64-bit)
		/// floating-point elements in a by packed elements
		/// in b
		inline Vector256 DividePd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_div_pd(a, b);
		}

		/// @brief	Divide packed single-precision (32-bit)
		/// floating-point elements in a by packed elements
		/// in b
		inline Vector256 DividePs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_div_ps(a, b);
		}

		/// @brief	Conditionally multiply the packed single-precision
		/// (32-bit) floating-point elements in a and b using the high
		/// 4 bits in imm8, sum the four products, and conditionally
		/// store the sum using the low 4 bits of imm8
		template<int32 VImm8>
		inline Vector256 DotProductPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_dp_ps(a, b, VImm8);
		}

		/// @brief	Duplicate even-indexed double-precision (64-bit)
		/// floating-point elements from a
		inline Vector256 DuplicateEvenPd(
			const Vector256 a
		)
		{
			return _mm256_movedup_pd(a);
		}

		/// @brief	Duplicate even-indexed single-precision (32-bit)
		/// floating-point elements from a
		inline Vector256 DuplicateEvenPs(
			const Vector256 a
		)
		{
			return _mm256_moveldup_ps(a);
		}

		/// @brief	Duplicate odd-indexed single-precision (32-bit)
		/// floating-point elements from a
		inline Vector256 DuplicateOddPs(
			const Vector256 a
		)
		{
			return _mm256_movehdup_ps(a);
		}

		/// @brief	Extract 128 bits (composed of 2 packed
		/// double-precision (64-bit) floating-point elements)
		/// from a, selected with mask imm8
		template<int32 VImm8>
		inline Vector128 ExtractF128Pd(
			const Vector256 a
		)
		{
			return _mm256_extractf128_pd(a, VImm8);
		}

		/// @brief	Extract 128 bits (composed of 4 packed
		/// single-precision (32-bit) floating-point elements)
		/// from a, selected with mask imm8
		template<int32 VImm8>
		inline Vector128 ExtractF128Ps(
			const Vector256 a
		)
		{
			return _mm256_extractf128_ps(a, VImm8);
		}

		/// @brief	Extract 128 bits (composed of integer data)
		/// from a, selected with mask imm8
		template<int32 VImm8>
		inline Vector128 ExtractF128Si256(
			const Vector256 a
		)
		{
			return _mm256_extractf128_si256(a, VImm8);
		}

		/// @brief	Horizontally add adjacent pairs of double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector256 HorizontallyAddPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hadd_pd(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of single-precision
		/// (32-bit) floating-point elements in a and b
		inline Vector256 HorizontallyAddPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hadd_ps(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector256 HorizontallySubtractPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hsub_pd(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of single-precision
		/// (32-bit) floating-point elements in a and b
		inline Vector256 HorizontallySubtractPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hsub_ps(a, b);
		}

		/// @brief	Copy a, then insert 128 bits (composed of 2 packed
		/// double-precision (64-bit) floating-point elements) from b
		/// at the location specified by mask imm8
		template<int32 VImm8>
		inline Vector256 InsertF128Pd(
			const Vector256 a,
			const Vector128 b
		)
		{
			return _mm256_insertf128_pd(a, b, VImm8);
		}

		/// @brief	Copy a, then insert 128 bits (composed of 4 packed
		/// single-precision (32-bit) floating-point elements) from b
		/// at the location specified by mask imm8
		template<int32 VImm8>
		inline Vector256 InsertF128Ps(
			const Vector256 a,
			const Vector128 b
		)
		{
			return _mm256_insertf128_ps(a, b, VImm8);
		}

		/// @brief	Copy a, then insert 128 bits from b
		/// at the location specified by mask imm8
		template<int32 VImm8>
		inline Vector256 InsertF128Si256(
			const Vector256 a,
			const Vector128 b
		)
		{
			return _mm256_insertf128_si256(a, b, VImm8);
		}

		/// @brief	Load 256-bits (composed of 4 packed double-precision
		/// (64-bit) floating-point elements) from memory
		/// Memory address must be aligned on a 32-byte boundary or a
		/// general-protection exception may be generated
		inline Vector256 LoadPd(
			const float64* address
		)
		{
			return _mm256_load_pd(address);
		}

		/// @brief	Load 256-bits (composed of 8 packed single-precision
		/// (32-bit) floating-point elements) from memory
		/// Memory address must be aligned on a 32-byte boundary or a
		/// general-protection exception may be generated
		inline Vector256 LoadPs(
			const float32* address
		)
		{
			return _mm256_load_ps(address);
		}

		/// @brief	Load 256-bits of integer data from unaligned memory
		/// @note	May perform better than LoadUnalignedSi256 when
		/// the data crosses a cache line boundary
		inline Vector256 LoadQUnalignedSi256(
			const Vector256* address
		)
		{
			return _mm256_lddqu_si256(*address);
		}

		/// @brief	Load 256-bits of integer data from memory
		/// Memory address must be aligned on a 32-byte boundary or a
		/// general-protection exception may be generated
		inline Vector256 LoadSi256(
			const Vector256* address
		)
		{
			return _mm256_load_si256(*address);
		}

		/// @brief	Load 256-bits (composed of 4 packed double-precision
		/// (64-bit) floating-point elements) from memory
		/// Memory address does not need to be aligned on any particular boundary
		inline Vector256 LoadUnalignedPd(
			const float64* address
		)
		{
			return _mm256_loadu_pd(address);
		}

		/// @brief	Load 256-bits (composed of 8 packed single-precision
		/// (32-bit) floating-point elements) from memory
		/// Memory address does not need to be aligned on any particular boundary
		inline Vector256 LoadUnalignedPs(
			const float32* address
		)
		{
			return _mm256_loadu_ps(address);
		}

		/// @brief	Load 256-bits of integer data from memory
		/// Memory address does not need to be aligned on any particular boundary
		inline Vector256 LoadUnalignedSi256(
			const Vector256* address
		)
		{
			return _mm256_loadu_si256(*address);
		}

		/// @brief	Load packed double-precision (64-bit) floating-point
		/// elements from memory using mask (elements are zeroed
		/// out when the high bit of the corresponding element is not set)
		inline Vector128 MaskLoadPd(
			const float64* address,
			const Vector128 mask
		)
		{
			return _mm_maskload_pd(address, mask);
		}

		/// @brief	Load packed double-precision (64-bit) floating-point
		/// elements from memory using mask (elements are zeroed
		/// out when the high bit of the corresponding element is not set)
		inline Vector256 MaskLoadPd(
			const float64* address,
			const Vector256 mask
		)
		{
			return _mm256_maskload_pd(address, mask);
		}

		/// @brief	Load packed single-precision (32-bit) floating-point
		/// elements from memory using mask (elements are zeroed
		/// out when the high bit of the corresponding element is not set)
		inline Vector128 MaskLoadPs(
			const float32* address,
			const Vector128 mask
		)
		{
			return _mm_maskload_ps(address, mask);
		}

		/// @brief	Load packed single-precision (32-bit) floating-point
		/// elements from memory using mask (elements are zeroed
		/// out when the high bit of the corresponding element is not set)
		inline Vector256 MaskLoadPs(
			const float32* address,
			const Vector256 mask
		)
		{
			return _mm256_maskload_ps(address, mask);
		}

		/// @brief	Store packed double-precision (64-bit) floating-point
		/// elements from a into memory using mask
		inline void MaskStorePd(
			float64* address,
			const Vector256 mask,
			const Vector256 a
		)
		{
			_mm256_maskstore_pd(address, mask, a);
		}

		/// @brief	Store packed double-precision (64-bit) floating-point
		/// elements from a into memory using mask
		inline void MaskStorePd(
			float64* address,
			const Vector128 mask,
			const Vector128 a
		)
		{
			_mm_maskstore_pd(address, mask, a);
		}

		/// @brief	Store packed single-precision (32-bit) floating-point
		/// elements from a into memory using mask
		inline void MaskStorePs(
			float32* address,
			const Vector256 mask,
			const Vector256 a
		)
		{
			_mm256_maskstore_ps(address, mask, a);
		}

		/// @brief	Store packed single-precision (32-bit) floating-point
		/// elements from a into memory using mask
		inline void MaskStorePs(
			float32* address,
			const Vector128 mask,
			const Vector128 a
		)
		{
			_mm_maskstore_ps(address, mask, a);
		}

		/// @brief	Compare packed double-precision (64-bit) floating-point
		/// elements in a and b, and return packed maximum values\n
		/// Result does not follow the IEEE Standard for Floating-Point Arithmetic
		/// (IEEE 754) maximum value when inputs are NaN or signed-zero values
		inline Vector256 MaxPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_max_pd(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit) floating-point
		/// elements in a and b, and return packed maximum values\n
		/// Result does not follow the IEEE Standard for Floating-Point Arithmetic
		/// (IEEE 754) maximum value when inputs are NaN or signed-zero values
		inline Vector256 MaxPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_max_ps(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit) floating-point
		/// elements in a and b, and return packed minimum values\n
		/// Result does not follow the IEEE Standard for Floating-Point Arithmetic
		/// (IEEE 754) minimum value when inputs are NaN or signed-zero values
		inline Vector256 MinPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_min_pd(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit) floating-point
		/// elements in a and b
		/// @return	Packed minimum values
		/// @note	Result does not follow the IEEE Standard for Floating-Point Arithmetic
		/// (IEEE 754) minimum value when inputs are NaN or signed-zero values
		inline Vector256 MinPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_min_ps(a, b);
		}

		/// @brief	Set each bit of mask based on the most
		/// significant bit of the corresponding packed double-precision
		/// (64-bit) floating-point element in a
		inline int32 MoveMaskPd(
			const Vector256 a
		)
		{
			return _mm256_movemask_pd(a);
		}

		/// @brief	Set each bit of mask based on the most
		/// significant bit of the corresponding packed single-precision
		/// (32-bit) floating-point element in a
		inline int32 MoveMaskPs(
			const Vector256 a
		)
		{
			return _mm256_movemask_ps(a);
		}

		/// @brief	Multiply packed double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector256 MultiplyPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mul_pd(a, b);
		}

		/// @brief	Multiply packed single-precision
		/// (32-bit) floating-point elements in a and b
		inline Vector256 MultiplyPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mul_ps(a, b);
		}

		/// @brief	Compute the bitwise OR of packed double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector256 OrPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_or_pd(a, b);
		}

		/// @brief	Compute the bitwise OR of packed single-precision
		/// (32-bit) floating-point elements in a and b
		inline Vector256 OrPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_or_ps(a, b);
		}

		/// @brief	Shuffle 128-bits (composed of 2 packed double-precision
		/// (64-bit) floating-point elements) in a using the control imm8
		template<int32 VImm8>
		inline Vector256 Permute2F128Pd(
			const Vector256 a
		)
		{
			return _mm256_permute2f128_pd(a, a, VImm8);
		}

		/// @brief	Shuffle 128-bits (composed of 2 packed double-precision
		/// (64-bit) floating-point elements) selected by mask imm8 from a and b
		template<int32 VImm8>
		inline Vector256 Permute2F128Pd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_permute2f128_pd(a, b, VImm8);
		}

		/// @brief	Shuffle 128-bits (composed of 4 packed single-precision
		/// (32-bit) floating-point elements) in a using the control imm8
		template<int32 VImm8>
		inline Vector256 Permute2F128Ps(
			const Vector256 a
		)
		{
			return _mm256_permute2f128_ps(a, a, VImm8);
		}

		/// @brief	Shuffle 128-bits (composed of 4 packed single-precision
		/// (32-bit) floating-point elements) selected by mask imm8 from a and b
		template<int32 VImm8>
		inline Vector256 Permute2F128Ps(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_permute2f128_ps(a, b, VImm8);
		}

		/// @brief	Shuffle 128-bits (composed of integer data)
		/// in a using the control imm8
		template<int32 VImm8>
		inline Vector256 Permute2F128Si256(
			const Vector256 a
		)
		{
			return _mm256_permute2f128_si256(a, a, VImm8);
		}

		/// @brief	Shuffle 128-bits (composed of integer data)
		/// selected by mask imm8 from a and b
		template<int32 VImm8>
		inline Vector256 Permute2F128Si256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_permute2f128_si256(a, b, VImm8);
		}

		/// @brief	Shuffle double-precision (64-bit) floating-point
		/// elements in a using the control imm8
		template<int32 VImm8>
		inline Vector128 PermutePd(
			const Vector128 a
		)
		{
			return _mm_permute_pd(a, VImm8);
		}

		/// @brief	Shuffle double-precision (64-bit) floating-point
		/// elements in a within 128-bit lanes using the control imm8
		template<int32 VImm8>
		inline Vector256 PermutePd(
			const Vector256 a
		)
		{
			return _mm256_permute_pd(a, VImm8);
		}

		/// @brief	Shuffle double-precision (64-bit) floating-point
		/// elements in a using the control in b
		inline Vector128 PermutePd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_permutevar_pd(a, b);
		}

		/// @brief	Shuffle double-precision (64-bit) floating-point
		/// elements in a within 128-bit lanes using the control in b
		inline Vector256 PermutePd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_permutevar_pd(a, b);
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a using the control imm8
		template<int32 VImm8>
		inline Vector128 PermutePs(
			const Vector128 a
		)
		{
			return _mm_permute_ps(a, VImm8);
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a within 128-bit lanes using the control imm8
		template<int32 VImm8>
		inline Vector256 PermutePs(
			const Vector256 a
		)
		{
			return _mm256_permute_ps(a, VImm8);
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a using the control in b
		inline Vector128 PermutePs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_permutevar_ps(a, b);
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a within 128-bit lanes using the control in b
		inline Vector256 PermutePs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_permutevar_ps(a, b);
		}

		/// @brief	Compute the approximate reciprocal of packed single-precision (32-bit)
		/// floating-point elements in a
		/// The maximum relative error for this approximation is less than 1.5*2^-12
		inline Vector256 ReciprocalPs(
			const Vector256 a
		)
		{
			return _mm256_rcp_ps(a);
		}

		/// @brief	Compute the approximate reciprocal square root
		/// of packed single-precision (32-bit) floating-point elements
		/// in a
		/// The maximum relative error for this approximation is less than 1.5*2^-12
		inline Vector256 ReciprocalSqrtPs(
			const Vector256 a
		)
		{
			return _mm256_rsqrt_ps(a);
		}

		/// @brief	Round the packed double-precision (64-bit)
		/// floating-point elements in a using the rounding parameter\n
		/// Rounding is done according to the rounding[3:0] parameter,
		/// which can be one of ERoundingType NoExcept fields
		template<ERoundingType VRounding>
		inline Vector256 RoundPd(
			const Vector256 a
		)
		{
			return _mm256_round_pd(a, static_cast<int32>(VRounding));
		}

		/// @brief	Round the packed single-precision (32-bit)
		/// floating-point elements in a using the rounding parameter\n
		/// Rounding is done according to the rounding[3:0] parameter,
		/// which can be one of ERoundingType NoExcept fields
		template<ERoundingType VRounding>
		inline Vector256 RoundPs(
			const Vector256 a
		)
		{
			return _mm256_round_ps(a, static_cast<int32>(VRounding));
		}

		/// @brief	Broadcast 16-bit integer a to all vector elements
		/// @note	May generate the vpbroadcastw
		inline Vector256 Set1Epi16(
			const int16 a
		)
		{
			return _mm256_set1_epi16(a);
		}

		/// @brief	Broadcast 32-bit integer a to all vector elements
		/// @note	May generate the vpbroadcastd
		inline Vector256 Set1Epi32(
			const int32 a
		)
		{
			return _mm256_set1_epi32(a);
		}

		/// @brief	Broadcast 8-bit integer a to all vector elements
		/// @note	May generate the vpbroadcastb
		inline Vector256 Set1Epi8(
			const int8 a
		)
		{
			return _mm256_set1_epi8(a);
		}

		/// @brief	Broadcast double-precision (64-bit)
		/// floating-point value a to all vector elements
		inline Vector256 Set1Pd(
			const float64 a
		)
		{
			return _mm256_set1_pd(a);
		}

		/// @brief	Broadcast single-precision (32-bit)
		/// floating-point value a to all vector elements
		inline Vector256 Set1Ps(
			const float32 a
		)
		{
			return _mm256_set1_ps(a);
		}

		/// @brief	Set packed 16-bit integers
		/// in vector with the supplied values
		inline Vector256 SetEpi16(
			const int16 e0,
			const int16 e1,
			const int16 e2,
			const int16 e3,
			const int16 e4,
			const int16 e5,
			const int16 e6,
			const int16 e7,
			const int16 e8,
			const int16 e9,
			const int16 e10,
			const int16 e11,
			const int16 e12,
			const int16 e13,
			const int16 e14,
			const int16 e15
		)
		{
			return _mm256_set_epi16(e15, e14, e13, e12, e11, e10, e9, e8, e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed 32-bit integers
		/// in vector with the supplied values
		inline Vector256 SetEpi32(
			const int32 e0,
			const int32 e1,
			const int32 e2,
			const int32 e3,
			const int32 e4,
			const int32 e5,
			const int32 e6,
			const int32 e7
		)
		{
			return _mm256_set_epi32(e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed 8-bit integers
		/// in vector with the supplied values
		inline Vector256 SetEpi8(
			const int8 e0,
			const int8 e1,
			const int8 e2,
			const int8 e3,
			const int8 e4,
			const int8 e5,
			const int8 e6,
			const int8 e7,
			const int8 e8,
			const int8 e9,
			const int8 e10,
			const int8 e11,
			const int8 e12,
			const int8 e13,
			const int8 e14,
			const int8 e15,
			const int8 e16,
			const int8 e17,
			const int8 e18,
			const int8 e19,
			const int8 e20,
			const int8 e21,
			const int8 e22,
			const int8 e23,
			const int8 e24,
			const int8 e25,
			const int8 e26,
			const int8 e27,
			const int8 e28,
			const int8 e29,
			const int8 e30,
			const int8 e31
		)
		{
			return _mm256_set_epi8(
				e31,
				e30,
				e29,
				e28,
				e27,
				e26,
				e25,
				e24,
				e23,
				e22,
				e21,
				e20,
				e19,
				e18,
				e17,
				e16,
				e15,
				e14,
				e13,
				e12,
				e11,
				e10,
				e9,
				e8,
				e7,
				e6,
				e5,
				e4,
				e3,
				e2,
				e1,
				e0
			);
		}

		/// @brief	Set packed double-precision (64-bit) floating-point
		/// elements in vector with the supplied values
		inline Vector256 SetPd(
			const float64 e0,
			const float64 e1,
			const float64 e2,
			const float64 e3
		)
		{
			return _mm256_set_pd(e3, e2, e1, e0);
		}

		/// @brief	Set packed single-precision (32-bit) floating-point
		/// elements in vector with the supplied values
		inline Vector256 SetPs(
			const float32 e0,
			const float32 e1,
			const float32 e2,
			const float32 e3,
			const float32 e4,
			const float32 e5,
			const float32 e6,
			const float32 e7
		)
		{
			return _mm256_set_ps(e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed 16-bit integers in vector
		/// with the supplied values in reverse order
		inline Vector256 SetReverseEpi16(
			const int16 e0,
			const int16 e1,
			const int16 e2,
			const int16 e3,
			const int16 e4,
			const int16 e5,
			const int16 e6,
			const int16 e7,
			const int16 e8,
			const int16 e9,
			const int16 e10,
			const int16 e11,
			const int16 e12,
			const int16 e13,
			const int16 e14,
			const int16 e15
		)
		{
			return _mm256_setr_epi16(e15, e14, e13, e12, e11, e10, e9, e8, e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed 32-bit integers in vector
		/// with the supplied values in reverse order
		inline Vector256 SetReverseEpi32(
			const int32 e0,
			const int32 e1,
			const int32 e2,
			const int32 e3,
			const int32 e4,
			const int32 e5,
			const int32 e6,
			const int32 e7
		)
		{
			return _mm256_setr_epi32(e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed 8-bit integers in vector
		/// with the supplied values in reverse order
		inline Vector256 SetReverseEpi8(
			const int8 e0,
			const int8 e1,
			const int8 e2,
			const int8 e3,
			const int8 e4,
			const int8 e5,
			const int8 e6,
			const int8 e7,
			const int8 e8,
			const int8 e9,
			const int8 e10,
			const int8 e11,
			const int8 e12,
			const int8 e13,
			const int8 e14,
			const int8 e15,
			const int8 e16,
			const int8 e17,
			const int8 e18,
			const int8 e19,
			const int8 e20,
			const int8 e21,
			const int8 e22,
			const int8 e23,
			const int8 e24,
			const int8 e25,
			const int8 e26,
			const int8 e27,
			const int8 e28,
			const int8 e29,
			const int8 e30,
			const int8 e31
		)
		{
			return _mm256_setr_epi8(
				e31,
				e30,
				e29,
				e28,
				e27,
				e26,
				e25,
				e24,
				e23,
				e22,
				e21,
				e20,
				e19,
				e18,
				e17,
				e16,
				e15,
				e14,
				e13,
				e12,
				e11,
				e10,
				e9,
				e8,
				e7,
				e6,
				e5,
				e4,
				e3,
				e2,
				e1,
				e0
			);
		}

		/// @brief	Set packed double-precision (64-bit) floating-point
		/// elements in vector with the supplied values in reverse order
		inline Vector256 SetReversePd(
			const float64 e0,
			const float64 e1,
			const float64 e2,
			const float64 e3
		)
		{
			return _mm256_setr_pd(e3, e2, e1, e0);
		}

		/// @brief	Set packed single-precision (32-bit) floating-point
		/// elements in vector with the supplied values in reverse order
		inline Vector256 SetReversePs(
			const float32 e0,
			const float32 e1,
			const float32 e2,
			const float32 e3,
			const float32 e4,
			const float32 e5,
			const float32 e6,
			const float32 e7
		)
		{
			return _mm256_setr_ps(e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Return vector of type Vector256 (double)
		/// with all elements set to zero
		inline Vector256 SetZeroPd()
		{
			return _mm256_setzero_pd();
		}

		/// @brief	Return vector of type Vector256
		/// with all elements set to zero
		inline Vector256 SetZeroPs()
		{
			return _mm256_setzero_ps();
		}

		/// @brief	Return vector of type Vector256 (int)
		/// with all elements set to zero
		inline Vector256 SetZeroSi256()
		{
			return _mm256_setzero_si256();
		}

		/// @brief	Shuffle double-precision (64-bit) floating-point
		/// elements within 128-bit lanes using the control imm8
		template<int32 VImm8>
		inline Vector256 ShufflePd(
			const Vector256 a
		)
		{
			return _mm256_shuffle_pd(a, a, VImm8);
		}

		/// @brief	Shuffle double-precision (64-bit) floating-point
		/// elements within 128-bit lanes using the control imm8
		template<int32 VImm8>
		inline Vector256 ShufflePd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_shuffle_pd(a, b, VImm8);
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a within 128-bit lanes using the control imm8
		template<int32 VImm8>
		inline Vector256 ShufflePs(
			const Vector256 a
		)
		{
			return _mm256_shuffle_ps(a, a, VImm8);
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a within 128-bit lanes using the control imm8
		template<int32 VImm8>
		inline Vector256 ShufflePs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_shuffle_ps(a, b, VImm8);
		}

		/// @brief	Compute the square root of packed double-precision
		/// (64-bit) floating-point elements in a
		inline Vector256 SqrtPd(
			const Vector256 a
		)
		{
			return _mm256_sqrt_pd(a);
		}

		/// @brief	Compute the square root of packed single-precision
		/// (32-bit) floating-point elements in a
		inline Vector256 SqrtPs(
			const Vector256 a
		)
		{
			return _mm256_sqrt_ps(a);
		}

		/// @brief	Store 256-bits (composed of 4 packed double-precision
		/// (64-bit) floating-point elements) from a into memory
		/// Memory address must be aligned on a 32-byte boundary or a
		/// general-protection exception may be generated
		inline void StorePd(
			float64* address,
			const Vector256 a
		)
		{
			_mm256_store_pd(address, a);
		}

		/// @brief	Store 256-bits (composed of 8 packed single-precision
		/// (32-bit) floating-point elements) from a into memory
		/// Memory address must be aligned on a 32-byte boundary or a
		/// general-protection exception may be generated
		inline void StorePs(
			float32* address,
			const Vector256 a
		)
		{
			_mm256_store_ps(address, a);
		}

		/// @brief	Store 256-bits of integer data from a into memory
		/// Memory address must be aligned on a 32-byte boundary or a
		/// general-protection exception may be generated
		inline void StoreSi256(
			Vector256* address,
			const Vector256 a
		)
		{
			_mm256_store_si256(*address, a);
		}

		/// @brief	Store 256-bits (composed of 4 packed double-precision
		/// (64-bit) floating-point elements) from a into memory
		/// Memory address does not need to be aligned on any particular boundary
		inline void StoreUnalignedPd(
			float64* address,
			const Vector256 a
		)
		{
			_mm256_storeu_pd(address, a);
		}

		/// @brief	Store 256-bits (composed of 8 packed single-precision
		/// (32-bit) floating-point elements) from a into memory
		/// Memory address does not need to be aligned on any particular boundary
		inline void StoreUnalignedPs(
			float32* address,
			const Vector256 a
		)
		{
			_mm256_storeu_ps(address, a);
		}

		/// @brief	Store 256-bits of integer data from a into memory
		/// Memory address does not need to be aligned on any particular boundary
		inline void StoreUnalignedSi256(
			Vector256* address,
			const Vector256 a
		)
		{
			_mm256_storeu_si256(*address, a);
		}

		/// @brief	Store 256-bits (composed of 8 packed single-precision (32-bit) floating-point
		/// elements) from a into memory using a non-temporal memory hint
		/// Memory address must be aligned on a 32-byte boundary or a
		/// general-protection exception may be generated
		inline void StreamPs(
			float32* address,
			const Vector256 a
		)
		{
			_mm256_stream_ps(address, a);
		}

		/// @brief	Subtract packed double-precision (64-bit) floating-point
		/// elements in b from packed double-precision (64-bit) floating-point
		/// elements in a
		inline Vector256 SubtractPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sub_pd(a, b);
		}

		/// @brief	Subtract packed single-precision (32-bit) floating-point elements
		/// in b from packed single-precision (32-bit) floating-point elements in a
		inline Vector256 SubtractPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sub_ps(a, b);
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing double-precision
		/// (64-bit) floating-point elements) in a and b, producing an intermediate
		/// 256-bit value, and set ZF to 1 if the sign bit of each 64-bit element
		/// in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	CF value
		inline bool TestCarryFlagPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testc_pd(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits (representing double-precision
		/// (64-bit) floating-point elements) in a and b, producing an intermediate
		/// 128-bit value, and set ZF to 1 if the sign bit of each 64-bit element
		/// in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	CF value
		inline bool TestCarryFlagPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testc_pd(a, b));
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing single-precision
		/// (32-bit) floating-point elements) in a and b, producing an intermediate
		/// 256-bit value, and set ZF to 1 if the sign bit of each 32-bit element
		/// in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	CF value
		inline bool TestCarryFlagPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testc_ps(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits (representing single-precision
		/// (32-bit) floating-point elements) in a and b, producing an intermediate
		/// 128-bit value, and set ZF to 1 if the sign bit of each 32-bit element
		/// in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	CF value
		inline bool TestCarryFlagPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testc_ps(a, b));
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing integer data)
		/// in a and b, and set ZF to 1 if the result is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, and set CF to 1 if the
		/// result is zero, otherwise set CF to 0
		/// @return	CF value
		inline bool TestCarryFlagSi256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testc_si256(a, b));
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing double-precision
		/// (64-bit) floating-point elements) in a and b, producing an intermediate
		/// 256-bit value, and set ZF to 1 if the sign bit of each 64-bit element
		/// in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	'true' if both the ZF and CF values are zero
		inline bool TestZeroCarryFlagPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testnzc_pd(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits (representing double-precision
		/// (64-bit) floating-point elements) in a and b, producing an intermediate
		/// 128-bit value, and set ZF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	'true' if both the ZF and CF values are zero
		inline bool TestZeroCarryFlagPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testnzc_pd(a, b));
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing single-precision
		/// (32-bit) floating-point elements) in a and b, producing an intermediate
		/// 256-bit value, and set ZF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	'true' if both the ZF and CF values are zero
		inline bool TestZeroCarryFlagPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testnzc_ps(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits (representing single-precision
		/// (32-bit) floating-point elements) in a and b, producing an intermediate
		/// 128-bit value, and set ZF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	'true' if both the ZF and CF values are zero
		inline bool TestZeroCarryFlagPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testnzc_ps(a, b));
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing integer data)
		/// in a and b, and set ZF to 1 if the result is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, and set CF to 1 if the
		/// result is zero, otherwise set CF to 0
		/// @return	'true' if both the ZF and CF values are zero
		inline bool TestZeroCarryFlagSi256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testnzc_si256(a, b));
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing double-precision
		/// (64-bit) floating-point elements) in a and b, producing an intermediate
		/// 256-bit value, and set ZF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	the ZF value
		inline bool TestZeroFlagPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testz_pd(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits (representing double-precision
		/// (64-bit) floating-point elements) in a and b, producing an intermediate
		/// 128-bit value, and set ZF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 64-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	the ZF value
		inline bool TestZeroFlagPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testz_pd(a, b));
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing single-precision
		/// (32-bit) floating-point elements) in a and b, producing an intermediate
		/// 256-bit value, and set ZF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	the ZF value
		inline bool TestZeroFlagPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testz_ps(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits (representing single-precision
		/// (32-bit) floating-point elements) in a and b, producing an intermediate
		/// 128-bit value, and set ZF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, producing an
		/// intermediate value, and set CF to 1 if the sign bit of each 32-bit
		/// element in the intermediate value is zero, otherwise set CF to 0
		/// @return	the ZF value
		inline bool TestZeroFlagPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testz_ps(a, b));
		}

		/// @brief	Compute the bitwise AND of 256 bits (representing integer data)
		/// in a and b, and set ZF to 1 if the result is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, and set
		/// CF to 1 if the result is zero, otherwise set CF to 0
		/// @return	the ZF value
		inline bool TestZeroFlagSi256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return static_cast<bool>(_mm256_testz_si256(a, b));
		}

		/// @brief	Unpack and interleave double-precision (64-bit)
		/// floating-point elements from the high half of each 128-bit
		/// lane in a and b
		inline Vector256 UnpackHighPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpackhi_pd(a, b);
		}

		/// @brief	Unpack and interleave single-precision (32-bit)
		/// floating-point elements from the high half of each 128-bit
		/// lane in a and b
		inline Vector256 UnpackHighPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpackhi_ps(a, b);
		}

		/// @brief	Unpack and interleave double-precision (64-bit)
		/// floating-point elements from the low half of each 128-bit
		/// lane in a and b
		inline Vector256 UnpackLowPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpacklo_pd(a, b);
		}

		/// @brief	Unpack and interleave single-precision (32-bit)
		/// floating-point elements from the low half of each 128-bit
		/// lane in a and b
		inline Vector256 UnpackLowPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpacklo_ps(a, b);
		}

		/// @brief	Compute the bitwise XOR of packed double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector256 XorPd(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_xor_pd(a, b);
		}

		/// @brief	Compute the bitwise XOR of packed single-precision
		/// (32-bit) floating-point elements in a and b
		inline Vector256 XorPs(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_xor_ps(a, b);
		}

		/// @brief	Zero the contents of all XMM or YMM registers
		inline void ZeroAll()
		{
			_mm256_zeroall();
		}

		/// @brief	Zero the upper 128 bits of all YMM registers
		/// The lower 128-bits of the registers are unmodified
		inline void ZeroUpper()
		{
			_mm256_zeroupper();
		}


		#if ARCH_IS_X64

		/// @brief	Broadcast 64-bit integer a to all vector elements
		/// @note	May generate the vpbroadcastq
		/// @note   Available only on x64 targets
		inline Vector256 Set1Epi64x(
			const int64 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm256_set1_epi64x(a);
		}

		/// @brief	Set packed 64-bit integers
		/// in vector with the supplied values
		/// @note   Available only on x64 targets
		inline Vector256 SetEpi64x(
			const int64 e0,
			const int64 e1,
			const int64 e2,
			const int64 e3
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm256_set_epi64x(e3, e2, e1, e0);
		}

		/// @brief	Set packed 64-bit integers in vector
		/// with the supplied values in reverse order
		/// @note   Available only on x64 targets
		inline Vector256 SetReverseEpi64x(
			const int64 e0,
			const int64 e1,
			const int64 e2,
			const int64 e3
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm256_setr_epi64x(e3, e2, e1, e0);
		}

		#endif
	}
}
