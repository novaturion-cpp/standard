﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"
#include "standard/platform/intrinsic/type/vector256.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Avx2
	{
		/// @brief	Compute the absolute value
		/// of packed signed 16-bit integers in a
		inline Vector256 AbsoluteEpi16(
			const Vector256 a
		)
		{
			return _mm256_abs_epi16(a);
		}

		/// @brief	Compute the absolute value
		/// of packed signed 32-bit integers in a
		inline Vector256 AbsoluteEpi32(
			const Vector256 a
		)
		{
			return _mm256_abs_epi32(a);
		}

		/// @brief	Compute the absolute value
		/// of packed signed 8-bit integers in a
		inline Vector256 AbsoluteEpi8(
			const Vector256 a
		)
		{
			return _mm256_abs_epi8(a);
		}

		/// @brief	Add packed 16-bit integers
		/// in a and b
		inline Vector256 AddEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_add_epi16(a, b);
		}

		/// @brief	Add packed 32-bit integers
		/// in a and b
		inline Vector256 AddEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_add_epi32(a, b);
		}

		/// @brief	Add packed 64-bit integers
		/// in a and b
		inline Vector256 AddEpi64(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_add_epi64(a, b);
		}

		/// @brief	Add packed 8-bit integers
		/// in a and b
		inline Vector256 AddEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_add_epi8(a, b);
		}

		/// @brief	Add packed 16-bit integers
		/// in a and b using saturation
		inline Vector256 AddSaturationEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_adds_epi16(a, b);
		}

		/// @brief	Add packed 8-bit integers
		/// in a and b using saturation
		inline Vector256 AddSaturationEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_adds_epi8(a, b);
		}

		/// @brief	Add packed unsigned 16-bit
		/// integers in a and b using saturation
		inline Vector256 AddSaturationEpu16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_adds_epu16(a, b);
		}

		/// @brief	Add packed unsigned 8-bit
		/// integers in a and b using saturation
		inline Vector256 AddSaturationEpu8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_adds_epu8(a, b);
		}

		/// @brief	Concatenate pairs of 16-byte blocks
		/// in a and b into a 32-byte temporary result,
		/// shift the result right by imm8 bytes,
		/// and store the low 16 bytes
		template<int32 VImm8>
		inline Vector256 AlignRightEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_alignr_epi8(a, b, VImm8);
		}

		/// @brief	Compute the bitwise NOT of 256 bits
		/// (representing integer data) in a and then AND with b
		inline Vector256 AndNotSi256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_andnot_si256(a, b);
		}

		/// @brief	Compute the bitwise AND of 256 bits
		/// (representing integer data) in a and b
		inline Vector256 AndSi256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_and_si256(a, b);
		}

		/// @brief	Average packed unsigned 16-bit
		/// integers in a and b
		inline Vector256 AverageEpu16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_avg_epu16(a, b);
		}

		/// @brief	Average packed unsigned 8-bit
		/// integers in a and b
		inline Vector256 AverageEpu8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_avg_epu8(a, b);
		}

		/// @brief	Blend packed 16-bit integers from
		/// a and b within 128-bit lanes using control mask imm8
		template<int32 VImm8>
		inline Vector256 BlendEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_blend_epi16(a, b, VImm8);
		}

		/// @brief	Blend packed 32-bit integers from
		/// a and b using control mask imm8
		template<int32 VImm8>
		inline Vector128 BlendEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_blend_epi32(a, b, VImm8);
		}

		/// @brief	Blend packed 32-bit integers from
		/// a and b using control mask imm8
		template<int32 VImm8>
		inline Vector256 BlendEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_blend_epi32(a, b, VImm8);
		}

		/// @brief	Blend packed 8-bit integers from
		/// a and b using mask
		inline Vector256 BlendEpi8(
			const Vector256 a,
			const Vector256 b,
			const Vector256 mask
		)
		{
			return _mm256_blendv_epi8(a, b, mask);
		}

		/// @brief	Broadcast the low packed 16-bit
		/// integer from a to all elements of vector
		inline Vector256 Broadcast256Epi16(
			const Vector128 a
		)
		{
			return _mm256_broadcastw_epi16(a);
		}

		/// @brief	Broadcast the low packed 32-bit
		/// integer from a to all elements of vector
		inline Vector256 Broadcast256Epi32(
			const Vector128 a
		)
		{
			return _mm256_broadcastd_epi32(a);
		}

		/// @brief	Broadcast the low packed 64-bit
		/// integer from a to all elements of vector
		inline Vector256 Broadcast256Epi64(
			const Vector128 a
		)
		{
			return _mm256_broadcastq_epi64(a);
		}

		/// @brief	Broadcast the low packed 8-bit
		/// integer from a to all elements of vector
		inline Vector256 Broadcast256Epi8(
			const Vector128 a
		)
		{
			return _mm256_broadcastb_epi8(a);
		}

		/// @brief	Broadcast the low packed 16-bit
		/// integer from a to all elements of vector
		inline Vector128 BroadcastEpi16(
			const Vector128 a
		)
		{
			return _mm_broadcastw_epi16(a);
		}

		/// @brief	Broadcast the low packed 32-bit
		/// integer from a to all elements of vector
		inline Vector128 BroadcastEpi32(
			const Vector128 a
		)
		{
			return _mm_broadcastd_epi32(a);
		}

		/// @brief	Broadcast the low packed 64-bit
		/// integer from a to all elements of vector
		inline Vector128 BroadcastEpi64(
			const Vector128 a
		)
		{
			return _mm_broadcastq_epi64(a);
		}

		/// @brief	Broadcast the low packed 8-bit
		/// integer from a to all elements of vector
		inline Vector128 BroadcastEpi8(
			const Vector128 a
		)
		{
			return _mm_broadcastb_epi8(a);
		}

		/// @brief	Broadcast the low double-precision
		/// (64-bit) floating-point element from a to
		/// all elements of vector
		inline Vector128 BroadcastSdPd(
			const Vector128 a
		)
		{
			return _mm_broadcastsd_pd(a);
		}

		/// @brief	Broadcast the low double-precision
		/// (64-bit) floating-point element from a to
		/// all elements of vector
		inline Vector256 BroadcastSdPd256(
			const Vector128 a
		)
		{
			return _mm256_broadcastsd_pd(a);
		}

		/// @brief	Broadcast 128 bits of integer data
		/// from a to all 128-bit lanes of vector
		inline Vector256 BroadcastSi128Si256(
			const Vector128 a
		)
		{
			return _mm256_broadcastsi128_si256(a);
		}

		/// @brief	Broadcast the low single-precision
		/// (32-bit) floating-point element from a to
		/// all elements of vector
		inline Vector128 BroadcastSsPs(
			const Vector128 a
		)
		{
			return _mm_broadcastss_ps(a);
		}

		/// @brief	Broadcast the low single-precision
		/// (32-bit) floating-point element from a to
		/// all elements of vector
		inline Vector256 BroadcastSsPs256(
			const Vector128 a
		)
		{
			return _mm256_broadcastss_ps(a);
		}

		/// @brief	Compare packed 16-bit
		/// integers in a and b for equality
		inline Vector256 CompareEqualEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmpeq_epi16(a, b);
		}

		/// @brief	Compare packed 32-bit
		/// integers in a and b for equality
		inline Vector256 CompareEqualEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmpeq_epi32(a, b);
		}

		/// @brief	Compare packed 64-bit
		/// integers in a and b for equality
		inline Vector256 CompareEqualEpi64(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmpeq_epi64(a, b);
		}

		/// @brief	Compare packed 8-bit
		/// integers in a and b for equality
		inline Vector256 CompareEqualEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmpeq_epi8(a, b);
		}

		/// @brief	Compare packed 16-bit
		/// integers in a and b for greater-than
		inline Vector256 CompareGreaterEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmpgt_epi16(a, b);
		}

		/// @brief	Compare packed 32-bit
		/// integers in a and b for greater-than
		inline Vector256 CompareGreaterEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmpgt_epi32(a, b);
		}

		/// @brief	Compare packed 64-bit
		/// integers in a and b for greater-than
		inline Vector256 CompareGreaterEpi64(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmpgt_epi64(a, b);
		}

		/// @brief	Compare packed 8-bit
		/// integers in a and b for greater-than
		inline Vector256 CompareGreaterEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_cmpgt_epi8(a, b);
		}

		/// @brief	Sign extend packed 16-bit
		/// integers in a to packed 32-bit integers
		inline Vector256 ConvertEpi16Epi32(
			const Vector128 a
		)
		{
			return _mm256_cvtepi16_epi32(a);
		}

		/// @brief	Sign extend packed 16-bit
		/// integers in a to packed 64-bit integers
		inline Vector256 ConvertEpi16Epi64(
			const Vector128 a
		)
		{
			return _mm256_cvtepi16_epi64(a);
		}

		/// @brief	Sign extend packed 32-bit
		/// integers in a to packed 64-bit integers
		inline Vector256 ConvertEpi32Epi64(
			const Vector128 a
		)
		{
			return _mm256_cvtepi32_epi64(a);
		}

		/// @brief	Sign extend packed 8-bit
		/// integers in a to packed 16-bit integers
		inline Vector256 ConvertEpi8Epi16(
			const Vector128 a
		)
		{
			return _mm256_cvtepi8_epi16(a);
		}

		/// @brief	Sign extend packed 8-bit
		/// integers in a to packed 32-bit integers
		inline Vector256 ConvertEpi8Epi32(
			const Vector128 a
		)
		{
			return _mm256_cvtepi8_epi32(a);
		}

		/// @brief	Sign extend packed 8-bit
		/// integers in the low 8 bytes of
		/// a to packed 64-bit integers
		inline Vector256 ConvertEpi8Epi64(
			const Vector128 a
		)
		{
			return _mm256_cvtepi8_epi64(a);
		}

		/// @brief	Zero extend packed unsigned 16-bit
		/// integers in a to packed 32-bit integers
		inline Vector256 ConvertEpu16Epi32(
			const Vector128 a
		)
		{
			return _mm256_cvtepu16_epi32(a);
		}

		/// @brief	Zero extend packed unsigned 16-bit
		/// integers in a to packed 64-bit integers
		inline Vector256 ConvertEpu16Epi64(
			const Vector128 a
		)
		{
			return _mm256_cvtepu16_epi64(a);
		}

		/// @brief	Zero extend packed unsigned 32-bit
		/// integers in a to packed 64-bit integers
		inline Vector256 ConvertEpu32Epi64(
			const Vector128 a
		)
		{
			return _mm256_cvtepu32_epi64(a);
		}

		/// @brief	Zero extend packed unsigned 8-bit
		/// integers in a to packed 16-bit integers
		inline Vector256 ConvertEpu8Epi16(
			const Vector128 a
		)
		{
			return _mm256_cvtepu8_epi16(a);
		}

		/// @brief	Zero extend packed unsigned 8-bit
		/// integers in a to packed 32-bit integers
		inline Vector256 ConvertEpu8Epi32(
			const Vector128 a
		)
		{
			return _mm256_cvtepu8_epi32(a);
		}

		/// @brief	Zero extend packed unsigned 8-bit integers
		/// in the low 8 bytes of a to packed 64-bit integers
		inline Vector256 ConvertEpu8Epi64(
			const Vector128 a
		)
		{
			return _mm256_cvtepu8_epi64(a);
		}

		/// @brief	Extract 128 bits (composed of
		/// integer data) from a, selected with imm8
		template<int32 VImm8>
		inline Vector128 ExtractI128Si256(
			const Vector256 a
		)
		{
			return _mm256_extracti128_si256(a, VImm8);
		}

		/// @brief	Gather 64-bit integers from memory using 32-bit indices
		/// 64-bit elements are loaded from addresses starting at address and offset
		/// by each 32-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 Gather256Epi64(
			const int64* address,
			const Vector128 index
		)
		{
			return _mm256_i32gather_epi64(address, index, VScale);
		}

		/// @brief	Gather double-precision (64-bit) floating-point elements
		/// from memory using 32-bit indices. 64-bit elements are loaded from
		/// addresses starting at address and offset by each 32-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 Gather256Pd(
			const float64* address,
			const Vector128 index
		)
		{
			return _mm256_i32gather_pd(address, index, VScale);
		}

		/// @brief	Gather 32-bit integers from memory using 64-bit indices
		/// 32-bit elements are loaded from addresses starting at address and offset
		/// by each 64-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 Gather64Epi32(
			const int32* address,
			const Vector128 index
		)
		{
			return _mm_i64gather_epi32(address, index, VScale);
		}

		/// @brief	Gather 32-bit integers from memory using 64-bit indices
		/// 32-bit elements are loaded from addresses starting at address and offset
		/// by each 64-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 Gather64Epi32(
			const int32* address,
			const Vector256 index
		)
		{
			return _mm256_i64gather_epi32(address, index, VScale);
		}

		/// @brief	Gather 64-bit integers from memory using 64-bit indices
		/// 64-bit elements are loaded from addresses starting at address and offset
		/// by each 64-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 Gather64Epi64(
			const int64* address,
			const Vector128 index
		)
		{
			return _mm_i64gather_epi64(address, index, VScale);
		}

		/// @brief	Gather 64-bit integers from memory using 64-bit indices
		/// 64-bit elements are loaded from addresses starting at address and offset
		/// by each 64-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 Gather64Epi64(
			const int64* address,
			const Vector256 index
		)
		{
			return _mm256_i64gather_epi64(address, index, VScale);
		}

		/// @brief	Gather double-precision (64-bit) floating-point elements
		/// from memory using 64-bit indices. 64-bit elements are loaded from
		/// addresses starting at address and offset by each 64-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 Gather64Pd(
			const float64* address,
			const Vector128 index
		)
		{
			return _mm_i64gather_pd(address, index, VScale);
		}

		/// @brief	Gather double-precision (64-bit) floating-point elements
		/// from memory using 64-bit indices. 64-bit elements are loaded from
		/// addresses starting at address and offset by each 64-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 Gather64Pd(
			const float64* address,
			const Vector256 index
		)
		{
			return _mm256_i64gather_pd(address, index, VScale);
		}

		/// @brief	Gather single-precision (32-bit) floating-point elements
		/// from memory using 64-bit indices. 32-bit elements are loaded from
		/// addresses starting at address and offset by each 64-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 Gather64Ps(
			const float32* address,
			const Vector128 index
		)
		{
			return _mm_i64gather_ps(address, index, VScale);
		}

		/// @brief	Gather single-precision (32-bit) floating-point elements
		/// from memory using 64-bit indices. 32-bit elements are loaded from
		/// addresses starting at address and offset by each 64-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 Gather64Ps(
			const float32* address,
			const Vector256 index
		)
		{
			return _mm256_i64gather_ps(address, index, VScale);
		}

		/// @brief	Gather 32-bit integers from memory using 32-bit indices
		/// 32-bit elements are loaded from addresses starting at address and offset
		/// by each 32-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherEpi32(
			const int32* address,
			const Vector128 index
		)
		{
			return _mm_i32gather_epi32(address, index, VScale);
		}

		/// @brief	Gather 32-bit integers from memory using 32-bit indices
		/// 32-bit elements are loaded from addresses starting at address and offset
		/// by each 32-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 GatherEpi32(
			const int32* address,
			const Vector256 index
		)
		{
			return _mm256_i32gather_epi32(address, index, VScale);
		}

		/// @brief	Gather 64-bit integers from memory using 32-bit indices
		/// 64-bit elements are loaded from addresses starting at address and offset
		/// by each 32-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherEpi64(
			const int64* address,
			const Vector128 index
		)
		{
			return _mm_i32gather_epi64(address, index, VScale);
		}

		/// @brief	Gather 32-bit integers from memory using 64-bit indices
		/// 32-bit elements are loaded from addresses starting at address and offset
		/// by each 64-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMask64Epi32(
			const Vector128 src,
			const int32* address,
			const Vector128 index,
			const Vector128 mask
		)
		{
			return _mm_mask_i64gather_epi32(src, address, index, mask, VScale);
		}

		/// @brief	Gather 32-bit integers from memory using 64-bit indices
		/// 32-bit elements are loaded from addresses starting at address and offset
		/// by each 64-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMask64Epi32(
			const Vector128 src,
			const int32* address,
			const Vector256 index,
			const Vector128 mask
		)
		{
			return _mm256_mask_i64gather_epi32(src, address, index, mask, VScale);
		}

		/// @brief	Gather 64-bit integers from memory using 64-bit indices
		/// 64-bit elements are loaded from addresses starting at address and offset
		/// by each 64-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMask64Epi64(
			const Vector128 src,
			const int64* address,
			const Vector128 index,
			const Vector128 mask
		)
		{
			return _mm_mask_i64gather_epi64(src, address, index, mask, VScale);
		}

		/// @brief	Gather 64-bit integers from memory using 64-bit indices
		/// 64-bit elements are loaded from addresses starting at address and offset
		/// by each 64-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 GatherMask64Epi64(
			const Vector256 src,
			const int64* address,
			const Vector256 index,
			const Vector256 mask
		)
		{
			return _mm256_mask_i64gather_epi64(src, address, index, mask, VScale);
		}

		/// @brief	Gather double-precision (64-bit) floating-point elements
		/// from memory using 64-bit indices. 64-bit elements are loaded from
		/// addresses starting at address and offset by each 64-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMask64Pd(
			const Vector128 src,
			const float64* address,
			const Vector128 index,
			const Vector128 mask
		)
		{
			return _mm_mask_i64gather_pd(src, address, index, mask, VScale);
		}

		/// @brief	Gather double-precision (64-bit) floating-point elements
		/// from memory using 64-bit indices. 64-bit elements are loaded from
		/// addresses starting at address and offset by each 64-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 GatherMask64Pd(
			const Vector256 src,
			const float64* address,
			const Vector256 index,
			const Vector256 mask
		)
		{
			return _mm256_mask_i64gather_pd(src, address, index, mask, VScale);
		}

		/// @brief	Gather single-precision (32-bit) floating-point elements
		/// from memory using 64-bit indices. 32-bit elements are loaded from
		/// addresses starting at address and offset by each 64-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMask64Ps(
			const Vector128 src,
			const float32* address,
			const Vector128 index,
			const Vector128 mask
		)
		{
			return _mm_mask_i64gather_ps(src, address, index, mask, VScale);
		}

		/// @brief	Gather single-precision (32-bit) floating-point elements
		/// from memory using 64-bit indices. 32-bit elements are loaded from
		/// addresses starting at address and offset by each 64-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMask64Ps(
			const Vector128 src,
			const float32* address,
			const Vector256 index,
			const Vector128 mask
		)
		{
			return _mm256_mask_i64gather_ps(src, address, index, mask, VScale);
		}

		/// @brief	Gather 32-bit integers from memory using 32-bit indices
		/// 32-bit elements are loaded from addresses starting at address and offset
		/// by each 32-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMaskEpi32(
			const Vector128 src,
			const int32* address,
			const Vector128 index,
			const Vector128 mask
		)
		{
			return _mm_mask_i32gather_epi32(src, address, index, mask, VScale);
		}

		/// @brief	Gather 32-bit integers from memory using 32-bit indices
		/// 32-bit elements are loaded from addresses starting at address and offset
		/// by each 32-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 GatherMaskEpi32(
			const Vector256 src,
			const int32* address,
			const Vector256 index,
			const Vector256 mask
		)
		{
			return _mm256_mask_i32gather_epi32(src, address, index, mask, VScale);
		}

		/// @brief	Gather 64-bit integers from memory using 32-bit indices
		/// 64-bit elements are loaded from addresses starting at address and offset
		/// by each 32-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMaskEpi64(
			const Vector128 src,
			const int64* address,
			const Vector128 index,
			const Vector128 mask
		)
		{
			return _mm_mask_i32gather_epi64(src, address, index, mask, VScale);
		}

		/// @brief	Gather 64-bit integers from memory using 32-bit indices
		/// 64-bit elements are loaded from addresses starting at address and offset
		/// by each 32-bit element in index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 GatherMaskEpi64(
			const Vector256 src,
			const int64* address,
			const Vector128 index,
			const Vector256 mask
		)
		{
			return _mm256_mask_i32gather_epi64(src, address, index, mask, VScale);
		}

		/// @brief	Gather double-precision (64-bit) floating-point elements
		/// from memory using 32-bit indices. 64-bit elements are loaded from
		/// addresses starting at address and offset by each 32-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMaskPd(
			const Vector128 src,
			const float64* address,
			const Vector128 index,
			const Vector128 mask
		)
		{
			return _mm_mask_i32gather_pd(src, address, index, mask, VScale);
		}

		/// @brief	Gather double-precision (64-bit) floating-point elements
		/// from memory using 32-bit indices. 64-bit elements are loaded from
		/// addresses starting at address and offset by each 32-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 GatherMaskPd(
			const Vector256 src,
			const float64* address,
			const Vector128 index,
			const Vector256 mask
		)
		{
			return _mm256_mask_i32gather_pd(src, address, index, mask, VScale);
		}

		/// @brief	Gather single-precision (32-bit) floating-point elements
		/// from memory using 32-bit indices. 32-bit elements are loaded from
		/// addresses starting at address and offset by each 32-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherMaskPs(
			const Vector128 src,
			const float32* address,
			const Vector128 index,
			const Vector128 mask
		)
		{
			return _mm_mask_i32gather_ps(src, address, index, mask, VScale);
		}

		/// @brief	Gather single-precision (32-bit) floating-point elements
		/// from memory using 32-bit indices. 32-bit elements are loaded from
		/// addresses starting at address and offset by each 32-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged into dst using mask (elements are copied
		/// from src when the highest bit is not set in the corresponding element)\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 GatherMaskPs(
			const Vector256 src,
			const float32* address,
			const Vector256 index,
			const Vector256 mask
		)
		{
			return _mm256_mask_i32gather_ps(src, address, index, mask, VScale);
		}

		/// @brief	Gather double-precision (64-bit) floating-point elements
		/// from memory using 32-bit indices. 64-bit elements are loaded from
		/// addresses starting at address and offset by each 32-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherPd(
			const float64* address,
			const Vector128 index
		)
		{
			return _mm_i32gather_pd(address, index, VScale);
		}

		/// @brief	Gather single-precision (32-bit) floating-point elements
		/// from memory using 32-bit indices. 32-bit elements are loaded from
		/// addresses starting at address and offset by each 32-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector128 GatherPs(
			const float32* address,
			const Vector128 index
		)
		{
			return _mm_i32gather_ps(address, index, VScale);
		}

		/// @brief	Gather single-precision (32-bit) floating-point elements
		/// from memory using 32-bit indices. 32-bit elements are loaded from
		/// addresses starting at address and offset by each 32-bit element in
		/// index (each index is scaled by the factor in scale)\n
		/// Gathered elements are merged\n
		/// Scale should be 1, 2, 4 or 8
		template<int32 VScale>
		inline Vector256 GatherPs(
			const float32* address,
			const Vector256 index
		)
		{
			return _mm256_i32gather_ps(address, index, VScale);
		}

		/// @brief	Horizontally add adjacent pairs of 16-bit
		/// integers in a and b, and pack the signed 16-bit results
		inline Vector256 HorizontallyAddEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hadd_epi16(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of 32-bit
		/// integers in a and b, and pack the signed 32-bit results
		inline Vector256 HorizontallyAddEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hadd_epi32(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of 16-bit
		/// integers in a and b using saturation, and pack the
		/// signed 16-bit results
		inline Vector256 HorizontallyAddsEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hadds_epi16(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of 16-bit
		/// integers in a and b, and pack the signed 16-bit results
		inline Vector256 HorizontallySubtractEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hsub_epi16(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of 32-bit
		/// integers in a and b, and pack the signed 32-bit results
		inline Vector256 HorizontallySubtractEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hsub_epi32(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs of 16-bit
		/// integers in a and b using saturation, and pack the
		/// signed 16-bit results
		inline Vector256 HorizontallySubtractsEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_hsubs_epi16(a, b);
		}

		/// @brief	Copy a, then insert 128 bits (composed of
		/// integer data) from b at the location specified by imm8
		template<int32 VImm8>
		inline Vector256 InsertI128Si256(
			const Vector256 a,
			const Vector128 b
		)
		{
			return _mm256_inserti128_si256(a, b, VImm8);
		}

		/// @brief	Load packed 32-bit integers from memory using
		/// mask (elements are zeroed out when the highest
		/// bit is not set in the corresponding element)
		inline Vector128 MaskLoadEpi32(
			const int32* address,
			const Vector128 mask
		)
		{
			return _mm_maskload_epi32(address, mask);
		}

		/// @brief	Load packed 32-bit integers from memory using
		/// mask (elements are zeroed out when the highest
		/// bit is not set in the corresponding element)
		inline Vector256 MaskLoadEpi32(
			const int32* address,
			const Vector256 mask
		)
		{
			return _mm256_maskload_epi32(address, mask);
		}

		/// @brief	Load packed 64-bit integers from memory using
		/// mask (elements are zeroed out when the highest
		/// bit is not set in the corresponding element)
		inline Vector128 MaskLoadEpi64(
			const int64* address,
			const Vector128 mask
		)
		{
			return _mm_maskload_epi64(address, mask);
		}

		/// @brief	Load packed 64-bit integers from memory using
		/// mask (elements are zeroed out when the highest
		/// bit is not set in the corresponding element)
		inline Vector256 MaskLoadEpi64(
			const int64* address,
			const Vector256 mask
		)
		{
			return _mm256_maskload_epi64(address, mask);
		}

		/// @brief	Store packed 32-bit integers from a in memory
		/// using mask (elements are not stored when the highest
		/// bit is not set in the corresponding element)
		inline void MaskStoreEpi32(
			int32* address,
			const Vector128 mask,
			const Vector128 a
		)
		{
			_mm_maskstore_epi32(address, mask, a);
		}

		/// @brief	Store packed 32-bit integers from a in memory
		/// using mask (elements are not stored when the highest
		/// bit is not set in the corresponding element)
		inline void MaskStoreEpi32(
			int32* address,
			const Vector256 mask,
			const Vector256 a
		)
		{
			_mm256_maskstore_epi32(address, mask, a);
		}

		/// @brief	Store packed 64-bit integers from a in memory
		/// using mask (elements are not stored when the highest
		/// bit is not set in the corresponding element)
		inline void MaskStoreEpi64(
			int64* address,
			const Vector128 mask,
			const Vector128 a
		)
		{
			_mm_maskstore_epi64(address, mask, a);
		}

		/// @brief	Store packed 64-bit integers from a in memory
		/// using mask (elements are not stored when the highest
		/// bit is not set in the corresponding element)
		inline void MaskStoreEpi64(
			int64* address,
			const Vector256 mask,
			const Vector256 a
		)
		{
			_mm256_maskstore_epi64(address, mask, a);
		}

		/// @brief	Compare packed signed 16-bit integers
		/// in a and b, and store packed maximum values
		inline Vector256 MaxEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_max_epi16(a, b);
		}

		/// @brief	Compare packed signed 32-bit integers
		/// in a and b, and store packed maximum values
		inline Vector256 MaxEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_max_epi32(a, b);
		}

		/// @brief	Compare packed signed 8-bit integers
		/// in a and b, and store packed maximum values
		inline Vector256 MaxEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_max_epi8(a, b);
		}

		/// @brief	Compare packed unsigned 16-bit integers
		/// in a and b, and store packed maximum values
		inline Vector256 MaxEpu16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_max_epu16(a, b);
		}

		/// @brief	Compare packed unsigned 32-bit integers
		/// in a and b, and store packed maximum values
		inline Vector256 MaxEpu32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_max_epu32(a, b);
		}

		/// @brief	Compare packed unsigned 8-bit integers
		/// in a and b, and store packed maximum values
		inline Vector256 MaxEpu8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_max_epu8(a, b);
		}

		/// @brief	Compare packed signed 16-bit integers
		/// in a and b, and store packed minimum values
		inline Vector256 MinEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_min_epi16(a, b);
		}

		/// @brief	Compare packed signed 32-bit integers
		/// in a and b, and store packed minimum values
		inline Vector256 MinEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_min_epi32(a, b);
		}

		/// @brief	Compare packed signed 8-bit integers
		/// in a and b, and store packed minimum values
		inline Vector256 MinEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_min_epi8(a, b);
		}

		/// @brief	Compare packed unsigned 16-bit integers
		/// in a and b, and store packed minimum values
		inline Vector256 MinEpu16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_min_epu16(a, b);
		}

		/// @brief	Compare packed unsigned 32-bit integers
		/// in a and b, and store packed minimum values
		inline Vector256 MinEpu32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_min_epu32(a, b);
		}

		/// @brief	Compare packed unsigned 8-bit integers
		/// in a and b, and store packed minimum values
		inline Vector256 MinEpu8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_min_epu8(a, b);
		}

		/// @brief	Create mask from the most significant
		/// bit of each 8-bit element in a
		inline int32 MoveMaskEpi8(
			const Vector256 a
		)
		{
			return _mm256_movemask_epi8(a);
		}

		/// @brief	Multiply the low signed 32-bit integers
		/// from each packed 64-bit element in a and b,
		/// and store the signed 64-bit results
		inline Vector256 MultiplyEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mul_epi32(a, b);
		}

		/// @brief	Multiply the low unsigned 32-bit integers
		/// from each packed 64-bit element in a and b,
		/// and store the unsigned 64-bit results
		inline Vector256 MultiplyEpu32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mul_epu32(a, b);
		}

		/// @brief	Multiply the packed signed 16-bit integers
		/// in a and b, producing intermediate 32-bit integers,
		/// and store the high 16 bits of the intermediate integers
		inline Vector256 MultiplyHighEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mulhi_epi16(a, b);
		}

		/// @brief	Multiply the packed unsigned 16-bit integers
		/// in a and b, producing intermediate 32-bit integers,
		/// and store the high 16 bits of the intermediate integers
		inline Vector256 MultiplyHighEpu16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mulhi_epu16(a, b);
		}

		/// @brief	Multiply packed signed 16-bit integers in a
		/// and b, producing intermediate signed 32-bit integers\n
		/// Horizontally add adjacent pairs of intermediate
		/// 32-bit integers, and pack the results
		inline Vector256 MultiplyHorizontallyAddEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_madd_epi16(a, b);
		}

		/// @brief	Multiply the packed signed 16-bit integers
		/// in a and b, producing intermediate 32-bit integers,
		/// and store the low 16 bits of the intermediate integers
		inline Vector256 MultiplyLowEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mullo_epi16(a, b);
		}

		/// @brief	Multiply the packed signed 32-bit integers
		/// in a and b, producing intermediate 64-bit integers,
		/// and store the low 32 bits of the intermediate integers
		inline Vector256 MultiplyLowEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mullo_epi32(a, b);
		}

		/// @brief	Multiply the packed signed 16-bit integers
		/// in a and b, producing intermediate 32-bit integers
		/// Truncate each intermediate integer to the 18 most
		/// significant bits, round by adding 1, and store bits [16:1]
		inline Vector256 MultiplyTruncateEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mulhrs_epi16(a, b);
		}

		/// @brief	Compute the bitwise OR of 256 bits
		/// (representing integer data) in a and b
		inline Vector256 OrSi256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_or_si256(a, b);
		}

		/// @brief	Convert packed signed 16-bit integers from a and
		/// b to packed 8-bit integers using signed saturation
		inline Vector256 PackSaturationEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_packs_epi16(a, b);
		}

		/// @brief	Convert packed signed 32-bit integers from a and
		/// b to packed 16-bit integers using signed saturation
		inline Vector256 PackSaturationEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_packs_epi32(a, b);
		}

		/// @brief	Convert packed signed 16-bit integers from a and
		/// b to packed 8-bit integers using unsigned saturation
		inline Vector256 PackUnsignedSaturationEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_packus_epi16(a, b);
		}

		/// @brief	Convert packed signed 32-bit integers from a and
		/// b to packed 16-bit integers using unsigned saturation
		inline Vector256 PackUnsignedSaturationEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_packus_epi32(a, b);
		}

		/// @brief	Shuffle 128-bits (composed of
		/// integer data) selected by imm8 from a and b
		template<int32 VImm8>
		inline Vector256 Permute2x128Si256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_permute2x128_si256(a, b, VImm8);
		}

		/// @brief	Shuffle 64-bit integers in a
		/// across lanes using the control in imm8
		template<int32 VImm8>
		inline Vector256 Permute4x64Epi64(
			const Vector256 a
		)
		{
			return _mm256_permute4x64_epi64(a, VImm8);
		}

		/// @brief	Shuffle double-precision (64-bit) floating-point
		/// elements in a across lanes using the control in imm8
		template<int32 VImm8>
		inline Vector256 Permute4x64Pd(
			const Vector256 a
		)
		{
			return _mm256_permute4x64_pd(a, VImm8);
		}

		/// @brief	Shuffle 32-bit integers in a across lanes
		/// using the corresponding index in index
		inline Vector256 Permutevar8x32Epi32(
			const Vector256 a,
			const Vector256 index
		)
		{
			return _mm256_permutevar8x32_epi32(a, index);
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a across lanes using the corresponding index in index
		inline Vector256 Permutevar8x32Ps(
			const Vector256 a,
			const Vector256 index
		)
		{
			return _mm256_permutevar8x32_ps(a, index);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a left by count while shifting in zeros
		inline Vector256 ShiftLeftLogicalEpi16(
			const Vector256 a,
			const Vector128 count
		)
		{
			return _mm256_sll_epi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a left by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector256 ShiftLeftLogicalEpi16(
			const Vector256 a
		)
		{
			return _mm256_slli_epi16(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a left by count while shifting in zeros
		inline Vector256 ShiftLeftLogicalEpi32(
			const Vector256 a,
			const Vector128 count
		)
		{
			return _mm256_sll_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a left by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector256 ShiftLeftLogicalEpi32(
			const Vector256 a
		)
		{
			return _mm256_slli_epi32(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in a left
		/// by the amount specified by the corresponding
		/// element in count while shifting in zeros
		inline Vector128 ShiftLeftLogicalEpi32(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_sllv_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in a left
		/// by the amount specified by the corresponding
		/// element in count while shifting in zeros
		inline Vector256 ShiftLeftLogicalEpi32(
			const Vector256 a,
			const Vector256 count
		)
		{
			return _mm256_sllv_epi32(a, count);
		}

		/// @brief	Shift packed 64-bit integers in
		/// a left by count while shifting in zeros
		inline Vector256 ShiftLeftLogicalEpi64(
			const Vector256 a,
			const Vector128 count
		)
		{
			return _mm256_sll_epi64(a, count);
		}

		/// @brief	Shift packed 64-bit integers in
		/// a left by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector256 ShiftLeftLogicalEpi64(
			const Vector256 a
		)
		{
			return _mm256_slli_epi64(a, VImm8);
		}

		/// @brief	Shift packed 64-bit integers in a left
		/// by the amount specified by the corresponding
		/// element in count while shifting in zeros
		inline Vector128 ShiftLeftLogicalEpi64(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_sllv_epi64(a, count);
		}

		/// @brief	Shift packed 64-bit integers in a left
		/// by the amount specified by the corresponding
		/// element in count while shifting in zeros
		inline Vector256 ShiftLeftLogicalEpi64(
			const Vector256 a,
			const Vector256 count
		)
		{
			return _mm256_sllv_epi64(a, count);
		}

		/// @brief	Shift 128-bit lanes in a left
		/// by imm8 bytes while shifting in zeros
		template<int32 VImm8>
		inline Vector256 ShiftLeftLogicalSi256(
			const Vector256 a
		)
		{
			return _mm256_slli_si256(a, VImm8);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a right by count while shifting in sign bits
		inline Vector256 ShiftRightArithmeticEpi16(
			const Vector256 a,
			const Vector128 count
		)
		{
			return _mm256_sra_epi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a right by imm8 while shifting in sign bits
		template<int32 VImm8>
		inline Vector256 ShiftRightArithmeticEpi16(
			const Vector256 a
		)
		{
			return _mm256_srai_epi16(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by count while shifting in sign bits
		inline Vector256 ShiftRightArithmeticEpi32(
			const Vector256 a,
			const Vector128 count
		)
		{
			return _mm256_sra_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by imm8 while shifting in sign bits
		template<int32 VImm8>
		inline Vector256 ShiftRightArithmeticEpi32(
			const Vector256 a
		)
		{
			return _mm256_srai_epi32(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by the amount specified by the
		/// corresponding element in count while
		/// shifting in sign bits
		inline Vector128 ShiftRightArithmeticEpi32(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_srav_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by the amount specified by the
		/// corresponding element in count while
		/// shifting in sign bits
		inline Vector256 ShiftRightArithmeticEpi32(
			const Vector256 a,
			const Vector256 count
		)
		{
			return _mm256_srav_epi32(a, count);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a right by count while shifting in zeros
		inline Vector256 ShiftRightLogicalEpi16(
			const Vector256 a,
			const Vector128 count
		)
		{
			return _mm256_srl_epi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a right by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector256 ShiftRightLogicalEpi16(
			const Vector256 a
		)
		{
			return _mm256_srli_epi16(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by count while shifting in zeros
		inline Vector256 ShiftRightLogicalEpi32(
			const Vector256 a,
			const Vector128 count
		)
		{
			return _mm256_srl_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector256 ShiftRightLogicalEpi32(
			const Vector256 a
		)
		{
			return _mm256_srli_epi32(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in a right
		/// by the amount specified by the corresponding
		/// element in count while shifting in zeros
		inline Vector128 ShiftRightLogicalEpi32(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_srlv_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in a right
		/// by the amount specified by the corresponding
		/// element in count while shifting in zeros
		inline Vector256 ShiftRightLogicalEpi32(
			const Vector256 a,
			const Vector256 count
		)
		{
			return _mm256_srlv_epi32(a, count);
		}

		/// @brief	Shift packed 64-bit integers in
		/// a right by count while shifting in zeros
		inline Vector256 ShiftRightLogicalEpi64(
			const Vector256 a,
			const Vector128 count
		)
		{
			return _mm256_srl_epi64(a, count);
		}

		/// @brief	Shift packed 64-bit integers in
		/// a right by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector256 ShiftRightLogicalEpi64(
			const Vector256 a
		)
		{
			return _mm256_srli_epi64(a, VImm8);
		}

		/// @brief	Shift packed 64-bit integers in a right
		/// by the amount specified by the corresponding
		/// element in count while shifting in zeros
		inline Vector128 ShiftRightLogicalEpi64(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_srlv_epi64(a, count);
		}

		/// @brief	Shift packed 64-bit integers in a right
		/// by the amount specified by the corresponding
		/// element in count while shifting in zeros
		inline Vector256 ShiftRightLogicalEpi64(
			const Vector256 a,
			const Vector256 count
		)
		{
			return _mm256_srlv_epi64(a, count);
		}

		/// @brief	Shift 128-bit lanes in a right
		/// by imm8 bytes while shifting in zeros
		template<int32 VImm8>
		inline Vector256 ShiftRightLogicalSi256(
			const Vector256 a
		)
		{
			return _mm256_srli_si256(a, VImm8);
		}

		/// @brief	Shuffle 32-bit integers in a within
		/// 128-bit lanes using the control in imm8
		template<int32 VImm8>
		inline Vector256 ShuffleEpi32(
			const Vector256 a
		)
		{
			return _mm256_shuffle_epi32(a, VImm8);
		}

		/// @brief	Shuffle 8-bit integers in a within 128-bit lanes according
		/// to shuffle control mask in the corresponding 8-bit element of b
		inline Vector256 ShuffleEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_shuffle_epi8(a, b);
		}

		/// @brief	Shuffle 16-bit integers in the high 64 bits of 128-bit lanes of
		/// a using the control in imm8. Store the results in the high 64 bits of
		/// 128-bit lanes, with the low 64 bits of 128-bit lanes being copied from a
		template<int32 VImm8>
		inline Vector256 ShuffleHighEpi16(
			const Vector256 a
		)
		{
			return _mm256_shufflehi_epi16(a, VImm8);
		}

		/// @brief	Shuffle 16-bit integers in the low 64 bits of 128-bit lanes of
		/// a using the control in imm8. Store the results in the low 64 bits of
		/// 128-bit lanes, with the high 64 bits of 128-bit lanes being copied from a
		template<int32 VImm8>
		inline Vector256 ShuffleLowEpi16(
			const Vector256 a
		)
		{
			return _mm256_shufflelo_epi16(a, VImm8);
		}

		/// @brief	Negate packed signed 16-bit integers in a when the
		/// corresponding signed 16-bit integer in b is negative
		/// Element are zeroed out when the corresponding element in b is zero
		inline Vector256 SignEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sign_epi16(a, b);
		}

		/// @brief	Negate packed signed 32-bit integers in a when the
		/// corresponding signed 32-bit integer in b is negative
		/// Element are zeroed out when the corresponding element in b is zero
		inline Vector256 SignEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sign_epi32(a, b);
		}

		/// @brief	Negate packed signed 8-bit integers in a when the
		/// corresponding signed 8-bit integer in b is negative
		/// Element are zeroed out when the corresponding element in b is zero
		inline Vector256 SignEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sign_epi8(a, b);
		}

		/// @brief	Load 256-bits of integer data from memory
		/// using a non-temporal memory hint\n
		/// Address must be aligned on a 32-byte boundary or a
		/// general-protection exception may be generated
		inline Vector256 StreamLoadSi256(
			const Vector256* address
		)
		{
			return _mm256_stream_load_si256(*address);
		}

		/// @brief	Subtract packed 16-bit integers
		/// in b from packed 16-bit integers in a
		inline Vector256 SubtractEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sub_epi16(a, b);
		}

		/// @brief	Subtract packed 32-bit integers
		/// in b from packed 32-bit integers in a
		inline Vector256 SubtractEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sub_epi32(a, b);
		}

		/// @brief	Subtract packed 64-bit integers
		/// in b from packed 64-bit integers in a
		inline Vector256 SubtractEpi64(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sub_epi64(a, b);
		}

		/// @brief	Subtract packed 8-bit integers
		/// in b from packed 8-bit integers in a
		inline Vector256 SubtractEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sub_epi8(a, b);
		}

		/// @brief	Subtract packed 16-bit integers
		/// in b from packed 16-bit integers in a
		/// using saturation
		inline Vector256 SubtractSaturationEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_subs_epi16(a, b);
		}

		/// @brief	Subtract packed 8-bit integers
		/// in b from packed 8-bit integers in a
		/// using saturation
		inline Vector256 SubtractSaturationEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_subs_epi8(a, b);
		}

		/// @brief	Subtract packed 16-bit integers
		/// in b from packed unsigned 16-bit integers
		/// in a using saturation
		inline Vector256 SubtractSaturationEpu16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_subs_epu16(a, b);
		}

		/// @brief	Subtract packed 8-bit integers
		/// in b from packed unsigned 8-bit integers
		/// in a using saturation
		inline Vector256 SubtractSaturationEpu8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_subs_epu8(a, b);
		}

		/// @brief	Compute the sum of absolute differences (SADs)
		/// of quadruplets of unsigned 8-bit integers in a compared
		/// to those in b, and store the 16-bit results\n
		/// Eight SADs are performed for each 128-bit lane using
		/// one quadruplet from b and eight quadruplets from a\n
		/// One quadruplet is selected from b starting at on the
		/// offset specified in imm8\n
		/// Eight quadruplets are formed from sequential 8-bit integers
		/// selected from a starting at the offset specified in imm8
		template<int32 VImm8>
		inline Vector256 SumAbsDifferenceEpu8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_mpsadbw_epu8(a, b, VImm8);
		}

		/// @brief	Compute the absolute differences of packed unsigned
		/// 8-bit integers in a and b, horizontally sum each consecutive
		/// 8 differences to produce four unsigned 16-bit integers, and pack
		/// these unsigned 16-bit integers in the low 16 bits of 64-bit elements
		inline Vector256 SumAbsoluteDifferenceEpu8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_sad_epu8(a, b);
		}

		/// @brief	Unpack and interleave 16-bit integers
		/// from the high half of each 128-bit lane in
		/// a and b
		inline Vector256 UnpackHighEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpackhi_epi16(a, b);
		}

		/// @brief	Unpack and interleave 32-bit integers
		/// from the high half of each 128-bit lane in
		/// a and b
		inline Vector256 UnpackHighEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpackhi_epi32(a, b);
		}

		/// @brief	Unpack and interleave 64-bit integers
		/// from the high half of each 128-bit lane in
		/// a and b
		inline Vector256 UnpackHighEpi64(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpackhi_epi64(a, b);
		}

		/// @brief	Unpack and interleave 8-bit integers
		/// from the high half of each 128-bit lane in
		/// a and b
		inline Vector256 UnpackHighEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpackhi_epi8(a, b);
		}

		/// @brief	Unpack and interleave 16-bit integers
		/// from the low half of each 128-bit lane in
		/// a and b
		inline Vector256 UnpackLowEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpacklo_epi16(a, b);
		}

		/// @brief	Unpack and interleave 32-bit integers
		/// from the low half of each 128-bit lane in
		/// a and b
		inline Vector256 UnpackLowEpi32(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpacklo_epi32(a, b);
		}

		/// @brief	Unpack and interleave 64-bit integers
		/// from the low half of each 128-bit lane in
		/// a and b
		inline Vector256 UnpackLowEpi64(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpacklo_epi64(a, b);
		}

		/// @brief	Unpack and interleave 8-bit integers
		/// from the low half of each 128-bit lane in
		/// a and b
		inline Vector256 UnpackLowEpi8(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_unpacklo_epi8(a, b);
		}

		/// @brief	Vertically multiply each unsigned 8-bit integer
		/// from a with the corresponding signed 8-bit integer from
		/// b, producing intermediate signed 16-bit integers\n
		/// Horizontally add adjacent pairs of intermediate
		/// 16-bit integers, and pack the saturated results
		inline Vector256 VerticallyMultiplyHorizontallyAddEpi16(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_maddubs_epi16(a, b);
		}

		/// @brief	Compute the bitwise XOR of 256 bits
		/// (representing integer data) in a and b
		inline Vector256 XorSi256(
			const Vector256 a,
			const Vector256 b
		)
		{
			return _mm256_xor_si256(a, b);
		}
	}
}
