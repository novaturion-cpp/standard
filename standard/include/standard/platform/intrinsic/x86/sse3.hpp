﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Sse3
	{
		/// @brief	Alternatively add and subtract packed
		/// double-precision (64-bit) floating-point
		/// elements in a to/from packed elements in b
		inline Vector128 AddSubtractPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_addsub_pd(a, b);
		}

		/// @brief	Alternatively add and subtract packed
		/// single-precision (32-bit) floating-point
		/// elements in a to/from packed elements in b
		inline Vector128 AddSubtractPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_addsub_ps(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of
		/// double-precision (64-bit) floating-point
		/// elements in a and b
		inline Vector128 HorizontallyAddPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hadd_pd(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of
		/// single-precision (32-bit) floating-point
		/// elements in a and b
		inline Vector128 HorizontallyAddPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hadd_ps(a, b);
		}

		/// @brief	Horizontally subtract adjacent pairs
		/// of double-precision (64-bit) floating-point
		/// elements in a and b
		inline Vector128 HorizontallySubtractPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hsub_pd(a, b);
		}

		/// @brief	Horizontally add adjacent pairs of
		/// single-precision (32-bit) floating-point
		/// elements in a and b
		inline Vector128 HorizontallySubtractPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_hsub_ps(a, b);
		}

		/// @brief	Load a double-precision (64-bit)
		/// floating-point element from memory into both elements
		inline Vector128 LoadBothPd(
			const float64* address
		)
		{
			return _mm_loaddup_pd(address);
		}

		/// @brief	Load 128-bits of integer data from unaligned memory
		/// @note	May perform better than LoadUnalignedSi128
		/// when the data crosses a cache line boundary
		inline Vector128 LoadQUnalignedSi128(
			const Vector128* address
		)
		{
			return _mm_lddqu_si128(*address);
		}

		/// @brief	Arm address monitoring hardware
		/// A store to an address within the specified
		/// address range triggers the monitoring hardware
		inline void Monitor(
			const void* address,
			const uint32 extensions,
			const uint32 hints
		)
		{
			_mm_monitor(address, extensions, hints);
		}

		/// @brief	Hint to the processor that it can enter an
		/// implementation-dependent-optimized state while waiting
		/// for an event or store operation to the address range specified by MONITOR
		inline void MonitorWait(
			const uint32 extensions,
			const uint32 hints
		)
		{
			_mm_mwait(extensions, hints);
		}

		/// @brief	Copy odd-indexed single-precision
		/// (32-bit) floating-point elements from a
		inline Vector128 MoveHighPs(
			const Vector128 a
		)
		{
			return _mm_movehdup_ps(a);
		}

		/// @brief	Copy the low double-precision
		/// (64-bit) floating-point element from a
		inline Vector128 MoveLowPd(
			const Vector128 a
		)
		{
			return _mm_movedup_pd(a);
		}

		/// @brief	Copy even-indexed single-precision
		/// (32-bit) floating-point elements from a
		inline Vector128 MoveLowPs(
			const Vector128 a
		)
		{
			return _mm_moveldup_ps(a);
		}
	}
}
