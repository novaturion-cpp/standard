﻿#pragma once

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::XTest
{
	/// @brief	Query the transactional execution status
	/// @return	'true' if inside a transactionally
	/// executing RTM or HLE region
	inline bool XTest()
	{
		return static_cast<bool>(_xtest());
	}
}
