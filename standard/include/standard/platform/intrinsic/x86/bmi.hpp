﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Bmi
{
	/// @brief	Compute the bitwise NOT of
	/// 32-bit integer a and then AND with b
	inline uint32 AndNot(
		const uint32 a,
		const uint32 b
	)
	{
		return _andn_u32(a, b);
	}

	/// @brief	Extract the number of bits specified by
	/// length, starting at the bit specified by start
	inline uint32 Extract(
		const uint32 a,
		const uint32 start,
		const uint32 length
	)
	{
		return _bextr_u32(a, start, length);
	}

	/// @brief	Extract the lowest set bit from
	/// unsigned 32-bit integer a and set the
	/// corresponding bit in return value\n
	/// All other bits in dst are zeroed, and all
	/// bits are zeroed if no bits are set in a
	inline uint32 ExtractLowestSetBit(
		const uint32 a
	)
	{
		return _blsi_u32(a);
	}

	/// @brief	Set all the lower bits of return
	/// value up to and including the lowest
	/// set bit in unsigned 32-bit integer a
	inline uint32 LowestSetBitMask(
		const uint32 a
	)
	{
		return _blsmsk_u32(a);
	}

	/// @brief	Copy all bits from unsigned 32-bit
	/// integer a, and reset (set to 0) the bit that
	/// corresponds to the lowest set bit in a
	inline uint32 ResetLowestSetBit(
		const uint32 a
	)
	{
		return _blsr_u32(a);
	}

	/// @brief	Count the number of trailing
	/// zero bits in unsigned 32-bit integer a
	inline uint32 TrailingZerosCount(
		const uint32 a
	)
	{
		return _tzcnt_u32(a);
	}


	/// @brief	C++ equivalent of AndNot\n
	/// Compute the bitwise NOT of
	/// 32-bit integer a and then AND with b
	constexpr uint32 AndNotEquivalent(
		const uint32 a,
		const uint32 b
	)
	{
		return ~a & b;
	}

	/// @brief	C++ equivalent of Extract\n
	/// Extract the number of bits specified by
	/// length, starting at the bit specified by start
	constexpr uint32 ExtractEquivalent(
		const uint32 a,
		const uint32 start,
		const uint32 length
	)
	{
		return (a >> start) & ((1 << length) - 1);
	}

	/// @brief	C++ equivalent of ExtractLowestSetBit\n
	/// Extract the lowest set bit from
	/// unsigned 32-bit integer a and set the
	/// corresponding bit in return value\n
	/// All other bits in dst are zeroed, and all
	/// bits are zeroed if no bits are set in a
	constexpr uint32 ExtractLowestSetBitEquivalent(
		const uint32 a
	)
	{
		return a & -static_cast<int32>(a);
	}

	/// @brief	C++ equivalent of LowestSetBitMask\n
	/// Set all the lower bits of return
	/// a up to and including the lowest
	/// set bit in unsigned 32-bit integer a
	constexpr uint32 LowestSetBitMaskEquivalent(
		const uint32 a
	)
	{
		return a ^ a - 1;
	}

	/// @brief	C++ equivalent of ResetLowestSetBit\n
	/// Copy all bits from unsigned 32-bit
	/// integer a, and reset (set to 0) the bit that
	/// corresponds to the lowest set bit in a
	constexpr uint32 ResetLowestSetBitEquivalent(
		const uint32 a
	)
	{
		return a & a - 1;
	}

	/// @brief	C++ equivalent of TrailingZerosCount\n
	/// Count the number of trailing
	/// zero bits in unsigned 32-bit integer a
	constexpr uint32 TrailingZerosCountEquivalent(
		const uint32 a
	)
	{
		constexpr auto size = sizeof(decltype(a)) * 8;

		if (!a)
		{
			return size;
		}

		return size - 1
			// check the 16 low order bits in group of 32 bits
			// subtract if result isn't 0
			- ((a & 0x0000FFFF)
				? 16
				: 0)
			// check the 8 low order bits in each group of 16 bits
			// subtract if result isn't 0
			- ((a & 0x00FF00FF)
				? 8
				: 0)
			// check the 4 low order bits in each group of 8 bits
			// subtract if result isn't 0
			- ((a & 0x0F0F0F0F)
				? 4
				: 0)
			// check the 2 low order bits in each group of 4 bits
			// subtract if result isn't 0
			- ((a & 0x33333333)
				? 2
				: 0)
			// check the low order bit in each group of 2 bits
			// subtract if result isn't 0
			- ((a & 0x55555555)
				? 1
				: 0);
	}


	#if ARCH_IS_X64

	/// @brief	Compute the bitwise NOT of
	/// 64-bit integer a and then AND with b
	/// @note   Available only on x64 targets
	inline uint64 AndNot(
		const uint64 a,
		const uint64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _andn_u64(a, b);
	}

	/// @brief	Extract the number of bits specified by
	/// length, starting at the bit specified by start
	/// @note   Available only on x64 targets
	inline uint64 Extract(
		const uint64 a,
		const uint32 start,
		const uint32 length
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _bextr_u64(a, start, length);
	}

	/// @brief	Extract the lowest set bit from
	/// unsigned 64-bit integer a and set the
	/// corresponding bit in return value\n
	/// All other bits in dst are zeroed, and all
	/// bits are zeroed if no bits are set in a
	/// @note   Available only on x64 targets
	inline uint64 ExtractLowestSetBit(
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _blsi_u64(a);
	}

	/// @brief	Set all the lower bits of return
	/// value up to and including the lowest
	/// set bit in unsigned 64-bit integer a
	/// @note   Available only on x64 targets
	inline uint64 LowestSetBitMask(
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _blsmsk_u64(a);
	}

	/// @brief	Copy all bits from unsigned 64-bit
	/// integer a, and reset (set to 0) the bit that
	/// corresponds to the lowest set bit in a
	/// @note   Available only on x64 targets
	inline uint64 ResetLowestSetBit(
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _blsr_u64(a);
	}

	/// @brief	Count the number of trailing
	/// zero bits in unsigned 64-bit integer a
	/// @note   Available only on x64 targets
	inline uint64 TrailingZerosCount(
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _tzcnt_u64(a);
	}

	#endif

	/// @brief	C++ equivalent of AndNot\n
	/// Compute the bitwise NOT of
	/// 64-bit integer a and then AND with b
	constexpr uint64 AndNotEquivalent(
		const uint64 a,
		const uint64 b
	)
	{
		return ~a & b;
	}

	/// @brief	C++ equivalent of Extract\n
	/// Extract the number of bits specified by
	/// length, starting at the bit specified by start
	constexpr uint64 ExtractEquivalent(
		const uint64 a,
		const uint32 start,
		const uint32 length
	)
	{
		return (a >> start) & ((1_ui64 << length) - 1);
	}

	/// @brief	C++ equivalent of ExtractLowestSetBit\n
	/// Extract the lowest set bit from
	/// unsigned 64-bit integer a and set the
	/// corresponding bit in return a\n
	/// All other bits in dst are zeroed, and all
	/// bits are zeroed if no bits are set in a
	constexpr uint64 ExtractLowestSetBitEquivalent(
		const uint64 a
	)
	{
		return a & -static_cast<int64>(a);
	}

	/// @brief	C++ equivalent of LowestSetBitMask\n
	/// Set all the lower bits of return
	/// a up to and including the lowest
	/// set bit in unsigned 64-bit integer a
	constexpr uint64 LowestSetBitMaskEquivalent(
		const uint64 a
	)
	{
		return a ^ a - 1;
	}

	/// @brief	C++ equivalent of ResetLowestSetBit\n
	/// Copy all bits from unsigned 64-bit
	/// integer a, and reset (set to 0) the bit that
	/// corresponds to the lowest set bit in a
	constexpr uint64 ResetLowestSetBitEquivalent(
		const uint64 a
	)
	{
		return a & a - 1;
	}

	/// @brief	C++ equivalent of TrailingZerosCount\n
	/// Count the number of trailing
	/// zero bits in unsigned 64-bit integer a
	constexpr uint64 TrailingZerosCountEquivalent(
		const uint64 a
	)
	{
		constexpr auto size = sizeof(decltype(a)) * 8;

		if (!a)
		{
			return size;
		}

		return size - 1
			// check the 32 low order bits in group of 64 bits
			// subtract if result isn't 0
			- ((a & 0x00000000FFFFFFFF)
				? 32
				: 0)
			// check the 16 low order bits in each group of 32 bits
			// subtract if result isn't 0
			- ((a & 0x0000FFFF0000FFFF)
				? 16
				: 0)
			// check the 8 low order bits in each group of 16 bits
			// subtract if result isn't 0
			- ((a & 0x00FF00FF00FF00FF)
				? 8
				: 0)
			// check the 4 low order bits in each group of 8 bits
			// subtract if result isn't 0
			- ((a & 0x0F0F0F0F0F0F0F0F)
				? 4
				: 0)
			// check the 2 low order bits in each group of 4 bits
			// subtract if result isn't 0
			- ((a & 0x3333333333333333)
				? 2
				: 0)
			// check the low order bit in each group of 2 bits
			// subtract if result isn't 0
			- ((a & 0x5555555555555555)
				? 1
				: 0);
	}
}
