﻿#pragma once
#include "csr.hpp"

#include "standard/platform/intrinsic/type/vector128.hpp"
#include "standard/platform/intrinsic/type/vector64.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Sse
	{
		enum class EMemoryHint: uint8
		{
			Nta = 0,
			T0 = 1,
			T1 = 2,
			T2 = 3,
			Enta = 4,
		};


		/// @brief	Add packed single-precision (32-bit)
		/// floating-point elements in a and b
		inline Vector128 AddPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_add_ps(a, b);
		}

		/// @brief	Add the lower single-precision (32-bit) floating-point
		/// element in a and b, store the result in the lower element, and
		/// copy the upper 3 packed elements from a to the upper elements
		inline Vector128 AddSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_add_ss(a, b);
		}

		/// @brief	Compute the bitwise NOT of packed
		/// single-precision (32-bit) floating-point
		/// elements in a and then AND with b
		inline Vector128 AndNotPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_andnot_ps(a, b);
		}

		/// @brief	Compute the bitwise AND of packed
		/// single-precision (32-bit) floating-point
		/// elements in a and b
		inline Vector128 AndPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_and_ps(a, b);
		}

		/// @brief	Compute the approximate reciprocal of
		/// packed single-precision (32-bit) floating-point
		/// elements in a
		/// @note	The maximum relative error for this
		/// approximation is less than 1.5*2^-12
		inline Vector128 ApproximateReciprocalPs(
			const Vector128 a
		)
		{
			return _mm_rcp_ps(a);
		}

		/// @brief	Compute the approximate reciprocal square root
		/// of packed single-precision (32-bit) floating-point
		/// elements in a
		/// @note	The maximum relative error for this
		/// approximation is less than 1.5*2^-12
		inline Vector128 ApproximateReciprocalSqrtPs(
			const Vector128 a
		)
		{
			return _mm_rsqrt_ps(a);
		}

		/// @brief	Compute the approximate reciprocal square root
		/// of the lower single-precision (32-bit) floating-point
		/// element in a, store the result in the lower element,
		/// and copy the upper 3 packed elements from a to the
		/// upper elements
		/// @note	The maximum relative error for this
		/// approximation is less than 1.5*2^-12
		inline Vector128 ApproximateReciprocalSqrtSs(
			const Vector128 a
		)
		{
			return _mm_rsqrt_ss(a);
		}

		/// @brief	Compute the approximate reciprocal of the lower
		/// single-precision (32-bit) floating-point element in a,
		/// store the result in the lower element, and copy the upper
		/// 3 packed elements from a to the upper elements
		/// @note	The maximum relative error for this
		/// approximation is less than 1.5*2^-12
		inline Vector128 ApproximateReciprocalSs(
			const Vector128 a
		)
		{
			return _mm_rcp_ss(a);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for equality
		inline Vector128 CompareEqualPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpeq_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for equality,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpeq_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for greater-than-or-equal
		inline Vector128 CompareGreaterEqualPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpge_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for greater-than-or-equal,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareGreaterEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpge_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for greater-than
		inline Vector128 CompareGreaterPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpgt_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for greater-than,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareGreaterSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpgt_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for less-than-or-equal
		inline Vector128 CompareLessEqualPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmple_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for less-than-or-equal,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareLessEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmple_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for less-than
		inline Vector128 CompareLessPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmplt_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for less-than,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareLessSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmplt_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for not-equal
		inline Vector128 CompareNotEqualPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpneq_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for not-equal,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareNotEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpneq_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for not-greater-than-or-equal
		inline Vector128 CompareNotGreaterEqualPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnge_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for not-greater-than-or-equal,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareNotGreaterEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnge_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for not-greater-than
		inline Vector128 CompareNotGreaterPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpngt_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for not-greater-than,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareNotGreaterSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpngt_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for not-less-than-or-equal
		inline Vector128 CompareNotLessEqualPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnle_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for not-less-than-or-equal,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareNotLessEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnle_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b for not-less-than
		inline Vector128 CompareNotLessPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnlt_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b for not-less-than,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareNotLessSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnlt_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b to see if neither is NaN
		inline Vector128 CompareOrderedPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpord_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b to see if neither is NaN,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareOrderedSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpord_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit)
		/// floating-point elements in a and b to see if either is NaN
		inline Vector128 CompareUnorderedPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpunord_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point elements in a and b to see if either is NaN,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 CompareUnorderedSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpunord_ss(a, b);
		}

		/// @brief	Convert the signed 32-bit integer b to a
		/// single-precision (32-bit) floating-point element,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 ConvertSi32Ss(
			const Vector128 a,
			const int32 b
		)
		{
			return _mm_cvt_si2ss(a, b);
		}

		/// @brief	Convert the lower single-precision (32-bit)
		/// floating-point element in a to a 32-bit integer
		inline int32 ConvertSsSi32(
			const Vector128 a
		)
		{
			return _mm_cvt_ss2si(a);
		}

		/// @brief	Convert the lower single-precision (32-bit)
		/// floating-point element in a to a 32-bit integer with truncation
		inline int32 ConvertSsSi32Truncation(
			const Vector128 a
		)
		{
			return _mm_cvtt_ss2si(a);
		}

		/// @brief	Divide packed single-precision (32-bit)
		/// floating-point elements in a by packed elements in b
		inline Vector128 DividePs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_div_ps(a, b);
		}

		/// @brief	Divide the lower single-precision (32-bit) floating-point
		/// element in a by the lower single-precision (32-bit) floating-point
		/// element in b, store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 DivideSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_div_ss(a, b);
		}

		/// @brief	Get the unsigned 32-bit value of
		/// the MXCSR control and status register
		inline uint32 GetCsr()
		{
			return _mm_getcsr();
		}

		/// @brief	Get the exception mask bits of
		/// the MXCSR control and status register
		/// @note	Use Csr::EExceptionMask for interpretation
		inline uint32 GetCsrExceptionMask()
		{
			return _mm_getcsr() & Csr::ExceptionMaskMask;
		}

		/// @brief	Get the exception state bits of
		/// the MXCSR control and status register
		/// @note	Use Csr::EExceptionState for interpretation
		inline uint32 GetCsrExceptionState()
		{
			return _mm_getcsr() & Csr::ExceptionStateMask;
		}

		/// @brief	Get the flush zero mode bits of
		/// the MXCSR control and status register
		/// @note	Use Csr::EFlushZeroMode for interpretation
		inline uint32 GetCsrFlushZeroMode()
		{
			return _mm_getcsr() & Csr::FlushZeroModeMask;
		}

		/// @brief	Get the rounding mode bits of
		/// the MXCSR control and status register
		/// @note	Use Csr::ERoundingMode for interpretation
		inline uint32 GetCsrRoundingMode()
		{
			return _mm_getcsr() & Csr::RoundingModeMask;
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for equality
		inline bool IsEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comieq_ss(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for equality
		/// @note	This instruction will not signal an exception for QNaNs
		inline bool IsEqualSsNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomieq_ss(a, b));
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for greater-than-or-equal
		inline bool IsGreaterEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comige_ss(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for greater-than-or-equal
		/// @note	This instruction will not signal an exception for QNaNs
		inline bool IsGreaterEqualSsNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomige_ss(a, b));
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for greater-than
		inline bool IsGreaterSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comigt_ss(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for greater-than
		/// @note	This instruction will not signal an exception for QNaNs
		inline bool IsGreaterSsNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomigt_ss(a, b));
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for less-than-or-equal
		inline bool IsLessEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comile_ss(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for less-than-or-equal
		/// @note	This instruction will not signal an exception for QNaNs
		inline bool IsLessEqualSsNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomile_ss(a, b));
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for less-than
		inline bool IsLessSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comilt_ss(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for less-than
		/// @note	This instruction will not signal an exception for QNaNs
		inline bool IsLessSsNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomilt_ss(a, b));
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for not-equal
		inline bool IsNotEqualSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comineq_ss(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit)
		/// floating-point element in a and b for not-equal
		/// @note	This instruction will not signal an exception for QNaNs
		inline bool IsNotEqualSsNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomineq_ss(a, b));
		}

		/// @brief	Load 2 single-precision (32-bit) floating-point
		/// elements from memory into the upper 2 elements, and copy
		/// the lower 2 elements from a
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline Vector128 LoadHighPi(
			const Vector128 a,
			Vector64* address
		)
		{
			return _mm_loadh_pi(a, *address);
		}

		/// @brief	Load 2 single-precision (32-bit) floating-point
		/// elements from memory into the lower 2 elements, and copy
		/// the upper 2 elements from a
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline Vector128 LoadLowPi(
			const Vector128 a,
			Vector64* address
		)
		{
			return _mm_loadl_pi(a, *address);
		}

		/// @brief	Load 128-bits (composed of 4 packed single-precision
		/// (32-bit) floating-point elements) from memory
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline Vector128 LoadPs(
			const float32* address
		)
		{
			return _mm_load_ps(address);
		}

		/// @brief	Load a single-precision (32-bit) floating-point
		/// element from memory into all elements
		inline Vector128 LoadPs1(
			const float32* address
		)
		{
			return _mm_load_ps1(address);
		}

		/// @brief	Load 4 single-precision (32-bit) floating-point
		/// elements from memory in reverse order
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline Vector128 LoadReversePs(
			const float32* address
		)
		{
			return _mm_loadr_ps(address);
		}

		/// @brief	Load a single-precision (32-bit) floating-point element
		/// from memory into the lower element, and zero the upper 3 elements
		/// @note	Address does not need to be aligned on any particular boundary
		inline Vector128 LoadSs(
			const float32* address
		)
		{
			return _mm_load_ss(address);
		}

		/// @brief	Load 128-bits (composed of 4 packed single-precision
		/// (32-bit) floating-point elements) from memory
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline Vector128 LoadUnalignedPs(
			const float32* address
		)
		{
			return _mm_loadu_ps(address);
		}

		/// @brief	Compare packed single-precision (32-bit) floating-point
		/// elements in a and b, and store packed maximum values
		/// @note	Result does not follow the IEEE Standard for
		/// Floating-Point Arithmetic (IEEE 754) maximum value when
		/// inputs are NaN or signed-zero values
		inline Vector128 MaxPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit) floating-point
		/// elements in a and b, store the maximum value in the lower element,
		/// and copy the upper 3 packed elements from a to the upper element
		/// @note	Result does not follow the IEEE Standard for
		/// Floating-Point Arithmetic (IEEE 754) maximum value when
		/// inputs are NaN or signed-zero values
		inline Vector128 MaxSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_ss(a, b);
		}

		/// @brief	Compare packed single-precision (32-bit) floating-point
		/// elements in a and b, and store packed minimum values
		/// @note	Result does not follow the IEEE Standard for
		/// Floating-Point Arithmetic (IEEE 754) minimum value when
		/// inputs are NaN or signed-zero values
		inline Vector128 MinPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_ps(a, b);
		}

		/// @brief	Compare the lower single-precision (32-bit) floating-point
		/// elements in a and b, store the minimum value in the lower element,
		/// and copy the upper 3 packed elements from a to the upper element
		/// @note	Result does not follow the IEEE Standard for
		/// Floating-Point Arithmetic (IEEE 754) minimum value when
		/// inputs are NaN or signed-zero values
		inline Vector128 MinSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_ss(a, b);
		}

		/// @brief	Move the upper 2 single-precision (32-bit) floating-point
		/// elements from b to the lower 2 elements, and copy the
		/// upper 2 elements from a to the upper 2 elements
		inline Vector128 MoveHighLowPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_movehl_ps(a, b);
		}

		/// @brief	Move the lower 2 single-precision (32-bit) floating-point
		/// elements from b to the upper 2 elements, and copy the
		/// lower 2 elements from a to the lower 2 elements
		inline Vector128 MoveLowHighPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_movelh_ps(a, b);
		}

		/// @brief	Set each bit of mask based on the most
		/// significant bit of the corresponding packed
		/// single-precision (32-bit) floating-point element in a
		inline int32 MoveMaskPs(
			const Vector128 a
		)
		{
			return _mm_movemask_ps(a);
		}

		/// @brief	Move the lower single-precision (32-bit) floating-point
		/// element from b to the lower element, and copy the upper
		/// 3 packed elements from a to the upper elements
		inline Vector128 MoveSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_move_ss(a, b);
		}

		/// @brief	Multiply packed single-precision
		/// (32-bit) floating-point elements in a and b
		inline Vector128 MultiplyPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mul_ps(a, b);
		}

		/// @brief	Multiply the lower single-precision (32-bit) floating-point
		/// element in a and b, store the result in the lower element, and
		/// copy the upper 3 packed elements from a to the upper elements
		inline Vector128 MultiplySs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mul_ss(a, b);
		}

		/// @brief	Compute the bitwise OR of packed single-precision
		/// (32-bit) floating-point elements in a and b
		inline Vector128 OrPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_or_ps(a, b);
		}

		/// @brief	Loads the entire 32- or 64-byte aligned memory sequence
		/// containing the specified memory address into the all data caches
		inline void PrefetchAll(
			const void* address
		)
		{
			_mm_prefetch(static_cast<const char*>(address), 1);
		}

		/// @brief	Loads the entire 32- or 64-byte aligned memory sequence
		/// containing the specified memory address into the L2 and higher data caches
		inline void PrefetchL2(
			const void* address
		)
		{
			_mm_prefetch(static_cast<const char*>(address), 2);
		}

		/// @brief	Loads the entire 32- or 64-byte aligned memory sequence
		/// containing the specified memory address into the L3 and higher data caches
		inline void PrefetchL3(
			const void* address
		)
		{
			_mm_prefetch(static_cast<const char*>(address), 3);
		}

		/// @brief	Loads the entire 32- or 64-byte aligned memory sequence
		/// containing the specified memory address with minimum cache pollution
		/// @note	This is intended for data that will
		/// be used only once, rather than repeatedly
		inline void PrefetchMin(
			const void* address
		)
		{
			_mm_prefetch(static_cast<const char*>(address), 0);
		}

		/// @brief	Loads the entire 32- or 64-byte aligned
		/// memory sequence containing the specified memory address
		/// to an location in the cache hierarchy specified by the hint
		template<EMemoryHint VHint>
		inline void Prefetch(
			const void* address
		)
		{
			_mm_prefetch(
				static_cast<const char*>(address),
				static_cast<int32>(VHint)
			);
		}

		/// @brief	Perform a serializing operation on all store-to-memory
		/// instructions that were issued prior to this instruction\n
		/// Guarantees that every store instruction that precedes,
		/// in program order, is globally visible before any store
		/// instruction which follows the fence in program order
		inline void SerializeStoreFence()
		{
			_mm_sfence();
		}

		/// @brief	Set the MXCSR control and status register
		/// with the value in unsigned 32-bit integer a
		inline void SetCsr(
			const uint32 a
		)
		{
			_mm_setcsr(a);
		}

		/// @brief	Set the exception mask bits of the MXCSR control and
		/// status register to the value in unsigned 32-bit integer a
		/// @note	Use Csr::EExceptionMask
		inline void SetCsrExceptionMask(
			const uint32 mask
		)
		{
			_mm_setcsr((_mm_getcsr() & ~Csr::ExceptionMaskMask) | mask);
		}

		/// @brief	Set the exception state bits of the MXCSR control and
		/// status register to the value in unsigned 32-bit integer a
		/// @note	Use Csr::EExceptionState
		inline void SetCsrExceptionState(
			const uint32 mask
		)
		{
			_mm_setcsr((_mm_getcsr() & ~Csr::ExceptionStateMask) | mask);
		}

		/// @brief	Set the flush zero mode bits of the MXCSR control and
		/// status register to the value in unsigned 32-bit integer a
		/// @note	Use Csr::EFlushZeroMode
		inline void SetCsrFlushZeroMode(
			const uint32 mode
		)
		{
			_mm_setcsr((_mm_getcsr() & ~Csr::FlushZeroModeMask) | mode);
		}

		/// @brief	Set the rounding mode bits of the MXCSR control and
		/// status register to the value in unsigned 32-bit integer a
		/// @note	Use Csr::ERoundingMode
		inline void SetCsrRoundingMode(
			const uint32 mode
		)
		{
			_mm_setcsr((_mm_getcsr() & ~Csr::RoundingModeMask) | mode);
		}

		/// @brief	Set packed single-precision (32-bit)
		/// floating-point elements with the supplied values
		inline Vector128 SetPs(
			const float32 e0,
			const float32 e1,
			const float32 e2,
			const float32 e3
		)
		{
			return _mm_set_ps(e3, e2, e1, e0);
		}

		/// @brief	Broadcast single-precision (32-bit)
		/// floating-point value a to all elements
		inline Vector128 Set1Ps(
			const float32 a
		)
		{
			return _mm_set_ps1(a);
		}

		/// @brief	Set packed single-precision (32-bit) floating-point
		/// elements with the supplied values in reverse order
		inline Vector128 SetReversePs(
			const float32 e0,
			const float32 e1,
			const float32 e2,
			const float32 e3
		)
		{
			return _mm_setr_ps(e3, e2, e1, e0);
		}

		/// @brief	Copy single-precision (32-bit) floating-point
		/// element a to the lower element, and zero the upper 3 elements
		inline Vector128 SetSs(
			const float32 a
		)
		{
			return _mm_set_ss(a);
		}

		/// @brief	Return vector with all elements set to zero
		inline Vector128 SetZeroPs()
		{
			return _mm_setzero_ps();
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a using the control in imm8
		template<int32 VImm8>
		inline Vector128 ShufflePs(
			const Vector128 a
		)
		{
			return _mm_shuffle_ps(a, a, VImm8);
		}

		/// @brief	Shuffle single-precision (32-bit) floating-point
		/// elements in a using the control in imm8
		template<int32 VImm8>
		inline Vector128 ShufflePs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_shuffle_ps(a, b, VImm8);
		}

		/// @brief	Compute the square root of packed single-precision
		/// (32-bit) floating-point elements in a
		inline Vector128 SqrtPs(
			const Vector128 a
		)
		{
			return _mm_sqrt_ps(a);
		}

		/// @brief	Compute the square root of the lower single-precision (32-bit)
		/// floating-point element in a, store the result in the lower element,
		/// and copy the upper 3 packed elements from a to the upper elements
		inline Vector128 SqrtSs(
			const Vector128 a
		)
		{
			return _mm_sqrt_ss(a);
		}

		/// @brief	Store the upper 2 single-precision (32-bit)
		/// floating-point elements from a into memory
		inline void StoreHighPs(
			Vector64* address,
			const Vector128 a
		)
		{
			_mm_storeh_pi(*address, a);
		}

		/// @brief	Store the lower 2 single-precision (32-bit)
		/// floating-point elements from a into memory
		inline void StoreLowPs(
			Vector64* address,
			const Vector128 a
		)
		{
			_mm_storel_pi(*address, a);
		}

		/// @brief	Store 128-bits (composed of 4 packed single-precision
		/// (32-bit) floating-point elements) from a into memory
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StorePs(
			float32* address,
			const Vector128 a
		)
		{
			_mm_store_ps(address, a);
		}

		/// @brief	Store the lower single-precision (32-bit) floating-point
		/// element from a into 4 contiguous elements in memory
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StorePs1(
			float32* address,
			const Vector128 a
		)
		{
			_mm_store_ps1(address, a);
		}

		/// @brief	Store 4 single-precision (32-bit) floating-point
		/// elements from a into memory in reverse order
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StoreReversePs(
			float32* address,
			const Vector128 a
		)
		{
			_mm_storer_ps(address, a);
		}

		/// @brief	Store the lower single-precision (32-bit)
		/// floating-point element from a into memory
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline void StoreSs(
			float32* address,
			const Vector128 a
		)
		{
			_mm_store_ss(address, a);
		}

		/// @brief	Store 128-bits (composed of 4 packed single-precision
		/// (32-bit) floating-point elements) from a into memory
		///	@note	Address does not need to be
		///	aligned on any particular boundary
		inline void StoreUnalignedPs(
			float32* address,
			const Vector128 a
		)
		{
			_mm_storeu_ps(address, a);
		}

		/// @brief	Store 128-bits (composed of 4 packed single-precision (32-bit)
		/// floating-point elements) from a into memory using a non-temporal memory hint
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StreamPs(
			float32* address,
			const Vector128 a
		)
		{
			_mm_stream_ps(address, a);
		}

		/// @brief	Subtract packed single-precision (32-bit) floating-point elements
		/// in b from packed single-precision (32-bit) floating-point elements in a
		inline Vector128 SubtractPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sub_ps(a, b);
		}

		/// @brief	Subtract the lower single-precision (32-bit) floating-point
		/// element in b from the lower single-precision (32-bit) floating-point
		/// element in a, store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		inline Vector128 SubtractSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sub_ss(a, b);
		}

		/// @brief	Unpack and interleave single-precision (32-bit)
		/// floating-point elements from the high half a and b
		inline Vector128 UnpackHighPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpackhi_ps(a, b);
		}

		/// @brief	Unpack and interleave single-precision (32-bit)
		/// floating-point elements from the low half of a and b
		inline Vector128 UnpackLowPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpacklo_ps(a, b);
		}

		/// @brief	Compute the bitwise XOR of packed single-precision
		/// (32-bit) floating-point elements in a and b
		inline Vector128 XorPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_xor_ps(a, b);
		}

		#if ARCH_IS_X86

		/// @brief	Average packed unsigned 16-bit
		/// integers in a and b
		inline Vector64 AverageEpu16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _m_pavgw(a, b);
		}

		/// @brief	Average packed unsigned 8-bit
		/// integers in a and b
		inline Vector64 AverageEpu8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _m_pavgb(a, b);
		}

		/// @brief	Convert packed signed 32-bit integers in b to
		/// packed single-precision (32-bit) floating-point elements,
		/// store the results in the lower 2 elements, and copy the
		/// upper 2 packed elements from a to the upper elements
		inline Vector128 ConvertPiPs(
			const Vector128 a,
			const Vector64 b
		)
		{
			return _mm_cvt_pi2ps(a, b);
		}

		/// @brief	Convert packed single-precision (32-bit)
		/// floating-point elements in a to packed 32-bit integers
		inline Vector64 ConvertPsPi(
			const Vector128 a
		)
		{
			return _mm_cvt_ps2pi(a);
		}

		/// @brief	Convert packed single-precision (32-bit) floating-point
		/// elements in a to packed 32-bit integers with truncation
		inline Vector64 ConvertPsPiTruncation(
			const Vector128 a
		)
		{
			return _mm_cvtt_ps2pi(a);
		}

		/// @brief	Extract a 16-bit integer from a, selected with
		/// imm8, and store the result in the lower element
		template<int32 VImm8>
		inline int32 ExtractEpi16(
			const Vector64 a
		)
		{
			return _m_pextrw(a, VImm8);
		}

		/// @brief	Copy a, and insert the 16-bit integer
		/// i at the location specified by imm8
		template<int32 VImm8>
		inline Vector64 InsertEpi16(
			const Vector64 a,
			const int32 i
		)
		{
			return _m_pinsrw(a, i, VImm8);
		}

		/// @brief	Set each bit of mask based on the most
		/// significant bit of each 8-bit element in a
		inline int32 MaskMoveEpi8(
			const Vector64 a
		)
		{
			return _m_pmovmskb(a);
		}

		/// @brief	Conditionally store 8-bit integer elements from a
		/// into memory using mask (elements are not stored when the
		/// highest bit is not set in the corresponding element)
		inline void MaskStore(
			int8* address,
			const Vector64 a,
			const Vector64 mask
		)
		{
			_m_maskmovq(a, mask, reinterpret_cast<char*>(address));
		}

		/// @brief	Compare packed signed 16-bit integers
		/// in a and b, and store packed maximum values
		inline Vector64 MaxEpi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _m_pmaxsw(a, b);
		}

		/// @brief	Compare packed unsigned 8-bit integers
		/// in a and b, and store packed maximum values
		inline Vector64 MaxEpu8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _m_pmaxub(a, b);
		}


		/// @brief	Compare packed signed 16-bit integers
		/// in a and b, and store packed minimum values
		inline Vector64 MinEpi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _m_pminsw(a, b);
		}

		/// @brief	Compare packed unsigned 8-bit integers
		/// in a and b, and store packed minimum values
		inline Vector64 MinEpu8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _m_pminub(a, b);
		}


		/// @brief	Multiply the packed unsigned 16-bit integers
		/// in a and b, producing intermediate 32-bit integers,
		/// and store the high 16 bits of the intermediate integers
		inline Vector64 MultiplyEpu16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _m_pmulhuw(a, b);
		}


		/// @brief	Shuffle 16-bit integers
		/// in a using the control in imm8
		template<int32 VImm8>
		inline Vector64 ShuffleEpi16(
			const Vector64 a
		)
		{
			return _m_pshufw(a, VImm8);
		}


		/// @brief	Store 64-bits of integer data from
		/// a into memory using a non-temporal memory hint
		inline void StreamPi(
			Vector64* address,
			const Vector64 a
		)
		{
			_mm_stream_pi(*address, a);
		}


		/// @brief	Compute the absolute differences of packed unsigned 8-bit
		/// integers in a and b, then horizontally sum each consecutive 8
		/// differences to produce four unsigned 16-bit integers, and
		/// pack these unsigned 16-bit integers in the low 16 bits
		inline Vector64 SumAbsDifferenceEpu8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _m_psadbw(a, b);
		}

		#endif


		#if ARCH_IS_X64

		/// @brief	Convert the signed 64-bit integer b to a
		/// single-precision (32-bit) floating-point element,
		/// store the result in the lower element, and copy the
		/// upper 3 packed elements from a to the upper elements
		/// @note   Available only on x64 targets
		inline Vector128 ConvertSi64Ss(
			const Vector128 a,
			const int64 b
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsi64_ss(a, b);
		}

		/// @brief	Convert the lower single-precision (32-bit)
		/// floating-point element in a to a 64-bit integer
	    /// @note   Available only on x64 targets
		inline int64 ConvertSsSi64(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtss_si64(a);
		}

		#endif
	}
}
