﻿#pragma once
#include "standard/platform/compiler/msvc.hpp"
#include "standard/platform/intrinsic/type/vector128.hpp"
#include "standard/platform/intrinsic/type/vector256.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

#if !COMPILER_IS_MSVC
#error Attempt to include MSVC only header on non-MSVC compiler
#endif

namespace Std::inline Platform::inline Intrinsic::ExtensionMsvc
{
	// todo: move to according files
	#if ARCH_IS_X86

	using JumpBufferType = int32;


	struct JumpState
	{
		uint32 Ebp;
		uint32 Ebx;
		uint32 Edi;
		uint32 Esi;
		uint32 Esp;
		uint32 Eip;
		uint32 Registration;
		uint32 TryLevel;
		uint32 Cookie;
		uint32 UnwindFunc;
		uint32 UnwindData[6];
	};

	#elif ARCH_IS_X64

	using JumpFloat128 = SETJMP_FLOAT128;
	using JumpBufferType = JumpFloat128;


	struct JumpState
	{
		uint64 Frame;
		uint64 Rbx;
		uint64 Rsp;
		uint64 Rbp;
		uint64 Rsi;
		uint64 Rdi;
		uint64 R12;
		uint64 R13;
		uint64 R14;
		uint64 R15;
		uint64 Rip;
		uint32 MxCsr;
		uint16 FpCsr;
		uint16 Spare;

		JumpFloat128 Xmm6;
		JumpFloat128 Xmm7;
		JumpFloat128 Xmm8;
		JumpFloat128 Xmm9;
		JumpFloat128 Xmm10;
		JumpFloat128 Xmm11;
		JumpFloat128 Xmm12;
		JumpFloat128 Xmm13;
		JumpFloat128 Xmm14;
		JumpFloat128 Xmm15;
	};

	#elif ARCH_IS_ARM

	using JumpBufferType = int32

	struct JumpState
	{
		uint32 Frame;

		uint32 R4;
		uint32 R5;
		uint32 R6;
		uint32 R7;
		uint32 R8;
		uint32 R9;
		uint32 R10;
		uint32 R11;

		uint32 Sp;
		uint32 Pc;
		uint32 FpScr;

	/// D8-D15 VFP/NEON registers
		uint64 D[8];
	};

	#elif ARCH_IS_ARM64

	using JumpBufferType = uint64

	struct JumpState
	{
		uint64 Frame;
		uint64 Reserved;
		// Callee saved register
		uint64 X19;
		// Callee saved register
		uint64 X20;
		// Callee saved register
		uint64 X21;
		// Callee saved register
		uint64 X22;
		// Callee saved register
		uint64 X23;
		// Callee saved register
		uint64 X24;
		// Callee saved register
		uint64 X25;
		// Callee saved register
		uint64 X26;
		// Callee saved register
		uint64 X27;
		// Callee saved register
		uint64 X28;
		// X29 frame pointer
		uint64 Fp;
		// X30 link register
		uint64 Lr;
		// X31 stack pointer
		uint64 Sp;
		// Fp control register
		uint32 FpCr;
		// Fp status register
		uint32 FpSr;

		// D8-D15 FP registers
		float128 D[8];
	};

	#endif

	/// @brief	Compute the inverse cosine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 AcosPs(
		const Vector128 a
	)
	{
		return _mm_acos_ps(a);
	}

	/// @brief	Compute the inverse cosine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 AcosPd(
		const Vector128 a
	)
	{
		return _mm_acos_pd(a);
	}

	/// @brief	Compute the inverse cosine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 AcosPs(
		const Vector256 a
	)
	{
		return _mm256_acos_ps(a);
	}

	/// @brief	Compute the inverse cosine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 AcosPd(
		const Vector256 a
	)
	{
		return _mm256_acos_pd(a);
	}

	/// @brief	Compute the inverse hyperbolic cosine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 AcosHyperbolicPs(
		const Vector128 a
	)
	{
		return _mm_acosh_ps(a);
	}

	/// @brief	Compute the inverse hyperbolic cosine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 AcosHyperbolicPd(
		const Vector128 a
	)
	{
		return _mm_acosh_pd(a);
	}

	/// @brief	Compute the inverse hyperbolic cosine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 AcosHyperbolicPs(
		const Vector256 a
	)
	{
		return _mm256_acosh_ps(a);
	}

	/// @brief	Compute the inverse hyperbolic cosine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 AcosHyperbolicPd(
		const Vector256 a
	)
	{
		return _mm256_acosh_pd(a);
	}

	/// @brief	Add unsigned 8-bit integers a and b
	/// with unsigned 8-bit carry flag
	inline void AddCarry(
		uint8* outResult,
		uint8* outFlag,
		const uint8 a,
		const uint8 b,
		const uint8 flag
	)
	{
		*outFlag = _addcarry_u8(flag, a, b, outResult);
	}

	/// @brief	Add unsigned 16-bit integers a and b
	/// with unsigned 8-bit carry flag
	inline void AddCarry(
		uint16* outResult,
		uint8* outFlag,
		const uint16 a,
		const uint16 b,
		const uint8 flag
	)
	{
		*outFlag = _addcarry_u16(flag, a, b, outResult);
	}

	/// @brief	Add unsigned 8-bit borrow flag (carry
	/// flag) to unsigned 32-bit integer b, and subtract
	/// the result from unsigned 32-bit integer a
	inline void AddSubtractBorrow(
		uint8* outResult,
		uint8* outFlag,
		const uint8 a,
		const uint8 b,
		const uint8 flag
	)
	{
		*outFlag = _subborrow_u8(flag, a, b, outResult);
	}

	/// @brief	Add unsigned 8-bit borrow flag (carry
	/// flag) to unsigned 32-bit integer b, and subtract
	/// the result from unsigned 32-bit integer a
	inline void AddSubtractBorrow(
		uint16* outResult,
		uint8* outFlag,
		const uint16 a,
		const uint16 b,
		const uint8 flag
	)
	{
		*outFlag = _subborrow_u16(flag, a, b, outResult);
	}

	/// @brief	Add unsigned 8-bit borrow flag (carry
	/// flag) to unsigned 32-bit integer b, and subtract
	/// the result from unsigned 32-bit integer a
	inline void AddSubtractBorrow(
		uint32* outResult,
		uint8* outFlag,
		const uint32 a,
		const uint32 b,
		const uint8 flag
	)
	{
		*outFlag = _subborrow_u32(flag, a, b, outResult);
	}

	/// @brief	Compute the inverse sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 AsinPs(
		const Vector128 a
	)
	{
		return _mm_asin_ps(a);
	}

	/// @brief	Compute the inverse sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 AsinPd(
		const Vector128 a
	)
	{
		return _mm_asin_pd(a);
	}

	/// @brief	Compute the inverse sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 AsinPs(
		const Vector256 a
	)
	{
		return _mm256_asin_ps(a);
	}

	/// @brief	Compute the inverse sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 AsinPd(
		const Vector256 a
	)
	{
		return _mm256_asin_pd(a);
	}

	/// @brief	Compute the inverse hyperbolic sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 AsinHyperbolicPs(
		const Vector128 a
	)
	{
		return _mm_asinh_ps(a);
	}

	/// @brief	Compute the inverse hyperbolic sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 AsinHyperbolicPd(
		const Vector128 a
	)
	{
		return _mm_asinh_pd(a);
	}

	/// @brief	Compute the inverse hyperbolic sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 AsinHyperbolicPs(
		const Vector256 a
	)
	{
		return _mm256_asinh_ps(a);
	}

	/// @brief	Compute the inverse hyperbolic sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 AsinHyperbolicPd(
		const Vector256 a
	)
	{
		return _mm256_asinh_pd(a);
	}

	template<uintSize... VAlignment>
	inline void AssumeAligned(
		const void* a
	)
	{
		__builtin_assume_aligned(a, VAlignment...);
	}

	/// @brief	Compute the inverse tan of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 AtanPs(
		const Vector128 a
	)
	{
		return _mm_atan_ps(a);
	}

	/// @brief	Compute the inverse tan of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 AtanPd(
		const Vector128 a
	)
	{
		return _mm_atan_pd(a);
	}

	/// @brief	Compute the inverse tan of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 AtanPs(
		const Vector256 a
	)
	{
		return _mm256_atan_ps(a);
	}

	/// @brief	Compute the inverse tan of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 AtanPd(
		const Vector256 a
	)
	{
		return _mm256_atan_pd(a);
	}

	/// @brief	Compute the inverse tan of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 AtanHyperbolicPs(
		const Vector128 a
	)
	{
		return _mm_atanh_ps(a);
	}

	/// @brief	Compute the inverse tan of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 AtanHyperbolicPd(
		const Vector128 a
	)
	{
		return _mm_atanh_pd(a);
	}

	/// @brief	Compute the inverse tan of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 AtanHyperbolicPs(
		const Vector256 a
	)
	{
		return _mm256_atanh_ps(a);
	}

	/// @brief	Compute the inverse tan of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 AtanHyperbolicPd(
		const Vector256 a
	)
	{
		return _mm256_atanh_pd(a);
	}

	/// @brief	Compute the inverse tan of packed single-precision
	/// (32-bit) floating-point elements in a divided
	/// by packed elements in b expressed in radians
	inline Vector128 Atan2Ps(
		const Vector128 a,
		const Vector128 b
	)
	{
		return _mm_atan2_ps(a, b);
	}

	/// @brief	Compute the inverse tan of packed single-precision
	/// (64-bit) floating-point elements in a divided
	/// by packed elements in b expressed in radians
	inline Vector128 Atan2Pd(
		const Vector128 a,
		const Vector128 b
	)
	{
		return _mm_atan2_pd(a, b);
	}

	/// @brief	Compute the inverse tan of packed single-precision
	/// (32-bit) floating-point elements in a divided
	/// by packed elements in b expressed in radians
	inline Vector256 Atan2Ps(
		const Vector256 a,
		const Vector256 b
	)
	{
		return _mm256_atan2_ps(a, b);
	}

	/// @brief	Compute the inverse tan of packed single-precision
	/// (64-bit) floating-point elements in a divided
	/// by packed elements in b expressed in radians
	inline Vector256 Atan2Pd(
		const Vector256 a,
		const Vector256 b
	)
	{
		return _mm256_atan2_pd(a, b);
	}

	/// @brief	Set outIndex to the outIndex of
	/// the lowest set bit in 32-bit integer mask
	/// @return	'false' if no bits are set
	inline bool BitScanForward(
		uint32* outIndex,
		const uint32 a
	)
	{
		return static_cast<bool>(_BitScanForward(reinterpret_cast<unsigned long*>(outIndex), a));
	}

	/// @brief	Set outIndex to the outIndex of
	/// the highest set bit in 32-bit integer mask
	/// @return	'false' if no bits are set
	inline bool BitScanReverse(
		uint32* outIndex,
		const uint32 a
	)
	{
		return static_cast<bool>(_BitScanReverse(reinterpret_cast<unsigned long*>(outIndex), a));
	}

	/// @brief	Return the bit at
	/// index of 32-bit integer a
	inline uint8 BitTest(
		const int32* a,
		const int32 index
	)
	{
		return _bittest(reinterpret_cast<const long*>(a), index);
	}

	/// @brief	Return the bit at index of 32-bit
	/// integer a, and set that bit to its complement
	inline uint8 BitTestAndComplement(
		int32* a,
		const int32 index
	)
	{
		return _bittestandcomplement(reinterpret_cast<long*>(a), index);
	}

	/// @brief	Return the bit at index of 32-bit
	/// integer a, and set that bit to zero
	inline uint8 BitTestAndReset(
		int32* a,
		const int32 index
	)
	{
		return _bittestandreset(reinterpret_cast<long*>(a), index);
	}

	/// @brief	Return the bit at index of 32-bit
	/// integer a, and set that bit to one
	inline uint8 BitTestAndSet(
		int32* a,
		const int32 index
	)
	{
		return _bittestandset(reinterpret_cast<long*>(a), index);
	}

	/// @brief	Reverse the order of bytes
	inline uint16 ByteSwap(
		const uint16 a
	)
	{
		return _byteswap_ushort(a);
	}

	/// @brief	Reverse the order of bytes
	inline uint32 ByteSwap(
		const uint32 a
	)
	{
		return _byteswap_ulong(a);
	}

	inline float32 Ceil(
		const float32 a
	)
	{
		return __ceilf(a);
	}

	inline float64 Ceil(
		const float64 a
	)
	{
		return __ceil(a);
	}

	/// @brief	Clear AC Flag in EFLAGS register
	inline void ClearAc()
	{
		_clac();
	}

	inline void ClearGlobalInterruptFlag()
	{
		__svm_clgi();
	}

	/// @brief	Clear Task-Switched Flag in CR0
	inline void ClearTaskSwitchedFlag()
	{
		__clts();
	}

	/// @brief	Convert float32 to int32 use an
	/// ARM processor-compatible saturation strategy
	inline int32 ConvertFloat32Int32Saturation(
		const float32 value
	)
	{
		return _cvt_ftoi_sat(value);
	}

	/// @brief	Convert float64 to uint64 use an Intel
	/// Architecture (IA) AVX-512 compatible sentinel strategy
	inline int32 ConvertFloat32Int32Sentinel(
		const float32 value
	)
	{
		return _cvt_ftoi_sent(value);
	}

	/// @brief	Convert float32 to int64 use an
	/// ARM processor-compatible saturation strategy
	inline int64 ConvertFloat32Int64Saturation(
		const float32 value
	)
	{
		return _cvt_ftoll_sat(value);
	}

	/// @brief	Convert float64 to uint64 use an Intel
	/// Architecture (IA) AVX-512 compatible sentinel strategy
	inline int64 ConvertFloat32Int64Sentinel(
		const float32 value
	)
	{
		return _cvt_ftoll_sent(value);
	}

	/// @brief	Convert float32 to uint32 use an
	/// ARM processor-compatible saturation strategy
	inline uint32 ConvertFloat32Uint32Saturation(
		const float32 value
	)
	{
		return _cvt_ftoui_sat(value);
	}

	/// @brief	Convert float64 to uint64 use an Intel
	/// Architecture (IA) AVX-512 compatible sentinel strategy
	inline uint32 ConvertFloat32Uint32Sentinel(
		const float32 value
	)
	{
		return _cvt_ftoui_sent(value);
	}

	/// @brief	Convert float32 to uint64 use an
	/// ARM processor-compatible saturation strategy
	inline uint64 ConvertFloat32Uint64Saturation(
		const float32 value
	)
	{
		return _cvt_ftoull_sat(value);
	}

	/// @brief	Convert float64 to uint64 use an Intel
	/// Architecture (IA) AVX-512 compatible sentinel strategy
	inline uint64 ConvertFloat32Uint64Sentinel(
		const float32 value
	)
	{
		return _cvt_ftoull_sent(value);
	}

	/// @brief	Convert float64 to int32 use an
	/// ARM processor-compatible saturation strategy
	inline int32 ConvertFloat64Int32Saturation(
		const float64 value
	)
	{
		return _cvt_dtoi_sat(value);
	}

	/// @brief	Convert float64 to uint64 use an Intel
	/// Architecture (IA) AVX-512 compatible sentinel strategy
	inline int32 ConvertFloat64Int32Sentinel(
		const float64 value
	)
	{
		return _cvt_dtoi_sent(value);
	}

	/// @brief	Convert float64 to int64 use an
	/// ARM processor-compatible saturation strategy
	inline int64 ConvertFloat64Int64Saturation(
		const float64 value
	)
	{
		return _cvt_dtoll_sat(value);
	}

	/// @brief	Convert float64 to uint64 use an Intel
	/// Architecture (IA) AVX-512 compatible sentinel strategy
	inline int64 ConvertFloat64Int64Sentinel(
		const float64 value
	)
	{
		return _cvt_dtoll_sent(value);
	}

	/// @brief	Convert float64 to uint32 use an
	/// ARM processor-compatible saturation strategy
	inline uint32 ConvertFloat64Uint32Saturation(
		const float64 value
	)
	{
		return _cvt_dtoui_sat(value);
	}

	/// @brief	Convert float64 to uint64 use an Intel
	/// Architecture (IA) AVX-512 compatible sentinel strategy
	inline uint32 ConvertFloat64Uint32Sentinel(
		const float64 value
	)
	{
		return _cvt_dtoui_sent(value);
	}

	/// @brief	Convert float64 to uint64 use an
	/// ARM processor-compatible saturation strategy
	inline uint64 ConvertFloat64Uint64Saturation(
		const float64 value
	)
	{
		return _cvt_dtoull_sat(value);
	}

	/// @brief	Convert float64 to uint64 use an Intel
	/// Architecture (IA) AVX-512 compatible sentinel strategy
	inline uint64 ConvertFloat64Uint64Sentinel(
		const float64 value
	)
	{
		return _cvt_dtoull_sent(value);
	}

	/// @brief	Save the current state of the program.	
	/// @return	0 after saving the stack environment
	/// If SetJump returns because of an LongJump call,
	/// it returns the value argument of LongJump, or
	/// if the value argument of LongJump is 0, SetJump
	/// returns 1. There's no error return
	inline JumpState* ConvertJumpBufferJumpState(
		JumpBufferType buffer[sizeof(JumpState)]
	)
	{
		return reinterpret_cast<JumpState*>(buffer);
	}

	inline float64 CopySign(
		const float64 a,
		const float64 sign
	)
	{
		return __copysign(a, sign);
	}

	inline float32 CopySign(
		const float32 a,
		const float32 sign
	)
	{
		return __copysignf(a, sign);
	}

	/// @brief	Compute the cosine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 CosPs(
		const Vector128 a
	)
	{
		return _mm_cos_ps(a);
	}

	/// @brief	Compute the cosine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 CosPd(
		const Vector128 a
	)
	{
		return _mm_cos_pd(a);
	}

	/// @brief	Compute the cosine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 CosPs(
		const Vector256 a
	)
	{
		return _mm256_cos_ps(a);
	}

	/// @brief	Compute the cosine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 CosPd(
		const Vector256 a
	)
	{
		return _mm256_cos_pd(a);
	}

	/// @brief	Compute the cosine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 CosHyperbolicPs(
		const Vector128 a
	)
	{
		return _mm_cosh_ps(a);
	}

	/// @brief	Compute the cosine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 CosHyperbolicPd(
		const Vector128 a
	)
	{
		return _mm_cosh_pd(a);
	}

	/// @brief	Compute the cosine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 CosHyperbolicPs(
		const Vector256 a
	)
	{
		return _mm256_cosh_ps(a);
	}

	/// @brief	Compute the cosine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 CosHyperbolicPd(
		const Vector256 a
	)
	{
		return _mm256_cosh_pd(a);
	}

	/// @brief	Cause a breakpoint in code, where
	/// the user will be prompted to run the debugger
	inline void DebugBreak()
	{
		__debugbreak();
	}

	/// @brief	Clear the interrupt flag
	inline void DisableInterrupts()
	{
		_disable();
	}

	inline void Divide(
		int32* outResult,
		int32* outRemainder,
		const int64 dividend,
		const int32 divisor
	)
	{
		*outResult = _div64(dividend, divisor, outRemainder);
	}

	inline void Divide(
		uint32* outResult,
		uint32* outRemainder,
		const uint64 dividend,
		const uint32 divisor
	)
	{
		*outResult = _udiv64(dividend, divisor, outRemainder);
	}

	/// @brief	Set the interrupt flag
	inline void EnableInterrupts()
	{
		_enable();
	}

	/// @brief	Immediately terminate the
	/// calling process with minimum overhead
	/// @param	code	A FAST_FAIL_* symbolic
	/// constant from winnt.h or wdm.h that indicates
	/// the reason for process termination
	inline void FastFail(
		const uint32 code
	)
	{
		__fastfail(code);
	}

	inline float32 Floor(
		const float32 a
	)
	{
		return __floorf(a);
	}

	inline float64 Floor(
		const float64 a
	)
	{
		return __floor(a);
	}

	/// @brief	Get the address of the memory location
	/// that holds the return address of the current function\n
	/// This address may not be used to access other memory
	/// locations (for example, the function's arguments)
	/// @note	When GetAddressOfReturnAddress is used in a
	/// program compiled with /clr, the function containing
	/// the GetAddressOfReturnAddress call is compiled as a native
	/// function. When a function compiled as managed calls into
	/// the function containing GetAddressOfReturnAddress,
	/// GetAddressOfReturnAddress might not behave as expected
	inline void* GetAddressOfReturnAddress()
	{
		return _AddressOfReturnAddress();
	}

	/// @brief	Get the EFLAGS value from the caller's context
	inline uint32 GetCallerEFlags()
	{
		return __getcallerseflags();
	}

	/// @brief	Get the address of the instruction in
	/// the calling function that will be executed after
	/// control returns to the caller
	/// @note	When GetReturnAddress is used in a program
	/// compiled with /clr, the function containing the
	/// GetReturnAddress call will be compiled as a native
	/// function. When a function compiled as managed calls
	/// into the function containing _ReturnAddress,
	/// GetReturnAddress may not behave as expected
	inline void* GetReturnAddress()
	{
		return _ReturnAddress();
	}

	/// @brief	Halt the microprocessor until an enabled interrupt,
	/// a nonmaskable interrupt (NMI), or a reset occurs
	inline void Halt()
	{
		__halt();
	}

	/// @brief	Read a single byte from the specified
	/// port using the IN instruction
	inline uint8 InByte(
		const uint16 port
	)
	{
		return __inbyte(port);
	}

	/// @brief	Read data from the specified port using
	/// the REP INSB instruction
	inline void InByteString(
		uint8* outBuffer,
		const uint16 port,
		const uint32 count
	)
	{
		__inbytestring(port, outBuffer, count);
	}

	/// @brief	Read one double word of data from the
	/// specified port using the IN instruction
	inline uint32 InDword(
		const uint16 port
	)
	{
		return __indword(port);
	}

	/// @brief	Read one double word of data from the
	/// specified port using the IN instruction
	inline void InDwordString(
		uint32* outBuffer,
		const uint16 port,
		const uint32 count
	)
	{
		__indwordstring(port, reinterpret_cast<unsigned long*>(outBuffer), count);
	}

	/// @brief	Read data from the specified port using
	/// the IN instruction
	inline uint16 InWord(
		const uint16 port
	)
	{
		return __inword(port);
	}

	/// @brief	Read data from the specified port using
	/// the REP INSW instruction
	inline void InWordString(
		uint16* outBuffer,
		const uint16 port,
		const uint32 count
	)
	{
		__inwordstring(port, outBuffer, count);
	}

	/// @brief	Initiate the loading of verifiably secure
	/// software, such as a virtual machine monitor
	/// @param	secureLoaderBlock	The 32-bit physical
	/// address of a 64K byte Secure Loader Block (SLB)
	inline void InitSecurityKernel(
		const int32 secureLoaderBlock
	)
	{
		__svm_skinit(secureLoaderBlock);
	}

	/// @brief	Trigger the 2C interrupt
	inline void Int2C()
	{
		__int2c();
	}

	/// @brief	Perform addition of two 32-bit integers
	/// @return	Result of addition
	inline int32 InterlockedAdd(
		volatile int32* a,
		const int32 b
	)
	{
		return _interlockedadd(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform addition of two 64-bit integers
	/// @return	Result of addition
	inline int64 InterlockedAdd(
		volatile int64* a,
		const int64 b
	)
	{
		return _interlockedadd64(a, b);
	}

	/// @brief	Perform bitwise AND operation
	/// @return	Original value of a
	inline int8 InterlockedAnd(
		volatile int8* a,
		const int8 b
	)
	{
		return _InterlockedAnd8(reinterpret_cast<volatile char*>(a), b);
	}

	/// @brief	Perform bitwise AND operation
	/// @return	Original value of a
	inline int16 InterlockedAnd(
		volatile int16* a,
		const int16 b
	)
	{
		return _InterlockedAnd16(a, b);
	}

	/// @brief	Perform bitwise AND operation
	/// @return	Original value of a
	inline int32 InterlockedAnd(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedAnd(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Set bit b of the address a to 0
	/// @return	Original value of a
	inline uint8 InterlockedBitTestAndReset(
		volatile int32* a,
		const int32 b
	)
	{
		return _interlockedbittestandreset(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Set bit b of the address a to 1
	/// @return	Original value of a
	inline uint8 InterlockedBitTestAndSet(
		int32* a,
		const int32 b
	)
	{
		return _interlockedbittestandset(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	inline int8 InterlockedCompareExchange(
		volatile int8* a,
		const int8 b,
		const int8 exchange
	)
	{
		return _InterlockedCompareExchange8(reinterpret_cast<volatile char*>(a), exchange, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	inline int16 InterlockedCompareExchange(
		volatile int16* a,
		const int16 b,
		const int16 exchange
	)
	{
		return _InterlockedCompareExchange16(a, exchange, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	inline int32 InterlockedCompareExchange(
		volatile int32* a,
		const int32 b,
		const int32 exchange
	)
	{
		return _InterlockedCompareExchange(reinterpret_cast<volatile long*>(a), exchange, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	inline int64 InterlockedCompareExchange(
		volatile int64* a,
		const int64 b,
		const int64 exchange
	)
	{
		return _InterlockedCompareExchange64(a, exchange, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	inline void* InterlockedCompareExchangePointer(
		void* volatile* a,
		void* b,
		void* exchange
	)
	{
		return _InterlockedCompareExchangePointer(a, exchange, b);
	}

	inline int16 InterlockedDecrement(
		volatile int16* a
	)
	{
		return _InterlockedDecrement16(a);
	}

	inline int32 InterlockedDecrement(
		volatile int32* a
	)
	{
		return _InterlockedDecrement(reinterpret_cast<volatile long*>(a));
	}

	/// @brief	Copy value to the address a
	/// @return	Initial value of a
	inline int8 InterlockedExchange(
		volatile int8* a,
		const int8 value
	)
	{
		return _InterlockedExchange8(reinterpret_cast<volatile char*>(a), value);
	}

	/// @brief	Copy value to the address a
	/// @return	Initial value of a
	inline int16 InterlockedExchange(
		volatile int16* a,
		const int16 value
	)
	{
		return _InterlockedExchange16(a, value);
	}

	/// @brief	Copy value to the address a
	/// @return	Initial value of a
	inline int32 InterlockedExchange(
		volatile int32* a,
		const int32 value
	)
	{
		return _InterlockedExchange(reinterpret_cast<volatile long*>(a), value);
	}

	/// @brief	Perform addition of two 8-bit integers
	/// @return	Initial value of a
	inline int8 InterlockedExchangeAdd(
		volatile int8* a,
		const int8 value
	)
	{
		return _InterlockedExchangeAdd8(reinterpret_cast<volatile char*>(a), value);
	}

	/// @brief	Perform addition of two 16-bit integers
	/// @return	Initial value of a
	inline int16 InterlockedExchangeAdd(
		volatile int16* a,
		const int16 value
	)
	{
		return _InterlockedExchangeAdd16(a, value);
	}

	/// @brief	Copy value to the address a
	/// @return	Initial value of a
	inline int32 InterlockedExchangeAdd(
		volatile int32* a,
		const int32 value
	)
	{
		return _InterlockedExchangeAdd(reinterpret_cast<volatile long*>(a), value);
	}

	/// @brief	Copy value to the address a
	/// @return	Initial value of a
	inline void* InterlockedExchangePointer(
		void* volatile* a,
		void* value
	)
	{
		return _InterlockedExchangePointer(a, value);
	}

	inline int16 InterlockedIncrement(
		volatile int16* a
	)
	{
		return _InterlockedIncrement16(a);
	}

	inline int32 InterlockedIncrement(
		volatile int32* a
	)
	{
		return _InterlockedIncrement(reinterpret_cast<volatile long*>(a));
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	inline int8 InterlockedOr(
		volatile int8* a,
		const int8 b
	)
	{
		return _InterlockedOr8(reinterpret_cast<volatile char*>(a), b);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	inline int16 InterlockedOr(
		volatile int16* a,
		const int16 b
	)
	{
		return _InterlockedOr16(a, b);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	inline int32 InterlockedOr(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedOr(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise XOR operation
	/// @return	Original value of a
	inline int8 InterlockedXor(
		volatile int8* a,
		const int8 b
	)
	{
		return _InterlockedXor8(reinterpret_cast<volatile char*>(a), b);
	}

	/// @brief	Perform bitwise XOR operation
	/// @return	Original value of a
	inline int16 InterlockedXor(
		volatile int16* a,
		const int16 b
	)
	{
		return _InterlockedXor16(a, b);
	}

	/// @brief	Perform bitwise XOR operation
	/// @return	Original value of a
	inline int32 InterlockedXor(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedXor(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Invalidate the address mapping entry
	/// in the computer's translation look-aside buffer
	/// @param	address	The virtual address
	/// @param	id	Address space identifier of the page
	/// to invalidate
	inline void InvalidateAddressMappingEntry(
		void* address,
		const int32 id
	)
	{
		__svm_invlpga(address, id);
	}

	/// @brief	Invalidate the translation lookaside buffer
	/// (TLB) for the page associated with memory pointed
	/// to by address
	inline void InvalidateTlbPage(
		void* address
	)
	{
		__invlpg(address);
	}

	/// @brief	Count the number of leading bits set to 0
	inline uint16 LeadingZerosCount(
		const uint16 a
	)
	{
		return __lzcnt16(a);
	}

	/// @brief	Count the number of leading bits set to 0
	inline uint32 LeadingZerosCount(
		const uint32 a
	)
	{
		return __lzcnt(a);
	}

	/// @brief	Generate the LSL instruction
	/// @note	If the segment limit can't be retrieved,
	/// this instruction fails. On failure, this instruction
	/// clears the ZF flag and the return value is undefined
	inline uint32 LoadSegmentLimit(
		const uint32 id
	)
	{
		return __segmentlimit(id);
	}

	/// @brief	Restore the stack environment and execution locale
	/// set by a SetJump call and return status to SetJump call
	inline void LongJump(
		JumpBufferType buffer[sizeof(JumpState)],
		const int32 status
	)
	{
		longjmp(buffer, status);
	}

	/// @brief	Perform multiplication that
	/// overflow what a 32-bit integer can hold
	/// @note	May generate MUL instructions
	inline int64 Multiply(
		const int32 a,
		const int32 b
	)
	{
		return __emul(a, b);
	}

	/// @brief	Perform multiplication that
	/// overflow what a 32-bit integer can hold
	/// @note	May generate MUL instructions
	inline uint64 Multiply(
		const uint32 a,
		const uint32 b
	)
	{
		return __emulu(a, b);
	}

	/// @brief	Perform no operation using NOP instruction
	inline void NoOperation()
	{
		__nop();
	}

	/// @brief	Deactivate virtual machine extensions (VMX)
	/// operation in the processor
	inline void Off()
	{
		__vmx_off();
	}

	/// @brief	Send a single byte specified by data out
	/// the I/O port specified by port using OUT instruction
	inline void OutByte(
		const uint16 port,
		const uint8 data
	)
	{
		__outbyte(port, data);
	}

	/// @brief	Send the first count bytes of data pointed
	/// to by buffer to the port specified by port using
	/// the REP OUTSB instruction
	inline void OutByteString(
		uint8* buffer,
		const uint16 port,
		const uint32 count
	)
	{
		__outbytestring(port, buffer, count);
	}

	/// @brief	Send a single byte specified by data out the
	/// I/O port specified by port using OUT instruction
	inline void OutDword(
		const uint16 port,
		const uint32 data
	)
	{
		__outdword(port, data);
	}

	/// @brief	Send the first count bytes of data pointed to by
	/// buffer to the port specified by port using the REP OUTSB instruction
	inline void OutDwordString(
		uint32* buffer,
		const uint16 port,
		const uint32 count
	)
	{
		__outdwordstring(port, reinterpret_cast<unsigned long*>(buffer), count);
	}

	/// @brief	Send a single byte specified by data out the
	/// I/O port specified by port using OUT instruction
	inline void OutWord(
		const uint16 port,
		const uint16 data
	)
	{
		__outword(port, data);
	}

	/// @brief	Send the first count bytes of data pointed to by
	/// buffer to the port specified by port using the REP OUTSB instruction
	inline void OutWordString(
		uint16* buffer,
		const uint16 port,
		const uint32 count
	)
	{
		__outwordstring(port, buffer, count);
	}

	/// @brief	Count the number of bits set to 1
	inline uint16 PopulationCount(
		const uint16 a
	)
	{
		return __popcnt16(a);
	}

	/// @brief	Count the number of bits set to 1
	inline uint32 PopulationCount(
		const uint32 a
	)
	{
		return __popcnt(a);
	}

	/// @brief	Raise an invalid opcode exception
	inline void RaiseUndefinedInstruction()
	{
		__ud2();
	}

	inline uintSize ReadCr0()
	{
		return __readcr0();
	}

	inline uintSize ReadCr2()
	{
		return __readcr2();
	}

	inline uintSize ReadCr3()
	{
		return __readcr3();
	}

	inline uintSize ReadCr4()
	{
		return __readcr4();
	}

	inline uintSize ReadCr8()
	{
		return __readcr8();
	}

	/// @brief	Read the value of the debug register specified
	/// by id
	inline uintSize ReadDr(
		const uint32 id
	)
	{
		return __readdr(id);
	}

	inline uintSize ReadEFlags()
	{
		return __readeflags();
	}

	/// @brief	Read the value of the global descriptor table
	/// register (GDTR) to the specified memory location
	inline void ReadGdt(
		void* outData
	)
	{
		_sgdt(outData);
	}

	/// @brief	Read the value of the interrupt descriptor table
	/// register (IDTR) to the specified memory location
	inline void ReadIdt(
		void* outData
	)
	{
		__sidt(outData);
	}

	/// @brief	Read the value of the model-specific register
	/// specified by id
	inline uint64 ReadMsr(
		const uint32 id
	)
	{
		return __readmsr(id);
	}

	/// @brief	Read the value of the performance monitoring
	/// counter specified by id
	inline uint64 ReadPmc(
		const uint32 id
	)
	{
		return __readpmc(id);
	}

	/// @brief	Get the current 64-bit value
	/// of the processor's time-stamp counter
	inline uint64 ReadTimeStampCounter()
	{
		return __rdtsc();
	}

	/// @brief	Return program control from system management mode
	/// (SMM) to the application program or operating-system procedure
	/// that was interrupted when the processor received an SMM interrupt
	inline void ResumeFromSmm()
	{
		_rsm();
	}

	inline uint8 RotateLeft(
		const uint8 a,
		const uint8 length
	)
	{
		return _rotl8(a, length);
	}

	inline uint16 RotateLeft(
		const uint16 a,
		const uint8 length
	)
	{
		return _rotl16(a, length);
	}

	inline uint32 RotateLeft(
		const uint32 a,
		const int32 length
	)
	{
		return _rotl(a, length);
	}

	inline uint8 RotateRight(
		const uint8 a,
		const uint8 length
	)
	{
		return _rotr8(a, length);
	}

	inline uint16 RotateRight(
		const uint16 a,
		const uint8 length
	)
	{
		return _rotr16(a, length);
	}

	inline uint32 RotateRight(
		const uint32 a,
		const int32 length
	)
	{
		return _rotr(a, length);
	}

	inline float32 Round(
		const float32 a
	)
	{
		return __roundf(a);
	}

	inline float64 Round(
		const float64 a
	)
	{
		return __round(a);
	}

	inline uint32 SegmentLimit(
		const uint32 a
	)
	{
		return __segmentlimit(a);
	}

	/// @brief	Set AC Flag in EFLAGS register
	inline void SetAc()
	{
		_stac();
	}

	inline void SetGlobalInterruptFlag()
	{
		__svm_stgi();
	}

	/// @brief	Save the current state of the program.	
	/// @return	0 after saving the stack environment
	/// If SetJump returns because of an LongJump call,
	/// it returns the value argument of LongJump, or
	/// if the value argument of LongJump is 0, SetJump
	/// returns 1. There's no error return
	inline int32 SetJump(
		JumpBufferType buffer[sizeof(JumpState)]
	)
	{
		return _setjmp(buffer);
	}

	inline uint64 ShiftLeft(
		const uint64 a,
		const int32 count
	)
	{
		return __ll_lshift(a, count);
	}

	inline int64 ShiftRight(
		const int64 a,
		const int32 count
	)
	{
		return __ll_rshift(a, count);
	}

	inline uint64 ShiftRight(
		const uint64 a,
		const int32 count
	)
	{
		return __ull_rshift(a, count);
	}

	/// @brief	Determine if the given
	/// floating point number is negative
	inline bool SignBitValue(
		const float32 a
	)
	{
		return __signbitvaluef(a);
	}

	/// @brief	Determine if the given
	/// floating point number is negative
	inline bool SignBitValue(
		const float64 a
	)
	{
		return __signbitvalue(a);
	}

	/// @brief	Compute the sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 SinPs(
		const Vector128 a
	)
	{
		return _mm_sin_ps(a);
	}

	/// @brief	Compute the sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 SinPd(
		const Vector128 a
	)
	{
		return _mm_sin_pd(a);
	}

	/// @brief	Compute the sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 SinPs(
		const Vector256 a
	)
	{
		return _mm256_sin_ps(a);
	}

	/// @brief	Compute the sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 SinPd(
		const Vector256 a
	)
	{
		return _mm256_sin_pd(a);
	}

	/// @brief	Compute the sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 SinHyperbolicPs(
		const Vector128 a
	)
	{
		return _mm_sinh_ps(a);
	}

	/// @brief	Compute the sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 SinHyperbolicPd(
		const Vector128 a
	)
	{
		return _mm_sinh_pd(a);
	}

	/// @brief	Compute the sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 SinHyperbolicPs(
		const Vector256 a
	)
	{
		return _mm256_sinh_ps(a);
	}

	/// @brief	Compute the sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 SinHyperbolicPd(
		const Vector256 a
	)
	{
		return _mm256_sinh_pd(a);
	}

	/// @brief	Copy value using
	/// REP STOSB instruction
	inline void StoreString(
		uint8* outDestination,
		const uint8 value,
		const uintSize count
	)
	{
		__stosb(outDestination, value, count);
	}

	/// @brief	Copy value using
	/// REP STOSW instruction
	inline void StoreString(
		uint16* outDestination,
		const uint16 value,
		const uintSize count
	)
	{
		__stosw(outDestination, value, count);
	}

	/// @brief	Copy value using
	/// REP STOSD instruction
	inline void StoreString(
		uint32* outDestination,
		const uint32 value,
		const uintSize count
	)
	{
		__stosd(reinterpret_cast<unsigned long*>(outDestination), value, count);
	}

	/// @brief	Ensure that all logical processors in a system
	/// have responded to an InvalidateAddressMappingEntry
	/// previously executed by the current logical processor
	inline void SyncTlb()
	{
		__svm_tlbsync();
	}

	/// @brief	Compute the sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 TanPs(
		const Vector128 a
	)
	{
		return _mm_tan_ps(a);
	}

	/// @brief	Compute the sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 TanPd(
		const Vector128 a
	)
	{
		return _mm_tan_pd(a);
	}

	/// @brief	Compute the sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 TanPs(
		const Vector256 a
	)
	{
		return _mm256_tan_ps(a);
	}

	/// @brief	Compute the sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 TanPd(
		const Vector256 a
	)
	{
		return _mm256_tan_pd(a);
	}

	/// @brief	Compute the sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector128 TanHyperbolicPs(
		const Vector128 a
	)
	{
		return _mm_tanh_ps(a);
	}

	/// @brief	Compute the sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector128 TanHyperbolicPd(
		const Vector128 a
	)
	{
		return _mm_tanh_pd(a);
	}

	/// @brief	Compute the sine of packed single-precision
	/// (32-bit) floating-point elements in a expressed in radians
	inline Vector256 TanHyperbolicPs(
		const Vector256 a
	)
	{
		return _mm256_tanh_ps(a);
	}

	/// @brief	Compute the sine of packed double-precision
	/// (64-bit) floating-point elements in a expressed in radians
	inline Vector256 TanHyperbolicPd(
		const Vector256 a
	)
	{
		return _mm256_tanh_pd(a);
	}

	/// @brief	Count the number of trailing
	/// zero bits in unsigned 32-bit integer a
	inline uint32 TrailingZerosCount(
		const uint32 a
	)
	{
		return _tzcnt_u32(a);
	}

	inline float32 Truncate(
		const float32 a
	)
	{
		return __truncf(a);
	}

	inline float64 Truncate(
		const float64 a
	)
	{
		return __trunc(a);
	}

	/// @brief	Store the pointer to the current virtual-machine
	/// control structure (VMCS) at the specified address
	inline void VmGetVmcsAddress(
		uint64* outVmcbAddress
	)
	{
		__vmx_vmptrst(outVmcbAddress);
	}

	/// @brief	Load a subset of processor state from the
	/// specified virtual machine control block (VMCB)
	inline void VmLoad(
		const uintSize vmcbAddress
	)
	{
		__svm_vmload(vmcbAddress);
	}

	/// @brief	Start execution of the virtual machine
	/// guest code that corresponds to the specified
	/// virtual machine control block (VMCB)
	inline void VmRun(
		const uintSize vmcbAddress
	)
	{
		__svm_vmrun(vmcbAddress);
	}

	/// @brief	Store a subset of processor state in the
	/// specified virtual machine control block (VMCB)
	inline void VmSave(
		const uintSize vmcbAddress
	)
	{
		__svm_vmsave(vmcbAddress);
	}

	/// @brief	Perform memory accesses that
	/// aren't subject to compiler optimizations
	/// @return	The value of the memory location specified by address
	inline int16 VolatileLoad(
		const volatile int16* address
	)
	{
		return __iso_volatile_load16(address);
	}

	/// @brief	Perform memory accesses that
	/// aren't subject to compiler optimizations
	/// @return	The value of the memory location specified by address
	inline int32 VolatileLoad(
		const volatile int32* address
	)
	{
		return __iso_volatile_load32(address);
	}

	/// @brief	Perform memory accesses that
	/// aren't subject to compiler optimizations
	/// @return	The value of the memory location specified by address
	inline int8 VolatileLoad(
		const volatile int8* address
	)
	{
		return __iso_volatile_load8(reinterpret_cast<const volatile char*>(address));
	}

	/// @brief	Perform memory accesses that
	/// aren't subject to compiler optimizations
	inline void VolatileStore(
		volatile int8* address,
		const int8 value
	)
	{
		__iso_volatile_store8(reinterpret_cast<volatile char*>(address), value);
	}

	/// @brief	Perform memory accesses that
	/// aren't subject to compiler optimizations
	inline void VolatileStore(
		volatile int16* address,
		const int16 value
	)
	{
		__iso_volatile_store16(address, value);
	}

	/// @brief	Perform memory accesses that
	/// aren't subject to compiler optimizations
	inline void VolatileStore(
		volatile int32* address,
		const int32 value
	)
	{
		__iso_volatile_store32(address, value);
	}

	/// @brief	Generate the Write Back and Invalidate
	/// Cache (WBINVD) instruction
	inline void WriteBackInvalidate()
	{
		__wbinvd();
	}

	inline void WriteCr0(
		const uintSize value
	)
	{
		__writecr0(value);
	}

	inline void WriteCr3(
		const uintSize value
	)
	{
		__writecr3(value);
	}

	inline void WriteCr4(
		const uintSize value
	)
	{
		__writecr4(value);
	}

	inline void WriteCr8(
		const uintSize value
	)
	{
		__writecr8(value);
	}

	/// @brief	Write the value to the debug register specified
	/// by id
	inline void WriteDr(
		const uint32 id,
		const uintSize value
	)
	{
		__writedr(id, value);
	}

	inline void WriteEFlags(
		const unsigned value
	)
	{
		__writeeflags(value);
	}

	/// @brief	Write the value in the specified memory location
	/// to the global descriptor table register (GDTR)
	inline void WriteGdt(
		void* source
	)
	{
		_lgdt(source);
	}

	/// @brief	Write the value in the specified memory location
	/// to the interrupt descriptor table register (IDTR)
	inline void WriteIdt(
		void* address
	)
	{
		__lidt(address);
	}

	/// @brief	Write the value to the model-specific register
	/// specified by id
	inline void WriteMsr(
		const uint32 id,
		const uint64 value
	)
	{
		__writemsr(id, value);
	}

	// todo: #if ARCH_IS_X86
	//
	// /// @brief	Add a value to a memory location specified by
	// /// an offset relative to the beginning of the FS segment
	// inline void AddFsByte(
	// 	const uint32 offset,
	// 	const uint8 value
	// )
	// {
	// 	__addfsbyte(offset, value);
	// }
	//
	// /// @brief	Add a value to a memory location specified by
	// /// an offset relative to the beginning of the FS segment
	// inline void AddFsWord(
	// 	const uint32 offset,
	// 	const uint16 value
	// )
	// {
	// 	__addfsword(offset, value);
	// }
	//
	// /// @brief	Add a value to a memory location specified by
	// /// an offset relative to the beginning of the FS segment
	// inline void AddFsDword(
	// 	const uint32 offset,
	// 	const uint32 value
	// )
	// {
	// 	__addfsdword(offset, value);
	// }
	//
	// /// @brief	Copy string by byte (8-bit)
	// /// using REP MOVSB instruction
	// inline void CopyString(
	// 	uint8* outDestination,
	// 	const uint8* source,
	// 	const uintSize count
	// )
	// {
	// 	__movsb(outDestination, source, count);
	// }
	//
	// /// @brief	Copy string by double word
	// /// (32-bit) using REP MOVSD instruction
	// inline void CopyString(
	// 	uint32* outDestination,
	// 	const uint32* source,
	// 	const uintSize count
	// )
	// {
	// 	__movsd(
	// 		reinterpret_cast<unsigned long*>(outDestination),
	// 		reinterpret_cast<const unsigned long*>(source),
	// 		count
	// 	);
	// }
	//
	// /// @brief	Copy string by word (16-bit)
	// /// using REP MOVSW instruction
	// inline void CopyString(
	// 	uint16* outDestination,
	// 	const uint16* source,
	// 	const uintSize count
	// )
	// {
	// 	__movsw(outDestination, source, count);
	// }
	//
	// /// @brief	Add one to the value at a memory
	// /// location specified by an offset relative
	// /// to the beginning of the FS segment
	// inline void IncrementFsByte(
	// 	const uint32 offset
	// )
	// {
	// 	__incfsbyte(offset);
	// }
	//
	// /// @brief	Add one to the value at a memory
	// /// location specified by an offset relative
	// /// to the beginning of the FS segment
	// inline void IncrementFsDword(
	// 	const uint32 offset
	// )
	// {
	// 	__incfsdword(offset);
	// }
	//
	// /// @brief	Add one to the value at a memory
	// /// location specified by an offset relative
	// /// to the beginning of the FS segment
	// inline void IncrementFsWord(
	// 	const uint32 offset
	// )
	// {
	// 	__incfsword(offset);
	// }
	//
	// /// @brief	Perform an interlocked addition in
	// /// which the first operand is a 64-bit value
	// inline int32 InterlockedAddLargeStatistic(
	// 	volatile int64* a,
	// 	const int32 b
	// )
	// {
	// 	return _InterlockedAddLargeStatistic(a, b);
	// }
	//
	// /// @brief	Read memory from an location specified by an
	// /// offset relative to the beginning of the FS segment
	// inline uint8 ReadFsByte(
	// 	const uint32 offset
	// )
	// {
	// 	return __readfsbyte(offset);
	// }
	//
	// /// @brief	Read memory from an location specified by an
	// /// offset relative to the beginning of the FS segment
	// inline uint32 ReadFsDword(
	// 	const uint32 offset
	// )
	// {
	// 	return __readfsdword(offset);
	// }
	//
	// /// @brief	Read memory from an location specified by an
	// /// offset relative to the beginning of the FS segment
	// inline uint16 ReadFsWord(
	// 	const uint32 offset
	// )
	// {
	// 	return __readfsword(offset);
	// }
	//
	// /// @brief	Write memory to an location specified by an
	// /// offset relative to the beginning of the FS segment
	// inline void WriteFsByte(
	// 	const uint32 offset,
	// 	const uint8 value
	// )
	// {
	// 	__writefsbyte(offset, value);
	// }
	//
	// /// @brief	Write memory to an location specified by an
	// /// offset relative to the beginning of the FS segment
	// inline void WriteFsDword(
	// 	const uint32 offset,
	// 	const uint32 value
	// )
	// {
	// 	__writefsdword(offset, value);
	// }
	//
	// /// @brief	Write memory to an location specified by an
	// /// offset relative to the beginning of the FS segment
	// inline void WriteFsWord(
	// 	const uint32 offset,
	// 	const uint16 value
	// )
	// {
	// 	__writefsword(offset, value);
	// }
	//
	// #endif


	#if ARCH_IS_X64

	/// @brief	Add a value to a memory location specified by
	/// an offset relative to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline void AddGsByte(
		const uint32 offset,
		const uint8 value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__addgsbyte(offset, value);
	}

	/// @brief	Add a value to a memory location specified by
	/// an offset relative to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline void AddGsDword(
		const uint32 offset,
		const uint32 value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__addgsdword(offset, value);
	}

	/// @brief	Add a value to a memory location specified by
	/// an offset relative to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline void AddGsQword(
		const uint32 offset,
		const uint64 value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__addgsqword(offset, value);
	}

	/// @brief	Add a value to a memory location specified by
	/// an offset relative to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline void AddGsWord(
		const uint32 offset,
		const uint16 value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__addgsword(offset, value);
	}

	/// @brief	Add unsigned 8-bit borrow flag (carry
	/// flag) to unsigned 32-bit integer b, and subtract
	/// the result from unsigned 32-bit integer a
	/// @note   Available only on x64 targets
	inline void AddSubtractBorrow(
		uint64* outResult,
		uint8* outFlag,
		const uint64 a,
		const uint64 b,
		const uint8 flag
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		*outFlag = _subborrow_u64(flag, a, b, outResult);
	}

	/// @brief	Set outIndex to the outIndex of
	/// the lowest set bit in 32-bit integer mask
	/// @return	'false' if no bits are set
	/// @note   Available only on x64 targets
	inline bool BitScanForward(
		uint32* outIndex,
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return static_cast<bool>(_BitScanForward64(reinterpret_cast<unsigned long*>(outIndex), a));
	}

	/// @brief	Set outIndex to the outIndex of
	/// the highest set bit in 32-bit integer mask
	/// @return	'false' if no bits are set
	/// @note   Available only on x64 targets
	inline bool BitScanReverse(
		uint32* outIndex,
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return static_cast<bool>(_BitScanReverse64(reinterpret_cast<unsigned long*>(outIndex), a));
	}

	/// @brief	Return the bit at
	/// index of 32-bit integer a
	/// @note   Available only on x64 targets
	inline uint8 BitTest(
		const int64* a,
		const int32 index
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _bittest64(a, index);
	}

	/// @brief	Return the bit at index of 32-bit
	/// integer a, and set that bit to its complement
	/// @note   Available only on x64 targets
	inline uint8 BitTestAndComplement(
		int64* a,
		const int32 index
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _bittestandcomplement64(a, index);
	}

	/// @brief	Return the bit at index of 32-bit
	/// integer a, and set that bit to zero
	/// @note   Available only on x64 targets
	inline uint8 BitTestAndReset(
		int64* a,
		const int32 index
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _bittestandreset64(a, index);
	}

	/// @brief	Return the bit at index of 32-bit
	/// integer a, and set that bit to one
	/// @note   Available only on x64 targets
	inline uint8 BitTestAndSet(
		int64* a,
		const int32 index
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _bittestandset64(a, index);
	}

	/// @brief	Reverse the order of bytes
	/// @note   Available only on x64 targets
	inline uint64 ByteSwap(
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _byteswap_uint64(a);
	}

	/// @note   Available only on x64 targets
	inline void Divide(
		int64* outResult,
		int64* outRemainder,
		const int64 dividendHigh,
		const int64 dividendLow,
		const int64 divisor
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		*outResult = _div128(dividendHigh, dividendLow, divisor, outRemainder);
	}

	/// @note   Available only on x64 targets
	inline void Divide(
		uint64* outResult,
		uint64* outRemainder,
		const uint64 dividendHigh,
		const uint64 dividendLow,
		const uint64 divisor
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		*outResult = _udiv128(dividendHigh, dividendLow, divisor, outRemainder);
	}

	/// @brief	Guarantee that every previous memory reference,
	/// including both load and store memory references, is
	/// globally visible before any subsequent memory reference
	/// @note   Available only on x64 targets
	inline void FastStoreFence()
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__faststorefence();
	}

	/// @brief	Add one to the value at a memory
	/// location specified by an offset relative
	/// to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline void IncrementFsByte(
		const uint32 offset
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__incgsbyte(offset);
	}

	/// @brief	Add one to the value at a memory
	/// location specified by an offset relative
	/// to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline void IncrementFsDword(
		const uint32 offset
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__incgsdword(offset);
	}

	/// @brief	Add one to the value at a memory
	/// location specified by an offset relative
	/// to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline void IncrementFsWord(
		const uint32 offset
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__incgsword(offset);
	}

	/// @brief	Perform bitwise AND operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int8 InterlockedAndNp(
		volatile int8* a,
		const int8 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedAnd8_np(reinterpret_cast<volatile char*>(a), b);
	}

	/// @brief	Perform bitwise AND operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int16 InterlockedAndNp(
		volatile int16* a,
		const int16 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedAnd16_np(a, b);
	}

	/// @brief	Perform bitwise AND operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int64 InterlockedAnd(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedAnd64(a, b);
	}

	/// @brief	Perform bitwise AND operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int32 InterlockedAndNp(
		volatile int32* a,
		const int32 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedAnd_np(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise AND operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int64 InterlockedAndNp(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedAnd64_np(a, b);
	}

	/// @brief	Set bit b of the address a to 0
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline uint8 InterlockedBitTestAndReset(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _interlockedbittestandreset64(a, b);
	}

	/// @brief	Set bit b of the address a to 1
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline uint8 InterlockedBitTestAndSet(
		int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _interlockedbittestandset64(a, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	/// @note   Available only on x64 targets
	inline uint8 InterlockedCompareExchange128(
		volatile int64* a,
		int64* b,
		const int64 exchangeHigh,
		const int64 exchangeLow
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedCompareExchange128(a, exchangeHigh, exchangeLow, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	/// @note   Available only on x64 targets
	inline uint8 InterlockedCompareExchange128Np(
		volatile int64* a,
		int64* b,
		const int64 exchangeHigh,
		const int64 exchangeLow
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedCompareExchange128_np(a, exchangeHigh, exchangeLow, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	/// @note   Available only on x64 targets
	inline int16 InterlockedCompareExchangeNp(
		volatile int16* a,
		const int16 b,
		const int16 exchange
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedCompareExchange16_np(a, exchange, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	/// @note   Available only on x64 targets
	inline int32 InterlockedCompareExchangeNp(
		volatile int32* a,
		const int32 b,
		const int32 exchange
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedCompareExchange_np(reinterpret_cast<volatile long*>(a), exchange, b);
	}

	/// @brief	Compare a and b, replace a with
	/// exchange if the compared values are equal
	/// @return	Initial value of the a pointer
	/// @note   Available only on x64 targets
	inline int64 InterlockedCompareExchangeNp(
		volatile int64* a,
		const int64 b,
		const int64 exchange
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedCompareExchange64_np(a, exchange, b);
	}

	/// @brief	Copy value to the address a
	/// @return	Initial value of a
	/// @note   Available only on x64 targets
	inline void* InterlockedCompareExchangePointerNp(
		void* volatile* a,
		void* b,
		void* exchange
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedCompareExchangePointer_np(a, exchange, b);
	}

	/// @note   Available only on x64 targets
	inline int64 InterlockedDecrement(
		volatile int64* a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedDecrement64(a);
	}

	/// @brief	Copy value to the address a
	/// @return	Initial value of a
	/// @note   Available only on x64 targets
	inline int64 InterlockedExchange(
		volatile int64* a,
		const int64 value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedExchange64(a, value);
	}

	/// @brief	Copy value to the address a
	/// @return	Initial value of a
	/// @note   Available only on x64 targets
	inline int64 InterlockedExchangeAdd(
		volatile int64* a,
		const int64 value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedExchangeAdd64(a, value);
	}

	/// @note   Available only on x64 targets
	inline int64 InterlockedIncrement(
		volatile int64* a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedIncrement64(a);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int64 InterlockedOr(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedOr64(a, b);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int8 InterlockedOrNp(
		volatile int8* a,
		const int8 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedOr8_np(reinterpret_cast<volatile char*>(a), b);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int16 InterlockedOrNp(
		volatile int16* a,
		const int16 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedOr16_np(a, b);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int32 InterlockedOrNp(
		volatile int32* a,
		const int32 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedOr_np(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int64 InterlockedOrNp(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedOr64_np(a, b);
	}

	/// @brief	Perform bitwise XOR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int64 InterlockedXor(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedXor64(a, b);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int8 InterlockedXorNp(
		volatile int8* a,
		const int8 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedXor8_np(reinterpret_cast<volatile char*>(a), b);
	}

	/// @brief	Perform bitwise XOR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int16 InterlockedXorNp(
		volatile int16* a,
		const int16 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedXor16_np(a, b);
	}

	/// @brief	Perform bitwise OR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int32 InterlockedXorNp(
		volatile int32* a,
		const int32 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedXor_np(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise XOR operation
	/// @return	Original value of a
	/// @note   Available only on x64 targets
	inline int64 InterlockedXorNp(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedXor64_np(a, b);
	}

	/// @brief	Perform multiplication that
	/// overflow what a 32-bit integer can hold
	/// @note	May generate MUL instructions
	/// @note   Available only on x64 targets
	inline int64 Multiply(
		const int64 a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __mulh(a, b);
	}

	/// @brief	Perform multiplication that
	/// overflow what a 32-bit integer can hold
	/// @note	May generate MUL instructions
	/// @note   Available only on x64 targets
	inline void Multiply(
		int64* outResultHigh,
		int64* outResultLow,
		const int32 a,
		const int32 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		*outResultLow = _mul128(a, b, outResultHigh);
	}

	/// @brief	Read memory from an location specified by an
	/// offset relative to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline uint8 ReadFsByte(
		const uint32 offset
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __readgsbyte(offset);
	}

	/// @brief	Read memory from an location specified by an
	/// offset relative to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline uint32 ReadFsDword(
		const uint32 offset
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __readgsdword(offset);
	}

	/// @brief	Read memory from an location specified by an
	/// offset relative to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline uint64 ReadFsQword(
		const uint32 offset
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __readgsqword(offset);
	}

	/// @brief	Read memory from an location specified by an
	/// offset relative to the beginning of the GS segment
	/// @note   Available only on x64 targets
	inline uint16 ReadFsWord(
		const uint32 offset
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __readgsword(offset);
	}

	/// @return	High 64-bits of the result
	/// @note   Available only on x64 targets
	inline uint64 ShiftLeft(
		const uint64 aHigh,
		const uint64 aLow,
		const uint8 count
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __shiftleft128(aHigh, aLow, count);
	}

	/// @return	High 64-bits of the result
	/// @note   Available only on x64 targets
	inline uint64 ShiftRight(
		const uint64 aHigh,
		const uint64 aLow,
		const uint8 count
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __shiftright128(aHigh, aLow, count);
	}

	/// @brief	Copy value using
	/// REP STOSQ instruction
	/// @note   Available only on x64 targets
	inline void StoreString(
		uint64* outDestination,
		const uint64 value,
		const uintSize count
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		__stosq(outDestination, value, count);
	}

	/// @brief	Store 32-bit integer a into memory using
	/// a non-temporal hint to minimize cache pollution
	/// If the cache line containing address is already
	/// in the cache, the cache will be updated
	/// @note   Available only on x64 targets
	inline void StreamSi64x(
		int64* address,
		const int64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		_mm_stream_si64x(address, a);
	}

	/// @brief	Initialize the specified virtual machine control
	/// structure (VMCS) and set its launch state to Clear
	/// @return	0 - The operation succeeded\n
	/// 1 - The operation failed with extended status available
	/// in the VM-instruction error field of the current VMCS\n
	/// 2 - The operation failed without status available\n
	/// @note   Available only on x64 targets
	inline uint8 VmClear(
		uint64* vmcsAddress
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __vmx_vmclear(vmcsAddress);
	}

	/// @brief	Place the calling application in VMX
	/// non-root operation state (VM enter) by using the
	/// current virtual-machine control structure (VMCS)
	/// @return	0 - The operation succeeded\n
	/// 1 - The operation failed with extended status available
	/// in the VM-instruction error field of the current VMCS\n
	/// 2 - The operation failed without status available\n
	/// @note   Available only on x64 targets
	inline uint8 VmLaunch()
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __vmx_vmlaunch();
	}

	/// @brief	Load the pointer to the current virtual-machine
	/// control structure (VMCS) from the specified address
	/// @return	0 - The operation succeeded\n
	/// 1 - The operation failed with extended status available
	/// in the VM-instruction error field of the current VMCS\n
	/// 2 - The operation failed without status available\n
	/// @note   Available only on x64 targets
	inline uint8 VmLoadVmcsPointer(
		uint64* vmcsAddress
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __vmx_vmptrld(vmcsAddress);
	}

	/// @brief	Activate virtual machine extensions
	/// (VMX) operation in the processor
	/// @param	vmxRegion	A pointer to a 64-bit, 4KB-aligned
	/// physical address that points to a VMXON region
	/// @return	0 - The operation succeeded\n
	/// 1 - The operation failed with extended status available
	/// in the VM-instruction error field of the current VMCS\n
	/// 2 - The operation failed without status available\n
	/// @note   Available only on x64 targets
	inline uint8 VmOn(
		uint64* vmxRegion
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __vmx_on(vmxRegion);
	}

	/// @brief	Read a specified field from the current virtual machine
	/// control structure (VMCS) and places it in the specified location
	/// @return	0 - The operation succeeded\n
	/// 1 - The operation failed with extended status available
	/// in the VM-instruction error field of the current VMCS\n
	/// 2 - The operation failed without status available\n
	/// @note   Available only on x64 targets
	inline uint8 VmRead(
		uintSize* outValue,
		const uintSize field
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __vmx_vmread(field, outValue);
	}

	/// @brief	Resume VMX non-root operation by using the
	/// current virtual machine control structure (VMCS)
	/// @return	0 - The operation succeeded\n
	/// 1 - The operation failed with extended status available
	/// in the VM-instruction error field of the current VMCS\n
	/// 2 - The operation failed without status available\n
	/// @note   Available only on x64 targets
	inline uint8 VmResume()
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __vmx_vmresume();
	}

	/// @brief	Write the specified value to the specified field
	/// in the current virtual machine control structure (VMCS)
	/// @return	0 - The operation succeeded\n
	/// 1 - The operation failed with extended status available
	/// in the VM-instruction error field of the current VMCS\n
	/// 2 - The operation failed without status available\n
	/// @note   Available only on x64 targets
	inline uint8 VmWrite(
		const uintSize field,
		const uintSize value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return __vmx_vmwrite(field, value);
	}

	#endif
}
