﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Cpuid
{
	struct Leaf final
	{
		uint32 Eax;
		uint32 Ebx;
		uint32 Ecx;
		uint32 Edx;
	};


	inline bool operator==(
		const Leaf& left,
		const Leaf& right
	)
	{
		return left.Eax == right.Eax
			&& left.Ebx == right.Ebx
			&& left.Ecx == right.Ecx
			&& left.Edx == right.Edx;
	}
}
