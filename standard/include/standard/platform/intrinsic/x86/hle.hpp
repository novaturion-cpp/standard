﻿#pragma once
#include "standard/platform/type/int.hpp"
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Hle
{
	/// @brief	Perform bitwise AND operation
	/// and begin a HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedAndAcquire(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedAnd_HLEAcquire(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise AND operation
	/// and release pending active HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedAndRelease(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedAnd_HLERelease(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Set bit of the address a to
	/// zero and begin a HLE transaction
	/// @return	Bit original value
	inline uint8 InterlockedBitTestResetAcquire(
		int32* a,
		const int32 b
	)
	{
		return _interlockedbittestandreset_HLEAcquire(reinterpret_cast<long*>(a), b);
	}

	/// @brief	Set bit of the address a to zero
	/// and release pending active HLE transaction
	/// @return	Bit original value
	inline uint8 InterlockedBitTestResetRelease(
		int32* a,
		const int32 b
	)
	{
		return _interlockedbittestandreset_HLERelease(reinterpret_cast<long*>(a), b);
	}

	/// @brief	Set bit of the address a
	/// to 1 and begin a HLE transaction
	/// @return	Bit original value
	inline uint8 InterlockedBitTestSetAcquire(
		int32* a,
		const int32 b
	)
	{
		return _interlockedbittestandset_HLEAcquire(reinterpret_cast<long*>(a), b);
	}

	/// @brief	Set bit of the address a to 1
	/// and release pending active HLE transaction
	/// @return	Bit original value
	inline uint8 InterlockedBitTestSetRelease(
		int32* a,
		const int32 b
	)
	{
		return _interlockedbittestandset_HLERelease(reinterpret_cast<long*>(a), b);
	}

	/// @brief	Compare a and b, replace a with exchange if the
	/// compared values are equal and begin a HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedCompareExchangeAcquire(
		volatile int32* a,
		const int32 b,
		const int32 exchange
	)
	{
		return _InterlockedCompareExchange_HLEAcquire(reinterpret_cast<volatile long*>(a), exchange, b);
	}

	/// @brief	Compare a and b, replace a with exchange if the
	/// compared values are equal and begin a HLE transaction
	/// @return	Original value of a
	inline int64 InterlockedCompareExchangeAcquire(
		volatile int64* a,
		const int64 b,
		const int64 exchange
	)
	{
		return _InterlockedCompareExchange64_HLEAcquire(a, exchange, b);
	}

	/// @brief	Compare a and b, replace a with exchange if the
	/// compared values are equal and begin a HLE transaction
	/// @return	Original value of a
	inline void* InterlockedCompareExchangePointerAcquire(
		void* volatile* a,
		void* b,
		void* exchange
	)
	{
		return _InterlockedCompareExchangePointer_HLEAcquire(a, exchange, b);
	}

	/// @brief	Compare a and b, replace a with exchange if the compared
	/// values are equal and release pending active HLE transaction
	/// @return	Original value of a
	inline void* InterlockedCompareExchangePointerRelease(
		void* volatile* a,
		void* b,
		void* exchange
	)
	{
		return _InterlockedCompareExchangePointer_HLERelease(a, exchange, b);
	}

	/// @brief	Compare a and b, replace a with exchange if the compared
	/// values are equal and release pending active HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedCompareExchangeRelease(
		volatile int32* a,
		const int32 b,
		const int32 exchange
	)
	{
		return _InterlockedCompareExchange_HLERelease(reinterpret_cast<volatile long*>(a), exchange, b);
	}

	/// @brief	Compare a and b, replace a with exchange if the compared
	/// values are equal and release pending active HLE transaction
	/// @return	Original value of a
	inline int64 InterlockedCompareExchangeRelease(
		volatile int64* a,
		const int64 b,
		const int64 exchange
	)
	{
		return _InterlockedCompareExchange64_HLERelease(a, exchange, b);
	}

	/// @brief	Copy value to the address
	/// a and begin a HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedExchangeAcquire(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedExchange_HLEAcquire(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform addition of two 32-bit
	/// integers and begin a HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedExchangeAddAcquire(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedExchangeAdd_HLEAcquire(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform addition of two 32-bit integers
	/// and release pending active HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedExchangeAddRelease(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedExchangeAdd_HLERelease(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Copy value to the address
	/// a and begin a HLE transaction
	/// @return	Original value of a
	inline void* InterlockedExchangePointerAcquire(
		void* volatile* a,
		void* b
	)
	{
		return _InterlockedExchangePointer_HLEAcquire(a, b);
	}

	/// @brief	Copy value to the address a and
	/// release pending active HLE transaction
	/// @return	Original value of a
	inline void* InterlockedExchangePointerRelease(
		void* volatile* a,
		void* b
	)
	{
		return _InterlockedExchangePointer_HLERelease(a, b);
	}

	/// @brief	Copy value to the address a and
	/// release pending active HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedExchangeRelease(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedExchange_HLERelease(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise OR operation
	/// and begin a HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedOrAcquire(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedOr_HLEAcquire(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise OR operation
	/// and release pending active HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedOrRelease(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedOr_HLERelease(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise XOR operation
	/// and begin a HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedXorAcquire(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedXor_HLEAcquire(reinterpret_cast<volatile long*>(a), b);
	}

	/// @brief	Perform bitwise XOR operation
	/// and release pending active HLE transaction
	/// @return	Original value of a
	inline int32 InterlockedXorRelease(
		volatile int32* a,
		const int32 b
	)
	{
		return _InterlockedXor_HLERelease(reinterpret_cast<volatile long*>(a), b);
	}

	inline void StorePointerRelease(
		void* volatile* a,
		void* b
	)
	{
		_StorePointer_HLERelease(a, b);
	}

	/// @brief	Store the value b at the address a
	/// and release pending active HLE transaction
	inline void StoreRelease(
		volatile int32* a,
		const int32 b
	)
	{
		_Store_HLERelease(reinterpret_cast<volatile long*>(a), b);
	}


	#if ARCH_IS_X64

	/// @brief	Perform an bitwise AND operation
	/// and begin a HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedAndAcquire(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedAnd64_HLEAcquire(a, b);
	}

	/// @brief	Perform an bitwise AND operation
	/// and release pending active HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedAndRelease(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedAnd64_HLERelease(a, b);
	}

	/// @brief	Set bit of the address a to
	/// zero and begin a HLE transaction
	/// @return	Bit original value
    /// @note   Available only on x64 targets
	inline uint8 InterlockedBitTestResetAcquire(
		int64* a,
		const int64 index
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _interlockedbittestandreset64_HLEAcquire(a, index);
	}

	/// @brief	Set bit of the address a to zero
	/// and release pending active HLE transaction
	/// @return	Bit original value
    /// @note   Available only on x64 targets
	inline uint8 InterlockedBitTestResetRelease(
		int64* a,
		const int64 index
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _interlockedbittestandreset64_HLERelease(a, index);
	}

	/// @brief	Set bit of the address a
	/// to 1 and begin a HLE transaction
	/// @return	Bit original value
    /// @note   Available only on x64 targets
	inline uint8 InterlockedBitTestSetAcquire(
		int64* a,
		const int64 index
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _interlockedbittestandset64_HLEAcquire(a, index);
	}

	/// @brief	Set bit of the address a to 1
	/// and release pending active HLE transaction
	/// @return	Bit original value
    /// @note   Available only on x64 targets
	inline uint8 InterlockedBitTestSetRelease(
		int64* a,
		const int64 index
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _interlockedbittestandset64_HLERelease(a, index);
	}

	/// @brief	Copy value to the address
	/// a and begin a HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedExchangeAcquire(
		volatile int64* a,
		const int64 value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedExchange64_HLEAcquire(a, value);
	}

	/// @brief	Perform an addition of two 64-bit
	/// integers and begin a HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedExchangeAddAcquire(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedExchangeAdd64_HLEAcquire(a, b);
	}

	/// @brief	Perform an addition of two 64-bit integers
	/// and release pending active HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedExchangeAddRelease(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedExchangeAdd64_HLERelease(a, b);
	}

	/// @brief	Copy value to the address a and
	/// release pending active HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedExchangeRelease(
		volatile int64* a,
		const int64 value
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedExchange64_HLERelease(a, value);
	}

	/// @brief	Perform an bitwise OR operation
	/// and begin a HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedOrAcquire(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedOr64_HLEAcquire(a, b);
	}

	/// @brief	Perform an bitwise OR operation
	/// and release pending active HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedOrRelease(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedOr64_HLERelease(a, b);
	}

	/// @brief	Perform an bitwise XOR operation
	/// and begin a HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedXorAcquire(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedXor64_HLEAcquire(a, b);
	}

	/// @brief	Perform an bitwise XOR operation
	/// and release pending active HLE transaction
	/// @return	Original value of a
    /// @note   Available only on x64 targets
	inline int64 InterlockedXorRelease(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _InterlockedXor64_HLERelease(a, b);
	}

	/// @brief	Store the value b at the address a
	/// and release pending active HLE transaction
    /// @note   Available only on x64 targets
	inline void StoreRelease(
		volatile int64* a,
		const int64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		_Store64_HLERelease(a, b);
	}

	#endif
}
