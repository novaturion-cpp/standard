﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Aes
{
	/// @brief	Perform one round of an AES decryption
	/// flow on data (state) in a using the round key
	inline Vector128 Decrypt(
		const Vector128 a,
		const Vector128 key
	)
	{
		return _mm_aesdec_si128(a, key);
	}

	/// @brief	Perform the last round of an AES decryption
	/// flow on data (state) in a using the round key
	inline Vector128 DecryptLast(
		const Vector128 a,
		const Vector128 key
	)
	{
		return _mm_aesdeclast_si128(a, key);
	}

	/// @brief	Perform one round of an AES encryption
	/// flow on data (state) in a using the round key
	inline Vector128 Encrypt(
		const Vector128 a,
		const Vector128 key
	)
	{
		return _mm_aesenc_si128(a, key);
	}

	/// @brief	Perform the last round of an AES encryption
	/// flow on data (state) in a using the round key
	inline Vector128 EncryptLast(
		const Vector128 a,
		const Vector128 key
	)
	{
		return _mm_aesenclast_si128(a, key);
	}

	/// @brief	Perform the InvMixColumns transformation on a
	inline Vector128 InverseMixColumns(
		const Vector128 a
	)
	{
		return _mm_aesimc_si128(a);
	}

	/// @brief	Assist in expanding the AES cipher key by
	/// computing steps towards generating a round key for
	/// encryption cipher using data from a and an 8-bit
	/// round constant imm8
	template<int32 VImm8>
	inline Vector128 KeygenAssist(
		const Vector128 a
	)
	{
		return _mm_aeskeygenassist_si128(a, VImm8);
	}
}
