﻿#pragma once
#include "standard/platform/type/int.hpp"
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Rtm
{
	enum class EAbortStatus : uint8
	{
		Unknown,
		BeginStarted = static_cast<uint8>(-1),
		AbortExplicit = 1,
		AbortRetry = 2,
		AbortConflict = 4,
		AbortCapacity = 8,
		AbortDebug = 16,
		AbortNested = 32,
	};


	/// @brief	Force an RTM abort\n
	/// The EAX register is updated to reflect an XAbort
	/// instruction caused the abort, and the imm8 parameter
	/// will be provided in bits [31:24] of EAX\n
	/// Following an RTM abort, the logical processor resumes
	/// execution at the fallback address computed
	/// through the outermost XBegin instruction
	template<int32 VImm8>
	inline void XAbort()
	{
		_xabort(VImm8);
	}

	constexpr uint8 XAbortCode(
		const uint32 a
	)
	{
		return static_cast<uint8>((a >> 24) & 255);
	}

	/// @brief	Specify the start of an RTM code region\n
	/// If the logical processor was not already in transactional
	/// execution, then this call causes the logical processor to
	/// transition into transactional execution\n
	/// On an RTM abort, the logical processor discards all
	/// architectural register and memory updates performed
	/// during the RTM execution, restores architectural state,
	/// and starts execution beginning at the fallback address
	/// computed from the outermost XBegin instruction\n
	/// @return	EAbortStatus::BeginStarted if continuing inside
	/// transaction. All other codes are aborts
	inline EAbortStatus XBegin()
	{
		const auto result = static_cast<uint8>(_xbegin());
		return result >= static_cast<uint8>(EAbortStatus::Unknown)
			? EAbortStatus::Unknown
			: static_cast<EAbortStatus>(result);
	}

	/// @brief	Specify the end of an RTM code region\n
	/// If this corresponds to the outermost scope, the
	/// logical processor will attempt to commit the
	/// logical processor state atomically\n
	/// If the commit fails, the logical processor
	/// will perform an RTM abort
	inline void XEnd()
	{
		_xend();
	}
}
