﻿#pragma once
#include "standard/platform/intrinsic/type/vector64.hpp"

#if !ARCH_IS_X86
#error Attempt to include x86 only header on non-x86 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Mmx
	{
		/// @brief	Add packed 16-bit
		/// integers in a and b
		inline Vector64 AddPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_add_pi16(a, b);
		}

		/// @brief	Add packed 32-bit
		/// integers in a and b
		inline Vector64 AddPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_add_pi32(a, b);
		}

		/// @brief	Add packed 8-bit
		/// integers in a and b
		inline Vector64 AddPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_add_pi8(a, b);
		}

		/// @brief	Add packed signed 16-bit
		/// integers in a and b using saturation
		inline Vector64 AddSaturationPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_adds_pi16(a, b);
		}

		/// @brief	Add packed signed 8-bit
		/// integers in a and b using saturation
		inline Vector64 AddSaturationPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_adds_pi8(a, b);
		}

		/// @brief	Add packed unsigned 16-bit
		/// integers in a and b using saturation
		inline Vector64 AddSaturationPu16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_adds_pu16(a, b);
		}

		/// @brief	Add packed unsigned 8-bit
		/// integers in a and b using saturation
		inline Vector64 AddSaturationPu8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_adds_pu8(a, b);
		}

		/// @brief	Compute the bitwise NOT of 64 bits
		/// (representing integer data) in a and then AND with b
		inline Vector64 AndNotSi64(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_andnot_si64(a, b);
		}

		/// @brief	Compute the bitwise AND of 64 bits
		/// (representing integer data) in a and b
		inline Vector64 AndSi64(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_and_si64(a, b);
		}

		/// @brief	Compare packed 16-bit
		/// integers in a and b for equality
		inline Vector64 CompareEqualPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_cmpeq_pi16(a, b);
		}

		/// @brief	Compare packed 32-bit
		/// integers in a and b for equality
		inline Vector64 CompareEqualPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_cmpeq_pi32(a, b);
		}

		/// @brief	Compare packed 8-bit
		/// integers in a and b for equality
		inline Vector64 CompareEqualPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_cmpeq_pi8(a, b);
		}

		/// @brief	Compare packed signed 16-bit
		/// integers in a and b for greater-than
		inline Vector64 CompareGreaterPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_cmpgt_pi16(a, b);
		}

		/// @brief	Compare packed signed 32-bit
		/// integers in a and b for greater-than
		inline Vector64 CompareGreaterPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_cmpgt_pi32(a, b);
		}

		/// @brief	Compare packed signed 8-bit
		/// integers in a and b for greater-than
		inline Vector64 CompareGreaterPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_cmpgt_pi8(a, b);
		}

		/// @brief	Copy 32-bit integer a to the lower
		/// elements of vector, and zero the upper element
		inline Vector64 ConvertSi32Si64(
			const int32 a
		)
		{
			return _mm_cvtsi32_si64(a);
		}

		/// @brief	Copy the lower 32-bit
		/// integer from a
		inline int32 ConvertSi64Si32(
			const Vector64 a
		)
		{
			return _mm_cvtsi64_si32(a);
		}

		/// @brief	Empty the MMX state, which marks the x87 FPU
		/// registers as available for use by x87 instructions
		/// @note	This instruction must be used at
		/// the end of all MMX technology procedures
		inline void EmptyState()
		{
			_mm_empty();
		}

		/// @brief	Multiply packed signed 16-bit integers in a
		/// and b, producing intermediate signed 32-bit integers\n
		/// Horizontally add adjacent pairs of intermediate 32-bit
		/// integers
		inline Vector64 MultiplyAddPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_madd_pi16(a, b);
		}

		/// @brief	Multiply the packed signed 16-bit integers in a
		/// and b, producing intermediate 32-bit integers, and store
		/// the high 16 bits of the intermediate integers
		inline Vector64 MultiplyHighPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_mulhi_pi16(a, b);
		}

		/// @brief	Multiply the packed 16-bit integers in a and b,
		/// producing intermediate 32-bit integers, and store the
		/// low 16 bits of the intermediate integers
		inline Vector64 MultiplyLowPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_mullo_pi16(a, b);
		}

		/// @brief	Compute the bitwise OR of 64 bits
		/// (representing integer data) in a and b
		inline Vector64 OrSi64(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_or_si64(a, b);
		}

		/// @brief	Convert packed signed 16-bit integers from a
		/// and b to packed 8-bit integers using signed saturation
		inline Vector64 PackSaturationPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_packs_pi16(a, b);
		}

		/// @brief	Convert packed signed 32-bit integers from a
		/// and b to packed 16-bit integers using signed saturation
		inline Vector64 PackSaturationPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_packs_pi32(a, b);
		}

		/// @brief	Convert packed signed 16-bit integers from a
		/// and b to packed 8-bit integers using unsigned saturation
		inline Vector64 PackUnsignedSaturationPu16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_packs_pu16(a, b);
		}

		/// @brief	Broadcast 16-bit integer
		/// a to all elements of vector
		inline Vector64 Set1Pi16(
			const int16 a
		)
		{
			return _mm_set1_pi16(a);
		}

		/// @brief	Broadcast 32-bit integer
		/// a to all elements of vector
		inline Vector64 Set1Pi32(
			const int32 a
		)
		{
			return _mm_set1_pi32(a);
		}

		/// @brief	Broadcast 8-bit integer
		/// a to all elements of vector
		inline Vector64 Set1Pi8(
			const int8 a
		)
		{
			return _mm_set1_pi8(a);
		}

		/// @brief	Set packed 16-bit integers
		/// in vector with the supplied values
		inline Vector64 SetPi16(
			const int16 e0,
			const int16 e1,
			const int16 e2,
			const int16 e3
		)
		{
			return _mm_set_pi16(e3, e2, e1, e0);
		}

		/// @brief	Set packed 32-bit integers
		/// in vector with the supplied values
		inline Vector64 SetPi32(
			const int32 e0,
			const int32 e1
		)
		{
			return _mm_set_pi32(e1, e0);
		}

		/// @brief	Set packed 8-bit integers
		/// in vector with the supplied values
		inline Vector64 SetPi8(
			const int8 e0,
			const int8 e1,
			const int8 e2,
			const int8 e3,
			const int8 e4,
			const int8 e5,
			const int8 e6,
			const int8 e7
		)
		{
			return _mm_set_pi8(e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed 16-bit integers in vector
		/// with the supplied values in reverse order
		inline Vector64 SetReversePi16(
			const int16 e0,
			const int16 e1,
			const int16 e2,
			const int16 e3
		)
		{
			return _mm_setr_pi16(e3, e2, e1, e0);
		}

		/// @brief	Set packed 32-bit integers in vector
		/// with the supplied values in reverse order
		inline Vector64 SetReversePi32(
			const int32 e0,
			const int32 e1
		)
		{
			return _mm_setr_pi32(e1, e0);
		}

		/// @brief	Set packed 8-bit integers in vector
		/// with the supplied values in reverse order
		inline Vector64 SetReversePi8(
			const int8 e0,
			const int8 e1,
			const int8 e2,
			const int8 e3,
			const int8 e4,
			const int8 e5,
			const int8 e6,
			const int8 e7
		)
		{
			return _mm_setr_pi8(e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Return vector with
		/// all elements set to zero
		inline Vector64 SetZeroSi64()
		{
			return _mm_setzero_si64();
		}

		/// @brief	Shift packed 16-bit integers in
		/// a left by count while shifting in zeros
		inline Vector64 ShiftLeftLogicalPi16(
			const Vector64 a,
			const Vector64 count
		)
		{
			return _mm_sll_pi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a left by imm8 while shifting in zeros
		inline Vector64 ShiftLeftLogicalPi16(
			const Vector64 a,
			const int32 imm8
		)
		{
			return _mm_slli_pi16(a, imm8);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a left by count while shifting in zeros
		inline Vector64 ShiftLeftLogicalPi32(
			const Vector64 a,
			const Vector64 count
		)
		{
			return _mm_sll_pi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a left by imm8 while shifting in zeros
		inline Vector64 ShiftLeftLogicalPi32(
			const Vector64 a,
			const int32 imm8
		)
		{
			return _mm_slli_pi32(a, imm8);
		}

		/// @brief	Shift 64-bit integer a left
		/// by count while shifting in zeros
		inline Vector64 ShiftLeftLogicalSi64(
			const Vector64 a,
			const Vector64 count
		)
		{
			return _mm_sll_si64(a, count);
		}

		/// @brief	Shift 64-bit integer a left
		/// by imm8 while shifting in zeros
		inline Vector64 ShiftLeftLogicalSi64(
			const Vector64 a,
			const int32 imm8
		)
		{
			return _mm_slli_si64(a, imm8);
		}

		/// @brief	Shift packed 16-bit integers in a
		/// right by count while shifting in sign bits
		inline Vector64 ShiftRightArithmeticPi16(
			const Vector64 a,
			const Vector64 count
		)
		{
			return _mm_sra_pi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in a
		/// right by imm8 while shifting in sign bits
		inline Vector64 ShiftRightArithmeticPi16(
			const Vector64 a,
			const int32 imm8
		)
		{
			return _mm_srai_pi16(a, imm8);
		}

		/// @brief	Shift packed 32-bit integers in a
		/// right by count while shifting in sign bits
		inline Vector64 ShiftRightArithmeticPi32(
			const Vector64 a,
			const Vector64 count
		)
		{
			return _mm_sra_pi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in a
		/// right by imm8 while shifting in sign bits
		inline Vector64 ShiftRightArithmeticPi32(
			const Vector64 a,
			const int32 imm8
		)
		{
			return _mm_srai_pi32(a, imm8);
		}

		/// @brief	Shift packed 16-bit integers in a
		/// right by count while shifting in zeros
		inline Vector64 ShiftRightLogicalPi16(
			const Vector64 a,
			const Vector64 count
		)
		{
			return _mm_srl_pi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a right by imm8 while shifting in zeros
		inline Vector64 ShiftRightLogicalPi16(
			const Vector64 a,
			const int32 imm8
		)
		{
			return _mm_srli_pi16(a, imm8);
		}

		/// @brief	Shift packed 32-bit integers in a
		/// right by count while shifting in zeros
		inline Vector64 ShiftRightLogicalPi32(
			const Vector64 a,
			const Vector64 count
		)
		{
			return _mm_srl_pi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by imm8 while shifting in zeros
		inline Vector64 ShiftRightLogicalPi32(
			const Vector64 a,
			const int32 imm8
		)
		{
			return _mm_srli_pi32(a, imm8);
		}

		/// @brief	Shift 64-bit integer a right
		/// by count while shifting in zeros
		inline Vector64 ShiftRightLogicalSi64(
			const Vector64 a,
			const Vector64 count
		)
		{
			return _mm_srl_si64(a, count);
		}

		/// @brief	Shift 64-bit integer a right
		/// by imm8 while shifting in zeros
		inline Vector64 ShiftRightLogicalSi64(
			const Vector64 a,
			const int32 imm8
		)
		{
			return _mm_srli_si64(a, imm8);
		}

		/// @brief	Subtract packed 16-bit integers
		/// in b from packed 16-bit integers in a
		inline Vector64 SubtractPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_sub_pi16(a, b);
		}

		/// @brief	Subtract packed 32-bit integers
		/// in b from packed 32-bit integers in a
		inline Vector64 SubtractPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_sub_pi32(a, b);
		}

		/// @brief	Subtract packed 8-bit integers
		/// in b from packed 8-bit integers in a
		inline Vector64 SubtractPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_sub_pi8(a, b);
		}

		/// @brief	Subtract packed signed 16-bit integers in b
		/// from packed 16-bit integers in a using saturation
		inline Vector64 SubtractSaturationPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_subs_pi16(a, b);
		}

		/// @brief	Subtract packed signed 8-bit integers in b
		/// from packed 8-bit integers in a using saturation
		inline Vector64 SubtractSaturationPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_subs_pi8(a, b);
		}

		/// @brief	Subtract packed unsigned 16-bit integers in b
		/// from packed unsigned 16-bit integers in a using saturation
		inline Vector64 SubtractSaturationPu16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_subs_pu16(a, b);
		}

		/// @brief	Subtract packed unsigned 8-bit integers in b
		/// from packed unsigned 8-bit integers in a using saturation
		inline Vector64 SubtractSaturationPu8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_subs_pu8(a, b);
		}

		/// @brief	Unpack and interleave 16-bit
		/// integers from the high half of a and b
		inline Vector64 UnpackHighPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_unpackhi_pi16(a, b);
		}

		/// @brief	Unpack and interleave 32-bit
		/// integers from the high half of a and b
		inline Vector64 UnpackHighPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_unpackhi_pi32(a, b);
		}

		/// @brief	Unpack and interleave 8-bit
		/// integers from the high half of a and b
		inline Vector64 UnpackHighPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_unpackhi_pi8(a, b);
		}

		/// @brief	Unpack and interleave 16-bit
		/// integers from the low half of a and b
		inline Vector64 UnpackLowPi16(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_unpacklo_pi16(a, b);
		}

		/// @brief	Unpack and interleave 32-bit
		/// integers from the low half of a and b
		inline Vector64 UnpackLowPi32(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_unpacklo_pi32(a, b);
		}

		/// @brief	Unpack and interleave 8-bit
		/// integers from the low half of a and b
		inline Vector64 UnpackLowPi8(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_unpacklo_pi8(a, b);
		}

		/// @brief	Compute the bitwise XOR of 64 bits
		/// (representing integer data) in a and b
		inline Vector64 XorSi64(
			const Vector64 a,
			const Vector64 b
		)
		{
			return _mm_xor_si64(a, b);
		}
	}
}
