﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief
	/// <pre>
	/// Comparison				| A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
	/// ---------------------------------------|-------|-------|-------|---------------|---------------------
	/// EqualOrderedNonSignaling		| False	| False	| True	| False		| No
	/// EqualOrderedSignaling			| False	| False	| True	| False		| Yes
	/// EqualUnorderedNonSignaling		| False	| False	| True	| True		| No
	/// EqualUnorderedSignaling		| False	| False	| True	| True		| Yes
	/// FalseOrderedNonSignaling		| False	| False	| False	| False		| No
	/// FalseOrderedSignaling			| False	| False	| False	| False		| Yes
	/// GreaterEqualOrderedNonSignaling	| True	| False	| True	| False		| No
	/// GreaterEqualOrderedSignaling		| True	| False	| True	| False		| Yes
	/// GreaterOrderedNonSignaling		| True	| False	| False	| False		| No
	/// GreaterOrderedSignaling		| True	| False	| False	| False		| Yes
	/// LessEqualOrderedNonSignaling		| False	| True	| True	| False		| No
	/// LessEqualOrderedSignaling		| False	| True	| True	| False		| Yes
	/// LessOrderedNonSignaling		| False	| True	| False	| False		| No
	/// LessOrderedSignaling			| False	| True	| False	| False		| Yes
	/// NotEqualOrderedNonSignaling		| True	| True	| False	| False		| No
	/// NotEqualOrderedSignaling		| True	| True	| False	| False		| Yes
	/// NotEqualUnorderedNonSignaling		| True	| True	| False	| True		| No
	/// NotEqualUnorderedSignaling		| True	| True	| False	| True		| Yes
	/// NotGreaterEqualUnorderedNonSignaling	| False	| True	| False	| True		| No
	/// NotGreaterEqualUnorderedSignaling	| False	| True	| False	| True		| Yes
	/// NotGreaterUnorderedNonSignaling	| False	| True	| True	| True		| No
	/// NotGreaterUnorderedSignaling		| False	| True	| True	| True		| Yes
	/// NotLessEqualUnorderedNonSignaling	| True	| False	| False	| True		| No
	/// NotLessEqualUnorderedSignaling		| True	| False	| False	| True		| Yes
	/// NotLessUnorderedNonSignaling		| True	| False	| True	| True		| No
	/// NotLessUnorderedSignaling		| True	| False	| True	| True		| Yes
	/// OrderedNonSignaling			| True	| True	| True	| False		| No
	/// OrderedSignaling			| True	| True	| True	| False		| Yes
	/// TrueUnorderedNonSignaling		| True	| True	| True	| True		| No
	/// TrueUnorderedSignaling			| True	| True	| True	| True		| Yes
	/// UnorderedNonSignaling			| False	| False	| False	| True		| No
	/// UnorderedSignaling			| False	| False	| False	| True		| Yes
	///
	/// *If either operand A or B is NAN
	/// </pre>
	enum class EComparisonType : uint8
	{
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| False	| True	| False		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		EqualOrderedNonSignaling = 0,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| False	| True	| False		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		EqualOrderedSignaling = 16,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| False	| True	| True		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		EqualUnorderedNonSignaling = 8,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| False	| True	| True		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		EqualUnorderedSignaling = 24,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| False	| False	| False		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		FalseOrderedNonSignaling = 11,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| False	| False	| False		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		FalseOrderedSignaling = 27,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| False	| True	| False		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		GreaterEqualOrderedNonSignaling = 29,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| False	| True	| False		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		GreaterEqualOrderedSignaling = 13,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| False	| False	| False		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		GreaterOrderedNonSignaling = 30,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| False	| False	| False		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		GreaterOrderedSignaling = 14,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| True	| True	| False		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		LessEqualOrderedNonSignaling = 18,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| True	| True	| False		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		LessEqualOrderedSignaling = 2,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| True	| False	| False		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		LessOrderedNonSignaling = 17,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| True	| False	| False		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		LessOrderedSignaling = 1,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| True	| False	| False		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotEqualOrderedNonSignaling = 12,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| True	| False	| False		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotEqualOrderedSignaling = 28,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| True	| False	| True		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotEqualUnorderedNonSignaling = 4,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| True	| False	| True		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotEqualUnorderedSignaling = 20,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| True	| False	| True		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotGreaterEqualUnorderedNonSignaling = 25,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| True	| False	| True		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotGreaterEqualUnorderedSignaling = 9,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| True	| True	| True		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotGreaterUnorderedNonSignaling = 26,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| True	| True	| True		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotGreaterUnorderedSignaling = 10,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| False	| False	| True		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotLessEqualUnorderedNonSignaling = 22,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| False	| False	| True		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotLessEqualUnorderedSignaling = 6,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| False	| True	| True		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotLessUnorderedNonSignaling = 21,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| False	| True	| True		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		NotLessUnorderedSignaling = 5,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| True	| True	| False		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		OrderedNonSignaling = 7,
		// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| True	| True	| False		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		OrderedSignaling = 23,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| True	| True	| True		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		TrueUnorderedNonSignaling = 15,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// True	| True	| True	| True		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		TrueUnorderedSignaling = 31,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| False	| False	| True		| No
		///
		/// *If either operand A or B is NAN
		/// </pre>
		UnorderedNonSignaling = 3,
		/// @brief
		/// <pre>
		/// A > B	| A < B	| A = B	| Unordered*	| Signals #IA on QNAN
		/// -------|-------|-------|---------------|---------------------
		/// False	| False	| False	| True		| Yes
		///
		/// *If either operand A or B is NAN
		/// </pre>
		UnorderedSignaling = 19,
	};
}
