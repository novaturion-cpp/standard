﻿#pragma once
#include "standard/platform/architecture.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::ClFlush
{
	/// @brief	Invalidate and flush the cache line that
	/// contains a from all levels of the cache hierarchy
	inline void FlushCache(
		const void* a
	)
	{
		_mm_clflush(a);
	}
}
