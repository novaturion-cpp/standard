﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::ClMul
{
	/// @brief	Perform a carry-less multiplication of two
	/// 64-bit integers, selected from a and b according to imm8
	template<int32 VImm8>
	inline Vector128 MultiplyEpi64Si128(
		const Vector128 a,
		const Vector128 b,
		const int32 imm8
	)
	{
		return _mm_clmulepi64_si128(a, b, VImm8);
	}
}
