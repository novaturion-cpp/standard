﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	enum class ERoundingType : uint16
	{
		Nearest = 0x00,
		Down = 0x01,
		Up = 0x02,
		Truncate = 0x03,
		CurrentDirection = 0x04,

		NearestNoExcept = 0x00 | 0x08,
		DownNoExcept = 0x01 | 0x08,
		UpNoExcept = 0x02 | 0x08,
		TruncateNoExcept = 0x03 | 0x08,
		CurrentDirectionNoExcept = 0x04 | 0x08,

		NoExcept = 0x08,
	};
}
