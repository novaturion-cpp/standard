﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"
#include "standard/platform/intrinsic/type/vector256.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// ps	| 4  single-precision floats (32-bit)
	/// pd	| 2  double-precision floats (64-bit)
	/// ss	| 1  single-precision float (32-bit)
	/// sd	| 1  double-precision float (64-bit)
	/// </pre>
	namespace Fma3
	{
		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, add the
		/// intermediate result to packed elements in c
		inline Vector128 MultiplyAddPd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmadd_pd(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, add the
		/// intermediate result to packed elements in c
		inline Vector256 MultiplyAddPd(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fmadd_pd(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, add the
		/// intermediate result to packed elements in c
		inline Vector128 MultiplyAddPs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmadd_ps(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, add the
		/// intermediate result to packed elements in c
		inline Vector256 MultiplyAddPs(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fmadd_ps(a, b, c);
		}

		/// @brief	Multiply the lower double-precision (64-bit)
		/// floating-point elements in a and b, and add the
		/// intermediate result to the lower element in c
		/// Store the result in the lower element, and copy
		/// the upper element from a to the upper element
		inline Vector128 MultiplyAddSd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmadd_sd(a, b, c);
		}

		/// @brief	Multiply the lower single-precision (32-bit)
		/// floating-point elements in a and b, and add the
		/// intermediate result to the lower element in c
		/// Store the result in the lower element, and copy
		/// the upper 3 packed elements from a to the upper elements
		inline Vector128 MultiplyAddSs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmadd_ss(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, alternatively
		/// add and subtract packed elements in c to/from the
		/// intermediate result
		inline Vector128 MultiplyAddSubtractPd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmaddsub_pd(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, alternatively
		/// add and subtract packed elements in c to/from the
		/// intermediate result
		inline Vector256 MultiplyAddSubtractPd(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fmaddsub_pd(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, alternatively
		/// add and subtract packed elements in c to/from the
		/// intermediate result
		inline Vector128 MultiplyAddSubtractPs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmaddsub_ps(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, alternatively
		/// add and subtract packed elements in c to/from the
		/// intermediate result
		inline Vector256 MultiplyAddSubtractPs(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fmaddsub_ps(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, add the negated
		/// intermediate result to packed elements in c
		inline Vector128 MultiplyNegateAddPd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fnmadd_pd(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, add the negated
		/// intermediate result to packed elements in c
		inline Vector256 MultiplyNegateAddPd(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fnmadd_pd(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, add the negated
		/// intermediate result to packed elements in c
		inline Vector128 MultiplyNegateAddPs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fnmadd_ps(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, add the negated
		/// intermediate result to packed elements in c
		inline Vector256 MultiplyNegateAddPs(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fnmadd_ps(a, b, c);
		}

		/// @brief	Multiply the lower double-precision (64-bit)
		/// floating-point elements in a and b, and add the negated
		/// intermediate result to the lower element in c
		/// Store the result in the lower element, and copy
		/// the upper element from a to the upper element
		inline Vector128 MultiplyNegateAddSd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fnmadd_sd(a, b, c);
		}

		/// @brief	Multiply the lower single-precision (32-bit)
		/// floating-point elements in a and b, and add the negated
		/// intermediate result to the lower element in c
		/// Store the result in the lower element, and copy
		/// the upper 3 packed elements from a to the upper elements
		inline Vector128 MultiplyNegateAddSs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fnmadd_ss(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, subtract packed
		/// elements in c from the negated intermediate result
		inline Vector128 MultiplyNegateSubtractPd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fnmsub_pd(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, subtract packed
		/// elements in c from the negated intermediate result
		inline Vector256 MultiplyNegateSubtractPd(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fnmsub_pd(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, subtract packed
		/// elements in c from the negated intermediate result
		inline Vector128 MultiplyNegateSubtractPs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fnmsub_ps(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, subtract packed
		/// elements in c from the negated intermediate result
		inline Vector256 MultiplyNegateSubtractPs(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fnmsub_ps(a, b, c);
		}

		/// @brief	Multiply the lower double-precision (64-bit)
		/// floating-point elements in a and b, and subtract the
		/// lower element in c from the negated intermediate result
		/// Store the result in the lower element, and copy the upper
		/// element from a to the upper element
		inline Vector128 MultiplyNegateSubtractSd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fnmsub_sd(a, b, c);
		}

		/// @brief	Multiply the lower single-precision (32-bit)
		/// floating-point elements in a and b, and subtract the
		/// lower element in c from the negated intermediate result
		/// Store the result in the lower element, and copy the upper
		/// 3 packed elements from a to the upper elements
		inline Vector128 MultiplyNegateSubtractSs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fnmsub_ss(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, alternatively
		/// subtract and add packed elements in c from/to the
		/// intermediate result
		inline Vector128 MultiplySubtractAddPd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmsubadd_pd(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, alternatively
		/// subtract and add packed elements in c from/to the
		/// intermediate result
		inline Vector256 MultiplySubtractAddPd(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fmsubadd_pd(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, alternatively
		/// subtract and add packed elements in c from/to the
		/// intermediate result
		inline Vector128 MultiplySubtractAddPs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmsubadd_ps(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, alternatively
		/// subtract and add packed elements in c from/to the
		/// intermediate result
		inline Vector256 MultiplySubtractAddPs(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fmsubadd_ps(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, subtract packed
		/// elements in c from the intermediate result
		inline Vector128 MultiplySubtractPd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmsub_pd(a, b, c);
		}

		/// @brief	Multiply packed double-precision (64-bit)
		/// floating-point elements in a and b, subtract packed
		/// elements in c from the intermediate result
		inline Vector256 MultiplySubtractPd(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fmsub_pd(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, subtract packed
		/// elements in c from the intermediate result
		inline Vector128 MultiplySubtractPs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmsub_ps(a, b, c);
		}

		/// @brief	Multiply packed single-precision (32-bit)
		/// floating-point elements in a and b, subtract packed
		/// elements in c from the intermediate result
		inline Vector256 MultiplySubtractPs(
			const Vector256 a,
			const Vector256 b,
			const Vector256 c
		)
		{
			return _mm256_fmsub_ps(a, b, c);
		}

		/// @brief	Multiply the lower double-precision (64-bit)
		/// floating-point elements in a and b, and subtract the
		/// lower element in c from the intermediate result
		/// Store the result in the lower element, and copy
		/// the upper element from a to the upper element
		inline Vector128 MultiplySubtractSd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmsub_sd(a, b, c);
		}

		/// @brief	Multiply the lower single-precision (32-bit)
		/// floating-point elements in a and b, and subtract the
		/// lower element in c from the intermediate result
		/// Store the result in the lower element, and copy
		/// the upper 3 packed elements from a to the upper elements
		inline Vector128 MultiplySubtractSs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 c
		)
		{
			return _mm_fmsub_ss(a, b, c);
		}
	}
}
