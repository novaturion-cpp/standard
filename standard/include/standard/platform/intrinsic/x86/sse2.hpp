﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Sse2
	{
		/// @brief	Add packed 16-bit
		/// integers in a and b
		inline Vector128 AddEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_add_epi16(a, b);
		}

		/// @brief	Add packed 32-bit
		/// integers in a and b
		inline Vector128 AddEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_add_epi32(a, b);
		}

		/// @brief	Add packed 64-bit
		/// integers in a and b
		inline Vector128 AddEpi64(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_add_epi64(a, b);
		}

		/// @brief	Add packed 8-bit
		/// integers in a and b
		inline Vector128 AddEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_add_epi8(a, b);
		}

		/// @brief	Add packed double-precision (64-bit)
		/// floating-point elements in a and b
		inline Vector128 AddPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_add_pd(a, b);
		}

		/// @brief	Add packed signed 16-bit integers
		/// in a and b using saturation
		inline Vector128 AddSaturationEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_adds_epi16(a, b);
		}

		/// @brief	Add packed signed 8-bit integers
		/// in a and b using saturation
		inline Vector128 AddSaturationEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_adds_epi8(a, b);
		}

		/// @brief	Add packed unsigned 16-bit integers
		/// in a and b using saturation
		inline Vector128 AddSaturationEpu16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_adds_epu16(a, b);
		}

		/// @brief	Add packed unsigned 8-bit integers
		/// in a and b using saturation
		inline Vector128 AddSaturationEpu8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_adds_epu8(a, b);
		}

		/// @brief	Add the lower double-precision (64-bit) floating-point
		/// element in a and b, store the result in the lower element,
		/// and copy the upper element from a to the upper element
		inline Vector128 AddSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_add_sd(a, b);
		}

		/// @brief	Compute the bitwise NOT of packed double-precision
		/// (64-bit) floating-point elements in a and then AND with b
		inline Vector128 AndNotPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_andnot_pd(a, b);
		}

		/// @brief	Compute the bitwise NOT of 128 bits
		/// (representing integer data) in a and then AND with b
		inline Vector128 AndNotSi128(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_andnot_si128(a, b);
		}

		/// @brief	Compute the bitwise AND of packed double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector128 AndPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_and_pd(a, b);
		}

		/// @brief	Compute the bitwise AND of 128 bits
		/// (representing integer data) in a and b
		inline Vector128 AndSi128(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_and_si128(a, b);
		}

		/// @brief	Average packed unsigned
		/// 16-bit integers in a and b
		inline Vector128 AverageEpu16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_avg_epu16(a, b);
		}

		/// @brief	Average packed unsigned
		/// 8-bit integers in a and b
		inline Vector128 AverageEpu8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_avg_epu8(a, b);
		}

		/// @brief	Compare packed 16-bit
		/// integers in a and b for equality
		inline Vector128 CompareEqualEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpeq_epi16(a, b);
		}

		/// @brief	Compare packed 32-bit
		/// integers in a and b for equality
		inline Vector128 CompareEqualEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpeq_epi32(a, b);
		}

		/// @brief	Compare packed 8-bit
		/// integers in a and b for equality
		inline Vector128 CompareEqualEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpeq_epi8(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for equality
		inline Vector128 CompareEqualPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpeq_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for equality, store the result in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 CompareEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpeq_sd(a, b);
		}

		/// @brief	Compare packed signed 16-bit
		/// integers in a and b for greater-than
		inline Vector128 CompareGreaterEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpgt_epi16(a, b);
		}

		/// @brief	Compare packed signed 32-bit
		/// integers in a and b for greater-than
		inline Vector128 CompareGreaterEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpgt_epi32(a, b);
		}

		/// @brief	Compare packed signed 8-bit
		/// integers in a and b for greater-than
		inline Vector128 CompareGreaterEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpgt_epi8(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for greater-than-or-equal
		inline Vector128 CompareGreaterEqualPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpge_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for greater-than-or-equal, store the result in the
		/// lower element, and copy the upper element from a to the upper element
		inline Vector128 CompareGreaterEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpge_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for greater-than
		inline Vector128 CompareGreaterPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpgt_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for greater-than, store the result in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 CompareGreaterSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpgt_sd(a, b);
		}

		/// @brief	Compare packed signed 16-bit
		/// integers in a and b for less-than
		/// @note	Emits the pcmpgtw instruction
		/// with the order of the operands switched
		inline Vector128 CompareLessEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmplt_epi16(a, b);
		}

		/// @brief	Compare packed signed 32-bit
		/// integers in a and b for less-than
		/// @note	Emits the pcmpgtd instruction
		/// with the order of the operands switched
		inline Vector128 CompareLessEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmplt_epi32(a, b);
		}

		/// @brief	Compare packed signed 8-bit
		/// integers in a and b for less-than
		/// @note	Emits the pcmpgtb instruction
		/// with the order of the operands switched
		inline Vector128 CompareLessEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmplt_epi8(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for less-than-or-equal
		inline Vector128 CompareLessEqualPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmple_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for less-than-or-equal, store the result in the
		/// lower element, and copy the upper element from a to the upper element
		inline Vector128 CompareLessEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmple_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for less-than
		inline Vector128 CompareLessPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmplt_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for less-than, store the result in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 CompareLessSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmplt_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for not-equal
		inline Vector128 CompareNotEqualPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpneq_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for not-equal, store the result in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 CompareNotEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpneq_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for not-greater-than-or-equal
		inline Vector128 CompareNotGreaterEqualPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnge_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for not-greater-than-or-equal, store the result in
		/// the lower element, and copy the upper element from a to the upper element
		inline Vector128 CompareNotGreaterEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnge_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for not-greater-than
		inline Vector128 CompareNotGreaterPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpngt_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for not-greater-than, store the result in the
		/// lower element, and copy the upper element from a to the upper element
		inline Vector128 CompareNotGreaterSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpngt_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for not-less-than-or-equal
		inline Vector128 CompareNotLessEqualPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnle_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for not-less-than-or-equal, store the result in
		/// the lower element, and copy the upper element from a to the upper element
		inline Vector128 CompareNotLessEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnle_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b for not-less-than
		inline Vector128 CompareNotLessPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnlt_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b for not-less-than, store the result in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 CompareNotLessSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpnlt_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b to see if neither is NaN
		inline Vector128 CompareOrderedPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpord_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b to see if neither is NaN, store the result in the
		/// lower element, and copy the upper element from a to the upper element
		inline Vector128 CompareOrderedSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpord_sd(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit)
		/// floating-point elements in a and b to see if either is NaN
		inline Vector128 CompareUnorderedPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpunord_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b to see if either is NaN, store the result in the
		/// lower element, and copy the upper element from a to the upper element
		inline Vector128 CompareUnorderedSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpunord_sd(a, b);
		}

		/// @brief	Convert packed signed 32-bit integers in a to
		/// packed double-precision (64-bit) floating-point elements
		inline Vector128 ConvertEpi32Pd(
			const Vector128 a
		)
		{
			return _mm_cvtepi32_pd(a);
		}

		/// @brief	Convert packed signed 32-bit integers in a to
		/// packed single-precision (32-bit) floating-point elements
		inline Vector128 ConvertEpi32Ps(
			const Vector128 a
		)
		{
			return _mm_cvtepi32_ps(a);
		}

		/// @brief	Convert packed double-precision (64-bit)
		/// floating-point elements in a to packed 32-bit integers
		inline Vector128 ConvertPdEpi32(
			const Vector128 a
		)
		{
			return _mm_cvtpd_epi32(a);
		}

		/// @brief	Convert packed double-precision (64-bit) floating-point
		/// elements in a to packed 32-bit integers with truncation
		inline Vector128 ConvertPdEpi32Truncate(
			const Vector128 a
		)
		{
			return _mm_cvttpd_epi32(a);
		}

		/// @brief	Convert packed double-precision (64-bit) floating-point elements
		/// in a to packed single-precision (32-bit) floating-point elements
		inline Vector128 ConvertPdPs(
			const Vector128 a
		)
		{
			return _mm_cvtpd_ps(a);
		}

		/// @brief	Convert packed single-precision (32-bit)
		/// floating-point elements in a to packed 32-bit integers
		inline Vector128 ConvertPsEpi32(
			const Vector128 a
		)
		{
			return _mm_cvtps_epi32(a);
		}

		/// @brief	Convert packed single-precision (32-bit) floating-point
		/// elements in a to packed 32-bit integers with truncation
		inline Vector128 ConvertPsEpi32Truncate(
			const Vector128 a
		)
		{
			return _mm_cvttps_epi32(a);
		}

		/// @brief	Convert packed single-precision (32-bit) floating-point elements
		/// in a to packed double-precision (64-bit) floating-point elements
		inline Vector128 ConvertPsPd(
			const Vector128 a
		)
		{
			return _mm_cvtps_pd(a);
		}

		/// @brief	Convert the lower double-precision (64-bit)
		/// floating-point element in a to a 32-bit integer
		inline int32 ConvertSdSi32(
			const Vector128 a
		)
		{
			return _mm_cvtsd_si32(a);
		}

		/// @brief	Convert the lower double-precision (64-bit) floating-point
		/// element in a to a 32-bit integer with truncation
		inline int32 ConvertSdSi32Truncate(
			const Vector128 a
		)
		{
			return _mm_cvttsd_si32(a);
		}

		/// @brief	Convert the lower double-precision (64-bit)
		/// floating-point element in b to a single-precision (32-bit)
		/// floating-point element, store the result in the lower element,
		/// and copy the upper 3 packed elements from a to the upper elements
		inline Vector128 ConvertSdSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cvtsd_ss(a, b);
		}

		/// @brief	Copy the lower 32-bit integer in a
		inline int32 ConvertSi128Si32(
			const Vector128 a
		)
		{
			return _mm_cvtsi128_si32(a);
		}

		/// @brief	Convert the signed 32-bit integer b to a double-precision
		/// (64-bit) floating-point element, store the result in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 ConvertSi32Sd(
			const Vector128 a,
			const int32 b
		)
		{
			return _mm_cvtsi32_sd(a, b);
		}

		/// @brief	Copy 32-bit integer a to the lower
		/// elements of dst, and zero the upper elements
		inline Vector128 ConvertSi32Si128(
			const int32 a
		)
		{
			return _mm_cvtsi32_si128(a);
		}

		/// @brief	Convert the lower single-precision (32-bit) floating-point element
		/// in b to a double-precision (64-bit) floating-point element, store the result
		/// in the lower element, and copy the upper element from a to the upper element
		inline Vector128 ConvertSsSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cvtss_sd(a, b);
		}

		/// @brief	Divide packed double-precision (64-bit)
		/// floating-point elements in a by packed elements in b
		inline Vector128 DividePd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_div_pd(a, b);
		}

		/// @brief	Divide the lower double-precision (64-bit) floating-point
		/// element in a by the lower double-precision (64-bit) floating-point
		/// element in b, store the result in the lower element, and
		/// copy the upper element from a to the upper element
		inline Vector128 DivideSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_div_sd(a, b);
		}

		/// @brief	Extract a 16-bit integer from a, selected with
		/// imm8, and store the result in the lower element
		template<int32 VImm8>
		inline int32 ExtractEpi16(
			const Vector128 a
		)
		{
			return _mm_extract_epi16(a, VImm8);
		}

		/// @brief	Invalidate and flush the cache line that
		/// contains a from all levels of the cache hierarchy
		inline void FlushCache(
			const void* a
		)
		{
			_mm_clflush(a);
		}

		/// @brief	Copy a, and insert the 16-bit integer
		/// i into dst at the location specified by imm8
		template<int32 VImm8>
		inline Vector128 InsertEpi16(
			const Vector128 a,
			const int32 i
		)
		{
			return _mm_insert_epi16(a, i, VImm8);
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for equality
		inline bool IsEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comieq_sd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for equality
		/// @note	This instruction will not
		/// signal an exception for QNaNs
		inline bool IsEqualSdNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomieq_sd(a, b));
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for greater-than-or-equal
		inline bool IsGreaterEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comige_sd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for greater-than-or-equal
		/// @note	This instruction will not
		/// signal an exception for QNaNs
		inline bool IsGreaterEqualSdNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomige_sd(a, b));
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for greater-than
		inline bool IsGreaterSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comigt_sd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for greater-than
		/// @note	This instruction will not
		/// signal an exception for QNaNs
		inline bool IsGreaterSdNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomigt_sd(a, b));
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for less-than-or-equal
		inline bool IsLessEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comile_sd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for less-than-or-equal
		/// @note	This instruction will not
		/// signal an exception for QNaNs
		inline bool IsLessEqualSdNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomile_sd(a, b));
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for less-than
		inline bool IsLessSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comilt_sd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for less-than
		/// @note	This instruction will not
		/// signal an exception for QNaNs
		inline bool IsLessSdNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomilt_sd(a, b));
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for not-equal
		inline bool IsNotEqualSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_comineq_sd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit)
		/// floating-point element in a and b for not-equal
		/// @note	This instruction will not
		/// signal an exception for QNaNs
		inline bool IsNotEqualSdNonSignaling(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_ucomineq_sd(a, b));
		}

		/// @brief	Load a double-precision (64-bit)
		/// floating-point element from memory into both elements
		inline Vector128 LoadBothPd(
			const float64* address
		)
		{
			return _mm_load1_pd(address);
		}

		/// @brief	Load a double-precision (64-bit)
		/// floating-point element from memory into the
		/// upper element, and copy the lower element from a
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline Vector128 LoadHighPd(
			const Vector128 a,
			const float64* address
		)
		{
			return _mm_loadh_pd(a, address);
		}

		/// @brief	Load 64-bit integer from
		/// memory into the first element
		inline Vector128 LoadLowEpi64(
			Vector128* address
		)
		{
			return _mm_loadl_epi64(*address);
		}

		/// @brief	Load a double-precision (64-bit)
		/// floating-point element from memory into the
		/// lower element, and copy the upper element from a
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline Vector128 LoadLowPd(
			const Vector128 a,
			const float64* address
		)
		{
			return _mm_loadl_pd(a, address);
		}

		/// @brief	Load 128-bits (composed of 2 packed double-precision
		/// (64-bit) floating-point elements) from memory
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline Vector128 LoadPd(
			const float64* address
		)
		{
			return _mm_load_pd(address);
		}

		/// @brief	Load 2 double-precision (64-bit) floating-point
		/// elements from memory in reverse order
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline Vector128 LoadReversePd(
			const float64* address
		)
		{
			return _mm_loadr_pd(address);
		}

		/// @brief	Load a double-precision (64-bit) floating-point
		/// element from memory into the lower, and zero the upper element
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline Vector128 LoadSd(
			const float64* address
		)
		{
			return _mm_load_sd(address);
		}

		/// @brief	Load 128-bits of integer data from memory
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline Vector128 LoadSi128(
			Vector128* address
		)
		{
			return _mm_load_si128(*address);
		}

		/// @brief	Load 128-bits (composed of 2 packed double-precision
		/// (64-bit) floating-point elements) from memory
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline Vector128 LoadUnalignedPd(
			const float64* address
		)
		{
			return _mm_loadu_pd(address);
		}

		/// @brief	Load 128-bits of integer data from memory
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline Vector128 LoadUnalignedSi128(
			Vector128* address
		)
		{
			return _mm_loadu_si128(*address);
		}

		/// @brief	Conditionally store 8-bit integer elements from a into
		/// memory using mask (elements are not stored when the highest bit is
		/// not set in the corresponding element) and a non-temporal memory hint
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline void MaskStoreUnalignedSi128(
			const Vector128 a,
			const Vector128 mask,
			int8* address
		)
		{
			_mm_maskmoveu_si128(a, mask, reinterpret_cast<char*>(address));
		}

		/// @brief	Compare packed signed 16-bit integers
		/// in a and b, and store packed maximum values
		inline Vector128 MaxEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_epi16(a, b);
		}

		/// @brief	Compare packed unsigned 8-bit integers
		/// in a and b, and store packed maximum values
		inline Vector128 MaxEpu8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_epu8(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit) floating-point
		/// elements in a and b, and store packed maximum values
		/// @note	Result does not follow the IEEE Standard for
		/// Floating-Point Arithmetic (IEEE 754) maximum value
		/// when inputs are NaN or signed-zero values
		inline Vector128 MaxPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b, store the maximum value in the lower element,
		/// and copy the upper element from a to the upper element
		/// @note	Result does not follow the IEEE Standard for
		/// Floating-Point Arithmetic (IEEE 754) maximum value
		/// when inputs are NaN or signed-zero values
		inline Vector128 MaxSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_sd(a, b);
		}

		/// @brief	Compare packed signed 16-bit integers
		/// in a and b, and store packed minimum values
		inline Vector128 MinEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_epi16(a, b);
		}

		/// @brief	Compare packed unsigned 8-bit integers
		/// in a and b, and store packed minimum values
		inline Vector128 MinEpu8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_epu8(a, b);
		}

		/// @brief	Compare packed double-precision (64-bit) floating-point
		/// elements in a and b, and store packed minimum values
		/// @note	Result does not follow the IEEE Standard for
		/// Floating-Point Arithmetic (IEEE 754) minimum value
		/// when inputs are NaN or signed-zero values
		inline Vector128 MinPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_pd(a, b);
		}

		/// @brief	Compare the lower double-precision (64-bit) floating-point
		/// elements in a and b, store the minimum value in the lower element,
		/// and copy the upper element from a to the upper element
		/// @note	Result does not follow the IEEE Standard for
		/// Floating-Point Arithmetic (IEEE 754) minimum value
		/// when inputs are NaN or signed-zero values
		inline Vector128 MinSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_sd(a, b);
		}

		/// @brief	Copy the lower 64-bit integer in a to
		/// the lower element, and zero the upper element
		inline Vector128 MoveEpi64(
			const Vector128 a
		)
		{
			return _mm_move_epi64(a);
		}

		/// @brief	Set each bit of mask based on the most
		/// significant bit of each 8-bit element in a
		inline int32 MoveMaskEpi8(
			const Vector128 a
		)
		{
			return _mm_movemask_epi8(a);
		}

		/// @brief	Set each bit of mask based on the most
		/// significant bit of the corresponding packed
		/// double-precision (64-bit) floating-point element in a
		inline int32 MoveMaskPd(
			const Vector128 a
		)
		{
			return _mm_movemask_pd(a);
		}

		/// @brief	Move the lower double-precision (64-bit)
		/// floating-point element from b to the lower element,
		/// and copy the upper element from a to the upper element
		inline Vector128 MoveSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_move_sd(a, b);
		}

		/// @brief	Multiply packed signed 16-bit integers in a
		/// and b, producing intermediate signed 32-bit integers
		/// Horizontally add adjacent pairs of intermediate 32-bit integers
		inline Vector128 MultiplyAddEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_madd_epi16(a, b);
		}

		/// @brief	Multiply the packed signed 16-bit integers in
		/// a and b, producing intermediate 32-bit integers, and
		/// store the high 16 bits of the intermediate integers
		inline Vector128 MultiplyHighEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mulhi_epi16(a, b);
		}

		/// @brief	Multiply the packed unsigned 16-bit integers in
		/// a and b, producing intermediate 32-bit integers, and
		/// store the high 16 bits of the intermediate integers
		inline Vector128 MultiplyHighEpu16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mulhi_epu16(a, b);
		}

		/// @brief	Multiply the packed 16-bit integers in a
		/// and b, producing intermediate 32-bit integers, and
		/// store the low 16 bits of the intermediate integers
		inline Vector128 MultiplyLowEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mullo_epi16(a, b);
		}

		/// @brief	Multiply the low unsigned 32-bit integers
		/// from each packed 64-bit element in a and b,
		/// and store the unsigned 64-bit results
		inline Vector128 MultiplyLowEpu32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mul_epu32(a, b);
		}

		/// @brief	Multiply packed double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector128 MultiplyPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mul_pd(a, b);
		}

		/// @brief	Multiply the lower double-precision
		/// (64-bit) floating-point element in a and b,
		/// store the result in the lower element, and copy
		/// the upper element from a to the upper element
		inline Vector128 MultiplySd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mul_sd(a, b);
		}

		/// @brief	Compute the bitwise OR of packed double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector128 OrPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_or_pd(a, b);
		}

		/// @brief	Compute the bitwise OR of 128 bits
		/// (representing integer data) in a and b
		inline Vector128 OrSi128(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_or_si128(a, b);
		}

		/// @brief	Convert packed signed 16-bit integers from a and
		/// b to packed 8-bit integers using signed saturation
		inline Vector128 PackSaturationEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_packs_epi16(a, b);
		}

		/// @brief	Convert packed signed 32-bit integers from a and
		/// b to packed 16-bit integers using signed saturation
		inline Vector128 PackSaturationEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_packs_epi32(a, b);
		}

		/// @brief	Convert packed signed 16-bit integers from a and
		/// b to packed 8-bit integers using unsigned saturation
		inline Vector128 PackUnsignedSaturationEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_packus_epi16(a, b);
		}

		/// @brief	Provide a hint to the processor that the code
		/// sequence is a spin-wait loop. This can help improve the
		/// performance and power consumption of spin-wait loops
		inline void Pause()
		{
			_mm_pause();
		}

		/// @brief	Perform a serializing operation on all load-from-memory
		/// instructions that were issued prior to this instruction\n
		/// Guarantees that every load instruction that precedes,
		/// in program order, is globally visible before any load
		/// instruction which follows the fence in program order
		inline void SerializeLoadFence()
		{
			_mm_lfence();
		}

		/// @brief	Perform a serializing operation on all load-from-memory and
		/// store-to-memory instructions that were issued prior to this instruction\n
		/// Guarantees that every memory access that precedes, in program
		/// order, the memory fence instruction is globally visible before
		/// any memory instruction which follows the fence in program order
		inline void SerializeModifyFence()
		{
			_mm_mfence();
		}

		/// @brief	Broadcast 16-bit integer a to all elements
		/// @note	May generate vpbroadcastw
		inline Vector128 Set1Epi16(
			const int16 a
		)
		{
			return _mm_set1_epi16(a);
		}

		/// @brief	Broadcast 32-bit integer a to all elements
		/// @note	May generate vpbroadcastd
		inline Vector128 Set1Epi32(
			const int32 a
		)
		{
			return _mm_set1_epi32(a);
		}

		/// @brief	Broadcast 8-bit integer a to all elements
		/// @note	May generate vpbroadcastb
		inline Vector128 Set1Epi8(
			const int8 a
		)
		{
			return _mm_set1_epi8(a);
		}

		/// @brief	Broadcast double-precision (64-bit)
		/// floating-point value a to all elements
		inline Vector128 Set1Pd(
			const float64 a
		)
		{
			return _mm_set1_pd(a);
		}

		/// @brief	Set packed 16-bit integers
		/// with the supplied values
		inline Vector128 SetEpi16(
			const int16 e0,
			const int16 e1,
			const int16 e2,
			const int16 e3,
			const int16 e4,
			const int16 e5,
			const int16 e6,
			const int16 e7
		)
		{
			return _mm_set_epi16(e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed 32-bit integers
		/// with the supplied values
		inline Vector128 SetEpi32(
			const int32 e0,
			const int32 e1,
			const int32 e2,
			const int32 e3
		)
		{
			return _mm_set_epi32(e3, e2, e1, e0);
		}

		/// @brief	Set packed 8-bit integers
		/// with the supplied values
		inline Vector128 SetEpi8(
			const int8 e0,
			const int8 e1,
			const int8 e2,
			const int8 e3,
			const int8 e4,
			const int8 e5,
			const int8 e6,
			const int8 e7,
			const int8 e8,
			const int8 e9,
			const int8 e10,
			const int8 e11,
			const int8 e12,
			const int8 e13,
			const int8 e14,
			const int8 e15
		)
		{
			return _mm_set_epi8(e15, e14, e13, e12, e11, e10, e9, e8, e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed double-precision (64-bit)
		/// floating-point elements with the supplied values
		inline Vector128 SetPd(
			const float64 e0,
			const float64 e1
		)
		{
			return _mm_set_pd(e1, e0);
		}

		/// @brief	Set packed 16-bit integers with
		/// the supplied values in reverse order
		inline Vector128 SetReverseEpi16(
			const int16 e0,
			const int16 e1,
			const int16 e2,
			const int16 e3,
			const int16 e4,
			const int16 e5,
			const int16 e6,
			const int16 e7
		)
		{
			return _mm_setr_epi16(e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed 32-bit integers with
		/// the supplied values in reverse order
		inline Vector128 SetReverseEpi32(
			const int32 e0,
			const int32 e1,
			const int32 e2,
			const int32 e3
		)
		{
			return _mm_setr_epi32(e3, e2, e1, e0);
		}

		/// @brief	Set packed 8-bit integers with
		/// the supplied values in reverse order
		inline Vector128 SetReverseEpi8(
			const int8 e0,
			const int8 e1,
			const int8 e2,
			const int8 e3,
			const int8 e4,
			const int8 e5,
			const int8 e6,
			const int8 e7,
			const int8 e8,
			const int8 e9,
			const int8 e10,
			const int8 e11,
			const int8 e12,
			const int8 e13,
			const int8 e14,
			const int8 e15
		)
		{
			return _mm_setr_epi8(e15, e14, e13, e12, e11, e10, e9, e8, e7, e6, e5, e4, e3, e2, e1, e0);
		}

		/// @brief	Set packed double-precision (64-bit) floating-point
		/// elements with the supplied values in reverse order
		inline Vector128 SetReversePd(
			const float64 e0,
			const float64 e1
		)
		{
			return _mm_setr_pd(e1, e0);
		}

		/// @brief	Copy double-precision (64-bit) floating-point
		/// element a to the lower element, and zero the upper element
		inline Vector128 SetSd(
			const float64 a
		)
		{
			return _mm_set_sd(a);
		}

		/// @brief	Return vector with all elements set to zero
		inline Vector128 SetZeroPd()
		{
			return _mm_setzero_pd();
		}

		/// @brief	Return vector with all elements set to zero
		inline Vector128 SetZeroSi128()
		{
			return _mm_setzero_si128();
		}

		/// @brief	Shift packed 16-bit integers in
		/// a left by count while shifting in zeros
		inline Vector128 ShiftLeftLogicalEpi16(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_sll_epi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a left by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector128 ShiftLeftLogicalEpi16(
			const Vector128 a
		)
		{
			return _mm_slli_epi16(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a left by count while shifting in zeros
		inline Vector128 ShiftLeftLogicalEpi32(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_sll_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a left by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector128 ShiftLeftLogicalEpi32(
			const Vector128 a
		)
		{
			return _mm_slli_epi32(a, VImm8);
		}

		/// @brief	Shift packed 64-bit integers in
		/// a left by count while shifting in zeros
		inline Vector128 ShiftLeftLogicalEpi64(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_sll_epi64(a, count);
		}

		/// @brief	Shift packed 64-bit integers in
		/// a left by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector128 ShiftLeftLogicalEpi64(
			const Vector128 a
		)
		{
			return _mm_slli_epi64(a, VImm8);
		}

		/// @brief	Shift a left by imm8
		/// bytes while shifting in zeros
		template<int32 VImm8>
		inline Vector128 ShiftLeftLogicalSi128(
			const Vector128 a
		)
		{
			return _mm_slli_si128(a, VImm8);
		}

		/// @brief	Shift packed 16-bit integers in a
		/// right by count while shifting in sign bits
		inline Vector128 ShiftRightArithmeticEpi16(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_sra_epi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in a
		/// right by imm8 while shifting in sign bits
		template<int32 VImm8>
		inline Vector128 ShiftRightArithmeticEpi16(
			const Vector128 a
		)
		{
			return _mm_srai_epi16(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in a
		/// right by count while shifting in sign bits
		inline Vector128 ShiftRightArithmeticEpi32(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_sra_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in a
		/// right by imm8 while shifting in sign bits
		template<int32 VImm8>
		inline Vector128 ShiftRightArithmeticEpi32(
			const Vector128 a
		)
		{
			return _mm_srai_epi32(a, VImm8);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a right by count while shifting in zeros
		inline Vector128 ShiftRightLogicalEpi16(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_srl_epi16(a, count);
		}

		/// @brief	Shift packed 16-bit integers in
		/// a right by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector128 ShiftRightLogicalEpi16(
			const Vector128 a
		)
		{
			return _mm_srli_epi16(a, VImm8);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by count while shifting in zeros
		inline Vector128 ShiftRightLogicalEpi32(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_srl_epi32(a, count);
		}

		/// @brief	Shift packed 32-bit integers in
		/// a right by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector128 ShiftRightLogicalEpi32(
			const Vector128 a
		)
		{
			return _mm_srli_epi32(a, VImm8);
		}

		/// @brief	Shift packed 64-bit integers in
		/// a right by count while shifting in zeros
		inline Vector128 ShiftRightLogicalEpi64(
			const Vector128 a,
			const Vector128 count
		)
		{
			return _mm_srl_epi64(a, count);
		}

		/// @brief	Shift packed 64-bit integers in
		/// a right by imm8 while shifting in zeros
		template<int32 VImm8>
		inline Vector128 ShiftRightLogicalEpi64(
			const Vector128 a
		)
		{
			return _mm_srli_epi64(a, VImm8);
		}

		/// @brief	Shift a right by imm8
		/// bytes while shifting in zeros
		template<int32 VImm8>
		inline Vector128 ShiftRightLogicalSi128(
			const Vector128 a
		)
		{
			return _mm_srli_si128(a, VImm8);
		}

		/// @brief	Shuffle 32-bit integers
		/// in a using the control in imm8
		template<int32 VImm8>
		inline Vector128 ShuffleEpi32(
			const Vector128 a
		)
		{
			return _mm_shuffle_epi32(a, VImm8);
		}

		/// @brief	Shuffle 16-bit integers in the high
		/// 64 bits of a using the control in imm8
		/// Store the results in the high 64 bits,
		/// with the low 64 bits being copied from a
		template<int32 VImm8>
		inline Vector128 ShuffleHighEpi16(
			const Vector128 a
		)
		{
			return _mm_shufflehi_epi16(a, VImm8);
		}

		/// @brief	Shuffle 16-bit integers in the low
		/// 64 bits of a using the control in imm8
		/// Store the results in the low 64 bits,
		/// with the high 64 bits being copied from a
		template<int32 VImm8>
		inline Vector128 ShuffleLowEpi16(
			const Vector128 a
		)
		{
			return _mm_shufflelo_epi16(a, VImm8);
		}

		/// @brief	Shuffle double-precision (64-bit)
		/// floating-point elements using the control in imm8
		template<int32 VImm8>
		inline Vector128 ShufflePd(
			const Vector128 a
		)
		{
			return _mm_shuffle_pd(a, a, VImm8);
		}

		/// @brief	Shuffle double-precision (64-bit)
		/// floating-point elements using the control in imm8
		template<int32 VImm8>
		inline Vector128 ShufflePd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_shuffle_pd(a, b, VImm8);
		}

		/// @brief	Compute the square root of packed
		/// double-precision (64-bit) floating-point elements in a
		inline Vector128 SqrtPd(
			const Vector128 a
		)
		{
			return _mm_sqrt_pd(a);
		}

		/// @brief	Compute the square root of the lower double-precision
		/// (64-bit) floating-point element in b, store the result in the
		/// lower element, and copy the upper element from a to the upper element
		inline Vector128 SqrtSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sqrt_sd(a, b);
		}

		/// @brief	Store the upper double-precision (64-bit)
		/// floating-point element from a into memory
		inline void StoreHighPd(
			float64* address,
			const Vector128 a
		)
		{
			_mm_storeh_pd(address, a);
		}

		/// @brief	Store 64-bit integer from
		/// the first element of a into memory
		inline void StoreLowEpi64(
			Vector128* address,
			const Vector128 a
		)
		{
			_mm_storel_epi64(*address, a);
		}

		/// @brief	Store the lower double-precision (64-bit)
		/// floating-point element from a into memory
		inline void StoreLowPd(
			float64* address,
			const Vector128 a
		)
		{
			_mm_storel_pd(address, a);
		}

		/// @brief	Store the lower double-precision (64-bit) floating-point
		/// element from a into 2 contiguous elements in memory
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StoreLowTwicePd(
			float64* address,
			const Vector128 a
		)
		{
			_mm_store1_pd(address, a);
		}

		/// @brief	Store 128-bits (composed of 2 packed double-precision
		/// (64-bit) floating-point elements) from a into memory
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StorePd(
			float64* address,
			const Vector128 a
		)
		{
			_mm_store_pd(address, a);
		}

		/// @brief	Store 2 double-precision (64-bit) floating-point
		/// elements from a into memory in reverse order
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StoreReversePd(
			float64* address,
			const Vector128 a
		)
		{
			_mm_storer_pd(address, a);
		}

		/// @brief	Store the lower double-precision (64-bit)
		/// floating-point element from a into memory
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline void StoreSd(
			float64* address,
			const Vector128 a
		)
		{
			_mm_store_sd(address, a);
		}

		/// @brief	Store 128-bits of integer data from a into memory
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StoreSi128(
			Vector128* address,
			const Vector128 a
		)
		{
			_mm_store_si128(*address, a);
		}

		/// @brief	Store 128-bits (composed of 2 packed double-precision
		/// (64-bit) floating-point elements) from a into memory
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline void StoreUnalignedPd(
			float64* address,
			const Vector128 a
		)
		{
			_mm_storeu_pd(address, a);
		}

		/// @brief	Store 128-bits of integer data from a into memory
		/// @note	Address does not need to be
		/// aligned on any particular boundary
		inline void StoreUnalignedSi128(
			Vector128* address,
			const Vector128 a
		)
		{
			_mm_storeu_si128(*address, a);
		}

		/// @brief	Store 128-bits (composed of 2 packed double-precision (64-bit)
		/// floating-point elements) from a into memory using a non-temporal memory hint
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StreamPd(
			float64* address,
			const Vector128 a
		)
		{
			_mm_stream_pd(address, a);
		}

		/// @brief	Store 128-bits of integer data from
		/// a into memory using a non-temporal memory hint
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline void StreamSi128(
			Vector128* address,
			const Vector128 a
		)
		{
			_mm_stream_si128(*address, a);
		}

		/// @brief	Store 32-bit integer a into memory using
		/// a non-temporal hint to minimize cache pollution
		/// If the cache line containing address is already
		/// in the cache, the cache will be updated
		inline void StreamSi32(
			int32* address,
			const int32 a
		)
		{
			_mm_stream_si32(address, a);
		}

		/// @brief	Subtract packed 16-bit integers
		/// in b from packed 16-bit integers in a
		inline Vector128 SubtractEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sub_epi16(a, b);
		}

		/// @brief	Subtract packed 32-bit integers
		/// in b from packed 32-bit integers in a
		inline Vector128 SubtractEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sub_epi32(a, b);
		}

		/// @brief	Subtract packed 64-bit integers
		/// in b from packed 64-bit integers in a
		inline Vector128 SubtractEpi64(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sub_epi64(a, b);
		}

		/// @brief	Subtract packed 8-bit integers
		/// in b from packed 8-bit integers in a
		inline Vector128 SubtractEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sub_epi8(a, b);
		}

		/// @brief	Subtract packed double-precision (64-bit)
		/// floating-point elements in b from packed double-precision
		/// (64-bit) floating-point elements in a
		inline Vector128 SubtractPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sub_pd(a, b);
		}

		/// @brief	Subtract packed signed 16-bit integers in
		/// b from packed 16-bit integers in a using saturation
		inline Vector128 SubtractSaturationEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_subs_epi16(a, b);
		}

		/// @brief	Subtract packed signed 8-bit integers in
		/// b from packed 8-bit integers in a using saturation
		inline Vector128 SubtractSaturationEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_subs_epi8(a, b);
		}

		/// @brief	Subtract packed unsigned 16-bit integers in b
		/// from packed unsigned 16-bit integers in a using saturation
		inline Vector128 SubtractSaturationEpu16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_subs_epu16(a, b);
		}

		/// @brief	Subtract packed unsigned 8-bit integers in b
		/// from packed unsigned 8-bit integers in a using saturation
		inline Vector128 SubtractSaturationEpu8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_subs_epu8(a, b);
		}

		/// @brief	Subtract the lower double-precision (64-bit)
		/// floating-point element in b from the lower double-precision
		/// (64-bit) floating-point element in a, store the result in the
		/// lower element, and copy the upper element from a to the upper element
		inline Vector128 SubtractSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sub_sd(a, b);
		}

		/// @brief	Compute the absolute differences of packed unsigned
		/// 8-bit integers in a and b, then horizontally sum each consecutive
		/// 8 differences to produce two unsigned 16-bit integers, and pack
		/// these unsigned 16-bit integers in the low 16 bits of 64-bit elements
		inline Vector128 SumAbsDifferenceEpu8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_sad_epu8(a, b);
		}

		/// @brief	Unpack and interleave 16-bit
		/// integers from the high half of a and b
		inline Vector128 UnpackHighEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpackhi_epi16(a, b);
		}

		/// @brief	Unpack and interleave 32-bit
		/// integers from the high half of a and b
		inline Vector128 UnpackHighEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpackhi_epi32(a, b);
		}

		/// @brief	Unpack and interleave 64-bit
		/// integers from the high half of a and b
		inline Vector128 UnpackHighEpi64(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpackhi_epi64(a, b);
		}

		/// @brief	Unpack and interleave 8-bit
		/// integers from the high half of a and b
		inline Vector128 UnpackHighEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpackhi_epi8(a, b);
		}

		/// @brief	Unpack and interleave double-precision (64-bit)
		/// floating-point elements from the high half of a and b
		inline Vector128 UnpackHighPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpackhi_pd(a, b);
		}

		/// @brief	Unpack and interleave 16-bit
		/// integers from the low half of a and b
		inline Vector128 UnpackLowEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpacklo_epi16(a, b);
		}

		/// @brief	Unpack and interleave 32-bit
		/// integers from the low half of a and b
		inline Vector128 UnpackLowEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpacklo_epi32(a, b);
		}

		/// @brief	Unpack and interleave 64-bit
		/// integers from the low half of a and b
		inline Vector128 UnpackLowEpi64(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpacklo_epi64(a, b);
		}

		/// @brief	Unpack and interleave 8-bit
		/// integers from the low half of a and b
		inline Vector128 UnpackLowEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpacklo_epi8(a, b);
		}

		/// @brief	Unpack and interleave double-precision (64-bit)
		/// floating-point elements from the low half of a and b
		inline Vector128 UnpackLowPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_unpacklo_pd(a, b);
		}

		/// @brief	Compute the bitwise XOR of packed double-precision
		/// (64-bit) floating-point elements in a and b
		inline Vector128 XorPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_xor_pd(a, b);
		}

		/// @brief	Compute the bitwise XOR of 128 bits
		/// (representing integer data) in a and b
		inline Vector128 XorSi128(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_xor_si128(a, b);
		}

		#if ARCH_IS_X86

		/// @brief	Add 64-bit integers a and b
	    /// @note   Available only on x86 targets
		inline Vector64 AddSi64(
			const Vector64 a,
			const Vector64 b
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_add_si64(a, b);
		}

		/// @brief	Convert packed double-precision (64-bit)
		/// floating-point elements in a to packed 32-bit integers
	    /// @note   Available only on x86 targets
		inline Vector64 ConvertPdPi32(
			const Vector128 a
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_cvtpd_pi32(a);
		}

		/// @brief	Convert packed double-precision (64-bit) floating-point
		/// elements in a to packed 32-bit integers with truncation
	    /// @note   Available only on x86 targets
		inline Vector64 ConvertPdPi32Truncate(
			const Vector128 a
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_cvttpd_pi32(a);
		}

		/// @brief	Convert packed signed 32-bit integers in a to
		/// packed double-precision (64-bit) floating-point elements
	    /// @note   Available only on x86 targets
		inline Vector128 ConvertPi32Pd(
			const Vector64 a
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_cvtpi32_pd(a);
		}

		/// @brief	Copy the 64-bit integer a to the
		/// lower element, and zero the upper element
	    /// @note   Available only on x86 targets
		inline Vector128 MovPi64Epi64(
			const Vector64 a
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_movpi64_epi64(a);
		}

		/// @brief	Copy the lower 64-bit integer in a
	    /// @note   Available only on x86 targets
		inline Vector64 MovePi64Pi64(
			const Vector128 a
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_movepi64_pi64(a);
		}

		/// @brief	Multiply the low unsigned 32-bit integers
		/// from a and b, and store the unsigned 64-bit result
	    /// @note   Available only on x86 targets
		inline Vector64 MultiplySu32(
			const Vector64 a,
			const Vector64 b
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_mul_su32(a, b);
		}

		/// @brief	Broadcast 64-bit integer a to all elements
	    /// @note   Available only on x86 targets
		inline Vector128 Set1Epi64(
			const Vector64 a
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_set1_epi64(a);
		}

		/// @brief	Set packed 64-bit integers
		/// with the supplied values
	    /// @note   Available only on x86 targets
		inline Vector128 SetEpi64(
			const Vector64 e0,
			const Vector64 e1
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_set_epi64(e1, e0);
		}

		/// @brief	Set packed 64-bit integers with
		/// the supplied values in reverse order
	    /// @note   Available only on x86 targets
		inline Vector128 SetReverseEpi64(
			const Vector64 e0,
			const Vector64 e1
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_setr_epi64(e1, e0);
		}

		/// @brief	Subtract 64-bit integer
		/// b from 64-bit integer a
	    /// @note   Available only on x86 targets
		inline Vector64 SubtractSi64(
			const Vector64 a,
			const Vector64 b
		)
		{
			static_assert(IS_X86, "Attempt to use x86 only intrinsic on non-x86 target");
			return _mm_sub_si64(a, b);
		}

		#endif

		#if ARCH_IS_X64

		/// @brief	Convert the lower double-precision (64-bit)
		/// floating-point element in a to a 64-bit integer
	    /// @note   Available only on x64 targets
		inline int64 ConvertSdSi64(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsd_si64(a);
		}

		/// @brief	Convert the lower double-precision (64-bit)
		/// floating-point element in a to a 64-bit integer
	    /// @note   Available only on x64 targets
		inline int64 ConvertSdSi64x(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsd_si64x(a);
		}

		/// @brief	Copy the lower 64-bit integer in a
	    /// @note   Available only on x64 targets
		inline int64 ConvertSi128Si64(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsi128_si64(a);
		}

		/// @brief	Copy the lower 64-bit integer in a
	    /// @note   Available only on x64 targets
		inline int64 ConvertSi128Si64x(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsi128_si64x(a);
		}

		/// @brief	Convert the signed 64-bit integer b to a double-precision
		/// (64-bit) floating-point element, store the result in the lower
		/// element, and copy the upper element from a to the upper element
	    /// @note   Available only on x64 targets
		inline Vector128 ConvertSi64Sd(
			const Vector128 a,
			const int64 b
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsi64_sd(a, b);
		}

		/// @brief	Copy 64-bit integer a to the lower
		/// element, and zero the upper element
	    /// @note   Available only on x64 targets
		inline Vector128 ConvertSi64Si128(
			const int64 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsi64_si128(a);
		}

		/// @brief	Convert the signed 64-bit integer b to a double-precision
		/// (64-bit) floating-point element, store the result in the lower
		/// element, and copy the upper element from a to the upper element
	    /// @note   Available only on x64 targets
		inline Vector128 ConvertSi64xSd(
			const Vector128 a,
			const int64 b
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsi64x_sd(a, b);
		}

		/// @brief	Copy 64-bit integer a to the lower
		/// element, and zero the upper element
	    /// @note   Available only on x64 targets
		inline Vector128 ConvertSi64xSi128(
			const int64 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvtsi64x_si128(a);
		}

		/// @brief	Convert the lower double-precision (64-bit)
		/// floating-point element in a to a 64-bit integer with truncation
	    /// @note   Available only on x64 targets
		inline int64 ConvertTruncateSdSi64(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvttsd_si64(a);
		}

		/// @brief	Convert the lower double-precision (64-bit)
		/// floating-point element in a to a 64-bit integer with truncation
	    /// @note   Available only on x64 targets
		inline int64 ConvertTruncateSdSi64x(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvttsd_si64x(a);
		}

		/// @brief	Convert the lower single-precision (32-bit)
		/// floating-point element in a to a 64-bit integer with truncation
	    /// @note   Available only on x64 targets
		inline int64 ConvertTruncateSsSi64(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_cvttss_si64(a);
		}

		/// @brief	Broadcast 64-bit integer a to all elements
		/// @note	May generate the vpbroadcastq
	    /// @note   Available only on x64 targets
		inline Vector128 Set1Epi64x(
			const int64 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_set1_epi64x(a);
		}

		/// @brief	Set packed 64-bit integers
		/// with the supplied values
	    /// @note   Available only on x64 targets
		inline Vector128 SetEpi64x(
			const int64 e0,
			const int64 e1
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_set_epi64x(e1, e0);
		}

		#endif
	}
}
