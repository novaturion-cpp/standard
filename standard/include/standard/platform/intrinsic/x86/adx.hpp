﻿#pragma once
#include "standard/platform/type/int.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Adx
{
	/// @brief	Add unsigned 32-bit integers a and b
	/// with unsigned 8-bit carry flag
	inline void AddCarry(
		uint32* outResult,
		uint8* outFlag,
		const uint32 a,
		const uint32 b,
		const uint8 flag
	)
	{
		*outFlag = _addcarry_u32(flag, a, b, outResult);
	}

	/// @brief	Add unsigned 32-bit integers a and b
	/// with unsigned 8-bit carry or overflow flag
	inline void AddCarryX(
		uint32* outResult,
		uint8* outFlag,
		const uint32 a,
		const uint32 b,
		const uint8 flag
	)
	{
		*outFlag = _addcarryx_u32(flag, a, b, outResult);
	}


	#if ARCH_IS_X64

	/// @brief	Add unsigned 64-bit integers a and b
	/// with unsigned 8-bit carry flag
	/// @note   Available only on x64 targets
	inline void AddCarry(
		uint64* outResult,
		uint8* outFlag,
		const uint64 a,
		const uint64 b,
		const uint8 flag
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		*outFlag = _addcarry_u64(flag, a, b, outResult);
	}

	/// @brief	Add unsigned 64-bit integers a and b
	/// with unsigned 8-bit carry or overflow flag
	/// @note   Available only on x64 targets
	inline void AddCarryX(
		uint64* outResult,
		uint8* outFlag,
		const uint64 a,
		const uint64 b,
		const uint8 flag
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		*outFlag = _addcarryx_u64(flag, a, b, outResult);
	}

	#endif
}
