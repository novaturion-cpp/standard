﻿#pragma once
#include "standard/platform/type/int.hpp"
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Tbm
{
	/// @note	C++ equivalent
	constexpr uint32 Extract(
		const uint32 a,
		const int8 start,
		const int8 length
	)
	{
		return (a >> start) & ((1 << length) - 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 Extract(
		const uint64 a,
		const int8 start,
		const int8 length
	)
	{
		return (a >> start) & ((1 << length) - 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 FillLowestClear(
		const uint32 a
	)
	{
		return a & (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 FillLowestClear(
		const uint64 a
	)
	{
		return a & (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 FillLowestSet(
		const uint32 a
	)
	{
		return a | (a - 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 FillLowestSet(
		const uint64 a
	)
	{
		return a | (a - 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 IsolateLowestClear(
		const uint32 a
	)
	{
		return a | ~(a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 IsolateLowestClear(
		const uint64 a
	)
	{
		return a | ~(a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 IsolateLowestClearComplement(
		const uint32 a
	)
	{
		return ~a & (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 IsolateLowestClearComplement(
		const uint64 a
	)
	{
		return ~a & (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 IsolateLowestSetComplement(
		const uint32 a
	)
	{
		return ~a | (a - 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 IsolateLowestSetComplement(
		const uint64 a
	)
	{
		return ~a | (a - 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 MaskLowestClear(
		const uint32 a
	)
	{
		return a ^ (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 MaskLowestClear(
		const uint64 a
	)
	{
		return a ^ (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 MaskTrailingZeros(
		const uint32 a
	)
	{
		return ~a & (a - 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 MaskTrailingZeros(
		const uint64 a
	)
	{
		return ~a & (a - 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 MaskTrailingZerosInversed(
		const uint32 a
	)
	{
		return ~a | (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 MaskTrailingZerosInversed(
		const uint64 a
	)
	{
		return ~a | (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint32 SetLowestClear(
		const uint32 a
	)
	{
		return a | (a + 1);
	}

	/// @note	C++ equivalent
	constexpr uint64 SetLowestClear(
		const uint64 a
	)
	{
		return a | (a + 1);
	}
}
