#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Bmi2
{
	/// @brief	Multiply unsigned 32-bit integers a and b,
	/// store the low 32-bits of the result outLow,
	/// and store the high 32-bits in outHigh
	/// @note	Does not read or write arithmetic flags
	inline void Multiply(
		uint32* outLow,
		uint32* outHigh,
		const uint32 a,
		const uint32 b
	)
	{
		*outLow = _mulx_u32(a, b, outHigh);
	}

	/// @brief	Deposit contiguous low bits from
	/// unsigned 32-bit integer a at the corresponding
	/// bit locations specified by mask\n
	/// All other bits are set to zero
	inline uint32 ParallelBitsDeposit(
		const uint32 a,
		const uint32 mask
	)
	{
		return _pdep_u32(a, mask);
	}

	/// @brief	Extract bits from unsigned 32-bit
	/// integer a at the corresponding bit locations
	/// specified by mask to contiguous low bits in dst\n
	/// The remaining upper bits in dst are set to zero
	inline uint32 ParallelBitsExtract(
		const uint32 a,
		const uint32 mask
	)
	{
		return _pext_u32(a, mask);
	}

	/// @brief	Rotate the bits of unsigned
	/// 32-bit integer a right by the count imm8\n
	/// Does not read or write the arithmetic flags
	template<uint32 VCount>
	inline uint32 RotateRightLogicalX(
		const uint32 a
	)
	{
		return _rorx_u32(a, VCount);
	}

	/// @brief	Shift the bits of unsigned 32-bit
	/// integer a to the left by the count\n
	/// Do not keep and propagate the most significant
	/// bit (sign bit) while shifting
	inline uint32 ShiftLeftLogicalX(
		const uint32 a,
		const uint32 count
	)
	{
		return _shlx_u32(a, count);
	}

	/// @brief	Shift the bits of unsigned 32-bit
	/// integer a to the right by the count\n
	/// Keep and propagate the most significant
	/// bit (sign bit) while shifting
	inline int32 ShiftRightArithmeticX(
		const int32 a,
		const uint32 count
	)
	{
		return _sarx_i32(a, count);
	}

	/// @brief	Shift the bits of unsigned 32-bit
	/// integer a to the right by the count\n
	/// Do not keep and propagate the most significant
	/// bit (sign bit) while shifting
	inline uint32 ShiftRightLogicalX(
		const uint32 a,
		const uint32 count
	)
	{
		return _shrx_u32(a, count);
	}

	/// @brief	Copy all bits from unsigned 32-bit integer a ,
	/// and reset (set to 0) the high bits starting at start
	inline uint32 ZeroHighBits(
		const uint32 a,
		const uint32 start
	)
	{
		return _bzhi_u32(a, start);
	}


	#if ARCH_IS_X64

	/// @brief	Multiply unsigned 64-bit integers a and b,
	/// store the low 64-bits of the result outLow,
	/// and store the high 64-bits in outHigh
	/// @note	Does not read or write arithmetic flags
	/// @note   Available only on x64 targets
	inline void Multiply(
		uint64* outLow,
		uint64* outHigh,
		const uint64 a,
		const uint64 b
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		*outLow = _mulx_u64(a, b, outHigh);
	}

	/// @brief	Deposit contiguous low bits from
	/// unsigned 64-bit integer a at the corresponding
	/// bit locations specified by mask\n
	/// All other bits are set to zero
	/// @note   Available only on x64 targets
	inline uint64 ParallelBitsDeposit(
		const uint64 a,
		const uint64 mask
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _pdep_u64(a, mask);
	}

	/// @brief	Extract bits from unsigned 64-bit
	/// integer a at the corresponding bit locations
	/// specified by mask to contiguous low bits in dst\n
	/// The remaining upper bits in dst are set to zero
	/// @note   Available only on x64 targets
	inline uint64 ParallelBitsExtract(
		const uint64 a,
		const uint64 mask
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _pext_u64(a, mask);
	}

	/// @brief	Rotate the bits of unsigned
	/// 64-bit integer a right by the count imm8\n
	/// Does not read or write the arithmetic flags
	/// @note   Available only on x64 targets
	template<uint64 VImm8>
	inline uint64 RotateRightLogicalX(
		const uint32 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _rorx_u64(a, VImm8);
	}

	/// @brief	Shift the bits of unsigned 64-bit
	/// integer a to the left by the count\n
	/// Do not keep and propagate the most significant
	/// bit (sign bit) while shifting
	/// @note   Available only on x64 targets
	inline uint64 ShiftLeftLogicalX(
		const uint64 a,
		const uint32 count
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _shlx_u64(a, count);
	}

	/// @brief	Shift the bits of unsigned 64-bit
	/// integer a to the right by the count\n
	/// Keep and propagate the most significant
	/// bit (sign bit) while shifting
	/// @note   Available only on x64 targets
	inline uint64 ShiftRightArithmeticX(
		const uint64 a,
		const uint32 count
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _sarx_i64(a, count);
	}

	/// @brief	Shift the bits of unsigned 64-bit
	/// integer a to the right by the count\n
	/// Do not keep and propagate the most significant
	/// bit (sign bit) while shifting
	/// @note   Available only on x64 targets
	inline uint64 ShiftRightLogicalX(
		const uint64 a,
		const uint32 count
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _shrx_u64(a, count);
	}

	/// @brief	Copy all bits from unsigned 64-bit integer a,
	/// and reset (set to 0) the high bits starting at start
	/// @note   Available only on x64 targets
	inline uint64 ZeroHighBits(
		const uint64 a,
		const uint32 start
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _bzhi_u64(a, start);
	}

	#endif
}
