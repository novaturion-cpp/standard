﻿#pragma once
#include "standard/platform/type/uint.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::Abm
{
	/// @brief	Count the number of leading bits set to 0
	inline uint32 LeadingZerosCount(
		const uint32 a
	)
	{
		return _lzcnt_u32(a);
	}

	/// @brief	Count the number of bits set to 1
	inline uint32 PopulationCount(
		const uint32 a
	)
	{
		return _mm_popcnt_u32(a);
	}


	#if ARCH_IS_X64

	/// @brief	Count the number of leading bits set to 0
	/// @note   Available only on x64 targets
	inline uint64 LeadingZerosCount(
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _lzcnt_u64(a);
	}

	/// @brief	Count the number of bits set to 1
	/// @note   Available only on x64 targets
	inline uint64 PopulationCount(
		const uint64 a
	)
	{
		static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
		return _mm_popcnt_u64(a);
	}

	#endif
}
