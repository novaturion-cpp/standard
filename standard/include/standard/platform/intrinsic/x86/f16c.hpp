﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"
#include "standard/platform/intrinsic/type/vector256.hpp"
#include "standard/platform/intrinsic/x86/rounding-type.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// ph	| half-precision float (16-bit)
	/// ps	| single-precision float (32-bit)
	/// </pre>
	namespace F16C
	{
		/// @brief	Convert packed half-precision (16-bit) floating-point elements
		/// in a to packed single-precision (32-bit) floating-point elements
		inline Vector128 ConvertPhPs(
			const Vector128 a
		)
		{
			return _mm_cvtph_ps(a);
		}

		/// @brief	Convert packed half-precision (16-bit) floating-point elements
		/// in a to packed single-precision (32-bit) floating-point elements
		inline Vector256 ConvertPhPs256(
			const Vector128 a
		)
		{
			return _mm256_cvtph_ps(a);
		}

		/// @brief	Convert packed single-precision (32-bit) floating-point elements
		/// in a to packed half-precision (16-bit) floating-point elements
		/// @note	Exceptions can be suppressed by passing
		/// ERoundingType::NoExcept in the sae parameter
		template<ERoundingType VSae>
		inline Vector128 ConvertPsPh(
			const Vector128 a
		)
		{
			return _mm_cvtps_ph(a, static_cast<int32>(VSae));
		}

		/// @brief	Convert packed single-precision (32-bit) floating-point elements
		/// in a to packed half-precision (16-bit) floating-point elements
		/// @note	Exceptions can be suppressed by passing
		/// ERoundingType::NoExcept in the sae parameter
		template<ERoundingType VSae>
		inline Vector128 ConvertPsPh(
			const Vector256 a
		)
		{
			return _mm256_cvtps_ph(a, static_cast<int32>(VSae));
		}
	}
}
