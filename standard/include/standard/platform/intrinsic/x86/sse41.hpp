﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Sse41
	{
		/// @brief	Blend packed 16-bit integers
		/// from a and b using control mask imm8
		template<int32 VImm8>
		inline Vector128 BlendEpi16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_blend_epi16(a, b, VImm8);
		}

		/// @brief	Blend packed 8-bit
		/// integers from a and b using mask
		inline Vector128 BlendEpi8(
			const Vector128 a,
			const Vector128 b,
			const Vector128 mask
		)
		{
			return _mm_blendv_epi8(a, b, mask);
		}

		/// @brief	Blend packed double-precision (64-bit)
		/// floating-point elements from a and b using control mask imm8
		template<int32 VImm8>
		inline Vector128 BlendPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_blend_pd(a, b, VImm8);
		}

		/// @brief	Blend packed double-precision (64-bit)
		/// floating-point elements from a and b using mask
		inline Vector128 BlendPd(
			const Vector128 a,
			const Vector128 b,
			const Vector128 mask
		)
		{
			return _mm_blendv_pd(a, b, mask);
		}

		/// @brief	Blend packed single-precision (32-bit)
		/// floating-point elements from a and b using control mask imm8
		template<int32 VImm8>
		inline Vector128 BlendPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_blend_ps(a, b, VImm8);
		}

		/// @brief	Blend packed single-precision (32-bit)
		/// floating-point elements from a and b using mask
		inline Vector128 BlendPs(
			const Vector128 a,
			const Vector128 b,
			const Vector128 mask
		)
		{
			return _mm_blendv_ps(a, b, mask);
		}

		/// @brief	Round the packed double-precision (64-bit)
		/// floating-point elements in a up to an integer value
		inline Vector128 CeilPd(
			const Vector128 a
		)
		{
			return _mm_round_pd(a, static_cast<int32>(ERoundingType::UpNoExcept));
		}

		/// @brief	Round the packed double-precision (64-bit)
		/// floating-point elements in a up to an integer value
		inline Vector128 CeilPs(
			const Vector128 a
		)
		{
			return _mm_round_ps(a, static_cast<int32>(ERoundingType::UpNoExcept));
		}

		/// @brief	Round the lower double-precision (64-bit)
		/// floating-point element in b up to an integer value, store the
		/// result as a double-precision floating-point element in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 CeilSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_round_sd(a, b, static_cast<int32>(ERoundingType::UpNoExcept));
		}

		/// @brief	Round the lower single-precision (32-bit)
		/// floating-point element in b up to an integer value, store the
		/// result as a double-precision floating-point element in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 CeilSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_round_ss(a, b, static_cast<int32>(ERoundingType::UpNoExcept));
		}

		/// @brief	Compare packed 64-bit
		/// integers in a and b for equality
		inline Vector128 CompareEqualEpi64(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpeq_epi64(a, b);
		}

		/// @brief	Sign extend packed 16-bit
		/// integers in a to packed 32-bit integers
		inline Vector128 ConvertEpi16Epi32(
			const Vector128 a
		)
		{
			return _mm_cvtepi16_epi32(a);
		}

		/// @brief	Sign extend packed 16-bit
		/// integers in a to packed 64-bit integers
		inline Vector128 ConvertEpi16Epi64(
			const Vector128 a
		)
		{
			return _mm_cvtepi16_epi64(a);
		}

		/// @brief	Sign extend packed 32-bit
		/// integers in a to packed 64-bit integers
		inline Vector128 ConvertEpi32Epi64(
			const Vector128 a
		)
		{
			return _mm_cvtepi32_epi64(a);
		}

		/// @brief	Sign extend packed 8-bit
		/// integers in a to packed 16-bit integers
		inline Vector128 ConvertEpi8Epi16(
			const Vector128 a
		)
		{
			return _mm_cvtepi8_epi16(a);
		}

		/// @brief	Sign extend packed 8-bit
		/// integers in a to packed 32-bit integers
		inline Vector128 ConvertEpi8Epi32(
			const Vector128 a
		)
		{
			return _mm_cvtepi8_epi32(a);
		}

		/// @brief	Sign extend packed 8-bit integers
		/// in the low 8 bytes of a to packed 64-bit integers
		inline Vector128 ConvertEpi8Epi64(
			const Vector128 a
		)
		{
			return _mm_cvtepi8_epi64(a);
		}

		/// @brief	Zero extend packed unsigned 16-bit
		/// integers in a to packed 32-bit integers
		inline Vector128 ConvertEpu16Epi32(
			const Vector128 a
		)
		{
			return _mm_cvtepu16_epi32(a);
		}

		/// @brief	Zero extend packed unsigned 16-bit
		/// integers in a to packed 64-bit integers
		inline Vector128 ConvertEpu16Epi64(
			const Vector128 a
		)
		{
			return _mm_cvtepu16_epi64(a);
		}

		/// @brief	Zero extend packed unsigned 32-bit
		/// integers in a to packed 64-bit integers
		inline Vector128 ConvertEpu32Epi64(
			const Vector128 a
		)
		{
			return _mm_cvtepu32_epi64(a);
		}

		/// @brief	Zero extend packed unsigned 8-bit
		/// integers in a to packed 16-bit integers
		inline Vector128 ConvertEpu8Epi16(
			const Vector128 a
		)
		{
			return _mm_cvtepu8_epi16(a);
		}

		/// @brief	Zero extend packed unsigned 8-bit
		/// integers in a to packed 32-bit integers
		inline Vector128 ConvertEpu8Epi32(
			const Vector128 a
		)
		{
			return _mm_cvtepu8_epi32(a);
		}

		/// @brief	Zero extend packed unsigned 8-bit integers
		/// in the low 8 byte sof a to packed 64-bit integers
		inline Vector128 ConvertEpu8Epi64(
			const Vector128 a
		)
		{
			return _mm_cvtepu8_epi64(a);
		}

		/// @brief	Extract a 32-bit integer
		/// from a, selected with imm8
		template<int32 VImm8>
		inline int32 ExtractEpi32(
			const Vector128 a
		)
		{
			return _mm_extract_epi32(a, VImm8);
		}

		/// @brief	Extract an 8-bit integer from a, selected with
		/// imm8, and store the result in the lower element
		template<int32 VImm8>
		inline int32 ExtractEpi8(
			const Vector128 a
		)
		{
			return _mm_extract_epi8(a, VImm8);
		}

		/// @brief	Extract a single-precision (32-bit)
		/// floating-point element from a, selected with imm8
		template<int32 VImm8>
		inline int32 ExtractPs(
			const Vector128 a
		)
		{
			return _mm_extract_ps(a, VImm8);
		}

		/// @brief	Round the packed double-precision (64-bit)
		/// floating-point elements in a down to an integer value
		inline Vector128 FloorPd(
			const Vector128 a
		)
		{
			return _mm_round_pd(a, static_cast<int32>(ERoundingType::DownNoExcept));
		}

		/// @brief	Round the packed double-precision (64-bit)
		/// floating-point elements in a down to an integer value
		inline Vector128 FloorPs(
			const Vector128 a
		)
		{
			return _mm_round_ps(a, static_cast<int32>(ERoundingType::DownNoExcept));
		}

		/// @brief	Round the lower double-precision (64-bit)
		/// floating-point element in b down to an integer value, store the
		/// result as a double-precision floating-point element in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 FloorSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_round_sd(a, b, static_cast<int32>(ERoundingType::DownNoExcept));
		}

		/// @brief	Round the lower single-precision (32-bit)
		/// floating-point element in b down to an integer value, store the
		/// result as a double-precision floating-point element in the lower
		/// element, and copy the upper element from a to the upper element
		inline Vector128 FloorSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_round_ss(a, b, static_cast<int32>(ERoundingType::DownNoExcept));
		}

		/// @brief	Copy a, and insert the 32-bit integer
		/// i at the location specified by imm8
		template<int32 VImm8>
		inline Vector128 InsertEpi32(
			const Vector128 a,
			const int32 i
		)
		{
			return _mm_insert_epi32(a, i, VImm8);
		}

		/// @brief	Copy a, and insert the lower 8-bit integer
		/// from i at the location specified by imm8
		template<int32 VImm8>
		inline Vector128 InsertEpi8(
			const Vector128 a,
			const int32 i
		)
		{
			return _mm_insert_epi8(a, i, VImm8);
		}

		/// @brief	Copy a to tmp, then insert a single-precision (32-bit)
		/// floating-point element from b into tmp using the control in imm8\n
		/// Store tmp using the mask in imm8 (elements are
		/// zeroed out when the corresponding bit is set)
		template<int32 VImm8>
		inline Vector128 InsertPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_insert_ps(a, b, VImm8);
		}

		/// @brief	Compare packed signed 32-bit integers
		/// in a and b, and store packed maximum values
		inline Vector128 MaxEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_epi32(a, b);
		}

		/// @brief	Compare packed signed 8-bit integers
		/// in a and b, and store packed maximum values
		inline Vector128 MaxEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_epi8(a, b);
		}

		/// @brief	Compare packed unsigned 16-bit integers
		/// in a and b, and store packed maximum values
		inline Vector128 MaxEpu16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_epu16(a, b);
		}

		/// @brief	Compare packed unsigned 32-bit integers
		/// in a and b, and store packed maximum values
		inline Vector128 MaxEpu32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_max_epu32(a, b);
		}

		/// @brief	Compare packed signed 32-bit integers
		/// in a and b, and store packed minimum values
		inline Vector128 MinEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_epi32(a, b);
		}

		/// @brief	Compare packed signed 8-bit integers
		/// in a and b, and store packed minimum values
		inline Vector128 MinEpi8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_epi8(a, b);
		}

		/// @brief	Compare packed unsigned 16-bit integers
		/// in a and b, and store packed minimum values
		inline Vector128 MinEpu16(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_epu16(a, b);
		}

		/// @brief	Compare packed unsigned 32-bit integers
		/// in a and b, and store packed minimum values
		inline Vector128 MinEpu32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_min_epu32(a, b);
		}

		/// @brief	Horizontally compute the minimum amongst the
		/// packed unsigned 16-bit integers in a, store the
		/// minimum and index, and zero the remaining bits
		inline Vector128 MinIndexEpu16(
			const Vector128 a
		)
		{
			return _mm_minpos_epu16(a);
		}

		/// @brief	Multiply the low signed 32-bit integers from each packed
		/// 64-bit element in a and b, and store the signed 64-bit results
		inline Vector128 MultiplyEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mul_epi32(a, b);
		}

		/// @brief	Multiply the packed 32-bit integers in a
		/// and b, producing intermediate 64-bit integers, and
		/// store the low 32 bits of the intermediate integers
		inline Vector128 MultiplyLowEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mullo_epi32(a, b);
		}

		/// @brief	Conditionally multiply the packed double-precision (64-bit)
		/// floating-point elements in a and b using the high 4 bits in imm8, sum the
		/// four products, and conditionally store the sum using the low 4 bits of imm8
		template<int32 VImm8>
		inline Vector128 DotProductPd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_dp_pd(a, b, VImm8);
		}

		/// @brief	Conditionally multiply the packed single-precision (32-bit)
		/// floating-point elements in a and b using the high 4 bits in imm8, sum the
		/// four products, and conditionally store the sum using the low 4 bits of imm8
		template<int32 VImm8>
		inline Vector128 DotProductPs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_dp_ps(a, b, VImm8);
		}

		/// @brief	Convert packed signed 32-bit integers from a and
		/// b to packed 16-bit integers using unsigned saturation
		inline Vector128 PackUnsignedSaturationEpi32(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_packus_epi32(a, b);
		}

		/// @brief	Round the packed double-precision (64-bit) floating-point
		/// elements in a using the rounding parameter, and store the
		/// results as packed double-precision floating-point elements\n
		/// Rounding is done according to the rounding[3:0] parameter,
		/// which can be one of ERoundingType NoExcept fields
		template<ERoundingType VRounding>
		inline Vector128 RoundPd(
			const Vector128 a
		)
		{
			return _mm_round_pd(a, static_cast<int32>(VRounding));
		}

		/// @brief	Round the packed single-precision (32-bit) floating-point
		/// elements in a using the rounding parameter, and store the
		/// results as packed single-precision floating-point elements\n
		/// Rounding is done according to the rounding[3:0] parameter,
		/// which can be one of ERoundingType NoExcept fields
		template<ERoundingType VRounding>
		inline Vector128 RoundPs(
			const Vector128 a
		)
		{
			return _mm_round_ps(a, static_cast<int32>(VRounding));
		}

		/// @brief	Round the lower double-precision (64-bit) floating-point
		/// element in b using the rounding parameter, store the result as
		/// a double-precision floating-point element in the lower element,
		/// and copy the upper element from a to the upper element\n
		/// Rounding is done according to the rounding[3:0] parameter,
		/// which can be one of ERoundingType NoExcept fields
		template<ERoundingType VRounding>
		inline Vector128 RoundSd(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_round_sd(a, b, static_cast<int32>(VRounding));
		}

		/// @brief	Round the lower single-precision (32-bit) floating-point
		/// element in b using the rounding parameter, store the result as
		/// a single-precision floating-point element in the lower element,
		/// and copy the upper 3 packed elements from a to the upper elements\n
		/// Rounding is done according to the rounding[3:0] parameter,
		/// which can be one of ERoundingType NoExcept fields
		template<ERoundingType VRounding>
		inline Vector128 RoundSs(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_round_ss(a, b, static_cast<int32>(VRounding));
		}

		/// @brief	Load 128-bits of integer data from
		/// memory using a non-temporal memory hint
		/// @note	Address must be aligned on a 16-byte boundary
		/// or a general-protection exception may be generated
		inline Vector128 StreamLoadSi128(
			Vector128* address
		)
		{
			return _mm_stream_load_si128(*address);
		}

		/// @brief	Compute the sum of absolute differences
		/// (SADs) of quadruplets of unsigned 8-bit integers in a
		/// compared to those in b, and store the 16-bit results\n
		/// Eight SADs are performed using one quadruplet
		/// from b and eight quadruplets from a\n
		/// One quadruplet is selected from b starting
		/// at on the offset specified in imm8\n
		/// Eight quadruplets are formed from sequential 8-bit integers
		/// selected from a starting at the offset specified in imm8
		template<int32 VImm8>
		inline Vector128 SumAbsDifferenceEpu8(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_mpsadbw_epu8(a, b, VImm8);
		}

		/// @brief	Compute the bitwise NOT of a and then
		/// AND with a 128-bit vector containing all 1's
		/// @return	'true' if the result is zero
		inline bool TestAllOnes(
			const Vector128 a
		)
		{
			return static_cast<bool>(_mm_testc_si128(a, _mm_cmpeq_epi32(a, a)));
		}

		/// @brief	Compute the bitwise AND of 128 bits
		/// (representing integer data) in a and b, and set ZF
		/// to 1 if the result is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, and
		/// set CF to 1 if the result is zero, otherwise set CF to 0
		/// @return	ZF value
		inline bool TestAllZeros(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testz_si128(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits
		/// (representing integer data) in a and b, and set ZF
		/// to 1 if the result is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, and
		/// set CF to 1 if the result is zero, otherwise set CF to 0
		/// @return	CF value
		inline bool TestCarryFlagSi128(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testc_si128(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits
		/// (representing integer data) in a and b, and set ZF
		/// to 1 if the result is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, and set
		/// CF to 1 if the result is zero, otherwise set CF to 0
		/// @return	'true' if both the ZF and CF values are zero
		inline bool TestMixOnesZeros(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testnzc_si128(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits
		/// (representing integer data) in a and b, and set ZF
		/// to 1 if the result is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, and set
		/// CF to 1 if the result is zero, otherwise set CF to 0
		/// @return	'true' if both the ZF and CF values are zero
		inline bool TestNotZeroCarryFlagSi128(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testnzc_si128(a, b));
		}

		/// @brief	Compute the bitwise AND of 128 bits
		/// (representing integer data) in a and b, and set ZF
		/// to 1 if the result is zero, otherwise set ZF to 0\n
		/// Compute the bitwise NOT of a and then AND with b, and
		/// set CF to 1 if the result is zero, otherwise set CF to 0
		/// @return	ZF value
		inline bool TestZeroFlagSi128(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_testz_si128(a, b));
		}


		#if ARCH_IS_X64

		/// @brief	Extract a 64-bit integer
		/// from a, selected with imm8
	    /// @note   Available only on x64 targets
		template<int32 VImm8>
		inline int64 ExtractEpi64(
			const Vector128 a
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_extract_epi64(a, VImm8);
		}

		/// @brief	Copy a, and insert the 64-bit integer
		/// i at the location specified by imm8
	    /// @note   Available only on x64 targets
		template<int32 VImm8>
		inline Vector128 InsertEpi64(
			const Vector128 a,
			const int64 i
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_insert_epi64(a, i, VImm8);
		}

		#endif
	}
}
