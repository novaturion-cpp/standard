﻿#pragma once
#include "standard/platform/intrinsic/type/vector128.hpp"

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic
{
	/// @brief	Types:
	/// <pre>
	/// epi8	| packed signed 8-bit integer
	/// epi16	| packed signed 16-bit integer
	/// epi32	| packed signed 32-bit integer
	/// epi64	| packed signed 64-bit integer
	/// epu8	| packed unsigned 8-bit integer
	/// epu16	| packed unsigned 16-bit integer
	/// epu32	| packed unsigned 32-bit integer
	/// epu64	| packed unsigned 64-bit integer
	/// 
	/// si8	| scalar signed 8-bit integer
	/// si16	| scalar signed 16-bit integer
	/// si32	| scalar signed 32-bit integer
	/// si64	| scalar signed 64-bit integer
	/// si128	| scalar signed 128-bit integer
	/// su8	| scalar unsigned 8-bit integer
	/// su16	| scalar unsigned 16-bit integer
	/// su32	| scalar unsigned 32-bit integer
	/// su64	| scalar unsigned 64-bit integer
	/// su128	| scalar unsigned 128-bit integer
	/// 
	/// ps	| packed single-precision float (32-bit)
	/// pd	| packed double-precision float (64-bit)
	/// ss	| scalar single-precision float (32-bit)
	/// sd	| scalar double-precision float (64-bit)
	/// </pre>
	namespace Sse42
	{
		struct Comparison
		{
			// todo: replace with enums?

			struct Index
			{
				/// @brief	Return last significant bit
				static inline constexpr uint8 LeastSignificant = 0x00;
				/// @brief	Return most significant bit
				static inline constexpr uint8 MostSignificant = 0x40;
			};


			struct Mask
			{
				/// @brief	Return bit mask
				static inline constexpr uint8 BitMask = 0x00;
				/// @brief	Return byte/word mask
				static inline constexpr uint8 UnitMask = 0x40;
			};


			struct Type
			{
				static inline constexpr uint8 Ranges = 0x04;
				static inline constexpr uint8 EqualAny = 0x00;
				static inline constexpr uint8 EqualEach = 0x08;
				static inline constexpr uint8 EqualOrdered = 0x0C;
			};


			struct Polarity
			{
				/// @brief	todo: add documentation
				static inline constexpr uint8 Positive = 0x00;
				/// @brief	Negate results
				static inline constexpr uint8 Negative = 0x10;
				/// @brief	todo: add documentation
				static inline constexpr uint8 MaskedPositive = 0x20;
				/// @brief	Negate results only before end of string
				static inline constexpr uint8 MaskedNegative = 0x30;
			};


			struct Unit
			{
				/// @brief	Unsigned 8-bit characters
				static inline constexpr uint8 Unsigned8 = 0x00;
				/// @brief	Unsigned 16-bit characters
				static inline constexpr uint8 Unsigned16 = 0x01;
				/// @brief	Signed 8-bit characters
				static inline constexpr uint8 Signed8 = 0x02;
				/// @brief	Signed 16-bit characters
				static inline constexpr uint8 Signed16 = 0x03;
			};
		};


		/// @brief	Compare packed signed 64-bit
		/// integers in a and b for greater-than
		inline Vector128 CompareGreaterEpi64(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpgt_epi64(a, b);
		}

		/// @brief	Starting with the initial value in crc,
		/// accumulate a CRC32 value for unsigned 8-bit integer v
		inline uint32 Crc32(
			const uint32 crc,
			const uint8 v
		)
		{
			return _mm_crc32_u8(crc, v);
		}

		/// @brief	Starting with the initial value in crc,
		/// accumulate a CRC32 value for unsigned 16-bit integer v
		inline uint32 Crc32(
			const uint32 crc,
			const uint16 v
		)
		{
			return _mm_crc32_u16(crc, v);
		}

		/// @brief	Starting with the initial value in crc,
		/// accumulate a CRC32 value for unsigned 32-bit integer v
		inline uint32 Crc32(
			const uint32 crc,
			const uint32 v
		)
		{
			return _mm_crc32_u32(crc, v);
		}

		/// @brief	Compare packed strings in a
		/// and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	'true' if the resulting mask was non-zero
		template<int32 VImm8>
		inline bool IsMaskNotZero(
			const Vector128 a,
			const Vector128 b,
			const int32 aLength,
			const int32 bLength
		)
		{
			return static_cast<bool>(_mm_cmpestrc(a, aLength, b, bLength, VImm8));
		}

		/// @brief	Compare packed strings with implicit
		/// lengths in a and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	'true' if the resulting mask was non-zero
		template<int32 VImm8>
		inline bool IsMaskNotZero(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_cmpistrc(a, b, VImm8));
		}

		/// @brief	Compare packed strings in a
		/// and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	'true' if b did not contain a null
		/// character and the resulting mask was zero
		template<int32 VImm8>
		inline bool IsNotNullBMaskNotZero(
			const Vector128 a,
			const Vector128 b,
			const int32 aLength,
			const int32 bLength
		)
		{
			return static_cast<bool>(_mm_cmpestra(a, aLength, b, bLength, VImm8));
		}

		/// @brief	Compare packed strings with implicit
		/// lengths in a and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	'true' if b did not contain a null
		/// character and the resulting mask was zero
		template<int32 VImm8>
		inline bool IsNotNullBMaskNotZero(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_cmpistra(a, b, VImm8));
		}

		/// @brief	Compare packed strings in a
		/// and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	'true' if any character in a was null
		template<int32 VImm8>
		inline bool IsNullA(
			const Vector128 a,
			const Vector128 b,
			const int32 aLength,
			const int32 bLength
		)
		{
			return static_cast<bool>(_mm_cmpestrs(a, aLength, b, bLength, VImm8));
		}

		/// @brief	Compare packed strings with implicit
		/// lengths in a and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	'true' if any character in a was null
		template<int32 VImm8>
		inline bool IsNullA(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_cmpistrs(a, b, VImm8));
		}

		/// @brief	Compare packed strings in a
		/// and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	'true' if any character in b was null
		template<int32 VImm8>
		inline bool IsNullB(
			const Vector128 a,
			const Vector128 b,
			const int32 aLength,
			const int32 bLength
		)
		{
			return static_cast<bool>(_mm_cmpestrz(a, aLength, b, bLength, VImm8));
		}

		/// @brief	Compare packed strings with implicit
		/// lengths in a and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	'true' if any character in b was null
		template<int32 VImm8>
		inline bool IsNullB(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<bool>(_mm_cmpistrz(a, b, VImm8));
		}

		/// @brief	Compare packed strings in a
		/// and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	Generated index
		template<int32 VImm8>
		inline int32 StringCompareIndex(
			const Vector128 a,
			const Vector128 b,
			const int32 aLength,
			const int32 bLength
		)
		{
			return _mm_cmpestri(a, aLength, b, bLength, VImm8);
		}

		/// @brief	Compare packed strings with implicit
		/// lengths in a and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	Generated index
		template<int32 VImm8>
		inline int32 StringCompareIndex(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpistri(a, b, VImm8);
		}

		/// @brief	Compare packed strings in a
		/// and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	Generated mask
		template<int32 VImm8>
		inline Vector128 StringCompareMask(
			const Vector128 a,
			const Vector128 b,
			const int32 aLength,
			const int32 bLength
		)
		{
			return _mm_cmpestrm(a, aLength, b, bLength, VImm8);
		}

		/// @brief	Compare packed strings with implicit
		/// lengths in a and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	Generated mask
		template<int32 VImm8>
		inline Vector128 StringCompareMask(
			const Vector128 a,
			const Vector128 b
		)
		{
			return _mm_cmpistrm(a, b, VImm8);
		}

		/// @brief	Compare packed strings in a
		/// and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	Bit 0 of the resulting bit mask
		template<int32 VImm8>
		inline int8 StringCompareMaskFirstBit(
			const Vector128 a,
			const Vector128 b,
			const int32 aLength,
			const int32 bLength
		)
		{
			return static_cast<int8>(_mm_cmpestro(a, aLength, b, bLength, VImm8));
		}

		/// @brief	Compare packed strings with implicit
		/// lengths in a and b using the control in imm8\n
		/// imm8 can be a combination of:
		/// - Comparison::Index
		/// - Comparison::Mask
		/// - Comparison::Polarity::Negative, Comparison::Polarity::MaskedNegative
		/// - Comparison::Type
		/// - Comparison::Unit
		/// @return	Bit 0 of the resulting bit mask
		template<int32 VImm8>
		inline int8 StringCompareMaskFirstBit(
			const Vector128 a,
			const Vector128 b
		)
		{
			return static_cast<int8>(_mm_cmpistro(a, b, VImm8));
		}


		#if ARCH_IS_X64

		/// @brief	Starting with the initial value in crc,
		/// accumulate a CRC32 value for unsigned 64-bit integer v
	    /// @note   Available only on x64 targets
		inline uint64 Crc32(
			const uint64 crc,
			const uint64 v
		)
		{
			static_assert(ARCH_IS_X64, "Attempt to use x64 only intrinsic on non-x64 target");
			return _mm_crc32_u64(crc, v);
		}

		#endif
	}
}
