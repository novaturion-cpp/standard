﻿#pragma once

#if !(ARCH_IS_X64 || ARCH_IS_X86)
#error Attempt to include x86/x64 only header on non-x86/x64 target
#endif

namespace Std::inline Platform::inline Intrinsic::InvPcid
{
	/// @brief	Invalidate mappings in the Translation Lookaside Buffers (TLBs)
	/// and paging-structure caches for the processor context identifier (PCID)
	/// specified by descriptor based on the invalidation type specified in type\n
	/// The PCID descriptor is specified as a 16-byte memory operand (with no
	/// alignment restrictions) where bits [11:0] specify the PCID, and bits
	/// [127:64] specify the linear address; bits [63:12] are reserved\n
	/// The types supported are:
	/// - Individual-address invalidation: If type is 0, the logical processor
	/// invalidates mappings for a single linear address and tagged with the PCID
	/// specified in descriptor, except global translations. The instruction may
	/// also invalidate global translations, mappings for other linear addresses,
	/// or mappings tagged with other PCIDs
	/// - Single-context invalidation: If type is 1, the logical processor invalidates
	/// all mappings tagged with the PCID specified in descriptor except global translations
	/// In some cases, it may invalidate mappings for other PCIDs as well
	/// - All-context invalidation: If type is 2, the logical
	/// processor invalidates all mappings tagged with any PCID
	/// - All-context invalidation, retaining global translations: If type
	/// is 3, the logical processor invalidates all mappings tagged with any
	/// PCID except global translations, ignoring descriptor\n
	/// The instruction may also invalidate global translations as well
	inline void InvalidatePcid(
		const uint32 type,
		void* descriptor
	)
	{
		_invpcid(type, descriptor);
	}
}
