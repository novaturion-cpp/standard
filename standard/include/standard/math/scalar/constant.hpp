﻿#pragma once
#include "standard/platform/type/float.hpp"

// todo: infinity, nan, epsilon, etc.

namespace Std::inline Math::inline Scalar
{
	template<class T = float32>
	inline constexpr auto E = static_cast<T>(2.71828182845904523536028747135266249775724709369996_f128);

	template<class T = float32>
	inline constexpr auto Euler = static_cast<T>(0.5772156649015328606065120900824024310421593359399236_f128);

	template<class T = float32>
	inline constexpr auto Gauss = static_cast<T>(0.834626841674073186281429732799046808993993013490347_f128);

	template<class T = float32>
	inline constexpr auto Phi = static_cast<T>(1.618033988749894848204586834365638117720309179805763_f128);

	template<class T = float32>
	inline constexpr auto Pi = static_cast<T>(3.141592653589793238462643383279502884197169399375106_f128);


	template<class T = float32>
	inline constexpr auto OneDiv2 = static_cast<T>(0.5_f128);

	template<class T = float32>
	inline constexpr auto OneDiv2Pi = static_cast<T>(0.1591549430918953357688837633725143620344596457404564_f128);

	template<class T = float32>
	inline constexpr auto OneDiv3 = static_cast<T>(0.3333333333333333333333333333333333333333333333333333_f128);

	template<class T = float32>
	inline constexpr auto OneDiv4 = static_cast<T>(0.25_f128);

	template<class T = float32>
	inline constexpr auto OneDiv6 = static_cast<T>(0.1666666666666666666666666666666666666666666666666667_f128);

	template<class T = float32>
	inline constexpr auto OneDivEuler = static_cast<T>(1.732454714600633473583025315860829681155776552266805_f128);

	template<class T = float32>
	inline constexpr auto OneDivPi = static_cast<T>(0.3183098861837906715377675267450287240689192914809129_f128);

	template<class T = float32>
	inline constexpr auto OneDivRoot2 = static_cast<T>(0.707106781186547524400844362104849039284835937688474_f128);

	template<class T = float32>
	inline constexpr auto OneDivRoot3 = static_cast<T>(0.5773502691896257645091487805019574556476017512701269_f128);

	template<class T = float32>
	inline constexpr auto ThreeDiv4 = static_cast<T>(0.75_f128);

	template<class T = float32>
	inline constexpr auto TwoDiv3 = static_cast<T>(0.6666666666666666666666666666666666666666666666666667_f128);

	template<class T = float32>
	inline constexpr auto TwoDivPi = static_cast<T>(0.6366197723675813430755350534900574481378385829618258_f128);


	template<class T = float32>
	inline constexpr auto Root2 = static_cast<T>(1.414213562373095048801688724209698078569671875376948_f128);

	template<class T = float32>
	inline constexpr auto Root3 = static_cast<T>(1.732050807568877293527446341505872366942805253810381_f128);

	template<class T = float32>
	inline constexpr auto RootE = static_cast<T>(1.648721270700128146848650787814163571653776100710148_f128);

	template<class T = float32>
	inline constexpr auto RootPi = static_cast<T>(1.772453850905516027298167483341145182797549456122387_f128);

	template<class T = float32>
	inline constexpr auto RootPiMul2 = static_cast<T>(2.506628274631000502415765284811045253006986740609938_f128);


	template<class T = float32>
	inline constexpr auto Ln10 = static_cast<T>(2.302585092994045684017991454684364207601101488628773_f128);

	template<class T = float32>
	inline constexpr auto Ln2 = static_cast<T>(0.6931471805599453094172321214581765680755001343602553_f128);

	template<class T = float32>
	inline constexpr auto LnLn2 = static_cast<T>(-0.3665129205816643270124391582326694694542634478371053_f128);

	template<class T = float32>
	inline constexpr auto LnPhi = static_cast<T>(0.4812118250596034474977589134243684231351843343856605_f128);

	template<class T = float32>
	inline constexpr auto Log10E = static_cast<T>(0.4342944819032518276511289189166050822943970058036666_f128);

	template<class T = float32>
	inline constexpr auto Log2E = static_cast<T>(1.442695040888963407359924681001892137426645954152986_f128);


	template<class T = float32>
	inline constexpr auto EPowPi = static_cast<T>(23.14069263277926900572908636794854738026610624260021_f128);

	template<class T = float32>
	inline constexpr auto EulerPow2 = static_cast<T>(0.3331779238077186743183761363552442266594171402496297_f128);


	template<class T = float32>
	inline constexpr auto DegreesToRadians = static_cast<T>(
		0.01745329251994329576923690768488612713442871888541725_f128);

	template<class T = float32>
	inline constexpr auto RadiansToDegrees = static_cast<T>(57.29577951308232087679815481410517033240547246656432_f128);


	template<class T = float32>
	inline constexpr auto PiDiv2 = static_cast<T>(1.570796326794896619231321691639751442098584699687553_f128);

	template<class T = float32>
	inline constexpr auto PiDiv3 = static_cast<T>(1.047197551196597746154214461093167628065723133125035_f128);

	template<class T = float32>
	inline constexpr auto PiDiv4 = static_cast<T>(0.7853981633974483096156608458198757210492923498437765_f128);

	template<class T = float32>
	inline constexpr auto PiDiv6 = static_cast<T>(0.5235987755982988730771072305465838140328615665625176_f128);

	template<class T = float32>
	inline constexpr auto PiPow2 = static_cast<T>(9.869604401089358618834490999876151135313699407240791_f128);

	template<class T = float32>
	inline constexpr auto PiPowE = static_cast<T>(22.45915771836104547342715220454373502758931513399669_f128);

	template<class T = float32>
	inline constexpr auto PiMul2 = static_cast<T>(6.283185307179586476925286766559005768394338798750212_f128);

	template<class T = float32>
	inline constexpr auto PiMul3Div4 = static_cast<T>(2.356194490192344928846982537459627163147877049531329_f128);


	template<class T = float32>
	inline constexpr auto Cos0 = static_cast<T>(1.0_f128);

	template<class T = float32>
	inline constexpr auto Cos15 = static_cast<T>(0.9659258262890682867497431997288973676339048390084046_f128);

	template<class T = float32>
	inline constexpr auto Cos30 = static_cast<T>(0.8660254037844386467637231707529361834714026269051903_f128);

	template<class T = float32>
	inline constexpr auto Cos45 = static_cast<T>(0.707106781186547524400844362104849039284835937688474_f128);

	template<class T = float32>
	inline constexpr auto Cos60 = static_cast<T>(0.5_f128);

	template<class T = float32>
	inline constexpr auto Cos75 = static_cast<T>(0.2588190451025207623488988376240483283490689013199305_f128);

	template<class T = float32>
	inline constexpr auto Cos90 = static_cast<T>(0.0_f128);


	template<class T = float32>
	inline constexpr auto Sin0 = static_cast<T>(0.0_f128);

	template<class T = float32>
	inline constexpr auto Sin15 = static_cast<T>(0.2588190451025207623488988376240483283490689013199305_f128);

	template<class T = float32>
	inline constexpr auto Sin30 = static_cast<T>(0.5_f128);

	template<class T = float32>
	inline constexpr auto Sin45 = static_cast<T>(0.707106781186547524400844362104849039284835937688474_f128);

	template<class T = float32>
	inline constexpr auto Sin60 = static_cast<T>(0.8660254037844386467637231707529361834714026269051903_f128);

	template<class T = float32>
	inline constexpr auto Sin75 = static_cast<T>(0.9659258262890682867497431997288973676339048390084046_f128);

	template<class T = float32>
	inline constexpr auto Sin90 = static_cast<T>(1.0_f128);


	template<class T = float32>
	inline constexpr auto Tan0 = static_cast<T>(0.0_f128);

	template<class T = float32>
	inline constexpr auto Tan15 = static_cast<T>(0.2679491924311227064725536584941276330571947461896194_f128);

	template<class T = float32>
	inline constexpr auto Tan30 = static_cast<T>(0.5773502691896257645091487805019574556476017512701269_f128);

	template<class T = float32>
	inline constexpr auto Tan45 = static_cast<T>(1.0_f128);

	template<class T = float32>
	inline constexpr auto Tan60 = static_cast<T>(1.732050807568877293527446341505872366942805253810381_f128);

	template<class T = float32>
	inline constexpr auto Tan75 = static_cast<T>(3.732050807568877293527446341505872366942805253810381_f128);


	template<class T = float32>
	inline constexpr auto TaylorCoefficient0 = static_cast<T>(1.0 / 2.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient1 = static_cast<T>(1.0 / 6.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient2 = static_cast<T>(1.0 / 24.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient3 = static_cast<T>(1.0 / 120.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient4 = static_cast<T>(1.0 / 720.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient5 = static_cast<T>(1.0 / 5040.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient6 = static_cast<T>(1.0 / 40320.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient7 = static_cast<T>(1.0 / 362880.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient8 = static_cast<T>(1.0 / 3628800.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient9 = static_cast<T>(1.0 / 39916800.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient10 = static_cast<T>(1.0 / 479001600.0_f128);

	template<class T = float32>
	inline constexpr auto TaylorCoefficient11 = static_cast<T>(1.0 / 6227020800.0_f128);
}
