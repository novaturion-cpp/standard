﻿#pragma once
#include "standard/utility/type-traits.hpp"

// todo: math_errhandling

namespace Std::inline Math::inline Scalar
{
	/// @brief	Compute absolute value of an argument (|x|)
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline TNumeric Abs(
		const TNumeric x
	)
	{
		return static_cast<TNumeric>(std::abs(x));
	}

	/// @brief	Compute absolute value of an argument (|x|) as unsigned int
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingUint<TNumeric> AbsUnsigned(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingUint<TNumeric>>(std::abs(x));
	}

	/// @brief	Compute the positive difference between x and y
	/// @return	x - y if x > y, +0 otherwise
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> DifferenceOrZero(
		const TNumeric x,
		const TNumeric y
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::fdim(x, y));
	}

	/// @brief	Compute signed remainder of the division
	/// operation according to IEEE 754 float specification
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> IeeeRemainder(
		const TNumeric x,
		const TNumeric y
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::remainder(x, y));
	}

	/// @brief	Compute signed remainder of the division
	/// operation according to IEEE 754 float specification
	/// and three last bits of the division operation
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> IeeeRemainderQuotient(
		int32* outQuotient,
		const TNumeric x,
		const TNumeric y
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::remquo(x, y, outQuotient));
	}

	/// @brief	Compute the larger of arguments
	/// @note	between a NaN and a numeric
	/// value, the numeric value is chosen
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	constexpr TNumeric Max(
		const TNumeric x,
		const TNumeric y
	)
	{
		if constexpr (IsFloatValue<TNumeric>)
		{
			return static_cast<TNumeric>(std::fmax(x, y));
		}
		else
		{
			return x > y
				? x
				: y;
		}
	}

	/// @brief	Compute the larger of arguments
	/// @note	between a NaN and a numeric
	/// value, the numeric value is chosen
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	constexpr TNumeric Max(
		const TNumeric x,
		const TNumeric y,
		const TNumeric z
	)
	{
		if constexpr (IsFloatValue<TNumeric>)
		{
			return static_cast<TNumeric>(std::fmax(x, std::fmax(y, z)));
		}
		else
		{
			return x > y && x > z
				? x
				: y > x && y > z
				? y
				: z;
		}
	}

	/// @brief	Compute the smaller of arguments
	/// @note	between a NaN and a numeric
	/// value, the numeric value is chosen
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	constexpr TNumeric Min(
		const TNumeric x,
		const TNumeric y
	)
	{
		if constexpr (IsFloatValue<TNumeric>)
		{
			return static_cast<TNumeric>(std::fmin(x, y));
		}
		else
		{
			return x < y
				? x
				: y;
		}
	}

	/// @brief	Compute the larger of arguments
	/// @note	between a NaN and a numeric
	/// value, the numeric value is chosen
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	constexpr TNumeric Min(
		const TNumeric x,
		const TNumeric y,
		const TNumeric z
	)
	{
		if constexpr (IsFloatValue<TNumeric>)
		{
			return static_cast<TNumeric>(std::fmin(x, std::fmin(y, z)));
		}
		else
		{
			return x < y && x < z
				? x
				: y < x && y < z
				? y
				: z;
		}
	}

	/// @brief	Compute remainder of the division operation
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline TNumeric Mod(
		const TNumeric x,
		const TNumeric y
	)
	{
		if constexpr (IsFloatValue<TNumeric>)
		{
			return static_cast<TNumeric>(std::fmod(x, y));
		}
		else
		{
			return x % y;
		}
	}

	/// @brief	 Compute (x * y) + z as if to infinite precision
	/// and rounded only once to fit the result type
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> MultiplyAdd(
		const TNumeric x,
		const TNumeric y,
		const TNumeric z
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::fma(x, y, z));
	}
}
