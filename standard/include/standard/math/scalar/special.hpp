﻿#pragma once
#include "standard/utility/type-traits.hpp"


namespace Std::inline Math::inline Scalar
{
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat BesselCylindrical(
		const TFloat order,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::cyl_bessel_jf(order, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::cyl_bessel_j(order, x);
		}
		else
		{
			return std::cyl_bessel_jl(order, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat BesselCylindricalIrregular(
		const TFloat order,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::cyl_bessel_kf(order, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::cyl_bessel_k(order, x);
		}
		else
		{
			return std::cyl_bessel_kl(order, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat BesselCylindricalRegular(
		const TFloat order,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::cyl_bessel_if(order, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::cyl_bessel_i(order, x);
		}
		else
		{
			return std::cyl_bessel_il(order, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat BesselSpherical(
		const TFloat order,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::sph_besself(order, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::sph_bessel(order, x);
		}
		else
		{
			return std::sph_bessell(order, x);
		}
	}

	/// @brief	 Compute complete elliptic integral of the first kind
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat EllipticIntegral1Complete(
		const TFloat eccentricity
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::comp_ellint_1f(eccentricity);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::comp_ellint_1(eccentricity);
		}
		else
		{
			return std::comp_ellint_1l(eccentricity);
		}
	}

	/// @brief	 Compute complete elliptic integral of the second kind
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat EllipticIntegral2Complete(
		const TFloat eccentricity
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::comp_ellint_2f(eccentricity);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::comp_ellint_2(eccentricity);
		}
		else
		{
			return std::comp_ellint_2l(eccentricity);
		}
	}

	/// @brief	 Compute complete elliptic integral of the third kind
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat EllipticIntegral3Complete(
		const TFloat eccentricity,
		const TFloat characteristic
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::comp_ellint_3f(eccentricity, characteristic);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::comp_ellint_3(eccentricity, characteristic);
		}
		else
		{
			return std::comp_ellint_3l(eccentricity, characteristic);
		}
	}

	/// @brief	 Compute incomplete elliptic integral of the first kind
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat EllipticIntegral1Incomplete(
		const TFloat eccentricity,
		const TFloat radians
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::ellint_1f(eccentricity, radians);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::ellint_1(eccentricity, radians);
		}
		else
		{
			return std::ellint_1l(eccentricity, radians);
		}
	}

	/// @brief	 Compute incomplete elliptic integral of the second kind
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat EllipticIntegral2Incomplete(
		const TFloat eccentricity,
		const TFloat radians
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::ellint_2f(eccentricity, radians);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::ellint_2(eccentricity, radians);
		}
		else
		{
			return std::ellint_2l(eccentricity, radians);
		}
	}

	/// @brief	 Compute incomplete elliptic integral of the third kind
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat EllipticIntegral3Incomplete(
		const TFloat eccentricity,
		const TFloat characteristic,
		const TFloat radians
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::ellint_3f(eccentricity, characteristic, radians);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::ellint_3(eccentricity, characteristic, radians);
		}
		else
		{
			return std::ellint_3l(eccentricity, characteristic, radians);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Error(
		const TFloat x
	)
	{
		return std::erf(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat ErrorComplementary(
		const TFloat x
	)
	{
		return std::erfc(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat EulerBeta(
		const TFloat x,
		const TFloat y
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::betaf(x, y);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::beta(x, y);
		}
		else
		{
			return std::betal(x, y);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat ExponentialIntegral(
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::expintf(x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::expint(x);
		}
		else
		{
			return std::expintl(x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Gamma(
		const TFloat x
	)
	{
		return std::tgamma(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Hermite(
		const uint32 degree,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::hermitef(degree, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::hermite(degree, x);
		}
		else
		{
			return std::hermitel(degree, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Laguerre(
		const uint32 degree,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::laguerref(degree, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::laguerre(degree, x);
		}
		else
		{
			return std::laguerrel(degree, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat LaguerreAssociated(
		const uint32 degree,
		const uint32 order,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::assoc_laguerref(degree, order, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::assoc_laguerre(degree, order, x);
		}
		else
		{
			return std::assoc_laguerrel(degree, order, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Legendre(
		const uint32 degree,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::legendref(degree, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::legendre(degree, x);
		}
		else
		{
			return std::legendrel(degree, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat LegendreAssociated(
		const uint32 degree,
		const uint32 order,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::assoc_legendref(degree, order, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::assoc_legendre(degree, order, x);
		}
		else
		{
			return std::assoc_legendrel(degree, order, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat LegendreSpherical(
		const uint32 degree,
		const uint32 order,
		const TFloat radians
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::sph_legendref(degree, order, radians);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::sph_legendre(degree, order, radians);
		}
		else
		{
			return std::sph_legendrel(degree, order, radians);
		}
	}

	/// @brief	Compute natural logarithm of gamma function of an argument
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat LnGamma(
		const TFloat x
	)
	{
		return std::lgamma(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat NeumannCylindrical(
		const TFloat order,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::cyl_neumannf(order, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::cyl_neumann(order, x);
		}
		else
		{
			return std::cyl_neumannl(order, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat NeumannSpherical(
		const TFloat order,
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::sph_neumannf(order, x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::sph_neumann(order, x);
		}
		else
		{
			return std::sph_neumannl(order, x);
		}
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat RiemannZeta(
		const TFloat x
	)
	{
		if constexpr (IsSameWeakValue<TFloat, float32>)
		{
			return std::riemann_zetaf(x);
		}
		else if constexpr (IsSameWeakValue<TFloat, float64>)
		{
			return std::riemann_zeta(x);
		}
		else
		{
			return std::riemann_zetal(x);
		}
	}
}
