﻿#pragma once
#include "standard/math/type/float-category.hpp"
#include "standard/utility/type-traits.hpp"


namespace Std::inline Math::inline Scalar
{
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline EFloatCategory CategorizeFloat(
		const TFloat x
	)
	{
		return static_cast<EFloatCategory>(std::fpclassify(x));
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsFinite(
		const TFloat x
	)
	{
		return std::isfinite(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsInfinite(
		const TFloat x
	)
	{
		return std::isinf(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsNan(
		const TFloat x
	)
	{
		return std::isnan(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsNegative(
		const TFloat x
	)
	{
		return std::signbit(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsNormal(
		const TFloat x
	)
	{
		return std::isnormal(x);
	}

	/// @brief	Decompose an argument into
	/// significand and a power of 2
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool DecomposeFractionExponent(
		int32* outExponent,
		TFloat* outFraction,
		const TFloat x
	)
	{
		*outFraction = std::frexp(x, outExponent);
		return false;
	}

	/// @brief	Decompose an argument into
	/// integer and fractional parts
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool DecomposeFractionInteger(
		TFloat* outInteger,
		TFloat* outFraction,
		const TFloat x
	)
	{
		*outFraction = std::modf(x, outInteger);
		return false;
	}

	/// @brief	Multiply an argument by the number
	/// 2 raised to the given power (x * 2^power)
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat ComputeExponent(
		const TFloat x,
		const int32 power
	)
	{
		return std::ldexp(x, power);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat ExtractExponent(
		const TFloat x
	)
	{
		return std::logb(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline int32 ExtractExponentInt(
		const TFloat x
	)
	{
		return std::ilogb(x);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat CopySign(
		const TFloat x,
		const TFloat sign
	)
	{
		return std::copysign(x, sign);
	}

	/// @brief Compute the next representable value
	/// of and argument in the given direction
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat NextAfter(
		const TFloat x,
		const TFloat direction
	)
	{
		return std::nextafter(x, direction);
	}

	/// @brief Compute the next representable value
	/// of and argument in the given direction
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat NextToward(
		const TFloat x,
		const TFloat direction
	)
	{
		return std::nexttoward(x, direction);
	}

	/// @brief	Convert the character string into the corresponding
	/// quiet NaN value, as if by calling ToFloat32(String),
	/// ToFloat64(String), or ToFloat128(String), respectively
	template<class TFloat = float32>
		requires (IsFloatValue<TFloat>)
	inline TFloat ToNan(
		const char* x
	)
	{
		return static_cast<TFloat>(std::nan(x));
	}
}
