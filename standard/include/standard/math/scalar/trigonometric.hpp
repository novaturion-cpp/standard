﻿#pragma once
#include "constant.hpp"
#include "standard/utility/type-traits.hpp"


namespace Std::inline Math::inline Scalar
{
	/// @brief	Compute inverse cosine of an argument
	/// @param	x	value in [-1, 1] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Acos(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::acos(x)
		);
	}

	/// @brief	Compute inverse cosine of an
	/// argument and convert to degrees
	/// @param	x	value in [-1, 1] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AcosDegrees(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::acos(x) * RadiansToDegrees<CorrespondingFloat<TNumeric>>
		);
	}

	/// @brief	Compute inverse hyperbolic
	/// cosine of an argument
	/// @param	x	value in [1, +infinity] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AcosHyperbolic(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::acosh(x)
		);
	}

	/// @brief	Compute inverse hyperbolic cosine
	/// of an argument and convert to degrees
	/// @param	x	value in [1, +infinity] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AcosHyperbolicDegrees(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::acosh(x) * RadiansToDegrees<CorrespondingFloat<TNumeric>>
		);
	}

	/// @brief	Compute inverse sine of an argument
	/// @param	x	value in [-1, 1] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Asin(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::asin(x)
		);
	}

	/// @brief	Compute inverse sine of an
	/// argument and convert to degrees
	/// @param	x	value in [-1, 1] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AsinDegrees(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::asin(x) * RadiansToDegrees<CorrespondingFloat<TNumeric>>
		);
	}

	/// @brief	Compute inverse hyperbolic
	/// sine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AsinHyperbolic(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::asinh(x)
		);
	}

	/// @brief	Compute inverse hyperbolic sine
	/// of an argument and convert to degrees
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AsinHyperbolicDegrees(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::asinh(x) * RadiansToDegrees<CorrespondingFloat<TNumeric>>
		);
	}

	/// @brief	Compute inverse tangent of an argument
	/// @param	x	value in [-1, 1] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Atan(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::atan(x)
		);
	}

	/// @brief	Compute inverse tangent of an
	/// argument and convert to degrees
	/// @param	x	value in [-1, 1] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AtanDegrees(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::atan(x) * RadiansToDegrees<CorrespondingFloat<TNumeric>>
		);
	}

	/// @brief	Compute the inverse tangent of y/x
	/// @note	Using the signs of arguments
	/// to determine the correct quadrant
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Atan(
		const TNumeric x,
		const TNumeric y
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::atan2(y, x)
		);
	}

	/// @brief	Compute the inverse tangent of y/x
	/// @note	Using the signs of arguments
	/// to determine the correct quadrant
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AtanDegrees(
		const TNumeric x,
		const TNumeric y
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::atan2(y, x) * RadiansToDegrees<CorrespondingFloat<TNumeric>>
		);
	}

	/// @brief	Compute inverse hyperbolic
	/// tangent of an argument
	/// @param	x	value in [-1, 1] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AtanHyperbolic(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::atanh(x)
		);
	}

	/// @brief	Compute inverse hyperbolic tangent
	/// of an argument and convert to degrees
	/// @param	x	value in [-1, 1] range
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> AtanHyperbolicDegrees(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::atanh(x) * RadiansToDegrees<CorrespondingFloat<TNumeric>>
		);
	}

	/// @brief	Compute cosine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Cos(
		const TNumeric radians
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::cos(radians)
		);
	}

	/// @brief	Compute cosine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> CosDegrees(
		const TNumeric degrees
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::cos(degrees * DegreesToRadians<CorrespondingFloat<TNumeric>>)
		);
	}

	/// @brief	Compute hyperbolic
	/// cosine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> CosHyperbolic(
		const TNumeric radians
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::cosh(radians)
		);
	}

	/// @brief	Compute hyperbolic
	/// cosine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> CosHyperbolicDegrees(
		const TNumeric degrees
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::cosh(degrees * DegreesToRadians<CorrespondingFloat<TNumeric>>)
		);
	}

	/// @brief	Compute sine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Sin(
		const TNumeric radians
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::sin(radians)
		);
	}

	/// @brief	Compute sine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> SinDegrees(
		const TNumeric degrees
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::sin(degrees * DegreesToRadians<CorrespondingFloat<TNumeric>>)
		);
	}

	/// @brief	Compute hyperbolic
	/// sine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> SinHyperbolic(
		const TNumeric radians
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::sinh(radians)
		);
	}

	/// @brief	Compute hyperbolic
	/// sine of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> SinHyperbolicDegrees(
		const TNumeric degrees
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::sinh(degrees * DegreesToRadians<CorrespondingFloat<TNumeric>>)
		);
	}

	/// @brief	Compute tangent of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Tan(
		const TNumeric radians
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::tan(radians)
		);
	}

	/// @brief	Compute tangent of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> TanDegrees(
		const TNumeric degrees
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::tan(degrees * DegreesToRadians<CorrespondingFloat<TNumeric>>)
		);
	}

	/// @brief	Compute hyperbolic
	/// tangent of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> TanHyperbolic(
		const TNumeric radians
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::tanh(radians)
		);
	}

	/// @brief	Compute hyperbolic
	/// tangent of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> TanHyperbolicDegrees(
		const TNumeric degrees
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(
			std::tanh(degrees * DegreesToRadians<CorrespondingFloat<TNumeric>>)
		);
	}
}
