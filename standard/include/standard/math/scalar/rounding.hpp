﻿#pragma once
#include "standard/utility/type-traits.hpp"


namespace Std::inline Math::inline Scalar
{
	/// @brief	Round an argument to
	/// nearest not less integer 
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Ceil(
		const TFloat x
	)
	{
		return std::ceil(x);
	}

	/// @brief	Round an argument to
	/// nearest not greater integer 
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Floor(
		const TFloat x
	)
	{
		return std::floor(x);
	}

	/// @brief	Round an argument to nearest
	/// integer using current rounding mode
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat NearestCurrentMode(
		const TFloat x
	)
	{
		return std::nearbyint(x);
	}

	/// @brief	Round an argument to nearest integer
	/// @note	Rounding away from zero in halfway cases
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Round(
		const TFloat x
	)
	{
		return std::round(x);
	}

	/// @brief	Round an argument to nearest
	/// integer using current rounding mode
	/// with exception if the result differs
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat RoundCurrentMode(
		const TFloat x
	)
	{
		return std::rint(x);
	}

	/// @brief	Round an argument to nearest integer
	/// @note	Rounding away from zero in halfway cases
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline int32 RoundToInt32(
		const TFloat x
	)
	{
		return std::lround(x);
	}

	/// @brief	Round an argument to nearest
	/// integer using current rounding mode
	/// with exception if the result differs
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline int32 RoundToInt32CurrentMode(
		const TFloat x
	)
	{
		return std::lrint(x);
	}

	/// @brief	Round an argument to nearest integer
	/// @note	Rounding away from zero in halfway cases
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline int64 RoundToInt64(
		const TFloat x
	)
	{
		return std::llround(x);
	}

	/// @brief	Round an argument to nearest
	/// integer using current rounding mode
	/// with exception if the result differs
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline int64 RoundToInt64CurrentMode(
		const TFloat x
	)
	{
		return std::llrint(x);
	}

	/// @brief	Round an argument to nearest
	/// not greater in magnitude integer 
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline TFloat Truncate(
		const TFloat x
	)
	{
		return std::trunc(x);
	}
}
