﻿#pragma once
#include "standard/utility/type-traits.hpp"


namespace Std::inline Math::inline Scalar
{
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Lerp(
		const TNumeric a,
		const TNumeric b,
		const TNumeric t
	)
	{
		return std::lerp(a, b, t);
	}
}
