﻿#pragma once
#include "standard/utility/type-traits.hpp"


namespace Std::inline Math::inline Scalar
{
	/// @brief	Compute cubic root of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Cbrt(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::cbrt(x));
	}

	/// @brief	Compute e raised to the given power (e^x)
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Exp(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::exp(x));
	}

	/// @brief	Compute 2 raised to the given power (2^x)
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Exp2(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::exp2(x));
	}

	/// @brief	Compute e raised to the given power minus one ((e^x) - 1)
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> ExpMinus1(
		const TNumeric power
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::expm1(power));
	}

	/// @brief	Compute natural (base e) logarithm of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Ln(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::log(x));
	}

	/// @brief	Compute natural (base e) logarithm
	/// of an argument plus 1 (ln(x + 1))
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> LnPlus1(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::log1p(x));
	}

	/// @brief	Compute common (base 10) logarithm of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Log10(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::log10(x));
	}

	/// @brief	Compute (base 2) logarithm of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Log2(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::log2(x));
	}

	/// @brief	Compute an argument raised to the given power (x^power)
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Pow(
		const TNumeric x,
		const TNumeric power
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::pow(x, power));
	}

	/// @brief	Compute square root of an argument
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Sqrt(
		const TNumeric x
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::sqrt(x));
	}
}
