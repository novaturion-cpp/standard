﻿#pragma once
#include "standard/utility/type-traits.hpp"


namespace Std::inline Math::inline Scalar
{
	/// @brief	Compute square root of the sum of the
	/// squares of an arguments (sqrt(x^2 + y^2))
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Hypotenuse(
		const TNumeric x,
		const TNumeric y
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::hypot(x, y));
	}

	/// @brief	Compute square root of the sum of the
	/// squares of an arguments (sqrt(x^2 + y^2 + z^2))
	template<class TNumeric>
		requires (IsNumericValue<TNumeric>)
	inline CorrespondingFloat<TNumeric> Hypotenuse(
		const TNumeric x,
		const TNumeric y,
		const TNumeric z
	)
	{
		return static_cast<CorrespondingFloat<TNumeric>>(std::hypot(x, y, z));
	}
}
