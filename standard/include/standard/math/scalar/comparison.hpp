﻿#pragma once
#include "standard/utility/type-traits.hpp"


namespace Std::inline Math::inline Scalar
{
	/// @brief	Compare two integer values without value change
	/// caused by conversion between signed and unsigned
	template<class TIntA, class TIntB>
		requires (IsIntValue<TIntA> && IsIntValue<TIntB>)
	inline bool IsEqual(
		const TIntA left,
		const TIntB right
	) noexcept
	{
		if constexpr (IsSignedValue<TIntA> == IsSignedValue<TIntB>)
		{
			return left == right;
		}
		else if constexpr (IsSignedValue<TIntB>)
		{
			return left == static_cast<MakeUnsigned<TIntB>>(right) && right >= 0;
		}
		else
		{
			return static_cast<MakeUnsigned<TIntA>>(left) == right && left >= 0;
		}
	}

	/// @brief	Compare two integer values without value change
	/// caused by conversion between signed and unsigned
	template<class TIntA, class TIntB>
		requires (IsIntValue<TIntA> && IsIntValue<TIntB>)
	inline bool IsLess(
		const TIntA left,
		const TIntB right
	) noexcept
	{
		if constexpr (IsSignedValue<TIntA> == IsSignedValue<TIntB>)
		{
			return left < right;
		}
		else if constexpr (IsSignedValue<TIntB>)
		{
			return right > 0 && left < static_cast<MakeUnsigned<TIntB>>(right);
		}
		else
		{
			return left < 0 || static_cast<MakeUnsigned<TIntA>>(left) < right;
		}
	}

	/// @brief	Compare two integer values without value change
	/// caused by conversion between signed and unsigned
	template<class TIntA, class TIntB>
		requires (IsIntValue<TIntA> && IsIntValue<TIntB>)
	inline bool IsGreater(
		const TIntA left,
		const TIntB right
	) noexcept
	{
		return IsLess(right, left);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsGreater(
		const TFloat left,
		const TFloat right
	)
	{
		return std::isgreater(left, right);
	}

	/// @brief	Compare two integer values without value change
	/// caused by conversion between signed and unsigned
	template<class TIntA, class TIntB>
		requires (IsIntValue<TIntA> && IsIntValue<TIntB>)
	inline bool IsGreaterEqual(
		const TIntA left,
		const TIntB right
	) noexcept
	{
		return !IsLess(left, right);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsGreaterEqual(
		const TFloat left,
		const TFloat right
	)
	{
		return std::isgreaterequal(left, right);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsLess(
		const TFloat left,
		const TFloat right
	)
	{
		return std::isless(left, right);
	}

	/// @brief	Compare two integer values without value change
	/// caused by conversion between signed and unsigned
	template<class TIntA, class TIntB>
		requires (IsIntValue<TIntA> && IsIntValue<TIntB>)
	inline bool IsLessEqual(
		const TIntA left,
		const TIntB right
	) noexcept
	{
		return !IsGreater(left, right);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsLessEqual(
		const TFloat left,
		const TFloat right
	)
	{
		return std::islessequal(left, right);
	}

	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsLessGreater(
		const TFloat left,
		const TFloat right
	)
	{
		return std::islessgreater(left, right);
	}

	/// @brief	Compare two integer values without value change
	/// caused by conversion between signed and unsigned
	template<class TIntA, class TIntB>
		requires (IsIntValue<TIntA> && IsIntValue<TIntB>)
	inline bool IsNotEqual(
		const TIntA left,
		const TIntB right
	) noexcept
	{
		return !IsEqual(left, right);
	}

	/// @brief	Check if one or both arguments are nan 
	template<class TFloat>
		requires (IsFloatValue<TFloat>)
	inline bool IsUnordered(
		const TFloat left,
		const TFloat right
	)
	{
		return std::isunordered(left, right);
	}
}
