#pragma once
#include "standard/math/simd/comparison.hpp"
#include "standard/math/type/vector.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval

namespace Std::inline Math::inline Vector
{
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T EqualVector(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return Equal<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T EqualAsUintVector(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return EqualAsUint<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T> && IsFloatValue<typename T::ValueType>)
	inline T EqualNearVector(
		const T vectorA,
		const T vectorB,
		const typename T::ValueType epsilon
	) noexcept
	{
		return EqualNear<typename T::SimdType, typename T::ValueType>(vectorA, vectorB, epsilon);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T GreaterVector(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return Greater<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T GreaterEqualVector(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return GreaterEqual<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T LessVector(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return Less<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T LessEqualVector(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return LessEqual<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline bool Equal(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return BoolComparison<typename T::SimdType, typename
			T::ValueType>(EqualVector<T>(vectorA, vectorB));
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline bool EqualAsUint(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return BoolComparison<typename T::SimdType, typename T::ValueType>(
			EqualAsUintVector<T>(vectorA, vectorB)
		);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T> && IsFloatValue<typename T::ValueType>)
	inline bool EqualNear(
		const T vectorA,
		const T vectorB,
		const typename T::ValueType epsilon
	) noexcept
	{
		return BoolComparison<typename T::SimdType, typename T::ValueType>(
			EqualNearVector<T>(vectorA, vectorB, epsilon)
		);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline bool Greater(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return BoolComparison<typename T::SimdType, typename T::ValueType>(
			GreaterVector<T>(vectorA, vectorB)
		);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline bool GreaterEqual(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return BoolComparison<typename T::SimdType, typename T::ValueType>(
			GreaterEqualVector<T>(vectorA, vectorB)
		);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline bool Less(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return BoolComparison<typename T::SimdType, typename T::ValueType>(
			LessVector<T>(vectorA, vectorB)
		);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline bool LessEqual(
		const T vectorA,
		const T vectorB
	) noexcept
	{
		return BoolComparison<typename T::SimdType, typename T::ValueType>(
			LessEqualVector<T>(vectorA, vectorB)
		);
	}
}
