﻿#pragma once
#include "standard/math/scalar/constant.hpp"
#include "standard/math/type/vector.hpp"
#include "standard/math/utility/is-vector.hpp"


namespace Std::inline Math::inline Vector
{
	// todo: replace with simd-constant values

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto E = T(E<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Euler = T(Euler<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Gauss = T(Gauss<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Phi = T(Phi<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Pi = T(Pi<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDiv2 = T(OneDiv2<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDiv2Pi = T(OneDiv2Pi<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDiv3 = T(OneDiv3<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDiv4 = T(OneDiv4<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDiv6 = T(OneDiv6<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDivEuler = T(OneDivEuler<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDivPi = T(OneDivPi<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDivRoot2 = T(OneDivRoot2<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto OneDivRoot3 = T(OneDivRoot3<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto ThreeDiv4 = T(ThreeDiv4<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto TwoDiv3 = T(TwoDiv3<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto TwoDivPi = T(TwoDivPi<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Root2 = T(Root2<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Root3 = T(Root3<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto RootE = T(RootE<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto RootPi = T(RootPi<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto RootPiMul2 = T(RootPiMul2<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Ln10 = T(Ln10<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Ln2 = T(Ln2<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto LnLn2 = T(LnLn2<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto LnPhi = T(LnPhi<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Log10E = T(Log10E<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Log2E = T(Log2E<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto EPowPi = T(EPowPi<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto EulerPow2 = T(EulerPow2<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto DegreesToRadians = T(DegreesToRadians<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto RadiansToDegrees = T(RadiansToDegrees<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto PiDiv2 = T(PiDiv2<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto PiDiv3 = T(PiDiv3<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto PiDiv4 = T(PiDiv4<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto PiDiv6 = T(PiDiv6<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto PiPow2 = T(PiPow2<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto PiPowE = T(PiPowE<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto PiMul2 = T(PiMul2<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto PiMul3Div4 = T(PiMul3Div4<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Cos0 = T(Cos0<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Cos15 = T(Cos15<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Cos30 = T(Cos30<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Cos45 = T(Cos45<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Cos60 = T(Cos60<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Cos75 = T(Cos75<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Cos90 = T(Cos90<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Sin0 = T(Sin0<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Sin15 = T(Sin15<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Sin30 = T(Sin30<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Sin45 = T(Sin45<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Sin60 = T(Sin60<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Sin75 = T(Sin75<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Sin90 = T(Sin90<typename T::ValueType>);


	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Tan0 = T(Tan0<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Tan15 = T(Tan15<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Tan30 = T(Tan30<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Tan45 = T(Tan45<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Tan60 = T(Tan60<typename T::ValueType>);

	template<class T = Vector3>
		requires (Utility::IsVectorTSpecializationValue<T>)
	inline constexpr auto Tan75 = T(Tan75<typename T::ValueType>);
}
