#pragma once
#include "standard/math/simd/common.hpp"
#include "standard/math/simd/exponential.hpp"
#include "standard/math/type/vector.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval
// todo: add, subtract, multiply, divide, mod 

namespace Std::inline Math::inline Vector
{
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Abs(
		const T vector
	) noexcept
	{
		return T(Abs<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Add(
		const T left,
		const T right
	) noexcept
	{
		return T(Add<typename T::SimdType, typename T::ValueType>(left.Simd, right.Simd));
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Divide(
		const T left,
		const T right
	) noexcept
	{
		return T(Divide<typename T::SimdType, typename T::ValueType>(left.Simd, right.Simd));
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T LengthSquare(
		const T vector
	) noexcept
	{
		return T(LengthSquare<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline typename T::ValueType LengthSquareScalar(
		const T vector
	) noexcept
	{
		return LengthSquare(vector).X;
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Length(
		const T vector
	) noexcept
	{
		return T(Sqrt<typename T::SimdType, typename T::ValueType>(LengthSquare(vector)));
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline typename T::ValueType LengthScalar(
		const T vector
	) noexcept
	{
		return Length(vector).X;
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T DistanceSquare(
		const T left,
		const T right
	) noexcept
	{
		return LengthSquare(left - right);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline typename T::ValueType DistanceSquareScalar(
		const T left,
		const T right
	) noexcept
	{
		return LengthSquare(left - right).X;
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Mod(
		const T left,
		const T right
	) noexcept
	{
		return T(Mod<typename T::SimdType, typename T::ValueType>(left.Simd, right.Simd));
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Multiply(
		const T left,
		const T right
	) noexcept
	{
		return T(Multiply<typename T::SimdType, typename T::ValueType>(left.Simd, right.Simd));
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Subtract(
		const T left,
		const T right
	) noexcept
	{
		return T(Subtract<typename T::SimdType, typename T::ValueType>(left.Simd, right.Simd));
	}


	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Distance(
		const T left,
		const T right
	) noexcept
	{
		return Length(left - right);
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T DistanceScalar(
		const T left,
		const T right
	) noexcept
	{
		return Length(left - right).X;
	}

	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Normalize(
		const T vector
	) noexcept
	{
		return (vector * vector) / LengthSquare(vector);
	}
}
