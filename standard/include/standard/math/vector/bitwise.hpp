#pragma once
#include "standard/math/simd/bitwise.hpp"
#include "standard/math/utility/is-vector.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval

namespace Std::inline Math::inline Vector
{
	template<class T>
		requires Utility::IsVectorTSpecializationValue<T>
	inline T BitAnd(
		const T vectorA,
		const T vectorB
	)
	{
		return BitAnd<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires Utility::IsVectorTSpecializationValue<T>
	inline T BitNor(
		const T vectorA,
		const T vectorB
	)
	{
		return BitNor<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires Utility::IsVectorTSpecializationValue<T>
	inline T BitNot(
		const T vector
	)
	{
		return BitNot<typename T::SimdType, typename T::ValueType>(vector);
	}

	template<class T>
		requires Utility::IsVectorTSpecializationValue<T>
	inline T BitOr(
		const T vectorA,
		const T vectorB
	)
	{
		return BitOr<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}

	template<class T>
		requires Utility::IsVectorTSpecializationValue<T>
	inline T BitXor(
		const T vectorA,
		const T vectorB
	)
	{
		return BitXor<typename T::SimdType, typename T::ValueType>(vectorA, vectorB);
	}
}
