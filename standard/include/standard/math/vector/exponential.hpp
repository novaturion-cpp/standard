#pragma once
#include "standard/math/simd/exponential.hpp"
#include "standard/math/type/vector.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval

namespace Std::inline Math::inline Vector
{
	/// @brief	Compute cubic root of an argument
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Cbrt(
		const T vector
	)
	{
		return T(Cbrt<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute e raised to the given power (e^x)
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Exp(
		const T vector
	)
	{
		return T(Exp<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute e raised to the given power (e^x)
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T ExpFast(
		const T vector
	)
	{
		return T(ExpFast<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute e raised to the given power minus one ((e^x) - 1)
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T ExpMinus1(
		const T vector
	)
	{
		return T(ExpMinus1<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute natural (base e) logarithm of an argument
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Ln(
		const T vector
	)
	{
		return T(Ln<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute natural (base e) logarithm
	/// of an argument plus 1 (ln(x + 1))
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T LnPlus1(
		const T vector
	)
	{
		return T(LnPlus1<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute common (base 10) logarithm of an argument
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Log10(
		const T vector
	)
	{
		return T(Log10<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute (base 2) logarithm of an argument
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Log2(
		const T vector
	)
	{
		return T(Log2<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute 2 raised to the given power (2^x)
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Pow2(
		const T vector
	)
	{
		return T(Pow2<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute an argument raised to the given power (x^power)
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Pow(
		const T vector,
		const T power
	)
	{
		return T(Pow<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}

	/// @brief	Compute square root of an argument
	template<class T>
		requires (IsVectorTSpecializationValue<T>)
	inline T Sqrt(
		const T vector
	)
	{
		return T(Sqrt<typename T::SimdType, typename T::ValueType>(vector.Simd));
	}
}
