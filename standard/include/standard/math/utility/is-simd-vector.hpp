﻿#pragma once
#include "standard/platform/intrinsic/type.hpp"

// todo: namespace

namespace Std::inline Utility
{
	/// @brief	Check if type is SIMD vector
	/// @see {@link Std::Platform::Intrinsic::Vector64}
	/// @see {@link Std::Platform::Intrinsic::Vector128}
	/// @see {@link Std::Platform::Intrinsic::Vector256}
	template<class T>
	inline constexpr bool IsSimdVectorValue = IsAnyValue<RemoveAll<T>, Vector64, Vector128, Vector256>;

	/// @brief	Check if type is SIMD vector
	/// @see {@link Std::Platform::Intrinsic::Vector64}
	/// @see {@link Std::Platform::Intrinsic::Vector128}
	/// @see {@link Std::Platform::Intrinsic::Vector256}
	template<class T>
	using IsSimdVector = BoolConstant<IsSimdVectorValue<RemoveAll<T>>>;
}
