#pragma once

// todo: namespace
// todo: replace with macros?

namespace Std::inline Utility
{
	template<class TSimd, class TValue, class TReturn>
	static constexpr TSimd Vectorize(
		TReturn (*function)(
			TValue
		),
		const TSimd a
	)
	{
		constexpr auto elementsCount = sizeof(TSimd) / sizeof(TValue);

		auto& array = a.template AsT<TValue>();
		TValue result[elementsCount] = {};

		if constexpr (elementsCount == 2)
		{
			result[0] = static_cast<TValue>(function(array[0]));
			result[1] = static_cast<TValue>(function(array[1]));
		}
		else if constexpr (elementsCount == 3)
		{
			result[0] = static_cast<TValue>(function(array[0]));
			result[1] = static_cast<TValue>(function(array[1]));
			result[2] = static_cast<TValue>(function(array[2]));
		}
		else if constexpr (elementsCount == 4)
		{
			result[0] = static_cast<TValue>(function(array[0]));
			result[1] = static_cast<TValue>(function(array[1]));
			result[2] = static_cast<TValue>(function(array[2]));
			result[3] = static_cast<TValue>(function(array[3]));
		}
		else
		{
			for (auto i = 0u; i < elementsCount; ++i)
			{
				result[i] = static_cast<TValue>(function(array[i]));
			}
		}

		return TSimd(result);
	}

	template<class TSimd, class TValue, class TReturn>
	static constexpr TSimd Vectorize(
		TReturn (*function)(
			TValue,
			TValue
		),
		const TSimd a,
		const TSimd b
	)
	{
		constexpr auto elementsCount = sizeof(TSimd) / sizeof(TValue);

		auto& aArray = a.template AsT<TValue>();
		auto& bArray = b.template AsT<TValue>();
		TValue result[elementsCount] = {};

		if constexpr (elementsCount == 2)
		{
			result[0] = static_cast<TValue>(function(aArray[0], bArray[0]));
			result[1] = static_cast<TValue>(function(aArray[1], bArray[1]));
		}
		else if constexpr (elementsCount == 3)
		{
			result[0] = static_cast<TValue>(function(aArray[0], bArray[0]));
			result[1] = static_cast<TValue>(function(aArray[1], bArray[1]));
			result[2] = static_cast<TValue>(function(aArray[2], bArray[2]));
		}
		else if constexpr (elementsCount == 4)
		{
			result[0] = static_cast<TValue>(function(aArray[0], bArray[0]));
			result[1] = static_cast<TValue>(function(aArray[1], bArray[1]));
			result[2] = static_cast<TValue>(function(aArray[2], bArray[2]));
			result[3] = static_cast<TValue>(function(aArray[3], bArray[3]));
		}
		else
		{
			for (auto i = 0u; i < elementsCount; ++i)
			{
				result[i] = static_cast<TValue>(function(aArray[i], bArray[i]));
			}
		}

		return TSimd(result);
	}

	template<class TSimd, class TValue, class TReturn>
	static constexpr TSimd Vectorize(
		TReturn (*function)(
			TValue,
			TValue,
			TValue
		),
		const TSimd a,
		const TSimd b,
		const TSimd c
	)
	{
		constexpr auto elementsCount = sizeof(TSimd) / sizeof(TValue);

		auto& aArray = a.template AsT<TValue>();
		auto& bArray = b.template AsT<TValue>();
		auto& cArray = c.template AsT<TValue>();
		TValue result[elementsCount] = {};

		if constexpr (elementsCount == 2)
		{
			result[0] = static_cast<TValue>(function(aArray[0], bArray[0], cArray[0]));
			result[1] = static_cast<TValue>(function(aArray[1], bArray[1], cArray[1]));
		}
		else if constexpr (elementsCount == 3)
		{
			result[0] = static_cast<TValue>(function(aArray[0], bArray[0], cArray[0]));
			result[1] = static_cast<TValue>(function(aArray[1], bArray[1], cArray[1]));
			result[2] = static_cast<TValue>(function(aArray[2], bArray[2], cArray[2]));
		}
		else if constexpr (elementsCount == 4)
		{
			result[0] = static_cast<TValue>(function(aArray[0], bArray[0], cArray[0]));
			result[1] = static_cast<TValue>(function(aArray[1], bArray[1], cArray[1]));
			result[2] = static_cast<TValue>(function(aArray[2], bArray[2], cArray[2]));
			result[3] = static_cast<TValue>(function(aArray[3], bArray[3], cArray[3]));
		}
		else
		{
			for (auto i = 0u; i < elementsCount; ++i)
			{
				result[i] = static_cast<TValue>(function(aArray[i], bArray[i], cArray[i]));
			}
		}

		return TSimd(result);
	}


	template<class TSimd, class TValue>
	static constexpr bool VectorizeBool(
		bool (*function)(
			TValue
		),
		const TSimd a
	)
	{
		constexpr auto elementsCount = sizeof(TSimd) / sizeof(TValue);

		auto& array = a.template AsT<TValue>();
		auto result = true;

		if constexpr (elementsCount == 2)
		{
			result = function(array[0]) && function(array[1]);
		}
		else if constexpr (elementsCount == 3)
		{
			result = function(array[0]) && function(array[1]) && function(array[2]);
		}
		else if constexpr (elementsCount == 4)
		{
			result = function(array[0]) && function(array[1]) && function(array[2]) && function(array[3]);
		}
		else
		{
			for (auto i = 0u; result && i < elementsCount; ++i)
			{
				result = result && function(array[i]);
			}
		}

		return result;
	}
}
