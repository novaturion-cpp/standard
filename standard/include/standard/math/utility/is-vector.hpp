﻿#pragma once
#include "standard/math/type/vector.hpp"

// todo: namespace

namespace Std::inline Utility
{
	/// @brief	Check if type is specialization of VectorT template
	/// @see {@link Std::Math::Type::VectorT}
	template<class T>
	inline constexpr auto IsVectorTSpecializationValue = false;

	/// @brief	Check if type is specialization of VectorT template
	/// @see {@link Std::Math::Type::VectorT}
	template<class T, uint16 VElements>
	inline constexpr auto IsVectorTSpecializationValue<VectorT<T, VElements>> = true;

	/// @brief	Check if type is specialization of VectorT template
	/// @see {@link Std::Math::Type::VectorT}
	template<class T>
	using IsVectorTSpecialization = BoolConstant<IsVectorTSpecializationValue<RemoveAll<T>>>;
}
