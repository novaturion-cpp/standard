﻿#pragma once
#include "standard/math/type/vector.hpp"

// todo: namespace

namespace Std::inline Utility
{
	template<class TTo, class TFrom>
		requires (IsNumericValue<TTo> && IsNumericValue<TFrom>)
	constexpr Vector2T<TTo> VectorStaticCast(
		const Vector2T<TFrom>& vector
	)
	{
		return Vector2T<TTo>(
			static_cast<TTo>(vector.X),
			static_cast<TTo>(vector.Y)
		);
	}

	template<class TTo, class TFrom>
		requires (IsNumericValue<TTo> && IsNumericValue<TFrom>)
	constexpr Vector3T<TTo> VectorStaticCast(
		const Vector3T<TFrom>& vector
	)
	{
		return Vector3T<TTo>(
			static_cast<TTo>(vector.X),
			static_cast<TTo>(vector.Y),
			static_cast<TTo>(vector.Z)
		);
	}

	template<class TTo, class TFrom>
		requires (IsNumericValue<TTo> && IsNumericValue<TFrom>)
	constexpr Vector4T<TTo> VectorStaticCast(
		const Vector4T<TFrom>& vector
	)
	{
		return Vector4T<TTo>(
			static_cast<TTo>(vector.X),
			static_cast<TTo>(vector.Y),
			static_cast<TTo>(vector.Z),
			static_cast<TTo>(vector.W)
		);
	}
}
