﻿#pragma once
#include "type/float-category.hpp"
#include "type/matrix.hpp"
#include "type/quaternion.hpp"
#include "type/vector.hpp"
