﻿#pragma once
#include "constant.hpp"

#include "standard/math/utility/is-simd-vector.hpp"
#include "standard/math/utility/vectorize.hpp"

#include "standard/platform/intrinsic/x86/avx.hpp"
#include "standard/platform/intrinsic/x86/avx2.hpp"
#include "standard/platform/intrinsic/x86/shuffle.hpp"
#include "standard/platform/intrinsic/x86/sse.hpp"
#include "standard/platform/intrinsic/x86/sse2.hpp"
#include "standard/platform/intrinsic/x86/sse41.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval
// todo: replace calls to stl with Std?

namespace Std::inline Math::inline Simd
{
	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd Abs(
		const TSimd vector
	)
	{
		using ReturnType = decltype(std::abs(std::declval<TValue>()));
		return Vectorize<TSimd, TValue, ReturnType>(std::abs, vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsAnyValue<TValue, float32, int32>
	static inline Vector128 Abs(
		const Vector128 vector
	)
	{
		return Sse::AndPs(vector, AbsMaskSimd128Ps);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsAnyValue<TValue, float64, int64>
	static inline Vector128 Abs(
		const Vector128 vector
	)
	{
		return Sse2::AndPd(vector, AbsMaskSimd128Pd);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsAnyValue<TValue, float64, int64>
	static inline Vector256 Abs(
		const Vector256 vector
	)
	{
		return Avx::AndPd(vector, AbsMaskSimd256Pd);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd Add(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return a + b;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Add(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::AddPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Add(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::AddPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int32>
	static inline Vector128 Add(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::AddEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int64>
	static inline Vector128 Add(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::AddEpi64(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Add(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::AddPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int64>
	static inline Vector256 Add(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::AddEpi64(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd Divide(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return b
				? a / b
				: 0;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Divide(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::DividePs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Divide(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::DividePd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Divide(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::DividePd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd LengthSquare(
		const TSimd vector
	)
	{
		auto result = TValue();
		for (auto i = 0; i < sizeof(TSimd) / sizeof(TValue); ++i)
		{
			result += vector[i] * vector[i];
		}
		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 LengthSquare(
		const Vector128 vector
	)
	{
		return Sse41::DotProductPs<0x7f>(vector, vector);

		// auto dot = Sse::MultiplyPs(left, right);
		// // y, x, w, z
		// auto shuffled = Sse::ShufflePs<ShuffleMask(1, 0, 3, 2)>(dot);
		// // x, y, z, w +
		// // y, x, w, z
		// dot = Sse::AddSs(dot, shuffled);
		// // z+w, w+z, x+y, y+x
		// shuffled = Sse::ShufflePs<ShuffleMask(2, 3, 0, 1)>(dot);
		// // x+y, y+x, z+w, w+z +
		// // z+w, w+z, x+y, y+x
		// dot = Sse::AddSs(dot, shuffled);
		//
		// return Sse::ShufflePs<ShuffleMask(0)>(dot);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 LengthSquare(
		const Vector128 vector
	)
	{
		return Sse41::DotProductPd<0x7f>(vector, vector);

		// auto dot = Sse2::MultiplyPd(left, right);
		// // splat y
		// const auto temp = Sse2::ShufflePd<ShuffleMask(1)>(dot);
		// dot = Sse2::AddPd(dot, temp);
		//
		// return Sse2::ShufflePd<ShuffleMask(0)>(dot);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 LengthSquare(
		const Vector256 vector
	)
	{
		auto dot = Avx::MultiplyPd(vector, vector);
		// y, x, w, z
		auto shuffled = Avx::ShufflePd<ShuffleMask(2, 1, 4, 3)>(dot);
		// x, y, z, w +
		// y, x, w, z
		dot = Avx::AddPd(dot, shuffled);
		// z+w, w+z, x+y, y+x
		shuffled = Avx::Permute2F128Pd<1>(dot);
		// x+y, y+x, z+w, w+z +
		// z+w, w+z, x+y, y+x
		dot = Avx::AddPd(dot, shuffled);

		return dot;
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd Mod(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return b
				? a % b
				: 0;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd Multiply(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return a * b;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Multiply(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::MultiplyPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Multiply(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::MultiplyPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int32>
	static inline Vector128 Multiply(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse41::MultiplyLowEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, uint32>
	static inline Vector128 Multiply(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse41::MultiplyLowEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Multiply(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::MultiplyPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd Subtract(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return a - b;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Subtract(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::SubtractPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Subtract(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::SubtractPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int32>
	static inline Vector128 Subtract(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::SubtractEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int64>
	static inline Vector128 Subtract(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::SubtractEpi64(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Subtract(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::SubtractPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int64>
	static inline Vector256 Subtract(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::SubtractEpi64(left, right);
	}
}
