#pragma once
#include "constant.hpp"

#include "standard/platform/intrinsic/x86/avx.hpp"
#include "standard/platform/intrinsic/x86/avx2.hpp"
#include "standard/platform/intrinsic/x86/fma3.hpp"
#include "standard/platform/intrinsic/x86/sse.hpp"
#include "standard/platform/intrinsic/x86/sse2.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval

namespace Std::inline Math::inline Simd::Helper
{
	inline Vector128 CbrtHalleyPs(
		const Vector128 vector,
		const Vector128 approximation
	)
	{
		// https://en.wikipedia.org/wiki/Halley%27s_method
		// x1 = x0 - 2 * f(x0) * f'(x0) / (2 * f'(x0)^2 - f(x0) * f''(x0))
		// f(x) = x^3 - r
		// result = approximation * (2 * vector + approximation^3) / (vector + 2 * approximation^3)

		const auto approximation3 = Sse::MultiplyPs(
			Sse::MultiplyPs(approximation, approximation),
			approximation
		);
		// vector + approximation^3
		const auto sharedSum = Sse::AddPs(vector, approximation3);
		// 2 * vector + approximation^3
		const auto numerator = Sse::AddPs(vector, sharedSum);
		// vector + 2 * approximation^3
		const auto denominator = Sse::AddPs(approximation3, sharedSum);

		return Sse::DividePs(Sse::MultiplyPs(numerator, approximation), denominator);
	}

	inline Vector128 CbrtHalleyPd(
		const Vector128 vector,
		const Vector128 approximation
	)
	{
		// https://en.wikipedia.org/wiki/Halley%27s_method
		// x1 = x0 - 2 * f(x0) * f'(x0) / (2 * f'(x0)^2 - f(x0) * f''(x0))
		// f(x) = x^3 - r
		// result = approximation * (2 * vector + approximation^3) / (vector + 2 * approximation^3)

		const auto approximation3 = Sse2::MultiplyPd(
			Sse2::MultiplyPd(approximation, approximation),
			approximation
		);
		// vector + approximation^3
		const auto sharedSum = Sse2::AddPd(vector, approximation3);
		// 2 * vector + approximation^3
		const auto numerator = Sse2::AddPd(vector, sharedSum);
		// vector + 2 * approximation^3
		const auto denominator = Sse2::AddPd(approximation3, sharedSum);

		return Sse2::DividePd(Sse2::MultiplyPd(numerator, approximation), denominator);
	}

	inline Vector256 CbrtHalleyPd(
		const Vector256 vector,
		const Vector256 approximation
	)
	{
		// https://en.wikipedia.org/wiki/Halley%27s_method
		// x1 = x0 - 2 * f(x0) * f'(x0) / (2 * f'(x0)^2 - f(x0) * f''(x0))
		// f(x) = x^3 - r
		// result = approximation * (2 * vector + approximation^3) / (vector + 2 * approximation^3)

		const auto approximation3 = Avx::MultiplyPd(
			Avx::MultiplyPd(approximation, approximation),
			approximation
		);
		// vector + approximation^3
		const auto sharedSum = Avx::AddPd(vector, approximation3);
		// 2 * vector + approximation^3
		const auto numerator = Avx::AddPd(vector, sharedSum);
		// vector + 2 * approximation^3
		const auto denominator = Avx::AddPd(approximation3, sharedSum);

		return Avx::DividePd(Avx::MultiplyPd(numerator, approximation), denominator);
	}

	inline float32 Log2Approx(
		float32 x
	)
	{
		// https://web.archive.org/web/20120922001736/http://metamerist.blogspot.com/2007/09/faster-cube-root-iii.html

		const auto& i = reinterpret_cast<uint32&>(x);
		constexpr auto exponentBias = 127 << 23;
		constexpr auto oneDiv2Pow23 = 1.0f / (1 << 23);
		return static_cast<float32>((i & 0x7fffffff) - exponentBias) * oneDiv2Pow23;
	}

	template<int VN>
	inline float32 NthRoot(
		float32 x
	)
	{
		// https://web.archive.org/web/20131227144655/http://metamerist.com/cbrt/cbrt.htm

		constexpr auto exponentBits = 8;
		constexpr auto mantissaBits = 23;
		constexpr auto bias = (1 << (exponentBits - 1)) - 1;

		auto& i = reinterpret_cast<int&>(x);
		i = (i - (bias << mantissaBits)) / VN + (bias << mantissaBits);

		return x;
	}

	static Vector128 PolynomialTaylor5Ps(
		const Vector128 x
	)
	{
		// cn * x^n + cn-1 * x^n-1 + ... + c0 * x^0 ->
		// (c2 + c3 * x) * x^2 + ((c4 + c5 * x) * x^4 + (c0 + c1 * x))
		const auto x2 = Sse::MultiplyPs(x, x);
		const auto x4 = Sse::MultiplyPs(x2, x2);
		return Fma3::MultiplyAddPs(
			Fma3::MultiplyAddPs(TaylorCoefficient3Simd128Ps, x, TaylorCoefficient2Simd128Ps),
			x2,
			Fma3::MultiplyAddPs(
				Fma3::MultiplyAddPs(TaylorCoefficient5Simd128Ps, x, TaylorCoefficient4Simd128Ps),
				x4,
				Fma3::MultiplyAddPs(TaylorCoefficient1Simd128Ps, x, TaylorCoefficient0Simd128Ps)
			)
		);
	}

	static Vector128 PolynomialTaylor11Pd(
		const Vector128 x
	)
	{
		// cn * x^n + cn-1 * x^n-1 + ... + c0 * x^0 ->
		// ((c6 + c7 * x) + (c8 + c9 * x) * x^2 + (c10 + c11 * x) * x^4) * x^8 +
		// (((c4 + c5 * x) * x^2 + (c2 + c3 * x)) * x^4 + ((c0 + c1 * x) * x^2 + x))
		const auto x2 = Sse2::MultiplyPd(x, x);
		const auto x4 = Sse2::MultiplyPd(x2, x2);
		const auto x8 = Sse2::MultiplyPd(x4, x4);
		return Fma3::MultiplyAddPd(
			Fma3::MultiplyAddPd(
				Fma3::MultiplyAddPd(TaylorCoefficient11Simd128Pd, x, TaylorCoefficient10Simd128Pd),
				x4,
				Fma3::MultiplyAddPd(
					Fma3::MultiplyAddPd(TaylorCoefficient9Simd128Pd, x, TaylorCoefficient8Simd128Pd),
					x2,
					Fma3::MultiplyAddPd(TaylorCoefficient7Simd128Pd, x, TaylorCoefficient6Simd128Pd)
				)
			),
			x8,
			Fma3::MultiplyAddPd(
				Fma3::MultiplyAddPd(
					Fma3::MultiplyAddPd(TaylorCoefficient5Simd128Pd, x, TaylorCoefficient4Simd128Pd),
					x2,
					Fma3::MultiplyAddPd(TaylorCoefficient3Simd128Pd, x, TaylorCoefficient2Simd128Pd)
				),
				x4,
				Fma3::MultiplyAddPd(
					Fma3::MultiplyAddPd(TaylorCoefficient1Simd128Pd, x, TaylorCoefficient0Simd128Pd),
					x2,
					x
				)
			)
		);
	}

	static Vector256 PolynomialTaylor11Pd(
		const Vector256 x
	)
	{
		// cn * x^n + cn-1 * x^n-1 + ... + c0 * x^0 ->
		// ((c6 + c7 * x) + (c8 + c9 * x) * x^2 + (c10 + c11 * x) * x^4) * x^8 +
		// (((c4 + c5 * x) * x^2 + (c2 + c3 * x)) * x^4 + ((c0 + c1 * x) * x^2 + x))
		const auto x2 = Avx::MultiplyPd(x, x);
		const auto x4 = Avx::MultiplyPd(x2, x2);
		const auto x8 = Avx::MultiplyPd(x4, x4);
		return Fma3::MultiplyAddPd(
			Fma3::MultiplyAddPd(
				Fma3::MultiplyAddPd(TaylorCoefficient11Simd256Pd, x, TaylorCoefficient10Simd256Pd),
				x4,
				Fma3::MultiplyAddPd(
					Fma3::MultiplyAddPd(TaylorCoefficient9Simd256Pd, x, TaylorCoefficient8Simd256Pd),
					x2,
					Fma3::MultiplyAddPd(TaylorCoefficient7Simd256Pd, x, TaylorCoefficient6Simd256Pd)
				)
			),
			x8,
			Fma3::MultiplyAddPd(
				Fma3::MultiplyAddPd(
					Fma3::MultiplyAddPd(TaylorCoefficient5Simd256Pd, x, TaylorCoefficient4Simd256Pd),
					x2,
					Fma3::MultiplyAddPd(TaylorCoefficient3Simd256Pd, x, TaylorCoefficient2Simd256Pd)
				),
				x4,
				Fma3::MultiplyAddPd(
					Fma3::MultiplyAddPd(TaylorCoefficient1Simd256Pd, x, TaylorCoefficient0Simd256Pd),
					x2,
					x
				)
			)
		);
	}

	inline Vector128 Pow2Ps(
		const Vector128 power
	)
	{
		constexpr auto fraction = Vector128(
			(1_ui32 << NumericLimits<float32>::MantissaRealSize)
			+ static_cast<float32>(-NumericLimits<float32>::ExponentBias)
		);
		return Sse2::ShiftLeftLogicalEpi32<NumericLimits<float32>::MantissaRealSize>(
			Sse::AddPs(power, fraction)
		);
	}

	inline Vector128 Pow2Pd(
		const Vector128 power
	)
	{
		constexpr auto fraction = Vector128(
			(1_ui64 << NumericLimits<float64>::MantissaRealSize)
			+ static_cast<float64>(-NumericLimits<float64>::ExponentBias)
		);
		return Sse2::ShiftLeftLogicalEpi64<NumericLimits<float64>::MantissaRealSize>(
			Sse2::AddPd(power, fraction)
		);
	}

	inline Vector256 Pow2Pd(
		Vector256 power
	)
	{
		constexpr auto fraction = Vector256(
			(1_ui64 << NumericLimits<float64>::MantissaRealSize)
			+ static_cast<float64>(-NumericLimits<float64>::ExponentBias)
		);
		return Avx2::ShiftLeftLogicalEpi64<NumericLimits<float64>::MantissaRealSize>(
			Avx::AddPd(power, fraction)
		);
	}
}
