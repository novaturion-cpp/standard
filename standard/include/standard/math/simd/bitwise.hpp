#pragma once
#include "standard/math/utility/is-simd-vector.hpp"
#include "standard/math/utility/vectorize.hpp"

#include "standard/platform/intrinsic/x86/avx.hpp"
#include "standard/platform/intrinsic/x86/avx2.hpp"
#include "standard/platform/intrinsic/x86/sse.hpp"
#include "standard/platform/intrinsic/x86/sse2.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval

namespace Std::inline Math::inline Simd
{
	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd BitAnd(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return a & b;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsIntValue<TValue>
	static inline Vector128 BitAnd(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::AndSi128(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 BitAnd(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::AndPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 BitAnd(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::AndPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsIntValue<TValue>
	static inline Vector256 BitAnd(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::AndSi256(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 BitAnd(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::AndPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd BitNot(
		const TSimd vector
	)
	{
		using FunctionType = TValue (*)(
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a
		) -> TValue
		{
			return ~a;
		};

		return Vectorize(function, vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128>
	static inline Vector128 BitNot(
		const Vector128 vector
	)
	{
		return Sse2::XorSi128(vector, Vector128::GetMaskXYZW());
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256>
	static inline Vector256 BitNot(
		const Vector256 vector
	)
	{
		return Avx2::XorSi256(vector, Vector256::GetMaskXYZW());
	}

	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd BitOr(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return a | b;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsIntValue<TValue>
	static inline Vector128 BitOr(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::OrSi128(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 BitOr(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::OrPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 BitOr(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::OrPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsIntValue<TValue>
	static inline Vector256 BitOr(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::OrSi256(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 BitOr(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::OrPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd BitNor(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType bitAnd = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return ~(a | b);
		};

		return Vectorize(bitAnd, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128>
	static constexpr Vector128 BitNor(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::XorSi128(Sse2::OrSi128(left, right), Vector128::GetMaskXYZW());
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256>
	static constexpr Vector256 BitNor(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::XorSi256(Avx2::OrSi256(left, right), Vector256::GetMaskXYZW());
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd BitXor(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return a ^ b;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsIntValue<TValue>
	static inline Vector128 BitXor(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::XorSi128(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 BitXor(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::XorPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 BitXor(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::XorPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsIntValue<TValue>
	static inline Vector256 BitXor(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::XorSi256(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 BitXor(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::XorPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd ShiftLeft(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return a << b;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd ShiftRight(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			return a >> b;
		};

		return Vectorize(function, left, right);
	}
}
