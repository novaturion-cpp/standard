#pragma once
#include "constant.hpp"
#include "helper.hpp"

#include "standard/math/utility/vectorize.hpp"

#include "standard/platform/intrinsic/x86/avx.hpp"
#include "standard/platform/intrinsic/x86/avx2.hpp"
#include "standard/platform/intrinsic/x86/fma3.hpp"
#include "standard/platform/intrinsic/x86/shuffle.hpp"
#include "standard/platform/intrinsic/x86/sse.hpp"
#include "standard/platform/intrinsic/x86/sse2.hpp"
#include "standard/platform/intrinsic/x86/sse41.hpp"
#include "standard/platform/intrinsic/x86/sse42.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval

namespace Std::inline Math::inline Simd
{
	template<class TSimd, class TValue>
	static constexpr TSimd Cbrt(
		const TSimd vector
	)
	{
		return Vectorize(std::cbrt<TValue>, vector);
	}


	template<class TSimd, class TValue>
	static inline TSimd CbrtN(
		const TSimd vector
	)
	{
		const auto abs = Sse::AndPs(vector, AbsMaskSimd128Ps);
		const auto absDiv3 = Sse::MultiplyPs(abs, OneDiv3Simd128Ps);

		// multiply exponent by -1/3
		constexpr auto exponentBias = Vector128(0x54800000);
		constexpr auto exponentMultiplier = Vector128(0x002aaaaa);
		auto result = Sse2::SubtractEpi32(
			exponentBias,
			Sse41::MultiplyLowEpi32(
				Sse2::ShiftRightArithmeticEpi32<23>(abs),
				exponentMultiplier
			)
		);

		constexpr auto fourDiv3 = Vector128(4.0f / 3.0f);
		for (auto i = 0; i < 3; ++i)
		{
			const auto result2 = Sse::MultiplyPs(result, result);
			result = Fma3::MultiplyNegateAddPs(
				absDiv3,
				Sse::MultiplyPs(result2, result2),
				Sse::MultiplyPs(result, fourDiv3)
			);
		}
		const auto result2 = Sse::MultiplyPs(result, result);
		result = Fma3::MultiplyAddPs(
			OneDiv3Simd128Ps,
			Fma3::MultiplyNegateAddPs(
				abs,
				Sse::MultiplyPs(result2, result2),
				result
			),
			result
		);
		result = Sse::MultiplyPs(result, Sse::MultiplyPs(result, vector));

		constexpr auto denormalLimit = Vector128(0x00800000);
		const auto isDenormal = Sse2::CompareLessEpi32(abs, denormalLimit);
		const auto exponentBits = Sse2::AndSi128(
			vector,
			InfinitySimd128Ps
		);

		result = Sse41::BlendPs(result, ZeroSimd128Ps, isDenormal);
		result = Sse41::BlendPs(result, vector, Sse::CompareEqualPs(exponentBits, InfinitySimd128Ps));

		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Cbrt(
		const Vector128 vector
	)
	{
		// Subnormal handling and first estimate:
		// https://github.com/freebsd/freebsd-src/blob/master/lib/msun/src/s_cbrtf.c

		// https://web.archive.org/web/20131227144655/http://metamerist.com/cbrt/cbrt.htm

		const auto sign = Sse::AndPs(vector, NegativeZeroSimd128Ps);
		const auto abs = Sse::AndPs(vector, AbsMaskSimd128Ps);

		// todo: use/extract simd-logical functions
		const auto isZero = Sse::CompareEqualPs(vector, ZeroSimd128Ps);
		const auto isNanOrInf = Sse2::CompareGreaterEpi32(abs, Float32MaxSimd128Ps);
		const auto isNormal = Sse2::CompareGreaterEpi32(abs, Float32MinSimd128Ps);

		// todo: actually not a 2^24
		constexpr auto twoPow24 = Vector128(0x4b800000);
		// (127 - 127.0 / 3 - 24 / 3 - 0.03306235651) * 2^23
		constexpr auto normalOffset = Vector128(0x265119f2);
		// (127 - 127.0 / 3 - 0.03306235651) * 2^23
		constexpr auto subnormalOffset = Vector128(0x2a5119f2);

		auto result = Sse::AndNotPs(sign, Sse::MultiplyPs(vector, twoPow24));
		result = Sse41::BlendEpi8(abs, result, isNormal);

		constexpr auto a = Vector128(0x55555556);
		const auto b = Sse41::MultiplyEpi32(
			Sse2::ShuffleEpi32<ShuffleMask(1, 1, 3, 3)>(result),
			a
		);
		auto resultDiv3 = Sse41::MultiplyEpi32(
			result,
			a
		);
		resultDiv3 = Avx2::BlendEpi32<0b1010>(resultDiv3, b);
		Vector128 resultDiv3A(
			__m128 {
				.m128_i32 = {
					result.AsInt32()[0] / 3,
					result.AsInt32()[1] / 3,
					result.AsInt32()[2] / 3,
					result.AsInt32()[3] / 3
				}
			}
		);

		result = Sse2::AddEpi32(
			Sse41::BlendEpi8(subnormalOffset, normalOffset, isNormal),
			resultDiv3
		);

		result = Sse::OrPs(result, sign);

		// first iteration
		result = Helper::CbrtHalleyPs(vector, result);
		// second iteration
		result = Helper::CbrtHalleyPs(vector, result);

		// handle nan, inf and zero
		result = Sse41::BlendPs(result, vector, isZero);
		result = Sse41::BlendPs(result, Sse::AddPs(vector, vector), isNanOrInf);

		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Cbrt(
		const Vector128 vector
	)
	{
		// Subnormal handling and first estimate:
		// https://github.com/freebsd/freebsd-src/blob/master/lib/msun/src/s_cbrt.c

		const auto sign = Sse2::AndPd(vector, NegativeZeroSimd128Pd);
		const auto abs = Sse2::AndPd(vector, AbsMaskSimd128Pd);

		// todo: use/extract simd-logical functions
		const auto isZero = Sse2::CompareEqualPd(vector, ZeroSimd128Ps);
		const auto isNanOrInf = Sse42::CompareGreaterEpi64(abs, Float64MaxSimd128Pd);
		const auto isNormal = Sse42::CompareGreaterEpi64(abs, Float64MinSimd128Pd);

		// todo: actually not a 2^54
		constexpr auto twoPow54 = Vector128(0x4350000000000000);
		// (1023 - 1023 / 3 - 54 / 3 - 0.03306235651) * 2^20
		constexpr auto normalOffset = Vector128(0x297f789300000000);
		// (1023 - 1023 / 3 - 0.03306235651) * 2^20
		constexpr auto subnormalOffset = Vector128(0x2a9f789300000000);

		auto result = Sse2::AndNotPd(sign, Sse2::MultiplyPd(vector, twoPow54));
		result = Sse41::BlendEpi8(abs, result, isNormal);

		result = Sse2::AddEpi64(
			Sse41::BlendEpi8(subnormalOffset, normalOffset, isNormal),
			Vector128(
				__m128i {
					.m128i_i64 = {
						result.AsInt64()[0] / 3,
						result.AsInt64()[1] / 3
					}
				}
			)
		);

		result = Sse2::OrPd(result, sign);

		// first iteration
		result = Helper::CbrtHalleyPd(vector, result);
		// second iteration
		result = Helper::CbrtHalleyPd(vector, result);

		// handle nan, inf and zero
		result = Sse41::BlendPd(result, vector, isZero);
		result = Sse41::BlendPd(result, Sse2::AddPd(vector, vector), isNanOrInf);

		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Cbrt(
		const Vector256 vector
	)
	{
		// Subnormal handling and first estimate:
		// https://github.com/freebsd/freebsd-src/blob/master/lib/msun/src/s_cbrt.c

		const auto sign = Avx::AndPd(vector, NegativeZeroSimd256Pd);
		const auto abs = Avx::AndPd(vector, AbsMaskSimd256Pd);

		// todo: use/extract simd-logical functions
		const auto isZero = Avx::ComparePd<EComparisonType::EqualOrderedNonSignaling>(
			vector,
			ZeroSimd256Pd
		);
		const auto isNanOrInf = Avx::ComparePd<EComparisonType::GreaterOrderedNonSignaling>(
			abs,
			Float64MaxSimd256Pd
		);
		const auto isNormal = Avx::ComparePd<EComparisonType::GreaterOrderedNonSignaling>(
			abs,
			Float64MinSimd256Pd
		);

		// todo: actually not a 2^54
		constexpr auto twoPow54 = Vector256(0x4350000000000000);
		// (1023 - 1023 / 3 - 54 / 3 - 0.03306235651) * 2^20
		constexpr auto normalOffset = Vector256(0x297f789300000000);
		// (1023 - 1023 / 3 - 0.03306235651) * 2^20
		constexpr auto subnormalOffset = Vector256(0x2a9f789300000000);

		auto result = Avx::AndNotPd(sign, Avx::MultiplyPd(vector, twoPow54));
		result = Avx2::BlendEpi8(abs, result, isNormal);
		result = Avx2::AddEpi64(
			Avx2::BlendEpi8(subnormalOffset, normalOffset, isNormal),
			Vector256(
				__m256i {
					.m256i_i64 = {
						result.AsInt64()[0] / 3,
						result.AsInt64()[1] / 3,
						result.AsInt64()[2] / 3,
						result.AsInt64()[3] / 3
					}
				}
			)
		);

		result = Avx::OrPd(result, sign);

		// first iteration
		result = Helper::CbrtHalleyPd(vector, result);
		// second iteration
		result = Helper::CbrtHalleyPd(vector, result);

		// handle nan, inf and zero
		result = Avx::BlendPd(result, vector, isZero);
		result = Avx::BlendPd(result, Avx::AddPd(vector, vector), isNanOrInf);

		return result;
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Exp(
		const TSimd vector
	)
	{
		return Vectorize(std::exp<TValue>, vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Exp(
		const Vector128 vector
	)
	{
		constexpr auto ln2High = Vector128(0.693359375f);
		constexpr auto ln2Low = Vector128(-2.12194440e-4f);

		auto rounded = Sse41::RoundPs<ERoundingType::NearestNoExcept>(
			Sse::MultiplyPs(vector, Log2ESimd128Ps)
		);

		auto x = Fma3::MultiplyNegateAddPs(rounded, ln2High, vector);
		x = Fma3::MultiplyNegateAddPs(rounded, ln2Low, x);

		const auto x2 = Sse::MultiplyPs(x, x);
		auto result = Fma3::MultiplyAddPs(Helper::PolynomialTaylor5Ps(x), x2, x);
		result = Sse::MultiplyPs(Sse::AddPs(result, OneSimd128Ps), Helper::Pow2Ps(rounded));

		const auto abs = Sse::AndPs(vector, AbsMaskSimd128Ps);
		auto isInRange = Sse::CompareLessPs(abs, Vector128(88.7f));

		// todo: use/extract simd-logical functions
		const auto exponentBits = Sse2::AndSi128(
			vector,
			InfinitySimd128Ps
		);
		isInRange = Sse::AndPs(
			isInRange,
			Sse::CompareNotEqualPs(exponentBits, InfinitySimd128Ps)
		);

		if (Sse::MoveMaskPs(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Sse2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Sse41::BlendPs(InfinitySimd128Ps, ZeroSimd128Ps, sign);
		// +/- underflow
		result = Sse41::BlendPs(rounded, result, isInRange);
		// nan
		result = Sse41::BlendPs(
			result,
			vector,
			Avx::ComparePs<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Exp(
		const Vector128 vector
	)
	{
		constexpr auto ln2High = Vector128(0.693145751953125);
		constexpr auto ln2Low = Vector128(1.42860682030941723212e-6);

		auto rounded = Sse41::RoundPd<ERoundingType::NearestNoExcept>(
			Sse2::MultiplyPd(vector, Log2ESimd128Pd)
		);

		auto x = Fma3::MultiplyNegateAddPd(rounded, ln2High, vector);
		x = Fma3::MultiplyNegateAddPd(rounded, ln2Low, x);

		auto result = Sse2::MultiplyPd(
			Sse2::AddPd(Helper::PolynomialTaylor11Pd(x), OneSimd128Pd),
			Helper::Pow2Pd(rounded)
		);

		const auto abs = Sse2::AndPd(vector, AbsMaskSimd128Pd);
		auto isInRange = Sse2::CompareLessPd(abs, Vector128(709.7));

		// todo: use/extract simd-logical functions
		const auto exponentBits = Sse2::AndSi128(
			vector,
			InfinitySimd128Pd
		);
		isInRange = Sse2::AndPd(
			isInRange,
			Sse2::CompareNotEqualPd(exponentBits, InfinitySimd128Pd)
		);

		if (Sse2::MoveMaskPd(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Sse2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Sse41::BlendPd(InfinitySimd128Pd, ZeroSimd128Pd, sign);
		// +/- underflow
		result = Sse41::BlendPd(rounded, result, isInRange);
		// nan
		result = Sse41::BlendPd(
			result,
			vector,
			Avx::ComparePd<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}

	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Exp(
		const Vector256 vector
	)
	{
		constexpr auto ln2High = Vector256(0.693145751953125);
		constexpr auto ln2Low = Vector256(1.42860682030941723212e-6);

		auto rounded = Avx::RoundPd<ERoundingType::NearestNoExcept>(
			Avx::MultiplyPd(vector, Log2ESimd256Pd)
		);

		auto x = Fma3::MultiplyNegateAddPd(rounded, ln2High, vector);
		x = Fma3::MultiplyNegateAddPd(rounded, ln2Low, x);

		auto result = Avx::MultiplyPd(
			Avx::AddPd(Helper::PolynomialTaylor11Pd(x), OneSimd256Pd),
			Helper::Pow2Pd(rounded)
		);

		const auto abs = Avx::AndPd(vector, AbsMaskSimd256Pd);
		auto isInRange = Avx::ComparePd<EComparisonType::LessOrderedNonSignaling>(abs, Vector256(709.7));

		// todo: use/extract simd-logical functions
		const auto exponentBits = Avx2::AndSi256(
			vector,
			InfinitySimd256Pd
		);
		isInRange = Avx::AndPd(
			isInRange,
			Avx::ComparePd<EComparisonType::NotEqualOrderedNonSignaling>(exponentBits, InfinitySimd256Pd)
		);

		if (Avx::MoveMaskPd(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Avx2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Avx::BlendPd(InfinitySimd256Pd, ZeroSimd256Pd, sign);
		// +/- underflow
		result = Avx::BlendPd(rounded, result, isInRange);
		// nan
		result = Avx::BlendPd(
			result,
			vector,
			Avx::ComparePd<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}


	template<class TSimd, class TValue>
	static constexpr TSimd ExpFast(
		const TSimd vector
	)
	{
		return Vectorize(std::exp<TValue>, vector);
	};


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 ExpFast(
		const Vector128 vector
	)
	{
		// https://stackoverflow.com/a/53872588

		constexpr auto aPositive = Vector128(0.5f / Ln2<>);
		constexpr auto aNegative = Vector128(-0.5f / Ln2<>);
		constexpr auto b = Vector128(3 * NumericLimits<float32>::ExponentRealMax - 0.05f);

		auto resultPositive = Fma3::MultiplyAddPs(vector, aPositive, b);
		auto resultNegative = Fma3::MultiplyAddPs(vector, aNegative, b);

		return Sse::DividePs(
			Sse2::ShiftLeftLogicalEpi32<NumericLimits<float32>::ExponentSize>(resultPositive),
			Sse2::ShiftLeftLogicalEpi32<NumericLimits<float32>::ExponentSize>(resultNegative)

		);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 ExpFast(
		const Vector128 vector
	)
	{
		// https://stackoverflow.com/a/53872588

		constexpr auto aPositive = Vector128(0.5f / Ln2<float64>);
		constexpr auto aNegative = Vector128(-0.5f / Ln2<float64>);
		constexpr auto b = Vector128(3 * NumericLimits<float64>::ExponentRealMax - 0.05f);

		auto resultPositive = Fma3::MultiplyAddPd(vector, aPositive, b);
		auto resultNegative = Fma3::MultiplyAddPd(vector, aNegative, b);

		return Sse2::DividePd(
			Sse2::ShiftLeftLogicalEpi64<NumericLimits<float64>::ExponentSize>(resultPositive),
			Sse2::ShiftLeftLogicalEpi64<NumericLimits<float64>::ExponentSize>(resultNegative)
		);
	};


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 ExpFast(
		const Vector256 vector
	)
	{
		// https://stackoverflow.com/a/53872588

		constexpr auto aPositive = Vector256(0.5 / Ln2<float64>);
		constexpr auto aNegative = Vector256(-0.5 / Ln2<float64>);
		constexpr auto b = Vector256(3 * NumericLimits<float64>::ExponentRealMax - 0.05);

		const auto resultPositive = Fma3::MultiplyAddPd(vector, aPositive, b);
		const auto resultNegative = Fma3::MultiplyAddPd(vector, aNegative, b);

		return Avx::DividePd(
			Avx2::ShiftLeftLogicalEpi64<NumericLimits<float64>::ExponentSize>(resultPositive),
			Avx2::ShiftLeftLogicalEpi64<NumericLimits<float64>::ExponentSize>(resultNegative)
		);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd ExpMinus1(
		const TSimd vector
	)
	{
		return Vectorize(std::expm1<TValue>, vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 ExpMinus1(
		const Vector128 vector
	)
	{
		constexpr auto ln2High = Vector128(0.693359375f);
		constexpr auto ln2Low = Vector128(-2.12194440e-4f);

		auto rounded = Sse41::RoundPs<ERoundingType::NearestNoExcept>(
			Sse::MultiplyPs(vector, Log2ESimd128Ps)
		);

		auto x = Fma3::MultiplyNegateAddPs(rounded, ln2High, vector);
		x = Fma3::MultiplyNegateAddPs(rounded, ln2Low, x);

		auto x2 = Sse::MultiplyPs(x, x);
		const auto powerOf2 = Helper::Pow2Ps(rounded);
		auto result = Fma3::MultiplyAddPs(
			Helper::PolynomialTaylor5Ps(x),
			x2,
			x
		);
		result = Fma3::MultiplyAddPs(
			result,
			powerOf2,
			Sse::SubtractPs(powerOf2, OneSimd128Ps)
		);

		const auto abs = Sse::AndPs(vector, AbsMaskSimd128Ps);
		auto isInRange = Sse::CompareLessPs(abs, Vector128(88.7f));

		// todo: use/extract simd-logical functions
		const auto exponentBits = Sse2::AndSi128(
			vector,
			InfinitySimd128Ps
		);
		isInRange = Sse::AndPs(
			isInRange,
			Sse::CompareNotEqualPs(exponentBits, InfinitySimd128Ps)
		);

		if (Sse::MoveMaskPs(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Sse2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Sse41::BlendPs(InfinitySimd128Ps, ZeroSimd128Ps, sign);
		// +/- underflow
		result = Sse41::BlendPs(rounded, result, isInRange);
		// nan
		result = Sse41::BlendPs(
			result,
			vector,
			Avx::ComparePs<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 ExpMinus1(
		const Vector128 vector
	)
	{
		constexpr auto ln2High = Vector128(0.693145751953125);
		constexpr auto ln2Low = Vector128(1.42860682030941723212e-6);

		auto rounded = Sse41::RoundPd<ERoundingType::NearestNoExcept>(
			Sse2::MultiplyPd(vector, Log2ESimd128Pd)
		);

		auto x = Fma3::MultiplyNegateAddPd(rounded, ln2High, vector);
		x = Fma3::MultiplyNegateAddPd(rounded, ln2Low, x);

		const auto powerOf2 = Helper::Pow2Pd(rounded);
		auto result = Fma3::MultiplyAddPd(
			Helper::PolynomialTaylor11Pd(x),
			powerOf2,
			Sse2::SubtractPd(powerOf2, OneSimd128Pd)
		);

		const auto abs = Sse2::AndPd(vector, AbsMaskSimd128Pd);
		auto isInRange = Sse2::CompareLessPd(abs, Vector128(709.7));

		// todo: use/extract simd-logical functions
		const auto exponentBits = Sse2::AndSi128(
			vector,
			InfinitySimd128Pd
		);
		isInRange = Sse2::AndPd(
			isInRange,
			Sse2::CompareNotEqualPd(exponentBits, InfinitySimd128Pd)
		);

		if (Sse2::MoveMaskPd(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Sse2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Sse41::BlendPd(InfinitySimd128Pd, ZeroSimd128Pd, sign);
		// +/- underflow
		result = Sse41::BlendPd(rounded, result, isInRange);
		// nan
		result = Sse41::BlendPd(
			result,
			vector,
			Avx::ComparePd<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 ExpMinus1(
		const Vector256 vector
	)
	{
		constexpr auto ln2High = Vector256(0.693145751953125);
		constexpr auto ln2Low = Vector256(1.42860682030941723212e-6);

		auto rounded = Avx::RoundPd<ERoundingType::NearestNoExcept>(
			Avx::MultiplyPd(vector, Log2ESimd256Pd)
		);

		auto x = Fma3::MultiplyNegateAddPd(rounded, ln2High, vector);
		x = Fma3::MultiplyNegateAddPd(rounded, ln2Low, x);

		const auto powerOf2 = Helper::Pow2Pd(rounded);
		auto result = Fma3::MultiplyAddPd(
			Helper::PolynomialTaylor11Pd(x),
			powerOf2,
			Avx::SubtractPd(powerOf2, OneSimd256Pd)
		);

		const auto abs = Avx::AndPd(vector, AbsMaskSimd256Pd);
		auto isInRange = Avx::ComparePd<EComparisonType::LessOrderedNonSignaling>(abs, Vector256(709.7));

		// todo: use/extract simd-logical functions
		const auto exponentBits = Avx2::AndSi256(
			vector,
			InfinitySimd256Pd
		);
		isInRange = Avx::AndPd(
			isInRange,
			Avx::ComparePd<EComparisonType::NotEqualOrderedNonSignaling>(exponentBits, InfinitySimd256Pd)
		);

		if (Avx::MoveMaskPd(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Avx2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Avx::BlendPd(InfinitySimd256Pd, ZeroSimd256Pd, sign);
		// +/- underflow
		result = Avx::BlendPd(rounded, result, isInRange);
		// nan
		result = Avx::BlendPd(
			result,
			vector,
			Avx::ComparePd<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Ln(
		const TSimd vector
	)
	{
		return Vectorize(std::log<TValue>, vector);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd LnPlus1(
		const TSimd vector
	)
	{
		return Vectorize(std::log1p<TValue>, vector);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Log10(
		const TSimd vector
	)
	{
		return Vectorize(std::log10<TValue>, vector);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Log2(
		const TSimd vector
	)
	{
		return Vectorize(std::log2<TValue>, vector);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Pow2(
		const TSimd vector
	)
	{
		return Vectorize(std::exp2<TValue>, vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Pow2(
		const Vector128 vector
	)
	{
		auto rounded = Sse41::RoundPs<ERoundingType::NearestNoExcept>(vector);
		auto result = Helper::Pow2Ps(rounded);

		const auto abs = Sse::AndPs(vector, AbsMaskSimd128Ps);
		auto isInRange = Sse::CompareLessPs(
			abs,
			Float32ExponentRealMaxSimd128Ps
		);

		// todo: use/extract simd-logical functions
		const auto exponentBits = Sse2::AndSi128(
			vector,
			InfinitySimd128Ps
		);
		isInRange = Sse::AndPs(
			isInRange,
			Sse::CompareNotEqualPs(exponentBits, InfinitySimd128Ps)
		);

		if (Sse::MoveMaskPs(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Sse2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Sse41::BlendPs(InfinitySimd128Ps, ZeroSimd128Ps, sign);
		// +/- underflow
		result = Sse41::BlendPs(rounded, result, isInRange);
		// nan
		result = Sse41::BlendPs(
			result,
			vector,
			Avx::ComparePs<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Pow2(
		const Vector128 vector
	)
	{
		auto rounded = Sse41::RoundPd<ERoundingType::NearestNoExcept>(vector);
		auto result = Helper::Pow2Pd(rounded);

		const auto abs = Sse2::AndPd(vector, AbsMaskSimd128Pd);
		auto isInRange = Sse2::CompareLessPd(
			abs,
			Float64ExponentRealMaxSimd128Pd
		);

		// todo: use/extract simd-logical functions
		const auto exponentBits = Sse2::AndSi128(
			vector,
			InfinitySimd128Pd
		);
		isInRange = Sse2::AndPd(
			isInRange,
			Sse2::CompareNotEqualPd(exponentBits, InfinitySimd128Pd)
		);

		if (Sse2::MoveMaskPd(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Sse2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Sse41::BlendPd(InfinitySimd128Pd, ZeroSimd128Pd, sign);
		// +/- underflow
		result = Sse41::BlendPd(rounded, result, isInRange);
		// nan
		result = Sse41::BlendPd(
			result,
			vector,
			Avx::ComparePd<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Pow2(
		const Vector256 vector
	)
	{
		auto rounded = Avx::RoundPd<ERoundingType::NearestNoExcept>(vector);
		auto result = Helper::Pow2Pd(rounded);

		const auto abs = Avx::AndPd(vector, AbsMaskSimd256Pd);
		auto isInRange = Avx::ComparePd<EComparisonType::LessOrderedNonSignaling>(
			abs,
			Float64ExponentRealMaxSimd256Pd
		);

		// todo: use/extract simd-logical functions
		const auto exponentBits = Avx2::AndSi256(
			vector,
			InfinitySimd256Pd
		);
		isInRange = Avx::AndPd(
			isInRange,
			Avx::ComparePd<EComparisonType::NotEqualOrderedNonSignaling>(exponentBits, InfinitySimd256Pd)
		);

		if (Avx::MoveMaskPd(isInRange) == 0xf)
		{
			return result;
		}

		// +/- overflow or infinity
		const auto sign = Avx2::ShiftRightArithmeticEpi32<NumericLimits<float32>::RealSize>(vector);
		rounded = Avx::BlendPd(InfinitySimd256Pd, ZeroSimd256Pd, sign);
		// +/- underflow
		result = Avx::BlendPd(rounded, result, isInRange);
		// nan
		result = Avx::BlendPd(
			result,
			vector,
			Avx::ComparePd<EComparisonType::UnorderedNonSignaling>(vector, vector)
		);

		return result;
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Pow(
		const TSimd vector,
		const TSimd power
	)
	{
		return Vectorize(std::pow<TValue>, vector, power);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Sqrt(
		const TSimd vector
	)
	{
		return Vectorize(std::sqrt<TValue>, vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Sqrt(
		const Vector128 vector
	)
	{
		return Sse::SqrtPs(vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Sqrt(
		const Vector128 vector
	)
	{
		return Sse2::SqrtPd(vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float32>
	static inline Vector256 Sqrt(
		const Vector256 vector
	)
	{
		return Avx::SqrtPs(vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Sqrt(
		const Vector256 vector
	)
	{
		return Avx::SqrtPd(vector);
	}
}
