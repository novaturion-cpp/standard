#pragma once
#include "standard/math/scalar/constant.hpp"

#include "standard/platform/intrinsic/type.hpp"

#include "standard/utility/numeric-limits.hpp"


namespace Std::inline Math::inline Simd
{
	// Vector128 float32

	inline constexpr auto AbsMaskSimd128Ps = Vector128(0x7fffffff_i32);

	inline constexpr auto InfinitySimd128Ps = Vector128(NumericLimits<float32>::Infinity);
	inline constexpr auto QuietNanSimd128Ps = Vector128(NumericLimits<float32>::QuietNan);
	inline constexpr auto SignalingNanSimd128Ps = Vector128(NumericLimits<float32>::SignalingNan);

	inline constexpr auto NegativeInfinitySimd128Ps = Vector128(-NumericLimits<float32>::Infinity);
	inline constexpr auto NegativeQuietNanSimd128Ps = Vector128(-NumericLimits<float32>::QuietNan);
	inline constexpr auto NegativeSignalingNanSimd128Ps = Vector128(-NumericLimits<float32>::SignalingNan);

	inline constexpr auto Float32MaxSimd128Ps = Vector128(NumericLimits<float32>::Max);
	inline constexpr auto Float32MinSimd128Ps = Vector128(NumericLimits<float32>::Min);
	inline constexpr auto Float32EpsilonSimd128Ps = Vector128(NumericLimits<float32>::Epsilon);

	inline constexpr auto Float32ExponentRealMaxSimd128Ps = Vector128(
		static_cast<float32>(NumericLimits<float32>::ExponentRealMax)
	);
	inline constexpr auto Float32ExponentRealMinSimd128Ps = Vector128(
		static_cast<float32>(NumericLimits<float32>::ExponentRealMin)
	);
	inline constexpr auto Float32ExponentBiasSimd128Ps = Vector128(
		static_cast<float32>(NumericLimits<float32>::ExponentBias)
	);

	inline constexpr auto Float32ExponentMaskSimd128Ps = Vector128(NumericLimits<float32>::ExponentMask);
	inline constexpr auto Float32MantissaMaskSimd128Ps = Vector128(NumericLimits<float32>::MantissaMask);

	inline constexpr auto ESimd128Ps = Vector128(E<>);
	inline constexpr auto EulerSimd128Ps = Vector128(Euler<>);
	inline constexpr auto GaussSimd128Ps = Vector128(Gauss<>);
	inline constexpr auto PhiSimd128Ps = Vector128(Phi<>);
	inline constexpr auto PiSimd128Ps = Vector128(Pi<>);

	inline constexpr auto ZeroSimd128Ps = Vector128(0.0f);
	inline constexpr auto OneSimd128Ps = Vector128(1.0f);
	// inline constexpr auto TwoSimd128Ps = Vector128(2.0f);
	// inline constexpr auto FourSimd128Ps = Vector128(4.0f);
	// inline constexpr auto SixSimd128Ps = Vector128(6.0f);

	inline constexpr auto NegativeZeroSimd128Ps = Vector128(0x80000000_i32);
	inline constexpr auto NegativeOneSimd128Ps = Vector128(-1.0f);

	inline constexpr auto OneDiv2Simd128Ps = Vector128(OneDiv2<>);
	inline constexpr auto OneDiv2PiSimd128Ps = Vector128(OneDiv2Pi<>);
	inline constexpr auto OneDiv3Simd128Ps = Vector128(OneDiv3<>);
	inline constexpr auto OneDiv4Simd128Ps = Vector128(OneDiv4<>);
	inline constexpr auto OneDiv6Simd128Ps = Vector128(OneDiv6<>);
	inline constexpr auto OneDivEulerSimd128Ps = Vector128(OneDivEuler<>);
	inline constexpr auto OneDivPiSimd128Ps = Vector128(OneDivPi<>);
	inline constexpr auto OneDivRoot2Simd128Ps = Vector128(OneDivRoot2<>);
	inline constexpr auto OneDivRoot3Simd128Ps = Vector128(OneDivRoot3<>);
	inline constexpr auto ThreeDiv4Simd128Ps = Vector128(ThreeDiv4<>);
	inline constexpr auto TwoDiv3Simd128Ps = Vector128(TwoDiv3<>);
	inline constexpr auto TwoDivPiSimd128Ps = Vector128(TwoDivPi<>);

	inline constexpr auto NegativeOneDiv2Simd128Ps = Vector128(-0.5f);

	inline constexpr auto Root2Simd128Ps = Vector128(Root2<>);
	inline constexpr auto Root3Simd128Ps = Vector128(Root3<>);
	inline constexpr auto RootESimd128Ps = Vector128(RootE<>);
	inline constexpr auto RootPiSimd128Ps = Vector128(RootPi<>);
	inline constexpr auto RootPiMul2Simd128Ps = Vector128(RootPiMul2<>);

	inline constexpr auto Ln10Simd128Ps = Vector128(Ln10<>);
	inline constexpr auto Ln2Simd128Ps = Vector128(Ln2<>);
	inline constexpr auto LnLn2Simd128Ps = Vector128(LnLn2<>);
	inline constexpr auto LnPhiSimd128Ps = Vector128(LnPhi<>);
	inline constexpr auto Log10ESimd128Ps = Vector128(Log10E<>);
	inline constexpr auto Log2ESimd128Ps = Vector128(Log2E<>);
	// inline constexpr auto Log2EDiv2Simd128Ps = Vector128(-0.721348f);
	// inline constexpr auto Log210Simd128Ps = Vector128(3.321928f);

	inline constexpr auto EPowPiSimd128Ps = Vector128(EPowPi<>);
	inline constexpr auto EulerPow2Simd128Ps = Vector128(EulerPow2<>);

	inline constexpr auto DegreesToRadiansSimd128Ps = Vector128(DegreesToRadians<>);
	inline constexpr auto RadiansToDegreesSimd128Ps = Vector128(RadiansToDegrees<>);

	inline constexpr auto PiDiv2Simd128Ps = Vector128(PiDiv2<>);
	inline constexpr auto PiDiv3Simd128Ps = Vector128(PiDiv3<>);
	inline constexpr auto PiDiv4Simd128Ps = Vector128(PiDiv4<>);
	inline constexpr auto PiDiv6Simd128Ps = Vector128(PiDiv6<>);
	inline constexpr auto PiPow2Simd128Ps = Vector128(PiPow2<>);
	inline constexpr auto PiPowESimd128Ps = Vector128(PiPowE<>);
	inline constexpr auto PiMul2Simd128Ps = Vector128(PiMul2<>);
	inline constexpr auto PiMul3Div4Simd128Ps = Vector128(PiMul3Div4<>);

	// inline constexpr auto PiConstants0Simd128Ps = Vector128(__m128 {.m128_f32 ={static_cast<float32>(Pi), static_cast<float32>(PiMul2), static_cast<float32>(OneDivPi), static_cast<float32>(OneDiv2Pi) } });

	inline constexpr auto Cos0Simd128Ps = Vector128(Cos0<>);
	inline constexpr auto Cos15Simd128Ps = Vector128(Cos15<>);
	inline constexpr auto Cos30Simd128Ps = Vector128(Cos30<>);
	inline constexpr auto Cos45Simd128Ps = Vector128(Cos45<>);
	inline constexpr auto Cos60Simd128Ps = Vector128(Cos60<>);
	inline constexpr auto Cos75Simd128Ps = Vector128(Cos75<>);
	inline constexpr auto Cos90Simd128Ps = Vector128(Cos90<>);

	// inline constexpr auto CosCoefficients0Simd128Ps = Vector128(__m128 {.m128_f32 ={ -0.5f, 0.041666638f, -0.0013888378f, 2.4760495e-05f } });
	// inline constexpr auto CosCoefficients1Simd128Ps = Vector128(__m128 {.m128_f32 ={ -2.6051615e-07f, -0.49992746f, 0.041493919f, -0.0012712436f } });

	inline constexpr auto Sin0Simd128Ps = Vector128(Sin0<>);
	inline constexpr auto Sin15Simd128Ps = Vector128(Sin15<>);
	inline constexpr auto Sin30Simd128Ps = Vector128(Sin30<>);
	inline constexpr auto Sin45Simd128Ps = Vector128(Sin45<>);
	inline constexpr auto Sin60Simd128Ps = Vector128(Sin60<>);
	inline constexpr auto Sin75Simd128Ps = Vector128(Sin75<>);
	inline constexpr auto Sin90Simd128Ps = Vector128(Sin90<>);

	// inline constexpr auto SinCoefficients0Simd128Ps = Vector128(__m128 {.m128_f32 ={ -0.16666667f, 0.0083333310f, -0.00019840874f, 2.7525562e-06f } });
	// inline constexpr auto SinCoefficients1Simd128Ps = Vector128(__m128 {.m128_f32 ={ -2.3889859e-08f, -0.16665852f, 0.0083139502f, -0.00018524670f } });

	inline constexpr auto Tan0Simd128Ps = Vector128(Tan0<>);
	inline constexpr auto Tan15Simd128Ps = Vector128(Tan15<>);
	inline constexpr auto Tan30Simd128Ps = Vector128(Tan30<>);
	inline constexpr auto Tan45Simd128Ps = Vector128(Tan45<>);
	inline constexpr auto Tan60Simd128Ps = Vector128(Tan60<>);
	inline constexpr auto Tan75Simd128Ps = Vector128(Tan75<>);

	// inline constexpr auto TanCoefficients0Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.0f, 0.333333333f, 0.133333333f, 5.396825397e-2f } });
	// inline constexpr auto TanCoefficients1Simd128Ps = Vector128(__m128 {.m128_f32 ={ 2.186948854e-2f, 8.863235530e-3f, 3.592128167e-3f, 1.455834485e-3f } });
	// inline constexpr auto TanCoefficients2Simd128Ps = Vector128(__m128 {.m128_f32 ={ 5.900274264e-4f, 2.391290764e-4f, 9.691537707e-5f, 3.927832950e-5f } });
	// inline constexpr auto TanEstimatedCoefficientsSimd128Ps = Vector128(__m128 {.m128_f32 ={ 2.484f, -1.954923183e-1f, 2.467401101f, static_cast<float32>(OneDivPi) } });

	// inline constexpr auto ArcCoefficients0Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.5707963050f, -0.2145988016f, 0.0889789874f, -0.0501743046f } });
	// inline constexpr auto ArcCoefficients1Simd128Ps = Vector128(__m128 {.m128_f32 ={ 0.0308918810f, -0.0170881256f, 0.0066700901f, -0.0012624911f } });

	// inline constexpr auto ArcEstimatedCoefficientsSimd128Ps = Vector128(__m128 {.m128_f32 ={ 1.5707288f, -0.2121144f, 0.0742610f, -0.0187293f } });

	// inline constexpr auto ATanCoefficients0Simd128Ps = Vector128(__m128 {.m128_f32 ={ -0.3333314528f, 0.1999355085f, -0.1420889944f, 0.1065626393f } });
	// inline constexpr auto ATanCoefficients1Simd128Ps = Vector128(__m128 {.m128_f32 ={ -0.0752896400f, 0.0429096138f, -0.0161657367f, 0.0028662257f } });

	// inline constexpr auto ATanEstimatedCoefficients0Simd128Ps = Vector128(__m128 {.m128_f32 ={ 0.999866f, 0.999866f, 0.999866f, 0.999866f } });
	// inline constexpr auto ATanEstimatedCoefficients1Simd128Ps = Vector128(__m128 {.m128_f32 ={ -0.3302995f, 0.180141f, -0.085133f, 0.0208351f } });

	inline constexpr auto TaylorCoefficient0Simd128Ps = Vector128(TaylorCoefficient0<>);
	inline constexpr auto TaylorCoefficient1Simd128Ps = Vector128(TaylorCoefficient1<>);
	inline constexpr auto TaylorCoefficient2Simd128Ps = Vector128(TaylorCoefficient2<>);
	inline constexpr auto TaylorCoefficient3Simd128Ps = Vector128(TaylorCoefficient3<>);
	inline constexpr auto TaylorCoefficient4Simd128Ps = Vector128(TaylorCoefficient4<>);
	inline constexpr auto TaylorCoefficient5Simd128Ps = Vector128(TaylorCoefficient5<>);
	inline constexpr auto TaylorCoefficient6Simd128Ps = Vector128(TaylorCoefficient6<>);
	inline constexpr auto TaylorCoefficient7Simd128Ps = Vector128(TaylorCoefficient7<>);
	inline constexpr auto TaylorCoefficient8Simd128Ps = Vector128(TaylorCoefficient8<>);
	inline constexpr auto TaylorCoefficient9Simd128Ps = Vector128(TaylorCoefficient9<>);
	inline constexpr auto TaylorCoefficient10Simd128Ps = Vector128(TaylorCoefficient10<>);
	inline constexpr auto TaylorCoefficient11Simd128Ps = Vector128(TaylorCoefficient11<>);

	inline constexpr auto IdentityMatrixRow0Simd128Ps = Vector128(__m128 {.m128_f32 = {1.0f, 0.0f, 0.0f, 0.0f}});
	inline constexpr auto IdentityMatrixRow1Simd128Ps = Vector128(__m128 {.m128_f32 = {0.0f, 1.0f, 0.0f, 0.0f}});
	inline constexpr auto IdentityMatrixRow2Simd128Ps = Vector128(__m128 {.m128_f32 = {0.0f, 0.0f, 1.0f, 0.0f}});
	inline constexpr auto IdentityMatrixRow3Simd128Ps = Vector128(__m128 {.m128_f32 = {0.0f, 0.0f, 0.0f, 1.0f}});
	inline constexpr auto NegativeIdentityMatrixRow0Simd128Ps = Vector128(
		__m128 {.m128_f32 = {-1.0f, 0.0f, 0.0f, 0.0f}}
	);
	inline constexpr auto NegativeIdentityMatrixRow1Simd128Ps = Vector128(
		__m128 {.m128_f32 = {0.0f, -1.0f, 0.0f, 0.0f}}
	);
	inline constexpr auto NegativeIdentityMatrixRow2Simd128Ps = Vector128(
		__m128 {.m128_f32 = {0.0f, 0.0f, -1.0f, 0.0f}}
	);
	inline constexpr auto NegativeIdentityMatrixRow3Simd128Ps = Vector128(
		__m128 {.m128_f32 = {0.0f, 0.0f, 0.0f, -1.0f}}
	);

	inline constexpr auto NegateXSimd128Ps = Vector128(__m128 {.m128_f32 = {-1.0f, 1.0f, 1.0f, 1.0f}});
	inline constexpr auto NegateYSimd128Ps = Vector128(__m128 {.m128_f32 = {1.0f, -1.0f, 1.0f, 1.0f}});
	inline constexpr auto NegateZSimd128Ps = Vector128(__m128 {.m128_f32 = {1.0f, 1.0f, -1.0f, 1.0f}});
	inline constexpr auto NegateWSimd128Ps = Vector128(__m128 {.m128_f32 = {1.0f, 1.0f, 1.0f, -1.0f}});


	// Vector128 float64

	inline constexpr auto AbsMaskSimd128Pd = Vector128(0x7fffffffffffffff_i64);

	inline constexpr auto InfinitySimd128Pd = Vector128(NumericLimits<float64>::Infinity);
	inline constexpr auto QuietNanSimd128Pd = Vector128(NumericLimits<float64>::QuietNan);
	inline constexpr auto SignalingNanSimd128Pd = Vector128(NumericLimits<float64>::SignalingNan);

	inline constexpr auto NegativeInfinitySimd128Pd = Vector128(-NumericLimits<float64>::Infinity);
	inline constexpr auto NegativeQuietNanSimd128Pd = Vector128(-NumericLimits<float64>::QuietNan);
	inline constexpr auto NegativeSignalingNanSimd128Pd = Vector128(-NumericLimits<float64>::SignalingNan);

	inline constexpr auto Float32MaxSimd128Pd = Vector128(static_cast<float64>(NumericLimits<float32>::Max));
	inline constexpr auto Float32MinSimd128Pd = Vector128(static_cast<float64>(NumericLimits<float32>::Min));
	inline constexpr auto Float32EpsilonSimd128Pd = Vector128(static_cast<float64>(NumericLimits<float32>::Epsilon));

	inline constexpr auto Float32ExponentRealMaxSimd128Pd = Vector128(
		static_cast<float64>(NumericLimits<float32>::ExponentRealMax)
	);
	inline constexpr auto Float32ExponentRealMinSimd128Pd = Vector128(
		static_cast<float64>(NumericLimits<float32>::ExponentRealMin)
	);
	inline constexpr auto Float32ExponentBiasSimd128Pd = Vector128(
		static_cast<float64>(NumericLimits<float32>::ExponentBias)
	);

	inline constexpr auto Float32ExponentMaskSimd128Pd = Vector128(NumericLimits<float32>::ExponentMask);
	inline constexpr auto Float32MantissaMaskSimd128Pd = Vector128(NumericLimits<float32>::MantissaMask);

	inline constexpr auto Float64MaxSimd128Pd = Vector128(NumericLimits<float64>::Max);
	inline constexpr auto Float64MinSimd128Pd = Vector128(NumericLimits<float64>::Min);
	inline constexpr auto Float64EpsilonSimd128Pd = Vector128(NumericLimits<float64>::Epsilon);

	inline constexpr auto Float64ExponentRealMaxSimd128Pd = Vector128(
		static_cast<float64>(NumericLimits<float64>::ExponentRealMax)
	);
	inline constexpr auto Float64ExponentRealMinSimd128Pd = Vector128(
		static_cast<float64>(NumericLimits<float64>::ExponentRealMin)
	);
	inline constexpr auto Float64ExponentBiasSimd128Pd = Vector128(
		static_cast<float64>(NumericLimits<float64>::ExponentBias)
	);

	inline constexpr auto Float64ExponentMaskSimd128Pd = Vector128(NumericLimits<float64>::ExponentMask);
	inline constexpr auto Float64MantissaMaskSimd128Pd = Vector128(NumericLimits<float64>::MantissaMask);

	inline constexpr auto ESimd128Pd = Vector128(E<float64>);
	inline constexpr auto EulerSimd128Pd = Vector128(Euler<float64>);
	inline constexpr auto GaussSimd128Pd = Vector128(Gauss<float64>);
	inline constexpr auto PhiSimd128Pd = Vector128(Phi<float64>);
	inline constexpr auto PiSimd128Pd = Vector128(Pi<float64>);

	inline constexpr auto ZeroSimd128Pd = Vector128(0.0);
	inline constexpr auto OneSimd128Pd = Vector128(1.0);
	// inline constexpr auto TwoSimd128Pd = Vector128(2.0);
	// inline constexpr auto FourSimd128Pd = Vector128(4.0);
	// inline constexpr auto SixSimd128Pd = Vector128(6.0);

	inline constexpr auto NegativeZeroSimd128Pd = Vector128(0x8000000000000000_i64);
	inline constexpr auto NegativeOneSimd128Pd = Vector128(-1.0);

	inline constexpr auto OneDiv2Simd128Pd = Vector128(OneDiv2<float64>);
	inline constexpr auto OneDiv2PiSimd128Pd = Vector128(OneDiv2Pi<float64>);
	inline constexpr auto OneDiv3Simd128Pd = Vector128(OneDiv3<float64>);
	inline constexpr auto OneDiv4Simd128Pd = Vector128(OneDiv4<float64>);
	inline constexpr auto OneDiv6Simd128Pd = Vector128(OneDiv6<float64>);
	inline constexpr auto OneDivEulerSimd128Pd = Vector128(OneDivEuler<float64>);
	inline constexpr auto OneDivPiSimd128Pd = Vector128(OneDivPi<float64>);
	inline constexpr auto OneDivRoot2Simd128Pd = Vector128(OneDivRoot2<float64>);
	inline constexpr auto OneDivRoot3Simd128Pd = Vector128(OneDivRoot3<float64>);
	inline constexpr auto ThreeDiv4Simd128Pd = Vector128(ThreeDiv4<float64>);
	inline constexpr auto TwoDiv3Simd128Pd = Vector128(TwoDiv3<float64>);
	inline constexpr auto TwoDivPiSimd128Pd = Vector128(TwoDivPi<float64>);

	inline constexpr auto NegativeOneDiv2Simd128Pd = Vector128(-0.5);

	inline constexpr auto Root2Simd128Pd = Vector128(Root2<float64>);
	inline constexpr auto Root3Simd128Pd = Vector128(Root3<float64>);
	inline constexpr auto RootESimd128Pd = Vector128(RootE<float64>);
	inline constexpr auto RootPiSimd128Pd = Vector128(RootPi<float64>);
	inline constexpr auto RootPiMul2Simd128Pd = Vector128(RootPiMul2<float64>);

	inline constexpr auto Ln10Simd128Pd = Vector128(Ln10<float64>);
	inline constexpr auto Ln2Simd128Pd = Vector128(Ln2<float64>);
	inline constexpr auto LnLn2Simd128Pd = Vector128(LnLn2<float64>);
	inline constexpr auto LnPhiSimd128Pd = Vector128(LnPhi<float64>);
	inline constexpr auto Log10ESimd128Pd = Vector128(Log10E<float64>);
	inline constexpr auto Log2ESimd128Pd = Vector128(Log2E<float64>);
	// inline constexpr auto Log2EDiv2Simd128Pd = Vector128(-0.721348);
	// inline constexpr auto Log210Simd128Pd = Vector128(3.321928);

	inline constexpr auto EPowPiSimd128Pd = Vector128(EPowPi<float64>);
	inline constexpr auto EulerPow2Simd128Pd = Vector128(EulerPow2<float64>);

	inline constexpr auto DegreesToRadiansSimd128Pd = Vector128(DegreesToRadians<float64>);
	inline constexpr auto RadiansToDegreesSimd128Pd = Vector128(RadiansToDegrees<float64>);

	inline constexpr auto PiDiv2Simd128Pd = Vector128(PiDiv2<float64>);
	inline constexpr auto PiDiv3Simd128Pd = Vector128(PiDiv3<float64>);
	inline constexpr auto PiDiv4Simd128Pd = Vector128(PiDiv4<float64>);
	inline constexpr auto PiDiv6Simd128Pd = Vector128(PiDiv6<float64>);
	inline constexpr auto PiPow2Simd128Pd = Vector128(PiPow2<float64>);
	inline constexpr auto PiPowESimd128Pd = Vector128(PiPowE<float64>);
	inline constexpr auto PiMul2Simd128Pd = Vector128(PiMul2<float64>);
	inline constexpr auto PiMul3Div4Simd128Pd = Vector128(PiMul3Div4<float64>);

	inline constexpr auto Cos0Simd128Pd = Vector128(Cos0<float64>);
	inline constexpr auto Cos15Simd128Pd = Vector128(Cos15<float64>);
	inline constexpr auto Cos30Simd128Pd = Vector128(Cos30<float64>);
	inline constexpr auto Cos45Simd128Pd = Vector128(Cos45<float64>);
	inline constexpr auto Cos60Simd128Pd = Vector128(Cos60<float64>);
	inline constexpr auto Cos75Simd128Pd = Vector128(Cos75<float64>);
	inline constexpr auto Cos90Simd128Pd = Vector128(Cos90<float64>);

	inline constexpr auto Sin0Simd128Pd = Vector128(Sin0<float64>);
	inline constexpr auto Sin15Simd128Pd = Vector128(Sin15<float64>);
	inline constexpr auto Sin30Simd128Pd = Vector128(Sin30<float64>);
	inline constexpr auto Sin45Simd128Pd = Vector128(Sin45<float64>);
	inline constexpr auto Sin60Simd128Pd = Vector128(Sin60<float64>);
	inline constexpr auto Sin75Simd128Pd = Vector128(Sin75<float64>);
	inline constexpr auto Sin90Simd128Pd = Vector128(Sin90<float64>);

	inline constexpr auto Tan0Simd128Pd = Vector128(Tan0<float64>);
	inline constexpr auto Tan15Simd128Pd = Vector128(Tan15<float64>);
	inline constexpr auto Tan30Simd128Pd = Vector128(Tan30<float64>);
	inline constexpr auto Tan45Simd128Pd = Vector128(Tan45<float64>);
	inline constexpr auto Tan60Simd128Pd = Vector128(Tan60<float64>);
	inline constexpr auto Tan75Simd128Pd = Vector128(Tan75<float64>);

	inline constexpr auto TaylorCoefficient0Simd128Pd = Vector128(TaylorCoefficient0<float64>);
	inline constexpr auto TaylorCoefficient1Simd128Pd = Vector128(TaylorCoefficient1<float64>);
	inline constexpr auto TaylorCoefficient2Simd128Pd = Vector128(TaylorCoefficient2<float64>);
	inline constexpr auto TaylorCoefficient3Simd128Pd = Vector128(TaylorCoefficient3<float64>);
	inline constexpr auto TaylorCoefficient4Simd128Pd = Vector128(TaylorCoefficient4<float64>);
	inline constexpr auto TaylorCoefficient5Simd128Pd = Vector128(TaylorCoefficient5<float64>);
	inline constexpr auto TaylorCoefficient6Simd128Pd = Vector128(TaylorCoefficient6<float64>);
	inline constexpr auto TaylorCoefficient7Simd128Pd = Vector128(TaylorCoefficient7<float64>);
	inline constexpr auto TaylorCoefficient8Simd128Pd = Vector128(TaylorCoefficient8<float64>);
	inline constexpr auto TaylorCoefficient9Simd128Pd = Vector128(TaylorCoefficient9<float64>);
	inline constexpr auto TaylorCoefficient10Simd128Pd = Vector128(TaylorCoefficient10<float64>);
	inline constexpr auto TaylorCoefficient11Simd128Pd = Vector128(TaylorCoefficient11<float64>);


	// Vector256 float64

	inline constexpr auto AbsMaskSimd256Pd = Vector256(0x7fffffffffffffff_i64);

	inline constexpr auto InfinitySimd256Pd = Vector256(NumericLimits<float64>::Infinity);
	inline constexpr auto QuietNanSimd256Pd = Vector256(NumericLimits<float64>::QuietNan);
	inline constexpr auto SignalingNanSimd256Pd = Vector256(NumericLimits<float64>::SignalingNan);

	inline constexpr auto NegativeInfinitySimd256Pd = Vector256(-NumericLimits<float64>::Infinity);
	inline constexpr auto NegativeQuietNanSimd256Pd = Vector256(-NumericLimits<float64>::QuietNan);
	inline constexpr auto NegativeSignalingNanSimd256Pd = Vector256(-NumericLimits<float64>::SignalingNan);

	inline constexpr auto Float64MaxSimd256Pd = Vector256(NumericLimits<float64>::Max);
	inline constexpr auto Float64MinSimd256Pd = Vector256(NumericLimits<float64>::Min);
	inline constexpr auto Float64EpsilonSimd256Pd = Vector256(NumericLimits<float64>::Epsilon);

	inline constexpr auto Float64ExponentRealMaxSimd256Pd = Vector256(
		static_cast<float64>(NumericLimits<float64>::ExponentRealMax)
	);
	inline constexpr auto Float64ExponentRealMinSimd256Pd = Vector256(
		static_cast<float64>(NumericLimits<float64>::ExponentRealMin)
	);
	inline constexpr auto Float64ExponentBiasSimd256Pd = Vector256(
		static_cast<float64>(NumericLimits<float64>::ExponentBias)
	);

	inline constexpr auto Float64ExponentMaskSimd256Pd = Vector256(NumericLimits<float64>::ExponentMask);
	inline constexpr auto Float64MantissaMaskSimd256Pd = Vector256(NumericLimits<float64>::MantissaMask);

	inline constexpr auto ESimd256Pd = Vector256(E<float64>);
	inline constexpr auto EulerSimd256Pd = Vector256(Euler<float64>);
	inline constexpr auto GaussSimd256Pd = Vector256(Gauss<float64>);
	inline constexpr auto PhiSimd256Pd = Vector256(Phi<float64>);
	inline constexpr auto PiSimd256Pd = Vector256(Pi<float64>);

	inline constexpr auto ZeroSimd256Pd = Vector256(0.0);
	inline constexpr auto OneSimd256Pd = Vector256(1.0);
	// inline constexpr auto TwoSimd256Pd = Vector256(2.0);
	// inline constexpr auto FourSimd256Pd = Vector256(4.0);
	// inline constexpr auto SixSimd256Pd = Vector256(6.0);

	inline constexpr auto NegativeZeroSimd256Pd = Vector256(0x8000000000000000_i64);
	inline constexpr auto NegativeOneSimd256Pd = Vector256(-1.0);

	inline constexpr auto OneDiv2Simd256Pd = Vector256(OneDiv2<float64>);
	inline constexpr auto OneDiv2PiSimd256Pd = Vector256(OneDiv2Pi<float64>);
	inline constexpr auto OneDiv3Simd256Pd = Vector256(OneDiv3<float64>);
	inline constexpr auto OneDiv4Simd256Pd = Vector256(OneDiv4<float64>);
	inline constexpr auto OneDiv6Simd256Pd = Vector256(OneDiv6<float64>);
	inline constexpr auto OneDivEulerSimd256Pd = Vector256(OneDivEuler<float64>);
	inline constexpr auto OneDivPiSimd256Pd = Vector256(OneDivPi<float64>);
	inline constexpr auto OneDivRoot2Simd256Pd = Vector256(OneDivRoot2<float64>);
	inline constexpr auto OneDivRoot3Simd256Pd = Vector256(OneDivRoot3<float64>);
	inline constexpr auto ThreeDiv4Simd256Pd = Vector256(ThreeDiv4<float64>);
	inline constexpr auto TwoDiv3Simd256Pd = Vector256(TwoDiv3<float64>);
	inline constexpr auto TwoDivPiSimd256Pd = Vector256(TwoDivPi<float64>);

	inline constexpr auto NegativeOneDiv2Simd256Pd = Vector256(-0.5);

	inline constexpr auto Root2Simd256Pd = Vector256(Root2<float64>);
	inline constexpr auto Root3Simd256Pd = Vector256(Root3<float64>);
	inline constexpr auto RootESimd256Pd = Vector256(RootE<float64>);
	inline constexpr auto RootPiSimd256Pd = Vector256(RootPi<float64>);
	inline constexpr auto RootPiMul2Simd256Pd = Vector256(RootPiMul2<float64>);

	inline constexpr auto Ln10Simd256Pd = Vector256(Ln10<float64>);
	inline constexpr auto Ln2Simd256Pd = Vector256(Ln2<float64>);
	inline constexpr auto LnLn2Simd256Pd = Vector256(LnLn2<float64>);
	inline constexpr auto LnPhiSimd256Pd = Vector256(LnPhi<float64>);
	inline constexpr auto Log10ESimd256Pd = Vector256(Log10E<float64>);
	inline constexpr auto Log2ESimd256Pd = Vector256(Log2E<float64>);
	// inline constexpr auto Log2EDiv2Simd256Pd = Vector256(-0.721348);
	// inline constexpr auto Log210Simd256Pd = Vector256(3.321928);

	inline constexpr auto EPowPiSimd256Pd = Vector256(EPowPi<float64>);
	inline constexpr auto EulerPow2Simd256Pd = Vector256(EulerPow2<float64>);

	inline constexpr auto DegreesToRadiansSimd256Pd = Vector256(DegreesToRadians<float64>);
	inline constexpr auto RadiansToDegreesSimd256Pd = Vector256(RadiansToDegrees<float64>);

	inline constexpr auto PiDiv2Simd256Pd = Vector256(PiDiv2<float64>);
	inline constexpr auto PiDiv3Simd256Pd = Vector256(PiDiv3<float64>);
	inline constexpr auto PiDiv4Simd256Pd = Vector256(PiDiv4<float64>);
	inline constexpr auto PiDiv6Simd256Pd = Vector256(PiDiv6<float64>);
	inline constexpr auto PiPow2Simd256Pd = Vector256(PiPow2<float64>);
	inline constexpr auto PiPowESimd256Pd = Vector256(PiPowE<float64>);
	inline constexpr auto PiMul2Simd256Pd = Vector256(PiMul2<float64>);
	inline constexpr auto PiMul3Div4Simd256Pd = Vector256(PiMul3Div4<float64>);

	// inline constexpr auto PiConstants0Simd256Pd = Vector256(__m256d {.m256d_f64 ={static_cast<float64>(Pi), static_cast<float64>(PiMul2), static_cast<float64>(OneDivPi), static_cast<float64>(OneDiv2Pi) } });

	inline constexpr auto Cos0Simd256Pd = Vector256(Cos0<float64>);
	inline constexpr auto Cos15Simd256Pd = Vector256(Cos15<float64>);
	inline constexpr auto Cos30Simd256Pd = Vector256(Cos30<float64>);
	inline constexpr auto Cos45Simd256Pd = Vector256(Cos45<float64>);
	inline constexpr auto Cos60Simd256Pd = Vector256(Cos60<float64>);
	inline constexpr auto Cos75Simd256Pd = Vector256(Cos75<float64>);
	inline constexpr auto Cos90Simd256Pd = Vector256(Cos90<float64>);

	// inline constexpr auto CosCoefficients0Simd256Pd = Vector256(__m256d {.m256d_f64 ={ -0.5, 0.041666638, -0.0013888378, 2.4760495e-05 } });
	// inline constexpr auto CosCoefficients1Simd256Pd = Vector256(__m256d {.m256d_f64 ={ -2.6051615e-07, -0.49992746, 0.041493919, -0.0012712436 } });

	inline constexpr auto Sin0Simd256Pd = Vector256(Sin0<float64>);
	inline constexpr auto Sin15Simd256Pd = Vector256(Sin15<float64>);
	inline constexpr auto Sin30Simd256Pd = Vector256(Sin30<float64>);
	inline constexpr auto Sin45Simd256Pd = Vector256(Sin45<float64>);
	inline constexpr auto Sin60Simd256Pd = Vector256(Sin60<float64>);
	inline constexpr auto Sin75Simd256Pd = Vector256(Sin75<float64>);
	inline constexpr auto Sin90Simd256Pd = Vector256(Sin90<float64>);

	// inline constexpr auto SinCoefficients0Simd256Pd = Vector256(__m256d {.m256d_f64 ={ -0.16666667, 0.0083333310, -0.00019840874, 2.7525562e-06 } });
	// inline constexpr auto SinCoefficients1Simd256Pd = Vector256(__m256d {.m256d_f64 ={ -2.3889859e-08, -0.16665852, 0.0083139502, -0.00018524670 } });

	inline constexpr auto Tan0Simd256Pd = Vector256(Tan0<float64>);
	inline constexpr auto Tan15Simd256Pd = Vector256(Tan15<float64>);
	inline constexpr auto Tan30Simd256Pd = Vector256(Tan30<float64>);
	inline constexpr auto Tan45Simd256Pd = Vector256(Tan45<float64>);
	inline constexpr auto Tan60Simd256Pd = Vector256(Tan60<float64>);
	inline constexpr auto Tan75Simd256Pd = Vector256(Tan75<float64>);

	// inline constexpr auto TanCoefficients0Simd256Pd = Vector256(__m256d {.m256d_f64 ={ 1.0, 0.333333333, 0.133333333, 5.396825397e-2 } });
	// inline constexpr auto TanCoefficients1Simd256Pd = Vector256(__m256d {.m256d_f64 ={ 2.186948854e-2, 8.863235530e-3, 3.592128167e-3, 1.455834485e-3 } });
	// inline constexpr auto TanCoefficients2Simd256Pd = Vector256(__m256d {.m256d_f64 ={ 5.900274264e-4, 2.391290764e-4, 9.691537707e-5, 3.927832950e-5 } });
	// inline constexpr auto TanEstimatedCoefficientsSimd256Pd = Vector256(__m256d {.m256d_f64 ={ 2.484, -1.954923183e-1, 2.467401101, static_cast<float64>(OneDivPi) } });

	// inline constexpr auto ArcCoefficients0Simd256Pd = Vector256(__m256d {.m256d_f64 ={ 1.5707963050, -0.2145988016, 0.0889789874, -0.0501743046 } });
	// inline constexpr auto ArcCoefficients1Simd256Pd = Vector256(__m256d {.m256d_f64 ={ 0.0308918810, -0.0170881256, 0.0066700901, -0.0012624911 } });

	// inline constexpr auto ArcEstimatedCoefficientsSimd256Pd = Vector256(__m256d {.m256d_f64 ={ 1.5707288, -0.2121144, 0.0742610, -0.0187293 } });

	// inline constexpr auto ATanCoefficients0Simd256Pd = Vector256(__m256d {.m256d_f64 ={ -0.3333314528, 0.1999355085, -0.1420889944, 0.1065626393 } });
	// inline constexpr auto ATanCoefficients1Simd256Pd = Vector256(__m256d {.m256d_f64 ={ -0.0752896400, 0.0429096138, -0.0161657367, 0.0028662257 } });

	// inline constexpr auto ATanEstimatedCoefficients0Simd256Pd = Vector256(__m256d {.m256d_f64 ={ 0.999866, 0.999866, 0.999866, 0.999866 } });
	// inline constexpr auto ATanEstimatedCoefficients1Simd256Pd = Vector256(__m256d {.m256d_f64 ={ -0.3302995, 0.180141, -0.085133, 0.0208351 } });

	inline constexpr auto TaylorCoefficient0Simd256Pd = Vector256(TaylorCoefficient0<float64>);
	inline constexpr auto TaylorCoefficient1Simd256Pd = Vector256(TaylorCoefficient1<float64>);
	inline constexpr auto TaylorCoefficient2Simd256Pd = Vector256(TaylorCoefficient2<float64>);
	inline constexpr auto TaylorCoefficient3Simd256Pd = Vector256(TaylorCoefficient3<float64>);
	inline constexpr auto TaylorCoefficient4Simd256Pd = Vector256(TaylorCoefficient4<float64>);
	inline constexpr auto TaylorCoefficient5Simd256Pd = Vector256(TaylorCoefficient5<float64>);
	inline constexpr auto TaylorCoefficient6Simd256Pd = Vector256(TaylorCoefficient6<float64>);
	inline constexpr auto TaylorCoefficient7Simd256Pd = Vector256(TaylorCoefficient7<float64>);
	inline constexpr auto TaylorCoefficient8Simd256Pd = Vector256(TaylorCoefficient8<float64>);
	inline constexpr auto TaylorCoefficient9Simd256Pd = Vector256(TaylorCoefficient9<float64>);
	inline constexpr auto TaylorCoefficient10Simd256Pd = Vector256(TaylorCoefficient10<float64>);
	inline constexpr auto TaylorCoefficient11Simd256Pd = Vector256(TaylorCoefficient11<float64>);

	inline constexpr auto IdentityMatrixRow0Simd256Pd = Vector256(__m256d {.m256d_f64 = {1.0, 0.0, 0.0, 0.0}});
	inline constexpr auto IdentityMatrixRow1Simd256Pd = Vector256(__m256d {.m256d_f64 = {0.0, 1.0, 0.0, 0.0}});
	inline constexpr auto IdentityMatrixRow2Simd256Pd = Vector256(__m256d {.m256d_f64 = {0.0, 0.0, 1.0, 0.0}});
	inline constexpr auto IdentityMatrixRow3Simd256Pd = Vector256(__m256d {.m256d_f64 = {0.0, 0.0, 0.0, 1.0}});
	inline constexpr auto NegativeIdentityMatrixRow0Simd256Pd = Vector256(__m256d {.m256d_f64 = {-1.0, 0.0, 0.0, 0.0}});
	inline constexpr auto NegativeIdentityMatrixRow1Simd256Pd = Vector256(__m256d {.m256d_f64 = {0.0, -1.0, 0.0, 0.0}});
	inline constexpr auto NegativeIdentityMatrixRow2Simd256Pd = Vector256(__m256d {.m256d_f64 = {0.0, 0.0, -1.0, 0.0}});
	inline constexpr auto NegativeIdentityMatrixRow3Simd256Pd = Vector256(__m256d {.m256d_f64 = {0.0, 0.0, 0.0, -1.0}});

	inline constexpr auto NegateXSimd256Pd = Vector256(__m256d {.m256d_f64 = {-1.0, 1.0, 1.0, 1.0}});
	inline constexpr auto NegateYSimd256Pd = Vector256(__m256d {.m256d_f64 = {1.0, -1.0, 1.0, 1.0}});
	inline constexpr auto NegateZSimd256Pd = Vector256(__m256d {.m256d_f64 = {1.0, 1.0, -1.0, 1.0}});
	inline constexpr auto NegateWSimd256Pd = Vector256(__m256d {.m256d_f64 = {1.0, 1.0, 1.0, -1.0}});


	// todo: sort out constants
	// inline constexpr auto ExpEstimated1Simd128Ps = Vector128(-6.93147182e-1f);
	// inline constexpr auto ExpEstimated2Simd128Ps = Vector128(2.40226462e-1f);
	// inline constexpr auto ExpEstimated3Simd128Ps = Vector128(-5.55036440e-2f);
	// inline constexpr auto ExpEstimated4Simd128Ps = Vector128(9.61597636e-3f);
	// inline constexpr auto ExpEstimated5Simd128Ps = Vector128(-1.32823968e-3f);
	// inline constexpr auto ExpEstimated6Simd128Ps = Vector128(1.47491097e-4f);
	// inline constexpr auto ExpEstimated7Simd128Ps = Vector128(-1.08635004e-5f);

	// inline constexpr auto Log2Simd128Ps = Vector128(0.479384f);
	// inline constexpr auto Log3Simd128Ps = Vector128(-0.350295f);
	// inline constexpr auto Log4Simd128Ps = Vector128(0.248590f);
	// inline constexpr auto Log5Simd128Ps = Vector128(-0.145700f);
	// inline constexpr auto Log6Simd128Ps = Vector128(0.057148f);
	// inline constexpr auto Log7Simd128Ps = Vector128(-0.010578f);
	// inline constexpr auto InvLgESimd128Ps = Vector128(6.93147182e-1f);
	// inline constexpr auto InvLg10Simd128Ps = Vector128(3.010299956e-1f);

	// inline constexpr auto YteMaxSimd128Ps = Vector128(255.0f);
	// inline constexpr auto TeMinSimd128Ps = Vector128(-127.0f);
	// inline constexpr auto TeMaxSimd128Ps = Vector128(127.0f);
	// inline constexpr auto OrtMinSimd128Ps = Vector128(-32767.0f);
	// inline constexpr auto OrtMaxSimd128Ps = Vector128(32767.0f);
	// inline constexpr auto HortMaxSimd128Ps = Vector128(65535.0f);
	//
	// inline constexpr auto NoFractionSimd128Ps = Vector128(8388608.0f);
	// inline constexpr auto FixupY16Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.0f, 1.0f / 65536.0f, 0.0f, 0.0f } });
	// inline constexpr auto FixupY16W16Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.0f, 1.0f, 1.0f / 65536.0f, 1.0f / 65536.0f } });
	// inline constexpr auto AddUDec4Simd128Ps = Vector128(__m128 {.m128_f32 ={ 0, 0, 0, 32768.0f * 65536.0f } });
	// inline constexpr auto AddDec4Simd128Ps = Vector128(__m128 {.m128_f32 ={ -512.0f, -512.0f * 1024.0f, -512.0f * 1024.0f * 1024.0f, 0 } });
	// inline constexpr auto MulDec4Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.0f, 1.0f / 1024.0f, 1.0f / (1024.0f * 1024.0f), 1.0f / (1024.0f * 1024.0f * 1024.0f) } });
	// inline constexpr auto AddByte4Simd128Ps = Vector128(__m128 {.m128_f32 ={ -128.0f, -128.0f * 256.0f, -128.0f * 65536.0f, 0 } });
	// inline constexpr auto FixUnsignedSimd128Ps = Vector128(32768.0f * 65536.0f);
	// inline constexpr auto MaxIntSimd128Ps = Vector128(65536.0f * 32768.0f - 128.0f);
	// inline constexpr auto MaxUIntSimd128Ps = Vector128(65536.0f * 65536.0f - 256.0f);
	// inline constexpr auto UnsignedFixSimd128Ps = Vector128(32768.0f * 65536.0f);
	//
	// inline constexpr auto AbsMaskSimd128Epi32 = Vector128(0x7FFFFFFF);
	// inline constexpr auto FlipX16Y16Simd128Epi32 = Vector128(__m128i {.m128i_i32 = { 0x00008000, 0x00000000, 0x00000000, 0x00000000 } });
	// inline constexpr auto FlipX16Y16Z16W16Simd128Epi32 = Vector128(__m128i {.m128i_i32 = { 0x00008000, 0x00008000, 0x00000000, 0x00000000 } });
	// inline constexpr auto MaskByteSimd128Epi32 = Vector128(0x000000FF);
	// inline constexpr auto MaskByte4Simd128Epu32 = Vector128(__m128i {.m128i_u32 = {0xff, 0xff00, 0xff0000, 0xff000000}});
	// inline constexpr auto MaskDec4Simd128Epi32 = Vector128(__m128i {.m128i_i32 = { 0x3FF, 0x3FF << 10, 0x3FF << 20, static_cast<int32>(0xC0000000) } });
	// inline constexpr auto MaskX16Y16Simd128Epu32 = Vector128(__m128i {.m128i_u32 = {0x0000ffff, 0xffff0000, 0x00000000, 0x00000000}});
	// inline constexpr auto MaskX16Y16Z16W16Simd128Epu32 = Vector128(__m128i {.m128i_u32 = {0x0000ffff, 0x0000ffff, 0xffff0000, 0xffff0000}});
	// inline constexpr auto XorByte4Simd128Epi32 = Vector128(__m128i {.m128i_i32 = { 0x80, 0x8000, 0x800000, 0x00000000 } });
	// inline constexpr auto XorDec4Simd128Epi32 = Vector128(__m128i {.m128i_i32 = { 0x200, 0x200 << 10, 0x200 << 20, 0 } });
	//
	// inline constexpr auto FixX16Y16Simd128Ps = Vector128(__m128 {.m128_f32 ={ -32768.0f, 0.0f, 0.0f, 0.0f } });
	// inline constexpr auto FixX16Y16Z16W16Simd128Ps = Vector128(__m128 {.m128_f32 ={ -32768.0f, -32768.0f, 0.0f, 0.0f } });
	// inline constexpr auto NormalizeX16Y16Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.0f / 32767.0f, 1.0f / (32767.0f * 65536.0f), 0.0f, 0.0f } });
	// inline constexpr auto NormalizeX16Y16Z16W16Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.0f / 32767.0f, 1.0f / 32767.0f, 1.0f / (32767.0f * 65536.0f), 1.0f / (32767.0f * 65536.0f) } });
	//
	// inline constexpr auto FixAA2B10G10R10Simd128Ps = Vector128(__m128 {.m128_f32 ={ -512.0f, -512.0f * static_cast<float32>(0x400), -512.0f * static_cast<float32>(0x100000), static_cast<float32>(0x80000000u) } });
	// inline constexpr auto FixAA8R8G8B8Simd128Ps = Vector128(__m128 {.m128_f32 ={ 0.0f, 0.0f, 0.0f, static_cast<float32>(0x80000000u) } });
	// inline constexpr auto FlipA2B10G10R10Simd128Epu32 = Vector128(__m128i {.m128i_u32 = { 0x00000200, 0x00080000, 0x20000000, 0x80000000 } });
	// inline constexpr auto FlipA8R8G8B8Simd128Epu32 = Vector128(__m128i {.m128i_u32 = { 0x00000000, 0x00000000, 0x00000000, 0x80000000 } });
	// inline constexpr auto MaskA2B10G10R10Simd128Epu32 = Vector128(__m128i {.m128i_u32 = { 0x000003ff, 0x000ffc00, 0x3ff00000, 0xc0000000 } });
	// inline constexpr auto MaskA8R8G8B8Simd128Epu32 = Vector128(__m128i {.m128i_u32 = { 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000 } });
	// inline constexpr auto NormalizeA2B10G10R10Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.0f / 511.0f, 1.0f / (511.0f * static_cast<float32>(0x400)), 1.0f / (511.0f * static_cast<float32>(0x100000)), 1.0f / (3.0f * static_cast<float32>(0x40000000)) } });
	// inline constexpr auto NormalizeA8R8G8B8Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.0f / (255.0f * static_cast<float32>(0x10000)), 1.0f / (255.0f * static_cast<float32>(0x100)), 1.0f / 255.0f, 1.0f / (255.0f * static_cast<float32>(0x1000000)) } });
	// inline constexpr auto SrgbScaleSimd128Ps = Vector128(__m128 {.m128_f32 ={ 12.92f, 12.92f, 12.92f, 1.0f } });
	// inline constexpr auto SrgbaSimd128Ps = Vector128(__m128 {.m128_f32 ={ 0.055f, 0.055f, 0.055f, 0.0f } });
	// inline constexpr auto Srgba1Simd128Ps = Vector128(__m128 {.m128_f32 ={ 1.055f, 1.055f, 1.055f, 1.0f } });
	//
	// inline constexpr auto BinNeg150Simd128Epu32 = Vector128(0xc3160000);
	// inline constexpr auto Bin128Simd128Epi32 = Vector128(0x43000000);
	// inline constexpr auto C253Simd128Epi32 = Vector128(253);
}
