#pragma once
#include "standard/math/utility/vectorize.hpp"

#include "standard/platform/intrinsic/x86/avx.hpp"
#include "standard/platform/intrinsic/x86/avx2.hpp"
#include "standard/platform/intrinsic/x86/sse.hpp"
#include "standard/platform/intrinsic/x86/sse2.hpp"
#include "standard/platform/intrinsic/x86/sse41.hpp"
#include "standard/platform/intrinsic/x86/sse42.hpp"

// todo: __vectorcall__
// todo: select constexpr Simd if consteval
// todo: AllOf, AnyOf, etc.

namespace Std::inline Math::inline Simd
{
	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr bool BoolComparison(
		const TSimd vector
	)
	{
		using FunctionType = bool (*)(
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a
		) -> bool
		{
			constexpr auto mask = static_cast<CorrespondingUint<TValue>>(-1);
			return static_cast<CorrespondingUint<TValue>>(a) == mask;
		};

		return VectorizeBool(function, vector);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsAnyValue<TValue, int32, uint32, float32>
	static inline bool BoolComparison(
		const Vector128 vector
	)
	{
		constexpr auto mask = (1_ui32 << (sizeof(Vector128) / sizeof(TValue))) - 1;
		return Sse::MoveMaskPs(vector) == mask;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsAnyValue<TValue, int64, uint64, float64>
	static inline bool BoolComparison(
		const Vector128 vector
	)
	{
		constexpr auto mask = (1_ui32 << (sizeof(Vector128) / sizeof(TValue))) - 1;
		return Sse2::MoveMaskPd(vector) == mask;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsAnyValue<TValue, int32, uint32, float32>
	static inline bool BoolComparison(
		const Vector256 vector
	)
	{
		constexpr auto mask = (1_ui32 << (sizeof(Vector256) / sizeof(TValue))) - 1;
		return Avx::MoveMaskPs(vector) == mask;
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsAnyValue<TValue, int64, uint64, float64>
	static inline bool BoolComparison(
		const Vector256 vector
	)
	{
		constexpr auto mask = (1_ui32 << (sizeof(Vector256) / sizeof(TValue))) - 1;
		return Avx::MoveMaskPd(vector) == mask;
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Equal(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			constexpr auto mask = static_cast<CorrespondingUint<TValue>>(-1);
			return (a == b) * mask;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int8>
	static inline Vector128 Equal(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareEqualEpi8(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int16>
	static inline Vector128 Equal(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareEqualEpi16(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int32>
	static inline Vector128 Equal(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareEqualEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int64>
	static inline Vector128 Equal(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse41::CompareEqualEpi64(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Equal(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::CompareEqualPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Equal(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareEqualPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int8>
	static inline Vector256 Equal(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareEqualEpi8(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int16>
	static inline Vector256 Equal(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareEqualEpi16(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int32>
	static inline Vector256 Equal(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareEqualEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int64>
	static inline Vector256 Equal(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareEqualEpi64(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float32>
	static inline Vector256 Equal(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePs<EComparisonType::EqualOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Equal(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePd<EComparisonType::EqualOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSimdVectorValue<TSimd>
	static constexpr TSimd EqualAsUint(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			constexpr auto mask = static_cast<CorrespondingUint<TValue>>(-1);
			return (static_cast<uintSize>(a) == static_cast<uintSize>(b)) * mask;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsIntValue<TValue>
	static inline Vector128 EqualAsUint(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Equal<Vector128, TValue>(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 EqualAsUint(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareEqualEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 EqualAsUint(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse41::CompareEqualEpi64(left, right);
	}


	template<class TSimd, class TValue>
	static inline Vector256 EqualAsUint(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Equal<Vector256, TValue>(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float32>
	static inline Vector256 EqualAsUint(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareEqualEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 EqualAsUint(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareEqualEpi64(left, right);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd EqualNear(
		const TSimd left,
		const TSimd right,
		const TValue epsilon
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b,
			const TValue c
		) -> TValue
		{
			constexpr auto mask = static_cast<CorrespondingUint<TValue>>(-1);
			return (std::abs(a - b) <= c) * mask;
		};

		return Vectorize(function, left, right, TSimd(epsilon));
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 EqualNear(
		const Vector128 left,
		const Vector128 right,
		const float32 epsilon
	)
	{
		return Sse::CompareLessEqualPs(
			Abs<Vector128, float32>(Subtract<Vector128, float32>(left, right)),
			Sse::Set1Ps(epsilon)
		);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 EqualNear(
		const Vector128 left,
		const Vector128 right,
		const float64 epsilon
	)
	{
		return Sse2::CompareLessEqualPd(
			Abs<Vector128, float64>(Subtract<Vector128, float64>(left, right)),
			Sse2::Set1Pd(epsilon)
		);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 EqualNear(
		const Vector256 left,
		const Vector256 right,
		const float64 epsilon
	)
	{
		return Avx::ComparePd<EComparisonType::LessEqualOrderedNonSignaling>(
			Abs<Vector256, float64>(Subtract<Vector256, float64>(left, right)),
			Avx::Set1Pd(epsilon)
		);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Greater(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			constexpr auto mask = static_cast<CorrespondingUint<TValue>>(-1);
			return (a > b) * mask;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int8>
	static inline Vector128 Greater(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareGreaterEpi8(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int16>
	static inline Vector128 Greater(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareGreaterEpi16(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int32>
	static inline Vector128 Greater(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareGreaterEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, int64>
	static inline Vector128 Greater(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse42::CompareGreaterEpi64(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Greater(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::CompareGreaterPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Greater(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareGreaterPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int8>
	static inline Vector256 Greater(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareGreaterEpi8(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int16>
	static inline Vector256 Greater(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareGreaterEpi16(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int32>
	static inline Vector256 Greater(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareGreaterEpi32(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, int64>
	static inline Vector256 Greater(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx2::CompareGreaterEpi64(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float32>
	static inline Vector256 Greater(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePs<EComparisonType::GreaterOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Greater(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePd<EComparisonType::GreaterOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd GreaterEqual(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			constexpr auto mask = static_cast<CorrespondingUint<TValue>>(-1);
			return (a >= b) * mask;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 GreaterEqual(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::CompareGreaterEqualPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 GreaterEqual(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareGreaterEqualPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float32>
	static inline Vector256 GreaterEqual(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePs<EComparisonType::GreaterEqualOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 GreaterEqual(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePd<EComparisonType::GreaterEqualOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd Less(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			constexpr auto mask = static_cast<CorrespondingUint<TValue>>(-1);
			return (a < b) * mask;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 Less(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::CompareLessPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 Less(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareLessPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float32>
	static inline Vector256 Less(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePs<EComparisonType::LessOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 Less(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePd<EComparisonType::LessOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
	static constexpr TSimd LessEqual(
		const TSimd left,
		const TSimd right
	)
	{
		using FunctionType = TValue (*)(
			TValue,
			TValue
		);

		constexpr FunctionType function = [](
			const TValue a,
			const TValue b
		) -> TValue
		{
			constexpr auto mask = static_cast<CorrespondingUint<TValue>>(-1);
			return (a <= b) * mask;
		};

		return Vectorize(function, left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float32>
	static inline Vector128 LessEqual(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse::CompareLessEqualPs(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector128> && IsSameValue<TValue, float64>
	static inline Vector128 LessEqual(
		const Vector128 left,
		const Vector128 right
	)
	{
		return Sse2::CompareLessEqualPd(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float32>
	static inline Vector256 LessEqual(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePs<EComparisonType::LessEqualOrderedNonSignaling>(left, right);
	}


	template<class TSimd, class TValue>
		requires IsSameValue<TSimd, Vector256> && IsSameValue<TValue, float64>
	static inline Vector256 LessEqual(
		const Vector256 left,
		const Vector256 right
	)
	{
		return Avx::ComparePd<EComparisonType::LessEqualOrderedNonSignaling>(left, right);
	}
}
