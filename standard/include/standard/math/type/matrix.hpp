﻿#pragma once
#include "vector.hpp"


namespace Std::inline Math::inline Type
{
	template<class TNumeric, uint16 VRows, uint16 VColumns>
		requires (IsNumericValue<TNumeric>)
	struct MatrixT
	{
	public:
		using RowType = VectorT<TNumeric, VColumns>;
		using ValueType = TNumeric;
		using ArrayType = TNumeric[VRows * sizeof(RowType) / sizeof(TNumeric)];

	public:
		static inline constexpr auto RowsCount = VRows;
		static inline constexpr auto ColumnsCount = VColumns;
		static inline constexpr auto ElementsCount = RowsCount * ColumnsCount;

	public:
		union
		{
			ArrayType Array;
			RowType Rows[VRows] = {};
		};

	public:
		constexpr explicit MatrixT(
			const TNumeric value = 0
		) noexcept
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i] = RowType(value);
			}
		}

		constexpr explicit MatrixT(
			const VectorT<TNumeric, ColumnsCount> vector
		) noexcept
		{
			for (auto i = 0; i < ColumnsCount; ++i)
			{
				Rows[i] = vector;
			}
		}

		constexpr explicit MatrixT(
			const TNumeric array[ElementsCount]
		) noexcept
		{
			for (auto i = 0; i < ElementsCount; ++i)
			{
				Array[i] = array[i];
			}
		}

		constexpr explicit MatrixT(
			const TNumeric array[RowsCount][ColumnsCount]
		) noexcept
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i] = RowType(array[i]);
			}
		}

		constexpr explicit MatrixT(
			const VectorT<TNumeric, ColumnsCount> (&array)[RowsCount]
		) noexcept
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i] = array[i];
			}
		}

		constexpr MatrixT(
			const MatrixT& other
		) = default;

		constexpr MatrixT(
			MatrixT&& other
		) noexcept = default;

	public:
		constexpr ~MatrixT() noexcept = default;

	public:
		template<class = void>
			requires (RowsCount == ColumnsCount)
		static constexpr MatrixT GetIdentity()
		{
			auto result = MatrixT();
			for (auto i = 0; i < ElementsCount; i += ColumnsCount + 1)
			{
				result.Array[i] = static_cast<TNumeric>(1);
			}
			return result;
		}

	public:
		template<class TOther>
			requires (
				requires
				{
					TOther::RowsCount;
					TOther::ColumnsCount;
				} &&
				TOther::RowsCount == RowsCount &&
				TOther::ColumnsCount == ColumnsCount &&
				IsCastableValue<typename TOther::ValueType, ValueType>
			)
		constexpr MatrixT& operator=(
			const TOther& other
		)
		{
			for (auto i = 0; i < ElementsCount; ++i)
			{
				Array[i] = static_cast<TNumeric>(other.Array[i]);
			}
			return *this;
		}

		MatrixT& operator=(
			const MatrixT& other
		) = default;

		MatrixT& operator=(
			MatrixT&& other
		) noexcept = default;

	public:
		constexpr RowType& operator[](
			const uintSize index
		)
		{
			return Rows[index];
		}

		constexpr const RowType& operator[](
			const uintSize index
		) const
		{
			return Rows[index];
		}


		constexpr MatrixT& operator++()
		{
			*this += MatrixT(1);
			return *this;
		}

		constexpr MatrixT& operator--()
		{
			*this -= MatrixT(1);
			return *this;
		}

		constexpr MatrixT operator++(
			int
		)
		{
			auto result = MatrixT(*this);
			++*this;
			return result;
		}

		constexpr MatrixT operator--(
			int
		)
		{
			auto result = MatrixT(*this);
			--*this;
			return result;
		}


		template<class TValue = TNumeric>
			requires (IsCastableValue<TValue, TNumeric>)
		constexpr MatrixT& operator+=(
			TValue value
		)
		{
			*this += MatrixT(static_cast<TNumeric>(value));
			return *this;
		}

		template<class TValue = TNumeric>
			requires (IsCastableValue<TValue, TNumeric>)
		constexpr MatrixT& operator-=(
			TValue value
		)
		{
			*this -= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}

		template<class TValue = TNumeric>
			requires (IsCastableValue<TValue, TNumeric>)
		constexpr MatrixT& operator*=(
			TValue value
		)
		{
			*this *= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}

		template<class TValue = TNumeric>
			requires (IsCastableValue<TValue, TNumeric>)
		constexpr MatrixT& operator/=(
			TValue value
		)
		{
			*this /= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}

		template<class TValue = TNumeric>
			requires (IsIntValue<TNumeric> && IsIntValue<TValue>)
		constexpr MatrixT& operator%=(
			TValue value
		)
		{
			*this %= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}


		constexpr MatrixT& operator+=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = Add<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}

		constexpr MatrixT& operator-=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = Subtract<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}

		constexpr MatrixT& operator*=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = Multiply<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}

		constexpr MatrixT& operator/=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = Divide<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}

		template<class = void>
			requires (IsIntValue<TNumeric>)
		constexpr MatrixT& operator%=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = Mod<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}


		template<class TValue>
			requires (IsIntValue<TNumeric> && IsIntValue<TValue>)
		constexpr MatrixT& operator&=(
			TValue value
		)
		{
			*this &= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}

		template<class TValue>
			requires (IsIntValue<TNumeric> && IsIntValue<TValue>)
		constexpr MatrixT& operator|=(
			TValue value
		)
		{
			*this |= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}

		template<class TValue>
			requires (IsIntValue<TNumeric> && IsIntValue<TValue>)
		constexpr MatrixT& operator^=(
			TValue value
		)
		{
			*this ^= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}

		template<class TValue>
			requires (IsIntValue<TNumeric> && IsIntValue<TValue>)
		constexpr MatrixT& operator<<=(
			TValue value
		)
		{
			*this <<= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}

		template<class TValue>
			requires (IsIntValue<TNumeric> && IsIntValue<TValue>)
		constexpr MatrixT& operator>>=(
			TValue value
		)
		{
			*this >>= MatrixT(static_cast<TNumeric>(value));
			return *this;
		}


		template<class = void>
			requires (IsIntValue<TNumeric>)
		constexpr MatrixT& operator&=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = BitAnd<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}

		template<class = void>
			requires (IsIntValue<TNumeric>)
		constexpr MatrixT& operator|=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = BitOr<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}

		template<class = void>
			requires (IsIntValue<TNumeric>)
		constexpr MatrixT& operator^=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = BitXor<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}

		template<class = void>
			requires (IsIntValue<TNumeric>)
		constexpr MatrixT& operator<<=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = ShiftLeft<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}

		template<class = void>
			requires (IsIntValue<TNumeric>)
		constexpr MatrixT& operator>>=(
			const MatrixT other
		)
		{
			for (auto i = 0; i < RowsCount; ++i)
			{
				Rows[i].Simd = ShiftRight<typename RowType::SimdType, TNumeric>(Rows[i].Simd, other[i].Simd);
			}
			return *this;
		}
	};


	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr MatrixT<TNumeric, VRows, VColumns> operator+(
		const MatrixT<TNumeric, VRows, VColumns>& matrix
	)
	{
		return matrix;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr MatrixT<TNumeric, VRows, VColumns> operator-(
		const MatrixT<TNumeric, VRows, VColumns>& matrix
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>() -= matrix;
	}


	template<class TNumeric, uint16 VRows, uint16 VColumns>
		requires (IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator~(
		const MatrixT<TNumeric, VRows, VColumns>& matrix
	)
	{
		auto result = MatrixT<TNumeric, VRows, VColumns>();
		for (auto i = 0; i < VRows; ++i)
		{
			result[i] = ~matrix[i];
		}
		return result;
	}


	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires (IsCastableValue<TValue, TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator+(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) += static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires (IsCastableValue<TValue, TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator-(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) -= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires (IsCastableValue<TValue, TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator*(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) *= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires (IsCastableValue<TValue, TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator/(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) /= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires (IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator%(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) %= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr MatrixT<TNumeric, VRows, VColumns> operator+(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) += right;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr MatrixT<TNumeric, VRows, VColumns> operator-(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) -= right;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr MatrixT<TNumeric, VRows, VColumns> operator*(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) *= right;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr MatrixT<TNumeric, VRows, VColumns> operator/(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) /= right;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
		requires (IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator%(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) %= right;
	}


	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator&(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) &= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator|(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) |= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator^(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) ^= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator<<(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) <<= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns, class TValue = TNumeric>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator>>(
		const MatrixT<TNumeric, VRows, VColumns>& matrix,
		TValue value
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(matrix) >>= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator&(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) &= right;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator|(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) |= right;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator^(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) ^= right;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator<<(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) <<= right;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
		requires(IsIntValue<TNumeric>)
	constexpr MatrixT<TNumeric, VRows, VColumns> operator>>(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return MatrixT<TNumeric, VRows, VColumns>(left) >>= right;
	}


	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr bool operator&&(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		auto result = 0;
		for (auto i = 0; i < VRows; ++i)
		{
			result += left[i] && right[i];
		}
		return result == VRows;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr bool operator||(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		auto result = 0;
		for (auto i = 0; i < VRows; ++i)
		{
			result += left[i] || right[i];
		}
		return result == VRows;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr bool operator==(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		auto result = 0;
		for (auto i = 0; i < VRows; ++i)
		{
			result += left[i] == right[i];
		}
		return result == VRows;
	}

	template<class TNumeric, uint16 VRows, uint16 VColumns>
	constexpr bool operator!=(
		const MatrixT<TNumeric, VRows, VColumns>& left,
		const MatrixT<TNumeric, VRows, VColumns>& right
	)
	{
		return !(left == right);
	}


	template<class TNumeric>
	using Matrix2x2T = MatrixT<TNumeric, 2, 2>;

	using Matrix2x2 = MatrixT<float32, 2, 2>;

	using Matrix2x2Int = MatrixT<int32, 2, 2>;


	template<class TNumeric>
	using Matrix3x3T = MatrixT<TNumeric, 3, 3>;

	using Matrix3x3 = MatrixT<float32, 3, 3>;

	using Matrix3x3Int = MatrixT<int32, 3, 3>;


	template<class TNumeric>
	using Matrix4x4T = MatrixT<TNumeric, 4, 4>;

	using Matrix4x4 = MatrixT<float32, 4, 4>;

	using Matrix4x4Int = MatrixT<int32, 4, 4>;
}
