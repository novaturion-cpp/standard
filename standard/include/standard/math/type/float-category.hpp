﻿#pragma once

namespace Std::inline Math::inline Type
{
	enum class EFloatCategory
	{
		Infinite = 1,
		Nan = 2,
		Normal = -1,
		Subnormal = -2,
		Zero = 0,
	};
}
