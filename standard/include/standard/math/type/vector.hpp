﻿#pragma once
#include "standard/math/simd/bitwise.hpp"
#include "standard/math/simd/common.hpp"
#include "standard/math/simd/comparison.hpp"


namespace Std::inline Math::inline Type
{
	template<class TNumeric, uint16 VSize>
		requires (IsNumericValue<TNumeric> && sizeof(TNumeric) * VSize <= sizeof(Vector256))
	struct VectorT
	{
	public:
		static constexpr auto Size = VSize;

	public:
		using ValueType = TNumeric;
		using ArrayType = TNumeric[Size];
		using SimdType = If<sizeof(TNumeric) * Size <= sizeof(Vector128), Vector128, Vector256>;

	public:
		union alignas(SimdType)
		{
			ArrayType Array;
			SimdType Simd = {};
		};

	public:
		constexpr VectorT() noexcept = default;

		constexpr explicit VectorT(
			const TNumeric value
		) noexcept
		{
			for (auto i = 0; i < Size; ++i)
			{
				Array[i] = value;
			}
		}

		constexpr explicit VectorT(
			const SimdType vector
		) noexcept:
			Simd(vector) {}

		constexpr explicit VectorT(
			const TNumeric array[Size]
		) noexcept
		{
			for (auto i = 0; i < Size; ++i)
			{
				Array[i] = array[i];
			}
		}

		constexpr VectorT(
			const VectorT& other
		) = default;

		constexpr VectorT(
			VectorT&& other
		) noexcept = default;

	public:
		constexpr ~VectorT() noexcept = default;

	public:
		constexpr TNumeric GetElement(
			uintSize index
		)
		{
			return (*this)[index];
		}

	public:
		constexpr VectorT& operator=(
			const VectorT& other
		) = default;

		constexpr VectorT& operator=(
			VectorT&& other
		) noexcept = default;

	public:
		#include "_internal/math/vector/member-operators.inl"
	};


	template<class TNumeric>
	struct VectorT<TNumeric, 2>
	{
	public:
		static constexpr auto Size = 2;

	public:
		using ValueType = TNumeric;
		using ArrayType = TNumeric[Size];
		using SimdType = If<sizeof(TNumeric) * Size <= sizeof(Vector128), Vector128, Vector256>;

	public:
		union alignas(SimdType)
		{
			struct
			{
				TNumeric X, Y;
			};


			// struct
			// {
			// 	TNumeric R, G;
			// };
			//
			//
			// struct
			// {
			// 	TNumeric S, T;
			// };
			//
			//
			// struct
			// {
			// 	TNumeric U, V;
			// };


			ArrayType Array;
			SimdType Simd = {};
		};

	public:
		constexpr VectorT() noexcept = default;

		constexpr explicit VectorT(
			const TNumeric value
		) noexcept
		{
			X = value;
			Y = value;
		}

		constexpr explicit VectorT(
			const TNumeric x,
			const TNumeric y
		) noexcept
		{
			X = x;
			Y = y;
		}

		constexpr explicit VectorT(
			const SimdType vector
		) noexcept:
			Simd(vector) {}

		constexpr explicit VectorT(
			const TNumeric array[Size]
		) noexcept
		{
			for (auto i = 0; i < Size; ++i)
			{
				Array[i] = array[i];
			}
		}

		constexpr VectorT(
			const VectorT& other
		) = default;

		constexpr VectorT(
			VectorT&& other
		) noexcept = default;

	public:
		constexpr ~VectorT() noexcept = default;

	public:
		static constexpr VectorT GetOne()
		{
			return VectorT(1, 1);
		}

		static constexpr VectorT GetZero()
		{
			return VectorT(0, 0);
		}

		static constexpr VectorT GetDown()
		{
			return VectorT(0, -1);
		}

		static constexpr VectorT GetLeft()
		{
			return VectorT(-1, 0);
		}

		static constexpr VectorT GetRight()
		{
			return VectorT(1, 0);
		}

		static constexpr VectorT GetUp()
		{
			return VectorT(0, 1);
		}

	public:
		constexpr TNumeric GetElement(
			uintSize index
		)
		{
			return (*this)[index];
		}

	public:
		constexpr VectorT& operator=(
			const VectorT& other
		) = default;

		constexpr VectorT& operator=(
			VectorT&& other
		) noexcept = default;

	public:
		#include "_internal/math/vector/member-operators.inl"
	};


	template<class TNumeric>
	struct VectorT<TNumeric, 3>
	{
	public:
		static constexpr auto Size = 3;

	public:
		using ValueType = TNumeric;
		using ArrayType = TNumeric[Size];
		using SimdType = If<sizeof(TNumeric) * Size <= sizeof(Vector128), Vector128, Vector256>;

	public:
		union alignas(SimdType)
		{
			struct
			{
				TNumeric X, Y, Z;
			};


			// struct
			// {
			// 	TNumeric R, G, B;
			// };
			//
			//
			// struct
			// {
			// 	TNumeric S, T, P;
			// };
			//
			//
			// struct
			// {
			// 	TNumeric U, V, W;
			// };


			ArrayType Array;
			SimdType Simd = {};
		};

	public:
		constexpr VectorT() noexcept = default;

		constexpr explicit VectorT(
			const TNumeric value
		) noexcept
		{
			X = value;
			Y = value;
			Z = value;
		}

		constexpr VectorT(
			const TNumeric x,
			const TNumeric y,
			const TNumeric z
		) noexcept
		{
			X = x;
			Y = y;
			Z = z;
		}

		constexpr explicit VectorT(
			const SimdType vector
		) noexcept:
			Simd(vector) {}

		constexpr explicit VectorT(
			const TNumeric array[Size]
		) noexcept
		{
			for (auto i = 0; i < Size; ++i)
			{
				Array[i] = array[i];
			}
		}

		constexpr VectorT(
			const VectorT& other
		) = default;

		constexpr VectorT(
			VectorT&& other
		) noexcept = default;

	public:
		constexpr ~VectorT() noexcept = default;

	public:
		static constexpr VectorT GetOne()
		{
			return VectorT(1, 1, 1);
		}

		static constexpr VectorT GetZero()
		{
			return VectorT(0, 0, 0);
		}

		static constexpr VectorT GetBack()
		{
			return VectorT(0, -1, 0);
		}

		static constexpr VectorT GetDown()
		{
			return VectorT(0, 0, -1);
		}

		static constexpr VectorT GetForward()
		{
			return VectorT(0, 1, 0);
		}

		static constexpr VectorT GetLeft()
		{
			return VectorT(-1, 0, 0);
		}

		static constexpr VectorT GetRight()
		{
			return VectorT(1, 0, 0);
		}

		static constexpr VectorT GetUp()
		{
			return VectorT(0, 0, 1);
		}

	public:
		constexpr TNumeric GetElement(
			uintSize index
		)
		{
			return (*this)[index];
		}

	public:
		constexpr VectorT& operator=(
			const VectorT& other
		) = default;

		constexpr VectorT& operator=(
			VectorT&& other
		) noexcept = default;

	public:
		#include "_internal/math/vector/member-operators.inl"
	};


	template<class TNumeric>
	struct VectorT<TNumeric, 4>
	{
	public:
		static constexpr auto Size = 4;

	public:
		using ValueType = TNumeric;
		using ArrayType = TNumeric[Size];
		using SimdType = If<sizeof(TNumeric) * Size <= sizeof(Vector128), Vector128, Vector256>;

	public:
		union alignas(SimdType)
		{
			struct
			{
				TNumeric X, Y, Z, W;
			};


			// struct
			// {
			// 	TNumeric R, G, B, A;
			// };
			//
			//
			// struct
			// {
			// 	TNumeric S, T, P, Q;
			// };


			ArrayType Array;
			SimdType Simd = {};
		};

	public:
		constexpr VectorT() noexcept = default;

		constexpr explicit VectorT(
			const TNumeric value
		) noexcept
		{
			X = value;
			Y = value;
			Z = value;
			W = value;
		}

		constexpr explicit VectorT(
			const TNumeric x,
			const TNumeric y,
			const TNumeric z,
			const TNumeric w
		) noexcept
		{
			X = x;
			Y = y;
			Z = z;
			W = w;
		}

		constexpr explicit VectorT(
			const SimdType vector
		) noexcept:
			Simd(vector) {}

		constexpr explicit VectorT(
			const TNumeric array[Size]
		) noexcept
		{
			for (auto i = 0; i < Size; ++i)
			{
				Array[i] = array[i];
			}
		}

		constexpr VectorT(
			const VectorT& other
		) = default;

		constexpr VectorT(
			VectorT&& other
		) noexcept = default;

	public:
		constexpr ~VectorT() noexcept = default;

	public:
		static constexpr VectorT GetOne()
		{
			return VectorT(1, 1, 1, 1);
		}

		static constexpr VectorT GetZero()
		{
			return VectorT(0, 0, 0, 0);
		}

	public:
		constexpr TNumeric GetElement(
			uintSize index
		)
		{
			return (*this)[index];
		}

	public:
		constexpr VectorT& operator=(
			const VectorT& other
		) = default;

		constexpr VectorT& operator=(
			VectorT&& other
		) noexcept = default;

	public:
		#include "_internal/math/vector/member-operators.inl"
	};


	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator+(
		const VectorT<TNumeric, VSize>& vector
	)
	{
		return vector;
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator-(
		const VectorT<TNumeric, VSize>& vector
	)
	{
		return VectorT<TNumeric, VSize>() -= vector;
	}


	template<class TNumeric, uint16 VSize>
		requires (IsIntValue<TNumeric>)
	constexpr VectorT<TNumeric, VSize> operator~(
		const VectorT<TNumeric, VSize>& vector
	)
	{
		return VectorT<TNumeric, VSize>(
			BitNot<typename VectorT<TNumeric, VSize>::SimdType, TNumeric>(vector.Simd)
		);
	}


	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
		requires (IsCastableValue<TValue, TNumeric>)
	constexpr VectorT<TNumeric, VSize> operator+(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) += static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
		requires (IsCastableValue<TValue, TNumeric>)
	constexpr VectorT<TNumeric, VSize> operator-(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) -= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
		requires (IsCastableValue<TValue, TNumeric>)
	constexpr VectorT<TNumeric, VSize> operator*(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) *= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
		requires (IsCastableValue<TValue, TNumeric>)
	constexpr VectorT<TNumeric, VSize> operator/(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) /= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
		requires (IsIntValue<TNumeric>)
	constexpr VectorT<TNumeric, VSize> operator%(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) %= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator+(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) += right;
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator-(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) -= right;
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator*(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) *= right;
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator/(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) /= right;
	}

	template<class TNumeric, uint16 VSize>
		requires (IsIntValue<TNumeric>)
	constexpr VectorT<TNumeric, VSize> operator%(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) %= right;
	}


	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
	constexpr VectorT<TNumeric, VSize> operator&(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) &= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
	constexpr VectorT<TNumeric, VSize> operator|(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) |= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
	constexpr VectorT<TNumeric, VSize> operator^(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) ^= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
	constexpr VectorT<TNumeric, VSize> operator<<(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) <<= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize, class TValue = TNumeric>
	constexpr VectorT<TNumeric, VSize> operator>>(
		const VectorT<TNumeric, VSize>& vector,
		TValue value
	)
	{
		return VectorT<TNumeric, VSize>(vector) >>= static_cast<TNumeric>(value);
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator&(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) &= right;
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator|(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) |= right;
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator^(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) ^= right;
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator<<(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) <<= right;
	}

	template<class TNumeric, uint16 VSize>
	constexpr VectorT<TNumeric, VSize> operator>>(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return VectorT<TNumeric, VSize>(left) >>= right;
	}


	template<class TNumeric, uint16 VSize>
	constexpr bool operator&&(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		uint8 result = 1;
		for (auto i = 0; i < VSize; ++i)
		{
			result *= left[i] && right[i];
		}
		return static_cast<bool>(result);
	}

	template<class TNumeric, uint16 VSize>
	constexpr bool operator||(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		uint8 result = 0;
		for (auto i = 0; i < VSize; ++i)
		{
			result += left[i] || right[i];
		}
		return result == VSize;
	}

	template<class TNumeric, uint16 VSize>
	constexpr bool operator==(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		const auto result = Equal<typename VectorT<TNumeric, VSize>::SimdType, TNumeric>(
			left.Simd,
			right.Simd
		);
		return BoolComparison<typename VectorT<TNumeric, VSize>::SimdType, TNumeric>(result);
	}

	template<class TNumeric, uint16 VSize>
	constexpr bool operator!=(
		const VectorT<TNumeric, VSize>& left,
		const VectorT<TNumeric, VSize>& right
	)
	{
		return !(left == right);
	}


	template<class TNumeric>
	using Vector2T = VectorT<TNumeric, 2>;

	using Vector2 = VectorT<float32, 2>;

	using Vector2Float64 = VectorT<float64, 2>;

	using Vector2Int32 = VectorT<int32, 2>;


	template<class TNumeric>
	using Vector3T = VectorT<TNumeric, 3>;

	using Vector3 = VectorT<float32, 3>;

	using Vector3Float64 = VectorT<float64, 3>;

	using Vector3Int32 = VectorT<int32, 3>;


	template<class TNumeric>
	using Vector4T = VectorT<TNumeric, 4>;

	using Vector4 = VectorT<float32, 4>;

	using Vector4Float64 = VectorT<float64, 4>;

	using Vector4Int32 = VectorT<int32, 4>;
}
