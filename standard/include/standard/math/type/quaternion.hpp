﻿#pragma once
#include "vector.hpp"


namespace Std::inline Math::inline Type
{
	template<class T>
	using QuaternionT = Vector4T<T>;

	template<class T>
	using Quaternion = Vector4;
}
