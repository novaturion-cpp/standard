﻿#pragma once
#include "type-traits.hpp"
#include "standard/platform/compiler.hpp"


namespace Std::inline Utility
{
	/// @brief	Obtain a const reference to given argument
	template<class T>
	[[nodiscard]]
	constexpr AddConst<T>& AsConst(
		T& value
	) noexcept
	{
		return value;
	}


	/// @brief	Obtain a const reference to given argument
	template<class T>
	[[noreturn]]
	constexpr void AsConst(
		T&& value
	) = delete;


	/// @brief	Obtain a value type of the given
	/// argument for use in unevaluated context
	template<class T>
	constexpr RemoveReference<T> DeclareValue() noexcept;


	/// @brief	Obtain an lvalue reference to the
	/// given argument for use in unevaluated context
	template<class T>
	constexpr AddLValueReference<T> DeclareLValue() noexcept;


	/// @brief	Obtain a rvalue reference to the
	/// given argument for use in unevaluated context
	template<class T>
	constexpr AddRValueReference<T> DeclareRValue() noexcept;


	/// @brief	Convert an enumeration to its underlying type
	template<class T>
		requires (IsEnumValue<T>)
	[[nodiscard]]
	EnumUnderlyingType<T> EnumAsUnderlyingType(
		const T& value
	)
	{
		return static_cast<EnumUnderlyingType<T>>(value);
	}


	/// @brief	Replace the argument with a new value
	/// @return	Previous value of argument
	template<class TA, class TB = TA>
	constexpr TA Exchange(
		TA& argument,
		TB&& newValue
	) noexcept(AndValue<
		IsMoveConstructibleNoThrow<TA>,
		IsAssignableNoThrow<TA&, TB>
	>)
	{
		TA oldValue = static_cast<TA&&>(argument);
		argument = static_cast<TB&&>(newValue);
		return oldValue;
	}


	/// @brief	Obtain an lvalue or rvalue reference to the given argument
	/// if its type is an lvalue or rvalue reference correspondingly
	template<class T>
	[[nodiscard]]
	constexpr T&& Forward(
		RemoveReference<T>& value
	) noexcept
	{
		return static_cast<T&&>(value);
	}


	/// @brief	Obtain an lvalue or rvalue reference to the given argument
	/// if its type is an lvalue or rvalue reference correspondingly 
	template<class T>
		requires (!IsLValueReferenceValue<T>)
	[[nodiscard]]
	constexpr T&& Forward(
		RemoveReference<T>&& value
	) noexcept
	{
		return static_cast<T&&>(value);
	}


	/// @brief	Detect whether the call occurs within a constant-evaluated context
	constexpr bool IsConstantEvaluated() noexcept
	{
		return __builtin_is_constant_evaluated();
	}


	/// @brief	Obtain a rvalue reference to the given argument
	template<class T>
	constexpr RemoveReference<T>&& Move(
		T&& value
	) noexcept
	{
		return static_cast<RemoveReference<T>&&>(value);
	}


	/// @brief	Obtain an rvalue reference to the given
	/// argument if its move constructor does not throw
	template<class T>
	constexpr If<
		!IsMoveConstructibleNoThrowValue<T> &&
		IsCopyConstructibleValue<T>,
		const T&,
		T&&> MoveIfNoexcept(
		T& value
	) noexcept
	{
		return Move(value);
	}


	/// @brief	Swap the values of two objects
	template<class T>
		requires (
			IsMoveConstructibleValue<T>
			&& IsMoveAssignableValue<T>
		)
	constexpr void Swap(
		T& left,
		T& right
	) noexcept(
		IsMoveConstructibleNoThrowValue<T> &&
		IsMoveAssignableNoThrowValue<T>
	)
	{
		T temp = Move(left);
		left = Move(right);
		right = Move(temp);
	}


	/// @brief	Swap the elements pointed to by two iterators
	template<class TA, class TB>
		requires (IsPointerValue<TA> && IsPointerValue<TB>)
	constexpr void SwapPointers(
		TA left,
		TB right
	)
	{
		Swap(*left, *right);
	}


	/// @brief	Swap the values of two objects
	template<class T, uintSize VSize>
		requires (IsSwappableValue<T>)
	constexpr void Swap(
		T (&left)[VSize],
		T (&right)[VSize]
	) noexcept(IsSwappableNoThrowValue<T>)
	{
		if (&left == &right)
		{
			return;
		}

		auto leftFirst = left;
		auto leftLast = leftFirst + VSize;
		auto rightFirst = right;
		for (; leftFirst != leftLast; ++leftFirst, ++rightFirst)
		{
			SwapPointers(leftFirst, rightFirst);
		}
	}


	/// @brief	Mark unreachable point of execution
	[[noreturn]]
	inline void Unreachable()
	{
		#if COMPILER_IS_GCC || COMPILER_IS_CLANG
			__builtin_unreachable();
		#elif COMPILER_IS_MSVC
		__assume(false);
		#endif
	}
}
