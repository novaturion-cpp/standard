﻿#pragma once


namespace Std::inline Utility::inline Type
{
	template<class T, T VValue>
	struct Constant
	{
		using ValueType = T;
		using ConstantType = Constant;

		static inline constexpr T Value = VValue;

		constexpr operator T() const noexcept
		{
			return Value;
		}

		[[nodiscard]]
		constexpr T operator()() const noexcept
		{
			return Value;
		}
	};
}
