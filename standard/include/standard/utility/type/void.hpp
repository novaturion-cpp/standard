﻿#pragma once


namespace Std::inline Utility::inline Type
{
	template<class...>
	using Void = void;
}
