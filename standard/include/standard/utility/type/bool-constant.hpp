﻿#pragma once
#include "constant.hpp"


namespace Std::inline Utility::inline Type
{
	template<bool VValue = false>
	using BoolConstant = Constant<bool, VValue>;

	using TrueConstant = BoolConstant<true>;
	using FalseConstant = BoolConstant<false>;
}
