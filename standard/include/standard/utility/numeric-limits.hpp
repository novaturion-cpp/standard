﻿#pragma once
#include "standard/platform/type.hpp"
#include "_internal/utility/numeric-limits.hpp"


namespace Std::inline Utility::inline Type
{
	template<class T>
	struct NumericLimits
	{
		static inline constexpr auto IsSpecialized = false;
	};


	template<class T>
	struct NumericLimits<const T> : NumericLimits<T> {};


	template<class T>
	struct NumericLimits<volatile T> : NumericLimits<T> {};


	template<class T>
	struct NumericLimits<const volatile T> : NumericLimits<T> {};


	template<>
	struct NumericLimits<int8> : _Internal::_NumericLimitsInt<int8>
	{
		static inline constexpr auto Digits = 2;
	};


	template<>
	struct NumericLimits<int16> : _Internal::_NumericLimitsInt<int16>
	{
		static inline constexpr auto Digits = 4;
	};


	template<>
	struct NumericLimits<int32> : _Internal::_NumericLimitsInt<int32>
	{
		static inline constexpr auto Digits = 9;
	};


	template<>
	struct NumericLimits<int64> : _Internal::_NumericLimitsInt<int64>
	{
		static inline constexpr auto Digits = 18;
	};


	template<>
	struct NumericLimits<uint8> : _Internal::_NumericLimitsInt<uint8>
	{
		static inline constexpr auto IsHandleOverflow = true;

		static inline constexpr auto Digits = 2;
	};


	template<>
	struct NumericLimits<uint16> : _Internal::_NumericLimitsInt<uint16>
	{
		static inline constexpr auto IsHandleOverflow = true;

		static inline constexpr auto Digits = 4;
	};


	template<>
	struct NumericLimits<uint32> : _Internal::_NumericLimitsInt<uint32>
	{
		static inline constexpr auto IsHandleOverflow = true;

		static inline constexpr auto Digits = 9;
	};


	template<>
	struct NumericLimits<uint64> : _Internal::_NumericLimitsInt<uint64>
	{
		static inline constexpr auto IsHandleOverflow = true;

		static inline constexpr auto Digits = 19;
	};


	template<>
	struct NumericLimits<float32> : _Internal::_NumericLimitsFloat<float32, 8>
	{
		static inline constexpr auto Digits = 6;
		static inline constexpr auto DigitsMax = 9;

		static inline constexpr auto Epsilon = 1.192092896e-07_f32;
		static inline constexpr auto Infinity = static_cast<float32>(1e+300 * 1e+300);
		static inline constexpr auto QuietNan = __builtin_nanf("0");
		static inline constexpr auto SignalingNan = __builtin_nanf("1");

		static inline constexpr auto Max = 3.402823466e+38_f32;
		static inline constexpr auto Min = 1.175494351e-38_f32;
		static inline constexpr auto RealMin = 1.401298464e-45_f32;
	};


	template<>
	struct NumericLimits<float64> : _Internal::_NumericLimitsFloat<float64, 11>
	{
		static inline constexpr auto Digits = 15;
		static inline constexpr auto DigitsMax = 17;

		static inline constexpr auto Epsilon = 2.220446049250313e-16_f64;
		static inline constexpr auto Infinity = static_cast<float64>(1e+300 * 1e+300);
		static inline constexpr auto QuietNan = __builtin_nan("0");
		static inline constexpr auto SignalingNan = __builtin_nans("1");

		static inline constexpr auto Max = 1.7976931348623157e+308_f64;
		static inline constexpr auto Min = 2.2250738585072014e-308_f64;
		static inline constexpr auto RealMin = 4.9406564584124654e-324_f64;
	};


	// todo: handle float128

	template<>
	struct NumericLimits<float128> : NumericLimits<float64> {};
}
