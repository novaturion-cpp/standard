﻿#pragma once
#include "_internal/utility/type-traits.hpp"


namespace Std::inline Utility::inline TypeTrait
{
	// todo: IsCorrespondingMember
	// /// @brief	Check if two specified members correspond to each
	// /// other in the common initial subsequence of two specified types
	// template<class TClassA, class TClassB, class TMemberA, class TMemberB>
	// constexpr bool IsCorrespondingMember(;

	// 	TMemberA TClassA::* memberA,
	// 	TMemberB TClassB::* member2
	// ) noexcept
	// {
	// 	return __is_corresponding_member(TClassA, TClassB, memberA, member2);
	// }

	// todo: IsPointerInterconvertible
	// /// @brief	Check if objects of a type are pointer-interconvertible
	// /// with the specified member of that type
	// 	template<class T, class TMember>
	// 	constexpr bool IsPointerInterconvertibleClass(
	// 		TMember T::* member
	// 	)
	// 	noexcept
	// 	{
	// 		return __is_pointer_interconvertible_with_class(T, member);
	// 	}


	/// @brief	Perform an logical AND on the sequence of types
	template<class T, class... TArgs>
	using And = _Internal::And<T, TArgs...>;

	/// @brief	Perform an logical AND on the sequence of types
	template<class T, class... TArgs>
	inline constexpr bool AndValue = And<T, TArgs...>::Value;

	/// @brief	Obtain type as when passing a function argument by value
	template<class T>
	using Decay = typename _Internal::Decay<T>::Type;

	/// @brief	Conditionally removes a function overload o
	/// template specialization from overload resolution
	template<bool VCondition, class T = void>
	using EnableIf = typename _Internal::EnableIf<VCondition, T>::Type;

	/// @brief	Get the type argument unchanged
	template<class T>
	using Identity = typename _Internal::Identity<T>::Type;

	/// @brief	Chooses TTrue if Condition is true, otherwise TFalse
	template<bool VCondition, class TTrue, class TFalse>
	using If = typename _Internal::If<VCondition, TTrue, TFalse>::Type;

	/// @brief	Make the given integral type signed
	template<class T>
	using MakeSigned = typename _Internal::MakeSigned<T>::Type;

	/// @brief	Make the given integral type unsigned
	template<class T>
	using MakeUnsigned = typename _Internal::MakeUnsigned<T>::Type;

	/// @brief	Form the logical NOT of the type
	template<class T>
	using Not = _Internal::Not<T>;

	/// @brief	Form the logical NOT of the type
	template<class T>
	inline constexpr bool NotValue = Not<T>::Value;

	/// @brief	Perform an logical OR on the sequence of types
	template<class T, class... TArgs>
	using Or = _Internal::Or<T, TArgs...>;

	/// @brief	Perform an logical OR on the sequence of types
	template<class T, class... TArgs>
	inline constexpr bool OrValue = Or<T, TArgs...>::Value;


	/// @brief	Add const qualifier to type T
	template<class T>
	using AddConst = typename _Internal::AddConst<T>::Type;

	/// @brief	Add volatile qualifier to type T
	template<class T>
	using AddVolatile = typename _Internal::AddVolatile<T>::Type;

	/// @brief	Add const and volatile qualifiers to type T
	template<class T>
	using AddConstVolatile = AddVolatile<AddConst<T>>;

	/// @brief	Add pointer to type T
	template<class T>
	using AddPointer = typename _Internal::AddPointer<T>::Type;

	/// @brief	Add const qualifier pointer to type T
	template<class T>
	using AddConstPointer = AddConst<AddPointer<T>>;

	/// @brief	Add volatile qualifier pointer to type T
	template<class T>
	using AddVolatilePointer = AddVolatile<AddPointer<T>>;

	/// @brief	Add const and volatile qualifiers pointer to type T
	template<class T>
	using AddConstVolatilePointer = AddConst<AddVolatilePointer<T>>;

	/// @brief	Add lvalue reference to type T
	template<class T>
	using AddLValueReference = typename _Internal::AddLValueReference<T>::Type;

	/// @brief	Remove const qualifier from type T
	template<class T>
	using RemoveConst = typename _Internal::RemoveConst<T>::Type;

	/// @brief	Remove volatile qualifier from type T
	template<class T>
	using RemoveVolatile = typename _Internal::RemoveVolatile<T>::Type;

	/// @brief	Remove const and volatile qualifiers from type T
	template<class T>
	using RemoveConstVolatile = _Internal::RemoveConstVolatile<T>;

	/// @brief	Remove reference from type T
	template<class T>
	using RemoveReference = typename _Internal::RemoveReference<T>::Type;

	/// @brief	Remove pointer from type T
	template<class T>
	using RemovePointer = typename _Internal::RemovePointer<RemoveConstVolatile<T>>::Type;

	/// @brief	Remove pointer and/or reference from type T
	template<class T>
	using RemovePointerReference = RemovePointer<RemoveReference<T>>;

	/// @brief	Add const qualifier to type under the pointer
	template<class T>
	using AddPointerToConst = AddPointer<AddConst<RemovePointerReference<T>>>;

	/// @brief	Add const and volatile qualifiers to type under the pointer
	template<class T>
	using AddPointerToConstVolatile = AddPointer<AddConst<AddVolatile<RemovePointerReference<T>>>>;

	/// @brief	Add volatile qualifier to type under the pointer
	template<class T>
	using AddPointerToVolatile = AddPointer<AddVolatile<RemovePointerReference<T>>>;

	/// @brief	Add rvalue reference to type T
	template<class T>
	using AddRValueReference = typename _Internal::AddRValueReference<T>::Type;

	/// @brief	Obtain the type's alignment requirements
	template<class T>
	inline constexpr uintSize AlignmentOf = Constant<uintSize, alignof(T)>::Value;

	/// @brief	Get the size of an array type along a specified dimension
	template<class T, uintSize VDimension = 0>
	using ArrayLength = Constant<uintSize, _Internal::ArraySize<T, VDimension>>;

	/// @brief	Get the size of an array type along a specified dimension
	template<class T, uintSize VDimension = 0>
	inline constexpr uintSize ArrayLengthValue = ArrayLength<T, VDimension>::Value;

	/// @brief	Get the number of dimensions of an array type
	template<class T>
	using ArrayRank = Constant<uintSize, _Internal::ArrayRank<T>>;

	/// @brief	Get the number of dimensions of an array type
	template<class T>
	inline constexpr uintSize ArrayRankValue = ArrayRank<T>::Value;


	// todo: common reference


	/// @brief	Determine the common type of a group of types
	template<class... TArgs>
	using CommonType = typename _Internal::CommonType<TArgs...>::Type;

	/// @brief	Copy qualifiers from type TFrom to type TTo
	template<class TFrom, class TTo>
	using CopyConstVolatile = typename _Internal::CopyReplaceConstVolatile<TFrom, TTo>::Copy;

	/// @brief	Obtain the float type which can contain a value of type T
	template<class T>
	using CorrespondingFloat = typename _Internal::CorrespondingFloat<sizeof(T)>::Type;

	/// @brief	Obtain the int type which can contain a value of type T
	template<class T>
	using CorrespondingInt = typename _Internal::CorrespondingInt<sizeof(T)>::Type;

	/// @brief	Obtain the uint type which can contain a value of type T
	template<class T>
	using CorrespondingUint = typename _Internal::CorrespondingUint<sizeof(T)>::Type;

	/// @brief	Obtain the underlying integer type for a given enumeration type
	template<class T>
	using EnumUnderlyingType = typename _Internal::EnumUnderlyingType<T>::Type;

	/// @brief	Check if every bit in the type's object representation contributes to its value
	template<class T>
	using HasUniqueObjectRepresentation = BoolConstant<__has_unique_object_representations(T)>;

	/// @brief	Check if every bit in the type's object representation contributes to its value
	template<class T>
	inline constexpr bool HasUniqueObjectRepresentationValue = HasUniqueObjectRepresentation<T>::Value;

	/// @brief	Check if a type has a virtual destructor
	template<class T>
	using HasVirtualDestructor = BoolConstant<__has_virtual_destructor(T)>;

	/// @brief	Check if a type has a virtual destructor
	template<class T>
	inline constexpr bool HasVirtualDestructorValue = HasVirtualDestructor<T>::Value;

	/// @brief	Check if a type is an abstract class type
	template<class T>
	using IsAbstract = BoolConstant<__is_abstract(T)>;

	/// @brief	Check if a type is an abstract class type
	template<class T>
	inline constexpr bool IsAbstractValue = IsAbstract<T>::Value;

	/// @brief	Check if a type is a raw array type
	template<class T>
	using IsArray = BoolConstant<_Internal::IsArray<T>>;

	/// @brief	Check if a type is a raw array type
	template<class T>
	inline constexpr bool IsArrayValue = IsArray<T>::Value;

	/// @brief	Check if a type has an assignment operator for a specific argument
	template<class TTo, class TFrom>
	using IsAssignable = BoolConstant<_Internal::IsAssignable<TTo, TFrom>>;

	/// @brief	Check if a type has an assignment operator for a specific argument
	template<class TTo, class TFrom>
	inline constexpr bool IsAssignableValue = IsAssignable<TTo, TFrom>::Value;

	/// @brief	Check if a type has a noexcept assignment operator for a specific argument
	template<class TTo, class TFrom>
	using IsAssignableNoThrow = BoolConstant<_Internal::IsAssignableNoThrow<TTo, TFrom>>;

	/// @brief	Check if a type has a noexcept assignment operator for a specific argument
	template<class TTo, class TFrom>
	inline constexpr bool IsAssignableNoThrowValue = IsAssignableNoThrow<TTo, TFrom>::Value;

	/// @brief	Check if a type has a trivial assignment operator for a specific argument
	template<class TTo, class TFrom>
	using IsAssignableTrivially = BoolConstant<__is_trivially_assignable(TTo, TFrom)>;

	/// @brief	Check if a type has a trivial assignment operator for a specific argument
	template<class TTo, class TFrom>
	inline constexpr bool IsAssignableTriviallyValue = IsAssignableTrivially<TTo, TFrom>::Value;

	/// @brief	Check if a type is a bool type
	template<class T>
	using IsBool = BoolConstant<_Internal::IsBool<RemoveConstVolatile<T>>>;

	/// @brief	Check if a type is a bool type
	template<class T>
	inline constexpr bool IsBoolValue = IsBool<T>::Value;

	/// @brief	Check if a type is an array type of known bound
	template<class T>
	using IsBoundedArray = BoolConstant<_Internal::IsBoundedArray<T>>;

	/// @brief	Check if a type is an array type of known bound
	template<class T>
	inline constexpr bool IsBoundedArrayValue = IsBoundedArray<T>::Value;

	/// @brief	Check if a type can be converted to the other type
	template<class TFrom, class TTo>
	using IsCastable = BoolConstant<_Internal::IsCastable<TFrom, TTo>>;

	/// @brief	Check if a type can be converted to the other type
	template<class TFrom, class TTo>
	inline constexpr bool IsCastableValue = IsCastable<TFrom, TTo>::Value;

	/// @brief	Check if a type can be converted to the other type and conversion is noexcept
	template<class TFrom, class TTo>
	using IsCastableNoThrow = BoolConstant<_Internal::IsCastableNoThrow<TFrom, TTo>>;

	/// @brief	Check if a type can be converted to the other type and conversion is noexcept
	template<class TFrom, class TTo>
	inline constexpr bool IsCastableNoThrowValue = IsCastableNoThrow<TFrom, TTo>::Value;

	/// @brief	Check if a type is a symbol type
	template<class T>
	using IsChar = BoolConstant<_Internal::IsChar<RemoveConstVolatile<T>>>;

	/// @brief	Check if a type is a symbol type
	template<class T>
	inline constexpr bool IsCharValue = IsChar<T>::Value;

	/// @brief	Check if a type is a non-union class type
	template<class T>
	using IsClass = BoolConstant<__is_class(T)>;

	/// @brief	Check if a type is a non-union class type
	template<class T>
	inline constexpr bool IsClassValue = IsClass<T>::Value;

	/// @brief	Check if a type is const-qualified
	template<class T>
	using IsConst = BoolConstant<_Internal::IsConst<T>>;

	/// @brief	Check if a type is const-qualified
	template<class T>
	inline constexpr bool IsConstValue = IsConst<T>::Value;

	/// @brief	Check if a type is volatile-qualified
	template<class T>
	using IsVolatile = BoolConstant<_Internal::IsVolatile<T>>;

	/// @brief	Check if a type is volatile-qualified
	template<class T>
	inline constexpr bool IsVolatileValue = IsVolatile<T>::Value;

	/// @brief	Check if a type is const- and volatile-qualified
	template<class T>
	using IsConstVolatile = And<IsConst<T>, IsVolatile<T>>;

	/// @brief	Check if a type is const- and volatile-qualified
	template<class T>
	inline constexpr bool IsConstVolatileValue = IsConstVolatile<T>::Value;

	/// @brief	Check if a type has a constructor for specific arguments
	template<class T, class... TArgs>
	using IsConstructible = BoolConstant<_Internal::IsConstructible<T, TArgs...>>;

	/// @brief	Check if a type has a constructor for specific arguments
	template<class T, class... TArgs>
	inline constexpr bool IsConstructibleValue = IsConstructible<T, TArgs...>::Value;

	/// @brief	Check if a type has a noexcept constructor for specific arguments
	template<class T, class... TArgs>
	using IsConstructibleNoThrow = BoolConstant<_Internal::IsConstructibleNoThrow<T, TArgs...>>;

	/// @brief	Check if a type has a noexcept constructor for specific arguments
	template<class T, class... TArgs>
	inline constexpr bool IsConstructibleNoThrowValue = IsConstructibleNoThrow<T, TArgs...>::Value;

	/// @brief	Check if a type has a trivial constructor for specific arguments
	template<class T, class... TArgs>
	using IsConstructibleTrivially = BoolConstant<__is_trivially_constructible(T, TArgs...)>;

	/// @brief	Check if a type has a trivial constructor for specific arguments
	template<class T, class... TArgs>
	inline constexpr bool IsConstructibleTriviallyValue = IsConstructibleTrivially<T, TArgs...>::Value;

	/// @brief	Check if a type has a copy assignment operator
	template<class T>
	using IsCopyAssignable = IsAssignable<AddLValueReference<T>, AddLValueReference<const T>>;

	/// @brief	Check if a type has a noexcept copy assignment operator
	template<class T>
	using IsCopyAssignableNoThrow = IsAssignableNoThrow<AddLValueReference<T>, AddLValueReference<const T>>;

	/// @brief	Check if a type has a noexcept copy assignment operator
	template<class T>
	inline constexpr bool IsCopyAssignableNoThrowValue = IsCopyAssignableNoThrow<T>::Value;

	/// @brief	Check if a type has a trivial copy assignment operator
	template<class T>
	using IsCopyAssignableTrivially = IsAssignableTrivially<AddLValueReference<T>, AddLValueReference<const T>>;

	/// @brief	Check if a type has a trivial copy assignment operator
	template<class T>
	inline constexpr bool IsCopyAssignableTriviallyValue = IsCopyAssignableTrivially<T>::Value;

	/// @brief	Check if a type has a copy assignment operator
	template<class T>
	inline constexpr bool IsCopyAssignableValue = IsCopyAssignable<T>::Value;

	/// @brief	Check if a type can be constructed from lvalue reference
	template<class T>
	using IsCopyConstructible = IsConstructible<T, AddLValueReference<const T>>;

	/// @brief	Check if a type can be constructed from lvalue reference and constructor is noexcept
	template<class T>
	using IsCopyConstructibleNoThrow = IsConstructibleNoThrow<T, AddLValueReference<const T>>;

	/// @brief	Check if a type can be constructed from lvalue reference and constructor is noexcept
	template<class T>
	inline constexpr bool IsCopyConstructibleNoThrowValue = IsCopyConstructibleNoThrow<T>::Value;

	/// @brief	Check if a type can be constructed from lvalue reference and constructor is trivial
	template<class T>
	using IsCopyConstructibleTrivially = IsConstructibleTrivially<T, AddLValueReference<const T>>;

	/// @brief	Check if a type can be constructed from lvalue reference and constructor is trivial
	template<class T>
	inline constexpr bool IsCopyConstructibleTriviallyValue = IsCopyConstructibleTrivially<T>::Value;

	/// @brief	Check if a type can be constructed from lvalue reference
	template<class T>
	inline constexpr bool IsCopyConstructibleValue = IsCopyConstructible<T>::Value;

	/// @brief	Check if a type is a class (but not union) type and does not have non-static data members
	template<class T>
	using IsEmpty = BoolConstant<__is_empty(T)>;

	/// @brief	Check if a type is a class (but not union) type and does not have non-static data members
	template<class T>
	inline constexpr bool IsEmptyValue = IsEmpty<T>::Value;

	/// @brief	Check if a type is an enumeration type
	template<class T>
	using IsEnum = BoolConstant<_Internal::IsEnum<T>>;

	/// @brief	Check if a type is a scoped enumeration type
	template<class T>
	using IsEnumClass = _Internal::IsEnumClass<T>;

	/// @brief	Check if a type is a scoped enumeration type
	template<class T>
	inline constexpr bool IsEnumClassValue = IsEnumClass<T>::Value;

	/// @brief	Check if a type is an enumeration type
	template<class T>
	inline constexpr bool IsEnumValue = IsEnum<T>::Value;

	/// @brief	Check if a type is a pointer to an non-static class/struct member variable
	template<class T>
	using IsFieldPointer = BoolConstant<_Internal::IsFieldPointer<T>>;

	/// @brief	Check if a type is a pointer to an non-static class/struct member variable
	template<class T>
	inline constexpr bool IsFieldPointerValue = IsFieldPointer<T>::Value;

	/// @brief	Check if a type is a final class type
	template<class T>
	using IsFinal = BoolConstant<__is_final(T)>;

	/// @brief	Check if a type is a final class type
	template<class T>
	inline constexpr bool IsFinalValue = IsFinal<T>::Value;

	/// @brief	Check if a type is an floating point type
	template<class T>
	using IsFloat = BoolConstant<_Internal::IsFloat<RemoveConstVolatile<T>>>;

	/// @brief	Check if a type is an floating point type
	template<class T>
	inline constexpr bool IsFloatValue = IsFloat<T>::Value;

	/// @brief	Check if a type is a function type
	template<class T>
	using IsFunction = BoolConstant<_Internal::IsFunction<T>>;

	/// @brief	Check if a type is a function type
	template<class T>
	inline constexpr bool IsFunctionValue = IsFunction<T>::Value;

	/// @brief	Check if a type is an integer type
	template<class T>
	using IsInt = BoolConstant<_Internal::IsInteger<RemoveConstVolatile<T>>>;

	/// @brief	Check if a type is an integer type
	template<class T>
	inline constexpr bool IsIntValue = IsInt<T>::Value;

	/// @brief	Check if two types are layout-compatible
	template<class TA, class TB>
	using IsLayoutCompatible = BoolConstant<__is_layout_compatible(TA, TB)>;

	/// @brief	Check if two types are layout-compatible
	template<class TA, class TB>
	inline constexpr bool IsLayoutCompatibleValue = IsLayoutCompatible<TA, TB>::Value;

	/// @brief	Check if a type is an lvalue reference type
	template<class T>
	using IsLValueReference = BoolConstant<_Internal::IsLValueReference<T>>;

	/// @brief	Check if a type is an lvalue reference type
	template<class T>
	inline constexpr bool IsLValueReferenceValue = IsLValueReference<T>::Value;

	/// @brief	Check if a type is a pointer to a non-static class/struct member function
	template<class T>
	using IsMethodPointer = BoolConstant<_Internal::IsMethodPointer<RemoveConstVolatile<T>>>;

	/// @brief	Check if a type is a pointer to a non-static class/struct member function
	template<class T>
	inline constexpr bool IsMethodPointerValue = IsMethodPointer<T>::Value;

	/// @brief	Check if a type is a pointer to a non-static class/struct member variable/function
	template<class T>
	using IsMemberPointer = Or<IsFieldPointer<T>, IsMethodPointer<T>>;

	/// @brief	Check if a type is a pointer to a non-static class/struct member variable/function
	template<class T>
	inline constexpr bool IsMemberPointerValue = IsMemberPointer<T>::Value;

	/// @brief	Check if a type has a move assignment operator
	template<class T>
	using IsMoveAssignable = IsAssignable<AddLValueReference<T>, AddRValueReference<T>>;

	/// @brief	Check if a type has a move assignment operator
	template<class T>
	inline constexpr bool IsMoveAssignableValue = IsMoveAssignable<T>::Value;

	/// @brief	Check if a type has a noexcept move assignment operator
	template<class T>
	using IsMoveAssignableNoThrow = IsAssignableNoThrow<AddLValueReference<T>, AddRValueReference<T>>;

	/// @brief	Check if a type has a noexcept move assignment operator
	template<class T>
	inline constexpr bool IsMoveAssignableNoThrowValue = IsMoveAssignableNoThrow<T>::Value;

	/// @brief	Check if a type has a trivial move assignment operator
	template<class T>
	using IsMoveAssignableTrivially = IsAssignableTrivially<AddLValueReference<T>, AddRValueReference<T>>;

	/// @brief	Check if a type has a trivial move assignment operator
	template<class T>
	inline constexpr bool IsMoveAssignableTriviallyValue = IsMoveAssignableTrivially<T>::Value;

	/// @brief	Check if a type can be constructed from rvalue reference
	template<class T>
	using IsMoveConstructible = IsConstructible<T, AddRValueReference<T>>;

	/// @brief	Check if a type can be constructed from rvalue reference
	template<class T>
	inline constexpr bool IsMoveConstructibleValue = IsMoveConstructible<T>::Value;

	/// @brief	Check if a type can be constructed from rvalue reference and constructor is noexcept
	template<class T>
	using IsMoveConstructibleNoThrow = IsConstructibleNoThrow<T, AddRValueReference<T>>;

	/// @brief	Check if a type can be constructed from rvalue reference and constructor is noexcept
	template<class T>
	inline constexpr bool IsMoveConstructibleNoThrowValue = IsMoveConstructibleNoThrow<T>::Value;

	/// @brief	Check if a type can be constructed from rvalue reference and constructor is trivial
	template<class T>
	using IsMoveConstructibleTrivially = IsConstructibleTrivially<T, AddRValueReference<T>>;

	/// @brief	Check if a type can be constructed from rvalue reference and constructor is trivial
	template<class T>
	inline constexpr bool IsMoveConstructibleTriviallyValue = IsMoveConstructibleTrivially<T>::Value;

	/// @brief	Check if a type is null
	template<class T>
	using IsNull = BoolConstant<_Internal::IsNull<T>>;

	/// @brief	Check if a type is null
	template<class T>
	inline constexpr bool IsNullValue = IsNull<T>::Value;

	/// @brief	Check if a type is an integer or floating point type
	template<class T>
	using IsNumeric = BoolConstant<_Internal::IsNumeric<RemoveConstVolatile<T>>>;

	/// @brief	Check if a type is an integer or floating point type
	template<class T>
	inline constexpr bool IsNumericValue = IsNumeric<T>::Value;

	/// @brief	Check if a type is derived from the other type
	template<class TParent, class TDerived>
	using IsParent = BoolConstant<_Internal::IsParent<TParent, TDerived>>;

	/// @brief	Check if a type is derived from the other type
	template<class TParent, class TDerived>
	inline constexpr bool IsParentValue = IsParent<TParent, TDerived>::Value;

	/// @brief	Remove const volatile pointer from type T
	template<class T>
	using RemoveConstVolatilePointer = RemoveVolatile<RemoveConst<RemovePointer<T>>>;

	/// @brief	Check if a type is a pointer type
	template<class T>
	using IsPointer = BoolConstant<_Internal::IsPointer<RemoveConstVolatile<T>>>;

	/// @brief	Check if a type is a pointer-interconvertible (initial) base of another type
	template<class TParent, class TDerived>
	using IsPointerInterconvertibleParent = BoolConstant<
		__is_pointer_interconvertible_base_of(TParent, TDerived)
	>;

	/// @brief	Check if a type is a pointer-interconvertible (initial) base of another type
	template<class TParent, class TDerived>
	inline constexpr bool IsPointerInterconvertibleParentValue =
		IsPointerInterconvertibleParent<TParent, TDerived>::Value;

	/// @brief	Check if a type is a pointer type
	template<class T>
	inline constexpr bool IsPointerValue = IsPointer<T>::Value;

	/// @brief	Check if a type is a polymorphic class type
	template<class T>
	using IsPolymorphic = BoolConstant<__is_polymorphic(T)>;

	/// @brief	Check if a type is a polymorphic class type
	template<class T>
	inline constexpr bool IsPolymorphicValue = IsPolymorphic<T>::Value;

	/// @brief	Check if a type is void
	template<class T>
	using IsVoid = BoolConstant<_Internal::IsVoid<T>>;

	/// @brief	Check if a type is void
	template<class T>
	inline constexpr bool IsVoidValue = IsVoid<T>::Value;

	/// @brief	Check if a type is a primitive type
	template<class T>
	using IsPrimitive = Or<
		IsNumeric<RemoveConstVolatile<T>>,
		IsChar<RemoveConstVolatile<T>>,
		IsNull<RemoveConstVolatile<T>>,
		IsVoid<RemoveConstVolatile<T>>
	>;

	/// @brief	Check if a type is a primitive type
	template<class T>
	inline constexpr bool IsPrimitiveValue = IsPrimitive<T>::Value;

	/// @brief	Check if a type is a compound type
	template<class T>
	using IsCompound = Not<IsPrimitive<RemoveConstVolatile<T>>>;

	/// @brief	Check if a type is a compound type
	template<class T>
	inline constexpr bool IsCompoundValue = IsCompound<T>::Value;

	/// @brief	Check if a type is a rvalue reference type
	template<class T>
	using IsRValueReference = BoolConstant<_Internal::IsRValueReference<T>>;

	/// @brief	Check if a type is a rvalue reference type
	template<class T>
	inline constexpr bool IsRValueReferenceValue = IsRValueReference<T>::Value;

	/// @brief	Check if a type is a rvalue or lvalue reference type
	template<class T>
	using IsReference = BoolConstant<_Internal::IsReference<T>>;

	/// @brief	Check if a type is a rvalue or lvalue reference type
	template<class T>
	inline constexpr bool IsReferenceValue = IsReference<T>::Value;

	/// @brief	Remove qualifiers, references and pointers
	template<class T>
	using RemoveAll = _Internal::RemoveAll<T>;

	/// @brief	Check if types are the same, do not take
	/// into account qualifiers, references and pointers
	template<class TA, class TB>
	using IsSameBase = BoolConstant<_Internal::IsSame<RemoveAll<TA>, RemoveAll<TB>>>;

	/// @brief	Check if types are the same, do not take
	/// into account qualifiers, references and pointers
	template<class TA, class TB>
	inline constexpr bool IsSameBaseValue = IsSameBase<TA, TB>::Value;

	/// @brief	Check if types are exactly the same
	template<class TA, class TB>
	using IsSameStrong = BoolConstant<_Internal::IsSame<TA, TB>>;

	/// @brief	Check if types are exactly the same
	template<class TA, class TB>
	inline constexpr bool IsSameStrongValue = IsSameStrong<TA, TB>::Value;

	/// @brief	Check if types are exactly the same
	/// @note	Alias for IsSameStrong
	template<class TA, class TB>
	using IsSame = IsSameStrong<TA, TB>;

	/// @brief	Check if types are exactly the same
	/// @note	Alias for IsSameStrongValue
	template<class TA, class TB>
	inline constexpr bool IsSameValue = IsSameStrongValue<TA, TB>;

	/// @brief	Check if types are the same,
	/// do not take into account qualifiers
	template<class TA, class TB>
	using IsSameWeak = BoolConstant<_Internal::IsSame<RemoveConstVolatile<TA>, RemoveConstVolatile<TB>>>;

	/// @brief	Check if types are the same,
	/// do not take into account qualifiers
	template<class TA, class TB>
	inline constexpr bool IsSameWeakValue = IsSameStrong<TA, TB>::Value;

	/// @brief	Check if all types in TArgs are the same as type T,
	/// do not take into account qualifiers, references and pointers
	template<class T, class... TArgs>
	inline constexpr bool IsAllBaseValue = AndValue<IsSameBase<T, TArgs>...>;

	/// @brief	Check if all types in TArgs are the same as type T,
	/// do not take into account qualifiers, references and pointers
	template<class T, class... TArgs>
	using IsAllBase = BoolConstant<IsAllBaseValue<T, TArgs...>>;

	/// @brief	Check if all types in TArgs are exactly the same as type T
	template<class T, class... TArgs>
	inline constexpr bool IsAllStrongValue = AndValue<IsSameStrong<T, TArgs>...>;

	/// @brief	Check if all types in TArgs are exactly the same as type T
	template<class T, class... TArgs>
	using IsAllStrong = BoolConstant<IsAllStrongValue<T, TArgs...>>;

	/// @brief	Check if all types in TArgs are th
	/// same as type T, do not take into account qualifiers
	template<class T, class... TArgs>
	inline constexpr bool IsAllWeakValue = AndValue<IsSameWeak<T, TArgs>...>;

	/// @brief	Check if all types in TArgs are th
	/// same as type T, do not take into account qualifiers
	template<class T, class... TArgs>
	using IsAllWeak = BoolConstant<IsAllWeakValue<T, TArgs...>>;

	/// @brief	Check if one type in TArgs is exactly the same as type T
	template<class T, class... TArgs>
	inline constexpr bool IsAllValue = IsAllStrongValue<T, TArgs>;

	/// @brief	Check if one type in TArgs is exactly the same as type T
	template<class T, class... TArgs>
	using IsAll = IsAllStrong<T, TArgs...>;

	/// @brief	Check if one type in TArgs is the same as type T, do not tak
	/// into account qualifiers, references and pointers
	template<class T, class... TArgs>
	inline constexpr bool IsAnyBaseValue = OrValue<IsSameBase<T, TArgs>...>;

	/// @brief	Check if one type in TArgs is the same as type T, do not tak
	/// into account qualifiers, references and pointers
	template<class T, class... TArgs>
	using IsAnyBase = BoolConstant<IsAnyBaseValue<T, TArgs...>>;

	/// @brief	Check if one type in TArgs is exactly the same as type T
	template<class T, class... TArgs>
	inline constexpr bool IsAnyStrongValue = OrValue<IsSameStrong<T, TArgs>...>;

	/// @brief	Check if one type in TArgs is exactly the same as type T
	template<class T, class... TArgs>
	using IsAnyStrong = BoolConstant<IsAnyStrongValue<T, TArgs...>>;

	/// @brief	Check if one type in TArgs is the same as type T,
	/// do not take into account qualifiers
	template<class T, class... TArgs>
	inline constexpr bool IsAnyWeakValue = OrValue<IsSameWeak<T, TArgs>...>;

	/// @brief	Check if one type in TArgs is the same as type T,
	/// do not take into account qualifiers
	template<class T, class... TArgs>
	using IsAnyWeak = BoolConstant<IsAnyWeakValue<T, TArgs...>>;

	/// @brief	Check if one type in TArgs is exactly the same as type T
	template<class T, class... TArgs>
	inline constexpr bool IsAnyValue = IsAnyStrongValue<T, TArgs...>;

	/// @brief	Check if one type in TArgs is exactly the same as type T
	template<class T, class... TArgs>
	using IsAny = IsAnyStrong<T, TArgs...>;

	/// @brief	Check if a type is a scalar type
	template<class T>
	using IsScalar = BoolConstant<_Internal::IsScalar<T>>;

	/// @brief	Check if a type is a scalar type
	template<class T>
	inline constexpr bool IsScalarValue = IsScalar<T>::Value;

	/// @brief	Check if a type is signed type
	template<class T>
	using IsSigned = BoolConstant<_Internal::IsSigned<T>>;

	/// @brief	Check if a type is signed type
	template<class T>
	inline constexpr bool IsSignedValue = IsSigned<T>::Value;

	// todo: add tests
	/// @brief	Check if type is specialization of template
	template<class TSpecialization, template <class...> class T>
	using IsSpecialization = BoolConstant<_Internal::IsSpecialization<RemoveAll<TSpecialization>, T>>;

	/// @brief	Check if type is specialization of template
	template<class TSpecialization, template <class...> class T>
	inline constexpr bool IsSpecializationValue = IsSpecialization<TSpecialization, T>::Value;

	/// @brief	Check if a type is a standard-layout type
	template<class T>
	using IsStandardLayout = BoolConstant<__is_standard_layout(T)>;

	/// @brief	Check if a type is a standard-layout type
	template<class T>
	inline constexpr bool IsStandardLayoutValue = IsStandardLayout<T>::Value;

	/// @brief	Check if a type is trivial
	template<class T>
	using IsTrivial = BoolConstant<__is_trivial(T)>;

	/// @brief	Check if a type is trivial
	template<class T>
	inline constexpr bool IsTrivialValue = IsTrivial<T>::Value;

	/// @brief	Check if a type is an array type of unknown bound
	template<class T>
	using IsUnboundedArray = BoolConstant<_Internal::IsUnboundedArray<T>>;

	/// @brief	Check if a type is an array type of unknown bound
	template<class T>
	inline constexpr bool IsUnboundedArrayValue = IsUnboundedArray<T>::Value;

	/// @brief	Check if a type is union
	template<class T>
	using IsUnion = BoolConstant<__is_union(T)>;

	/// @brief	Check if a type is union
	template<class T>
	inline constexpr bool IsUnionValue = IsUnion<T>::Value;

	/// @brief	Check if a type is an object type
	template<class T>
	using IsObject = Or<
		IsArray<RemoveConstVolatile<T>>,
		IsClass<RemoveConstVolatile<T>>,
		IsScalar<RemoveConstVolatile<T>>,
		IsUnion<RemoveConstVolatile<T>>
	>;

	/// @brief	Check if a type is an object type
	template<class T>
	inline constexpr bool IsObjectValue = IsObject<T>::Value;

	/// @brief	Check if a type is unsigned type
	template<class T>
	using IsUnsigned = BoolConstant<_Internal::IsUnsigned<T>>;

	/// @brief	Check if a type is unsigned type
	template<class T>
	inline constexpr bool IsUnsignedValue = IsUnsigned<T>::Value;

	/// @brief	Remove one dimension from the given array type
	template<class T>
	using RemoveArrayDimension = typename _Internal::RemoveArrayDimension<T>::Type;

	/// @brief	Remove all dimensions from the given array type
	template<class T>
	using RemoveArrayDimensions = typename _Internal::RemoveArrayDimensions<T>::Type;

	/// @brief	Remove const pointer from type T
	template<class T>
	using RemoveConstPointer = typename _Internal::RemoveConstPointer<T>::Type;

	/// @brief	Remove const and reference from type T
	template<class T>
	using RemoveConstReference = RemoveConst<RemoveReference<T>>;

	/// @brief	Remove const, volatile and reference from type T
	template<class T>
	using RemoveConstVolatileReference = _Internal::RemoveConstVolatileReference<T>;

	/// @brief	Remove volatile pointer from type T
	template<class T>
	using RemoveVolatilePointer = RemoveVolatile<RemovePointer<T>>;

	/// @brief	Remove volatile and reference from type T
	template<class T>
	using RemoveVolatileReference = RemoveVolatile<RemoveReference<T>>;

	/// @brief	Replace qualifiers of type TTo with qualifiers of type TFrom
	template<class TFrom, class TTo>
	using ReplaceConstVolatile = typename _Internal::CopyReplaceConstVolatile<TFrom, TTo>::Replace;


	/// @brief	Obtain the result type of invoking a callable object with a set of arguments
	template<class T, class... TArgs>
	using InvokeResult = typename _Internal::InvokeResultInternal<T, TArgs...>::Type;

	/// @brief	Check if a type has non-deleted destructor
	template<class T>
	using IsDestructible = BoolConstant<_Internal::IsDestructible<T>>;

	/// @brief	Check if a type has non-deleted destructor
	template<class T>
	inline constexpr bool IsDestructibleValue = IsDestructible<T>::Value;

	/// @brief	Check if a type has non-deleted noexcept destructor
	template<class T>
	using IsDestructibleNoThrow = BoolConstant<_Internal::IsDestructibleNoThrow<T>>;

	/// @brief	Check if a type has non-deleted noexcept destructor
	template<class T>
	inline constexpr bool IsDestructibleNoThrowValue = IsDestructibleNoThrow<T>::Value;

	/// @brief	Check if a type has non-deleted trivial destructor
	template<class T>
	using IsDestructibleTrivially = BoolConstant<__is_trivially_destructible(T)>;

	/// @brief	Check if a type has non-deleted trivial destructor
	template<class T>
	inline constexpr bool IsDestructibleTriviallyValue = IsDestructibleTrivially<T>::Value;

	/// @brief	Check if objects of a type can be swapped
	template<class T>
	using IsSwappable = _Internal::IsSwappable<T>;

	/// @brief	Check if objects of a type can be swapped
	template<class T>
	inline constexpr bool IsSwappableValue = IsSwappable<T>::Value;

	/// @brief	Check if objects of a type can be swapped
	template<class T>
	using IsSwappableNoThrow = _Internal::IsSwappableNoThrow<T>;

	/// @brief	Check if objects of a type can be swapped
	template<class T>
	inline constexpr bool IsSwappableNoThrowValue = IsSwappableNoThrow<T>::Value;

	/// @brief	Check if objects of a type can be swapped with objects of same or different type
	template<class TA, class TB>
	using IsSwappableWith = _Internal::IsSwappableWith<TA, TB>;

	/// @brief	Check if objects of a type can be swapped with objects of same or different type
	template<class TA, class TB>
	using IsSwappableWithNoThrow = And<_Internal::IsSwappableWith<TA, TB>, _Internal::IsSwapDoNotThrow<TA, TB>>;

	/// @brief	Check if objects of a type can be swapped with objects of same or different type
	template<class TA, class TB>
	inline constexpr bool IsSwappableWithNoThrowValue = IsSwappableWithNoThrow<TA, TB>::Value;

	/// @brief	Check if objects of a type can be swapped with objects of same or different type
	template<class TA, class TB>
	inline constexpr bool IsSwappableWithValue = IsSwappableWith<TA, TB>::Value;

	/// @brief	Check if a type can be invoked (as if by Invoke) with the given argument types
	template<class T, class... TArgs>
	using IsInvocable = BoolConstant<_Internal::IsInvocable<T, TArgs...>>;

	/// @brief	Check if a type can be invoked (as if by Invoke) with the given argument types
	template<class T, class... TArgs>
	using IsInvocableNoThrow = BoolConstant<_Internal::IsInvocableNoThrow<T, TArgs...>>;

	/// @brief	Check if a type can be invoked (as if by Invoke) with the given argument types
	template<class T, class... TArgs>
	inline constexpr bool IsInvocableNoThrowValue = IsInvocableNoThrow<T, TArgs...>::Value;

	/// @brief	Check if a type can be invoked (as if by Invoke) with the given argument types
	template<class TReturn, class T, class... TArgs>
	using IsInvocableReturn = BoolConstant<_Internal::IsInvocableReturn<TReturn, T, TArgs...>>;

	/// @brief	Check if a type can be invoked (as if by Invoke) with the given argument types
	template<class TReturn, class T, class... TArgs>
	using IsInvocableReturnNoThrow = BoolConstant<_Internal::IsInvocableReturnNoThrow<TReturn, T, TArgs...>>;

	/// @brief	Check if a type can be invoked (as if by Invoke) with the given argument types
	template<class TReturn, class T, class... TArgs>
	inline constexpr bool IsInvocableReturnNoThrowValue = IsInvocableReturnNoThrow<TReturn, T, TArgs...>::Value;

	/// @brief	Check if a type can be invoked (as if by Invoke) with the given argument types
	template<class TReturn, class T, class... TArgs>
	inline constexpr bool IsInvocableReturnValue = IsInvocableReturn<TReturn, T, TArgs...>::Value;

	/// @brief	Check if a type can be invoked (as if by Invoke) with the given argument types
	template<class T, class... TArgs>
	inline constexpr bool IsInvocableValue = IsInvocable<T, TArgs...>::Value;
}
