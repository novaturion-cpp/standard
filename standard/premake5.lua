standardIncludeDir = os.getcwd() .. "/include/"

project ( path.getbasename(os.getcwd()) )
	kind "staticlib"

	stl "none"
	inheritdependencies "off"

	pchheader ( precompiledHeader )
	pchsource ( precompiledSource )

	targetname ( binaryName )

	targetdir ( binaryDir )
	objdir ( intermediateDir )

	includedirs {
		includeDir
	}
	files {
		projectFiles,
		inlineFiles,
		includeFiles,
		sourceFiles
	}
